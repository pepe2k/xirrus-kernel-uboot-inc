/* Copyright (C) 2011  Xirrus, Inc. All rights reserved.
 *
 * This work is fully derived from Cavium's cvmx-error-custom.c file.
 * It retains the kernel-level api for all interfaces therein witht the exeception
 * that the cvmx_error_handle_lmcx_mem_cfg0() routine now internally manages the
 * Cavium 52xx trace control unit. This is specifically for PR7659 and I dont
 * want to regen the toolchain (which contains the actual source - see ls -asl .)
 * for cvmx-error-custom.c
 *   -bcyr
 */

/***********************license start***************
 * Copyright (c) 2003-2010  Cavium Networks (support@cavium.com). All rights
 * reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.

 *   * Neither the name of Cavium Networks nor the names of
 *     its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written
 *     permission.

 * This Software, including technical data, may be subject to U.S. export  control
 * laws, including the U.S. Export Administration Act and its  associated
 * regulations, and may be subject to export or import  regulations in other
 * countries.

 * TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS"
 * AND WITH ALL FAULTS AND CAVIUM  NETWORKS MAKES NO PROMISES, REPRESENTATIONS OR
 * WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, WITH RESPECT TO
 * THE SOFTWARE, INCLUDING ITS CONDITION, ITS CONFORMITY TO ANY REPRESENTATION OR
 * DESCRIPTION, OR THE EXISTENCE OF ANY LATENT OR PATENT DEFECTS, AND CAVIUM
 * SPECIFICALLY DISCLAIMS ALL IMPLIED (IF ANY) WARRANTIES OF TITLE,
 * MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF
 * VIRUSES, ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET POSSESSION OR
 * CORRESPONDENCE TO DESCRIPTION. THE ENTIRE  RISK ARISING OUT OF USE OR
 * PERFORMANCE OF THE SOFTWARE LIES WITH YOU.
 ***********************license end**************************************/


/**
 * @file
 *
 * Prototypes for custom error handler function not handled by the default
 * message display error function.
 *
 * <hr>$Revision: 44252 $<hr>
 */
#ifdef CVMX_BUILD_FOR_LINUX_KERNEL
#include <linux/module.h>
#include <asm/octeon/cvmx.h>
#include <asm/octeon/cvmx-helper.h>
#include <asm/octeon/cvmx-l2c.h>
#include <asm/octeon/cvmx-pcie.h>
#include <asm/octeon/cvmx-pexp-defs.h>
#include <asm/octeon/cvmx-dfa-defs.h>
#include <asm/octeon/cvmx-gmxx-defs.h>
#include <asm/octeon/cvmx-lmcx-defs.h>
#include <asm/octeon/cvmx-pemx-defs.h>
#include <asm/octeon/cvmx-sriox-defs.h>
#include <asm/octeon/cvmx-tra.h>   //Xirrus mod

//#define PRINT_ERROR(format, ...) cvmx_safe_printf("ERROR " format, ##__VA_ARGS__)
#define PRINT_ERROR(format, ...) printk(KERN_ALERT format, ##__VA_ARGS__)
//#else
//#include "cvmx.h"
#include "cvmx-error.h"
#include "cvmx-error-custom.h"
//#include "cvmx-helper.h"
//#include "cvmx-l2c.h"
//#include "cvmx-pcie.h"
//#include "cvmx-interrupt.h"
#include "cvmx-ciu-defs.h"
#include "cvmx-helper-cfg.h"
#endif

/**
 * @INTERNAL
 * XAUI interfaces need to be reset whenever a local or remote fault
 * is detected. Calling autoconf takes the link through a reset.
 *
 * @param info
 *
 * @return
 */
static int __cvmx_error_handle_gmxx_rxx_int_reg(const struct cvmx_error_info *info)
{
#ifdef CVMX_ENABLE_PKO_FUNCTIONS
    int ipd_port = info->group_index;
    switch(ipd_port)
    {
        case 0x800:
            ipd_port = 0x840;
            break;
        case 0xa00:
            ipd_port = 0xa40;
            break;
        case 0xb00:
            ipd_port = 0xb40;
            break;
        case 0xc00:
            ipd_port = 0xc40;
            break;
    }
    cvmx_helper_link_autoconf(ipd_port);
#endif
    cvmx_write_csr(info->status_addr, info->status_mask);
    return 1;
}

/**
 * @INTERNAL
 * When NPEI_INT_SUM[C0_LDWN] is set, the PCIe block requires a shutdown and
 * initialization to bring the link back up. This handler does this for port 0.
 * Note that config space is not enumerated again, so the devices will still be
 * unusable.
 *
 * @param info
 *
 * @return
 */
static int __cvmx_error_handle_npei_int_sum_c0_ldwn(const struct cvmx_error_info *info)
{
    cvmx_ciu_soft_prst_t ciu_soft_prst;
    PRINT_ERROR("NPEI_INT_SUM[C0_LDWN]: Reset request due to link0 down status.\n");
    ciu_soft_prst.u64 = cvmx_read_csr(CVMX_CIU_SOFT_PRST);
    if (!ciu_soft_prst.s.soft_prst)
    {
        /* Attempt to automatically bring the link back up */
        cvmx_pcie_rc_shutdown(0);
        cvmx_pcie_rc_initialize(0);
    }
    cvmx_write_csr(CVMX_PEXP_NPEI_INT_SUM, cvmx_read_csr(CVMX_PEXP_NPEI_INT_SUM));
    return 1;
}

/**
 * @INTERNAL
 * When NPEI_INT_SUM[C1_LDWN] is set, the PCIe block requires a shutdown and
 * initialization to bring the link back up. This handler does this for port 1.
 * Note that config space is not enumerated again, so the devices will still be
 * unusable.
 *
 * @param info
 *
 * @return
 */
static int __cvmx_error_handle_npei_int_sum_c1_ldwn(const struct cvmx_error_info *info)
{
    cvmx_ciu_soft_prst_t ciu_soft_prst;
    PRINT_ERROR("NPEI_INT_SUM[C1_LDWN]: Reset request due to link1 down status.\n");
    ciu_soft_prst.u64 = cvmx_read_csr(CVMX_CIU_SOFT_PRST1);
    if (!ciu_soft_prst.s.soft_prst)
    {
        /* Attempt to automatically bring the link back up */
        cvmx_pcie_rc_shutdown(1);
        cvmx_pcie_rc_initialize(1);
    }
    cvmx_write_csr(CVMX_PEXP_NPEI_INT_SUM, cvmx_read_csr(CVMX_PEXP_NPEI_INT_SUM));
    return 1;
}

#define DECODE_FAILING_ADDRESS
//#define DECODE_FAILING_BIT

#ifdef DECODE_FAILING_BIT
#define _Db(x) (x)              /* Data Bit */
#define _Ec(x) (0x100+x)        /* ECC Bit */
#define _Ad(x) (0x200+x)        /* Address Bit */
#define _Bu(x) (0x400+x)        /* Burst */
#define _Un()  (-1)             /* Unused */
/* Use ECC Code as index to lookup corrected bit */
const static short lmc_syndrome_bits[256] = {
    /*        __ 0 __  __ 1 __  __ 2 __  __ 3 __  __ 4 __  __ 5 __  __ 6 __  __ 7 __  __ 8 __  __ 9 __  __ A __  __ B __  __ C __  __ D __  __ E __  __ F __ */
    /* 00: */ _Un(  ), _Ec( 0), _Ec( 1), _Un(  ), _Ec( 2), _Un(  ), _Un(  ), _Un(  ), _Ec( 3), _Un(  ), _Un(  ), _Db(17), _Un(  ), _Un(  ), _Db(16), _Un(  ),
    /* 10: */ _Ec( 4), _Un(  ), _Un(  ), _Db(18), _Un(  ), _Db(19), _Db(20), _Un(  ), _Un(  ), _Db(21), _Db(22), _Un(  ), _Db(23), _Un(  ), _Un(  ), _Un(  ),
    /* 20: */ _Ec( 5), _Un(  ), _Un(  ), _Db( 8), _Un(  ), _Db( 9), _Db(10), _Un(  ), _Un(  ), _Db(11), _Db(12), _Un(  ), _Db(13), _Un(  ), _Un(  ), _Un(  ),
    /* 30: */ _Un(  ), _Db(14), _Un(  ), _Un(  ), _Db(15), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Ad(34), _Un(  ),
    /* 40: */ _Ec( 6), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Ad( 7), _Ad( 8), _Un(  ), _Un(  ), _Ad( 9), _Db(33), _Un(  ), _Ad(10), _Un(  ), _Un(  ), _Db(32),
    /* 50: */ _Un(  ), _Ad(11), _Db(34), _Un(  ), _Db(35), _Un(  ), _Un(  ), _Db(36), _Db(37), _Un(  ), _Un(  ), _Db(38), _Un(  ), _Db(39), _Ad(12), _Un(  ),
    /* 60: */ _Un(  ), _Ad(13), _Db(56), _Un(  ), _Db(57), _Un(  ), _Un(  ), _Db(58), _Db(59), _Un(  ), _Un(  ), _Db(60), _Un(  ), _Db(61), _Ad(14), _Un(  ),
    /* 70: */ _Db(62), _Un(  ), _Un(  ), _Ad(15), _Un(  ), _Db(63), _Ad(16), _Un(  ), _Un(  ), _Ad(17), _Ad(18), _Un(  ), _Ad(19), _Un(  ), _Ad(20), _Un(  ),
    /* 80: */ _Ec( 7), _Un(  ), _Un(  ), _Ad(21), _Un(  ), _Ad(22), _Ad(23), _Un(  ), _Un(  ), _Ad(24), _Db(49), _Un(  ), _Ad(25), _Un(  ), _Un(  ), _Db(48),
    /* 90: */ _Un(  ), _Ad(26), _Db(50), _Un(  ), _Db(51), _Un(  ), _Un(  ), _Db(52), _Db(53), _Un(  ), _Un(  ), _Db(54), _Un(  ), _Db(55), _Ad(27), _Un(  ),
    /* A0: */ _Un(  ), _Ad(28), _Db(40), _Un(  ), _Db(41), _Un(  ), _Un(  ), _Db(42), _Db(43), _Un(  ), _Un(  ), _Db(44), _Un(  ), _Db(45), _Ad(29), _Un(  ),
    /* B0: */ _Db(46), _Un(  ), _Un(  ), _Ad(30), _Un(  ), _Db(47), _Ad(31), _Un(  ), _Un(  ), _Ad(32), _Ad(33), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ),
    /* C0: */ _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Db( 1), _Un(  ), _Un(  ), _Db( 0), _Un(  ),
    /* D0: */ _Un(  ), _Un(  ), _Un(  ), _Db( 2), _Un(  ), _Db( 3), _Db( 4), _Un(  ), _Un(  ), _Db( 5), _Db( 6), _Un(  ), _Db( 7), _Un(  ), _Un(  ), _Un(  ),
    /* E0: */ _Un(  ), _Un(  ), _Un(  ), _Db(24), _Un(  ), _Db(25), _Db(26), _Un(  ), _Un(  ), _Db(27), _Db(28), _Un(  ), _Db(29), _Un(  ), _Un(  ), _Un(  ),
    /* F0: */ _Un(  ), _Db(30), _Un(  ), _Un(  ), _Db(31), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  ), _Un(  )
};
#endif

/**
 * @INTERNAL
 * This error bit handler clears the status and prints failure infomation.
 *
 * @param info   Error register to check
 *
 * @return
 */
static int __cvmx_cn6xxx_lmc_ecc_error_display(const cvmx_error_info_t *info)
{
#ifdef DECODE_FAILING_ADDRESS
    cvmx_lmcx_config_t lmc_config;
    uint64_t fadr_physical, fadr_data;
#endif

    int ddr_controller = info->group_index;
    cvmx_lmcx_int_t lmc_int;
    cvmx_lmcx_fadr_t fadr;
    cvmx_lmcx_ecc_synd_t ecc_synd;
    int sec_err;
    int ded_err;
    int syndrome = -1;
    int phase;

    lmc_int.u64 = cvmx_read_csr(CVMX_LMCX_INT(ddr_controller));
    fadr.u64 = cvmx_read_csr(CVMX_LMCX_FADR(ddr_controller));
    ecc_synd.u64 = cvmx_read_csr(CVMX_LMCX_ECC_SYND(ddr_controller));
    /* This assumes that all bits in the status register are RO or R/W1C */
    cvmx_write_csr(info->status_addr, info->status_mask);

#ifdef DECODE_FAILING_ADDRESS
    lmc_config.u64 = cvmx_read_csr(CVMX_LMCX_CONFIG(ddr_controller));
#endif

    sec_err = lmc_int.s.sec_err;
    ded_err = lmc_int.s.ded_err;

    phase = ded_err ? ded_err : sec_err; /* Double bit errors take precedence. */

    switch (phase) {
    case 1:
        syndrome = ecc_synd.cn63xx.mrdsyn0;
        break;
    case 2:
        syndrome = ecc_synd.cn63xx.mrdsyn1;
        break;
    case 4:
        syndrome = ecc_synd.cn63xx.mrdsyn2;
        break;
    case 8:
        syndrome = ecc_synd.cn63xx.mrdsyn3;
        break;
    }

#ifdef DECODE_FAILING_ADDRESS
    fadr_physical  = (uint64_t)fadr.cn63xx.fdimm      << (lmc_config.s.pbank_lsb + 28);
    fadr_physical |= (uint64_t)fadr.cn63xx.frow       << (lmc_config.s.row_lsb + 14);
    fadr_physical |= (uint64_t)fadr.cn63xx.fbank      << 7;
    fadr_physical |= (uint64_t)(fadr.cn63xx.fcol&0xf) << 3;
    fadr_physical |= (uint64_t)(fadr.cn63xx.fcol>>4)  << 10;

    fadr_data = *(uint64_t*)cvmx_phys_to_ptr(fadr_physical);
#endif

    PRINT_ERROR("LMC%d ECC: sec_err:%d ded_err:%d\n"
                "LMC%d ECC:\tFailing dimm:   %u\n"
                "LMC%d ECC:\tFailing rank:   %u\n"
                "LMC%d ECC:\tFailing bank:   %u\n"
                "LMC%d ECC:\tFailing row:    0x%x\n"
                "LMC%d ECC:\tFailing column: 0x%x\n"
                "LMC%d ECC:\tsyndrome: 0x%x"
#ifdef DECODE_FAILING_BIT
                ", bit: %d"
#endif
                "\n"
#ifdef DECODE_FAILING_ADDRESS
                "Failing  Address: 0x%016llx, Data: 0x%016llx\n"
#endif
                ,               /* Comma */
                ddr_controller, sec_err, ded_err,
                ddr_controller, fadr.cn63xx.fdimm,
                ddr_controller, fadr.cn63xx.fbunk,
                ddr_controller, fadr.cn63xx.fbank,
                ddr_controller, fadr.cn63xx.frow,
                ddr_controller, fadr.cn63xx.fcol,
                ddr_controller, syndrome
#ifdef DECODE_FAILING_BIT
                ,               /* Comma */
                lmc_syndrome_bits[syndrome]
#endif
#ifdef DECODE_FAILING_ADDRESS
                ,               /* Comma */
                (unsigned long long) fadr_physical, (unsigned long long) fadr_data
#endif
                );

    return 1;
}

/**
 * @INTERNAL
 * Some errors require more complicated error handing functions than the
 * automatically generated functions in cvmx-error-init-*.c. This function
 * replaces these handers with hand coded functions for these special cases.
 *
 * @return Zero on success, negative on failure.
 */
int __cvmx_error_custom_initialize(void)
{
    if (OCTEON_IS_MODEL(OCTEON_CN6XXX) || OCTEON_IS_MODEL(OCTEON_CN7XXX))
    {
        int lmc;
        for (lmc = 0; lmc < CVMX_L2C_TADS; lmc++)
        {
            cvmx_lmcx_dll_ctl2_t ctl;
            ctl.u64 = cvmx_read_csr(CVMX_LMCX_DLL_CTL2(lmc));

            if( (OCTEON_IS_MODEL(OCTEON_CN70XX) && (ctl.cn70xx.intf_en))  ||
                (OCTEON_IS_MODEL(OCTEON_CN71XX) && (ctl.cnf71xx.intf_en)) ||
                (OCTEON_IS_MODEL(OCTEON_CN78XX) && (ctl.cn78xx.intf_en))  ||
                (OCTEON_IS_MODEL(OCTEON_CN68XX) && (ctl.cn68xx.intf_en))  ||
                (OCTEON_IS_MODEL(OCTEON_CN61XX) && (ctl.cn61xx.intf_en))  )
                //(OCTEON_IS_MODEL(OCTEON_CN58XX) && (ctl.intf_en)) )// ||
            {
        cvmx_error_change_handler(CVMX_ERROR_REGISTER_IO64,
                            CVMX_LMCX_INT(lmc), 0xfull<<1 /* sec_err */,
                            __cvmx_cn6xxx_lmc_ecc_error_display, 0, NULL, NULL);
        cvmx_error_change_handler(CVMX_ERROR_REGISTER_IO64,
                            CVMX_LMCX_INT(lmc), 0xfull<<5 /* ded_err */,
                            __cvmx_cn6xxx_lmc_ecc_error_display, 0, NULL, NULL);
                if (!OCTEON_IS_MODEL(OCTEON_CN63XX_PASS1_X))
	        {
	            int i;

	            for (i = 0; i < 6; i++)
        cvmx_error_change_handler(CVMX_ERROR_REGISTER_IO64,
                            CVMX_L2C_TADX_INT(lmc), (1ull << i),
                            __cvmx_error_handle_63XX_l2_ecc, 0, NULL, NULL);
                }
            }
	}
    }
    if (OCTEON_IS_MODEL(OCTEON_CN52XX) || OCTEON_IS_MODEL(OCTEON_CN56XX) || OCTEON_IS_MODEL(OCTEON_CN6XXX))
    {
        int i;

        /* Install special handler for all the interfaces, these are
           specific to XAUI interface */
        for (i = 0; i < CVMX_HELPER_MAX_GMX; i++)
        {
            if ((OCTEON_IS_MODEL(OCTEON_CN63XX) || OCTEON_IS_MODEL(OCTEON_CN52XX)) && i == 1)
                continue;
        cvmx_error_change_handler(CVMX_ERROR_REGISTER_IO64,
                            CVMX_GMXX_RXX_INT_REG(0,i), 1ull<<21 /* rem_fault */,
                            __cvmx_error_handle_gmxx_rxx_int_reg, 0, NULL, NULL);
        cvmx_error_change_handler(CVMX_ERROR_REGISTER_IO64,
                            CVMX_GMXX_RXX_INT_REG(0,i), 1ull<<20 /* loc_fault */,
                            __cvmx_error_handle_gmxx_rxx_int_reg, 0, NULL, NULL);
    }
    }
    if (OCTEON_IS_MODEL(OCTEON_CN56XX))
    {
        cvmx_error_change_handler(CVMX_ERROR_REGISTER_IO64,
                            CVMX_PEXP_NPEI_INT_SUM, 1ull<<59 /* c0_ldwn */,
                            __cvmx_error_handle_npei_int_sum_c0_ldwn, 0, NULL, NULL);
        cvmx_error_change_handler(CVMX_ERROR_REGISTER_IO64,
                            CVMX_PEXP_NPEI_INT_SUM, 1ull<<60 /* c1_ldwn */,
                            __cvmx_error_handle_npei_int_sum_c1_ldwn, 0, NULL, NULL);
    }

    /* CN63XX pass 1.x has a bug where the PCIe config CRS counter does not
        stop. Disable reporting errors from CRS */
    if (OCTEON_IS_MODEL(OCTEON_CN63XX_PASS1_X))
    {
        cvmx_error_disable(CVMX_ERROR_REGISTER_IO64, CVMX_PEMX_INT_SUM(0),
            1ull<<12);
        cvmx_error_disable(CVMX_ERROR_REGISTER_IO64, CVMX_PEMX_INT_SUM(0),
            1ull<<13);
        cvmx_error_disable(CVMX_ERROR_REGISTER_IO64, CVMX_PEMX_INT_SUM(1),
            1ull<<12);
        cvmx_error_disable(CVMX_ERROR_REGISTER_IO64, CVMX_PEMX_INT_SUM(1),
            1ull<<13);
    }
    /* According to the workaround for errata SRIO-15282, clearing
       SRIOx_INT_ENABLE[MAC_BUF]. */
    if (OCTEON_IS_MODEL(OCTEON_CN63XX_PASS2_0) && OCTEON_IS_MODEL(OCTEON_CN63XX_PASS2_1))
    {
        cvmx_error_disable(CVMX_ERROR_REGISTER_IO64, CVMX_SRIOX_INT_ENABLE(0), 1ull<<22);
        cvmx_error_disable(CVMX_ERROR_REGISTER_IO64, CVMX_SRIOX_INT_ENABLE(1), 1ull<<22);
    }

    return 0;
}

/**
 * @INTERNAL
 * DFA_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_dfa_err_cp2dbe(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_DFA_ERR, cvmx_read_csr(CVMX_DFA_ERR));
    PRINT_ERROR("DFA_ERR[CP2DBE]: DFA PP-CP2 Double Bit Error Detected\n");
    return 1;
}

/**
 * @INTERNAL
 * DFA_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_dfa_err_cp2perr(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_DFA_ERR, cvmx_read_csr(CVMX_DFA_ERR));
    PRINT_ERROR("DFA_ERR[CP2PERR]: PP-CP2 Parity Error Detected\n");
    return 1;
}

/**
 * @INTERNAL
 * DFA_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_dfa_err_cp2sbe(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_DFA_ERR, cvmx_read_csr(CVMX_DFA_ERR));
    PRINT_ERROR("DFA_ERR[CP2SBE]: DFA PP-CP2 Single Bit Error Corrected\n");
    return 1;
}

/**
 * @INTERNAL
 * DFA_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_dfa_err_dblovf(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_DFA_ERR, cvmx_read_csr(CVMX_DFA_ERR));
    PRINT_ERROR("DFA_ERR[DBLOVF]: Doorbell Overflow detected\n");
    return 1;
}

/**
 * @INTERNAL
 * DFA_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_dfa_err_dtedbe(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_DFA_ERR, cvmx_read_csr(CVMX_DFA_ERR));
    PRINT_ERROR("DFA_ERR[DTEDBE]: DFA DTE 29b Double Bit Error Detected\n");
    return 1;
}

/**
 * @INTERNAL
 * DFA_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_dfa_err_dteperr(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_DFA_ERR, cvmx_read_csr(CVMX_DFA_ERR));
    PRINT_ERROR("DFA_ERR[DTEPERR]: DTE Parity Error Detected\n");
    return 1;
}

/**
 * @INTERNAL
 * DFA_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_dfa_err_dtesbe(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_DFA_ERR, cvmx_read_csr(CVMX_DFA_ERR));
    PRINT_ERROR("DFA_ERR[DTESBE]: DFA DTE 29b Single Bit Error Corrected\n");
    return 1;
}

/**
 * @INTERNAL
 * L2D_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_l2d_err_ded_err(const struct cvmx_error_info *info)
{
    cvmx_l2d_err_t derr;
    cvmx_l2d_fadr_t fadr;
    uint64_t syn0 = cvmx_read_csr(CVMX_L2D_FSYN0);
    uint64_t syn1 = cvmx_read_csr(CVMX_L2D_FSYN1);
    derr.u64 = cvmx_read_csr(CVMX_L2D_ERR);
    fadr.u64 = cvmx_read_csr(CVMX_L2D_FADR);

    PRINT_ERROR("L2D_ERR[DED_ERR] ECC double: fadr: 0x%llx, syn0:0x%llx, syn1: 0x%llx\n",
        (unsigned long long)fadr.u64, (unsigned long long)syn0, (unsigned long long)syn1);
    /* Flush the line that had the error */
    cvmx_l2c_flush_line(fadr.s.fset, fadr.s.fadr >> 1);
    cvmx_write_csr(CVMX_L2D_ERR, derr.u64);
    return 1;
}

/**
 * @INTERNAL
 * L2D_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_l2d_err_sec_err(const struct cvmx_error_info *info)
{
    cvmx_l2d_err_t derr;
    cvmx_l2d_fadr_t fadr;
    uint64_t syn0 = cvmx_read_csr(CVMX_L2D_FSYN0);
    uint64_t syn1 = cvmx_read_csr(CVMX_L2D_FSYN1);
    derr.u64 = cvmx_read_csr(CVMX_L2D_ERR);
    fadr.u64 = cvmx_read_csr(CVMX_L2D_FADR);

    PRINT_ERROR("L2D_ERR[SEC_ERR] ECC single: fadr: 0x%llx, syn0:0x%llx, syn1: 0x%llx\n",
        (unsigned long long)fadr.u64, (unsigned long long)syn0, (unsigned long long)syn1);
    /* Flush the line that had the error */
    cvmx_l2c_flush_line(fadr.s.fset, fadr.s.fadr >> 1);
    cvmx_write_csr(CVMX_L2D_ERR, derr.u64);
    return 1;
}

/**
 * @INTERNAL
 * L2T_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_l2t_err_ded_err(const struct cvmx_error_info *info)
{
    cvmx_l2t_err_t terr;
    terr.u64 = cvmx_read_csr(CVMX_L2T_ERR);
    cvmx_write_csr(CVMX_L2T_ERR, terr.u64);
    PRINT_ERROR("L2T_ERR[DED_ERR]: double bit:\tfadr: 0x%x, fset: 0x%x, fsyn: 0x%x\n",
                     terr.s.fadr, terr.s.fset, terr.s.fsyn);
    if (!terr.s.fsyn)
    {
        /* Syndrome is zero, which means error was in non-hit line,
            so flush all associations */
        int i;
        int l2_assoc = cvmx_l2c_get_num_assoc();

        for (i = 0; i < l2_assoc; i++)
            cvmx_l2c_flush_line(i, terr.s.fadr);
    }
    else
        cvmx_l2c_flush_line(terr.s.fset, terr.s.fadr);
    return 1;
}

/**
 * @INTERNAL
 * L2T_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_l2t_err_lckerr2(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_L2T_ERR, cvmx_read_csr(CVMX_L2T_ERR));
    PRINT_ERROR("L2T_ERR[LCKERR2]: HW detected a case where a Rd/Wr Miss from PP#n could not find an available/unlocked set (for replacement).\n");
    return 1;
}

/**
 * @INTERNAL
 * L2T_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_l2t_err_lckerr(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_L2T_ERR, cvmx_read_csr(CVMX_L2T_ERR));
    PRINT_ERROR("L2T_ERR[LCKERR]: SW attempted to LOCK DOWN the last available set of the INDEX (which is ignored by HW - but reported to SW).\n");
    return 1;
}

/**
 * @INTERNAL
 * L2T_ERR contains R/W1C bits along with R/W bits. This means that it requires
 * special handling instead of the normal __cvmx_error_display() function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_l2t_err_sec_err(const struct cvmx_error_info *info)
{
    cvmx_l2t_err_t terr;
    terr.u64 = cvmx_read_csr(CVMX_L2T_ERR);
    cvmx_write_csr(CVMX_L2T_ERR, terr.u64);
    PRINT_ERROR("L2T_ERR[SEC_ERR]: single bit:\tfadr: 0x%x, fset: 0x%x, fsyn: 0x%x\n",
                 terr.s.fadr, terr.s.fset, terr.s.fsyn);
    if (!terr.s.fsyn)
    {
        /* Syndrome is zero, which means error was in non-hit line,
            so flush all associations */
        int i;
        int l2_assoc = cvmx_l2c_get_num_assoc();

        for (i = 0; i < l2_assoc; i++)
            cvmx_l2c_flush_line(i, terr.s.fadr);
    }
    else
        cvmx_l2c_flush_line(terr.s.fset, terr.s.fadr);
    return 1;
}


/**
 * @INTERNAL
 * LMCX_MEM_CFG0 contains R/W1C bits along with R/W bits. This means that it
 * requires special handling instead of the normal __cvmx_error_display()
 * function.
 *
 * @param info
 *
 * @return
 */

/* PR 7423 assist... */
typedef struct DispRSpec {
	uint64_t    csr;
	char        *csrName;
} DispRSpec;

static DispRSpec _dispL2C_registers[] =
{
    { CVMX_L2C_CFG,                  "CVMX_L2C_CFG"                  },
    { CVMX_L2T_ERR,                  "CVMX_L2T_ERR"                  },
    { CVMX_L2D_ERR,                  "CVMX_L2D_ERR"                  },
    { CVMX_L2D_FADR,                 "CVMX_L2D_FADR"                 },
    { CVMX_L2D_FSYN0,                "CVMX_L2D_FSYN0"                },
    { CVMX_L2D_FSYN1,                "CVMX_L2D_FSYN1"                },
    { CVMX_L2C_DBG,                  "CVMX_L2C_DBG"                  },
    { CVMX_L2C_LFB0,                 "CVMX_L2C_LFB0"                 },
    { CVMX_L2C_LFB1,                 "CVMX_L2C_LFB1"                 },
    { CVMX_L2C_LFB2,                 "CVMX_L2C_LFB2"                 },
    { CVMX_L2C_DUT,                  "CVMX_L2C_DUT"                  },
    { CVMX_L2C_LCKBASE,              "CVMX_L2C_LCKBASE"              },
    { CVMX_L2C_LCKOFF,               "CVMX_L2C_LCKOFF"               },
    { CVMX_L2C_SPAR0,                "CVMX_L2C_SPAR0"                },
    { CVMX_L2C_SPAR1,                "CVMX_L2C_SPAR1"                },
    { CVMX_L2C_SPAR2,                "CVMX_L2C_SPAR2"                },
    { CVMX_L2C_SPAR3,                "CVMX_L2C_SPAR3"                },
    { CVMX_L2C_SPAR4,                "CVMX_L2C_SPAR4"                },
    { CVMX_L2C_PFCTL,                "CVMX_L2C_PFCTL"                },
    { CVMX_L2C_PFC0,                 "CVMX_L2C_PFC0"                 },
    { CVMX_L2C_PFC1,                 "CVMX_L2C_PFC1"                 },
    { CVMX_L2C_PFC2,                 "CVMX_L2C_PFC2"                 },
    { CVMX_L2C_PFC3,                 "CVMX_L2C_PFC3"                 },
    { CVMX_L2C_LFB3,                 "CVMX_L2C_LFB3"                 },
    { CVMX_L2C_PPGRP,                "CVMX_L2C_PPGRP"                },
    { CVMX_L2C_GRPWRR0,              "CVMX_L2C_GRPWRR0"              },
    { CVMX_L2C_GRPWRR1,              "CVMX_L2C_GRPWRR1"              },
    { CVMX_L2C_OOB,                  "CVMX_L2C_OOB"                  },
    { CVMX_L2C_OOB1,                 "CVMX_L2C_OOB1"                 },
    { CVMX_L2C_OOB2,                 "CVMX_L2C_OOB2"                 },
    { CVMX_L2C_OOB3,                 "CVMX_L2C_OOB3"                 },
    { CVMX_L2C_INT_STAT,             "CVMX_L2C_INT_STAT"             },
    { CVMX_L2C_INT_EN,               "CVMX_L2C_INT_EN"               },
    { CVMX_L2D_BST0,                 "CVMX_L2D_BST0"                 },
    { CVMX_L2D_BST1,                 "CVMX_L2D_BST1"                 },
    { CVMX_L2D_BST2,                 "CVMX_L2D_BST2"                 },
    { CVMX_L2D_BST3,                 "CVMX_L2D_BST3"                 },
    { CVMX_L2D_FUS1,                 "CVMX_L2D_FUS1"                 },
    { CVMX_L2D_FUS2,                 "CVMX_L2D_FUS2"                 },
    { CVMX_L2D_FUS3,                 "CVMX_L2D_FUS3"                 },
    { CVMX_L2C_BST2,                 "CVMX_L2C_BST2"                 },
    { CVMX_L2C_BST1,                 "CVMX_L2C_BST1"                 },
    { CVMX_L2C_BST0,                 "CVMX_L2C_BST0"                 }
};

#define MAX_DISPLAY_L2C_REGISTERS (sizeof(_dispL2C_registers) / sizeof(_dispL2C_registers[0]))

static DispRSpec _dispLMC_registers[] =
{
    { CVMX_LMCX_MEM_CFG0(0),         "CVMX_LMCX_MEM_CFG0(0)"         },
    { CVMX_LMCX_MEM_CFG1(0),         "CVMX_LMCX_MEM_CFG1(0)"         },
    { CVMX_LMCX_CTL(0),              "CVMX_LMCX_CTL(0)"              },
    { CVMX_LMCX_DDR2_CTL(0),         "CVMX_LMCX_DDR2_CTL(0)"         },
//    { CVMX_LMCX_FADR(0),             "CVMX_LMCX_FADR(0)"             },
    { CVMX_LMCX_COMP_CTL(0),         "CVMX_LMCX_COMP_CTL(0)"         },
//    { CVMX_LMCX_WODT_CTL0(0),        "CVMX_LMCX_WODT_CTL0(0)"        },
//    { CVMX_LMCX_ECC_SYND(0),         "CVMX_LMCX_ECC_SYND(0)"         },
    { CVMX_LMCX_IFB_CNT_LO(0),       "CVMX_LMCX_IFB_CNT_LO(0)"       },
    { CVMX_LMCX_IFB_CNT_HI(0),       "CVMX_LMCX_IFB_CNT_HI(0)"       },
    { CVMX_LMCX_OPS_CNT_LO(0),       "CVMX_LMCX_OPS_CNT_LO(0)"       },
    { CVMX_LMCX_OPS_CNT_HI(0),       "CVMX_LMCX_OPS_CNT_HI(0)"       },
    { CVMX_LMCX_DCLK_CNT_LO(0),      "CVMX_LMCX_DCLK_CNT_LO(0)"      },
    { CVMX_LMCX_DCLK_CNT_HI(0),      "CVMX_LMCX_DCLK_CNT_HI(0)"      },
    { CVMX_LMCX_WODT_CTL1(0),        "CVMX_LMCX_WODT_CTL1(0)"        },
    { CVMX_LMCX_DELAY_CFG(0),        "CVMX_LMCX_DELAY_CFG(0)"        },
//    { CVMX_LMCX_CTL1(0),             "CVMX_LMCX_CTL1(0)"             },
//    { CVMX_LMCX_DUAL_MEMCFG(0),      "CVMX_LMCX_DUAL_MEMCFG(0)"      },
    { CVMX_LMCX_RODT_COMP_CTL(0),    "CVMX_LMCX_RODT_COMP_CTL(0)"    },
    { CVMX_LMCX_PLL_CTL(0),          "CVMX_LMCX_PLL_CTL(0)"          },
    { CVMX_LMCX_PLL_STATUS(0),       "CVMX_LMCX_PLL_STATUS(0)"       },
//    { CVMX_LMCX_DLL_CTL(0),          "CVMX_LMCX_DLL_CTL(0)"          },
//    { CVMX_LMCX_NXM(0),              "CVMX_LMCX_NXM(0)"              },
//    { CVMX_LMCX_BIST_CTL(0),         "CVMX_LMCX_BIST_CTL(0)"         },
    { CVMX_LMCX_BIST_RESULT(0),      "CVMX_LMCX_BIST_RESULT(0)"      },
    { CVMX_LMCX_READ_LEVEL_RANKX(0,0),"CVMX_LMCX_READ_LEVEL_RANKX(0)" },
    { CVMX_LMCX_READ_LEVEL_RANKX(1,0),"CVMX_LMCX_READ_LEVEL_RANKX(1)" },
    { CVMX_LMCX_READ_LEVEL_RANKX(2,0),"CVMX_LMCX_READ_LEVEL_RANKX(2)" },
    { CVMX_LMCX_READ_LEVEL_RANKX(3,0),"CVMX_LMCX_READ_LEVEL_RANKX(3)" },
    { CVMX_LMCX_READ_LEVEL_CTL(0),   "CVMX_LMCX_READ_LEVEL_CTL(0)"   },
    { CVMX_LMCX_READ_LEVEL_DBG(0),   "CVMX_LMCX_READ_LEVEL_DBG(0)"   }
};

#define MAX_DISPLAY_LMC_REGISTERS (sizeof(_dispLMC_registers) / sizeof(_dispLMC_registers[0]))


typedef union /* Xirrus' version of the cvmx-trace-buf entry union - Big End and 52xx formats only... */
{
    struct
    {
        uint64_t  datahi;
        uint64_t  data;
    } u128 __attribute__((packed));

    struct
    {
        uint64_t    reserved3   : 64;
        uint64_t    valid       : 1;
        uint64_t    discontinuity:1;
        uint64_t    address     : 36;
        uint64_t    reserved    : 5;
        uint64_t    source      : 5;
        uint64_t    reserved2   : 3;
        uint64_t    type        : 5;
        uint64_t    timestamp   : 8;
    } cmn __attribute__((packed)); /* DWB, PL2, PSL1, LDD, LDI, LDT */
    struct
    {
        uint64_t    reserved3   : 64;
        uint64_t    valid       : 1;
        uint64_t    discontinuity:1;
        uint64_t    address     : 33;
        uint64_t    mask        : 8;
        uint64_t    source      : 5;
        uint64_t    reserved2   : 3;
        uint64_t    type        : 5;
        uint64_t    timestamp   : 8;
    } store __attribute__((packed)); /* STC, STF, STP, STT, SAA */
    struct
    {
        uint64_t    reserved3   : 64;
        uint64_t    valid       : 1;
        uint64_t    discontinuity:1;
        uint64_t    address     : 36;
        uint64_t    reserved    : 2;
        uint64_t    subid       : 3;
        uint64_t    source      : 4;
        uint64_t    dest        : 5;
        uint64_t    type        : 4;
        uint64_t    timestamp   : 8;
    } iobld __attribute__((packed)); /* IOBLD8, IOBLD16, IOBLD32, IOBLD64, IOBST */
    struct
    {
        uint64_t    reserved3   : 64;
        uint64_t    valid       : 1;
        uint64_t    discontinuity:1;
        uint64_t    address     : 33;
        uint64_t    mask        : 8;
        uint64_t    source      : 4;
        uint64_t    dest        : 5;
        uint64_t    type        : 4;
        uint64_t    timestamp   : 8;
    } iob __attribute__((packed)); /* IOBDMA */
} xcvmx_tra_data_t;

static const char *xtype[] = {
    "dwb",
    "pl2",
    "psl1",
    "ldd",
    "ldi",
    "ldt",
    "stf",
    "stc",
    "stp",
    "stt",
    "iobld8",
    "iobld16",
    "iobld32",
    "iobld64",
    "iobst",
    "iobdma",
    "saa",
    "rsvd17",
    "rsvd18",
    "rsvd19",
    "rsvd20",
    "rsvd21",
    "rsvd22",
    "rsvd23",
    "rsvd24",
    "rsvd25",
    "rsvd26",
    "rsvd27",
    "rsvd28",
    "rsvd29",
    "rsvd30",
    "rsvd31"
};

static const char *xsrc[] = {
    "core0",
    "core1",
    "core2",
    "core3",
    "core4",
    "core5",
    "core6",
    "core7",
    "core8",
    "core9",
    "core10",
    "core11",
    "core12",
    "core13",
    "core14",
    "core15",
    "pip/ipd",
    "pko/r",
#define FPA_ETC_IDX  18
    "  (*)  ",
    "dwb",
    "rsvd20",
    "rsvd21",
    "rsvd22",
    "rsvd23",
    "rsvd24",
    "rsvd25",
    "rsvd26",
    "rsvd27",
    "rsvd28",
    "rsvd29",
    "rsvd30",
    "rsvd31"
};

static const char *xdest[] = {
    "ciu/gpio",
    "rsvd1",
    "rsvd2",
    "pcie/sli",
    "key",
    "fpa",
    "dfa",
    "zip",
    "rng",
    "ipd",
    "pko",
    "rsvd11",
    "pow",
    "usb0",
    "rad",
    "rsvd15",
    "rsvd16",
    "rsvd17",
    "rsvd18",
    "rsvd19",
    "rsvd20",
    "rsvd21",
    "rsvd22",
    "rsvd23",
    "rsvd24",
    "rsvd25",
    "rsvd26",
    "dpi",
    "rsvd28",
    "rsvd29",
    "fau",
    "rsvd31"
};

//this is fbo the madwifi driver, which will pass status up to the health monitor -dbr
int cmb_error_occurred = 0;
EXPORT_SYMBOL(cmb_error_occurred);

void xcvmx_tra_display(void) /* Xirrus version of cvmx_tra_display */
{
    cvmx_tra_ctl_t    tra_ctl;
    xcvmx_tra_data_t  data;
    int               entry = 0,
                      first_inval=-1;
    const char       *fpa_etc = " *=Dest:fpa/tim/dfa/pci/zip/pow/pko-w";
#define MAX_MARKER    32
    unsigned          marker[MAX_MARKER];
    int               markerIdx = 0;

    //dump_stack();
    printk(KERN_ALERT "Cavium CMB Trace Buffer\n");
    printk(KERN_ALERT "=======================\n");
    printk(KERN_ALERT "Index   Type       Address       Timestamp  Source    Dest   Dis               Data\n");
    printk(KERN_ALERT "----- -------- ---------------- ---------- -------- -------- --- --------------------------------\n");

    /* we have up to 1024 of them. Depending on the trace mode, some or all may be valid */
    tra_ctl.u64 = cvmx_read_csr(CVMX_TRA_CTL);
    for(entry=0; entry<1024; entry++)
    {
        data.u128.data = cvmx_read_csr( (uint64_t) CVMX_TRA_READ_DAT);
        data.u128.datahi = 0;
        if (data.cmn.valid)
        {
            /* The type is a five bit field for some entries and 4 for other. The four
               bit entries can be mis-typed if the top is set */
            int type = data.cmn.type;
            if (type >= 0x1a)
                type &= 0xf;

            if(first_inval != -1)
            {
                /* we had a gap of invalid entries - dump the gap here... */
                if(first_inval == entry -1)
                    printk(KERN_ALERT "%4d) Unfilled entry\n", entry-1);
                else
                    printk(KERN_ALERT "%4d-%d) Unfilled entries\n", first_inval, entry-1);
                first_inval = -1; /* reset it */
            }

            /* The common part of every entry... */
            printk(KERN_ALERT "%4d) %8s ", entry, xtype[type]);

            switch (type)
            {
                case 0 ... 5:  /* dwb,pl2,psl1,ldd,ldi,ldt */
                    printk(KERN_ALERT "%016llx       %+4d %8s %8s   %c %016llx\n",
                        (unsigned long long)data.cmn.address,
                        data.cmn.timestamp << (tra_ctl.s.time_grn*3),
                        xsrc[data.cmn.source],
                        "", /* no dest on this transaction type */
                        (data.cmn.discontinuity) ? 'D' : ' ',
                        (unsigned long long)data.u128.data);
                    break;
                case 6 ... 9:  /* stf,stc,stp,stt */
                case 16:     /* saa */
                    printk(KERN_ALERT "%016llx       %+4d %8s %8s   %c %016llx (mask:%02x)\n",
                        (unsigned long long)data.store.address << 3,
                        data.cmn.timestamp << (tra_ctl.s.time_grn*3),
                        xsrc[data.store.source],
                        "", /* no dest on this transaction type */
                        (data.cmn.discontinuity) ? 'D' : ' ',
                        (unsigned long long)data.u128.data,
                        (unsigned int)data.store.mask);
                    break;
                case 10 ... 14:  /* iobld8,16,32,64, iobst */
                    printk(KERN_ALERT "%016llx       %+4d %8s %8s   %c %016llx (subdid:%x)%s\n",
                        (unsigned long long)data.iobld.address,
                        data.cmn.timestamp << (tra_ctl.s.time_grn*3),
                        xsrc[data.iobld.source],
                        xdest[data.iobld.dest],
                        (data.cmn.discontinuity) ? 'D' : ' ',
                        (unsigned long long)data.u128.data,
                        (unsigned int)data.iobld.subid,
                        data.iobld.dest == FPA_ETC_IDX ?  fpa_etc : "");
                    if( (data.iobld.address == 0xa8000000) && (data.iobld.source < 4))
                    {
                        if(markerIdx < MAX_MARKER)
                            marker[markerIdx++] = entry;
                    }
                    break;
                case 15:  /* iobdma */
                    printk(KERN_ALERT "%016llx       %+4d %8s %8s   %c %016llx (len:%x)%s\n",
                        (unsigned long long)data.iob.address << 3,
                        data.cmn.timestamp << (tra_ctl.s.time_grn*3),
                        xsrc[data.iob.source],
                        xdest[data.iob.dest],
                        (data.cmn.discontinuity) ? 'D' : ' ',
                        (unsigned long long)data.u128.data,
                        (unsigned int)data.iob.mask,
                        data.iob.dest == FPA_ETC_IDX ?  fpa_etc : "");
                    break;
                default:
                    printk(KERN_ALERT "UNKNOWN FORMAT         %+4d                     %c %016llx\n",
                        data.cmn.timestamp << (tra_ctl.s.time_grn*3),
                        (data.cmn.discontinuity) ? 'D' : ' ',
                        (unsigned long long)data.u128.data);
                    break;
            }
        }
        else
        {
            /* we have an invalid entry */
            if(first_inval==-1)
                first_inval = entry;
        }
    }
    if(first_inval != -1) /* no outstanding invalid entries seen - we're done. */
    {
        /* wrap up the residual invalid entries... */
        printk(KERN_ALERT "%d..1023) - Unfilled entries\n", first_inval);
    }
    if(markerIdx)
    {
        register int i;
        printk(KERN_ALERT "\nPotential trace termination entries are:\n");
        for(i=0;i<MAX_MARKER && i < markerIdx;i++)
            printk(KERN_ALERT "%d ", marker[i]);

    }
    //this is fbo the madwifi driver, which will pass status up to the health monitor -dbr
    ++cmb_error_occurred;
}

static int __cvmx_error_handle_lmcx_mem_cfg0(const struct cvmx_error_info *info)
{
    int ddr_controller = info->group_index;
    cvmx_lmcx_mem_cfg0_t mem_cfg0;
    cvmx_lmcx_fadr_t fadr;
    int sec_err;
    int ded_err;

    mem_cfg0.u64 = cvmx_read_csr(CVMX_LMCX_MEM_CFG0(ddr_controller));
    fadr.u64 = cvmx_read_csr(CVMX_LMCX_FADR(ddr_controller));
    cvmx_write_csr(CVMX_LMCX_MEM_CFG0(ddr_controller),mem_cfg0.u64);

    sec_err = cvmx_dpop(mem_cfg0.s.sec_err);
    ded_err = cvmx_dpop(mem_cfg0.s.ded_err);

    if (ded_err || sec_err)
    {
        register int           reg;
        PRINT_ERROR("DDR%d ECC: %d Single bit corrections, %d Double bit errors\n"
                     "DDR%d ECC:\tFailing dimm:   %u\n"
                     "DDR%d ECC:\tFailing rank:   %u\n"
                     "DDR%d ECC:\tFailing bank:   %u\n"
                     "DDR%d ECC:\tFailing row:    0x%x\n"
                     "DDR%d ECC:\tFailing column: 0x%x\n",
                     ddr_controller, sec_err, ded_err,
                     ddr_controller, fadr.cn38xx.fdimm,
                     ddr_controller, fadr.cn38xx.fbunk,
                     ddr_controller, fadr.cn38xx.fbank,
                     ddr_controller, fadr.cn38xx.frow,
                     ddr_controller, fadr.cn38xx.fcol);

        for(reg=0;reg< MAX_DISPLAY_L2C_REGISTERS; reg++)
        {
	    register uint64_t rv = cvmx_read_csr( _dispL2C_registers[reg].csr );
	    PRINT_ERROR("L2C Register %s (0x%llx) = 0x%llx\n",
		    _dispL2C_registers[reg].csrName, _dispL2C_registers[reg].csr, rv);
        }

        for(reg=0;reg< MAX_DISPLAY_LMC_REGISTERS; reg++)
        {
	    register uint64_t rv = cvmx_read_csr( _dispLMC_registers[reg].csr );
	    PRINT_ERROR("LMC Register %s (0x%llx) = 0x%llx\n",
		    _dispLMC_registers[reg].csrName, _dispLMC_registers[reg].csr, rv);
        }
    }
    return 1;
}

/**
 * @INTERNAL
 * LMCX_MEM_CFG0 contains R/W1C bits along with R/W bits. This means that it
 * requires special handling instead of the normal __cvmx_error_display()
 * function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_lmcx_mem_cfg0_ded_err(const struct cvmx_error_info *info)
{
    return __cvmx_error_handle_lmcx_mem_cfg0(info);
}

/**
 * @INTERNAL
 * LMCX_MEM_CFG0 contains R/W1C bits along with R/W bits. This means that it
 * requires special handling instead of the normal __cvmx_error_display()
 * function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_lmcx_mem_cfg0_sec_err(const struct cvmx_error_info *info)
{
    return __cvmx_error_handle_lmcx_mem_cfg0(info);
}

/**
 * @INTERNAL
 * POW_ECC_ERR contains R/W1C bits along with R/W bits. This means that it
 * requires special handling instead of the normal __cvmx_error_display()
 * function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_pow_ecc_err_dbe(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_POW_ECC_ERR, cvmx_read_csr(CVMX_POW_ECC_ERR));
    PRINT_ERROR("POW_ECC_ERR[DBE]: POW double bit error\n");
    return 1;
}

/**
 * @INTERNAL
 * POW_ECC_ERR contains R/W1C bits along with R/W bits. This means that it
 * requires special handling instead of the normal __cvmx_error_display()
 * function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_pow_ecc_err_iop(const struct cvmx_error_info *info)
{
    cvmx_pow_ecc_err_t err;
    err.u64 = cvmx_read_csr(CVMX_POW_ECC_ERR);
    cvmx_write_csr(CVMX_POW_ECC_ERR, err.u64);
    if (err.s.iop & (1 << 0))
        PRINT_ERROR("POW_ECC_ERR[IOP0]: Received SWTAG/SWTAG_FULL/SWTAG_DESCH/DESCH/UPD_WQP from PP in NULL_NULL state\n");
    if (err.s.iop & (1 << 1))
        PRINT_ERROR("POW_ECC_ERR[IOP1]: Received SWTAG/SWTAG_DESCH/DESCH/UPD_WQP from PP in NULL state\n");
    if (err.s.iop & (1 << 2))
        PRINT_ERROR("POW_ECC_ERR[IOP2]: Received SWTAG/SWTAG_FULL/SWTAG_DESCH/GET_WORK from PP with pending tag switch to ORDERED or ATOMIC\n");
    if (err.s.iop & (1 << 3))
        PRINT_ERROR("POW_ECC_ERR[IOP3]: Received SWTAG/SWTAG_FULL/SWTAG_DESCH from PP with tag specified as NULL_NULL\n");
    if (err.s.iop & (1 << 4))
        PRINT_ERROR("POW_ECC_ERR[IOP4]: Received SWTAG_FULL/SWTAG_DESCH from PP with tag specified as NULL\n");
    if (err.s.iop & (1 << 5))
        PRINT_ERROR("POW_ECC_ERR[IOP5]: Received SWTAG/SWTAG_FULL/SWTAG_DESCH/DESCH/UPD_WQP/GET_WORK/NULL_RD from PP with GET_WORK pending\n");
    if (err.s.iop & (1 << 6))
        PRINT_ERROR("POW_ECC_ERR[IOP6]: Received SWTAG/SWTAG_FULL/SWTAG_DESCH/DESCH/UPD_WQP/GET_WORK/NULL_RD from PP with NULL_RD pending\n");
    if (err.s.iop & (1 << 7))
        PRINT_ERROR("POW_ECC_ERR[IOP7]: Received CLR_NSCHED from PP with SWTAG_DESCH/DESCH/CLR_NSCHED pending\n");
    if (err.s.iop & (1 << 8))
        PRINT_ERROR("POW_ECC_ERR[IOP8]: Received SWTAG/SWTAG_FULL/SWTAG_DESCH/DESCH/UPD_WQP/GET_WORK/NULL_RD from PP with CLR_NSCHED pending\n");
    if (err.s.iop & (1 << 9))
        PRINT_ERROR("POW_ECC_ERR[IOP9]: Received illegal opcode\n");
    if (err.s.iop & (1 << 10))
        PRINT_ERROR("POW_ECC_ERR[IOP10]: Received ADD_WORK with tag specified as NULL_NULL\n");
    if (err.s.iop & (1 << 11))
        PRINT_ERROR("POW_ECC_ERR[IOP11]: Received DBG load from PP with DBG load pending\n");
    if (err.s.iop & (1 << 12))
        PRINT_ERROR("POW_ECC_ERR[IOP12]: Received CSR load from PP with CSR load pending\n");
    return 1;
}

/**
 * @INTERNAL
 * POW_ECC_ERR contains R/W1C bits along with R/W bits. This means that it
 * requires special handling instead of the normal __cvmx_error_display()
 * function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_pow_ecc_err_rpe(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_POW_ECC_ERR, cvmx_read_csr(CVMX_POW_ECC_ERR));
    PRINT_ERROR("POW_ECC_ERR[RPE]: Remote pointer error\n");
    return 1;
}

/**
 * @INTERNAL
 * POW_ECC_ERR contains R/W1C bits along with R/W bits. This means that it
 * requires special handling instead of the normal __cvmx_error_display()
 * function.
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_pow_ecc_err_sbe(const struct cvmx_error_info *info)
{
    cvmx_write_csr(CVMX_POW_ECC_ERR, cvmx_read_csr(CVMX_POW_ECC_ERR));
    PRINT_ERROR("POW_ECC_ERR[SBE]: POW single bit error\n");
    return 1;
}


/**
 * @INTERNAL
 *
 * @param info
 *
 * @return
 */
int __cvmx_error_handle_63XX_l2_ecc(const struct cvmx_error_info *info)
{
    cvmx_l2c_err_tdtx_t l2c_err_tdt;
    cvmx_l2c_err_ttgx_t l2c_err_ttg;
    cvmx_l2c_err_vbfx_t l2c_err_vbf;
    cvmx_l2c_tadx_int_t tadx_int;
    tadx_int.u64 = cvmx_read_csr(CVMX_L2C_TADX_INT(0));
    l2c_err_tdt.u64 = cvmx_read_csr(CVMX_L2C_ERR_TDTX(0));
    l2c_err_ttg.u64 = cvmx_read_csr(CVMX_L2C_ERR_TTGX(0));
    l2c_err_vbf.u64 = cvmx_read_csr(CVMX_L2C_ERR_VBFX(0));
    cvmx_write_csr(CVMX_L2C_TADX_INT(0), tadx_int.u64);

    if (tadx_int.cn63xx.l2ddbe ||  tadx_int.cn63xx.l2dsbe)
    {
	/* L2 Data error */


	if (tadx_int.cn63xx.l2dsbe)
        {
            /* l2c_err_tdt.cn63xx.wayidx formated same as CACHE instruction arg */
            CVMX_CACHE_WBIL2I((l2c_err_tdt.u64 & 0x1fff80) | (1ULL << 63), 0);
            CVMX_SYNC;
            PRINT_ERROR("L2C_TADX_INT(0)[L2DSBE]: Data Single-Bit Error\n");
        }
        if (tadx_int.cn63xx.l2ddbe)
        {
            /* TODO - fatal error, for now, flush so error cleared..... */
            CVMX_CACHE_WBIL2I((l2c_err_tdt.u64 & 0x1fff80) | (1ULL << 63), 0);
            CVMX_SYNC;
	    PRINT_ERROR("L2C_TADX_INT(0)[L2DDBE]: Data Double-Bit Error\n");
        }
	PRINT_ERROR("CVMX_L2C_ERR_TDT: 0x%llx\n", (unsigned long long)l2c_err_tdt.u64);
    }
    if (tadx_int.cn63xx.tagdbe || tadx_int.cn63xx.tagsbe)
    {
	/* L2 Tag error */
	if (tadx_int.cn63xx.tagsbe)
        {
            CVMX_CACHE_WBIL2I((l2c_err_ttg.u64 & 0x1fff80) | (1ULL << 63), 0);
            CVMX_SYNC;
	    PRINT_ERROR("L2C_TADX_INT(0)[TAGSBE]: Tag Single-Bit Error\n");
        }
	if (tadx_int.cn63xx.tagdbe)
        {
            /* TODO - fatal error, for now, flush so error cleared..... */
            CVMX_CACHE_WBIL2I((l2c_err_ttg.u64 & 0x1fff80) | (1ULL << 63), 0);
            CVMX_SYNC;
	    PRINT_ERROR("L2C_TADX_INT(0)[TAGDBE]: Tag Double-Bit Error\n");
        }
	PRINT_ERROR("CVMX_L2C_ERR_TTG: 0x%llx\n", (unsigned long long)l2c_err_ttg.u64);
    }
    if (tadx_int.cn63xx.vbfdbe || tadx_int.cn63xx.vbfsbe)
    {
	/* L2 Victim buffer error */
	if (tadx_int.cn63xx.vbfsbe)
        {
            /* No action here, hardware fixed up on write to DRAM */
	    PRINT_ERROR("L2C_TADX_INT(0)[VBFSBE]: VBF Single-Bit Error\n");
        }
	if (tadx_int.cn63xx.vbfdbe)
        {
            /* TODO - fatal error.  Bad data written to DRAM. */
	    PRINT_ERROR("L2C_TADX_INT(0)[VBFDBE]: VBF Double-Bit Error\n");
        }
	PRINT_ERROR("CVMX_L2C_ERR_VBF: 0x%llx\n", (unsigned long long)l2c_err_vbf.u64);
    }

    return 1;
}

static DispRSpec _dispPESC_registers[] = {
    { (uint64_t) 0x00011f0000008530ull, "NPEI_INT_SUM"},
    { (uint64_t) 0x00011f0000008550ull, "NPEI_INT_A_SUM"},

    { (uint64_t) 0x00011800c8000000ull, "PESC0_CTL_STATUS"},
    { (uint64_t) 0x00011800c8000008ull, "PESC0_DBG_INFO"},
    { (uint64_t) 0x00011800c8000018ull, "PESC0_BIST_STATUS"},
    { (uint64_t) 0x00011800c8000020ull, "PESC0_DIAG_STATUS"},
#if 0
    /* Staging registers shouldn't be dumped - just used... */
    { (uint64_t) 0x00011800c8000028ull, "PESC0_CFG_WR"},
    { (uint64_t) 0x00011800c8000030ull, "PESC0_CFG_RD"},
#endif
    { (uint64_t) 0x00011800c8000038ull, "PESC0_TLP_CREDITS"},
    { (uint64_t) 0x00011800c8000040ull, "PESC0_P2P_BAR000_START"},
    { (uint64_t) 0x00011800c8000048ull, "PESC0_P2P_BAR000_END"},
    { (uint64_t) 0x00011800c8000050ull, "PESC0_P2P_BAR001_START"},
    { (uint64_t) 0x00011800c8000058ull, "PESC0_P2P_BAR001_END"},
    { (uint64_t) 0x00011800c8000060ull, "PESC0_P2P_BAR002_START"},
    { (uint64_t) 0x00011800c8000068ull, "PESC0_P2P_BAR002_END"},
    { (uint64_t) 0x00011800c8000070ull, "PESC0_P2P_BAR003_START"},
    { (uint64_t) 0x00011800c8000078ull, "PESC0_P2P_BAR003_END"},
    { (uint64_t) 0x00011800c8000080ull, "PESC0_P2N_BAR0_START"},
    { (uint64_t) 0x00011800c8000088ull, "PESC0_P2N_BAR1_START"},
    { (uint64_t) 0x00011800c8000090ull, "PESC0_P2N_BAR2_START"},
    { (uint64_t) 0x00011800c8000098ull, "PESC0_CPL_LUT_VALID"},
    { (uint64_t) 0x00011800c80000a0ull, "PESC0_DBG_INFO_EN"},
    { (uint64_t) 0x00011800c8000400ull, "PESC0_CTL_STATUS2"},
    { (uint64_t) 0x00011800c8000418ull, "PESC0_BIST_STATUS2"},

    { (uint64_t) 0x00011800d0000000ull, "PESC1_CTL_STATUS"},
    { (uint64_t) 0x00011800d0000008ull, "PESC1_DBG_INFO"},
    { (uint64_t) 0x00011800d0000018ull, "PESC1_BIST_STATUS"},
    { (uint64_t) 0x00011800d0000020ull, "PESC1_DIAG_STATUS"},
#if 0
    /* Staging registers shouldn't be dumped - just used... */
    { (uint64_t) 0x00011800d0000028ull, "PESC1_CFG_WR"},
    { (uint64_t) 0x00011800d0000030ull, "PESC1_CFG_RD"},
#endif
    { (uint64_t) 0x00011800d0000038ull, "PESC1_TLP_CREDITS"},
    { (uint64_t) 0x00011800d0000040ull, "PESC1_P2P_BAR000_START"},
    { (uint64_t) 0x00011800d0000048ull, "PESC1_P2P_BAR000_END"},
    { (uint64_t) 0x00011800d0000050ull, "PESC1_P2P_BAR001_START"},
    { (uint64_t) 0x00011800d0000058ull, "PESC1_P2P_BAR001_END"},
    { (uint64_t) 0x00011800d0000060ull, "PESC1_P2P_BAR002_START"},
    { (uint64_t) 0x00011800d0000068ull, "PESC1_P2P_BAR002_END"},
    { (uint64_t) 0x00011800d0000070ull, "PESC1_P2P_BAR003_START"},
    { (uint64_t) 0x00011800d0000078ull, "PESC1_P2P_BAR003_END"},
    { (uint64_t) 0x00011800d0000080ull, "PESC1_P2N_BAR0_START"},
    { (uint64_t) 0x00011800d0000088ull, "PESC1_P2N_BAR1_START"},
    { (uint64_t) 0x00011800d0000090ull, "PESC1_P2N_BAR2_START"},
    { (uint64_t) 0x00011800d0000098ull, "PESC1_CPL_LUT_VALID"},
    { (uint64_t) 0x00011800d00000a0ull, "PESC1_DBG_INFO_EN"},
    { (uint64_t) 0x00011800d0000400ull, "PESC1_CTL_STATUS2"},
    { (uint64_t) 0x00011800d0000418ull, "PESC1_BIST_STATUS2"}
};
#define MAX_DISPLAY_PESC_REGISTERS (sizeof( _dispPESC_registers ) / sizeof( _dispPESC_registers[0]))


static DispRSpec _dispPCIRC_registers[] = {
    { (uint64_t) 0x0000ull, "PCIERC*_CFG000"},
    { (uint64_t) 0x0004ull, "PCIERC*_CFG001"},
    { (uint64_t) 0x0008ull, "PCIERC*_CFG002"},
    { (uint64_t) 0x000cull, "PCIERC*_CFG003"},
    { (uint64_t) 0x0010ull, "PCIERC*_CFG004"},
    { (uint64_t) 0x0014ull, "PCIERC*_CFG005"},
    { (uint64_t) 0x0018ull, "PCIERC*_CFG006"},
    { (uint64_t) 0x001cull, "PCIERC*_CFG007"},
    { (uint64_t) 0x0020ull, "PCIERC*_CFG008"},
    { (uint64_t) 0x0024ull, "PCIERC*_CFG009"},
    { (uint64_t) 0x0028ull, "PCIERC*_CFG010"},
    { (uint64_t) 0x002cull, "PCIERC*_CFG011"},
    { (uint64_t) 0x0030ull, "PCIERC*_CFG012"},
    { (uint64_t) 0x0034ull, "PCIERC*_CFG013"},
    { (uint64_t) 0x0038ull, "PCIERC*_CFG014"},
    { (uint64_t) 0x003cull, "PCIERC*_CFG015"},
    { (uint64_t) 0x0040ull, "PCIERC*_CFG016"},
    { (uint64_t) 0x0044ull, "PCIERC*_CFG017"},
    { (uint64_t) 0x0050ull, "PCIERC*_CFG020"},
    { (uint64_t) 0x0054ull, "PCIERC*_CFG021"},
    { (uint64_t) 0x0058ull, "PCIERC*_CFG022"},
    { (uint64_t) 0x005cull, "PCIERC*_CFG023"},
    { (uint64_t) 0x0070ull, "PCIERC*_CFG028"},
    { (uint64_t) 0x0074ull, "PCIERC*_CFG029"},
    { (uint64_t) 0x0078ull, "PCIERC*_CFG030"},
    { (uint64_t) 0x007cull, "PCIERC*_CFG031"},
    { (uint64_t) 0x0080ull, "PCIERC*_CFG032"},
    { (uint64_t) 0x0084ull, "PCIERC*_CFG033"},
    { (uint64_t) 0x0088ull, "PCIERC*_CFG034"},
    { (uint64_t) 0x008cull, "PCIERC*_CFG035"},
    { (uint64_t) 0x0090ull, "PCIERC*_CFG036"},
    { (uint64_t) 0x0094ull, "PCIERC*_CFG037"},
    { (uint64_t) 0x0098ull, "PCIERC*_CFG038"},
    { (uint64_t) 0x009cull, "PCIERC*_CFG039"},
    { (uint64_t) 0x00a0ull, "PCIERC*_CFG040"},
    { (uint64_t) 0x00a4ull, "PCIERC*_CFG041"},
    { (uint64_t) 0x00a8ull, "PCIERC*_CFG042"},
    { (uint64_t) 0x0100ull, "PCIERC*_CFG064"},
    { (uint64_t) 0x0104ull, "PCIERC*_CFG065"},
    { (uint64_t) 0x0108ull, "PCIERC*_CFG066"},
    { (uint64_t) 0x010cull, "PCIERC*_CFG067"},
    { (uint64_t) 0x0110ull, "PCIERC*_CFG068"},
    { (uint64_t) 0x0114ull, "PCIERC*_CFG069"},
    { (uint64_t) 0x0118ull, "PCIERC*_CFG070"},
    { (uint64_t) 0x011cull, "PCIERC*_CFG071"},
    { (uint64_t) 0x0120ull, "PCIERC*_CFG072"},
    { (uint64_t) 0x0124ull, "PCIERC*_CFG073"},
    { (uint64_t) 0x0128ull, "PCIERC*_CFG074"},
    { (uint64_t) 0x012cull, "PCIERC*_CFG075"},
    { (uint64_t) 0x0130ull, "PCIERC*_CFG076"},
    { (uint64_t) 0x0134ull, "PCIERC*_CFG077"},
    { (uint64_t) 0x0700ull, "PCIERC*_CFG448"},
    { (uint64_t) 0x0704ull, "PCIERC*_CFG449"},
    { (uint64_t) 0x0708ull, "PCIERC*_CFG450"},
    { (uint64_t) 0x070cull, "PCIERC*_CFG451"},
    { (uint64_t) 0x0710ull, "PCIERC*_CFG452"},
    { (uint64_t) 0x0714ull, "PCIERC*_CFG453"},
    { (uint64_t) 0x0718ull, "PCIERC*_CFG454"},
    { (uint64_t) 0x071cull, "PCIERC*_CFG455"},
    { (uint64_t) 0x0720ull, "PCIERC*_CFG456"},
    { (uint64_t) 0x0728ull, "PCIERC*_CFG458"},
    { (uint64_t) 0x072cull, "PCIERC*_CFG459"},
    { (uint64_t) 0x0730ull, "PCIERC*_CFG460"},
    { (uint64_t) 0x0734ull, "PCIERC*_CFG461"},
    { (uint64_t) 0x0738ull, "PCIERC*_CFG462"},
    { (uint64_t) 0x073cull, "PCIERC*_CFG463"},
    { (uint64_t) 0x0740ull, "PCIERC*_CFG464"},
    { (uint64_t) 0x0744ull, "PCIERC*_CFG465"},
    { (uint64_t) 0x0748ull, "PCIERC*_CFG466"},
    { (uint64_t) 0x074cull, "PCIERC*_CFG467"},
    { (uint64_t) 0x0750ull, "PCIERC*_CFG468"},
    { (uint64_t) 0x07a8ull, "PCIERC*_CFG490"},
    { (uint64_t) 0x07acull, "PCIERC*_CFG491"},
    { (uint64_t) 0x07b0ull, "PCIERC*_CFG492"},
    { (uint64_t) 0x0810ull, "PCIERC*_CFG516"},
    { (uint64_t) 0x0814ull, "PCIERC*_CFG517"}
};

#define MAX_DISPLAY_PCIRC_REGISTERS (sizeof( _dispPCIRC_registers ) / sizeof( _dispPCIRC_registers[0]))





int xcvmx_pci_display(void)
{
    register int           reg;
    register uint64_t      cfg;
    register uint64_t      rv;

    printk(KERN_ERR "NPEI/PESC Register Set:\n"
                "=======================\n");
    msleep(100);
    for(reg = 0;reg < MAX_DISPLAY_PESC_REGISTERS; reg++)
    {
        rv = cvmx_read_csr( CVMX_ADD_IO_SEG(_dispPESC_registers[reg].csr) );
        printk(KERN_ERR "%s (0x%llx) = 0x%llx\n",
            _dispPESC_registers[reg].csrName, _dispPESC_registers[reg].csr, rv);
    }
    msleep(1000);


    printk(KERN_ERR "RC Mode PCIe Register Set:\n"
                "==========================\n");
    msleep(100);
    /* Stage the PCIERC0_CFGXXX registers through PESC0_CFG_RD */
#define PESC0_CFG_RD ((uint64_t) 0x11800c8000030ull)
    /* Stage the PCIERC1_CFGXXX registers through PESC1_CFG_RD */
#define PESC1_CFG_RD ((uint64_t) 0x11800d0000030ull)

    cfg = PESC0_CFG_RD;
    printk(KERN_ERR "RC 0:\n");
    for(reg = 0;reg < MAX_DISPLAY_PCIRC_REGISTERS; reg++)
    {
        rv = _dispPCIRC_registers[reg].csr;
        cvmx_write_csr(CVMX_ADD_IO_SEG(PESC0_CFG_RD), rv); /* writing this starts a read op */

        /* now read the data */
        rv = cvmx_read_csr( CVMX_ADD_IO_SEG(PESC0_CFG_RD) );

        rv = (rv >> 32) & 0xfffffff;
        printk(KERN_ERR "%s (0x%llx) = 0x%08x\n",
            _dispPESC_registers[reg].csrName,
            _dispPESC_registers[reg].csr,
            (unsigned) rv);
    }
    msleep(1000);

    cfg = PESC1_CFG_RD;
    printk(KERN_ERR "RC 1:\n");
    msleep(100);
    for(reg = 0;reg < MAX_DISPLAY_PCIRC_REGISTERS; reg++)
    {
        rv = _dispPCIRC_registers[reg].csr;
        cvmx_write_csr(CVMX_ADD_IO_SEG(PESC1_CFG_RD), rv); /* writing this starts a read op */

        /* now read the data */
        rv = cvmx_read_csr( CVMX_ADD_IO_SEG(PESC1_CFG_RD) );

        rv = (rv >> 32) & 0xfffffff;
        printk("%s (0x%llx) = 0x%08x\n",
            _dispPESC_registers[reg].csrName,
            _dispPESC_registers[reg].csr,
            (unsigned) rv);
    }
    return(0);
}
EXPORT_SYMBOL( xcvmx_pci_display );
