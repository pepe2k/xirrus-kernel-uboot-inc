/*
 * Rusty Russell (C)2000 -- This code is GPL.
 * Patrick McHardy (c) 2006-2012
 */

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/skbuff.h>
#include <linux/netfilter.h>
#include <linux/seq_file.h>
#include <linux/rcupdate.h>
#include <net/protocol.h>
#include <net/netfilter/nf_queue.h>
#include <net/dst.h>
#ifdef CONFIG_XIRRUS
#include <linux/netfilter/nfnetlink.h>
#include <linux/netfilter/nfnetlink_queue.h>
#endif
#include "nf_internals.h"

/*
 * Hook for nfnetlink_queue to register its queue handler.
 * We do this so that most of the NFQUEUE code can be modular.
 *
 * Once the queue is registered it must reinject all packets it
 * receives, no matter what.
 */
static const struct nf_queue_handler __rcu *queue_handler __read_mostly;

#if defined(CONFIG_IMQ) || defined(CONFIG_IMQ_MODULE)
static const struct nf_queue_handler __rcu *queue_imq_handler __read_mostly;

void nf_register_queue_imq_handler(const struct nf_queue_handler *qh)
{
	rcu_assign_pointer(queue_imq_handler, qh);
}
EXPORT_SYMBOL_GPL(nf_register_queue_imq_handler);

void nf_unregister_queue_imq_handler(void)
{
	RCU_INIT_POINTER(queue_imq_handler, NULL);
	synchronize_rcu();
}
EXPORT_SYMBOL_GPL(nf_unregister_queue_imq_handler);
#endif

/* return EBUSY when somebody else is registered, return EEXIST if the
 * same handler is registered, return 0 in case of success. */
void nf_register_queue_handler(const struct nf_queue_handler *qh)
{
	/* should never happen, we only have one queueing backend in kernel */
	WARN_ON(rcu_access_pointer(queue_handler));
	rcu_assign_pointer(queue_handler, qh);
}
EXPORT_SYMBOL(nf_register_queue_handler);

/* The caller must flush their queue before this */
void nf_unregister_queue_handler(void)
{
	RCU_INIT_POINTER(queue_handler, NULL);
	synchronize_rcu();
}
EXPORT_SYMBOL(nf_unregister_queue_handler);

void nf_queue_entry_release_refs(struct nf_queue_entry *entry)
{
	/* Release those devices we held, or Alexey will kill me. */
#ifdef CONFIG_XIRRUS
        if (entry->nfct)
                nf_conntrack_put(&entry->nfct->ct_general);
#endif
	if (entry->indev)
		dev_put(entry->indev);
	if (entry->outdev)
		dev_put(entry->outdev);
#ifdef CONFIG_BRIDGE_NETFILTER
#ifdef CONFIG_XIRRUS_PARANOIA
	if (entry->skb)
#endif
            if(entry->skb->nf_bridge) {
		struct nf_bridge_info *nf_bridge = entry->skb->nf_bridge;

		if (nf_bridge->physindev)
			dev_put(nf_bridge->physindev);
		if (nf_bridge->physoutdev)
			dev_put(nf_bridge->physoutdev);
	}
#endif
	/* Drop reference to owner of hook which queued us. */
	module_put(entry->elem->owner);
}
EXPORT_SYMBOL_GPL(nf_queue_entry_release_refs);

/* Bump dev refs so they don't vanish while packet is out */
bool nf_queue_entry_get_refs(struct nf_queue_entry *entry)
{
	if (!try_module_get(entry->elem->owner))
		return false;

#ifdef CONFIG_XIRRUS
        if (entry->nfct)
                nf_conntrack_get(&entry->nfct->ct_general);
#endif
        if (entry->indev)
		dev_hold(entry->indev);
	if (entry->outdev)
		dev_hold(entry->outdev);
#ifdef CONFIG_BRIDGE_NETFILTER
#ifdef CONFIG_XIRRUS_PARANOIA
	if (entry->skb)
#endif
            if (entry->skb->nf_bridge) {
		struct nf_bridge_info *nf_bridge = entry->skb->nf_bridge;
		struct net_device *physdev;

		physdev = nf_bridge->physindev;
		if (physdev)
			dev_hold(physdev);
		physdev = nf_bridge->physoutdev;
		if (physdev)
			dev_hold(physdev);
	}
#endif

	return true;
}
EXPORT_SYMBOL_GPL(nf_queue_entry_get_refs);

/*
 * Any packet that leaves via this function must come back
 * through nf_reinject().
 */
int nf_queue(struct sk_buff *skb,
		      struct nf_hook_ops *elem,
		      u_int8_t pf, unsigned int hook,
		      struct net_device *indev,
		      struct net_device *outdev,
		      int (*okfn)(struct sk_buff *),
		      unsigned int queuenum,
		      unsigned int queuetype)
{
	int status = -ENOENT;
	struct nf_queue_entry *entry = NULL;
	const struct nf_afinfo *afinfo;
	const struct nf_queue_handler *qh;
#ifdef CONFIG_XIRRUS
        enum ip_conntrack_info ctinfo;
        struct nf_conn *ct;
#endif
#ifdef  EXTRA_NFQUEUE_CHECKS
        if (skb && SKB_IS_FREE(skb)) {
	        printk(KERN_ERR "hey! entry->skb has already been freed (skb=%p, releaser=%s): now being dropped", skb, skb->releaser);
                dump_stack();
                return status;
	}
#endif
	/* QUEUE == DROP if no one is waiting, to be safe. */
	rcu_read_lock();

	if (queuetype == NF_IMQ_QUEUE) {
#if defined(CONFIG_IMQ) || defined(CONFIG_IMQ_MODULE)
		qh = rcu_dereference(queue_imq_handler);
#else
		BUG();
		goto err_unlock;
#endif
	} else {
		qh = rcu_dereference(queue_handler);
	}

	if (!qh) {
		status = -ESRCH;
		goto err_unlock;
	}

	afinfo = nf_get_afinfo(pf);
	if (!afinfo)
		goto err_unlock;

	entry = kmalloc(sizeof(*entry) + afinfo->route_key_size, GFP_ATOMIC);
	if (!entry) {
		status = -ENOMEM;
		goto err_unlock;
	}
#ifdef CONFIG_XIRRUS
        ct = skb ? nf_ct_get(skb, &ctinfo) : NULL;
#endif
	*entry = (struct nf_queue_entry) {
		.skb	= skb,
		.elem	= elem,
		.pf	= pf,
		.hook	= hook,
		.indev	= indev,
		.outdev	= outdev,
                .okfn	= okfn,
		.size	= sizeof(*entry) + afinfo->route_key_size,
#ifdef CONFIG_XIRRUS
                .nfct   = (!ct || nf_ct_is_untracked(ct)) ? NULL : ct,
#endif
#ifdef  EXTRA_NFQUEUE_CHECKS
                .dev    = skb->dev,
                .nlmark = skb->nlmark,
                .bridge = skb->nf_bridge,
                .queue  = queuenum,
#endif
	};

	if (!nf_queue_entry_get_refs(entry)) {
		status = -ECANCELED;
		goto err_unlock;
	}
	skb_dst_force(skb);
	afinfo->saveroute(skb, entry);
	status = qh->outfn(entry, queuenum);

	rcu_read_unlock();

#ifdef CONFIG_XIRRUS
        if (status == NFQNL_REINJECT) {
                //use best effort: don't throw packets away if they
                //can't be queued instead, mark them and reinject them
                NLMARK(entry->skb) |= NLMARK_DPI_WORKING;
                //nf_dump_entry("reinject", __func__, entry);
                nf_reinject(entry, NF_REPEAT);
        } else if (status == NFQNL_DROP)
#else
        if (status < 0)
#endif
        {
		nf_queue_entry_release_refs(entry);
		goto err;
	}

	return 0;

err_unlock:
	rcu_read_unlock();
err:
	kfree(entry);

	return status;
}

void nf_reinject(struct nf_queue_entry *entry, unsigned int verdict)
{
	struct sk_buff *skb = entry->skb;
	struct nf_hook_ops *elem = entry->elem;
	const struct nf_afinfo *afinfo;
	int err;

	rcu_read_lock();

#ifdef  EXTRA_NFQUEUE_CHECKS
        {
                enum ip_conntrack_info ctinfo;
                struct nf_conn *ct = nf_ct_get(skb, &ctinfo);
                char *type = "reinject";

                if (skb && SKB_IS_FREE(skb)) {
                        printk(KERN_ERR "hey! nf_queue: entry->skb has already been freed (skb=%p, releaser=%s): verdict was %d, now being dropped", skb, skb->releaser, verdict);
                        dump_stack();
                        entry->skb = 0;
                        type = "error";
                }
                if (skb && skb->dev != entry->dev) {
                        printk(KERN_ERR "hey! nf_queue: entry->skb->dev (%p) does not match entry->dev (%p), now being dropped", skb->dev, entry->dev);
                        dump_stack();
                        verdict = NF_DROP;
                        type = "error";
                }
                if (skb && skb->nf_bridge != entry->bridge) {
                        printk(KERN_ERR "hey! nf_queue: entry->skb->nf_bridge (%p) does not match entry->bridge (%p), now being dropped", skb->nf_bridge, entry->bridge);
                        dump_stack();
                        verdict = NF_DROP;
                        type = "error";
                }
                if (ct && !nf_ct_is_untracked(ct) && (struct nf_conn *)skb->nfct != entry->nfct) {
                        printk(KERN_ERR "hey! nf_queue: entry->skb->nfct (%p) does not match entry->nfct (%p), now being dropped", skb->nfct, entry->nfct);
                        dump_stack();
                        verdict = NF_DROP;
                        type = "error";
                }
                //nf_dump_entry(type, __func__, entry);
        }
#endif
#ifdef CONFIG_XIRRUS
        if (entry->nfct)
            entry->nfct->nfmark |= NFMARK_DPI_REINJECTED;
#endif
        nf_queue_entry_release_refs(entry);

#ifdef  EXTRA_NFQUEUE_CHECKS
        if (!entry->skb)
                goto err_unlock;
#endif
#ifdef CONFIG_XIRRUS
        // mark skb as reinjected
        skb->mark |= NFMARK_DPI_REINJECTED;
#endif
        /* Continue traversal iff userspace said ok... */
	if (verdict == NF_REPEAT) {
		elem = list_entry(elem->list.prev, struct nf_hook_ops, list);
		verdict = NF_ACCEPT;
	}

	if (verdict == NF_ACCEPT) {
		afinfo = nf_get_afinfo(entry->pf);
		if (!afinfo || afinfo->reroute(skb, entry) < 0)
			verdict = NF_DROP;
	}

	if (verdict == NF_ACCEPT) {
next_hook:
		verdict = nf_iterate(&nf_hooks[entry->pf][entry->hook],
				     skb, entry->hook,
				     entry->indev, entry->outdev, &elem,
				     entry->okfn, INT_MIN);
	}

	switch (verdict & NF_VERDICT_MASK) {
	        case NF_ACCEPT:
	        case NF_STOP:
	        	local_bh_disable();
	        	entry->okfn(skb);
	        	local_bh_enable();
	        	break;
	        case NF_QUEUE:
	        	err = nf_queue(skb, elem, entry->pf, entry->hook,
	        			entry->indev, entry->outdev, entry->okfn,
	        			verdict >> NF_VERDICT_QBITS,
	        			verdict &  NF_VERDICT_MASK);
	        	if (err < 0) {
	        		if (err == -ECANCELED)
	        			goto next_hook;
                                if (err == -ESRCH &&
                                    (verdict & NF_VERDICT_FLAG_QUEUE_BYPASS))
	        			goto next_hook;
                                kfree_skb(skb);
	        	}
	        	break;
	        case NF_STOLEN:
	        	break;
	        default:
                        kfree_skb(skb);
	}

#ifdef  EXTRA_NFQUEUE_CHECKS
err_unlock:
#endif
#ifdef CONFIG_XIRRUS
        if (entry->nfct)
            entry->nfct->nfmark &= ~NFMARK_DPI_REINJECTED;
#endif
	rcu_read_unlock();
	kfree(entry);
}
EXPORT_SYMBOL(nf_reinject);
