/*
 *	xt_TRAFFIC - Netfilter module to modify the trafic limits for an skb
 *
 *	(C) 2012 Xirrus, Inc. *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License version 2 as
 *	published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <net/checksum.h>
#include <net/netfilter/nf_conntrack.h>
#include <net/netfilter/nf_conntrack_core.h>
#include <linux/netfilter/x_tables.h>
#include <linux/netfilter/xt_TRAFFIC.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Xirrus, Inc.");
MODULE_DESCRIPTION("Xtables: packet traffic limits");
MODULE_ALIAS("ipt_TRAFFIC");
MODULE_ALIAS("ip6t_TRAFFIC");

static struct nf_limit limit_tbl[MAX_TRAFFIC_LIMIT_INDEX] = {};

#define set_traffic_conn(limit, setting)        if (!atomic32_get(limit.value) || setting < atomic32_get(limit. value)) {                                       atomic32_set(limit. value, setting); set_limit_signature(&limit); }
#define set_traffic_all( limit, setting, idx)   if (!atomic_p_get(limit      ) || setting < atomic32_get(limit->value)) { atomic_p_set(limit, &limit_tbl[idx]); atomic32_set(limit->value, setting); set_limit_signature( limit); }

static unsigned int
traffic(struct sk_buff *skb, const struct xt_action_param *par)
{
        struct xt_traffic_target_info *traffic_info = (struct xt_traffic_target_info *)par->targinfo;
        enum ip_conntrack_info ctinfo;
        struct nf_conn *ct;

        // stash value in conntrack info
        conntrack_global_lock();
        if ((ct = nf_ct_get(skb, &ctinfo)) != NULL && !nf_ct_is_untracked(ct)) {
                if (conntrack_is_valid(ct) && conntrack_is_used(ct)) {
                        //printk(KERN_INFO "traffic: protocol=%d, mode=%d, limit=%d\n", ct->protocol & 0xfff, traffic_info->mode, traffic_info->limit.value);
                        conntrack_lock(ct);
                        switch (traffic_info->mode) {
                            case XT_TRAFFIC_CONN_PPS : set_traffic_conn(ct->limit_conn_pps , traffic_info->limit.value);                      break;
                            case XT_TRAFFIC_CONN_KBPS: set_traffic_conn(ct->limit_conn_kbps, traffic_info->limit.value);                      break;
                            case XT_TRAFFIC_ALL_PPS  : set_traffic_all (ct->limit_all_pps  , traffic_info->limit.value, traffic_info->index); break;
                            case XT_TRAFFIC_ALL_KBPS : set_traffic_all (ct->limit_all_kbps , traffic_info->limit.value, traffic_info->index); break;
                        }
                        conntrack_unlock(ct);
                }
        }
        conntrack_global_unlock();
        return XT_CONTINUE;
}

static struct xt_target traffic_reg __read_mostly = {
	.name           = "TRAFFIC",
	.revision       = 0,
	.family         = NFPROTO_UNSPEC,
	.target         = traffic,
	.targetsize     = sizeof(struct xt_traffic_target_info),
	.me             = THIS_MODULE,
};

static int __init traffic_init(void)
{
	return xt_register_target(&traffic_reg);
}

static void __exit traffic_exit(void)
{
	xt_unregister_target(&traffic_reg);
}

module_init(traffic_init);
module_exit(traffic_exit);
