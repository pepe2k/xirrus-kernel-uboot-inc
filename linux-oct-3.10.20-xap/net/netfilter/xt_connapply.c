/*
 *	xt_connapply - Netfilter module to apply conntrack actions to an skb
 *
 *	(C) 2012 Xirrus, Inc.
 */
#ifdef  CONFIG_XIRRUS

#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/netfilter/x_tables.h>
#include <linux/netfilter/xt_connapply.h>
#include <linux/netfilter/xt_DSCP.h>
#include <net/netfilter/nf_conntrack.h>
#include <net/netfilter/nf_conntrack_core.h>
#include <net/dsfield.h>

MODULE_AUTHOR("Xirrus, Inc.");
MODULE_DESCRIPTION("Xtables: apply conntrack actions");
MODULE_LICENSE("GPL");
MODULE_ALIAS("ipt_connapply");
MODULE_ALIAS("ip6t_connapply");

int xt_connapply_debug = 0;
int xt_connapply_mask  = 0xffffffff;
atomic_t xt_connapply_count = ATOMIC_INIT(0);
int (*connapply_callback)(struct sk_buff *skb) = NULL;
EXPORT_SYMBOL(xt_connapply_debug);
EXPORT_SYMBOL(xt_connapply_mask );
EXPORT_SYMBOL(connapply_callback);
EXPORT_SYMBOL(xt_connapply_count);

extern char (*nfct_app_names)[][MAX_DPI_APP_NAME];

#define ELAPSED_JIF(_then)           ({u_int32_t now = jiffies, then = _then, elapsed; elapsed = (now >= then) ? now - then : now - (int32_t)then;})

//------------------------------------------------------------------------------
// helper routine to check traffic limits
//------------------------------------------------------------------------------
static inline int dpi_check_traffic(struct nf_limit *limit, int application, int length, char *units)
{
        if (limit != 0 && atomic32_get(limit->value) != 0 && limit_is_valid(limit)) {
                if (ELAPSED_JIF(atomic32_get(limit->start)) > HZ) {
                        if (xt_connapply_debug) {
                                printk(KERN_INFO "dpi limit (%p): application %s dropped %d %s (accum=%d, limit=%d, elapsed=%u)\n", limit,
                                                    (application && nfct_app_names) ? (*nfct_app_names)[application % MAX_DPI_APP_INDEX] : "(unknown)",
                                                     atomic32_get(limit->accum) > atomic32_get(limit->value) ?
                                                     atomic32_get(limit->accum) - atomic32_get(limit->value) : 0, units,
                                                     atomic32_get(limit->accum) , atomic32_get(limit->value),
                                                                      ELAPSED_JIF(atomic32_get(limit->start)));
                        }
                        atomic32_clr(limit->accum);
                        atomic32_set(limit->start, jiffies);
                }
                return (atomic32_add(limit->accum, length) > atomic32_get(limit->value));
        }
        return (0);
}

static bool
connapply_mt(const struct sk_buff *_skb, struct xt_action_param *par)
{
        struct sk_buff *skb = (struct sk_buff *)_skb;
        enum ip_conntrack_info ctinfo;
        struct nf_conn *ct;
        int limit = 0;

        conntrack_global_lock();
        if ((ct = nf_ct_get(skb, &ctinfo)) != NULL && !nf_ct_is_untracked(ct) && !(skb->nlmark & NLMARK_DPI_WORKING)) {
                if (conntrack_is_valid(ct) && conntrack_is_used(ct)) {
                        //printk(KERN_INFO "connapply_mt: protocol=%d, nfmark=0x%016lx, dscp=%d, use=%d, status=%lu\n", ct->protocol & 0xfff, ct->nfmark, ct->dscp, atomic_read(&ct->ct_general.use), ct->status);
                        conntrack_lock(ct);
                        {
                            u32 protocol    = atomic32_get(ct->protocol   );
                            u32 proto_stack = atomic32_get(ct->proto_stack);
                            u32 nfmark      = atomic32_get(ct->nfmark     );
                            u32 nfmask      = atomic32_get(ct->nfmask     );
                            u32 dscp        = atomic32_get(ct->dscp       );
                            u32 application = protocol & NLMARK_DPI_PROTOCOL_MASK;

                            // check protocol modification
                            if (protocol && ct_data_is_valid(ct, protocol, xt_connapply_mask)) {
                                    skb->nlmark |= protocol;
                            }
                            // check protocol stack modification
                            if (proto_stack && ct_data_is_valid(ct, proto_stack, 0xffffffff)) {
                                    skb->nlmark |= ((u64)proto_stack << 32);
                            }
                            // check nfmark modification
                            if ((nfmark & ~NFMARK_DPI_REINJECTED) && ct_data_is_valid(ct, nfmark, 0xffffffff)) {
                                    skb->mark &= ~nfmask;
                                    skb->mark |=  nfmark;
                            }
                            // check dscp modification
                            if (dscp && ct_data_is_valid(ct, dscp, 0x3f)) {
                                    ipv4_change_dsfield(ip_hdr(skb), (__u8)(~XT_DSCP_MASK), dscp << XT_DSCP_SHIFT);
                            }
                            // check traffic limits
                            limit = dpi_check_traffic(atomic_p_get(ct->limit_all_kbps), application, skb->len, "packets") || dpi_check_traffic(atomic_p_get(ct->limit_all_pps), application, 1, "bytes") ||
                                    dpi_check_traffic(            &ct->limit_conn_kbps, application, skb->len, "packets") || dpi_check_traffic(            &ct->limit_conn_pps, application, 1, "bytes");
                        }
                        conntrack_unlock(ct);
                }
        }
        if (connapply_callback) {
            connapply_callback(skb);
        }
        atomic_inc(&xt_connapply_count);
        conntrack_global_unlock();
	return (limit);
}

static struct xt_match connapply_mt_reg __read_mostly = {
	.name      = "connapply",
	.revision  = 0,
	.family    = NFPROTO_UNSPEC,
	.match     = connapply_mt,
	.matchsize = sizeof(struct xt_connapply_info),
	.me        = THIS_MODULE,
};

static int __init connapply_mt_init(void)
{
	return xt_register_match(&connapply_mt_reg);
}

static void __exit connapply_mt_exit(void)
{
	xt_unregister_match(&connapply_mt_reg);
}

module_init(connapply_mt_init);
module_exit(connapply_mt_exit);

#endif
