/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2015 Cavium, Inc.
 */

#include <linux/module.h>
#include <linux/phy.h>
#ifdef CONFIG_XIRRUS
#include <linux/netdevice.h>
#endif

#define PHY_ID_AQR105			0x03a1b4a3
#define PHY_ID_AQR109revAe		0x03a1b5c0 // early AQR109revA silicon had device id incorrectly set to this value (wrong fuse bits blown)
#define PHY_ID_AQR109revA		0x03a1b501
#define PHY_ID_AQR109revB		0x03a1b502

#define PHY_ID_AQR105_NAME		"AQR105"
#define PHY_ID_AQR109revA_NAME		"AQR109revA"
#define PHY_ID_AQR109revB_NAME		"AQR109revB"

#define PMA_RECEIVE_VENDOR_STATE_1	(MII_ADDR_C45 | 0x01 << 16 | 0xe800)

#define AN_TX_VENDOR_ALARMS_2		(MII_ADDR_C45 | 0x07 << 16 | 0xcc01)
#define AN_VENDOR_STATUS_1		(MII_ADDR_C45 | 0x07 << 16 | 0xc800)
#define AN_TX_VENDOR_INT_MASK_2		(MII_ADDR_C45 | 0x07 << 16 | 0xd401)

#define GLOBAL_CW_VENDOR_INT_FLAGS	(MII_ADDR_C45 | 0x1e << 16 | 0xfc01)
#define GLOBAL_INT_CW_VENDOR_MASK	(MII_ADDR_C45 | 0x1e << 16 | 0xff01)

#define GLOBAL_LED_PROVISIONING_1	(MII_ADDR_C45 | 0x1e << 16 | 0xc430) //blue
#define GLOBAL_LED_PROVISIONING_2	(MII_ADDR_C45 | 0x1e << 16 | 0xc431) //orange
#define GLOBAL_LED_PROVISIONING_3	(MII_ADDR_C45 | 0x1e << 16 | 0xc432) //green

#ifdef CONFIG_XIRRUS
static int phy_speed(int encoded_speed)
{
	int speed;

	switch (encoded_speed) {
	case 0:
		speed = 10;
		break;
	case 1:
		speed = 100;
		break;
	case 2:
		speed = 1000;
		break;
	case 4:
		speed = 2500;
		break;
	default:
		speed = 0;
		break;
	}
	return (speed);
}

static void set_led_activity(struct phy_device *phydev, u32 regnum, int enable)
{
	u16  regdata;

	regdata = phy_read(phydev, regnum);
	if (enable)
		regdata |= 0x000c;
	else
		regdata &= 0xfff3;
	phy_write(phydev, regnum, regdata);
}
#endif

static int aqr105_config_aneg(struct phy_device *phydev)
{
	return 0;
}

static int aqr105_read_status(struct phy_device *phydev)
{
	int	reg;
#ifdef CONFIG_XIRRUS
	int speed;

	/* indicate that we support 2500baseX */
	phydev->supported |= SUPPORTED_2500baseX_Full;
	/* indicate that we support these features */
	phydev->supported |= SUPPORTED_100baseT_Full;
	phydev->supported |= SUPPORTED_1000baseT_Full;
	phydev->supported |= SUPPORTED_Autoneg;
#endif

	reg = phy_read(phydev, PMA_RECEIVE_VENDOR_STATE_1);
	/* Set the link state */
	if ((reg & 1) == 0)
		phydev->link = 0;
	else {
		phydev->link = 1;

		reg = phy_read(phydev, AN_VENDOR_STATUS_1);
		/* Set the duplex mode */
		if ((reg & 1) == 0)
			phydev->duplex = 0;
		else
			phydev->duplex = 1;

		/* Set the speed */
		reg = (reg >> 1) & 7;

#ifdef CONFIG_XIRRUS
		/* If the phy speed changes, show rx/tx activity only on the LED associated with the new speed. */
		if ((speed = phy_speed(reg)) != 0 && speed != phydev->speed) {
			/* Turn off blinking for tx/rx activity for all LEDs */
			set_led_activity(phydev, GLOBAL_LED_PROVISIONING_1, 0);
			set_led_activity(phydev, GLOBAL_LED_PROVISIONING_2, 0);
			set_led_activity(phydev, GLOBAL_LED_PROVISIONING_3, 0);

			/* Turn on blinking for tx/rx activity for specific LED */
			switch (speed) {
			case   10:
			case  100: //green
				set_led_activity(phydev, GLOBAL_LED_PROVISIONING_3, 1);
				break;
			case 1000: //orange
				set_led_activity(phydev, GLOBAL_LED_PROVISIONING_2, 1);
				break;
			case 2500: //blue
				set_led_activity(phydev, GLOBAL_LED_PROVISIONING_1, 1);
				break;
			}
		}
#endif
		switch (reg) {
		case 0:
			phydev->speed = 10;
			break;
		case 1:
			phydev->speed = 100;
			break;
		case 2:
			phydev->speed = 1000;
			break;
		case 3:
			phydev->speed = 10000;
			break;
		case 4:
			phydev->speed = 2500;
			break;
		case 5:
			phydev->speed = 5000;
			break;
		default:
			phydev->speed = -1;
			break;
		}
	}

	return 0;
}

static int  aqr105_ack_interrupt(struct phy_device *phydev)
{
	int	reg;

	reg = phy_read(phydev, AN_TX_VENDOR_ALARMS_2);

	return 0;
}

static int aqr105_config_intr(struct phy_device *phydev)
{
	int	reg;

	if (phydev->interrupts == PHY_INTERRUPT_ENABLED) {
		reg = phy_read(phydev, AN_TX_VENDOR_INT_MASK_2);
		reg |= 0x1;
		phy_write(phydev, AN_TX_VENDOR_INT_MASK_2, reg);

		reg = phy_read(phydev, GLOBAL_INT_CW_VENDOR_MASK);
		reg |= 0x1000;
		phy_write(phydev, GLOBAL_INT_CW_VENDOR_MASK, reg);
	} else {
		reg = phy_read(phydev, GLOBAL_INT_CW_VENDOR_MASK);
		reg &= ~0x1000;
		phy_write(phydev, GLOBAL_INT_CW_VENDOR_MASK, reg);

		reg = phy_read(phydev, AN_TX_VENDOR_INT_MASK_2);
		reg &= ~0x1;
		phy_write(phydev, AN_TX_VENDOR_INT_MASK_2, reg);
	}

	return 0;
}

static int aqr105_did_interrupt(struct phy_device *phydev)
{
	int	reg;

	reg = phy_read(phydev, GLOBAL_CW_VENDOR_INT_FLAGS);
	if (reg & 0x1000)
		return 1;

	return 0;
}

#ifdef CONFIG_XIRRUS
static int aqr105_led_ctrl(struct phy_device *phydev, int led_enable)
{
	if (led_enable & 0x1) {
		/* Configure all LEDs for link up indicator */
		phy_write(phydev, GLOBAL_LED_PROVISIONING_1, 0x4002);
		phy_write(phydev, GLOBAL_LED_PROVISIONING_2, 0x0042);
		phy_write(phydev, GLOBAL_LED_PROVISIONING_3, 0x0022);
		/* Turn on blinking for tx/rx activity for specific LED */
		switch (phydev->speed) {
		case   10:
		case  100: //green
			set_led_activity(phydev, GLOBAL_LED_PROVISIONING_3, 1);
			break;
		case 1000: //orange
			set_led_activity(phydev, GLOBAL_LED_PROVISIONING_2, 1);
			break;
		case 2500: //blue
			set_led_activity(phydev, GLOBAL_LED_PROVISIONING_1, 1);
			break;
		}
	}
	else {
		/* Turn off all LEDs */
		phy_write(phydev, GLOBAL_LED_PROVISIONING_1, 0);
		phy_write(phydev, GLOBAL_LED_PROVISIONING_2, 0);
		phy_write(phydev, GLOBAL_LED_PROVISIONING_3, 0);
	}
	return 0;
}

static int aqr105_soft_reset(struct phy_device *phydev)
{
	if (phydev->attached_dev)
		pr_info("%s - hey! why are we trying to give the aquantia ethernet phy a soft reset (%s)\n", __func__, phydev->attached_dev->name);
	else
		pr_info("%s - hey! why are we trying to give the aquantia ethernet phy a soft reset\n", __func__);

	return 0;
}
#endif

static int aqr105_match_phy_device(struct phy_device *phydev)
{
	if (phydev->c45_ids.device_ids[1] == PHY_ID_AQR105) {
          pr_info("Aquantia Ethernet PHY Type: %s (0x%08x)\n", PHY_ID_AQR105_NAME, phydev->c45_ids.device_ids[1]);
	}
	if (phydev->c45_ids.device_ids[1] == PHY_ID_AQR109revAe ||
            phydev->c45_ids.device_ids[1] == PHY_ID_AQR109revA ) {
          pr_info("Aquantia Ethernet PHY Type: %s (0x%08x)\n", PHY_ID_AQR109revA_NAME, phydev->c45_ids.device_ids[1]);
	}
	if (phydev->c45_ids.device_ids[1] == PHY_ID_AQR109revB) {
          pr_info("Aquantia Ethernet PHY Type: %s (0x%08x)\n", PHY_ID_AQR109revB_NAME, phydev->c45_ids.device_ids[1]);
	}

	return ((phydev->c45_ids.device_ids[1] & 0xfffffff0) == (PHY_ID_AQR105      & 0xfffffff0) ||
		(phydev->c45_ids.device_ids[1] & 0xfffffff0) == (PHY_ID_AQR109revAe & 0xfffffff0) ||
		(phydev->c45_ids.device_ids[1] & 0xfffffff0) == (PHY_ID_AQR109revA  & 0xfffffff0) || // PHY_ID_AQR109revA and PHY_ID_AQR109revB are actually equal when
		(phydev->c45_ids.device_ids[1] & 0xfffffff0) == (PHY_ID_AQR109revB  & 0xfffffff0));  // masking off lower nibble, but just included for completeness.
}

static struct phy_driver aqr105_driver[] = {
{
	.phy_id			= 0,
	.phy_id_mask		= 0,
	.name			= "Aquantia aqr105",
	.flags			= PHY_HAS_INTERRUPT,
	.config_aneg		= aqr105_config_aneg,
	.read_status		= aqr105_read_status,
	.ack_interrupt		= aqr105_ack_interrupt,
	.config_intr		= aqr105_config_intr,
	.did_interrupt		= aqr105_did_interrupt,
	.match_phy_device 	= aqr105_match_phy_device,
#ifdef CONFIG_XIRRUS
	.led_ctrl		= aqr105_led_ctrl,
	.soft_reset		= aqr105_soft_reset,
#endif
	.driver			= {
		.owner = THIS_MODULE,
	},
} };

static int __init aquantia_init(void)
{
	return phy_drivers_register(aqr105_driver, ARRAY_SIZE(aqr105_driver));
}
module_init(aquantia_init);

static void __exit aquantia_exit(void)
{
	phy_drivers_unregister(aqr105_driver, ARRAY_SIZE(aqr105_driver));
}
module_exit(aquantia_exit);

MODULE_AUTHOR("Carlos Munoz <cmunoz@caviumnetworks.com>");
MODULE_LICENSE("GPL");
