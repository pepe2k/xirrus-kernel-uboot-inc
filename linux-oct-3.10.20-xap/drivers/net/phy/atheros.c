/*
 * drivers/net/phy/atheros.c
 *
 * Driver for Atheros PHYs
 *
 * Author: Drew Bertagna
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 */
#ifdef  CONFIG_XIRRUS_AR8033

#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/unistd.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/spinlock.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/mii.h>
#include <linux/ethtool.h>
#include <linux/phy.h>
#include <linux/of.h>

#include <asm/io.h>
#include <asm/irq.h>
#include <asm/uaccess.h>

MODULE_DESCRIPTION("Atheros PHY driver");
MODULE_AUTHOR("Drew Bertagna");
MODULE_LICENSE("GPL");

#define XR2K_SELECT              0x10    /* XR2K is a special case       */

/* must hit the soft reset on the ar8033 after re-negotiating the link */

static int atheros_config_aneg(struct phy_device *phydev)
{
	int result;

	result = genphy_config_aneg(phydev);
	if (result < 0)
		return result;

	result = phy_read(phydev, MII_BMCR);
        result |= BMCR_RESET;
	result = phy_write(phydev, MII_BMCR, result);

	return result;
}

static int atheros_soft_reset(struct phy_device *phydev)
{
	int result;

	if (phydev->attached_dev)
		pr_info("%s - executing atheros ethernet phy (ar8033) soft reset (%s)\n", __func__, phydev->attached_dev->name);
	else
		pr_info("%s - executing atheros ethernet PHY (ar8033) soft reset\n", __func__);

	result = phy_read(phydev, MII_BMCR);
        result |= BMCR_RESET;
	result = phy_write(phydev, MII_BMCR, result);

	return result;
}

/* same as genphy_config_init which is declared as static in phy_device.c */

static int atheros_config_init(struct phy_device *phydev)
{
	int val;
	u32 features;

	/* For now, I'll claim that the generic driver supports
	 * all possible port types */
	features = (SUPPORTED_TP | SUPPORTED_MII
			| SUPPORTED_AUI | SUPPORTED_FIBRE |
			SUPPORTED_BNC);

	/* Do we support autonegotiation? */
	val = phy_read(phydev, MII_BMSR);

	if (val < 0)
		return val;

	if (val & BMSR_ANEGCAPABLE)
		features |= SUPPORTED_Autoneg;

	if (val & BMSR_100FULL)
		features |= SUPPORTED_100baseT_Full;
	if (val & BMSR_100HALF)
		features |= SUPPORTED_100baseT_Half;
	if (val & BMSR_10FULL)
		features |= SUPPORTED_10baseT_Full;
	if (val & BMSR_10HALF)
		features |= SUPPORTED_10baseT_Half;

	if (val & BMSR_ESTATEN) {
		val = phy_read(phydev, MII_ESTATUS);

		if (val < 0)
			return val;

		if (val & ESTATUS_1000_TFULL)
			features |= SUPPORTED_1000baseT_Full;
		if (val & ESTATUS_1000_THALF)
			features |= SUPPORTED_1000baseT_Half;
	}

	phydev->supported = features;
	phydev->advertising = features;

	/* clear interrupts that are clear-on-read */
	phy_read(phydev, MII_ATH_INT_STATUS);

	return 0;
}

static int atheros_led_ctrl(struct phy_device *phydev, int led_enable)
{
        int reg, err;

        if (led_enable & XR2K_SELECT) {
                reg = phy_read(phydev, MII_ATH_LED_OVERRIDE);
                if (reg < 0)
                        return reg;

                if (led_enable & 0x1)
                        reg &= ~MII_ATH_LED_OVERRIDE_OFF;
                else
                        reg |=  MII_ATH_LED_OVERRIDE_OFF;

	        err = phy_write(phydev, MII_ATH_LED_OVERRIDE, reg);
        }
        else {
                reg = phy_read(phydev, MII_ATH_LED_CTRL);
                if (reg < 0)
                        return reg;

                if (led_enable & 0x1)
                        reg &= ~MII_ATH_LED_CTRL_OFF;
                else
                        reg |=  MII_ATH_LED_CTRL_OFF;

	        err = phy_write(phydev, MII_ATH_LED_CTRL, reg);
        }

	return err;
}

static struct phy_driver atheros_drivers[] = {
        {
        .phy_id      = PHY_ID_AR8033,
	.phy_id_mask = 0xffffffff,
	.name        = "Atheros AR8033",
	.config_init = &atheros_config_init,
	.features    = PHY_GBIT_FEATURES |
		       SUPPORTED_Pause | SUPPORTED_Asym_Pause,
	.flags       = PHY_HAS_MAGICANEG,
	.config_aneg = &atheros_config_aneg,
	.read_status = &genphy_read_status,
	.led_ctrl    = &atheros_led_ctrl,
	.soft_reset  = &atheros_soft_reset,
	.driver      = { .owner = THIS_MODULE },
        },
};

static int __init atheros_init(void)
{
	int ret;
	int i;

	for (i = 0; i < ARRAY_SIZE(atheros_drivers); i++) {
		ret = phy_driver_register(&atheros_drivers[i]);

		if (ret) {
			while (i-- > 0)
				phy_driver_unregister(&atheros_drivers[i]);
			return ret;
		}
	}

	return 0;
}

static void __exit atheros_exit(void)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(atheros_drivers); i++)
		phy_driver_unregister(&atheros_drivers[i]);
}

module_init(atheros_init);
module_exit(atheros_exit);

#endif
