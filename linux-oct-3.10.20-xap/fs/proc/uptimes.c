//
// Xirrus' clone of the uptime interface: to provide per-cpu idle time
// JAJ - 04/06/2017
//

#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/seq_file.h>
#include <linux/time.h>
#include <linux/kernel_stat.h>
#include <asm/cputime.h>

static int uptimes_proc_show(struct seq_file *m, void *v)
{
    struct timespec uptime;
    struct timespec idle;
	u64 idletime = 0;
	u64 nsec = 0;
	u32 rem = 0;
	int i = 0;

    // Print total system uptime
	do_posix_clock_monotonic_gettime(&uptime);
	monotonic_to_bootbased(&uptime);
	seq_printf(m, "%lu.%02lu",
			(unsigned long) uptime.tv_sec,
			(uptime.tv_nsec / (NSEC_PER_SEC / 100)));

    // Print each cpu's idle time
	for_each_present_cpu(i) {
		idletime     = (__force u64) kcpustat_cpu(i).cpustat[CPUTIME_IDLE];
        nsec         = cputime64_to_jiffies64(idletime) * TICK_NSEC;
        idle.tv_sec  = div_u64_rem(nsec, NSEC_PER_SEC, &rem);
	    idle.tv_nsec = rem;
        seq_printf(m, " %lu.%02lu",
			(unsigned long) idle.tv_sec,
			(idle.tv_nsec / (NSEC_PER_SEC / 100)));
    }

    // End of output
    seq_printf(m, "\n");

	return 0;
}

static int uptimes_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, uptimes_proc_show, NULL);
}

static const struct file_operations uptimes_proc_fops = {
	.open		= uptimes_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init proc_uptimes_init(void)
{
	proc_create("uptimes", 0, NULL, &uptimes_proc_fops);
	return 0;
}
module_init(proc_uptimes_init);
