#ifndef _XT_TRAFFIC_H_target
#define _XT_TRAFFIC_H_target

enum {
        XT_TRAFFIC_CONN_PPS = 0,
        XT_TRAFFIC_CONN_KBPS   ,
        XT_TRAFFIC_ALL_PPS     ,
        XT_TRAFFIC_ALL_KBPS    ,
};

struct nf_limit {
        u_int32_t value;
        u_int32_t accum;
        u_int32_t start;
        u_int32_t signature;
};

struct xt_traffic_target_info {
        u_int8_t mode;
        u_int8_t index;
        struct nf_limit limit;  /* netfilter limit structure */
};

#define MAX_TRAFFIC_LIMIT_INDEX     1024

#endif /*_XT_TRAFFIC_H_target */
