/*
 * Connection state tracking for netfilter.  This is separated from,
 * but required by, the (future) NAT layer; it can also be used by an iptables
 * extension.
 *
 * 16 Dec 2003: Yasuyuki Kozakai @USAGI <yasuyuki.kozakai@toshiba.co.jp>
 *	- generalize L3 protocol dependent part.
 *
 * Derived from include/linux/netfiter_ipv4/ip_conntrack.h
 */

#ifndef _NF_CONNTRACK_H
#define _NF_CONNTRACK_H

#include <linux/netfilter/nf_conntrack_common.h>

#include <linux/bitops.h>
#include <linux/compiler.h>
#include <linux/atomic.h>

#include <linux/netfilter/nf_conntrack_tcp.h>
#include <linux/netfilter/nf_conntrack_dccp.h>
#include <linux/netfilter/nf_conntrack_sctp.h>
#include <linux/netfilter/nf_conntrack_proto_gre.h>
#include <net/netfilter/ipv6/nf_conntrack_icmpv6.h>

#include <net/netfilter/nf_conntrack_tuple.h>

#ifdef CONFIG_XIRRUS_PARANOIA
#include <linux/mm.h>
#endif

#ifdef CONFIG_XIRRUS_NF_NAT_FIXED
#include <net/netfilter/nf_nat.h>
#endif

#ifdef CONFIG_NETFILTER_XT_TARGET_TRAFFIC
#include <linux/netfilter/xt_TRAFFIC.h>
#endif

#ifdef CONFIG_XIRRUS
// MUST match definitions in xir_stats.h
#define MAX_DPI_APP_INDEX                                           3000        // must keep MAX_DPI_APP_INDEX * MAX_DPI_APP_NAME < 64KB to pass names in ioctl
#define MAX_DPI_APP_NAME                                            20

// MUST match definitions in xir_nlmark.h
#define NLMARK_DPI_WORKING                                          0x80000000
#define NLMARK_DPI_COMPLETE                                         0x40000000
#define NLMARK_DPI_PROTOCOL_MASK                                    0x00000fff

#define NLMARK_DPI_PROTOCOL0_MASK                                   NLMARK_DPI_PROTOCOL_MASK
#define NLMARK_DPI_PROTOCOL1_MASK                                   0x00000fff
#define NLMARK_DPI_PROTOCOL2_MASK                                   0x00fff000
#define NLMARK_DPI_PROTOCOL0_SHIFT                                  0
#define NLMARK_DPI_PROTOCOL1_SHIFT                                  0
#define NLMARK_DPI_PROTOCOL2_SHIFT                                  12
#define NLMARK_DPI_PROTOCOL0(protocol   )                           (((protocol   ) & NLMARK_DPI_PROTOCOL0_MASK) >> NLMARK_DPI_PROTOCOL0_SHIFT)
#define NLMARK_DPI_PROTOCOL1(proto_stack)                           (((proto_stack) & NLMARK_DPI_PROTOCOL1_MASK) >> NLMARK_DPI_PROTOCOL1_SHIFT)
#define NLMARK_DPI_PROTOCOL2(proto_stack)                           (((proto_stack) & NLMARK_DPI_PROTOCOL2_MASK) >> NLMARK_DPI_PROTOCOL2_SHIFT)

// MUST match definitions in xir_nfmark.h
#define NFMARK_DPI_REINJECTED                                       0x01000000
#endif

#ifdef  CONFIG_XIRRUS_PARANOIA
#undef  CONNTRACK_ATOMIC_OPS
#endif
#ifdef  CONNTRACK_ATOMIC_OPS
#define atomic_p_get(v)                                             ((typeof(v))((u64)__sync_add_and_fetch((u64 *)&(v), (u64)(0))))
#define atomic_p_set(v, i)                                          (u64)__sync_val_compare_and_swap((u64 *)&(v), (u64)(v), (u64)(i))
#define atomic32_set(v, i)                                          (u32)__sync_val_compare_and_swap((u32 *)&(v), (u32)(v), (u32)(i))
#define atomic32_or( v, i)                                          (u32)__sync_or_and_fetch( (u32 *)&(v), (u32)i)
#define atomic32_and(v, i)                                          (u32)__sync_and_and_fetch((u32 *)&(v), (u32)i)
#define atomic32_add(v, i)                                          (u32)__sync_add_and_fetch((u32 *)&(v), (u32)i)
#define atomic32_inc(v)                                             (u32)__sync_add_and_fetch((u32 *)&(v), (u32)1)
#define atomic32_clr(v)                                             (u32)__sync_and_and_fetch((u32 *)&(v), (u32)0)
#define atomic32_get(v)                                             (u32)__sync_add_and_fetch((u32 *)&(v), (u32)0)
#define atomic32_post_inc(v)                                        (u32)__sync_fetch_and_add((u32 *)&(v), (u32)1)
#else
#define atomic_p_get(v)                                                  ((v))
#define atomic_p_set(v, i)                                               ((v)  = (i))
#define atomic32_set(v, i)                                          (u32)((v)  = (i))
#define atomic32_or( v, i)                                          (u32)((v) |= (i))
#define atomic32_and(v, i)                                          (u32)((v) &= (i))
#define atomic32_add(v, i)                                          (u32)((v) += (i))
#define atomic32_inc(v)                                             (u32)((v) += (1))
#define atomic32_clr(v)                                             (u32)((v)  = (0))
#define atomic32_get(v)                                             (u32)((v))
#define atomic32_post_inc(v)                                        (u32)((v)++)
#endif

#ifdef  CONFIG_XIRRUS_PARANOIA
#define CONNTRACK_EXTRA_CHECKS
#endif
#ifdef  CONNTRACK_EXTRA_CHECKS
#define stupid_pointer_check(p)                                     (((u64)(p) & 0x7) || !virt_addr_valid(p))   // (((((u64)(p)) >> 32) & 0xfffffff0) != 0xa8000000)  // Xirrus
#define CT_SIGN_ct                                                  (*(u32 *)"=NCT")
#define CT_SIGN_protocol                                            (*(u32 *)"=PRO")
#define CT_SIGN_proto_stack                                         (*(u32 *)"=STK")
#define CT_SIGN_dscp                                                (*(u32 *)"=DSC")
#define CT_SIGN_nfmark                                              (*(u32 *)"=NFM")
#define CT_SIGN_limit                                               (*(u32 *)"=LIM")

#define conntrack_is_valid(ct)                                      ({ /* lot's of sanity checks/error checking to make sure we don't screw with something other than a conntrack entry */                                              \
    int ret = 1;                                                                                                                                                                                                                        \
         if ((ct) == NULL                                          ) { printk(KERN_ERR "%s: hey! ct is null!"                       "\n", __func__                                                                        ); ret = 0; } \
    else if (stupid_pointer_check(ct)                              ) { printk(KERN_ERR "%s: hey! invalid ct pointer: %p"            "\n", __func__, (ct)                                                                  ); ret = 0; } \
    else if (atomic_read(&(ct)->ct_general.use) < 0                ) { printk(KERN_ERR "%s: hey! invalid use=%d, ct signature: 0x%08x\n", __func__, atomic_read(&(ct)->ct_general.use), atomic32_get((ct)->ct_signature  )); ret = 0; } \
    else if (atomic32_get((ct)->ct_signature  ) != CT_SIGN_ct      ) { printk(KERN_ERR "%s: hey! invalid ct signature: 0x%08x, use=%d\n", __func__, atomic32_get((ct)->ct_signature  ), atomic_read(&(ct)->ct_general.use)); ret = 0; } \
    else if (atomic32_get((ct)->status        ) > (IPS_HELPER << 1)) { printk(KERN_ERR "%s: hey! invalid ct status: " "0x%08x, use=%d\n", __func__, atomic32_get((ct)->status        ), atomic_read(&(ct)->ct_general.use)); ret = 0; } \
    ret;                                                                                                                                                                                                                                \
})
#define conntrack_is_used(ct)                                       ({ /* only use entries that have a use count and some sort of status, but not dying or untracked (only complain about no use count, though, the others seem fine) */    \
    int ret = 1;                                                                                                                                                                                                                            \
         if (atomic_read(&(ct)->ct_general.use) <= 0               ) {   printk(KERN_ERR "%s: hey! invalid use=%d, ct signature: 0x%08x""\n", __func__, atomic_read(&(ct)->ct_general.use), atomic32_get((ct)->ct_signature));   ret = 0; } \
    else if (atomic32_get((ct)->status        ) == 0               ) { /*printk(KERN_ERR "%s: hey! null ct status, ct signature: 0x%08x""\n", __func__,                                     atomic32_get((ct)->ct_signature));*/ ret = 0; } \
    else if (test_bit(IPS_DYING_BIT, &(ct)->status )               ) { /*printk(KERN_ERR "%s: hey! invalid ct status: 0x%08x, dying=%d" "\n", __func__, atomic32_get((ct)->status),   test_bit(IPS_DYING_BIT, &(ct)->status));*/ ret = 0; } \
    else if (test_bit(IPS_UNTRACKED, &(ct)->status )               ) { /*printk(KERN_ERR "%s: hey! invalid ct status: 0x%08x, track=%d" "\n", __func__, atomic32_get((ct)->status),   test_bit(IPS_UNTRACKED, &(ct)->status));*/ ret = 0; } \
    ret;                                                                                                                                                                                                                                    \
})
#define ct_data_is_valid(ct, data, mask)                            ({                                                                                                                                                                                      \
    int ret = 1;                                                                                                                                                                                                                                            \
         if ( atomic32_get((ct)->data##_signature) != CT_SIGN_##data) { printk(KERN_ERR "%s: hey! invalid " #data " signature: 0x%08x (" #data ": 0x%08x)""\n", __func__, atomic32_get((ct)->data##_signature), atomic32_get((ct)->data)      ); ret = 0; } \
    else if ((atomic32_get((ct)->data) & ~mask   ) != 0             ) { printk(KERN_ERR "%s: hey! invalid " #data           ": 0x%08x (" "mask"": 0x%08x)""\n", __func__,                                       atomic32_get((ct)->data), mask); ret = 0; } \
    ret;                                                                                                                                                                                                                                                    \
})
#define limit_is_valid(limit_ptr)                                   (atomic32_get((limit_ptr)->signature) == CT_SIGN_limit )
#define set_limit_signature(limit_ptr)                              (atomic32_set((limit_ptr)->signature,    CT_SIGN_limit ))
#define set_ct_signature(ct, data)                                  (atomic32_set((ct)->data##_signature,    CT_SIGN_##data))
#define chk_ct_signature(ct, data)                                  (atomic32_get((ct)->data##_signature) == CT_SIGN_##data)
#define clr_ct_signature(ct, data)                                  (atomic32_clr((ct)->data##_signature))
#else
#define conntrack_is_valid(ct)                                      (ct)
#define conntrack_is_used(ct)                                       (atomic_read(&(ct)->ct_general.use) && atomic32_get((ct)->status) && !test_bit(IPS_DYING_BIT, &(ct)->status) && !test_bit(IPS_UNTRACKED, &(ct)->status))
#define ct_data_is_valid(ct, data, mask)                            1
#define limit_is_valid(limit_ptr)                                   (limit_ptr)
#define set_limit_signature(limit_ptr)
#define set_ct_signature(ct, data)
#define chk_ct_signature(ct, data)                                  1
#define clr_ct_signature(ct, data)
#endif

#ifdef  CONFIG_XIRRUS_PARANOIA
#define CONNTRACK_LOCKING
#endif
#ifdef  CONNTRACK_LOCKING
#define conntrack_lock(ct)                                          spin_lock_bh(  &(ct)->lock)
#define conntrack_unlock(ct)                                        spin_unlock_bh(&(ct)->lock)
#else
#define conntrack_lock(ct)
#define conntrack_unlock(ct)
#endif

#ifdef  CONFIG_XIRRUS
extern spinlock_t nf_nat_lock;
#endif

#ifdef  CONFIG_XIRRUS_PARANOIA
#undef  CONNTRACK_COMPREHENSIVE_LOCKING
#endif
#ifdef  CONNTRACK_COMPREHENSIVE_LOCKING
#define conntrack_global_lock()                                   { unsigned long                              ct_flags;   \
                                                                    unsigned long                             nat_flags;   \
                                                                    rcu_read_lock         ();                              \
                                                                    spin_lock_irqsave     (&nf_conntrack_lock, ct_flags);  \
                                                                    spin_lock_irqsave     (&nf_nat_lock,      nat_flags);
#define conntrack_global_unlock()                                   spin_unlock_irqrestore(&nf_nat_lock,      nat_flags);  \
                                                                    spin_unlock_irqrestore(&nf_conntrack_lock, ct_flags);  \
                                                                    rcu_read_unlock       ();                              }
#else
#ifdef  CONFIG_XIRRUS_PARANOIA
#undef  CONNTRACK_GLOBAL_LOCKING
#endif
#ifdef  CONNTRACK_GLOBAL_LOCKING
#define conntrack_global_lock()                                     ({ spin_lock_bh(  &nf_conntrack_lock); spin_lock_bh(  &nf_nat_lock); })
#define conntrack_global_unlock()                                   ({ spin_unlock_bh(&nf_nat_lock); spin_unlock_bh(&nf_conntrack_lock); })
#else
#define conntrack_global_lock()
#define conntrack_global_unlock()
#endif
#endif

/* per conntrack: protocol private data */
union nf_conntrack_proto {
	/* insert conntrack proto private data here */
	struct nf_ct_dccp dccp;
	struct ip_ct_sctp sctp;
	struct ip_ct_tcp tcp;
	struct nf_ct_gre gre;
};

union nf_conntrack_expect_proto {
	/* insert expect proto private data here */
};

#include <linux/types.h>
#include <linux/skbuff.h>
#include <linux/timer.h>

#ifdef CONFIG_NETFILTER_DEBUG
#define NF_CT_ASSERT(x)		WARN_ON(!(x))
#else
#define NF_CT_ASSERT(x)
#endif

struct nf_conntrack_helper;

/* Must be kept in sync with the classes defined by helpers */
#define NF_CT_MAX_EXPECT_CLASSES	4

/* nf_conn feature for connections that have a helper */
struct nf_conn_help {
	/* Helper. if any */
	struct nf_conntrack_helper __rcu *helper;

	struct hlist_head expectations;

	/* Current number of expected connections */
	u8 expecting[NF_CT_MAX_EXPECT_CLASSES];

	/* private helper information. */
	char data[];
};

#include <net/netfilter/ipv4/nf_conntrack_ipv4.h>
#include <net/netfilter/ipv6/nf_conntrack_ipv6.h>

struct nf_conn {
	/* Usage count in here is 1 for hash table/destruct timer, 1 per skb,
           plus 1 for any connection(s) we are `master' for */
	struct nf_conntrack ct_general;

	spinlock_t lock;

	/* XXX should I move this to the tail ? - Y.K */
	/* These are my tuples; original and reply */
	struct nf_conntrack_tuple_hash tuplehash[IP_CT_DIR_MAX];

	/* Have we seen traffic both ways yet? (bitset) */
	unsigned long status;

	/* If we were expected by an expectation, this will be it */
	struct nf_conn *master;

	/* Timer function; drops refcnt when it goes off. */
	struct timer_list timeout;

#if defined(CONFIG_NF_CONNTRACK_MARK)
	u_int32_t mark;
#endif

#ifdef CONFIG_NF_CONNTRACK_SECMARK
	u_int32_t secmark;
#endif

#ifdef CONFIG_NETFILTER_XT_MATCH_NLMARK
	u_int32_t protocol;             /* from netlink mark */
	u_int32_t proto_stack;          /* from netlink mark */
        atomic_t  verdict_outstanding;
#endif
#ifdef CONFIG_NETFILTER_XT_MATCH_CONNAPPLY
        u_int32_t nfmark;               /* netfilter mark */
        u_int32_t nfmask;               /* netfilter mask */
        u_int32_t dscp;                 /* dscp value     */
#endif
#ifdef CONFIG_NETFILTER_XT_TARGET_TRAFFIC
        struct nf_limit limit_conn_pps ; /* pps  limit for an individual connection */
        struct nf_limit limit_conn_kbps; /* kbps limit for an individual connection */
        struct nf_limit *limit_all_pps ; /* pps  limit for all connections          */
        struct nf_limit *limit_all_kbps; /* kbps limit for all connections          */
#endif
#ifdef  CONNTRACK_EXTRA_CHECKS
        u_int32_t ct_signature;
        u_int32_t protocol_signature;
        u_int32_t proto_stack_signature;
        u_int32_t nfmark_signature;
        u_int32_t dscp_signature;
#endif
	/* Extensions */
#ifdef CONFIG_XIRRUS_NF_NAT_FIXED
        struct nf_conn_nat nat;
#endif
        struct nf_ct_ext *ext;
#ifdef CONFIG_NET_NS
	struct net *ct_net;
#endif

	/* Storage reserved for other modules, must be the last member */
	union nf_conntrack_proto proto;
};

static inline struct nf_conn *
nf_ct_tuplehash_to_ctrack(const struct nf_conntrack_tuple_hash *hash)
{
	return container_of(hash, struct nf_conn,
			    tuplehash[hash->tuple.dst.dir]);
}

static inline u_int16_t nf_ct_l3num(const struct nf_conn *ct)
{
	return ct->tuplehash[IP_CT_DIR_ORIGINAL].tuple.src.l3num;
}

static inline u_int8_t nf_ct_protonum(const struct nf_conn *ct)
{
	return ct->tuplehash[IP_CT_DIR_ORIGINAL].tuple.dst.protonum;
}

#define nf_ct_tuple(ct, dir) (&(ct)->tuplehash[dir].tuple)

/* get master conntrack via master expectation */
#define master_ct(conntr) (conntr->master)

extern struct net init_net;

static inline struct net *nf_ct_net(const struct nf_conn *ct)
{
	return read_pnet(&ct->ct_net);
}

/* Alter reply tuple (maybe alter helper). */
extern void
nf_conntrack_alter_reply(struct nf_conn *ct,
			 const struct nf_conntrack_tuple *newreply);

/* Is this tuple taken? (ignoring any belonging to the given
   conntrack). */
extern int
nf_conntrack_tuple_taken(const struct nf_conntrack_tuple *tuple,
			 const struct nf_conn *ignored_conntrack);

/* Return conntrack_info and tuple hash for given skb. */
static inline struct nf_conn *
nf_ct_get(const struct sk_buff *skb, enum ip_conntrack_info *ctinfo)
{
	*ctinfo = skb->nfctinfo;
	return (struct nf_conn *)skb->nfct;
}

/* decrement reference count on a conntrack */
static inline void nf_ct_put(struct nf_conn *ct)
{
	NF_CT_ASSERT(ct);
	nf_conntrack_put(&ct->ct_general);
}

/* Protocol module loading */
extern int nf_ct_l3proto_try_module_get(unsigned short l3proto);
extern void nf_ct_l3proto_module_put(unsigned short l3proto);

/*
 * Allocate a hashtable of hlist_head (if nulls == 0),
 * or hlist_nulls_head (if nulls == 1)
 */
extern void *nf_ct_alloc_hashtable(unsigned int *sizep, int nulls);

extern void nf_ct_free_hashtable(void *hash, unsigned int size);

extern struct nf_conntrack_tuple_hash *
__nf_conntrack_find(struct net *net, u16 zone,
		    const struct nf_conntrack_tuple *tuple);

extern int nf_conntrack_hash_check_insert(struct nf_conn *ct);
extern void nf_ct_delete_from_lists(struct nf_conn *ct);
extern void nf_ct_dying_timeout(struct nf_conn *ct);

extern void nf_conntrack_flush_report(struct net *net, u32 portid, int report);

extern bool nf_ct_get_tuplepr(const struct sk_buff *skb,
			      unsigned int nhoff, u_int16_t l3num,
			      struct nf_conntrack_tuple *tuple);
extern bool nf_ct_invert_tuplepr(struct nf_conntrack_tuple *inverse,
				 const struct nf_conntrack_tuple *orig);

extern void __nf_ct_refresh_acct(struct nf_conn *ct,
				 enum ip_conntrack_info ctinfo,
				 const struct sk_buff *skb,
				 unsigned long extra_jiffies,
				 int do_acct);

/* Refresh conntrack for this many jiffies and do accounting */
static inline void nf_ct_refresh_acct(struct nf_conn *ct,
				      enum ip_conntrack_info ctinfo,
				      const struct sk_buff *skb,
				      unsigned long extra_jiffies)
{
	__nf_ct_refresh_acct(ct, ctinfo, skb, extra_jiffies, 1);
}

/* Refresh conntrack for this many jiffies */
static inline void nf_ct_refresh(struct nf_conn *ct,
				 const struct sk_buff *skb,
				 unsigned long extra_jiffies)
{
	__nf_ct_refresh_acct(ct, 0, skb, extra_jiffies, 0);
}

extern bool __nf_ct_kill_acct(struct nf_conn *ct,
			      enum ip_conntrack_info ctinfo,
			      const struct sk_buff *skb,
			      int do_acct);

/* kill conntrack and do accounting */
static inline bool nf_ct_kill_acct(struct nf_conn *ct,
				   enum ip_conntrack_info ctinfo,
				   const struct sk_buff *skb)
{
	return __nf_ct_kill_acct(ct, ctinfo, skb, 1);
}

/* kill conntrack without accounting */
static inline bool nf_ct_kill(struct nf_conn *ct)
{
	return __nf_ct_kill_acct(ct, 0, NULL, 0);
}

/* These are for NAT.  Icky. */
#ifdef CONFIG_XIRRUS_NF_NAT_FIXED
extern s16 (*nf_ct_nat_offset)(struct nf_conn *ct,
			       enum ip_conntrack_dir dir,
			       u32 seq);
#else
extern s16 (*nf_ct_nat_offset)(const struct nf_conn *ct,
			       enum ip_conntrack_dir dir,
			       u32 seq);
#endif
/* Fake conntrack entry for untracked connections */
DECLARE_PER_CPU(struct nf_conn, nf_conntrack_untracked);
static inline struct nf_conn *nf_ct_untracked_get(void)
{
	return &__raw_get_cpu_var(nf_conntrack_untracked);
}
extern void nf_ct_untracked_status_or(unsigned long bits);

/* Iterate over all conntracks: if iter returns true, it's deleted. */
extern void
nf_ct_iterate_cleanup(struct net *net, int (*iter)(struct nf_conn *i, void *data), void *data);
extern void nf_conntrack_free(struct nf_conn *ct);
extern struct nf_conn *
nf_conntrack_alloc(struct net *net, u16 zone,
		   const struct nf_conntrack_tuple *orig,
		   const struct nf_conntrack_tuple *repl,
		   gfp_t gfp);

static inline int nf_ct_is_template(const struct nf_conn *ct)
{
	return test_bit(IPS_TEMPLATE_BIT, &ct->status);
}

/* It's confirmed if it is, or has been in the hash table. */
static inline int nf_ct_is_confirmed(struct nf_conn *ct)
{
	return test_bit(IPS_CONFIRMED_BIT, &ct->status);
}

static inline int nf_ct_is_dying(struct nf_conn *ct)
{
	return test_bit(IPS_DYING_BIT, &ct->status);
}

static inline int nf_ct_is_untracked(const struct nf_conn *ct)
{
	return test_bit(IPS_UNTRACKED_BIT, &ct->status);
}

/* Packet is received from loopback */
static inline bool nf_is_loopback_packet(const struct sk_buff *skb)
{
	return skb->dev && skb->skb_iif && skb->dev->flags & IFF_LOOPBACK;
}

struct kernel_param;

extern int nf_conntrack_set_hashsize(const char *val, struct kernel_param *kp);
extern unsigned int nf_conntrack_htable_size;
extern unsigned int nf_conntrack_max;
extern unsigned int nf_conntrack_hash_rnd;
void init_nf_conntrack_hash_rnd(void);

#define NF_CT_STAT_INC(net, count)	  __this_cpu_inc((net)->ct.stat->count)
#define NF_CT_STAT_INC_ATOMIC(net, count)   this_cpu_inc((net)->ct.stat->count)

#define MODULE_ALIAS_NFCT_HELPER(helper) \
        MODULE_ALIAS("nfct-helper-" helper)

#endif /* _NF_CONNTRACK_H */
