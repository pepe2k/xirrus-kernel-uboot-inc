#ifndef _XT_NLMARK_H
#define _XT_NLMARK_H

#include <linux/types.h>

struct xt_nlmark_mtinfo1 {
	__u64 mark, mask;
	__u8 invert;
};

#endif /*_XT_NLMARK_H*/
