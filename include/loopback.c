/************************************************************************************************************************
 * Radio Loopback Test Routines                                                                                         *
 ************************************************************************************************************************/
/* -------------- Defines for OOPME -------------------- */
#if defined(XAP_ME)

#define printf(fmt, args...)        ({fprintf(lb_file, fmt, ##args); fflush(lb_file);})

#include "../iap-mac/src/pda.h"
#include "../iap-mac/src/radio.h"
//#include "../iap-mac/src/radio_tables.h"     // we get this from autochannel
#include "../iap-mac/src/wl64040.h"


#define radio_mac_reset(n)
#define radio_quick_rst()
#define radio_rx_setup(e,l,f,b)
#define build_pda_table()
#define check_debug_flags()
#define disp_header(s)              printf(s)
#define disp_cleanup()              printf("\n")
#define simple_strtoul(p,ep,b)      strtoul(p, ep, b)
#define simple_strtol(p,ep,b)       strtol(p, ep, b)
#define radio_pcl_adjust(r,p)       1
#define radio_init(p)               Loopback.radioInit(p)
#define radio_rx_check()            Loopback.radioRxCheck()
#define radio_tx(rp,r,l,p,m)        Loopback.radioTx(rp, r, l, p, m)
#define avg_samples(s,c)            (*s)
#define getenv(s)                   "132"
#define floyd_rx_buffers            NULL

#define set_timeout(t)             ((iSampleTime = t), 0)
#define has_expired(t)             (t++)
#define tstc()                      0
#define _getc()

#define WATCHDOG_RESET()

#undef  CFG_WL64040_DEBUG

pda_master_t pda_table[16];
pcl_master_t pcl_table[16];

typedef struct {
    char *usage;
} cmd_tbl_t;

/* -------------- Defines for U-Boot and Utilities -------------------- */
#else
#define sleep(n)    udelay(n*1000)
#define _getc()     getc()

extern int  radio_tx(radio_parm_t *rp, int bit_rate, int length, int period, int mode);
extern int  radio_pcl_adjust(radio_parm_t *rp, pcl_master_t *pcl);
extern void check_debug_flags(void);
extern void disp_header(char *header);
extern void disp_cleanup(void);

extern char *floyd_rx_buffers[FLOYD_RX_STATUS_QUEUE_DEPTH];

#endif

/* -------------- Defines for Loopback Tests -------------------- */
#define VIEW_POWER      bit_const(0)
#define VIEW_ISOLATION  bit_const(1)
#define VIEW_SNR        bit_const(2)
#define VIEW_PACKETS    bit_const(3)
#define VIEW_ERRORS     bit_const(4)

#define SCAN_RADIOS     0
#define SCAN_CHANNELS   1

#define LOOP_RADIOS     2
#define LOOP_CHANNELS   3
#define SCAN_CHAINS     4

static const char *chan_hdr[]    = {  "     |",
                                      "Chan |",
                                      "-----|"
};
static const char *radio_hdr[]   = {  "     |",
                                      "Radio|",
                                      "-----|"
};
static const char *power_hdr[]   = {  "    |",
                                      "Pwr |",
                                      "----|"
};
static const char *iso_16a_hdr[] = {  "============ Isolation ============|",
                                      " 0  1  3  4  5  7  8  9 11 12 13 15|",
                                      "-- -- -- -- -- -- -- -- -- -- -- --|"
};
static const char *snr_16a_hdr[] = {  "=============== SNR ===============|",
                                      " 0  1  3  4  5  7  8  9 11 12 13 15|",
                                      "-- -- -- -- -- -- -- -- -- -- -- --|"
};
static const char *err_16a_hdr[] = {  "============ Errors (%) ===========|",
                                      " 0  1  3  4  5  7  8  9 11 12 13 15|",
                                      "-- -- -- -- -- -- -- -- -- -- -- --|"
};
static const char *pkt_16a_hdr[] = {  "========================= Packets =========================|",
                                      "  0    1    3    4    5    7    8    9   11   12   13   15 |",
                                      "---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----|"
};
static const char *iso_12a_hdr[] = {  "====== Isolation ======|",
                                      " 1  3  5  7  9 11 13 15|",
                                      "-- -- -- -- -- -- -- --|"
};
static const char *snr_12a_hdr[] = {  "========= SNR =========|",
                                      " 1  3  5  7  9 11 13 15|",
                                      "-- -- -- -- -- -- -- --|"
};
static const char *err_12a_hdr[] = {  "===== Errors (%) ======|",
                                      " 1  3  5  7  9 11 13 15|",
                                      "-- -- -- -- -- -- -- --|"
};
static const char *pkt_12a_hdr[] = {  "=============== Packets ===============|",
                                      "  1    3    5    7    9   11   13   15 |",
                                      "---- ---- ---- ---- ---- ---- ---- ----|"
};
static const char *iso_8a_hdr[] = {   "====== Isolation ======|",
                                      " 0  2  4  6  8 10 12 14|",
                                      "-- -- -- -- -- -- -- --|"
};
static const char *snr_8a_hdr[] = {   "========= SNR =========|",
                                     " 0  2  4  6  8 10 12 14|",
                                     "-- -- -- -- -- -- -- --|"
};
static const char *err_8a_hdr[] = {   "===== Errors (%) ======|",
                                      " 0  2  4  6  8 10 12 14|",
                                      "-- -- -- -- -- -- -- --|"
};
static const char *pkt_8a_hdr[] = {   "=============== Packets ===============|",
                                      "  0    2    4    6    8   10   12   14 |",
                                      "---- ---- ---- ---- ---- ---- ---- ----|"
};
static const char *iso_4abg_hdr[] = { "=Isolation=|",
                                      " 2  6 10 14|",
                                      "-- -- -- --|"
};
static const char *snr_4abg_hdr[] = { "=== SNR ===|",
                                      " 2  6 10 14|",
                                      "-- -- -- --|"
};
static const char *err_4abg_hdr[] = { "= Error % =|",
                                      " 2  6 10 14|",
                                      "-- -- -- --|"
};
static const char *pkt_4abg_hdr[] = { "===== Packets =====|",
                                "  2    6   10   14 |",
                                "---- ---- ---- ----|"
};

static const char **lpbk_16a_hdrs[]  = { iso_16a_hdr,  snr_16a_hdr,  pkt_16a_hdr,  err_16a_hdr  };
static const char **lpbk_12a_hdrs[]  = { iso_12a_hdr,  snr_12a_hdr,  pkt_12a_hdr,  err_12a_hdr  };
static const char **lpbk_8a_hdrs[]   = { iso_8a_hdr,   snr_8a_hdr,   pkt_8a_hdr,   err_8a_hdr   };
static const char **lpbk_4abg_hdrs[] = { iso_4abg_hdr, snr_4abg_hdr, pkt_4abg_hdr, err_4abg_hdr };


void disp_loopback_header(const char **first_column, const char ***column_hdrs, int view)
{
        int i;

        printf("\n");
        for (i = 0; i < 3; i++) {
                printf(first_column[i]);
                if (view & VIEW_POWER)     printf(power_hdr[i]);
                if (view & VIEW_ISOLATION) printf(column_hdrs[0][i]);
                if (view & VIEW_SNR)       printf(column_hdrs[1][i]);
                if (view & VIEW_PACKETS)   printf(column_hdrs[2][i]);
                if (view & VIEW_ERRORS)    printf(column_hdrs[3][i]);
                printf("\n");
        }
}

void disp_loopback_data(int num_radios, u8 *radio_numbers, int view, int tx_power, int threshold)
{
        int i;

        if (view & VIEW_ISOLATION) {
                for (i = 0; i < num_radios; i++) {
                        int rssi = avg_samples(rx_stats[radio_numbers[i]].rssi, rx_stats[radio_numbers[i]].avg_count);
                        int isolation = tx_power - rssi;
                        printf(rssi ? "%2d " : " - ", (isolation > 99) ? 99 : isolation);
                }
                printf("\b|");
        }
        if (view & VIEW_SNR) {
                for (i = 0; i < num_radios; i++) {
                        int snr = avg_samples(rx_stats[radio_numbers[i]].snr, rx_stats[radio_numbers[i]].avg_count);
                        printf(snr ? "%2d " : " - ", snr);
                }
                printf("\b|");
        }
        if (view & VIEW_PACKETS) {
                for (i = 0; i < num_radios; i++)
                        printf(rx_stats[radio_numbers[i]].packets ? "%4d " : "   - ", rx_stats[radio_numbers[i]].packets);
                printf("\b|");
        }
        if (view & VIEW_ERRORS) {
                for (i = 0; i < num_radios; i++) {
                        int crc_rate = (rx_stats[radio_numbers[i]].crc_errors * 99) /
                                       (rx_stats[radio_numbers[i]].crc_errors + rx_stats[radio_numbers[i]].packets);
                        printf(crc_rate > threshold ? "%2d " : " - ", crc_rate);
                }
                printf("\b|");
        }
        printf("\n");
}

void loopback_radios(int channel, int band, int sample, int length, int period, int bit_rate, int power, int view, int threshold, int full_rst, int tx_chains, int rx_chains)
{
        unsigned long long timer;
        const char ***column_hdrs;
        u8 *radio_numbers;
        int num_radios;
        int i, j;

        if (band == 2) {
                num_radios    = num_11abg_radios;
                radio_numbers = dot11abg_radios;
                column_hdrs   = lpbk_4abg_hdrs;
        }
        else {
                num_radios    = (num_all_radios >= 12) ? num_11a_only_radios : num_11a_radios;
                radio_numbers = (num_all_radios >= 12) ? dot11a_only_radios  : dot11a_radios;
                column_hdrs   = (num_all_radios == 16) ? lpbk_16a_hdrs       :
                                (num_all_radios == 12) ? lpbk_12a_hdrs       :
                                (num_all_radios ==  8) ? lpbk_8a_hdrs        :
                                                         lpbk_4abg_hdrs      ;
        }

        printf("\nChannel:%4d (%4d MHz)  Sample Time:%4d ms    Error Threshold:%5d%%",     channel, CHAN2FREQ(channel), sample, threshold);
        printf("\nLength :%4d bytes       Bit Rate   :%4d Mbps  Transmit Period:%5d %s\n", length,  bit_rate,
               (period < 16000) ? period : period/1000, (period < 16000) ? "us"   : "ms");

        if (tx_chains) {
                int bChain = tx_chains;
                int iChain = 0;
                printf("Chains :  ");
                while (bChain) {
                        if (bChain & 1) {
                                printf(" %1d", iChain);
                        }
                        bChain >>= 1;
                        ++iChain;
                }
                bChain = rx_chains;
                iChain = 0;
                printf(" --> ");
                while (bChain) {
                        if (bChain & 1) {
                                printf(" %1d", iChain);
                        }
                        bChain >>= 1;
                        ++iChain;
                }
                printf("\n");
        }

        disp_loopback_header(radio_hdr, column_hdrs, view);
        build_pda_table();
        if (!full_rst)
                radio_mac_reset(0);
        for (i = 0; i < num_radios; i++) {
                if (!pda_table[radio_numbers[i]].begin.code)
                        continue;
                if (full_rst)
                        radio_mac_reset(0);
                else
                        radio_quick_rst();
                for (j = 0; j < num_radios; j++) {
                        radio_parms_tbl[radio_numbers[j]].channel =  channel;
                        radio_parms_tbl[radio_numbers[j]].band    = (channel <= 14) ? 2 : 5;
                        if (power > TX0) radio_parms_tbl[radio_numbers[j]].tx_power = power;
                        radio_parms_tbl[radio_numbers[j]].tx_rlb_chain = tx_chains;
                        radio_parms_tbl[radio_numbers[j]].rx_rlb_chain = rx_chains;
                        radio_init(&radio_parms_tbl[radio_numbers[j]]);
                }
                sleep(1);
                radio_tx(&radio_parms_tbl[radio_numbers[i]], bit_rate, length, period, TX_NORMAL);
                timer = set_timeout(100000);
                while (!tstc() && !has_expired(timer)) {
                        if (radio_pcl_adjust(&radio_parms_tbl[radio_numbers[i]], &pcl_table[radio_numbers[i]]) &&
                            pcl_table[radio_numbers[i]].meas_tx_pout == pcl_table[radio_numbers[i]].last_tx_pout)
                                break;
                }
                radio_rx_setup(0xffff, 0, (ACCEPT_ALL_NON_ERR | ACCEPT_SW_ONLY_BSSID), floyd_rx_buffers);
                timer = set_timeout(sample * 1000);
                while (!tstc() && !has_expired(timer))
                        radio_rx_check();
                if (tstc()) {
                        _getc();
                        return;
                }
                printf(" %3d |", radio_numbers[i]);
                if (view & VIEW_POWER) {
                        printf((pcl_table[radio_numbers[i]].meas_tx_limit > 0) ? "max |" :
                               (pcl_table[radio_numbers[i]].meas_tx_limit < 0) ? "min |" :
                               (pcl_table[radio_numbers[i]].meas_tx_pout == 0) ? " -  |" : "%3d |", pcl_table[radio_numbers[i]].meas_tx_pout);
                }
                disp_loopback_data(num_radios, radio_numbers, view, pcl_table[radio_numbers[i]].meas_tx_pout, threshold);
        }
        radio_mac_reset(0);
        printf("\n");
}

void loopback_channels(int radio, int band, int sample, int length, int period, int bit_rate, int power, int view, int threshold, int full_rst)
{
        unsigned long long timer;
        const char ***column_hdrs;
        u8 *radio_numbers;
        u8 *channel_numbers;
        int num_radios;
        int num_channels;
        int i, j;

        if (!pda_table[radio].begin.code)
                return;

        if (band == 2) {
                num_radios      = num_11abg_radios;
                radio_numbers   = dot11abg_radios;
                column_hdrs     = lpbk_4abg_hdrs;
                num_channels    = num_11bg_channels;
                channel_numbers = dot11bg_channels;

        }
        else {
                num_radios      = (num_all_radios >= 12) ? num_11a_only_radios : num_11a_radios;
                radio_numbers   = (num_all_radios >= 12) ? dot11a_only_radios  : dot11a_radios;
                column_hdrs     = (num_all_radios == 16) ? lpbk_16a_hdrs       :
                                  (num_all_radios == 12) ? lpbk_12a_hdrs       :
                                  (num_all_radios ==  8) ? lpbk_8a_hdrs        :
                                                           lpbk_4abg_hdrs      ;
                num_channels    =  num_11a_channels;
                channel_numbers =  dot11a_channels;
        }

        printf("\nRadio :%4d        Sample Time:%4d ms    Error Threshold:%5d%%",    radio,  sample,  threshold);
        printf("\nLength:%4d bytes  Bit Rate   :%4d Mbps  Transmit Period:%5d %s\n", length, bit_rate,
               (period < 16000) ? period : period/1000, (period < 16000) ? "us"   : "ms");

        disp_loopback_header(chan_hdr, column_hdrs, view);
        build_pda_table();
        if (!full_rst)
                radio_mac_reset(0);
        for (i = 0; i < num_channels; i++) {
                if (full_rst)
                        radio_mac_reset(0);
                else
                        radio_quick_rst();
                for (j = 0; j < num_radios; j++) {
                        radio_parms_tbl[radio_numbers[j]].channel =  channel_numbers[i];
                        radio_parms_tbl[radio_numbers[j]].band    = (channel_numbers[i] <= 14) ? 2 : 5;
                        if (power > TX0) radio_parms_tbl[radio_numbers[j]].tx_power = power;
                        radio_init(&radio_parms_tbl[radio_numbers[j]]);
                }
                radio_tx(&radio_parms_tbl[radio], bit_rate, length, period, TX_NORMAL);
                timer = set_timeout(100000);
                while (!tstc() && !has_expired(timer)) {
                        if (radio_pcl_adjust(&radio_parms_tbl[radio], &pcl_table[radio]) &&
                            pcl_table[radio].meas_tx_pout == pcl_table[radio].last_tx_pout)
                                break;
                }
                radio_rx_setup(0xffff, 0, (ACCEPT_ALL_NON_ERR | ACCEPT_SW_ONLY_BSSID), floyd_rx_buffers);
                timer = set_timeout(sample * 1000);
                while (!tstc() && !has_expired(timer))
                        radio_rx_check();
                if (tstc()) {
                        _getc();
                        break;
                }
                printf(" %3d |", channel_numbers[i]);
                if (view & VIEW_POWER) {
                        printf((pcl_table[radio].meas_tx_limit > 0) ? "max |" :
                               (pcl_table[radio].meas_tx_limit < 0) ? "min |" :
                               (pcl_table[radio].meas_tx_pout == 0) ? " -  |" : "%3d |", pcl_table[radio].meas_tx_pout);
                }
                disp_loopback_data(num_radios, radio_numbers, view, pcl_table[radio].meas_tx_pout, threshold);
        }
        radio_mac_reset(0);
        printf("\n");
}

/* Radio Loopback
 *
 * Syntax:
 *  rlb [channel n] [length n] [period n] [rate n] [radio n] [sample n]
 *      [scan radios | channels] [view errs | isolation | packets | snr]
 */
int do_radio_loopback ( cmd_tbl_t *cmdtp, int flag, int argc, const char *argv[])
{
        int channel   = 36;
        int power     = TX0;
        int length    = 1500;
        int period    = 1000;
        int bit_rate  = 54;
        int radio     = 0;
        int sample    = 250;
        int scan      = 0;
        int view      = 0;
        int loop      = 0;
        int threshold = 10;
        int full_rst  = 0;
        int console_width;
        const char *s;
        int i;
        int tx_chains    = 0;   // default all chains
        int rx_chains    = 0;   // default all chains

        check_debug_flags();

        if (argc > 24) {
                if (cmdtp)
                        printf ("Usage:\n%s\n", cmdtp->usage);
                return 1;
        }
        while (--argc > 0) {
                if (--argc <= 0) {
                        printf("Missing specification for parameter: %s\n", *++argv);
                        return(1);
                }
                switch (**++argv) {
                case 'c': channel   = simple_strtoul(*++argv, NULL, 10);                        break;
                case 't': threshold = simple_strtoul(*++argv, NULL, 10);                        break;

                case 'l': switch (*(*argv + 1)) {
                          case 'e': length  = simple_strtoul(*++argv, NULL, 10);                break;
                          case 'o': switch (**++argv) {
                                    case 'r' : loop = LOOP_RADIOS;                              break;
                                    case 'c' : loop = LOOP_CHANNELS;                            break;
                                    default  : printf("Unrecognized loop mode: %s\n", *argv);   return(1);
                          }                                                                     break;
                          default : printf("Unrecognized parameter: %s\n", *argv);              return(1);
                }                                                                               break;
                case 'p': switch (*(*argv + 1)) {
                          case 'e': period = simple_strtol (*++argv, NULL, 10);                 break;
                          case 'o': power  = simple_strtol (*++argv, NULL, 10);;                break;
                          default : printf("Unrecognized parameter: %s\n", *argv);              return(1);
                }                                                                               break;
                case 'r': switch (*(*argv + 2)) {
                          case 's': full_rst = (**++argv == 'f');                               break;
                          case 't': bit_rate = simple_strtoul(*++argv, NULL, 10);               break;
                          case 'd': radio    = simple_strtoul(*++argv, NULL, 10);               break;
                          default : printf("Unrecognized parameter: %s\n", *argv);              return(1);
                }                                                                               break;
                case 's': switch (*(*argv + 1)) {
                          case 'c': switch (**++argv) {
                                    case 'r' : scan = SCAN_RADIOS;                              break;
                                    case 'c' : scan = SCAN_CHANNELS;                            break;
                                    case 'h' :
                        scan = SCAN_CHAINS;
                    switch (*(*argv + 1)) {
                      case '1' : tx_chains = 1;             break;
                      case '2' : tx_chains = 2;             break;
                      case '3' : tx_chains = 3;             break;
                      case '4' : tx_chains = 4;             break;
                      case '5' : tx_chains = 5;             break;
                      case '6' : tx_chains = 6;             break;
                      case '7' : tx_chains = 7;             break;
                      case ' ' :                        break;
                      case '\n' :                       break;
                      case '\0' :                       break;
                      default :
                        printf("Unrecognized scan mode tx chain: %s\n", *argv);
                        return(1);
                        }
                        break;
                                    default  : printf("Unrecognized scan mode: %s\n", *argv);   return(1);
                          }                                                                     break;
                          case 'a': sample = simple_strtoul(*++argv, NULL, 10);                 break;
                          default : printf("Unrecognized parameter: %s\n", *argv);              return(1);
                }                                                                               break;
                case 'v': switch (**++argv) {
                          case 'e' : view |= VIEW_ERRORS;                                       break;
                          case 'i' : view |= VIEW_ISOLATION;                                    break;
                          case 'p' : view |= VIEW_PACKETS;                                      break;
                          case 's' : view |= VIEW_SNR;                                          break;
                          default  : printf("Unrecognized view: %s\n", *argv);                  return(1);
                }                                                                               break;
                default : printf("Unrecognized parameter: %s\n", *argv);                        return(1);
                }
        }
        if (length < 28)   length = 28;
        if (length > 2000) length = 2000;

        if (!is_ofdm_rate(bit_rate) && !is_cck_rate(bit_rate)) {
                printf("Invalid bit rate: %d\n", bit_rate);
                return(1);
        }
        period = max(period, ((bytes_2_us(length, bit_rate)+750)/100) * 100);

        console_width = ((s = getenv("consolewidth")) != NULL) ? simple_strtoul(s, NULL, 10) : 80;
        if (!view)
                view = VIEW_ISOLATION | VIEW_ERRORS;
        if (console_width > 80)
                view |= VIEW_POWER;

        disp_header("\n");
        if (loop == LOOP_CHANNELS) {
                for (i = 0; i < num_11a_channels; i++)
                        loopback_radios(dot11a_channels[i],  5, sample, length, period, bit_rate, power, view, threshold, full_rst, tx_chains, rx_chains);
                for (i = 0; i < num_11bg_channels; i++)
                        loopback_radios(dot11bg_channels[i], 2, sample, length, period, bit_rate, power, view, threshold, full_rst, tx_chains, rx_chains);
        }
        else if (loop == LOOP_RADIOS)   {
                for (i = 0; i < num_11a_radios ; i++)
                        loopback_channels(dot11a_radios[i],   5, sample, length, period, bit_rate, power, view, threshold, full_rst);
                for (i = 0; i < num_11abg_radios ; i++)
                        loopback_channels(dot11abg_radios[i], 2, sample, length, period, bit_rate, power, view, threshold, full_rst);
        }
        else if (scan == SCAN_CHANNELS) {
                if (dot11_radio_bands[radio] & 5) loopback_channels(radio, 5, sample, length, period, bit_rate, power, view, threshold, full_rst);
                if (dot11_radio_bands[radio] & 2) loopback_channels(radio, 2, sample, length, period, bit_rate, power, view, threshold, full_rst);
        }
        else if (scan == SCAN_CHAINS) {
                int tx_chain = 1;
        if (tx_chains == 0) {
            tx_chains = 7;  // 0 means use all chains
        }
        rx_chains = 1;  // just receive on chain 0; problems with noise floor otherwise
                for (i = 0; i < 3; ++i) {
            if (tx_chains & tx_chain) {
                            loopback_radios(channel, (channel <= 14) ? 2 : 5, sample, length, period, bit_rate, power, view, threshold, full_rst, tx_chain, rx_chains);
            }
                        tx_chain <<= 1;
                }
        }
        else {
                loopback_radios(channel, (channel <= 14) ? 2 : 5, sample, length, period, bit_rate, power, view, threshold, full_rst, tx_chains, rx_chains);
        }
        disp_cleanup();
        return(1);
}

