//******************************************************************************
/** @file xirsnmp.h
 *
 * <I>Copyright (C) 2010 Xirrus. All Rights Reserved</I>
 *
 * SNMP definitions
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRSNMP_H
#define _XIRSNMP_H


//****************************************************************
// Traps
//----------------------------------------------------------------
struct trap_req
{
    int trap;
    int intvalue;
    char strvalue[MAX_STR];
};

#define SIZE_TRAP_REQ sizeof(struct trap_req)

#define TRAP_ADMIN_LOGIN                                1
#define TRAP_ADMIN_LOGOUT                               2

#define TRAP_STA_ACL_FAIL                               3
#define TRAP_STA_RADIUS_FAIL                            4

#define TRAP_RESET_ARRAY                                5
#define TRAP_REBOOT_ARRAY                               6
//#define TRAP_FILE_UPLOAD                              7   // deprecated
#define TRAP_DHCP_RENEW_FAIL                            8

#define TRAP_ENV_CTRL_OVER_TEMP                         9
#define TRAP_ENV_CTRL_UNDER_TEMP                        10
#define TRAP_ENV_CTRL_OVER_HUMID                        11
#define TRAP_ENV_CTRL_FAN_FAIL                          12

#define TRAP_FILE_UPLOAD_FAIL                           13
#define TRAP_FILE_UPLOAD_GOOD                           14
#define TRAP_FILE_UPGRADE_FAIL                          15
#define TRAP_FILE_UPGRADE_GOOD                          16
#define TRAP_FILE_LOAD_FAIL                             17

#define TRAP_CFG_CHANGE                                 18

#define TRAP_IAP_BEACON_PROBE_FAILURE                   19
#define TRAP_IAP_BEACON_PROBE_FAILURE_PHY_RESET         20
#define TRAP_IAP_BEACON_PROBE_FAILURE_MAC_RESET         21
#define TRAP_IAP_BEACON_PROBE_FAILURE_ARRAY_REBOOT      22

#define TRAP_SNMP_KEEPALIVE                             23

#define TRAP_DOOR_OPENED                                24
#define TRAP_DOOR_CLOSED                                25

#define TRAP_FLASH_PARTITION_CORRUPT                    26
#define TRAP_LICENSE_UPDATE                             27

#define TRAP_FA_ELEMENT_DISCOVERED                      28
#define TRAP_FA_ELEMENT_AGED_OUT                        29
#define TRAP_FA_MGMT_VLAN_MISMATCH                      30

#define TRAP_INVALID_RADIOS                             31

#define TRAP_CFG_CHANGE_FREQ_INTERVAL                   30  // seconds

//****************************************************************
// SNMP Cfg Module Constants
//----------------------------------------------------------------
#define SNMP_CFG_MODULE_ACL             2
#define SNMP_CFG_MODULE_ADMIN           4
#define SNMP_CFG_MODULE_CDP             5
#define SNMP_CFG_MODULE_DATE_TIME       6
#define SNMP_CFG_MODULE_DHCP            8
#define SNMP_CFG_MODULE_DNS             10
#define SNMP_CFG_MODULE_FILTER          11
#define SNMP_CFG_MODULE_INTERFACE       12
#define SNMP_CFG_MODULE_NETWORK_MAP     13
#define SNMP_CFG_MODULE_RADIUS          14
#define SNMP_CFG_MODULE_ROAM_ASSIST     15
#define SNMP_CFG_MODULE_SECURITY        16
#define SNMP_CFG_MODULE_SNMP_AGENT      18
#define SNMP_CFG_MODULE_SSID            20
#define SNMP_CFG_MODULE_STATIONS        22
#define SNMP_CFG_MODULE_STATISTICS      24
#define SNMP_CFG_MODULE_SYSLOG          26
#define SNMP_CFG_MODULE_SYSTEM          28
#define SNMP_CFG_MODULE_TUNNEL          29
#define SNMP_CFG_MODULE_VLAN            30
#define SNMP_CFG_MODULE_CLUSTER         31
#define SNMP_CFG_MODULE_ENV_CTRL        32
#define SNMP_CFG_MODULE_LOCATION        33
#define SNMP_CFG_MODULE_GROUP           34
#define SNMP_CFG_MODULE_MDM             35
#define SNMP_CFG_MODULE_NETFLOW         36
#define SNMP_CFG_MODULE_WIFI_TAG        37
#define SNMP_CFG_MODULE_WPR             38
#define SNMP_CFG_MODULE_OAUTH           39
#define SNMP_CFG_MODULE_PROXY_FWD       40
#define SNMP_CFG_MODULE_PROXY_MGMT      41
#define SNMP_CFG_MODULE_LLDP            42
#define SNMP_CFG_MODULE_DEVICE          43

#define MAX_NUM_SNMP_CFG_MODULES        (43 + 1)

#endif // _XIRSNMP_H
