/* printf_ext.c
 * Extends glibc printf() format strings with new conversion characters
 */

#include <stdio.h>
#include <printf.h>
#include <stdint.h>
#include "xirver.h"
#include "printf_ext.h"

static char low_nibble_hex(uint8_t byte, int uc)
{
    uint8_t nibble = byte & 0xf;

    if (nibble <= 9)
        return ('0' + nibble);
    else if (uc)
        return ('A' + nibble - 10);
    else
        return ('a' + nibble - 10);
}

static char high_nibble_hex(uint8_t byte, int uc)
{
    return (low_nibble_hex(byte >> 4, uc));
}

static int printf_arginfo_M(const struct printf_info *info, size_t n, int *argtypes, int *size)
{
    // "%M" always takes one argument, a pointer to uint8_t[6]

    if (n > 0) {
        argtypes[0] = PA_POINTER;
        size[0] = sizeof(uint8_t *);
    }
    return 1;
}

static int printf_output_M(FILE *stream, const struct printf_info *info, const void *const *args)
{
    const int mac_width = 6 * 2 + 5;
    const uint8_t *arg = *(const uint8_t **)(args[0]);

    int ret       = 0;
    int pad_left  = 0;
    int pad_right = 0;
    int uc        = info->group;

    if (info->width > mac_width) {
        int pad = info->width - mac_width;

        if (info->alt) {
            pad_left  = pad / 2;
            pad_right = pad - pad_left;
        }
        else if (info->left) {
            pad_right = pad;
        }
        else {
            pad_left  = pad;
        }
    }

    while (pad_left) {
        if (putc(' ', stream) == EOF)
            return -1;
        ret++;
        pad_left--;
    }

    int i;
    for (i=0 ; i<6 ; i++) {
        if ((i && putc(':',                   stream) == EOF) ||
            putc(high_nibble_hex(arg[i], uc), stream) == EOF  ||
            putc( low_nibble_hex(arg[i], uc), stream) == EOF    )
        {
            return -1;
        }

        if (i) ret += 3;
        else   ret += 2;
    }

    while (pad_right) {
        if (putc(' ', stream) == EOF)
            return -1;
        ret++;
        pad_right--;
    }

    return ret;
}

static int printf_arginfo_P(const struct printf_info *info, size_t n, int *argtypes, int *size)
{
    // "%P" always takes one argument, a string (char*)

    if (n > 0) {
        argtypes[0] = PA_STRING;
        size[0] = sizeof(char *);
    }
    return 1;
}

static int printf_output_P(FILE *stream, const struct printf_info *info, const void *const *args)
{
    const char *arg = *(const char **)(args[0]);

    int str_width = 0;
    while (arg[str_width])
        str_width++;

    int ret       = 0;
    int pad_left  = 0;
    int pad_right = 0;

    int width = info->width;
    if (str_width > width)
        width = str_width;
    if (info->prec > -1 && width > info->prec)
        width = info->prec;

    if (width > str_width) {
        int pad = info->width - str_width;

        if (info->alt) {
            pad_left  = pad / 2;
            pad_right = pad - pad_left;
        }
        else if (info->left) {
            pad_right = pad;
        }
        else {
            pad_left  = pad;
        }
    }

    while (pad_left) {
        if (putc(' ', stream) == EOF)
            return -1;
        ret++;
        pad_left--;
    }

    int i = 0;
    while (ret + pad_right < width) {
        if (putc(arg[i++], stream) == EOF)
            return -1;
        ret++;
    }

    while (pad_right) {
        if (putc(' ', stream) == EOF)
            return -1;
        ret++;
        pad_right--;
    }

    return ret;
}

int init_printf_ext()
{
    int ret = 0;

    if ( register_printf_specifier('M', printf_output_M, printf_arginfo_M) ) {
        fprintf(stderr, "Error registering printf conversion character 'M'\n");
        ret--;
    }

    if ( register_printf_specifier('P', printf_output_P, printf_arginfo_P) ) {
        fprintf(stderr, "Error registering printf conversion character 'P'\n");
        ret--;
    }

    return ret;
}
