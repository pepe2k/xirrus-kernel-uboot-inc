#ifndef _XIRWPRD_H
#define _XIRWPRD_H

#include "xircfg.h"
#include "xiroid.h"
#include "xiriap.h"
#include "xirmac.h"
#include "cfgdefs.h"

#define MAX_NUM_WPR     (MAX_NUM_SSID + MAX_NUM_GROUP)

#endif // _XIRWPRD_H
