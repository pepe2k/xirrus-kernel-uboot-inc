//******************************************************************************
/** @file xirerr.h
 *
 * config module error definitions
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRERR_H
#define _XIRERR_H

#include "common.h"
#include "xiriap.h"

struct error_cfg
{
       int  code;              // unique error code
       int  legacy_code;       // legacy error code (rev 3.0 and below)
 const char *message;          // error message
};

#define num_cfg_error(err)                              ({struct error_cfg error = err; error.code;    })
#define msg_cfg_error(err)                              ({struct error_cfg error = err; error.message; })

#define set_read_struct_error(s, err)                   ({struct error_cfg error = err; if (s) { if (error.message) { rs_add_str((s),         error.message);          } rs_set_result(s, error.code); } error.code; })
#define set_cfg_struct_error( s, err)                   ({struct error_cfg error = err; if (s) { if (error.message) { strxncpy((s)->strvalue, error.message, MAX_STR); }  (s)->result   = error.code ; } error.code; })
#define set_cfg_error(        r, err)                   ({struct error_cfg error = err; if (r) { if (error.message) { strxncpy((r)->strvalue, error.message, MAX_STR); }  (r)->intvalue = error.code ; } error.code; })

#define set_n_log_read_struct_error(s, err)             ({struct error_cfg error = err; if (s) { if (error.message) { rs_add_str((s),         error.message);          syslog(LOG_ALERT, error.message); } rs_set_result(s, error.code); } error.code; })
#define set_n_log_cfg_struct_error( s, err)             ({struct error_cfg error = err; if (s) { if (error.message) { strxncpy((s)->strvalue, error.message, MAX_STR); syslog(LOG_ALERT, error.message); }  (s)->result   = error.code ; } error.code; })
#define set_n_log_cfg_error(        r, err)             ({struct error_cfg error = err; if (r) { if (error.message) { strxncpy((r)->strvalue, error.message, MAX_STR); syslog(LOG_ALERT, error.message); }  (r)->intvalue = error.code ; } error.code; })

#define set_read_struct_err_args(s, err, args...)       ({struct error_cfg error = err; if (s) { if (error.message) { char strvalue[MAX_STR]; snprintf(     strvalue, MAX_STR, error.message, ##args);  rs_add_str((s), strvalue); } rs_set_result(s, error.code); } error.code; })
#define set_cfg_struct_err_args( s, err, args...)       ({struct error_cfg error = err; if (s) { if (error.message) {                         snprintf((s)->strvalue, MAX_STR, error.message, ##args);                             }  (s)->result   = error.code ; } error.code; })
#define set_cfg_err_args(        r, err, args...)       ({struct error_cfg error = err; if (r) { if (error.message) {                         snprintf((r)->strvalue, MAX_STR, error.message, ##args);                             }  (r)->intvalue = error.code ; } error.code; })

#define set_n_log_read_struct_err_args(s, err, args...) ({struct error_cfg error = err; if (s) { if (error.message) { char strvalue[MAX_STR]; snprintf(     strvalue, MAX_STR, error.message, ##args);  rs_add_str((s), strvalue); syslog(LOG_ALERT,      strvalue); } rs_set_result(s, error.code); } error.code; })
#define set_n_log_cfg_struct_err_args( s, err, args...) ({struct error_cfg error = err; if (s) { if (error.message) {                         snprintf((s)->strvalue, MAX_STR, error.message, ##args);                             syslog(LOG_ALERT, (s)->strvalue); }  (s)->result   = error.code ; } error.code; })
#define set_n_log_cfg_err_args(        r, err, args...) ({struct error_cfg error = err; if (r) { if (error.message) {                         snprintf((r)->strvalue, MAX_STR, error.message, ##args);                             syslog(LOG_ALERT, (r)->strvalue); }  (r)->intvalue = error.code ; } error.code; })

#define copy_cfg_error(r1, r2)                          ({strxncpy((r1)->strvalue, (r2)->strvalue, MAX_STR); (r1)->intvalue = (r2)->intvalue;})
#define get_n_log_cfg_error(r)                          (!(r) ? 0 : ((r)->intvalue < 0 && (r)->strvalue[0]) ? (syslog(LOG_ALERT, (r)->strvalue), (r)->intvalue) : (r)->intvalue)
#define clr_cfg_error(r)                                (!(r) ? 0 : ((r)->intvalue = 0))
#define get_cfg_error(r)                                (!(r) ? 0 : ((r)->intvalue    ))

//---------------------------------------------------------
// MACE macros
//---------------------------------------------------------
#define CFG_ERR(      _err_ )          ( (error_cfg)_err_         )
#define CFG_ERR_MSG(  _err_ )          (((error_cfg)_err_).message)
#define CFG_ERR_CODE( _err_ )          (((error_cfg)_err_).code   )

//  Error codes ranges
// --------------------
//      ADD  : -xx10 -> -xx19
//      DEL  : -xx20 -> -xx29
//      EXEC : -xx30 -> -xx29
//      READ : -xx40 -> -xx39
//      WRITE: -xx50 ->


//      Error                                   Error Codes     Error Message
//      Identifier                              Unique  Legacy  Explanation (optional)                                                         Error ("at carrot")
//      ------------------------------          ------  ------  -----------------------------------------------------------------------------  -----------------------------------------------
//  ACL         (-10xx)
#define ERR_ACL_ADD_TABLE_FULL                 { -1010,   -1,   "Access control list full. "                                                   "Cannot add ACL entry"                         }
#define ERR_ACL_DEL_BAD_ENTRY                  { -1020,    1,   "Access control entry does not exist. "                                        "Cannot delete ACL entry"                      }
#define ERR_ACL_WRITE_OOPME_FAIL               { -1059,   -1,   "General error occurred. "                                                     "Cannot add or edit ACL entry"                 }

// ADMIN        (-11xx)
#define ERR_ADMIN_ADD_TABLE_FULL               { -1110,   -1,   "Admin user table full. "                                                      "Cannot add admin user"                        }
#define ERR_ADMIN_ADD_BAD_USER_SIZE            { -1111,   -5,   "Admin name must be greater than 5 characters long. "                          "Cannot add admin user"                        }
#define ERR_ADMIN_ADD_BAD_USER                 { -1112,   -1,   "Invalid admin user name. "                                                    "Cannot add admin user"                        }
#define ERR_ADMIN_DEL_LAST                     { -1121,   -2,   "At least one admin must be defined. "                                         "Cannot delete admin user"                     }
#define ERR_ADMIN_DEL_LAST_RW                  { -1122,   -3,   "At least one admin with read/write privilege must be defined. "               "Cannot delete admin user"                     }
#define ERR_ADMIN_DEL_BAD_USER                 { -1123,   -4,   "Admin user does not exist. "                                                  "Cannot delete admin user"                     }
#define ERR_ADMIN_EXEC_FAIL                    { -1130,   -1,   "Invalid user name or password. "                                              "Cannot authenticate user"                     }
#define ERR_ADMIN_READ_FAIL                    { -1140,   -1,                                                                                  "Undefined admin user"                         }
#define ERR_ADMIN_WRITE_FILE_FAIL              { -1150,   -1,   "General error occurred. "                                                     "Cannot add or edit admin user"                }
#define ERR_ADMIN_WRITE_BAD_RADIUS_ENB         { -1151,   -1,   "Admin RADIUS server not fully configured. "                                   "Cannot enable admin RADIUS authentication"    }
#define ERR_ADMIN_WRITE_LAST_RW                { -1152,   -3,   "At least one admin with admin read/write privilege must be defined. "         "Cannot change admin user privilege"           }
#define ERR_ADMIN_WRITE_LAST_RW_PRIV           { -1153,   -1,   "At least one admin with admin read/write privilege must be defined. "         "Cannot change admin privilege level"          }
#define ERR_ADMIN_WRITE_BAD_PRIV_NAME          { -1154,   -1,   "Unknown privilege level name. "                                               "Cannot change admin privilege level"          }
#define ERR_ADMIN_WRITE_BAD_SECTION_NAME       { -1155,   -1,   "Unknown privilege section name. "                                             "Cannot change section admin privilege level"  }
#define ERR_ADMIN_WRITE_BAD_AUTH               { -1156,   -1,   "User not authenticated. "                                                     "Cannot make requested change"                 }
#define ERR_ADMIN_WRITE_BAD_PRIV               { -1157,   -1,   "Insufficient user privilege. "                                                "Cannot make requested change"                 }
#define ERR_ADMIN_WRITE_BAD_RADIUS_DIS         { -1158,   -1,   "General error occurred. "                                                     "Cannot disable admin RADIUS authentication"   }
#define ERR_ADMIN_WRITE_BAD_PRIV_LOCKED        { -1155,   -1,   "Privilege section is locked. "                                                "Cannot change section admin privilege level"  }

// AUTOCELL/BAND (-12xx)
#define ERR_AUTOCELL_EXEC_NEG_NO_MONITOR       { -1230,   -1,   "Non-channel specific auto cell sizing requires the RF monitor to be enabled. ""Cannot execute auto cell sizing"              }
#define ERR_AUTOCELL_EXEC_NEG_MONITOR_DOWN     { -1231,   -1,   "Non-channel specific auto cell sizing requires the RF monitor IAP to be up. " "Cannot execute auto cell sizing"              }
#define ERR_AUTOCELL_EXEC_NEG_NO_IAPS          { -1232,   -1,   "No IAP cell sizes set to auto. "                                              "Auto cell configuration not run"              }
#define ERR_AUTOCELL_EXEC_OOPME_FAIL           { -1233,   -1,   "Error executing auto cell. "                                                  "General error occurred"                       }
#define ERR_AUTOBAND_EXEC_NEG_NO_LOG_FILE      { -1239,   -1,   "Failed to create auto band log file. "                                        "Cannot execute auto band assignment"          }
#define ERR_AUTOBAND_EXEC_NEG_NO_MONITOR       { -1240,   -1,   "Auto band assignment requires the RF monitor to be enabled. "                 "Cannot execute auto band assignment"          }
#define ERR_AUTOBAND_EXEC_NEG_MONITOR_DOWN     { -1241,   -1,   "Auto band assignment requires the RF monitor IAP to be up. "                  "Cannot execute auto band assignment"          }
#define ERR_AUTOBAND_EXEC_OOPME_FAIL           { -1242,   -1,   "Error executing auto band. "                                                  "General error occurred"                       }

// AUTOCHAN     (-13xx)
#define ERR_AUTOCHAN_EXEC_RUNNING              { -1330,   -2,   "Auto channel configuration is currently running. "                            "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_NON_WDS_IAPS         { -1331,   -3,   "No Non-WDS IAPs up or unlocked. "                                             "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_NON_WDS_A_IAPS       { -1332,   -3,   "No Non-WDS 802.11a IAPs up or unlocked. "                                     "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_NON_WDS_BG_IAPS      { -1333,   -3,   "No Non-WDS 802.11b/g IAPs up or unlocked. "                                   "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_NO_IAPS              { -1334,   -3,   "No IAPs up or unlocked. "                                                     "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_NO_A_IAPS            { -1335,   -3,   "No 802.11a IAPs up or unlocked. "                                             "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_NO_BG_IAPS           { -1336,   -3,   "No 802.11b/g IAPs up or unlocked. "                                           "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_TOO_FEW_A_CHAN       { -1337,   -3,   "802.11a auto channel list insufficient for all enabled 802.11a IAPs. "        "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_TOO_FEW_BG_CHAN      { -1338,   -3,   "802.11b/g auto channel list insufficient for all enabled 802.11b/g IAPs. "    "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_OOPME_FAIL           { -1339,   -1,   "General error occurred. "                                                     "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_READ_STAT_FAIL            { -1340,   -1,                                                                                  "Unable to read auto channel status"           }
#define ERR_AUTOCHAN_EXEC_THREAD_FAIL          { -1341,   -1,   "General error occurred. "                                                     "Cannot execute auto channel assignment"       }
#define ERR_AUTOCHAN_EXEC_NEG_NO_MONITOR       { -1341,   -1,   "Auto channel negotiation requires the RF monitor to be enabled. "             "Cannot execute auto channel negotiation"      }
#define ERR_AUTOCHAN_EXEC_NEG_MONITOR_DOWN     { -1341,   -1,   "Auto channel negotiation requires the RF monitor IAP to be up. "              "Cannot execute auto channel negotiation"      }

// CLR_STATS    (-14xx)
#define ERR_CLR_STATS_EXEC_BAD_IFACE           { -1430,   -1,   "Error clearing statistics. "                                                  "Invalid interface"                            }
#define ERR_CLR_STATS_EXEC_BAD_STA             { -1431,   -2,   "Error clearing statistics. "                                                  "Station not associated"                       }
#define ERR_CLR_STATS_EXEC_BAD_VLAN            { -1432,   -1,   "Error clearing statistics. "                                                  "Undefined VLAN"                               }
#define ERR_CLR_STATS_EXEC_READ_FAIL           { -1433,   -1,   "Error clearing statistics. "                                                  "Cannot read device"                           }
#define ERR_CLR_STATS_EXEC_OOPME_FAIL          { -1439,   -1,   "Error clearing statistics. "                                                  "General error occurred"                       }

// CONSOLE      (-15xx)
#define ERR_CONSOLE_WRITE_BAD_VALUE            { -1550,   -2,   "Invalid console port setting. "                                               "Cannot change setting"                        }

// DEAUTH       (-16xx)
#define ERR_DEAUTH_EXEC_BAD_STA                { -1639,   -1,   "Error deauthenticating station. "                                             "Unknown station"                              }
#define ERR_ASSIGN_EXEC_BAD_STA                { -1640,   -1,   "Error assigning station to user group. "                                      "Unknown station"                              }

// DESC         (-17xx)
#define ERR_DESC_WRITE_FAIL                    { -1750,   -1,   "General error occurred. "                                                     "Cannot write description"                     }

// DHCP         (-18xx)
#define ERR_DHCP_ADD_TABLE_FULL                { -1810,   -1,   "DHCP pool table full. "                                                       "Cannot add DHCP pool"                         }
#define ERR_DHCP_ADD_TABLE_BAD_NAME            { -1811,   -1,   "Invalid DHCP pool name. "                                                     "Cannot add DHCP pool"                         }
#define ERR_DHCP_ADD_TABLE_DUP_NAME            { -1811,   -1,   "Duplicate DHCP pool name. "                                                   "Cannot add DHCP pool"                         }
#define ERR_DHCP_DEL_BAD_POOL                  { -1820,   -1,   "Undefined DHCP Pool. "                                                        "Cannot delete DHCP pool"                      }
#define ERR_DHCP_DEL_IN_USE_SSID               { -1821,   -2,   "This DHCP Pool is in use by one or more SSIDs. "                              "DHCP Pool for those SSIDs will be cleared"    }
#define ERR_DHCP_DEL_IN_USE_GROUP              { -1822,   -2,   "This DHCP Pool is in use by one or more User Groups. "                        "DHCP Pool for those Groups will be cleared"   }
#define ERR_DHCP_WRITE_BAD_START_EDIT          { -1850,   -2,   "DHCP pool start address must be less than the pool end address. "             "Pool must first be disabled"                  }
#define ERR_DHCP_WRITE_BAD_START_ENB           { -1851,   -2,   "DHCP pool start address must be less than the pool end address. "             "Cannot enable DHCP pool"                      }
#define ERR_DHCP_WRITE_OVERLAP_EDIT            { -1852,   -3,   "DHCP pool address range conflicts with other pools defined. "                 "Pool must first be disabled"                  }
#define ERR_DHCP_WRITE_OVERLAP_ENB             { -1853,   -3,   "DHCP pool address range conflicts with other pools defined. "                 "Cannot enable DHCP pool"                      }
#define ERR_DHCP_WRITE_BAD_MASK_EDIT           { -1854,   -4,   "DHCP pool start and end address must belong to the same subnet. "             "Pool must first be disabled"                  }
#define ERR_DHCP_WRITE_BAD_MASK_ENB            { -1855,   -4,   "DHCP pool start and end address must belong to the same subnet. "             "Cannot enable DHCP pool"                      }
#define ERR_DHCP_WRITE_WPR_OVERLAP_EDIT        { -1856,   -5,   "DHCP pool NAT subnet must not overlap another DHCP Pool or VLAN subnet. "     "Pool must first be disabled"                  }
#define ERR_DHCP_WRITE_WPR_OVERLAP_ENB         { -1857,   -5,   "DHCP pool NAT subnet must not overlap another DHCP Pool or VLAN subnet. "     "Cannot enable DHCP pool"                      }
#define ERR_DHCP_WRITE_WPR_GATEWAY             { -1858,   -2,   "DHCP pool gateway is fixed when Web Page Redirect is enabled. "               "Cannot modify DHCP gateway"                   }
#define ERR_DHCP_WRITE_BAD_WPR_NAT             { -1859,   -8,   "DHCP pool NAT is forced on when Web Page Redirect is enabled. "               "Cannot modify DHCP NAT"                       }
#define ERR_DHCP_WRITE_WPR_OFF_FAIL            { -1860,   -1,   "General error occurred. "                                                     "Error disabling Web Page Redirect"            }
#define ERR_DHCP_WRITE_SERVER_FAIL             { -1861,   -1,   "General error occurred. "                                                     "Cannot restart DHCP server"                   }
#define ERR_DHCP_WRITE_BAD_GATEWAY_EDIT        { -1862,   -2,   "DHCP pool gateway address must not be within the pool. "                      "Pool must first be disabled"                  }
#define ERR_DHCP_WRITE_BAD_GATEWAY_ENB         { -1863,   -2,   "DHCP pool gateway address must not be within the pool. "                      "Cannot enable DHCP pool"                      }
#define ERR_DHCP_WRITE_BAD_GW_SUBNET_EDIT      { -1864,   -2,   "DHCP pool gateway address must belong to the pool subnet. "                   "Pool must first be disabled"                  }
#define ERR_DHCP_WRITE_BAD_GW_SUBNET_ENB       { -1865,   -2,   "DHCP pool gateway address must belong to the pool subnet. "                   "Cannot enable DHCP pool"                      }
#define ERR_DHCP_WRITE_OOPME_FAIL              { -1869,   -1,   "General error occurred. "                                                     "Cannot add or edit DHCP pool"                 }
#define ERR_DHCP_WRITE_OVERLAP_VLAN_EDIT       { -1870,   -3,   "DHCP pool NAT subnet must not overlap VLAN IP subnet. "                       "Cannot add or edit DHCP pool"                 }
#define ERR_DHCP_WRITE_OVERLAP_VLAN_ENB        { -1871,   -3,   "DHCP pool NAT subnet must not overlap VLAN IP subnet. "                       "Cannot enable DHCP pool"                      }
#define ERR_DHCP_WRITE_OVERLAP_ETH_EDIT        { -1872,   -3,   "DHCP pool NAT subnet must not overlap Ethernet interface IP subnet. "         "Cannot add or edit DHCP pool"                 }
#define ERR_DHCP_WRITE_OVERLAP_ETH_ENB         { -1873,   -3,   "DHCP pool NAT subnet must not overlap Ethernet interface IP subnet. "         "Cannot enable DHCP pool"                      }

// DNS          (-19xx)
#define ERR_DNS_WRITE_BAD_HOSTNAME             { -1910,   -1,   "Invalid hostname. "                                                           "Cannot update DNS information"                }
#define ERR_DNS_WRITE_BAD_DOMAIN               { -1920,   -1,   "Invalid domain name. "                                                        "Cannot update DNS information"                }
#define ERR_DNS_WRITE_FILE_FAIL                { -1950,   -1,   "Error writing DNS configuration file. "                                       "Cannot update DNS information"                }
#define ERR_DNS_WRITE_OOPME_FAIL               { -1959,   -1,   "General error occurred. "                                                     "Cannot update DNS information"                }

// ETH          (-20xx)
#define ERR_ETH_WRITE_BAD_ADDR                 { -2050,   -1,   "Invalid IP address. "                                                         "Cannot edit ethernet parameters"              }
#define ERR_ETH_WRITE_BAD_MASK                 { -2051,   -1,   "Invalid IP mask. "                                                            "Cannot edit ethernet parameters"              }
#define ERR_ETH_WRITE_IFACE_BAD                { -2052,   -1,                                                                                  "Invalid ethernet interface"                   }
#define ERR_ETH_WRITE_IFACE_FAIL               { -2053,   -1,   "General error occurred. "                                                     "Cannot access ethernet interface"             }
#define ERR_ETH_WRITE_OVERLAP_NAT              { -2054,   -2,   "Interface subnet must not overlap DHCP pool NAT subnet. "                     "Cannot modify IP address"                     }
#define ERR_ETH_WRITE_OVERLAP_VLAN             { -2055,   -2,   "Interface subnet must not overlap VLAN IP subnet. "                           "Cannot modify subnet mask"                    }
#define ERR_ETH_WRITE_DFLT_RTE_CLR_FAIL        { -2056,   -1,   "General error occurred. "                                                     "Unable to reset default route"                }
#define ERR_ETH_WRITE_DFLT_RTE_SET_FAIL        { -2057,   -1,   "General error occurred. "                                                     "Unable to set default route"                  }
#define ERR_ETH_WRITE_DFLT_RTE_INVALID         { -2058,   -1,   "Default route address is not in a directly connected subnet. "                "Unable to set default route"                  }
#define ERR_ETH_WRITE_OOPME_FAIL               { -2059,   -1,   "General error occurred. "                                                     "Unable to update ethernet settings"           }
#define ERR_ETH_WRITE_IFACE_DISABLED           { -2060,   -1,   "Interface is disabled. "                                                      "Cannot modify interface"                      }
#define ERR_ETH_WRITE_MIRROR_IFACE_BAD         { -2061,   -1,   "Source interface is in mirror mode. "                                         "Cannot mirror a mirror"                       }
#define ERR_ETH_WRITE_BOND_ENABLE_STP          { -2062,   -1,   "Configuration may create a network loop but spanning tree is disabled. "                                                     }
#define ERR_ETH_WRITE_BOND_IFACE_BAD           { -2063,   -1,                                                                                  "Invalid bond interface"                       }

// FILTER & APP LIST      (-21xx)
#define ERR_FILTER_ADD_TABLE_FULL              { -2110,   -2,   "Filter table full. "                                                          "Cannot add filter"                            }
#define ERR_FILTER_ADD_BAD_SRC_SSID            { -2111,   -3,   "Source SSID does not exist. "                                                 "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_DST_SSID            { -2112,   -4,   "Destination SSID does not exist. "                                            "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_SSID_SENSE          { -2113,   -5,   "Source and destination SSID tests must have the same sense. "                 "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_SRC_GROUP           { -2114,   -3,   "Source User Group does not exist. "                                           "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_DST_GROUP           { -2115,   -4,   "Destination User Group does not exist. "                                      "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_GROUP_SENSE         { -2116,   -5,   "Source and destination User Group tests must have the same sense. "           "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_APP_SUPPORT         { -2117,   -1,   "Application filters not supported on this platform. "                         "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_APP_LAYER           { -2118,   -1,   "Application, DSCP, traffic limit, tunnel, or time based filters must be layer 3. Cannot add or edit filter"                  }
#define ERR_FILTER_ADD_BAD_APP_IDENT           { -2119,   -1,   "Application identifier unknown. "                                             "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_SET                 { -2120,   -1,   "Cannot set both DSCP and either QoS or VLAN in the same filter. "             "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_LIMIT_SUPPORT       { -2121,   -1,   "Filters traffic limits not supported on this platform. "                      "Cannot add or edit filter"                    }
#define ERR_FILTER_ADD_BAD_PROTOCOL            { -2122,   -1,   "Source and destination addresses must both be IPv4 or IPv6. "                 "Cannot add or edit filter"                    }
#define ERR_FILTER_DEL_UNDEFINED               { -2125,   -2,   "Undefined filter. "                                                           "Cannot delete filter"                         }
#define ERR_FILTER_EXEC_MOVE_UNDEFINED         { -2130,   -1,   "Filter does not exist. "                                                      "Cannot move filter"                           }
#define ERR_FILTER_READ_FAIL                   { -2140,   -1,                                                                                  "Undefined filter"                             }
#define ERR_FILTER_WRITE_EBTABLES_FAIL         { -2150,   -1,   "General error occurred. "                                                     "Cannot restart layer 2 filters"               }
#define ERR_FILTER_WRITE_IPTABLES_FAIL         { -2151,   -1,   "General error occurred. "                                                     "Cannot restart layer 3 filters"               }
#define ERR_FILTER_WRITE_THREAD_FAIL           { -2152,   -1,   "General error occurred. "                                                     "Cannot restart layer 2/3 filters"             }

#define ERR_APP_ADD_TABLE_FULL                 { -2160,   -2,   "Application list full. "                                                      "Cannot add application"                       }
#define ERR_APP_ADD_BAD_APP_SUPPORT            { -2161,   -1,   "Application lists are not supported on this platform. "                       "Cannot add or edit application"               }
#define ERR_APP_DEL_UNDEFINED                  { -2162,   -2,   "Undefined application. "                                                      "Cannot delete application"                    }
#define ERR_APP_READ_FAIL                      { -2163,   -1,                                                                                  "Undefined application"                        }

// IAPs         (-22xx)
#define ERR_IAP_EXEC_BAD_CMD                   { -2230,   -1,   "Error enabling or disabling IAPs. "                                           "Unknown command"                              }
#define ERR_IAP_EXEC_DUP_CHAN                  { -2231,   -2,   "Duplicate or overlapping channels assigned. "                                 "Not all IAPs enabled"                         }
#define ERR_IAP_EXEC_DUP_A_CHAN                { -2232,   -2,   "Duplicate 802.11a channels assigned. "                                        "Not all 802.11a IAPs enabled"                 }
#define ERR_IAP_EXEC_DUP_BG_CHAN               { -2233,   -2,   "Duplicate or overlapping 802.11b/g channels assigned. "                       "Not all 802.11b/g IAPs enabled"               }
#define ERR_IAP_EXEC_RADAR_CHAN                { -2234,   -3,   "Radar detected on one or more channels. "                                     "Not all IAPs enabled"                         }
#define ERR_IAP_EXEC_RADAR_A_CHAN              { -2235,   -3,   "Radar detected on one or more channels. "                                     "Not all 802.11a IAPs enabled"                 }
#define ERR_IAP_EXEC_RADAR_BG_CHAN             { -2236,   -3,   "Radar detected on one or more channels. "                                     "Not all 802.11b/g IAPs enabled"               }
#define ERR_IAP_EXEC_CAPTURE_IAP_UP            { -2237,   -2,   "An IAP must be down before being put into packet capture mode. "              "Cannot enable capture mode"                   }
#define ERR_IAP_EXEC_CAPTURE_MONITOR           { -2238,   -4,   "Cannot capture on the monitor iap. "                                          "Cannot enable capture mode"                   }
#define ERR_IAP_EXEC_CAPTURE_ALREADY           { -2239,   -3,   "Another packet capture session is already in progress. "                      "Cannot enable capture mode"                   }
#define ERR_IAP_EXEC_CAPTURE_NO_MONITOR        { -2240,   -3,   "There is no monitor radio configured, to capture with. "                      "Cannot execute capture"                       }
#define ERR_IAP_EXEC_CAPTURE_TS_MONITOR        { -2241,   -3,   "Monitor radio must be in 'Dedicated' mode to capture WiFi traffic. "          "Cannot execute capture"                       }
#define ERR_IAP_WRITE_BAD_CHAN                 { -2250,   -4,                                                                                  "Invalid channel number"                       }
#define ERR_IAP_WRITE_BAD_CHAN_BG              { -2251,   -5,   "Cannot assign 802.11b/g channel to 802.11a only IAP. "                        "Invalid channel number"                       }
#define ERR_IAP_WRITE_BAD_JAPAN_BAND           { -2252,   -6,   "Cannot use 802.11a band on an abg IAP in Japan. "                             "Invalid band"                                 }
#define ERR_IAP_WRITE_BAD_JAPAN_CHAN           { -2253,   -6,   "Cannot assign 802.11a channel to abg IAP in Japan. "                          "Invalid channel number"                       }
#define ERR_IAP_WRITE_DUP_CHAN                 { -2254,   -2,   "Duplicate or overlapping channel in use. "                                    "Cannot assign channel"                        }
#define ERR_IAP_WRITE_DUP_CHAN_ENB             { -2255,   -2,   "Duplicate or overlapping channel in use. "                                    "Cannot enable IAP"                            }
#define ERR_IAP_WRITE_GLB_COUNTRY_SET          { -2256,   -1,   "Country code already set. "                                                   "Unable to set country code"                   }
#define ERR_IAP_WRITE_MONITOR_PARM             { -2257,   -1,   "Must disable RF monitor first. "                                              "Cannot adjust " MONITOR_STR " parameter"      }
#define ERR_IAP_WRITE_RADAR_CHAN               { -2258,   -3,   "Radar detected on channel. "                                                  "Cannot assign channel"                        }
#define ERR_IAP_WRITE_RADAR_CHAN_ENB           { -2259,   -3,   "Radar detected on channel. "                                                  "Cannot enable IAP"                            }
#define ERR_IAP_WRITE_WDS_LINK_MAX             { -2260,   -2,   "WDS link assigned maximum number of IAPs. "                                   "Cannot assign IAP to WDS link"                }
#define ERR_IAP_WRITE_RADAR_FAIL               { -2261,   -1,   "General error occurred. "                                                     "Unable to beacon channel switch"              }
#define ERR_IAP_WRITE_RFM_CHG_FAIL             { -2262,   -1,                                                                                  "Error enabling or disabling the RF monitor"   }
#define ERR_IAP_WRITE_CHG_ANT_FAIL             { -2263,   -1,                                                                                  "Error changing IAP antenna"                   }
#define ERR_IAP_WRITE_CHG_BAND_FAIL            { -2264,   -1,                                                                                  "Error changing IAP band"                      }
#define ERR_IAP_WRITE_CHG_CHAN_FAIL            { -2265,   -1,                                                                                  "Error changing IAP channel"                   }
#define ERR_IAP_WRITE_DOWN_FAIL                { -2266,   -1,                                                                                  "Error disabling IAP"                          }
#define ERR_IAP_WRITE_UP_FAIL                  { -2267,   -1,                                                                                  "Error enabling IAP"                           }
#define ERR_IAP_WRITE_WDS_MONITOR              { -2268,   -3,   "WDS link cannot be assigned to " MONITOR_STR " while RF monitor is enabled. " "Cannot assign IAP to WDS link"                }
#define ERR_IAP_WRITE_BAD_BAND                 { -2269,   -4,   "IAP is 802.11a only. "                                                        "Invalid band selected"                        }
#define ERR_IAP_WRITE_BAD_ANT_INT              { -2270,   -4,   "IAP has only an internal directional antenna. "                               "Invalid antenna selected"                     }
#define ERR_IAP_WRITE_BAD_ANT_EXT              { -2270,   -4,   "IAP has only an external antenna. "                                           "Invalid antenna selected"                     }
#define ERR_IAP_WRITE_MONITOR_BAD_ANT          { -2271,   -4,   "Cannot change " MONITOR_STR " antenna while RF monitor is enabled."           "Invalid antenna selected"                     }
#define ERR_IAP_WRITE_MONITOR_BAD_MODE         { -2272,   -4,   "Cannot change " MONITOR_STR " lock while RF monitor is enabled."              "Invalid lock selected"                        }
#define ERR_IAP_WRITE_CHG_RATES                { -2273,   -1,   "Must have at least one rate defined."                                         "Error changing 802.11 rates"                  }
#define ERR_IAP_WRITE_CHG_PREAMBLE             { -2274,   -1,                                                                                  "Error changing 802.11bg preamble"             }
#define ERR_IAP_WRITE_CHG_SLOT_TIME            { -2275,   -1,                                                                                  "Error changing 802.11bg slot time"            }
#define ERR_IAP_WRITE_CHG_PROTECT              { -2276,   -1,                                                                                  "Error changing 802.11bg protect mode"         }
#define ERR_IAP_WRITE_STANDBY_CHAN             { -2279,   -7,   "Standby mode requires the RF monitor to be enabled. "                         "Cannot change channel"                        }
#define ERR_IAP_WRITE_STANDBY_BAND             { -2280,   -7,   "Standby mode requires the RF monitor to be enabled. "                         "Cannot change band"                           }
#define ERR_IAP_WRITE_ASSURANCE_CHAN           { -2283,   -1,   "Radio assurance mode requires the RF monitor to be enabled. "                 "Cannot change channel"                        }
#define ERR_IAP_WRITE_ASSURANCE_BAND           { -2284,   -1,   "Radio assurance mode requires the RF monitor to be enabled. "                 "Cannot change band"                           }
#define ERR_IAP_WRITE_GLB_STANDBY_RFM_ON       { -2285,   -1,   "Standby mode requires the RF monitor to be enabled. "                         "Cannot enable Standby"                        }
#define ERR_IAP_WRITE_GLB_AUTOCELL_RFM_ON      { -2286,   -1,   "Non-channel specific autocell mode requires the RF monitor to be enabled. "   "Cannot enable Autocell"                       }
#define ERR_IAP_WRITE_GLB_ASSURANCE_RFM_ON     { -2287,   -1,   "Radio assurance mode requires the RF monitor to be enabled. "                 "Cannot enable Radio assurance mode"           }
#define ERR_IAP_WRITE_GLB_BAD_AUTOCELL         { -2288,   -1,   "Autocell period requested is too short. "                                     "Cannot set autocell period"                   }
#define ERR_IAP_WRITE_CHG_CELL_FAIL            { -2289,   -1,                                                                                  "Error changing IAP cell size"                 }
#define ERR_IAP_WRITE_CHG_BOND_FAIL            { -2290,   -1,                                                                                  "Error changing IAP channel bond"              }
#define ERR_IAP_WRITE_BAD_CHAN_BOND            { -2291,   -1,                                                                                  "Invalid channel bond"                         }
#define ERR_IAP_WRITE_BAD_CHAN_BOND_NO_11N     { -2292,   -1,   "Must enable 802.11n first. "                                                  "Cannot set channel bond"                      }
#define ERR_IAP_WRITE_BAD_WIFI_MODE            { -2293,   -1,   "Invalid WiFi mode setting for this IAP and/or band. "                         "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_A_BAND     { -2294,   -1,   "Invalid WiFi mode: Cannot enable 802.11b or g on a 5GHz IAP. "                "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_G_BAND     { -2295,   -1,   "Invalid WiFi mode: Cannot enable 802.11a on a 2.4GHz IAP. "                   "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_B_RATE     { -2296,   -1,   "Invalid WiFi mode: Cannot enable 802.11g without any valid g rates enabled. " "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_G_RATE     { -2297,   -1,   "Invalid WiFi mode: Cannot enable 802.11b without any valid b rates enabled. " "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_G_ONLY     { -2298,   -1,   "Invalid WiFi mode: Cannot enable 802.11b when g-only mode is enabled. "       "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_NO_N       { -2299,   -1,   "Invalid WiFi mode: Cannot enable 802.11n without valid 802.11n license. "     "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_N_OFF      { -2300,   -1,   "Invalid WiFi mode: Cannot enable 802.11n without 11n support enabled. "       "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_NO_AC      { -2201,   -1,   "Invalid WiFi mode: Cannot enable 802.11ac without valid 802.11ac license. "   "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_AC_OFF     { -2302,   -1,   "Invalid WiFi mode: Cannot enable 802.11ac without 11ac support enabled. "     "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_NONE       { -2303,   -1,   "Invalid WiFi mode: Must specify at least one valid mode. "                    "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_CHG_WIFI_FAIL            { -2304,   -1,                                                                                  "Error changing IAP wifi mode"                 }
#define ERR_IAP_WRITE_CHG_G_ONLY_RATES         { -2305,   -1,   "802.11g only mode enabled. 802.11b rates are disallowed."                     "Unable to update 802.11bg rates"              }
#define ERR_IAP_WRITE_AUTOCELL_RFM_FAIL        { -2307,   -1,   "Error occurred enabling RF monitor. "                                         "Unable to enable autocell"                    }
#define ERR_IAP_WRITE_DISABLE_WDS_LINK         { -2306,   -2,   "Error disabling WDS client link on " MONITOR_STR ". "                         "Cannot enable RF monitor"                     }
#define ERR_IAP_WRITE_ASSURANCE_RFM_FAIL       { -2309,   -1,   "Error occurred enabling RF monitor. "                                         "Unable to enable radio assurance"             }
#define ERR_IAP_WRITE_NOT_PRESENT              { -2310,   -1,   "IAP not present in system. "                                                  "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_ANGLE_ENB            { -2211,   -2,   "2.4GHz IAPs (including the RF monitor) must be spaced at least 90 deg apart. ""Cannot enable IAP"                            }
#define ERR_IAP_WRITE_BAD_NUM_CHAINS           { -2312,   -1,   "Invalid number of RF chains specified. "                                      "Unable to update 802.11n setting"             }
#define ERR_IAP_WRITE_CAPTURE_ENABLED          { -2313,   -1,   "Can't change radio setting while packet capture is enabled. "                 "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_OOPME_FAIL               { -2314,   -1,   "General error occurred. "                                                     "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_80_BOND_NO_2GHZ      { -2315,   -1,   "80MHz wide channels are not allowed in the 2.4GHz band. "                     "Cannot set channel bond"                      }
#define ERR_IAP_WRITE_BAD_160_BOND_NO_2GHZ     { -2316,   -1,   "160MHz wide channels are not allowed in the 2.4GHz band. "                    "Cannot set channel bond"                      }
#define ERR_IAP_WRITE_BAD_BOND_NO_11AC         { -2317,   -1,   "Must enable 802.11ac first. "                                                 "Cannot set channel bond"                      }
#define ERR_IAP_WRITE_BAD_BOND_NO_11AC_WAVE2   { -2318,   -1,   "Not 802.11ac wave 2 capable. "                                                "Cannot set channel bond"                      }
#define ERR_IAP_WRITE_BAD_BOND_NO_11AC_IAP     { -2319,   -1,   "IAP is not 802.11ac capable. "                                                "Cannot set channel bond"                      }
#define ERR_IAP_WRITE_BAD_BOND_NO_11AC_ENB     { -2320,   -1,   "Must enable 802.11ac on the IAP first. "                                      "Cannot set channel bond"                      }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_NOT_AC     { -2321,   -1,   "Invalid WiFi mode: Cannot enable 802.11ac on non-11ac capable IAP. "          "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WIFI_MODE_AC_BAND    { -2322,   -1,   "Invalid WiFi mode: Cannot enable 802.11ac on a 2.4GHz IAP. "                  "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_BAD_WAVE_2_NUM_CHAINS    { -2323,   -1,   "TX chains must be 3 or less. "                                                "Unable to update IAP setting"                 }
#define ERR_IAP_WRITE_TX_CHAINS_FAILED         { -2324,   -1,                                                                                  "Error changing TX chains"                     }
#define ERR_IAP_WRITE_MULTICAST_MODE_FAIL      { -2325,   -1,                                                                                  "Error changing multicast conversion setting"  }
#define ERR_IAP_WRITE_MULTICAST_ISOLATE_FAIL   { -2326,   -1,                                                                                  "Error changing multicast isolation setting"   }
#define ERR_IAP_WRITE_BAD_MIMO_NO_11AC_WAVE2   { -2327,   -1,   "Not 802.11ac wave 2 capable. "                                                "Cannot enable multi-user MIMO"                }
#define ERR_IAP_WRITE_BAD_TXBF_NO_11AC_WAVE2   { -2328,   -1,   "Not 802.11ac wave 2 capable. "                                                "Cannot enable transmit beamforming"           }
#define ERR_IAP_WRITE_BAD_11N                  { -2329,   -1,   "Not 802.11n capable. "                                                        "Unable to update setting"                     }
#define ERR_IAP_WRITE_BAD_11AC                 { -2330,   -1,   "Not 802.11ac capable. "                                                       "Unable to update setting"                     }
#define ERR_IAP_WRITE_BAD_BOND_NO_WAVE2_IAP    { -2331,   -1,   "IAP is not 802.11ac wave 2 capable. "                                         "Cannot set channel bond"                      }

// MANAGEMENT & GENERAL  (-23xx)
#define ERR_MANAGEMENT_WRITE                   { -2350,   -2,   "Must have at least one means of mamagement enabled. "                         "Cannot disable management"                    }
#define ERR_WRITE_BAD_MAC                      { -2351,   -2,                                                                                  "Invalid MAC address"                          }
#define ERR_WRITE_BAD_IPV4_MULTICAST           { -2352,   -2,   "Multicast address not allowed. "                                              "Invalid IPv4 address"                         }
#define ERR_WRITE_BAD_IPV4_RESERVED            { -2353,   -2,   "Reserved address not allowed. "                                               "Invalid IPv4 address"                         }
#define ERR_WRITE_BAD_IPV4_NETWORK             { -2354,   -2,   "Network address not allowed. "                                                "Invalid IPv4 address"                         }
#define ERR_WRITE_BAD_IPV4_BROADCAST           { -2355,   -2,   "Broadcast address not allowed. "                                              "Invalid IPv4 address"                         }
#define ERR_WRITE_BAD_IPV4_LOOPBACK            { -2356,   -2,   "Local loopback address not allowed. "                                         "Invalid IPv4 address"                         }
#define ERR_WRITE_BAD_IPV4_ZEROCONF            { -2357,   -2,   "Zero configuration address not allowed. "                                     "Invalid IPv4 address"                         }
#define ERR_WRITE_BAD_IPV4_ZEROADDR            { -2358,   -2,   "Address not allowed. "                                                        "Invalid IPv4 address"                         }
#define ERR_WRITE_BAD_IPV4_MASKADDR            { -2359,   -2,   "Mask not allowed as address. "                                                "Invalid IPv4 address"                         }
#define ERR_WRITE_BAD_IP_ADDRESS               { -2360,   -2,                                                                                  "Invalid IP address"                           }
#define ERR_WRITE_BAD_IP_MASK                  { -2361,   -2,                                                                                  "Invalid IP mask"                              }
#define ERR_WRITE_BAD_TCP_UDP_PORT             { -2362,   -2,   "Specified port is reserved or already in use. "                               "Invalid TCP or UDP port"                      }
#define ERR_WRITE_BAD_FIPS                     { -2370,   -2,   "Not allowed when running in FIPS mode. "                                      "Unable to change setting"                     }
#define ERR_WRITE_BAD_LICENSE_KEY              { -2380,   -2,   "Invalid license key. "                                                        "Unable to update license"                     }
#define ERR_WRITE_BAD_XR_LICENSE_KEY           { -2380,   -2,   "Invalid license key. System requires version 6.0 or higher"                   "Unable to update license"                     }
#define ERR_WRITE_BAD_TEMP_LICENSE_KEY         { -2381,   -2,   "Invalid temporary license key usage. "                                        "Unable to update license"                     }
#define ERR_WRITE_NET_ASSURANCE_FAIL           { -2382,   -1,   "Error occurred enabling Network Assurance. "                                  "Unable to enable fallback"                    }
#define ERR_WRITE_FALLBACK_FAIL                { -2383,   -1,   "Error occurred disabling fallback. "                                          "Unable to disable Network Assurance"          }
#define ERR_WRITE_BAD_RDK_IAPS_LOCKED          { -2384,   -1,   "IAP configuration is locked down. "                                           "Unable to enable RDK mode"                    }
#define ERR_WRITE_BAD_PWIFI_LIMIT              { -2385,   -1,   "Personal WiFi limit cannot equal or exceed maximum number of SSIDs. "         "Unable to set Personal WiFi limit"            }
#define ERR_WRITE_BAD_DEFAULT_EXPIRATION       { -2386,   -1,   "Default Personal WiFi SSID expiration string is malformed. "                  "Cannot set default expiration"                }

// RADAR        (-24xx)
#define ERR_RADAR_WRITE_TEST_MODE              { -2450,   -1,                                                                                  "Error entering radar test mode"               }

// RADIUS       (-25xx)
#define ERR_RADIUS_ADD_TABLE_FULL              { -2510,   -1,   "Internal RADIUS server full. "                                                "Cannot add RADIUS user"                       }
#define ERR_RADIUS_ADD_BAD_SSID                { -2511,   -2,   "SSID does not exist. "                                                        "Cannot add RADIUS user"                       }
#define ERR_RADIUS_ADD_BAD_SSID_ENC            { -2512,   -3,   "SSID must support WPA or WPA2 or Web Page Redirect. "                         "Cannot add RADIUS user"                       }
#define ERR_RADIUS_ADD_BAD_GROUP               { -2513,   -2,   "User Group does not exist. "                                                  "Cannot add RADIUS user"                       }
#define ERR_RADIUS_DEL_BAD_USER                { -2520,   -2,   "RADIUS user does not exist. "                                                 "Cannot delete RADIUS user"                    }
#define ERR_RADIUS_READ_USER_FAIL              { -2540,   -1,                                                                                  "Undefined internal RADIUS user"               }
#define ERR_RADIUS_WRITE_THREAD_FAIL           { -2550,   -1,   "General error occurred. "                                                     "Cannot add or edit RADIUS user"               }

// RESET        (-26xx)
#define ERR_RESET_EXEC_CFG_FAIL                { -2630,   -1,   "Error reseting running configuration to factory defaults. "                   "General error occurred"                       }
#define ERR_RESET_EXEC_IAPS_LOCKED             { -2631,   -1,   "IAP configuration is locked. "                                                "Factory reset is disallowed"                  }
#define ERR_RESET_READ_REASON                  { -2640,   -1,                                                                                  "Unable to read reset reason"                  }

// IDS          (-27xx)
#define ERR_IDS_ADD_TABLE_FULL                 { -2710,   -1,   "Rogue table full. "                                                           "Cannot add SSID or BSSID"                     }
#define ERR_IDS_DEL_UNDEFINED                  { -2720,   -2,   "Undefined rogue. "                                                            "Cannot delete SSID or BSSID"                  }
#define ERR_IDS_DEL_FOUND_WC                   { -2721,   -2,   "Rogue is defined by a wildcard. "                                             "Cannot delete SSID or BSSID"                  }
#define ERR_IDS_REQUIRES_RSM_LICENSE           { -2754,   -1,   "Intrusion detection requires RF Security Manager (RSM) license. "             "Cannot enable intrusion detection"            }
#define ERR_IDS_WRITE_BAD_ATTACK_PERIOD        { -2756,   -1,   "Attack period must be an increment of 10. "                                   "Invalid attack period"                        }

// SAVE         (-28xx)
#define ERR_SAVE_EXEC_CFG_FAIL                 { -2830,   -1,   "Error saving running configuration. "                                         "General error occurred"                       }
#define ERR_SAVE_EXEC_IMAGE_BAD_FILE           { -2831,   -1,   "Error saving new software image. "                                            "File does not exist"                          }
#define ERR_SAVE_EXEC_IMAGE_NO_FILE            { -2832,   -1,   "Error saving new software image. "                                            "No image specified"                           }
#define ERR_SAVE_EXEC_IMAGE_WRITE_FAIL         { -2833,   -1,   "Error saving new software image. "                                            "File write failed"                            }
#define ERR_SAVE_EXEC_IMAGE_BAD_CRC            { -2834,   -1,   "Software image integrity check failed. "                                      "Image will be deleted"                        }
#define ERR_SAVE_EXEC_IMAGE_CHK_NO_FILE        { -2835,   -1,   "Software image integrity check failed. "                                      "Image does not exist"                         }
#define ERR_SAVE_EXEC_IMAGE_BAD_LICENSE        { -2836,   -1,   "Software image license check failed. "                                        "Please contact %s Customer Support (support@%s.com) to request a new license key. "   "Image will be deleted"}
#define ERR_SAVE_EXEC_IMAGE_BAD_HARDWARE       { -2837,   -1,   "Software image hardware compatibility check failed. "                         "The software version is incompatible with this hardware and will not boot correctly. ""Image will be deleted"}
#define ERR_SAVE_EXEC_IMAGE_BAD_CMD            { -2838,   -1,   "Error saving new software image. "                                            "Invalid command"                              }
#define ERR_SAVE_EXEC_IMAGE_IN_PROCESS         { -2839,   -2,   "Saving new software image. "                                                  "File write in process"                        }
#define ERR_SAVE_EXEC_IMAGE_REBOOT             { -2840,   -2,   "Saving new software image. "                                                  "Reboot will occur when save is complete"      }
#define ERR_SAVE_EXEC_IMAGE_DOWNGRADE          { -2841,   -2,   "Older software image requires firmware to be downgraded. "                    "Cannot lose power during downgrade process"   }
#define ERR_SAVE_EXEC_CFG_VERIFY_FAIL          { -2842,   -1,   "Error verifying saved running configuration. "                                "Save configuration externally and reboot"     }
#define ERR_SAVE_EXEC_CFG_FILE_EXISTS          { -2843,   -1,   "Configuration file already exists. "                                          "It will be overwritten"                       }
#define ERR_SAVE_EXEC_CFG_FILE_PROTECTED       { -2844,   -1,   "Configuration file is write protected. "                                      "Cannot save configuration file"               }
#define ERR_SAVE_EXEC_DIAG_FILE_EXISTS         { -2845,   -1,   "Diagnostic file already exists. "                                             "It will be overwritten"                       }
#define ERR_SAVE_EXEC_TEST_FILE_EXISTS         { -2846,   -1,   "Self test output file already exists. "                                       "It will be overwritten"                       }
#define ERR_SAVE_EXEC_CLI_COMMAND              { -2847,   -1,   "Error executing CLI command. "                                                "General error occurred"                       }
#define ERR_SAVE_EXEC_WRITE_FILE               { -2848,   -1,   "Error writing file. "                                                         "General error occurred"                       }
#define ERR_SAVE_EXEC_IAPS_LOCKED              { -2849,   -1,   "IAP configuration is locked. "                                                "Software downgrade is disallowed"             }
#define ERR_SAVE_EXEC_DIAG_READ_ONLY_FS        { -2850,   -1,   "The file system is read-only. "                                               "Cannot save diagnostic file"                  }
#define ERR_SAVE_EXEC_CFG_READ_ONLY_FS         { -2851,   -1,   "The file system is read-only. "                                               "Cannot save configuration file"               }

// SEC          (-29xx)
#define ERR_SEC_WRITE_BAD_CIPHER_AES           { -2950,   -2,   "At least one cipher suite (AES or TKIP) must be enabled. "                    "Cannot disable AES"                           }
#define ERR_SEC_WRITE_BAD_CIPHER_TKIP          { -2951,   -2,   "At least one cipher suite (AES or TKIP) must be enabled. "                    "Cannot disable TKIP"                          }
#define ERR_SEC_WRITE_BAD_KEY_MGMT_EAP         { -2952,   -3,   "At least one key management (PSK or EAP) algorithm must be enabled. "         "Cannot disable EAP"                           }
#define ERR_SEC_WRITE_BAD_KEY_MGMT_PSK         { -2953,   -3,   "At least one key management (PSK or EAP) algorithm must be enabled. "         "Cannot disable PSK"                           }
#define ERR_SEC_WRITE_NO_PASSPHRASE_PSK        { -2954,   -4,   "Passphrase is not set. "                                                      "Cannot enable PSK"                            }
#define ERR_SEC_WRITE_BAD_HEX_PSK              { -2955,   -1,   "64 character long pre-shared key must contain only hexidecimal characters. "  "Invalid hex PSK"                              }
#define ERR_SEC_WRITE_EAP_PSK_FAIL             { -2956,   -1,   "General error occurred. "                                                     "Cannot change EAP/PSK settings"               }
#define ERR_SEC_WRITE_BAD_PSK                  { -2957,   -4,   "Passphrase is too short. "                                                    "Invalid PSK"                                  }
#define ERR_SEC_WRITE_OOPME_FAIL               { -2959,   -1,   "General error occurred. "                                                     "Unable to update security settings"           }
#define ERR_SEC_WRITE_BAD_WPA2                 { -2960,   -2,   "WPA2 on AC Wave2 device requires AES to be enabled."                          "Cannot disable AES"                           }

// SHOW / RESTORE / QUICK CONFIG (-30xx)
#define ERR_SHOW_CFG_EXEC_FAIL                 { -3030,   -1,   "Error showing configuration. "                                                "General error occurred"                       }
#define ERR_SHOW_CFG_EXEC_DIFF_FAIL            { -3031,   -1,   "Error showing configuration difference. "                                     "General error occurred"                       }
#define ERR_SHOW_CFG_EXEC_DIFF_NO_FILE         { -3032,   -1,   "Error showing configuration difference. "                                     "File does not exist"                          }
#define ERR_RESTORE_CFG_EXEC_FAIL              { -3033,   -1,   "Error restoring configuration. "                                              "General error occurred"                       }
#define ERR_RESTORE_CFG_EXEC_NO_FILE           { -3034,   -1,   "Error restoring configuration. "                                              "File does not exist"                          }
#define ERR_QUICK_CONFIG_EXEC_NO_FILE          { -3035,   -1,   "Error applying quick configuration. "                                         "Template does not exist"                      }

// SNMP         (-31xx)
#define ERR_SNMP_WRITE_FILE_FAIL               { -3150,   -1,   "Error writing SNMP configuration file. "                                      "Cannot update SNMP information"               }
#define ERR_SNMP_WRITE_OOPME_FAIL              { -3159,   -1,   "General error occurred. "                                                     "Cannot update SNMP information"               }

// SSIDs        (-32xx)
#define ERR_SSID_ADD_TABLE_FULL                { -3210,   -1,   "SSID table full. "                                                            "Cannot add SSID"                              }
#define ERR_SSID_ADD_TABLE_DUP_NAME            { -3211,   -1,   "Duplicate SSID name. "                                                        "Cannot add SSID"                              }
#define ERR_SSID_ADD_TABLE_INVALID_NAME        { -3212,   -1,   "Invalid SSID name. "                                                          "Cannot add SSID"                              }
#define ERR_SSID_DEL_UNDEFINED                 { -3220,   -1,   "Undefined SSID. "                                                             "Cannot delete SSID"                           }
#define ERR_SSID_DEL_RADIUS_USERS              { -3221,   -2,   "Internal RADIUS users are defined for this SSID. "                            "They will no longer be restricted by SSID"    }
#define ERR_SSID_DEL_LAST                      { -3222,   -3,   "At least one SSID must be defined. "                                          "Cannot delete SSID"                           }
#define ERR_SSID_DEL_WDS_LINKS                 { -3223,   -4,   "WDS links are defined for this SSID. "                                        "They will be disabled"                        }
#define ERR_SSID_DEL_RAD_AND_WDS               { -3224,   -5,   "Internal RADIUS users and WDS links are defined for this SSID. "              "They will become invalid and be disabled"     }
#define ERR_SSID_DEL_OOPME_FAIL                { -3225,   -1,   "General error occurred. "                                                     "Cannot enable SSID"                           }
#define ERR_SSID_WRITE_BROADCAST_FAIL          { -3250,   -2,                                                                                  "Undefined SSID"                               }
#define ERR_SSID_WRITE_DHCP_POOL_FAIL          { -3251,   -2,                                                                                  "Undefined DHCP Pool"                          }
#define ERR_SSID_WRITE_WPR_BAD_VLAN            { -3252,   -2,   "Web Page Redirect VLAN number must be unique. "                               "Cannot set VLAN"                              }
#define ERR_SSID_WRITE_BAD_DHCP_POOL           { -3253,   -3,   "Web Page Redirect or NAT subnet conflict exists. "                            "Cannot assign DHCP Pool"                      }
#define ERR_SSID_WRITE_WPR_BAD_NATIVE          { -3254,   -2,   "Web Page Redirect VLAN cannot be the native VLAN. "                           "Cannot set VLAN"                              }
#define ERR_SSID_WRITE_OOPME_FAIL              { -3259,   -1,   "General error occurred. "                                                     "Cannot add or edit SSID"                      }
#define ERR_SSID_WRITE_ENB_BAD_SUBNET          { -3262,   -4,   "Web Page Redirect or NAT subnet conflict exists. "                            "SSID cannot be enabled"                       }
#define ERR_SSID_WRITE_ENB_BAD_VLAN            { -3263,   -2,   "Web Page Redirect VLAN conflict exists. "                                     "SSID cannot be enabled"                       }
#define ERR_SSID_WRITE_ENB_NO_DHCP_POOL        { -3264,   -5,   "Web Page Redirect requires DHCP. No DHCP Pool selected for SSID. "            "SSID cannot be enabled"                       }
#define ERR_SSID_WRITE_ENB_NO_DHCP_NAT         { -3265,   -8,   "Web Page Redirect requires NAT. DHCP Pool for SSID is not NAT enabled. "      "SSID cannot be enabled"                       }
#define ERR_SSID_WRITE_ENB_DHCP_POOL_OFF       { -3266,   -6,   "Web Page Redirect requires DHCP. DHCP Pool for SSID is not enabled. "         "SSID cannot be enabled"                       }
#define ERR_SSID_WRITE_ENB_DHCP_SHARED         { -3267,   -7,   "Web Page Redirect DHCP Pool conflict exists. "                                "SSID cannot be enabled"                       }
#define ERR_SSID_WRITE_ENB_NO_DHCP_DNS         { -3268,   -9,   "Web Page Redirect requires DNS. DHCP Pool for SSID does not have DNS set. "   "SSID cannot be enabled"                       }
#define ERR_SSID_WRITE_ENB_BAD_NATIVE          { -3269,   -2,   "Web Page Redirect VLAN cannot be the native VLAN. "                           "SSID cannot be enabled"                       }
#define ERR_SSID_WRITE_BAD_AUTH_NO_1X          { -3270,   -2,   "SSID encryption type does not support 802.1x. "                               "Cannot change SSID authentication type"       }
#define ERR_SSID_WRITE_BAD_AUTH_ONLY_1X        { -3271,   -2,   "SSID encryption type only supports 802.1x. "                                  "Cannot change SSID authentication type"       }
#define ERR_SSID_WRITE_AUTH_FAIL               { -3272,   -1,   "General error occurred. "                                                     "Cannot change SSID authentication type"       }
#define ERR_SSID_WRITE_BAD_XRP_LAYER           { -3273,   -1,   "Layer 3 fast roaming must first be enabled in IAP global settings. "          "Cannot set SSID fast roaming layer"           }
#define ERR_SSID_WRITE_BAD_FILTER_LIST         { -3274,   -2,   "Undefined Filter List. "                                                      "Cannot set Filter List"                       }
#define ERR_SSID_WRITE_GLB_FILTER_LIST         { -3275,   -2,   "Global Filter List already applied to all SSIDs by default. "                 "Cannot set Filter List"                       }
#define ERR_SSID_WRITE_BAD_DEF_SEC_WEP         { -3276,   -2,   "SSID security settings must be global for WEP. "                              "Cannot set SSID security to unique settings"  }
#define ERR_SSID_WRITE_BAD_IAP                 { -3277,   -1,   "Invalid IAP. "                                                                "Cannot add IAP to active list"                }
#define ERR_SSID_WRITE_DEF_SEC                 { -3278,   -1,                                                                                  "Error changing SSID security settings"        }
#define ERR_SSID_WRITE_BAD_VLAN                { -3279,   -2,   "Undefined VLAN. "                                                             "Cannot set SSID VLAN"                         }
#define ERR_SSID_WRITE_EAP_PSK_FAIL            { -3280,   -1,   "General error occurred. "                                                     "Cannot change SSID EAP/PSK settings"          }
#define ERR_SSID_WPR_WLIST_ADD_FULL            { -3281,   -1,   "Whitelist table is full. "                                                    "Cannot add domain name"                       }
#define ERR_SSID_WPR_WLIST_ADD_EXISTS          { -3282,   -1,   "Domain name already in whitelist. "                                           "Cannot add domain name"                       }
#define ERR_SSID_WPR_WLIST_DEL_UNDEFINED       { -3283,   -1,   "Undefined whitelist entry. "                                                  "Cannot delete domain name"                    }
#define ERR_SSID_WPR_WLIST_INVALID_ENTRY       { -3284,   -1,                                                                                  "Invalid wildcard in domain name"              }
#define ERR_SSID_WRITE_MDM_ENB_FAIL            { -3285,   -1,   "MDM authentication not supported in conjunction with Web Page Redirect. "     "Cannot enable MDM authentication for this SSID"}
#define ERR_SSID_WRITE_WPR_ENB_FAIL            { -3286,   -1,   "Web Page Redirect not supported in conjunction with MDM authentication. "     "Cannot enable Web Page Redirect for this SSID"}
#define ERR_SSID_HONEYPOT_WLIST_ADD_FULL       { -3287,   -1,   "Honeypot whitelist is full. "                                                 "Cannot add SSID"                              }
#define ERR_SSID_HONEYPOT_WLIST_ADD_EXISTS     { -3288,   -1,   "SSID already in whitelist. "                                                  "Cannot add SSID"                              }
#define ERR_SSID_HONEYPOT_WLIST_DEL_UNDEFINED  { -3289,   -1,   "Undefined SSID. "                                                             "Cannot delete SSID"                           }
#define ERR_SSID_HONEYPOT_BCAST_ADD_FULL       { -3290,   -1,   "Honeypot broadcast list is full. "                                            "Cannot add SSID"                              }
#define ERR_SSID_HONEYPOT_BCAST_ADD_EXISTS     { -3291,   -1,   "SSID already in broadcast list. "                                             "Cannot add SSID"                              }
#define ERR_SSID_HONEYPOT_BCAST_DEL_UNDEFINED  { -3292,   -1,   "Undefined SSID. "                                                             "Cannot delete SSID"                           }
#define ERR_SSID_WRITE_DHCP_OPTION_ENB_FAIL    { -3293,   -1,   "DHCP Option 82 cannot be enabled if Tunnel-base DHCP Option is enabled. "     "Cannot enable DHCP Option 82 for this SSID"   }
#define ERR_SSID_WRITE_WPR_CLOUD_BLOCK_FAIL    { -3294,   -1,   "Parameter cannot be changed if Web-page Server is Cloud. "                    "Cannot change setting"                        }
#define ERR_SSID_WRITE_BAD_WFA_WPA_ONLY        { -3295,   -1,   "WPA-only mode is not allowed when WFA mode is enabled. "                      "Cannot set SSID encryption type"              }
#define ERR_SSID_WRITE_PWIFI_ALL_LIMIT_FAIL    { -3296,   -1,   "Personal WiFi SSID limit execeeded. "                                         "Cannot add Personal WiFi SSID"                }
#define ERR_SSID_WRITE_PWIFI_STA_LIMIT_FAIL    { -3297,   -1,   "Personal WiFi SSID station limit exceeded. "                                  "Cannot add Personal WiFi SSID"                }
#define ERR_SSID_CLEAR_VLAN_FAIL               { -3298,   -1,   "General error occurred. "                                                     "Cannot clear SSID VLAN"                       }
#define ERR_SSID_CLEAR_VLAN_POOL_FAIL          { -3299,   -1,   "General error occurred. "                                                     "Cannot clear SSID VLAN Pool"                  }
#define ERR_SSID_DHCP_PLUS_VLAN_POOL           { -3300,   -1,   "DHCP Pool is not compatible with VLAN POOL. "                                 "Cannot assign DHCP Pool"                      }
#define ERR_SSID_VLAN_PLUS_DHCP_POOL           { -3301,   -1,   "VLAN Pool is not compatible with DHCP POOL. "                                 "Cannot assign VLAN Pool"                      }
#define ERR_SSID_WRITE_NAME_DUPLICATE          { -3302,   -1,   "SSID names must be unique. "                                                  "Cannot change SSID name"                      }
#define ERR_SSID_WRITE_DATE_ON_FAIL            { -3303,   -1,   "SSID date on string is malformed. "                                           "Cannot set SSID date on"                      }
#define ERR_SSID_WRITE_DATE_ON_PAST            { -3304,   -1,   "SSID date on is in the past. "                                                "Cannot set SSID date on"                      }
#define ERR_SSID_WRITE_DATE_ON_GT_DATE_OFF     { -3305,   -1,   "SSID date on is later than date off. "                                        "Cannot set SSID date on"                      }
#define ERR_SSID_WRITE_DATE_ON_GT_EXPIRATION   { -3306,   -1,   "SSID date on is later than expiration. "                                      "Cannot set SSID date on"                      }
#define ERR_SSID_WRITE_DATE_OFF_FAIL           { -3307,   -1,   "SSID date off string is malformed. "                                          "Cannot set SSID date off"                     }
#define ERR_SSID_WRITE_DATE_OFF_PAST           { -3308,   -1,   "SSID date off is in the past. "                                               "Cannot set SSID date off"                     }
#define ERR_SSID_WRITE_DATE_OFF_LT_DATE_ON     { -3309,   -1,   "SSID date off is earlier than date on. "                                      "Cannot set SSID date off"                     }
#define ERR_SSID_WRITE_DATE_OFF_GT_EXPIRATION  { -3310,   -1,   "SSID date off is later than expiration. "                                     "Cannot set SSID date off"                     }
#define ERR_SSID_WRITE_EXPIRATION_FAIL         { -3311,   -1,   "SSID expiration string is malformed. "                                        "Cannot set SSID expiration"                   }
#define ERR_SSID_WRITE_EXPIRATION_PAST         { -3312,   -1,   "SSID expiration is in the past. "                                             "Cannot set SSID expiration"                   }
#define ERR_SSID_WRITE_EXPIRATION_LT_DATE_ON   { -3313,   -1,   "SSID expiration is earlier than date on. "                                    "Cannot set SSID expiration"                   }
#define ERR_SSID_WRITE_EXPIRATION_LT_DATE_OFF  { -3314,   -1,   "SSID expiration is earlier than date off. "                                   "Cannot set SSID expiration"                   }
#define ERR_SSID_WRITE_11K_ENABLE_FAIL         { -3315,   -1,   "General error occurred. "                                                     "Unable to enable 802.11k"                     }

// STANDBY      (-33xx)
#define ERR_STANDBY_WRITE_NO_TARGET            { -3350,   -1,   "Standby target not defined. "                                                 "Unable to enable standby mode"                }
#define ERR_STANDBY_WRITE_BAD_TARGET_OUI       { -3351,   -1,   "Standby target address must start with a valid OUI. "                         "Invalid target specified"                     }
#define ERR_STANDBY_WRITE_BAD_TARGET_LSB       { -3352,   -1,   "Standby target address must end with a 0. "                                   "Invalid target specified"                     }
#define ERR_STANDBY_WRITE_BAD_TARGET_US        { -3353,   -1,   "Standby target address must not be this system. "                             "Invalid target specified"                     }
#define ERR_STANDBY_WRITE_RFM_FAIL             { -3354,   -1,   "Error occurred enabling RF monitor. "                                         "Unable to enable standby mode"                }
#define ERR_STANDBY_WRITE_OOPME_FAIL           { -3359,   -1,   "General error occurred. "                                                     "Unable to enable standby mode"                }

// SYSLOG       (-34xx)
#define ERR_SYSLOG_EXEC_CLR_FAIL               { -3430,   -1,   "Error clearing syslog. "                                                      "Unable to open file"                          }
#define ERR_SYSLOG_READ_NUM_LOGS               { -3440,   -1,                                                                                  "Unable to read number of syslog entries"      }
#define ERR_SYSLOG_WRITE_THREAD_FAIL           { -3450,   -1,   "General error occurred. "                                                     "Cannot restart SYSLOG server"                 }

// TIME         (-35xx)
#define ERR_TIME_EXEC_NO_TIME                  { -3530,   -1,   "No date & time setting specified. "                                           "Cannot set date and time"                     }
#define ERR_TIME_EXEC_NTP_ON                   { -3531,   -2,   "Must disable NTP first. "                                                     "Cannot set date and time"                     }
#define ERR_TIME_EXEC_OPEN_FAIL                { -3532,   -1,   "Error opening real time clock. "                                              "Cannot set date and time"                     }
#define ERR_TIME_EXEC_WRITE_FAIL               { -3533,   -1,   "Error writing real time clock. "                                              "Cannot set date and time"                     }
#define ERR_TIME_WRITE_BAD_NTP_KEY_ID          { -3534,   -1,   "NTP authentication key IDs need to differ for servers. "                      "Invalid NTP authentication key ID"            }
#define ERR_TIME_WRITE_NTP_FILE_FAIL           { -3550,   -1,   "General error occurred. "                                                     "Cannot restart NTP"                           }
#define ERR_TIME_WRITE_SET_FAIL                { -3551,   -1,   "General error occurred. "                                                     "Cannot set time"                              }
#define ERR_TIME_WRITE_TEMP_LICENSE            { -3550,   -1,   "Operation under temporary license. "                                          "Cannot adjust any time or date settings"      }

// VLAN         (-36xx)
#define ERR_VLAN_ADD_TABLE_FULL                { -3610,   -2,   "VLAN table full. "                                                            "Cannot add VLAN"                              }
#define ERR_VLAN_ADD_TABLE_DUP_NAME            { -3611,   -1,   "Duplicate VLAN name. "                                                        "Cannot add VLAN"                              }
#define ERR_VLAN_ADD_TABLE_BAD_NAME            { -3612,   -2,   "Invalid VLAN name. "                                                          "Cannot add VLAN"                              }
#define ERR_VLAN_ADD_BAD_NUMBER                { -3613,   -3,   "VLAN number must be assigned and non-zero. "                                  "Cannot add or modify VLAN"                    }
#define ERR_VLAN_ADD_NON_UNIQUE                { -3614,   -4,   "VLAN number must be unique. "                                                 "Cannot add or modify VLAN"                    }
#define ERR_VLAN_ADD_WPR_NON_UNIQUE            { -3615,   -5,   "Web Page Redirect VLAN number must be unique. "                               "Cannot add of modify VLAN"                    }
#define ERR_VLAN_ADD_OVERLAP_NAT               { -3616,   -6,   "VLAN subnet must not overlap DHCP Pool NAT subnet. "                          "Cannot add or modify VLAN"                    }
#define ERR_VLAN_ADD_OVERLAP_ETH               { -3617,   -6,   "VLAN subnet must not overlap Ethernet interface IP subnet. "                  "Cannot add or modify VLAN"                    }
#define ERR_VLAN_DEL_UNDEFINED                 { -3620,   -2,   "Undefined VLAN. "                                                             "Cannot delete VLAN"                           }
#define ERR_VLAN_DEL_IN_USE_MGMT               { -3621,   -2,   "VLAN is configured as default route. "                                        "Default route will be cleared"                }
#define ERR_VLAN_DEL_IN_USE_SSID               { -3621,   -2,   "This VLAN is in use by one or more SSIDS. "                                   "VLAN for those SSIDs will be cleared"         }
#define ERR_VLAN_DEL_IN_USE_GROUP              { -3622,   -2,   "This VLAN is in use by one or more User Groups. "                             "VLAN for those Groups will be cleared"        }
#define ERR_VLAN_DEL_IN_USE_POOL               { -3623,   -2,   "This VLAN is in use by one or more VLAN pools. "                              "VLAN for those VLAN pools will be cleared"    }
#define ERR_VLAN_READ_FAIL                     { -3640,   -1,                                                                                  "Undefined VLAN"                               }
#define ERR_VLAN_WRITE_SET_IP                  { -3650,   -1,   "Error setting VLAN IP address. "                                              "Undefined VLAN"                               }
#define ERR_VLAN_WRITE_WPR_OVERLAP             { -3651,   -6,   "VLAN subnet must not overlap with Web Page Redirect or NAT subnet. "          "Subnet is invalid"                            }
#define ERR_VLAN_WRITE_BAD_ADDR                { -3652,   -1,   "Invalid IP address. "                                                         "Cannot edit VLAN parameters"                  }
#define ERR_VLAN_WRITE_BAD_MASK                { -3653,   -1,   "Invalid IP mask. "                                                            "Cannot edit VLAN parameters"                  }
#define ERR_VLAN_WRITE_DEFAULT_FAIL            { -3654,   -2,                                                                                  "Undefined VLAN"                               }
#define ERR_VLAN_WRITE_IFACE_FAIL              { -3655,   -1,                                                                                  "Cannot access VLAN interface"                 }
#define ERR_VLAN_WRITE_DFLT_RTE_FAIL           { -3656,   -1,   "General error occurred. "                                                     "Unable to set default route"                  }
#define ERR_VLAN_WRITE_DFLT_RTE_NO_VLAN        { -3657,   -1,   "VLAN not defined. "                                                           "Unable to set default route"                  }
#define ERR_VLAN_WRITE_DFLT_RTE_NO_ADDR        { -3658,   -1,   "VLAN IP address not set. "                                                    "Unable to set default route"                  }
#define ERR_VLAN_WRITE_DFLT_RTE_NO_GW          { -3659,   -1,   "VLAN gateway not set. "                                                       "Unable to set default route"                  }
#define ERR_VLAN_WRITE_NATIVE_NO_VLAN          { -3662,   -1,   "VLAN not defined. "                                                           "Unable to set native VLAN"                    }
#define ERR_VLAN_WRITE_NATIVE_NO_WPR           { -3663,   -1,   "Native VLAN cannot be assigned to a VLAN using Web Page Redirect. "           "Unable to set native VLAN"                    }
#define ERR_VLAN_WRITE_OOPME_FAIL              { -3679,   -1,   "General error occurred. "                                                     "Unable to update VLAN settings"               }
#define ERR_VLAN_POOL_ADD_TABLE_FULL           { -3680,   -2,   "VLAN Pool table full. "                                                       "Cannot add VLAN Pool"                         }
#define ERR_VLAN_POOL_ADD_TABLE_BAD_NAME       { -3681,   -2,   "Invalid VLAN Pool name. "                                                     "Cannot add VLAN Pool"                         }
#define ERR_VLAN_POOL_DEL_UNDEFINED            { -3682,   -2,   "Undefined VLAN Pool. "                                                        "Cannot delete VLAN Pool"                      }
#define ERR_VLAN_POOL_READ_FAIL                { -3683,   -1,                                                                                  "Undefined VLAN Pool"                          }
#define ERR_VLAN_POOL_WRITE_LIST               { -3684,   -1,   "Error setting VLAN Pool list. "                                               "Undefined VLAN Pool"                          }
#define ERR_VLAN_POOL_WRITE_OOPME_FAIL         { -3685,   -1,   "General error occurred. "                                                     "Unable to update VLAN Pool settings"          }
#define ERR_VLAN_POOL_DEL_IN_USE_SSID          { -3686,   -1,   "This VLAN Pool is in use by one or more SSIDS. "                              "VLAN Pool for those SSIDs will be cleared"    }
#define ERR_VLAN_POOL_DEL_IN_USE_GROUP         { -3687,   -1,   "This VLAN Pool is in use by one or more User Groups. "                        "VLAN Pool for those Groups will be cleared"   }
#define ERR_VLAN_POOL_ASSIGN_UNDEFINED         { -3688,   -1,   "Undefined VLAN Pool. "                                                        "Cannot assign VLAN Pool"                      }
#define ERR_VLAN_WRITE_SET_IPV6                { -3650,   -1,   "Error setting VLAN IPv6 DHCP address. "                                       "Undefined VLAN"                               }

// WDS          (-37xx)
#define ERR_WDS_WRITE_NO_SSID                  { -3750,   -3,   "WDS SSID undefined. "                                                         "Cannot enable WDS link"                       }
#define ERR_WDS_WRITE_NO_TARGET                { -3751,   -4,   "WDS target undefined. "                                                       "Cannot enable WDS link"                       }
#define ERR_WDS_WRITE_BAD_SSID                 { -3752,   -5,   "SSID does not exist. "                                                        "Cannot assign WDS SSID"                       }
#define ERR_WDS_WRITE_NO_MAX_IAPS              { -3753,   -7,   "Max IAP count must be non-zero. "                                             "Cannot enable WDS link"                       }
#define ERR_WDS_WRITE_NO_IAPS                  { -3754,   -8,   "No IAPs assigned to this WDS link. "                                          "Cannot enable WDS link"                       }
#define ERR_WDS_WRITE_BAD_MAX_IAPS             { -3755,   -9,   "WDS link already assigned greater number of IAPs. "                           "Cannot assign IAP to WDS link"                }
#define ERR_WDS_WRITE_BAD_TARGET_OUI           { -3756,  -10,   "WDS target address must start with a valid OUI. "                             "Invalid target specified"                     }
#define ERR_WDS_WRITE_BAD_TARGET_LSB           { -3757,  -11,   "WDS target address must end with a 0. "                                       "Invalid target specified"                     }
#define ERR_WDS_WRITE_BAD_TARGET_DUP           { -3758,  -16,   "WDS target address must only be used once per system. "                       "Invalid target specified"                     }
#define ERR_WDS_WRITE_BAD_TARGET_US            { -3759,  -17,   "WDS target address must not be this system. "                                 "Invalid target specified"                     }
#define ERR_WDS_WRITE_SSID_THEN_PASSWORD       { -3760,  -12,   "WDS SSID must be defined before setting password. "                           "Cannot set WDS link password"                 }
#define ERR_WDS_WRITE_PASSWORD_NOT_NEEDED      { -3761,  -13,   "WDS link password is not required or is determined by SSID. "                 "No need to set WDS link password"             }
#define ERR_WDS_WRITE_SSID_THEN_USERNAME       { -3762,  -14,   "WDS SSID must be defined before setting username. "                           "Cannot set WDS link username"                 }
#define ERR_WDS_WRITE_USERNAME_NOT_NEEDED      { -3763,  -15,   "WDS link username is not required for SSID. "                                 "No need to set WDS link username"             }
#define ERR_WDS_WRITE_ENABLE_STP               { -3764,   -1,   "Configuration may create a network loop but spanning tree is disabled. "                                                     }
#define ERR_WDS_WRITE_LOCKED                   { -3765,   -1,   "WDS configuration is locked. "                                                "Cannot change WDS settings"                   }
#define ERR_WDS_WRITE_OOPME_FAIL               { -3769,   -1,   "General error occurred. "                                                     "Unable to update WDS settings"                }
#define ERR_WDS_WRITE_BAD_SSID_SECURITY        { -3770,   -1,   "SSID security must be Open or WPA2-PSK-AES. "                                 "Cannot assign WDS SSID"                       }

// WPR          (-38xx)
#define ERR_WPR_WRITE_BAD_SUBNET               { -3850,   -2,   "Web Page Redirect subnet must be unique. "                                    "Subnet cannot be changed"                     }
#define ERR_WPR_WRITE_ENB_BAD_DHCP_POOL        { -3851,   -3,   "SSID DHCP Pool must be disabled. "                                            "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_ENB_BAD_SUBNET           { -3852,   -4,   "Web Page Redirect subnet must be unique. "                                    "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_ENB_BAD_VLAN             { -3853,   -2,   "SSID VLAN number not unique. "                                                "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_ENB_NO_DHCP_POOL         { -3854,   -5,   "No DHCP Pool selected for SSID. "                                             "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_ENB_NO_DHCP_NAT          { -3855,   -6,   "DHCP Pool for SSID does not have NAT enabled. "                               "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_ENB_DHCP_POOL_OFF        { -3856,   -6,   "DHCP Pool for SSID is not enabled. "                                          "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_ENB_DHCP_SHARED          { -3857,   -7,   "DHCP Pool for SSID is not unique. "                                           "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_ENB_GATEWAY              { -3858,   -1,   "General error occurred. "                                                     "Unable to update Web Page Redirect gateway"   }
#define ERR_WPR_WRITE_ENB_DHCP_RANGE           { -3859,   -1,   "General error occurred. "                                                     "Unable to update Web Page Redirect DHCP range"}
#define ERR_WPR_WRITE_ENB_NO_DHCP_DNS          { -3860,   -8,   "DHCP Pool for SSID does not have DNS defined. "                               "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_ENB_BAD_NATIVE           { -3861,   -1,   "SSID VLAN is the native VLAN. "                                               "Web Page Redirect cannot be enabled"          }
#define ERR_WPR_WRITE_FILE_FAIL                { -3862,   -1,   "Error writing WPR configuration file. "                                       "Cannot update WPR information"                }
#define ERR_WPR_WRITE_OOPME_FAIL               { -3869,   -1,   "General error occurred. "                                                     "Cannot update WPR information"                }

// XRP_TARGET   (-39xx)
#define ERR_XRP_TARGET_ADD_LIST_FULL           { -3910,   -2,   "Fast roaming target list full. "                                              "Unable to add fast roaming target"            }
#define ERR_XRP_TARGET_ADD_BAD_OUI             { -3911,   -3,   "Fast roaming target MAC address must start with a valid OUI. "                "Unable to add fast roaming target"            }
#define ERR_XRP_TARGET_ADD_BAD_LSB             { -3912,   -4,   "Fast roaming target MAC address must end with a 0. "                          "Unable to add fast roaming target"            }
#define ERR_XRP_TARGET_DEL_UNDEFINED           { -3920,   -2,   "Undefined fast roaming target. "                                              "Cannot delete fast roaming target"            }
#define ERR_XRP_TARGET_READ_FAIL               { -3940,   -1,                                                                                  "Undefined XRP target"                         }
#define ERR_XRP_TARGET_WRITE_BAD_TYPE          { -3950,   -1,   "Invalid fast roaming target type. "                                           "Cannot add or edit fast roaming target"       }
#define ERR_XRP_TARGET_WRITE_OOPME_FAIL        { -3959,   -1,   "General error occurred. "                                                     "Cannot add or edit fast roaming target"       }

// USER GROUPs   (-40xx)
#define ERR_GROUP_ADD_TABLE_FULL               { -4010,   -1,   "User Group table full. "                                                      "Cannot add User Group"                        }
#define ERR_GROUP_READ_FAIL                    { -4011,   -1,                                                                                  "Undefined User Group"                         }
#define ERR_GROUP_DEL_UNDEFINED                { -4020,   -1,   "Undefined User Group. "                                                       "Cannot delete User Group"                     }
#define ERR_GROUP_DEL_RADIUS_USERS             { -4021,   -2,   "Internal RADIUS users are defined for this User Group. "                      "They will no longer be part of a User Group"  }
#define ERR_GROUP_DEL_OOPME_FAIL               { -4025,   -1,   "General error occurred. "                                                     "Cannot enable User Group"                     }
#define ERR_GROUP_WRITE_DHCP_POOL_FAIL         { -4051,   -2,                                                                                  "Undefined DHCP Pool"                          }
#define ERR_GROUP_WRITE_WPR_BAD_VLAN           { -4052,   -2,   "Web Page Redirect VLAN number must be unique. "                               "Cannot set VLAN"                              }
#define ERR_GROUP_WRITE_BAD_DHCP_POOL          { -4053,   -3,   "Web Page Redirect or NAT subnet conflict exists. "                            "Cannot assign DHCP Pool"                      }
#define ERR_GROUP_WRITE_WPR_BAD_NATIVE         { -4054,   -2,   "Web Page Redirect VLAN cannot be the native VLAN. "                           "Cannot set VLAN"                              }
#define ERR_GROUP_WRITE_OOPME_FAIL             { -4059,   -1,   "General error occurred. "                                                     "Cannot add or edit User Group"                }
#define ERR_GROUP_WRITE_ENB_BAD_SUBNET         { -4062,   -4,   "Web Page Redirect or NAT subnet conflict exists. "                            "User Group cannot be enabled"                 }
#define ERR_GROUP_WRITE_ENB_BAD_VLAN           { -4063,   -2,   "Web Page Redirect VLAN conflict exists. "                                     "User Group cannot be enabled"                 }
#define ERR_GROUP_WRITE_ENB_NO_DHCP_POOL       { -4064,   -5,   "Web Page Redirect requires DHCP. No DHCP Pool selected for Group. "           "User Group cannot be enabled"                 }
#define ERR_GROUP_WRITE_ENB_NO_DHCP_NAT        { -4065,   -8,   "Web Page Redirect requires NAT. DHCP Pool for Group is not NAT enabled. "     "User Group cannot be enabled"                 }
#define ERR_GROUP_WRITE_ENB_DHCP_POOL_OFF      { -4066,   -6,   "Web Page Redirect requires DHCP. DHCP Pool for Group is not enabled. "        "User Group cannot be enabled"                 }
#define ERR_GROUP_WRITE_ENB_DHCP_SHARED        { -4067,   -7,   "Web Page Redirect DHCP Pool conflict exists. "                                "User Group cannot be enabled"                 }
#define ERR_GROUP_WRITE_ENB_NO_DHCP_DNS        { -4068,   -9,   "Web Page Redirect requires DNS. DHCP Pool for Group does not have DNS set. "  "User Group cannot be enabled"                 }
#define ERR_GROUP_WRITE_ENB_BAD_NATIVE         { -4069,   -2,   "Web Page Redirect VLAN cannot be the native VLAN. "                           "User Group cannot be enabled"                 }
#define ERR_GROUP_WRITE_BAD_XRP_LAYER          { -4070,   -1,   "Layer 3 fast roaming must first be enabled in IAP global settings. "          "Cannot set User Group fast roaming layer"     }
#define ERR_GROUP_WRITE_BAD_FILTER_LIST        { -4071,   -2,   "Undefined Filter List. "                                                      "Cannot set Filter List"                       }
#define ERR_GROUP_WRITE_GLB_FILTER_LIST        { -4072,   -2,   "Global Filter List already applied to all User Groups by default. "           "Cannot set Filter List"                       }
#define ERR_GROUP_WRITE_BAD_VLAN               { -4073,   -2,   "Undefined VLAN. "                                                             "Cannot set User Group VLAN"                   }
#define ERR_GROUP_WPR_WLIST_ADD_FULL           { -4074,   -1,   "Whitelist table is full. "                                                    "Cannot add domain name"                       }
#define ERR_GROUP_WPR_WLIST_ADD_EXISTS         { -4075,   -1,   "Domain name already in whitelist. "                                           "Cannot add domain name"                       }
#define ERR_GROUP_WPR_WLIST_DEL_UNDEFINED      { -4076,   -1,   "Undefined whitelist entry. "                                                  "Cannot delete domain name"                    }
#define ERR_GROUP_WPR_WLIST_INVALID_ENTRY      { -4077,   -1,                                                                                  "Invalid wildcard in domain name"              }
#define ERR_GROUP_CLEAR_VLAN_FAIL              { -4078,   -1,   "General error occurred. "                                                     "Cannot clear User Group VLAN"                 }
#define ERR_GROUP_CLEAR_VLAN_POOL_FAIL         { -4079,   -1,   "General error occurred. "                                                     "Cannot clear User Group VLAN Pool"            }
#define ERR_GROUP_DHCP_PLUS_VLAN_POOL          { -4080,   -1,   "DHCP Pool is not compatible with VLAN POOL. "                                 "Cannot assign DHCP Pool"                      }
#define ERR_GROUP_VLAN_PLUS_DHCP_POOL          { -4081,   -1,   "VLAN Pool is not compatible with DHCP POOL. "                                 "Cannot assign VLAN Pool"                      }
#define ERR_GROUP_WRITE_NAME_DUPLICATE         { -4082,   -1,   "User Group names must be unique. "                                            "Cannot change User Group name"                }

// UBOOT ENVIRONMENT (-41xx)
#define ERR_UBOOT_WRITE_ENV_FULL               { -4150,   -1,   "Boot environment full. "                                                      "Cannot set or modify environment variable"    }
#define ERR_UBOOT_WRITE_ENV_PROTECTED          { -4151,   -2,   "Protected environment variable. "                                             "Cannot set or modify environment variable"    }
#define ERR_UBOOT_WRITE_ENV_FAIL               { -4152,   -3,   "General error occurred. "                                                     "Cannot set or modify environment variable"    }
#define ERR_UBOOT_WRITE_ENV_SAVE_FAIL          { -4153,   -4,   "General error occurred. "                                                     "Unable to save boot loader environment"       }
#define ERR_UBOOT_WRITE_ENV_VAR_TOO_LONG       { -4154,   -5,   "Boot environment variable too long. "                                         "Cannot set or modify environment variable"    }
#define ERR_UBOOT_WRITE_ENV_CORRUPT            { -4155,   -6,   "Boot environment corrupted. "                                                 "Cannot set or modify environment variable"    }

// FILTER LISTs   (-42xx)
#define ERR_FILTER_LIST_ADD_TABLE_FULL         { -4210,   -1,   "Filter List table full. "                                                     "Cannot add Filter List"                       }
#define ERR_FILTER_LIST_ADD_TABLE_BAD_NAME     { -4211,   -1,   "Invalid Filter List name. "                                                   "Cannot add Filter List"                       }
#define ERR_FILTER_LIST_DEL_UNDEFINED          { -4220,   -1,   "Undefined Filter List. "                                                      "Cannot delete Filter List"                    }
#define ERR_FILTER_LIST_DEL_OOPME_FAIL         { -4225,   -1,   "General error occurred. "                                                     "Cannot enable Filter List"                    }
#define ERR_FILTER_LIST_BAD                    { -4241,   -1,                                                                                  "Undefined Filter List"                        }

// NetFlow (-43xx)
#define ERR_NETFLOW_FAIL_START                 { -4300,   -1,   "NetFlow probe failed to start. Verify the collector address or host name. "   "Cannot run NetFlow"                           }

// Cluster   (-44xx)
#define ERR_CLUST_ADD_TABLE_FULL               { -4410,   -1,   "Cluster table full. "                                                         "Cannot add Cluster"                           }
#define ERR_CLUST_DEL_UNDEFINED                { -4420,   -1,   "Undefined Cluster. "                                                          "Cannot delete Cluster"                        }
#define ERR_CLUST_UNDEFINED                    { -4430,   -1,   "Undefined Cluster. "                                                          "Cannot find Cluster"                          }
#define ERR_CLUST_ARRAY_ADD_TABLE_FULL         { -4411,   -1,   "Cluster table full. "                                                         "Cannot add Member to Cluster"                 }
#define ERR_CLUST_ARRAY_DEL_UNDEFINED          { -4421,   -1,   "Undefined Cluster Member. "                                                   "Cannot delete Member from Cluster"            }
#define ERR_CLUST_ARRAY_UNDEFINED              { -4431,   -1,   "Undefined Cluster Member. "                                                   "Cannot find Member in Cluster"                }
#define ERR_CLUST_CMD_FAILED                   { -4440,   -1,   "General error occurred. "                                                     "Cluster command failed"                       }
#define ERR_CLUST_CONNECT_FAILED               { -4441,   -1,   "General error occurred. "                                                     "Cluster connect failed"                       }
#define ERR_CLUST_LOGIN_FAILED                 { -4442,   -1,   "General error occurred. "                                                     "Cluster login failed"                         }
#define ERR_CLUST_DISCONNECT_FAILED            { -4443,   -1,   "General error occurred. "                                                     "Cluster disconnect failed"                    }
#define ERR_CLUST_ARRAY_REDIRECT               { -4444,   -1,   "Undefined Cluster Member. "                                                   "Cannot redirect command to member"            }
#define ERR_CLUST_REV_MISMATCH                 { -4445,   -1,   "Cluster revision level mismatch. "                                            "Cannot execute cluster command"               }

// WiFi Tag  / Location (Euclid) (-45xx)
#define ERR_WIFI_TAG_WRITE_OOPME_FAIL          { -4550,   -1,   "General error occurred. "                                                     "WiFi tag configuration failed"                }
#define ERR_LOCATION_WRITE_OOPME_FAIL          { -4551,   -1,   "General error occurred. "                                                     "Location server configuration failed"         }

// RF Monitor (-46xx)
#define ERR_IAP_CHANGE_RFM_STANDBY             { -4651,   -1,   "Error occurred disabling standby mode. "                                      "Unable to disable RF monitor"                 }
#define ERR_IAP_CHANGE_RFM_AUTOCELL            { -4652,   -1,   "Error occurred disabling autocell mode. "                                     "Unable to disable RF monitor"                 }
#define ERR_IAP_CHANGE_RFM_ASSURANCE           { -4653,   -1,   "Error occurred disabling radio assurance. "                                   "Unable to disable RF monitor"                 }

// Station Assurance (-47xx)
#define ERR_STA_ASSURE_WRITE_OOPME_FAIL        { -4750,   -1,   "General error occurred. "                                                     "Station assurance configuration failed"       }

// Tunnel (-48xx)
#define ERR_TUNNEL_NOT_FOUND                   { -4800,   -2,   "Tunnel not found. "                                                           "Could not find tunnel"                        }
#define ERR_TUNNEL_ADD_TABLE_FULL              { -4810,   -2,   "Tunnel table full. "                                                          "Cannot add tunnel"                            }
#define ERR_TUNNEL_ADD_TABLE_BAD_NAME          { -4811,   -2,   "Invalid Tunnel name. "                                                        "Cannot add tunnel"                            }
#define ERR_TUNNEL_DEL_UNDEFINED               { -4820,   -2,   "Undefined Tunnel. "                                                           "Cannot delete tunnel"                         }
#define ERR_TUNNEL_DEL_IN_USE_MGMT             { -4821,   -2,   "Tunnel is configured as default route. "                                      "Default route will be cleared"                }
#define ERR_TUNNEL_READ_FAIL                   { -4840,   -1,                                                                                  "Undefined tunnel"                             }
#define ERR_TUNNEL_WRITE_BAD_LOCAL_REMOTE      { -4849,   -1,   "Tunnel local/remote endpoint pair should be different for each tunnel. "      "Cannot edit tunnel parameters"                }
#define ERR_TUNNEL_WRITE_SET_IP                { -4850,   -1,   "Error setting Tunnel IP address. "                                            "Undefined tunnel"                             }
#define ERR_TUNNEL_WRITE_BAD_SSID_MASK         { -4851,   -1,   "Tunnel SSIDs list cannot overlap with other tunnel SSIDs. "                   "Cannot edit tunnel parameters"                }
#define ERR_TUNNEL_WRITE_BAD_ADDR              { -4852,   -1,   "Invalid IP address. "                                                         "Cannot edit tunnel parameters"                }
#define ERR_TUNNEL_WRITE_BAD_MASK              { -4853,   -1,   "Invalid IP mask. "                                                            "Cannot edit tunnel parameters"                }
#define ERR_TUNNEL_WRITE_DEFAULT_FAIL          { -4854,   -2,                                                                                  "Undefined tunnel"                             }
#define ERR_TUNNEL_WRITE_IFACE_FAIL            { -4855,   -1,                                                                                  "Cannot access tunnel interface"               }
#define ERR_TUNNEL_ADD_FAILED                  { -4856,   -1,   "General error occurred. "                                                     "Unable to create tunnel"                      }
#define ERR_TUNNEL_FAIL                        { -4879,   -1,   "General error occurred. "                                                     "Unable to update tunnel settings"             }
#define ERR_TUNNEL_ENABLE_NO_DESTINATION       { -4880,   -1,   "No destination defined. Cannot enable tunnel. "                               "Undefined destination"                        }
#define ERR_TUNNEL_ENABLE_FAILED               { -4881,   -1,   "Tunnel could not be created."                                                 ""                                             }
#define ERR_TUNNEL_DUPLICATE_PORT              { -4882,   -1,   "A tunnel exists with a duplicate port. "                                      "Cannot enable tunnel"                         }
#define ERR_TUNNEL_VPN_SERVER_TLS              { -4883,   -1,   "Openvpn server mode requires TLS security. "                                  "Cannot enable tunnel"                         }
#define ERR_TUNNEL_VTUN_NO_SECRET              { -4884,   -1,   "VTUN tunnels require a secret. "                                              "Cannot enable tunnel"                         }
#define ERR_TUNNEL_VPN_TLS_CA_NOT_FOUND        { -4885,   -1,   "VPN TLS Certificate Authority file not found. "                               "Cannot enable tunnel"                         }
#define ERR_TUNNEL_VPN_TLS_CERT_NOT_FOUND      { -4886,   -1,   "VPN TLS certificate file not found. "                                         "Cannot enable tunnel"                         }
#define ERR_TUNNEL_VPN_TLS_KEY_NOT_FOUND       { -4887,   -1,   "VPN TLS private key file not found. "                                         "Cannot enable tunnel"                         }
#define ERR_TUNNEL_WRITE_DHCP_OPTION_ENB_FAIL  { -4888,   -1,   "DHCP Option 82 cannot be enabled if SSID-base DHCP Option is enabled. "       "Cannot enable DHCP Option 82 for this tunnel" }

// Roaming Assistance (-49xx)
#define ERR_ROAM_ASSIST_WRITE_OOPME_FAIL       { -4950,   -1,   "General error occurred. "                                                     "Roaming assist configuration failed"          }

// Multicast Lists (-51xx)
#define ERR_MC_ADD_TABLE_FULL                  { -5110,   -1,   "Multicast table full. "                                                       "Cannot add multicast address"                 }
#define ERR_MC_VLAN_TABLE_FULL                 { -5111,   -1,   "Multicast forwarding VLAN table full. "                                       "Cannot add multicast forward VLAN"            }
#define ERR_MDNS_ADD_TABLE_FULL                { -5112,   -1,   "mDNS filter table full. "                                                     "Cannot add mDNS filter"                       }

// WPR (-52xx)
#define ERR_PORTAL_ADD_TABLE_FULL              { -5201,   -1,   "Portal table full. "                                                          "Cannot add Portal"                            }
#define ERR_PORTAL_DEL_UNDEFINED               { -5202,   -1,   "Undefined Portal. "                                                           "Cannot delete Portal"                         }
#define ERR_PORTAL_UNDEFINED                   { -5203,   -1,   "Undefined Portal. "                                                           "Cannot find Portal"                           }

// 802.11k/u/r (-53xx)
#define ERR_11U_ROAM_CONSORTIUM_ADD_LIST_FULL  { -5300,   -1,   "Roaming consortium list full. "                                               "Cannot add roaming consortium OI"             }
#define ERR_11U_ROAM_CONSORTIUM_DEL_UNDEFINED  { -5301,   -1,   "Undefined roaming consortium OI. "                                            "Cannot delete roaming consortium OI"          }
#define ERR_11U_DOMAIN_ADD_LIST_FULL           { -5302,   -1,   "Domain name list full. "                                                      "Cannot add domain name"                       }
#define ERR_11U_DOMAIN_DEL_UNDEFINED           { -5303,   -1,   "Undefined domain name. "                                                      "Cannot delete domain name"                    }
#define ERR_11U_CELL_NETWORK_ADD_LIST_FULL     { -5304,   -1,   "Cellular network information list full. "                                     "Cannot add cellular network"                  }
#define ERR_11U_CELL_NETWORK_DEL_UNDEFINED     { -5305,   -1,   "Undefined cellular network. "                                                 "Cannot delete cellular network"               }
#define ERR_11U_NAI_REALM_ADD_LIST_FULL        { -5306,   -1,   "NAI realm list full. "                                                        "Cannot add NAI realm"                         }
#define ERR_11U_NAI_REALM_DEL_UNDEFINED        { -5307,   -1,   "Undefined NAI realm. "                                                        "Cannot delete NAI realm"                      }
#define ERR_11U_NAI_REALM_READ_FAIL            { -5308,   -1,                                                                                  "Undefined NAI realm"                          }
#define ERR_11U_NET_AUTH_TYPE_DEL_UNDEFINED    { -5309,   -1,   "Undefined network authentication type. "                                      "Cannot delete network authentication type"    }
#define ERR_11U_VENUE_NAME_ADD_LIST_FULL       { -5310,   -1,   "Venue name list full. "                                                       "Cannot add venue name"                        }
#define ERR_11U_VENUE_NAME_DEL_UNDEFINED       { -5311,   -1,   "Undefined venue name. "                                                       "Cannot delete venue name"                     }
#define ERR_HS20_CONN_CAP_ADD_LIST_FULL        { -5312,   -1,   "Connection capability list full. "                                            "Cannot add connection capability"             }
#define ERR_HS20_CONN_CAP_DEL_UNDEFINED        { -5313,   -1,   "Undefined connection capability. "                                            "Cannot delete connection capability"          }
#define ERR_HS20_CONN_CAP_DEL_DEFAULT          { -5314,   -1,   "Default connection capability. "                                              "Cannot delete connection capability"          }
#define ERR_HS20_CONN_CAP_EDIT_DEFAULT         { -5315,   -1,   "Default connection capability. "                                              "Cannot edit connection capability"            }
#define ERR_HS20_CONN_CAP_READ_FAIL            { -5316,   -1,                                                                                  "Undefined connection capability"              }
#define ERR_HS20_OPER_NAME_DEL_UNDEFINED       { -5317,   -1,   "Undefined operator friendly name. "                                           "Cannot delete operator friendly name"         }
#define ERR_11K_DSB_11K_11R_IN_USE             { -5318,   -1,   "802.11k is recommended when using 802.11r, which is enabled on one or more SSIDs. Will not disable 802.11k support"          }
#define ERR_11R_ENB_11R_BAD_CRYPTO             { -5319,   -1,   "SSID must support WPA2. "                                                     "Cannot enable 802.11r support"                }

// MDM (-54xx)
#define ERR_MDM_WRITE_NO_AW_API_URL            { -5400,   -1,   "AirWatch MDM authentication requires AirWatch API URL to be set. "              "Cannot set SSID MDM authentication to AirWatch" }
#define ERR_MDM_WRITE_NO_AW_API_KEY            { -5401,   -1,   "AirWatch MDM authentication requires AirWatch API key to be set. "              "Cannot set SSID MDM authentication to AirWatch" }
#define ERR_MDM_WRITE_NO_AW_API_USERNAME       { -5402,   -1,   "AirWatch MDM authentication requires AirWatch API username to be set. "         "Cannot set SSID MDM authentication to AirWatch" }
#define ERR_MDM_WRITE_NO_AW_API_PASSWORD       { -5403,   -1,   "AirWatch MDM authentication requires AirWatch API password to be set. "         "Cannot set SSID MDM authentication to AirWatch" }
#define ERR_MDM_WRITE_NO_AW_REDIRECT_URL       { -5404,   -1,   "AirWatch MDM authentication requires AirWatch station redirect URL to be set. " "Cannot set SSID MDM authentication to AirWatch" }
#define ERR_MDM_WRITE_OOPME_FAIL               { -5405,   -1,   "General error occurred. "                                                       "MDM configuration failed"                       }
#define ERR_MDM_WRITE_BAD_API_USERNAME         { -5406,   -1,   "AirWatch API username invalid. "                                                "Cannot set AirWatch API username"               }
#define ERR_MDM_WRITE_BAD_API_KEY              { -5407,   -1,   "AirWatch API key invalid. "                                                     "Cannot set AirWatch API key"                    }

// License (-55xx)
#define ERR_LICENSE_REQUIRES_11N               { -5550,   -1,   "Requires 802.11n "               "feature set license. Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_REQUIRES_11AC              { -5551,   -1,   "Requires 802.11ac "              "feature set license. Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_REQUIRES_RPM               { -5552,   -1,   "Requires RF Performance Manager ""feature set license. Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_REQUIRES_RAM               { -5553,   -1,   "Requires RF Analysis " "Manager ""feature set license. Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_REQUIRES_RSM               { -5554,   -1,   "Requires RF Security " "Manager ""feature set license. Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_REQUIRES_PSB               { -5555,   -1,   "Requires Public Safety Band "    "feature set license. Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_REQUIRES_APP               { -5556,   -1,   "Requires Application Control "   "feature set license. Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_EXCEEDS_IAP_CNT            { -5557,   -1,   "Enabled IAPs exceed licensed IAP count. "             "Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_EXCEEDS_CHAINS             { -5558,   -1,   "TX/RX chains exceed licensed spatial streams. "       "Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_EXCEEDED                   { -5590,   -1,   "Software version requires an updated license key. "   "Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_EXPIRED                    { -5591,   -1,   "Temporary license key expired. "                      "Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_INVALID                    { -5592,   -1,   "License key is invalid. "                             "Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_FAIL                       { -5593,   -1,   "License key feature set mismatch. "                   "Please contact %s Customer Support (support@%s.com) to request a new license key"}
#define ERR_LICENSE_DOWNGRADE                  { -5594,   -1,   "New license key software version is less than current license key software version. " "Must use 'force' option to downgrade license key"}

// HW Requirements (-56xx)
#define ERR_SYSTEM_REQUIRES_XR600              { -5650,   -1,   "Features requires a 600 series or greater controller chassis. "       "This hardware does not support the feature requested" }
#define ERR_SYSTEM_REQUIRES_XR1000             { -5651,   -1,   "Features requires a 1000 series or greater controller chassis. "      "This hardware does not support the feature requested" }
#define ERR_SYSTEM_REQUIRES_XR2000             { -5652,   -1,   "Features requires a 2000 series or greater controller chassis. "      "This hardware does not support the feature requested" }
#define ERR_SYSTEM_REQUIRES_XR4000             { -5653,   -1,   "Features requires a 4000 series or greater controller chassis. "      "This hardware does not support the feature requested" }
#define ERR_SYSTEM_REQUIRES_XR6000             { -5654,   -1,   "Features requires a 6000 series or greater controller chassis. "      "This hardware does not support the feature requested" }
#define ERR_SYSTEM_REQUIREMENT_FAIL            { -5659,   -1,   "Feature / system requirement mismatch. "                              "This hardware does not support the feature requested" }
#define ERR_SYSTEM_FEATURE_NOT_IMPLEMENTED     { -5660,   -1,   "Feature not yet implemented. "                                        "This hardware does not support the feature requested" }

// Cloud/XMS/Auto-Discovery (-57xx)
#define ERR_CLOUD_WRITE_FAIL                   { -5700,   -1,   "General error occurred. "                                                     "Cloud configuration failed"                   }
#define ERR_CLOUD_FIRMWARE_SERVER_NOFLASH      { -5701,   -1,   "Firmware-server unavailable when flash disabled. "                            "Firmware-server activation failed."           }
#define ERR_XMS_E_WRITE_FAIL                   { -5702,   -1,   "General error occurred. "                                                     "XMS-E configuration failed"                   }
#define ERR_AUTO_DISC_WRITE_FAIL               { -5703,   -1,   "General error occurred. "                                                     "Auto-Discovery configuration failed"          }

// OAUTH        (-58xx)
#define ERR_OAUTH_ADD_TABLE_FULL               { -5800,   -1,   "OAUTH table full. "                                                           "Cannot add OAUTH entry"                       }
#define ERR_OAUTH_READ_FAIL                    { -5801,   -1,   "General error occurred. "                                                     "OAUTH read configuration failed"              }
#define ERR_OAUTH_DEL_UNDEFINED                { -5802,   -2,   "Undefined OAUTH token. "                                                      "Cannot delete OAUTH token"                    }

// Web Proxy (-59xx)
#define ERR_PROXY_FWD_WRITE_FAIL               { -5900,   -1,   "General error occurred. "                                                     "ProxyFwd configuration write failed"          }
#define ERR_PROXY_FWD_WRITE_OOPME_FAIL         { -5901,   -1,   "General error occurred. "                                                     "ProxyFwd OOPME configuration failed"          }
#define ERR_PROXY_MGMT_WRITE_OOPME_FAIL        { -5902,   -1,   "General error occurred. "                                                     "ProxyMgmt OOPME configuration failed"         }

// Active Directory (-60xx)
#define ERR_ACTIVE_DIRECTORY                   { -6000,   -1,   "General error occurred. "                                                     "Active Directory configuration write failed"  }
#define ERR_ACTIVE_DIRECTORY_JOIN_MISSING      { -6001,   -1,   "Missing Active Directory configuration items needed to join domain."          "Active Directory join failed"                 }
#define ERR_ACTIVE_DIRECTORY_LEAVE_MISSING     { -6002,   -1,   "Missing Active Directory configuration items needed to leave domain."         "Active Directory leave failed"                }
#define ERR_ACTIVE_DIRECTORY_JOIN_FAIL         { -6003,   -1,   "Request to join domain failed. Check Active Directory configuration. "        "Active Directory join failed"                 }
#define ERR_ACTIVE_DIRECTORY_LEAVE_FAIL        { -6004,   -1,   "Request to leave domain failed. Check Active Directory configuration. "       "Active Directory leave failed"                }
#define ERR_ACTIVE_DIRECTORY_LONG_HOSTNAME     { -6005,   -1,   "System hostname exceeds maximum NetBIOS name length of 15 characters. "       "Active Directory join failed"                 }
#define ERR_ACTIVE_DIRECTORY_CHAP_INCOMPAT     { -6006,   -1,   "Active Directory is not compatible with CHAP authentication. "                "Cannot modify setting"                        }
#define ERR_ACTIVE_DIRECTORY_HOSTNAME_AD_LEN   { -6007,   -1,   "System hostname exceeds maximum NetBIOS name length of 15 characters. "       "Active Directory Warning!"                    }

// Proxy Mamagement (-61xx)
#define ERR_PROXY_MGMT                         { -6100,   -1,   "General error occurred. "                                                     "Proxy Management configuration write failed"  }
#define ERR_PROXY_MGMT_CFG_MISSING             { -6101,   -1,   "Missing Proxy Management configuration items."                                "Proxy Management configuration write failed"  }
#define ERR_PROXY_MGMT_SOCKSNET_MISSING        { -6102,   -1,   "Socks Proxies require at least one subnet declaration."                       "Proxy Management socks configuration failed"  }
#define ERR_PROXY_MGMT_HTTP_ENV_FILE           { -6103,   -1,   "Unable to write http env file."                                               "Proxy Management configuration write failed"  }
#define ERR_PROXY_MGMT_SOCKS_CFG_FILE          { -6104,   -1,   "Unable to write socks config file."                                           "Proxy Management configuration write failed"  }

// App Lists   (-62xx)
#define ERR_APP_LIST_ADD_TABLE_FULL            { -6210,   -1,   "Application List table full. "                                                "Cannot add Application List"                  }
#define ERR_APP_LIST_ADD_TABLE_BAD_NAME        { -6211,   -1,   "Invalid Application List name. "                                              "Cannot add Application List"                  }
#define ERR_APP_LIST_ADD_NOT_UNIQUE            { -6222,   -1,   "Application List names must be unique across all applications & lists. "      "Cannot add Application List"                  }
#define ERR_APP_LIST_DEL_UNDEFINED             { -6220,   -1,   "Undefined Application List. "                                                 "Cannot delete Application List"               }
#define ERR_APP_LIST_APP_NOT_UNIQUE            { -6240,   -1,   "Applications must be unique across all application lists. "                   "Cannot add Application to List"               }
#define ERR_APP_LIST_BAD_APP                   { -6241,   -1,   "Undefined Application. "                                                      "Cannot add Application to List"               }
#define ERR_APP_LIST_BAD                       { -6242,   -1,                                                                                  "Undefined Application List"                   }

// LLDP (-63xx)
#define ERR_LLDP_WRITE_DISABLE                 { -6300,   -1,   "Fabric Attach requires LLDP to be enabled. "                                  "Cannot disable LLDP"                          }
#define ERR_LLDP_WRITE_FA_ENABLE               { -6301,   -1,   "Error occurred enabling LLDP. "                                               "Unable to enable Fabric Attach"               }
#define ERR_LLDP_WRITE_BAD_PWR_REQ             { -6302,   -1,   "Model does not support standard PoE. "                                        "Unable to enable LLDP power request"          }
#define ERR_LLDP_WRITE_FILE_FAIL               { -6303,   -1,   "Error writing LLDP configuration file. "                                      "Cannot update LLDP information"               }
#define ERR_LLDP_WRITE_FA_CFG_FAIL             { -6304,   -1,   "General error occurred. "                                                     "Cannot update Fabric Attach information"      }

// Activation (-64xx)
#define ERR_URL_PROTOCOL                       { -6400,   -1,   "URL must begin with http:// or https:// "                                     "Cannot set URL string"                        }
#define ERR_URL_CHAR                           { -6401,   -1,   "Invalid character in URL "                                                    "Cannot set URL string"                        }
#define ERR_URL_TOO_LONG                       { -6402,   -1,   "URL length is limited to 255 characters "                                     "Cannot set URL string"                        }
#define ERR_URL_SETTING_BOOTARG                { -6403,   -1,   "Unable to set URL in bootargs "                                               "Cannot set URL string"                        }

// Multicast Fixed Address (-65xx)
#define ERR_FIXED_ADDR_ADD_LIST_FULL           { -6510,   -2,   "Multicast fixed address list full. "                                          "Unable to add multicast fixed address"        }
#define ERR_FIXED_ADDR_INVALID_GROUP           { -6511,   -2,   "Undefined user group. "                                                       "Unable to add multicast fixed address"        }
#define ERR_FIXED_ADDR_DEL_UNDEFINED           { -6520,   -2,   "Undefined multicast fixed address. "                                          "Cannot delete fmulticast fixed address"       }

//  Device ID MAC Table (-66xx)
#define ERR_DEVICE_ID_ADD_TABLE_FULL           { -6610,   -1,   "Device ID MAC table full. "                                                   "Cannot add Device ID MAC entry"               }
#define ERR_DEVICE_ID_ADD_INCOMPLETE           { -6611,   -1,   "Incomplete Device ID MAC entry. "                                             "Cannot add Device ID MAC entry"               }
#define ERR_DEVICE_ID_ADD_BAD_CLASS            { -6612,   -1,   "Invalid Device ID Class. "                                                    "Cannot add Device ID MAC entry"               }
#define ERR_DEVICE_ID_ADD_NOT_EDIT             { -6613,   -1,   "Must use 'add' " "to create a Device ID MAC entry, entry does not exist. "    "Cannot edit Device ID MAC entry"              }
#define ERR_DEVICE_ID_EDIT_NOT_ADD             { -6614,   -1,   "Must use 'edit' ""to modify a Device ID MAC entry, entry already exists. "    "Cannot add Device ID MAC entry"               }
#define ERR_DEVICE_ID_DEL_BAD_ENTRY            { -6620,    1,   "Device ID MAC entry does not exist. "                                         "Cannot delete Device ID MAC entry"            }
#define ERR_DEVICE_ID_WRITE_OOPME_FAIL         { -6659,   -1,   "General error occurred. "                                                     "Cannot add or edit Device ID MAC entry"       }

// SID          (-70xx)
#define ERR_SID_NONE                           { -7000,   -1,   "General error occurred. "                                                     "Invalid information request"                  }
#define ERR_SID_ETH_STATS                      { -7001,   -1,   "General error occurred. "                                                     "Unable to read ethernet statistics"           }
#define ERR_SID_RADIO_STATS                    { -7002,   -1,   "General error occurred. "                                                     "Unable to read IAP statistics"                }
#define ERR_SID_SUMMARY                        { -7003,   -1,   "General error occurred. "                                                     "Unable to read module summary"                }
#define ERR_SID_STA_TABLE                      { -7004,   -1,   "General error occurred. "                                                     "Unable to read station table"                 }
#define ERR_SID_NUM_STATIONS                   { -7005,   -1,   "General error occurred. "                                                     "Unable to read number of associated stations" }
#define ERR_SID_CHANNELS                       { -7006,   -1,   "General error occurred. "                                                     "Unable to read channel list"                  }
#define ERR_SID_LOG                            { -7007,   -1,   "General error occurred. "                                                     "Unable to read syslog file"                   }
#define ERR_SID_DRIVER_VERSION                 { -7008,   -1,   "General error occurred. "                                                     "Unable to read driver version"                }
#define ERR_SID_STA_STATS                      { -7009,   -1,   "General error occurred. "                                                     "Unable to read station statistics"            }
#define ERR_SID_GEOGRAPHY_INFO                 { -7010,   -1,   "General error occurred. "                                                     "Unable to read geography information"         }
#define ERR_SID_IDS_DATABASE                   { -7011,   -1,   "General error occurred. "                                                     "Unable to read rogue classification database" }
#define ERR_SID_WDS_CLIENT_LINKS               { -7012,   -1,   "General error occurred. "                                                     "Unable to read WDS client link information"   }
#define ERR_SID_VLAN_STATS                     { -7013,   -1,   "General error occurred. "                                                     "Unable to read VLAN statistics"               }
#define ERR_SID_SPANNING_TREE                  { -7014,   -1,   "General error occurred. "                                                     "Unable to read spanning tree status"          }
#define ERR_SID_NEIGHBOR_ARRAY_INFO            { -7015,   -1,   "General error occurred. "                                                     "Unable to read network map information"       }
#define ERR_SID_ROGUE_AP_TABLE                 { -7016,   -1,   "General error occurred. "                                                     "Unable to read rogue AP table"                }
#define ERR_SID_WDS_STATUS                     { -7017,   -1,   "General error occurred. "                                                     "Unable to read WDS status"                    }
#define ERR_SID_WDS_HOST_LINKS                 { -7018,   -1,   "General error occurred. "                                                     "Unable to read WDS host link information"     }
#define ERR_SID_STATS_TIME_PERIOD              { -7019,   -1,   "General error occurred. "                                                     "Unable to read statistics time period info"   }
#define ERR_SID_AUTOCELL_RSSI_TABLE            { -7020,   -1,   "General error occurred. "                                                     "Unable to read Auto Cell RSSI information"    }
#define ERR_SID_CFG_MESSAGES                   { -7021,   -1,   "General error occurred. "                                                     "Unable to read config messages"               }
#define ERR_SID_DHCPD_LEASE_TABLE              { -7022,   -1,   "General error occurred. "                                                     "Unable to read DHCP lease table"              }
#define ERR_SID_FLASH_FILE_LIST                { -7023,   -1,   "General error occurred. "                                                     "Unable to read flash file list"               }
#define ERR_SID_SPECTRUM_ANALYZER              { -7024,   -1,   "General error occurred. "                                                     "Unable to read spectrum analyzer data"        }
#define ERR_SID_UNASSOC_STA_TABLE              { -7025,   -1,   "General error occurred. "                                                     "Unable to read unassociated station table"    }
#define ERR_SID_PACKED_RADIO_STATS             { -7026,   -1,   "General error occurred. "                                                     "Unable to read IAP statistics"                }
#define ERR_SID_PACKED_AUTOCELL_RSSI_TABLE     { -7027,   -1,   "General error occurred. "                                                     "Unable to read Auto Cell RSSI information"    }
#define ERR_SID_MAX_STATIONS                   { -7028,   -1,   "General error occurred. "                                                     "Unable to read maximum number of stations"    }
#define ERR_SID_ADMIN_HISTORY                  { -7029,   -1,   "General error occurred. "                                                     "Unable to read admin history"                 }
#define ERR_SID_READ_FILE                      { -7030,   -1,   "General error occurred. "                                                     "Unable to read file"                          }
#define ERR_SID_FILE_SIZE                      { -7031,   -1,   "General error occurred. "                                                     "Unable to read file size"                     }
#define ERR_SID_FLASH_FILE_TABLE               { -7032,   -1,   "General error occurred. "                                                     "Unable to read flash file list"               }
#define ERR_SID_UBOOT_ENV_VAR_TABLE            { -7033,   -1,   "General error occurred. "                                                     "Unable to read boot environment variables"    }
#define ERR_SID_DBG_SETTINGS                   { -7034,   -1,   "General error occurred. "                                                     "Unable to read debug settings"                }
#define ERR_SID_NEIGHBOR_TUNNEL_INFO           { -7035,   -1,   "General error occurred. "                                                     "Unable to read neighbor tunnel information"   }
#define ERR_SID_ROAM_STA_TABLE                 { -7036,   -1,   "General error occurred. "                                                     "Unable to read roaming station table"         }
#define ERR_SID_NETWORK_ASSURANCE_INFO         { -7037,   -1,   "General error occurred. "                                                     "Unable to read network assurance status"      }
#define ERR_SID_STA_TYPE_CNTS                  { -7038,   -1,   "General error occurred. "                                                     "Unable to read station counts by media type"  }
#define ERR_SID_STA_TYPE_TOTAL                 { -7039,   -1,   "General error occurred. "                                                     "Unable to read station totals by media type"  }
#define ERR_SID_STA_TYPE_TABLE                 { -7040,   -1,   "General error occurred. "                                                     "Unable to read station table  by media type"  }
#define ERR_SID_ROGUE_AP_TYPE_CNTS             { -7041,   -1,   "General error occurred. "                                                     "Unable to read rogue AP type counts"          }
#define ERR_SID_ROGUE_AP_TYPE_TABLE            { -7042,   -1,   "General error occurred. "                                                     "Unable to read rogue AP type table"           }
#define ERR_SID_LOG_CNTS                       { -7043,   -1,   "General error occurred. "                                                     "Unable to read syslog file counts"            }
#define ERR_SID_IDS_EVENT_LOG                  { -7044,   -1,   "General error occurred. "                                                     "Unable to read IDS event log"                 }
#define ERR_SID_IDS_STATS                      { -7045,   -1,   "General error occurred. "                                                     "Unable to read IDS statistics"                }
#define ERR_SID_STA_ASSURANCE_INFO             { -7046,   -1,   "General error occurred. "                                                     "Unable to read station assurance information" }
#define ERR_SID_DEVICE_ID_TOKENS               { -7047,   -1,   "General error occurred. "                                                     "Unable to read device ID tokens"              }
#define ERR_SID_RADIO_ASSURE_CNTS              { -7048,   -1,   "General error occurred. "                                                     "Unable to read radio assurance information"   }
#define ERR_SID_ACTIVE_VLAN_TABLE              { -7049,   -1,   "General error occurred. "                                                     "Unable to read active vlan table"             }
#define ERR_SID_QUICK_CONFIG_LIST              { -7050,   -1,   "General error occurred. "                                                     "Unable to read quick config list"             }
#define ERR_SID_DPI_STATISTICS                 { -7051,   -1,   "General error occurred. "                                                     "Unable to read dpi statistics"                }
#define ERR_SID_NEIGHBOR_TRAFFIC_INFO          { -7052,   -1,   "General error occurred. "                                                     "Unable to read neighbor traffic information"  }
#define ERR_SID_SPECTRUM_ANALYZER_TRAFFIC      { -7053,   -1,   "General error occurred. "                                                     "Unable to read spectrum analyzer traffic"     }
#define ERR_SID_UNDEF_VLAN_TABLE               { -7054,   -1,   "General error occurred. "                                                     "Unable to read undefined vlan table"          }
#define ERR_SID_STA_LOCATION_TABLE             { -7054,   -1,   "General error occurred. "                                                     "Unable to read station location table"        }
#define ERR_SID_RAP_LOCATION_TABLE             { -7055,   -1,   "General error occurred. "                                                     "Unable to read rogue AP location table"       }
#define ERR_SID_NEIGHBOR_STA_TABLE             { -7056,   -1,   "General error occurred. "                                                     "Unable to read neighbor station table"        }
#define ERR_SID_NEIGHBOR_RAP_TABLE             { -7057,   -1,   "General error occurred. "                                                     "Unable to read neighbor rogue AP table"       }

#define ERR_SID_RFM_REQ_ANALYZER               { -7101,   -1,   "Requires RF monitor to be enabled and monitor IAP to be up. "                 "Unable to display Spectrum Analyzer.\n"       }

// SUMMARY      (-80xx)
#define ERR_SUMMARY_NONE                       { -8000,   -1,   "General error occurred. "                                                     "Invalid summary request"                      }
#define ERR_SUMMARY_RADIO                      { -8001,   -1,   "General error occurred. "                                                     "Unable to read IAP settings"                  }
#define ERR_SUMMARY_ETHERNET                   { -8002,   -1,   "General error occurred. "                                                     "Unable to read ethernet settings"             }
#define ERR_SUMMARY_SECURITY                   { -8003,   -1,   "General error occurred. "                                                     "Unable to read security settings"             }
#define ERR_SUMMARY_SSID                       { -8004,   -1,   "General error occurred. "                                                     "Unable to read SSID settings"                 }
#define ERR_SUMMARY_DHCP                       { -8005,   -1,   "General error occurred. "                                                     "Unable to read DHCP settings"                 }
#define ERR_SUMMARY_DNS                        { -8006,   -1,   "General error occurred. "                                                     "Unable to read DNS settings"                  }
#define ERR_SUMMARY_SNMP                       { -8007,   -1,   "General error occurred. "                                                     "Unable to read SNMP settings"                 }
#define ERR_SUMMARY_DATE_TIME                  { -8008,   -1,   "General error occurred. "                                                     "Unable to read date & time settings"          }
#define ERR_SUMMARY_SYSLOG                     { -8009,   -1,   "General error occurred. "                                                     "Unable to read syslog settings"               }
#define ERR_SUMMARY_RADIUS                     { -8010,   -1,   "General error occurred. "                                                     "Unable to read RADIUS settings"               }
#define ERR_SUMMARY_LOCAL_RADIUS               { -8011,   -1,   "General error occurred. "                                                     "Unable to read RADIUS server settings"        }
#define ERR_SUMMARY_ACL                        { -8012,   -1,   "General error occurred. "                                                     "Unable to read ACL settings"                  }
#define ERR_SUMMARY_ADMIN                      { -8013,   -1,   "General error occurred. "                                                     "Unable to read admin settings"                }
#define ERR_SUMMARY_VERSION                    { -8014,   -1,   "General error occurred. "                                                     "Unable to read version settings"              }
#define ERR_SUMMARY_RADIO_GLB                  { -8015,   -1,   "General error occurred. "                                                     "Unable to read IAP global settings"           }
#define ERR_SUMMARY_RADIO_GLB_A                { -8016,   -1,   "General error occurred. "                                                     "Unable to read IAP global .11a settings"      }
#define ERR_SUMMARY_RADIO_GLB_G                { -8017,   -1,   "General error occurred. "                                                     "Unable to read IAP global .11bg settings"     }
#define ERR_SUMMARY_CONSOLE                    { -8018,   -1,   "General error occurred. "                                                     "Unable to read console settings"              }
#define ERR_SUMMARY_CONTACT                    { -8019,   -1,   "General error occurred. "                                                     "Unable to read contact settings"              }
#define ERR_SUMMARY_FILTER                     { -8020,   -1,   "General error occurred. "                                                     "Unable to read filter settings"               }
#define ERR_SUMMARY_WDS_CLIENTS                { -8021,   -1,   "General error occurred. "                                                     "Unable to read WDS client settings"           }
#define ERR_SUMMARY_IAP_DECODER                { -8022,   -1,   "General error occurred. "                                                     "Unable to read IAP capabilities"              }
#define ERR_SUMMARY_VLAN                       { -8023,   -1,   "General error occurred. "                                                     "Unable to read VLAN settings"                 }
#define ERR_SUMMARY_SSID_LIMIT                 { -8024,   -1,   "General error occurred. "                                                     "Unable to read SSID limit settings"           }
#define ERR_SUMMARY_IDS_DATABASE               { -8025,   -1,   "General error occurred. "                                                     "Unable to read rogue database"                }
#define ERR_SUMMARY_XRP_TARGETS                { -8026,   -1,   "General error occurred. "                                                     "Unable to read fast roaming targets"          }
#define ERR_SUMMARY_MANAGEMENT                 { -8027,   -1,   "General error occurred. "                                                     "Unable to read management settings"           }
#define ERR_SUMMARY_ENV_CTRL                   { -8028,   -1,   "General error occurred. "                                                     "Unable to read environmental control settings"}
#define ERR_SUMMARY_SSID_WPR                   { -8029,   -1,   "General error occurred. "                                                     "Unable to read SSID web redirect settings"    }
#define ERR_SUMMARY_RADIUS_ACCT                { -8030,   -1,   "General error occurred. "                                                     "Unable to read RADIUS accounting settings"    }
#define ERR_SUMMARY_STANDBY                    { -8031,   -1,   "General error occurred. "                                                     "Unable to read standby settings"              }
#define ERR_SUMMARY_LOCAL_RADIUS_PW            { -8032,   -1,   "General error occurred. "                                                     "Unable to read local RADIUS password settings"}
#define ERR_SUMMARY_UBOOT_ENV_VARS             { -8033,   -1,   "General error occurred. "                                                     "Unable to read boot environment variables"    }
#define ERR_SUMMARY_CDP                        { -8034,   -1,   "General error occurred. "                                                     "Unable to read CDP configuration"             }
#define ERR_SUMMARY_CDP_LIST                   { -8035,   -1,   "General error occurred. "                                                     "Unable to read CDP neighbor list"             }
#define ERR_SUMMARY_RADIO_GLB_N                { -8036,   -1,   "General error occurred. "                                                     "Unable to read 802.11N radio configuration"   }
#define ERR_SUMMARY_GROUP                      { -8037,   -1,   "General error occurred. "                                                     "Unable to read user group settings"           }
#define ERR_SUMMARY_FILTER_LIST                { -8038,   -1,   "Invalid filter list name. "                                                   "Unable to read filter list configuration"     }
#define ERR_SUMMARY_FILTER_LIST_RAW            { -8039,   -1,   "Invalid filter list name. "                                                   "Unable to read filter list raw data"          }
#define ERR_SUMMARY_FILTER_LIST_HEADERS        { -8040,   -1,   "General error occurred. "                                                     "Unable to read filter list headers"           }
#define ERR_SUMMARY_FILTER_LIST_ALL            { -8041,   -1,   "General error occurred. "                                                     "Unable to read all filter list data"          }
#define ERR_SUMMARY_NETFLOW                    { -8042,   -1,   "General error occurred. "                                                     "Unable to read NetFlow configuration"         }
#define ERR_SUMMARY_PACKED_RADIO               { -8043,   -1,   "General error occurred. "                                                     "Unable to read IAP settings"                  }
#define ERR_SUMMARY_ADMIN_RADIUS               { -8044,   -1,   "General error occurred. "                                                     "Unable to read admin RADIUS information"      }
#define ERR_SUMMARY_SECTIONS                   { -8045,   -1,   "General error occurred. "                                                     "Unable to read config section names"          }
#define ERR_SUMMARY_LOCAL_RADIUS_PW_ENC        { -8046,   -1,   "General error occurred. "                                                     "Unable to read local RADIUS enc passwords"    }
#define ERR_SUMMARY_WEP_KEYS_ENC               { -8047,   -1,   "General error occurred. "                                                     "Unable to read encrypted WEP keys"            }
#define ERR_SUMMARY_SSID_PARAMS_ENC            { -8048,   -1,   "General error occurred. "                                                     "Unable to read encrypted SSID parameters"     }
#define ERR_SUMMARY_VLAN_PARAMS_ENC            { -8049,   -1,   "General error occurred. "                                                     "Unable to read encrypted VLAN parameters"     }
#define ERR_SUMMARY_CLUSTERS                   { -8050,   -1,   "General error occurred. "                                                     "Unable to read cluster settings"              }
#define ERR_SUMMARY_CLUSTER_ARRAYS             { -8051,   -1,   "General error occurred. "                                                     "Unable to read cluster member settings"       }
#define ERR_SUMMARY_EXT_MGMT                   { -8052,   -1,   "General error occurred. "                                                     "Unable to read extended management settings"  }
#define ERR_SUMMARY_RADIUS_ALL                 { -8053,   -1,   "General error occurred. "                                                     "Unable to read all RADIUS settings"           }
#define ERR_SUMMARY_WIFI_TAG                   { -8054,   -1,   "General error occurred. "                                                     "Unable to read wifi tag settings"             }
#define ERR_SUMMARY_PRIVILEGE                  { -8055,   -1,   "General error occurred. "                                                     "Unable to read privilege settings"            }
#define ERR_SUMMARY_PRIVILEGE_LEVELS           { -8056,   -1,   "General error occurred. "                                                     "Unable to read privilege level names"         }
#define ERR_SUMMARY_PRIVILEGE_SECTIONS         { -8057,   -1,   "General error occurred. "                                                     "Unable to read privilege section settings"    }
#define ERR_SUMMARY_AUTOCHANNEL                { -8058,   -1,   "General error occurred. "                                                     "Unable to read auto channel status"           }
#define ERR_SUMMARY_LOAD_BALANCE               { -8059,   -1,   "General error occurred. "                                                     "Unable to read load balancing status"         }
#define ERR_SUMMARY_CLUSTER_ARRAYS_ENC         { -8060,   -1,   "General error occurred. "                                                     "Unable to read encrypted cluster parameters"  }
#define ERR_SUMMARY_FILTER_LIST_ALL_RAW        { -8061,   -1,   "General error occurred. "                                                     "Unable to read all filter list raw data"      }
#define ERR_SUMMARY_IDS                        { -8062,   -1,   "General error occurred. "                                                     "Unable to read IDS settings"                  }
#define ERR_SUMMARY_STA_ASSURANCE              { -8063,   -1,   "General error occurred. "                                                     "Unable to read station assurance settings"    }
#define ERR_SUMMARY_TUNNEL                     { -8064,   -1,   "General error occurred. "                                                     "Unable to read tunnel settings"               }
#define ERR_SUMMARY_ROAMING_ASSIST             { -8065,   -1,   "General error occurred. "                                                     "Unable to read roaming assist settings"       }
#define ERR_SUMMARY_VAP                        { -8066,   -1,   "General error occurred. "                                                     "Unable to read vap settings"                  }
#define ERR_SUMMARY_FILTER_LIST_NOSTATS        { -8067,   -1,   "Invalid filter list name. "                                                   "Unable to read filter list configuration"     }
#define ERR_SUMMARY_ETHERNET_BONDS             { -8068,   -1,   "General error occurred. "                                                     "Unable to read ethernet bond "    "settings"  }
#define ERR_SUMMARY_LOCATION                   { -8069,   -1,   "General error occurred. "                                                     "Unable to read location reporting  settings"  }
#define ERR_SUMMARY_GROUP_WPR_WHITELIST        { -8070,   -1,   "General error occurred. "                                                     "Unable to read whitelist entries"             }
#define ERR_SUMMARY_SSID_WPR_WHITELIST         { -8071,   -1,   "General error occurred. "                                                     "Unable to read whitelist entries"             }
#define ERR_SUMMARY_MDM                        { -8072,   -1,   "General error occurred. "                                                     "Unable to read MDM settings"                  }
#define ERR_SUMMARY_OAUTH                      { -8073,   -1,   "General error occurred. "                                                     "Unable to read OAUTH "           "settings"   }
#define ERR_SUMMARY_SSID_HONEYPOT_WLIST        { -8074,   -1,   "General error occurred. "                                                     "Unable to read honeypot whitelist"            }
#define ERR_SUMMARY_SSID_HONEYPOT_BCAST        { -8075,   -1,   "General error occurred. "                                                     "Unable to read honeypot broadcast list"       }
#define ERR_SUMMARY_RADIO_GLB_AC               { -8076,   -1,   "General error occurred. "                                                     "Unable to read 802.11ac radio configuration"  }
#define ERR_SUMMARY_PROXY_FWD                  { -8077,   -1,   "General error occurred. "                                                     "Unable to read proxy forwarding " "settings"  }
#define ERR_SUMMARY_ACTIVE_DIRECTORY           { -8078,   -1,   "General error occurred. "                                                     "Unable to read active directory " "settings"  }
#define ERR_SUMMARY_LLDP                       { -8079,   -1,   "General error occurred. "                                                     "Unable to read LLDP configuration"            }
#define ERR_SUMMARY_LLDP_LIST                  { -8070,   -1,   "General error occurred. "                                                     "Unable to read LLDP neighbor list"            }
#define ERR_SUMMARY_PROXY_MGMT                 { -8081,   -1,   "General error occurred. "                                                     "Unable to read Proxy Management configuration"}
#define ERR_SUMMARY_APP_LIST                   { -8082,   -1,   "Invalid application list name. "                                              "Unable to read application list configuration"}
#define ERR_SUMMARY_APP_LIST_HEADERS           { -8083,   -1,   "General error occurred. "                                                     "Unable to read application list headers"      }
#define ERR_SUMMARY_APP_LIST_ALL               { -8084,   -1,   "General error occurred. "                                                     "Unable to read all application list data"     }
#define ERR_SUMMARY_VLAN_POOL                  { -8085,   -1,   "General error occurred. "                                                     "Unable to read VLAN pool settings"            }
#define ERR_SUMMARY_POSITION                   { -8086,   -1,   "General error occurred. "                                                     "Unable to read position settings"             }
#define ERR_SUMMARY_GROUP_PARAMS_ENC           { -8087,   -1,   "General error occurred. "                                                     "Unable to read encrypted user group settings" }
#define ERR_SUMMARY_FIXED_ADDRS                { -8088,   -1,   "General error occurred. "                                                     "Unable to read multicast fixed addr settings" }
#define ERR_SUMMARY_BLUETOOTH                  { -8089,   -1,   "General error occurred. "                                                     "Unable to read bluetooth settings"            }
#define ERR_SUMMARY_DEVICE_ID_MAC_TABLE        { -8090,   -1,   "General error occurred. "                                                     "Unable to read Device ID MAC table"           }

// MISC         (-90xx)
#define ERR_OOPME_WRITE_FAIL                   { -9000,   -1,   "General error occurred. "                                                     "Unable to update setting"                     }
#define ERR_UNKNOWN_ADD_CFG_OBJ                { -9010,   -1,   "Add error. "                                                                  "Undefined configuration object"               }
#define ERR_UNKNOWN_DEL_CFG_OBJ                { -9020,   -1,   "Delete error. "                                                               "Undefined configuration object"               }
#define ERR_UNKNOWN_EXEC_CFG_OBJ               { -9030,   -1,   "Execute error. "                                                              "Undefined configuration object"               }
#define ERR_UNKNOWN_ENABLE_CFG_OBJ             { -9035,   -1,   "Enable error. "                                                               "Undefined configuration object"               }
#define ERR_UNKNOWN_READ_CFG_OBJ               { -9040,   -1,   "Read error. "                                                                 "Undefined configuration object"               }
#define ERR_UNKNOWN_READ_CFG_TYPE              { -9041,   -1,   "Read error. "                                                                 "Invalid configuration object type"            }
#define ERR_UNKNOWN_WRITE_CFG_OBJ              { -9050,   -1,   "Write error. "                                                                "Undefined configuration object"               }
#define ERR_UNKNOWN_WRITE_CFG_TYPE             { -9051,   -1,   "Write error. "                                                                "Invalid configuration object type"            }
#define ERR_UNKNOWN_WRITE_ERR_CODE             { -9052,   -1,   "General error occurred. "                                                     "Unknown error type"                           }
#define ERR_INSUFFICIENT_MEMORY                { -9053,   -1,   "Insufficient memory. "                                                        "Unable to enable requested feature"           }
#define ERR_NONE                               {     0,    0,   NULL                                                                                                                          }

#ifdef CFG_USE_LEGACY_ERR_CODES
#define MAX_ERR_CODE      -1
#define MIN_ERR_CODE     -12
#else
#define MAX_ERR_CODE   -1000
#define MIN_ERR_CODE   -9999
#endif


#endif //_XIRERR_H
