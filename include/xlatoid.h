//******************************************************************************
/** @file xlatoid.h
 *
 * translates old oid defines to new oid defines
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 * @author  Eric K. Henderson
 * @date    11/7/2005
 *
 * This file contains a mapping from legacy constants to the new names found in
 * xircfg.h, xiriap.h, and xiroid.h.
 *
 * PLEASE DO NOT ADD ANYTHING TO THIS FILE UNLESS IT IS NEEDED FOR LEGACY
 * SUPPORT.
 *
 * New OIDs should go in xiroid.h using the new naming conventions.
 * New WMI/CLI interface structures and defines belong in xircfg.h.
 * New iap/radio defines belong in xiriap.h
 **/
//------------------------------------------------------------------------------
#ifndef _XLATOID_H
#define _XLATOID_H

// Deprecated
#define WMI                         MOD_WMI
#define CLI                         MOD_CLI
#define DHCPCD                      MOD_DHCPCD
#define INTERNAL                    MOD_INTERNAL  // cfg
#define BOOT                        MOD_BOOT      // cfg
#define SNMPD                       MOD_SNMPD
#define HEALTH_MON                  MOD_HEALTH_MON

//---------------------------------------------------------
// Config Objects (cmd_read & cmd_write / cmd_add & cmd_delete)
//---------------------------------------------------------
#define ACL_LIST                    OID_ACL_LIST

#define ADMIN1_ID                   OID_ADMIN_USER(0)
#define ADMIN1_PASSWD               OID_ADMIN_PASS(0)
#define ADMIN1_PRIV                 OID_ADMIN_PRIV(0)
//---------------------------------------------------------
// Cfg
//---------------------------------------------------------
#define ADMIN2_ID                   OID_ADMIN_USER(1)
#define ADMIN2_PASSWD               OID_ADMIN_PASS(1)
#define ADMIN2_PRIV                 OID_ADMIN_PRIV(1)
#define ADMIN3_ID                   OID_ADMIN_USER(2)
#define ADMIN3_PASSWD               OID_ADMIN_PASS(2)
#define ADMIN3_PRIV                 OID_ADMIN_PRIV(2)
#define ADMIN4_ID                   OID_ADMIN_USER(3)
#define ADMIN4_PASSWD               OID_ADMIN_PASS(3)
#define ADMIN4_PRIV                 OID_ADMIN_PRIV(3)
#define ADMIN5_ID                   OID_ADMIN_USER(4)
#define ADMIN5_PASSWD               OID_ADMIN_PASS(4)
#define ADMIN5_PRIV                 OID_ADMIN_PRIV(4)
#define ADMIN6_ID                   OID_ADMIN_USER(5)
#define ADMIN6_PASSWD               OID_ADMIN_PASS(5)
#define ADMIN6_PRIV                 OID_ADMIN_PRIV(5)
#define ADMIN7_ID                   OID_ADMIN_USER(6)
#define ADMIN7_PASSWD               OID_ADMIN_PASS(6)
#define ADMIN7_PRIV                 OID_ADMIN_PRIV(6)
#define ADMIN8_ID                   OID_ADMIN_USER(7)
#define ADMIN8_PASSWD               OID_ADMIN_PASS(7)
#define ADMIN8_PRIV                 OID_ADMIN_PRIV(7)
#define ADMIN9_ID                   OID_ADMIN_USER(8)
#define ADMIN9_PASSWD               OID_ADMIN_PASS(8)
#define ADMIN9_PRIV                 OID_ADMIN_PRIV(8)
#define ADMIN10_ID                  OID_ADMIN_USER(9)
#define ADMIN10_PASSWD              OID_ADMIN_PASS(9)
#define ADMIN10_PRIV                OID_ADMIN_PRIV(9)
#define ADMIN11_ID                  OID_ADMIN_USER(10)
#define ADMIN11_PASSWD              OID_ADMIN_PASS(10)
#define ADMIN11_PRIV                OID_ADMIN_PRIV(10)
#define ADMIN12_ID                  OID_ADMIN_USER(11)
#define ADMIN12_PASSWD              OID_ADMIN_PASS(11)
#define ADMIN12_PRIV                OID_ADMIN_PRIV(11)
#define ADMIN13_ID                  OID_ADMIN_USER(12)
#define ADMIN13_PASSWD              OID_ADMIN_PASS(12)
#define ADMIN13_PRIV                OID_ADMIN_PRIV(12)
#define ADMIN14_ID                  OID_ADMIN_USER(13)
#define ADMIN14_PASSWD              OID_ADMIN_PASS(13)
#define ADMIN14_PRIV                OID_ADMIN_PRIV(13)
#define ADMIN15_ID                  OID_ADMIN_USER(14)
#define ADMIN15_PASSWD              OID_ADMIN_PASS(14)
#define ADMIN15_PRIV                OID_ADMIN_PRIV(14)
#define ADMIN16_ID                  OID_ADMIN_USER(15)
#define ADMIN16_PASSWD              OID_ADMIN_PASS(15)
#define ADMIN16_PRIV                OID_ADMIN_PRIV(15)
#define ADMIN17_ID                  OID_ADMIN_USER(16)
#define ADMIN17_PASSWD              OID_ADMIN_PASS(16)
#define ADMIN17_PRIV                OID_ADMIN_PRIV(16)
#define ADMIN18_ID                  OID_ADMIN_USER(17)
#define ADMIN18_PASSWD              OID_ADMIN_PASS(17)
#define ADMIN18_PRIV                OID_ADMIN_PRIV(17)
#define ADMIN19_ID                  OID_ADMIN_USER(18)
#define ADMIN19_PASSWD              OID_ADMIN_PASS(18)
#define ADMIN19_PRIV                OID_ADMIN_PRIV(18)
#define ADMIN20_ID                  OID_ADMIN_USER(19)
#define ADMIN20_PASSWD              OID_ADMIN_PASS(19)
#define ADMIN20_PRIV                OID_ADMIN_PRIV(19)
#define ADMIN21_ID                  OID_ADMIN_USER(20)
#define ADMIN21_PASSWD              OID_ADMIN_PASS(20)
#define ADMIN21_PRIV                OID_ADMIN_PRIV(20)
#define ADMIN22_ID                  OID_ADMIN_USER(21)
#define ADMIN22_PASSWD              OID_ADMIN_PASS(21)
#define ADMIN22_PRIV                OID_ADMIN_PRIV(21)
#define ADMIN23_ID                  OID_ADMIN_USER(22)
#define ADMIN23_PASSWD              OID_ADMIN_PASS(22)
#define ADMIN23_PRIV                OID_ADMIN_PRIV(22)
#define ADMIN24_ID                  OID_ADMIN_USER(23)
#define ADMIN24_PASSWD              OID_ADMIN_PASS(23)
#define ADMIN24_PRIV                OID_ADMIN_PRIV(23)
#define ADMIN25_ID                  OID_ADMIN_USER(24)
#define ADMIN25_PASSWD              OID_ADMIN_PASS(24)
#define ADMIN25_PRIV                OID_ADMIN_PRIV(24)
#define ADMIN26_ID                  OID_ADMIN_USER(25)
#define ADMIN26_PASSWD              OID_ADMIN_PASS(25)
#define ADMIN26_PRIV                OID_ADMIN_PRIV(25)
#define ADMIN27_ID                  OID_ADMIN_USER(26)
#define ADMIN27_PASSWD              OID_ADMIN_PASS(26)
#define ADMIN27_PRIV                OID_ADMIN_PRIV(26)
#define ADMIN28_ID                  OID_ADMIN_USER(27)
#define ADMIN28_PASSWD              OID_ADMIN_PASS(27)
#define ADMIN28_PRIV                OID_ADMIN_PRIV(27)
#define ADMIN29_ID                  OID_ADMIN_USER(28)
#define ADMIN29_PASSWD              OID_ADMIN_PASS(28)
#define ADMIN29_PRIV                OID_ADMIN_PRIV(28)
#define ADMIN30_ID                  OID_ADMIN_USER(29)
#define ADMIN30_PASSWD              OID_ADMIN_PASS(29)
#define ADMIN30_PRIV                OID_ADMIN_PRIV(29)
#define ADMIN31_ID                  OID_ADMIN_USER(30)
#define ADMIN31_PASSWD              OID_ADMIN_PASS(30)
#define ADMIN31_PRIV                OID_ADMIN_PRIV(30)
#define ADMIN32_ID                  OID_ADMIN_USER(31)
#define ADMIN32_PASSWD              OID_ADMIN_PASS(31)
#define ADMIN32_PRIV                OID_ADMIN_PRIV(31)

#define DESC_LOCATION               OID_DESC_LOCATION

#define CONTACT_EMAIL               OID_CONTACT_EMAIL
#define CONTACT_NAME                OID_CONTACT_NAME
#define CONTACT_PHONE               OID_CONTACT_PHONE

#define DHCP_DEFAULT_LEASE          OID_DHCP_LEASE_DEFAULT
#define DHCP_ENABLED                OID_DHCP_ENABLED
#define DHCP_MAX_LEASE              OID_DHCP_LEASE_MAX
#define DHCP_RANGE_END              OID_DHCP_RANGE_END
#define DHCP_RANGE_START            OID_DHCP_RANGE_START

#define DNS_DOMAIN                  OID_DNS_DOMAIN
#define DNS_HOSTNAME                OID_DNS_HOSTNAME
#define DNS_SERVER1                 OID_DNS_SERVER1
#define DNS_SERVER2                 OID_DNS_SERVER2
#define DNS_SERVER3                 OID_DNS_SERVER3

// Deprecated
#define FAST_ALLOW_MGMT             OID_ETH_ALLOW_MGMT(0)
#define FAST_AUTONEG                OID_ETH_AUTONEG(0)
#define FAST_DHCP_BIND              OID_ETH_DHCP_BIND(0)
#define FAST_DUPLEX                 OID_ETH_DUPLEX(0)
#define FAST_ENABLED                OID_ETH_ENABLED(0)
#define FAST_GATEWAY                OID_ETH_IPV4_GATEWAY(0)
#define FAST_IP_ADDR                OID_ETH_IPV4_ADDR(0)
#define FAST_MASK                   OID_ETH_IPV4_MASK(0)
#define FAST_MTU                    OID_ETH_MTU(0)
#define FAST_SPEED                  OID_ETH_SPEED(0)

#define GIG1_ALLOW_MGMT             OID_ETH_ALLOW_MGMT(1)
#define GIG1_AUTONEG                OID_ETH_AUTONEG(1)
#define GIG1_DHCP_BIND              OID_ETH_DHCP_BIND(1)
#define GIG1_DUPLEX                 OID_ETH_DUPLEX(1)
#define GIG1_ENABLED                OID_ETH_ENABLED(1)
#define GIG1_GATEWAY                OID_ETH_IPV4_GATEWAY(1)
#define GIG1_IP_ADDR                OID_ETH_IPV4_ADDR(1)
#define GIG1_MASK                   OID_ETH_IPV4_MASK(1)
#define GIG1_MTU                    OID_ETH_MTU(1)
#define GIG1_SPEED                  OID_ETH_SPEED(1)

#define GIG2_ALLOW_MGMT             OID_ETH_ALLOW_MGMT(2)
#define GIG2_ENABLED                OID_ETH_ENABLED(2)
#define GIG2_GATEWAY                OID_ETH_IPV4_GATEWAY(2)
#define GIG2_MASK                   OID_ETH_IPV4_MASK(2)
#define GIG2_IP_ADDR                OID_ETH_IPV4_ADDR(2)

#define LINE_BAUD_RATE              OID_CON_BAUD_RATE
#define LINE_PARITY                 OID_CON_PARITY
#define LINE_STOP_BITS              OID_CON_STOP_BITS
#define LINE_TIMEOUT                OID_CON_TIMEOUT
#define LINE_WORD_SIZE              OID_CON_WORD_SIZE

#define NTP_ENABLED                 OID_NTP_ENABLED
#define NTP_SERVER1                 OID_NTP_SERVER1
#define NTP_SERVER2                 OID_NTP_SERVER2

#define RADIO1_START                OID_RADIO_START(0)
#define RADIO1_ANTENNA              OID_RADIO_ANTENNA(0)
#define RADIO1_BAND                 OID_RADIO_BAND(0)
#define RADIO1_CELL_SIZE            OID_RADIO_CELL_SIZE(0)
#define RADIO1_CHANNEL              OID_RADIO_CHANNEL(0)
#define RADIO1_DESC                 OID_RADIO_DESC(0)
#define RADIO1_ENABLED              OID_RADIO_ENABLED(0)
#define RADIO1_WDS_LINK             OID_RADIO_WDS_LINK(0)
#define RADIO1_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(0)
#define RADIO1_TX_POWER             OID_RADIO_TX_POWER(0)
#define RADIO1_END                  OID_RADIO_END(0)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define RADIO2_START                OID_RADIO_START(1)
#define RADIO2_ANTENNA              OID_RADIO_ANTENNA(1)
#define RADIO2_BAND                 OID_RADIO_BAND(1)
#define RADIO2_CELL_SIZE            OID_RADIO_CELL_SIZE(1)
#define RADIO2_CHANNEL              OID_RADIO_CHANNEL(1)
#define RADIO2_DESC                 OID_RADIO_DESC(1)
#define RADIO2_ENABLED              OID_RADIO_ENABLED(1)
#define RADIO2_WDS_LINK             OID_RADIO_WDS_LINK(1)
#define RADIO2_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(1)
#define RADIO2_TX_POWER             OID_RADIO_TX_POWER(1)
#define RADIO2_END                  OID_RADIO_END(1)

#define RADIO3_START                OID_RADIO_START(2)
#define RADIO3_ANTENNA              OID_RADIO_ANTENNA(2)
#define RADIO3_BAND                 OID_RADIO_BAND(2)
#define RADIO3_CELL_SIZE            OID_RADIO_CELL_SIZE(2)
#define RADIO3_CHANNEL              OID_RADIO_CHANNEL(2)
#define RADIO3_DESC                 OID_RADIO_DESC(2)
#define RADIO3_ENABLED              OID_RADIO_ENABLED(2)
#define RADIO3_WDS_LINK             OID_RADIO_WDS_LINK(2)
#define RADIO3_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(2)
#define RADIO3_TX_POWER             OID_RADIO_TX_POWER(2)
#define RADIO3_END                  OID_RADIO_END(2)

#define RADIO4_START                OID_RADIO_START(3)
#define RADIO4_ANTENNA              OID_RADIO_ANTENNA(3)
#define RADIO4_BAND                 OID_RADIO_BAND(3)
#define RADIO4_CELL_SIZE            OID_RADIO_CELL_SIZE(3)
#define RADIO4_CHANNEL              OID_RADIO_CHANNEL(3)
#define RADIO4_DESC                 OID_RADIO_DESC(3)
#define RADIO4_ENABLED              OID_RADIO_ENABLED(3)
#define RADIO4_WDS_LINK             OID_RADIO_WDS_LINK(3)
#define RADIO4_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(3)
#define RADIO4_TX_POWER             OID_RADIO_TX_POWER(3)
#define RADIO4_END                  OID_RADIO_END(3)

#define RADIO5_START                OID_RADIO_START(4)
#define RADIO5_ANTENNA              OID_RADIO_ANTENNA(4)
#define RADIO5_BAND                 OID_RADIO_BAND(4)
#define RADIO5_CELL_SIZE            OID_RADIO_CELL_SIZE(4)
#define RADIO5_CHANNEL              OID_RADIO_CHANNEL(4)
#define RADIO5_DESC                 OID_RADIO_DESC(4)
#define RADIO5_ENABLED              OID_RADIO_ENABLED(4)
#define RADIO5_WDS_LINK             OID_RADIO_WDS_LINK(4)
#define RADIO5_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(4)
#define RADIO5_TX_POWER             OID_RADIO_TX_POWER(4)
#define RADIO5_END                  OID_RADIO_END(4)

#define RADIO6_START                OID_RADIO_START(5)
#define RADIO6_ANTENNA              OID_RADIO_ANTENNA(5)
#define RADIO6_BAND                 OID_RADIO_BAND(5)
#define RADIO6_CELL_SIZE            OID_RADIO_CELL_SIZE(5)
#define RADIO6_CHANNEL              OID_RADIO_CHANNEL(5)
#define RADIO6_DESC                 OID_RADIO_DESC(5)
#define RADIO6_ENABLED              OID_RADIO_ENABLED(5)
#define RADIO6_WDS_LINK             OID_RADIO_WDS_LINK(5)
#define RADIO6_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(5)
#define RADIO6_TX_POWER             OID_RADIO_TX_POWER(5)
#define RADIO6_END                  OID_RADIO_END(5)

#define RADIO7_START                OID_RADIO_START(6)
#define RADIO7_ANTENNA              OID_RADIO_ANTENNA(6)
#define RADIO7_BAND                 OID_RADIO_BAND(6)
#define RADIO7_CELL_SIZE            OID_RADIO_CELL_SIZE(6)
#define RADIO7_CHANNEL              OID_RADIO_CHANNEL(6)
#define RADIO7_DESC                 OID_RADIO_DESC(6)
#define RADIO7_ENABLED              OID_RADIO_ENABLED(6)
#define RADIO7_WDS_LINK             OID_RADIO_WDS_LINK(6)
#define RADIO7_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(6)
#define RADIO7_TX_POWER             OID_RADIO_TX_POWER(6)
#define RADIO7_END                  OID_RADIO_END(6)

#define RADIO8_START                OID_RADIO_START(7)
#define RADIO8_ANTENNA              OID_RADIO_ANTENNA(7)
#define RADIO8_BAND                 OID_RADIO_BAND(7)
#define RADIO8_CELL_SIZE            OID_RADIO_CELL_SIZE(7)
#define RADIO8_CHANNEL              OID_RADIO_CHANNEL(7)
#define RADIO8_DESC                 OID_RADIO_DESC(7)
#define RADIO8_ENABLED              OID_RADIO_ENABLED(7)
#define RADIO8_WDS_LINK             OID_RADIO_WDS_LINK(7)
#define RADIO8_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(7)
#define RADIO8_TX_POWER             OID_RADIO_TX_POWER(7)
#define RADIO8_END                  OID_RADIO_END(7)

#define RADIO9_START                OID_RADIO_START(8)
#define RADIO9_ANTENNA              OID_RADIO_ANTENNA(8)
#define RADIO9_BAND                 OID_RADIO_BAND(8)
#define RADIO9_CELL_SIZE            OID_RADIO_CELL_SIZE(8)
#define RADIO9_CHANNEL              OID_RADIO_CHANNEL(8)
#define RADIO9_DESC                 OID_RADIO_DESC(8)
#define RADIO9_ENABLED              OID_RADIO_ENABLED(8)
#define RADIO9_WDS_LINK             OID_RADIO_WDS_LINK(8)
#define RADIO9_RX_THRESHOLD         OID_RADIO_RX_THRESHOLD(8)
#define RADIO9_TX_POWER             OID_RADIO_TX_POWER(8)
#define RADIO9_END                  OID_RADIO_END(8)

#define RADIO10_START               OID_RADIO_START(9)
#define RADIO10_ANTENNA             OID_RADIO_ANTENNA(9)
#define RADIO10_BAND                OID_RADIO_BAND(9)
#define RADIO10_CELL_SIZE           OID_RADIO_CELL_SIZE(9)
#define RADIO10_CHANNEL             OID_RADIO_CHANNEL(9)
#define RADIO10_DESC                OID_RADIO_DESC(9)
#define RADIO10_ENABLED             OID_RADIO_ENABLED(9)
#define RADIO10_WDS_LINK            OID_RADIO_WDS_LINK(9)
#define RADIO10_RX_THRESHOLD        OID_RADIO_RX_THRESHOLD(9)
#define RADIO10_TX_POWER            OID_RADIO_TX_POWER(9)
#define RADIO10_END                 OID_RADIO_END(9)

#define RADIO11_START               OID_RADIO_START(10)
#define RADIO11_ANTENNA             OID_RADIO_ANTENNA(10)
#define RADIO11_BAND                OID_RADIO_BAND(10)
#define RADIO11_CELL_SIZE           OID_RADIO_CELL_SIZE(10)
#define RADIO11_CHANNEL             OID_RADIO_CHANNEL(10)
#define RADIO11_DESC                OID_RADIO_DESC(10)
#define RADIO11_ENABLED             OID_RADIO_ENABLED(10)
#define RADIO11_WDS_LINK            OID_RADIO_WDS_LINK(10)
#define RADIO11_RX_THRESHOLD        OID_RADIO_RX_THRESHOLD(10)
#define RADIO11_TX_POWER            OID_RADIO_TX_POWER(10)
#define RADIO11_END                 OID_RADIO_END(10)

#define RADIO12_START               OID_RADIO_START(11)
#define RADIO12_ANTENNA             OID_RADIO_ANTENNA(11)
#define RADIO12_BAND                OID_RADIO_BAND(11)
#define RADIO12_CELL_SIZE           OID_RADIO_CELL_SIZE(11)
#define RADIO12_CHANNEL             OID_RADIO_CHANNEL(11)
#define RADIO12_DESC                OID_RADIO_DESC(11)
#define RADIO12_ENABLED             OID_RADIO_ENABLED(11)
#define RADIO12_WDS_LINK            OID_RADIO_WDS_LINK(11)
#define RADIO12_RX_THRESHOLD        OID_RADIO_RX_THRESHOLD(11)
#define RADIO12_TX_POWER            OID_RADIO_TX_POWER(11)
#define RADIO12_END                 OID_RADIO_END(11)

#define RADIO13_START               OID_RADIO_START(12)
#define RADIO13_ANTENNA             OID_RADIO_ANTENNA(12)
#define RADIO13_BAND                OID_RADIO_BAND(12)
#define RADIO13_CELL_SIZE           OID_RADIO_CELL_SIZE(12)
#define RADIO13_CHANNEL             OID_RADIO_CHANNEL(12)
#define RADIO13_DESC                OID_RADIO_DESC(12)
#define RADIO13_ENABLED             OID_RADIO_ENABLED(12)
#define RADIO13_WDS_LINK            OID_RADIO_WDS_LINK(12)
#define RADIO13_RX_THRESHOLD        OID_RADIO_RX_THRESHOLD(12)
#define RADIO13_TX_POWER            OID_RADIO_TX_POWER(12)
#define RADIO13_END                 OID_RADIO_END(12)

#define RADIO14_START               OID_RADIO_START(13)
#define RADIO14_ANTENNA             OID_RADIO_ANTENNA(13)
#define RADIO14_BAND                OID_RADIO_BAND(13)
#define RADIO14_CELL_SIZE           OID_RADIO_CELL_SIZE(13)
#define RADIO14_CHANNEL             OID_RADIO_CHANNEL(13)
#define RADIO14_DESC                OID_RADIO_DESC(13)
#define RADIO14_ENABLED             OID_RADIO_ENABLED(13)
#define RADIO14_WDS_LINK            OID_RADIO_WDS_LINK(13)
#define RADIO14_RX_THRESHOLD        OID_RADIO_RX_THRESHOLD(13)
#define RADIO14_TX_POWER            OID_RADIO_TX_POWER(13)
#define RADIO14_END                 OID_RADIO_END(13)

#define RADIO15_START               OID_RADIO_START(14)
#define RADIO15_ANTENNA             OID_RADIO_ANTENNA(14)
#define RADIO15_BAND                OID_RADIO_BAND(14)
#define RADIO15_CELL_SIZE           OID_RADIO_CELL_SIZE(14)
#define RADIO15_CHANNEL             OID_RADIO_CHANNEL(14)
#define RADIO15_DESC                OID_RADIO_DESC(14)
#define RADIO15_ENABLED             OID_RADIO_ENABLED(14)
#define RADIO15_WDS_LINK            OID_RADIO_WDS_LINK(14)
#define RADIO15_RX_THRESHOLD        OID_RADIO_RX_THRESHOLD(14)
#define RADIO15_TX_POWER            OID_RADIO_TX_POWER(14)
#define RADIO15_END                 OID_RADIO_END(14)

#define RADIO16_START               OID_RADIO_START(15)
#define RADIO16_ANTENNA             OID_RADIO_ANTENNA(15)
#define RADIO16_BAND                OID_RADIO_BAND(15)
#define RADIO16_CELL_SIZE           OID_RADIO_CELL_SIZE(15)
#define RADIO16_CHANNEL             OID_RADIO_CHANNEL(15)
#define RADIO16_DESC                OID_RADIO_DESC(15)
#define RADIO16_ENABLED             OID_RADIO_ENABLED(15)
#define RADIO16_WDS_LINK            OID_RADIO_WDS_LINK(15)
#define RADIO16_RX_THRESHOLD        OID_RADIO_RX_THRESHOLD(15)
#define RADIO16_TX_POWER            OID_RADIO_TX_POWER(15)
#define RADIO16_END                 OID_RADIO_END(15)

#define RADIUS_ENABLED              OID_RADIUS_ENABLED
#define RADIUS_PRI_PORT             OID_RADIUS_PRI_PORT
#define RADIUS_PRI_SECRET           OID_RADIUS_PRI_SECRET
#define RADIUS_PRI_SERVER           OID_RADIUS_PRI_SERVER
#define RADIUS_SEC_PORT             OID_RADIUS_SEC_PORT
#define RADIUS_SEC_SECRET           OID_RADIUS_SEC_SECRET
#define RADIUS_SEC_SERVER           OID_RADIUS_SEC_SERVER
#define RADIUS_TIMEOUT              OID_RADIUS_TIMEOUT

#define SNMP_COMM_STRING            OID_SNMP_COMMUNITY_RW
#define SNMP_ENABLED                OID_SNMP_ENABLED
#define SNMP_TRAP_AUTH_ENABLED      OID_SNMP_TRAP_AUTH
#define SNMP_TRAP_HOST              OID_SNMP_TRAP_HOST
#define SNMP_TRAP_PORT_NUM          OID_SNMP_TRAP_PORT

#define SSID1_FLAGS                 OID_SSID_FLAGS(0)
#define SSID1_ENC_TYPE              OID_SSID_ENC_TYPE(0)
#define SSID1_NAME                  OID_SSID_NAME(0)
#define SSID1_QOS_LEVEL             OID_SSID_QOS_LEVEL(0)
#define SSID1_VLAN                  OID_SSID_VLAN(0)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define SSID2_FLAGS                 OID_SSID_FLAGS(1)
#define SSID2_ENC_TYPE              OID_SSID_ENC_TYPE(1)
#define SSID2_NAME                  OID_SSID_NAME(1)
#define SSID2_QOS_LEVEL             OID_SSID_QOS_LEVEL(1)
#define SSID2_VLAN                  OID_SSID_VLAN(1)

#define SSID3_FLAGS                 OID_SSID_FLAGS(2)
#define SSID3_ENC_TYPE              OID_SSID_ENC_TYPE(2)
#define SSID3_NAME                  OID_SSID_NAME(2)
#define SSID3_QOS_LEVEL             OID_SSID_QOS_LEVEL(2)
#define SSID3_VLAN                  OID_SSID_VLAN(2)

#define SSID4_FLAGS                 OID_SSID_FLAGS(3)
#define SSID4_ENC_TYPE              OID_SSID_ENC_TYPE(3)
#define SSID4_NAME                  OID_SSID_NAME(3)
#define SSID4_QOS_LEVEL             OID_SSID_QOS_LEVEL(3)
#define SSID4_VLAN                  OID_SSID_VLAN(3)

#define SSID5_FLAGS                 OID_SSID_FLAGS(4)
#define SSID5_ENC_TYPE              OID_SSID_ENC_TYPE(4)
#define SSID5_NAME                  OID_SSID_NAME(4)
#define SSID5_QOS_LEVEL             OID_SSID_QOS_LEVEL(4)
#define SSID5_VLAN                  OID_SSID_VLAN(4)

#define SSID6_FLAGS                 OID_SSID_FLAGS(5)
#define SSID6_ENC_TYPE              OID_SSID_ENC_TYPE(5)
#define SSID6_NAME                  OID_SSID_NAME(5)
#define SSID6_QOS_LEVEL             OID_SSID_QOS_LEVEL(5)
#define SSID6_VLAN                  OID_SSID_VLAN(5)

#define SSID7_FLAGS                 OID_SSID_FLAGS(6)
#define SSID7_ENC_TYPE              OID_SSID_ENC_TYPE(6)
#define SSID7_NAME                  OID_SSID_NAME(6)
#define SSID7_QOS_LEVEL             OID_SSID_QOS_LEVEL(6)
#define SSID7_VLAN                  OID_SSID_VLAN(6)

#define SSID8_FLAGS                 OID_SSID_FLAGS(7)
#define SSID8_ENC_TYPE              OID_SSID_ENC_TYPE(7)
#define SSID8_NAME                  OID_SSID_NAME(7)
#define SSID8_QOS_LEVEL             OID_SSID_QOS_LEVEL(7)
#define SSID8_VLAN                  OID_SSID_VLAN(7)

#define SSID9_FLAGS                 OID_SSID_FLAGS(8)
#define SSID9_ENC_TYPE              OID_SSID_ENC_TYPE(8)
#define SSID9_NAME                  OID_SSID_NAME(8)
#define SSID9_QOS_LEVEL             OID_SSID_QOS_LEVEL(8)
#define SSID9_VLAN                  OID_SSID_VLAN(8)

#define SSID10_FLAGS                OID_SSID_FLAGS(9)
#define SSID10_ENC_TYPE             OID_SSID_ENC_TYPE(9)
#define SSID10_NAME                 OID_SSID_NAME(9)
#define SSID10_QOS_LEVEL            OID_SSID_QOS_LEVEL(9)
#define SSID10_VLAN                 OID_SSID_VLAN(9)

#define SSID11_FLAGS                OID_SSID_FLAGS(10)
#define SSID11_ENC_TYPE             OID_SSID_ENC_TYPE(10)
#define SSID11_NAME                 OID_SSID_NAME(10)
#define SSID11_QOS_LEVEL            OID_SSID_QOS_LEVEL(10)
#define SSID11_VLAN                 OID_SSID_VLAN(10)

#define SSID12_FLAGS                OID_SSID_FLAGS(11)
#define SSID12_ENC_TYPE             OID_SSID_ENC_TYPE(11)
#define SSID12_NAME                 OID_SSID_NAME(11)
#define SSID12_QOS_LEVEL            OID_SSID_QOS_LEVEL(11)
#define SSID12_VLAN                 OID_SSID_VLAN(11)

#define SSID13_FLAGS                OID_SSID_FLAGS(12)
#define SSID13_ENC_TYPE             OID_SSID_ENC_TYPE(12)
#define SSID13_NAME                 OID_SSID_NAME(12)
#define SSID13_QOS_LEVEL            OID_SSID_QOS_LEVEL(12)
#define SSID13_VLAN                 OID_SSID_VLAN(12)

#define SSID14_FLAGS                OID_SSID_FLAGS(13)
#define SSID14_ENC_TYPE             OID_SSID_ENC_TYPE(13)
#define SSID14_NAME                 OID_SSID_NAME(13)
#define SSID14_QOS_LEVEL            OID_SSID_QOS_LEVEL(13)
#define SSID14_VLAN                 OID_SSID_VLAN(13)

#define SSID15_FLAGS                OID_SSID_FLAGS(14)
#define SSID15_ENC_TYPE             OID_SSID_ENC_TYPE(14)
#define SSID15_NAME                 OID_SSID_NAME(14)
#define SSID15_QOS_LEVEL            OID_SSID_QOS_LEVEL(14)
#define SSID15_VLAN                 OID_SSID_VLAN(14)

#define SSID16_FLAGS                OID_SSID_FLAGS(15)
#define SSID16_ENC_TYPE             OID_SSID_ENC_TYPE(15)
#define SSID16_NAME                 OID_SSID_NAME(15)
#define SSID16_QOS_LEVEL            OID_SSID_QOS_LEVEL(15)
#define SSID16_VLAN                 OID_SSID_VLAN(15)

#define STATION_ACTIVITY_TIMEOUT    OID_STA_TIMEOUT
#define STATION_REAUTH_PERIOD       OID_STA_REAUTH_PERIOD

#define SYSLOG_CONSOLE              OID_SYSLOG_CONSOLE
#define SYSLOG_ENABLED              OID_SYSLOG_ENABLED
#define SYSLOG_FILE_SIZE            OID_SYSLOG_FILE_SIZE
#define SYSLOG_LEVEL                OID_SYSLOG_LEVEL
#define SYSLOG_SERVER               OID_SYSLOG_SERVER

#define SEC_WEP_ENABLED             OID_SEC_WEP_ENABLED
#define SEC_WEP_KEY0                OID_SEC_WEP_KEY1
#define SEC_WEP_KEY1                OID_SEC_WEP_KEY2
#define SEC_WEP_KEY2                OID_SEC_WEP_KEY3
#define SEC_WEP_KEY3                OID_SEC_WEP_KEY4

#define SEC_WEP_KEY0_LEN            OID_SEC_WEP_KEY1_LEN
#define SEC_WEP_KEY1_LEN            OID_SEC_WEP_KEY2_LEN
#define SEC_WEP_KEY2_LEN            OID_SEC_WEP_KEY3_LEN
#define SEC_WEP_KEY3_LEN            OID_SEC_WEP_KEY4_LEN

#define SEC_WEP_DEFAULT_KEY_ID      OID_SEC_WEP_KEY_ID_DEF
#define SEC_WPA_ENABLED             OID_SEC_WPA_ENABLED
#define SEC_WPA_TKIP_ENABLED        OID_SEC_WPA_TKIP_ON
#define SEC_WPA_AES_ENABLED         OID_SEC_WPA_AES_ON
#define SEC_WPA_EAP_ENABLED         OID_SEC_WPA_EAP_ON
#define SEC_WPA_PSK_ENABLED         OID_SEC_WPA_PSK_ON
#define SEC_WPA_PASSPHRASE          OID_SEC_WPA_PASSPHRASE
#define SEC_WPA_GROUP_REKEY         OID_SEC_WPA_GROUP_REKEY

#define IDS_ENABLE                  OID_IDS_ENABLE        // 1 = On,  0 = Off
#define IDS_DATABASE                OID_IDS_DATABASE      // add or delete ssids to the rogue database

#define RADIO_GLB_COUNTRY_CODE      OID_IAP_GLB_COUNTRY_CODE
#define RADIO_GLB_BEACON_INTERVAL   OID_IAP_GLB_BEACON_INTERVAL
#define RADIO_GLB_BEACON_DTIM       OID_IAP_GLB_BEACON_DTIM
#define RADIO_GLB_LONG_RETRY_LIMIT  OID_IAP_GLB_RETRY_LONG
#define RADIO_GLB_SHORT_RETRY_LIMIT OID_IAP_GLB_RETRY_SHORT
#define RADIO_GLB_WORLD_BEACON      OID_IAP_GLB_WORLD_BEACON
#define RADIO_GLB_EDCF              OID_IAP_GLB_EDCF
#define RADIO_GLB_A_BASIC_RATES     OID_IAP_GLB_A_RATES_BASIC
#define RADIO_GLB_A_SUPPORTED_RATES OID_IAP_GLB_A_RATES_SUPP
#define RADIO_GLB_A_FRAG_THRESHOLD  OID_IAP_GLB_A_FRAG_THRESHOLD
#define RADIO_GLB_A_RTS_THRESHOLD   OID_IAP_GLB_A_RTS_THRESHOLD
#define RADIO_GLB_G_BASIC_RATES     OID_IAP_GLB_G_RATES_BASIC
#define RADIO_GLB_G_SUPPORTED_RATES OID_IAP_GLB_G_RATES_SUPP
#define RADIO_GLB_G_FRAG_THRESHOLD  OID_IAP_GLB_G_FRAG_THRESHOLD
#define RADIO_GLB_G_RTS_THRESHOLD   OID_IAP_GLB_G_RTS_THRESHOLD
#define RADIO_GLB_G_DOT11_SLOT      OID_IAP_GLB_G_DOT11_SLOT        // 0=long enabled (auto long/short) 1=short only
#define RADIO_GLB_G_DOT11_PREAMBLE  OID_IAP_GLB_G_DOT11_PREAMBLE    // 0=long only                      1=short enabled (auto long/short)
#define RADIO_GLB_LED_BLINK_MASK    OID_IAP_GLB_LED_BLINK_MASK

#define SSID_BROADCAST              OID_SSID_BROADCAST
#define MGMT_TELNET_ENABLED         OID_MGMT_TELNET_ENABLED
#define MGMT_SSH_ENABLED            OID_MGMT_SSH_ENABLED
#define TIME_OFFSET_HOURS           OID_TIME_OFFSET_HOURS
#define TIME_OFFSET_MINS            OID_TIME_OFFSET_MINS
#define RADIO_GLB_G_DOT11_PROTECT   OID_IAP_GLB_G_DOT11_PROTECT     // 0=off                            1=CTS enabled   (auto CTS)
#define RADIO_GLB_G_DOT11_GONLY     OID_IAP_GLB_G_DOT11_GONLY       // 0=off                            1=on
#define RADIO_GLB_AUTOCHANNEL_MODE  OID_IAP_GLB_AUTOCHANNEL_MODE
#define RADIO_GLB_AUTOCHANNEL_TIME  OID_IAP_GLB_AUTOCHANNEL_TIME
#define RADIO_GLB_MAX_IAP_STATIONS  OID_IAP_GLB_MAX_IAP_STATIONS
#define OID_IAP_GLB_MAX_PHONES      OID_IAP_GLB_MAX_IAP_PHONES
#define RADIO_GLB_IAP_ALLOW_MGMT    OID_IAP_GLB_MGMT_ENABLED
#define TIME_DST_ADJUST             OID_TIME_DST_ADJUST
#define RADIO_GLB_STA2STA_BLOCK     OID_IAP_GLB_STA2STA_BLOCK       // block or forward intra-Array STA to STA traffic
#define SEC_WPA2_ENABLED            OID_SEC_WPA2_ENABLED
#define ETH_MODE                    OID_ETH_MODE                    // 0=Link Backup, 1=eth1 only, 2=eth2 only, 3=both, 4=daisy chain
#define RADIO_GLB_LOAD_BALANCING    OID_IAP_GLB_LOAD_BALANCING      // 0=off
#define RADIO_GLB_TX_COORDINATION   OID_IAP_GLB_TX_COORDINATION     // 0=off, 1=on, 2=cts
#define RADIO_GLB_RX_COORDINATION   OID_IAP_GLB_RX_COORDINATION     // 0=off
#define RADIO_GLB_TXRX_BALANCING    OID_IAP_GLB_TXRX_BALANCING      // 0=off
#define MGMT_HTTPS_ENABLED          OID_MGMT_HTTPS_ENABLED
#define LINE_ALLOW_MGMT             OID_MGMT_CON_ENABLED
#define TELNET_TIMEOUT              OID_TIMEOUT_TELNET
#define SSH_TIMEOUT                 OID_TIMEOUT_SSH
#define HTTPS_TIMEOUT               OID_TIMEOUT_HTTPS

#define SSID1_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(0)
#define SSID1_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(0)
#define SSID1_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(0)
#define SSID1_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(0)
#define SSID1_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(0)
#define SSID1_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(0)
#define SSID1_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(0)
#define SSID1_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(0)
#define SSID1_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(0)
#define SSID1_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(0)
#define SSID1_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(0)
#define SSID1_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(0)
#define SSID1_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(0)
#define SSID1_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(0)
#define SSID1_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(0)
#define SSID1_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(0)
#define SSID1_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(0)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define SSID2_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(1)
#define SSID2_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(1)
#define SSID2_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(1)
#define SSID2_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(1)
#define SSID2_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(1)
#define SSID2_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(1)
#define SSID2_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(1)
#define SSID2_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(1)
#define SSID2_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(1)
#define SSID2_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(1)
#define SSID2_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(1)
#define SSID2_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(1)
#define SSID2_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(1)
#define SSID2_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(1)
#define SSID2_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(1)
#define SSID2_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(1)
#define SSID2_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(1)

#define SSID3_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(2)
#define SSID3_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(2)
#define SSID3_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(2)
#define SSID3_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(2)
#define SSID3_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(2)
#define SSID3_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(2)
#define SSID3_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(2)
#define SSID3_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(2)
#define SSID3_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(2)
#define SSID3_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(2)
#define SSID3_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(2)
#define SSID3_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(2)
#define SSID3_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(2)
#define SSID3_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(2)
#define SSID3_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(2)
#define SSID3_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(2)
#define SSID3_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(2)

#define SSID4_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(3)
#define SSID4_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(3)
#define SSID4_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(3)
#define SSID4_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(3)
#define SSID4_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(3)
#define SSID4_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(3)
#define SSID4_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(3)
#define SSID4_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(3)
#define SSID4_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(3)
#define SSID4_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(3)
#define SSID4_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(3)
#define SSID4_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(3)
#define SSID4_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(3)
#define SSID4_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(3)
#define SSID4_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(3)
#define SSID4_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(3)
#define SSID4_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(3)

#define SSID5_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(4)
#define SSID5_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(4)
#define SSID5_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(4)
#define SSID5_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(4)
#define SSID5_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(4)
#define SSID5_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(4)
#define SSID5_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(4)
#define SSID5_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(4)
#define SSID5_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(4)
#define SSID5_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(4)
#define SSID5_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(4)
#define SSID5_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(4)
#define SSID5_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(4)
#define SSID5_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(4)
#define SSID5_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(4)
#define SSID5_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(4)
#define SSID5_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(4)

#define SSID6_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(5)
#define SSID6_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(5)
#define SSID6_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(5)
#define SSID6_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(5)
#define SSID6_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(5)
#define SSID6_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(5)
#define SSID6_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(5)
#define SSID6_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(5)
#define SSID6_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(5)
#define SSID6_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(5)
#define SSID6_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(5)
#define SSID6_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(5)
#define SSID6_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(5)
#define SSID6_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(5)
#define SSID6_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(5)
#define SSID6_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(5)
#define SSID6_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(5)

#define SSID7_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(6)
#define SSID7_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(6)
#define SSID7_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(6)
#define SSID7_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(6)
#define SSID7_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(6)
#define SSID7_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(6)
#define SSID7_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(6)
#define SSID7_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(6)
#define SSID7_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(6)
#define SSID7_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(6)
#define SSID7_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(6)
#define SSID7_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(6)
#define SSID7_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(6)
#define SSID7_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(6)
#define SSID7_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(6)
#define SSID7_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(6)
#define SSID7_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(6)

#define SSID8_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(7)
#define SSID8_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(7)
#define SSID8_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(7)
#define SSID8_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(7)
#define SSID8_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(7)
#define SSID8_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(7)
#define SSID8_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(7)
#define SSID8_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(7)
#define SSID8_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(7)
#define SSID8_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(7)
#define SSID8_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(7)
#define SSID8_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(7)
#define SSID8_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(7)
#define SSID8_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(7)
#define SSID8_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(7)
#define SSID8_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(7)
#define SSID8_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(7)

#define SSID9_WEP_ENABLED           OID_SSID_SEC_WEP_ENABLED(8)
#define SSID9_WEP_KEY0              OID_SSID_SEC_WEP_KEY1(8)
#define SSID9_WEP_KEY1              OID_SSID_SEC_WEP_KEY2(8)
#define SSID9_WEP_KEY2              OID_SSID_SEC_WEP_KEY3(8)
#define SSID9_WEP_KEY3              OID_SSID_SEC_WEP_KEY4(8)
#define SSID9_WEP_KEY0_LEN          OID_SSID_SEC_WEP_KEY1_LEN(8)
#define SSID9_WEP_KEY1_LEN          OID_SSID_SEC_WEP_KEY2_LEN(8)
#define SSID9_WEP_KEY2_LEN          OID_SSID_SEC_WEP_KEY3_LEN(8)
#define SSID9_WEP_KEY3_LEN          OID_SSID_SEC_WEP_KEY4_LEN(8)
#define SSID9_WEP_DEFAULT_KEY_ID    OID_SSID_SEC_WEP_KEY_ID_DEF(8)
#define SSID9_WPA_ENABLED           OID_SSID_SEC_WPA_ENABLED(8)
#define SSID9_WPA_TKIP_ENABLED      OID_SSID_SEC_WPA_TKIP_ON(8)
#define SSID9_WPA_AES_ENABLED       OID_SSID_SEC_WPA_AES_ON(8)
#define SSID9_WPA_EAP_ENABLED       OID_SSID_SEC_WPA_EAP_ON(8)
#define SSID9_WPA_PSK_ENABLED       OID_SSID_SEC_WPA_PSK_ON(8)
#define SSID9_WPA_PASSPHRASE        OID_SSID_SEC_WPA_PASSPHRASE(8)
#define SSID9_WPA_GROUP_REKEY       OID_SSID_SEC_WPA_GROUP_REKEY(8)

#define SSID10_WEP_ENABLED          OID_SSID_SEC_WEP_ENABLED(9)
#define SSID10_WEP_KEY0             OID_SSID_SEC_WEP_KEY1(9)
#define SSID10_WEP_KEY1             OID_SSID_SEC_WEP_KEY2(9)
#define SSID10_WEP_KEY2             OID_SSID_SEC_WEP_KEY3(9)
#define SSID10_WEP_KEY3             OID_SSID_SEC_WEP_KEY4(9)
#define SSID10_WEP_KEY0_LEN         OID_SSID_SEC_WEP_KEY1_LEN(9)
#define SSID10_WEP_KEY1_LEN         OID_SSID_SEC_WEP_KEY2_LEN(9)
#define SSID10_WEP_KEY2_LEN         OID_SSID_SEC_WEP_KEY3_LEN(9)
#define SSID10_WEP_KEY3_LEN         OID_SSID_SEC_WEP_KEY4_LEN(9)
#define SSID10_WEP_DEFAULT_KEY_ID   OID_SSID_SEC_WEP_KEY_ID_DEF(9)
#define SSID10_WPA_ENABLED          OID_SSID_SEC_WPA_ENABLED(9)
#define SSID10_WPA_TKIP_ENABLED     OID_SSID_SEC_WPA_TKIP_ON(9)
#define SSID10_WPA_AES_ENABLED      OID_SSID_SEC_WPA_AES_ON(9)
#define SSID10_WPA_EAP_ENABLED      OID_SSID_SEC_WPA_EAP_ON(9)
#define SSID10_WPA_PSK_ENABLED      OID_SSID_SEC_WPA_PSK_ON(9)
#define SSID10_WPA_PASSPHRASE       OID_SSID_SEC_WPA_PASSPHRASE(9)
#define SSID10_WPA_GROUP_REKEY      OID_SSID_SEC_WPA_GROUP_REKEY(9)

#define SSID11_WEP_ENABLED          OID_SSID_SEC_WEP_ENABLED(10)
#define SSID11_WEP_KEY0             OID_SSID_SEC_WEP_KEY1(10)
#define SSID11_WEP_KEY1             OID_SSID_SEC_WEP_KEY2(10)
#define SSID11_WEP_KEY2             OID_SSID_SEC_WEP_KEY3(10)
#define SSID11_WEP_KEY3             OID_SSID_SEC_WEP_KEY4(10)
#define SSID11_WEP_KEY0_LEN         OID_SSID_SEC_WEP_KEY1_LEN(10)
#define SSID11_WEP_KEY1_LEN         OID_SSID_SEC_WEP_KEY2_LEN(10)
#define SSID11_WEP_KEY2_LEN         OID_SSID_SEC_WEP_KEY3_LEN(10)
#define SSID11_WEP_KEY3_LEN         OID_SSID_SEC_WEP_KEY4_LEN(10)
#define SSID11_WEP_DEFAULT_KEY_ID   OID_SSID_SEC_WEP_KEY_ID_DEF(10)
#define SSID11_WPA_ENABLED          OID_SSID_SEC_WPA_ENABLED(10)
#define SSID11_WPA_TKIP_ENABLED     OID_SSID_SEC_WPA_TKIP_ON(10)
#define SSID11_WPA_AES_ENABLED      OID_SSID_SEC_WPA_AES_ON(10)
#define SSID11_WPA_EAP_ENABLED      OID_SSID_SEC_WPA_EAP_ON(10)
#define SSID11_WPA_PSK_ENABLED      OID_SSID_SEC_WPA_PSK_ON(10)
#define SSID11_WPA_PASSPHRASE       OID_SSID_SEC_WPA_PASSPHRASE(10)
#define SSID11_WPA_GROUP_REKEY      OID_SSID_SEC_WPA_GROUP_REKEY(10)

#define SSID12_WEP_ENABLED          OID_SSID_SEC_WEP_ENABLED(11)
#define SSID12_WEP_KEY0             OID_SSID_SEC_WEP_KEY1(11)
#define SSID12_WEP_KEY1             OID_SSID_SEC_WEP_KEY2(11)
#define SSID12_WEP_KEY2             OID_SSID_SEC_WEP_KEY3(11)
#define SSID12_WEP_KEY3             OID_SSID_SEC_WEP_KEY4(11)
#define SSID12_WEP_KEY0_LEN         OID_SSID_SEC_WEP_KEY1_LEN(11)
#define SSID12_WEP_KEY1_LEN         OID_SSID_SEC_WEP_KEY2_LEN(11)
#define SSID12_WEP_KEY2_LEN         OID_SSID_SEC_WEP_KEY3_LEN(11)
#define SSID12_WEP_KEY3_LEN         OID_SSID_SEC_WEP_KEY4_LEN(11)
#define SSID12_WEP_DEFAULT_KEY_ID   OID_SSID_SEC_WEP_KEY_ID_DEF(11)
#define SSID12_WPA_ENABLED          OID_SSID_SEC_WPA_ENABLED(11)
#define SSID12_WPA_TKIP_ENABLED     OID_SSID_SEC_WPA_TKIP_ON(11)
#define SSID12_WPA_AES_ENABLED      OID_SSID_SEC_WPA_AES_ON(11)
#define SSID12_WPA_EAP_ENABLED      OID_SSID_SEC_WPA_EAP_ON(11)
#define SSID12_WPA_PSK_ENABLED      OID_SSID_SEC_WPA_PSK_ON(11)
#define SSID12_WPA_PASSPHRASE       OID_SSID_SEC_WPA_PASSPHRASE(11)
#define SSID12_WPA_GROUP_REKEY      OID_SSID_SEC_WPA_GROUP_REKEY(11)

#define SSID13_WEP_ENABLED          OID_SSID_SEC_WEP_ENABLED(12)
#define SSID13_WEP_KEY0             OID_SSID_SEC_WEP_KEY1(12)
#define SSID13_WEP_KEY1             OID_SSID_SEC_WEP_KEY2(12)
#define SSID13_WEP_KEY2             OID_SSID_SEC_WEP_KEY3(12)
#define SSID13_WEP_KEY3             OID_SSID_SEC_WEP_KEY4(12)
#define SSID13_WEP_KEY0_LEN         OID_SSID_SEC_WEP_KEY1_LEN(12)
#define SSID13_WEP_KEY1_LEN         OID_SSID_SEC_WEP_KEY2_LEN(12)
#define SSID13_WEP_KEY2_LEN         OID_SSID_SEC_WEP_KEY3_LEN(12)
#define SSID13_WEP_KEY3_LEN         OID_SSID_SEC_WEP_KEY4_LEN(12)
#define SSID13_WEP_DEFAULT_KEY_ID   OID_SSID_SEC_WEP_KEY_ID_DEF(12)
#define SSID13_WPA_ENABLED          OID_SSID_SEC_WPA_ENABLED(12)
#define SSID13_WPA_TKIP_ENABLED     OID_SSID_SEC_WPA_TKIP_ON(12)
#define SSID13_WPA_AES_ENABLED      OID_SSID_SEC_WPA_AES_ON(12)
#define SSID13_WPA_EAP_ENABLED      OID_SSID_SEC_WPA_EAP_ON(12)
#define SSID13_WPA_PSK_ENABLED      OID_SSID_SEC_WPA_PSK_ON(12)
#define SSID13_WPA_PASSPHRASE       OID_SSID_SEC_WPA_PASSPHRASE(12)
#define SSID13_WPA_GROUP_REKEY      OID_SSID_SEC_WPA_GROUP_REKEY(12)

#define SSID14_WEP_ENABLED          OID_SSID_SEC_WEP_ENABLED(13)
#define SSID14_WEP_KEY0             OID_SSID_SEC_WEP_KEY1(13)
#define SSID14_WEP_KEY1             OID_SSID_SEC_WEP_KEY2(13)
#define SSID14_WEP_KEY2             OID_SSID_SEC_WEP_KEY3(13)
#define SSID14_WEP_KEY3             OID_SSID_SEC_WEP_KEY4(13)
#define SSID14_WEP_KEY0_LEN         OID_SSID_SEC_WEP_KEY1_LEN(13)
#define SSID14_WEP_KEY1_LEN         OID_SSID_SEC_WEP_KEY2_LEN(13)
#define SSID14_WEP_KEY2_LEN         OID_SSID_SEC_WEP_KEY3_LEN(13)
#define SSID14_WEP_KEY3_LEN         OID_SSID_SEC_WEP_KEY4_LEN(13)
#define SSID14_WEP_DEFAULT_KEY_ID   OID_SSID_SEC_WEP_KEY_ID_DEF(13)
#define SSID14_WPA_ENABLED          OID_SSID_SEC_WPA_ENABLED(13)
#define SSID14_WPA_TKIP_ENABLED     OID_SSID_SEC_WPA_TKIP_ON(13)
#define SSID14_WPA_AES_ENABLED      OID_SSID_SEC_WPA_AES_ON(13)
#define SSID14_WPA_EAP_ENABLED      OID_SSID_SEC_WPA_EAP_ON(13)
#define SSID14_WPA_PSK_ENABLED      OID_SSID_SEC_WPA_PSK_ON(13)
#define SSID14_WPA_PASSPHRASE       OID_SSID_SEC_WPA_PASSPHRASE(13)
#define SSID14_WPA_GROUP_REKEY      OID_SSID_SEC_WPA_GROUP_REKEY(13)

#define SSID15_WEP_ENABLED          OID_SSID_SEC_WEP_ENABLED(14)
#define SSID15_WEP_KEY0             OID_SSID_SEC_WEP_KEY1(14)
#define SSID15_WEP_KEY1             OID_SSID_SEC_WEP_KEY2(14)
#define SSID15_WEP_KEY2             OID_SSID_SEC_WEP_KEY3(14)
#define SSID15_WEP_KEY3             OID_SSID_SEC_WEP_KEY4(14)
#define SSID15_WEP_KEY0_LEN         OID_SSID_SEC_WEP_KEY1_LEN(14)
#define SSID15_WEP_KEY1_LEN         OID_SSID_SEC_WEP_KEY2_LEN(14)
#define SSID15_WEP_KEY2_LEN         OID_SSID_SEC_WEP_KEY3_LEN(14)
#define SSID15_WEP_KEY3_LEN         OID_SSID_SEC_WEP_KEY4_LEN(14)
#define SSID15_WEP_DEFAULT_KEY_ID   OID_SSID_SEC_WEP_KEY_ID_DEF(14)
#define SSID15_WPA_ENABLED          OID_SSID_SEC_WPA_ENABLED(14)
#define SSID15_WPA_TKIP_ENABLED     OID_SSID_SEC_WPA_TKIP_ON(14)
#define SSID15_WPA_AES_ENABLED      OID_SSID_SEC_WPA_AES_ON(14)
#define SSID15_WPA_EAP_ENABLED      OID_SSID_SEC_WPA_EAP_ON(14)
#define SSID15_WPA_PSK_ENABLED      OID_SSID_SEC_WPA_PSK_ON(14)
#define SSID15_WPA_PASSPHRASE       OID_SSID_SEC_WPA_PASSPHRASE(14)
#define SSID15_WPA_GROUP_REKEY      OID_SSID_SEC_WPA_GROUP_REKEY(14)

#define SSID16_WEP_ENABLED          OID_SSID_SEC_WEP_ENABLED(15)
#define SSID16_WEP_KEY0             OID_SSID_SEC_WEP_KEY1(15)
#define SSID16_WEP_KEY1             OID_SSID_SEC_WEP_KEY2(15)
#define SSID16_WEP_KEY2             OID_SSID_SEC_WEP_KEY3(15)
#define SSID16_WEP_KEY3             OID_SSID_SEC_WEP_KEY4(15)
#define SSID16_WEP_KEY0_LEN         OID_SSID_SEC_WEP_KEY1_LEN(15)
#define SSID16_WEP_KEY1_LEN         OID_SSID_SEC_WEP_KEY2_LEN(15)
#define SSID16_WEP_KEY2_LEN         OID_SSID_SEC_WEP_KEY3_LEN(15)
#define SSID16_WEP_KEY3_LEN         OID_SSID_SEC_WEP_KEY4_LEN(15)
#define SSID16_WEP_DEFAULT_KEY_ID   OID_SSID_SEC_WEP_KEY_ID_DEF(15)
#define SSID16_WPA_ENABLED          OID_SSID_SEC_WPA_ENABLED(15)
#define SSID16_WPA_TKIP_ENABLED     OID_SSID_SEC_WPA_TKIP_ON(15)
#define SSID16_WPA_AES_ENABLED      OID_SSID_SEC_WPA_AES_ON(15)
#define SSID16_WPA_EAP_ENABLED      OID_SSID_SEC_WPA_EAP_ON(15)
#define SSID16_WPA_PSK_ENABLED      OID_SSID_SEC_WPA_PSK_ON(15)
#define SSID16_WPA_PASSPHRASE       OID_SSID_SEC_WPA_PASSPHRASE(15)
#define SSID16_WPA_GROUP_REKEY      OID_SSID_SEC_WPA_GROUP_REKEY(15)

#define SSID1_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(0)
#define SSID1_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(0)
#define SSID1_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(0)
#define SSID1_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(0)
#define SSID1_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(0)
#define SSID1_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(0)
#define SSID1_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(0)
#define SSID1_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(0)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define SSID2_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(1)
#define SSID2_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(1)
#define SSID2_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(1)
#define SSID2_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(1)
#define SSID2_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(1)
#define SSID2_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(1)
#define SSID2_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(1)
#define SSID2_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(1)

#define SSID3_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(2)
#define SSID3_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(2)
#define SSID3_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(2)
#define SSID3_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(2)
#define SSID3_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(2)
#define SSID3_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(2)
#define SSID3_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(2)
#define SSID3_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(2)

#define SSID4_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(3)
#define SSID4_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(3)
#define SSID4_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(3)
#define SSID4_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(3)
#define SSID4_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(3)
#define SSID4_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(3)
#define SSID4_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(3)
#define SSID4_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(3)

#define SSID5_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(4)
#define SSID5_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(4)
#define SSID5_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(4)
#define SSID5_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(4)
#define SSID5_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(4)
#define SSID5_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(4)
#define SSID5_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(4)
#define SSID5_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(4)

#define SSID6_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(5)
#define SSID6_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(5)
#define SSID6_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(5)
#define SSID6_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(5)
#define SSID6_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(5)
#define SSID6_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(5)
#define SSID6_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(5)
#define SSID6_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(5)

#define SSID7_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(6)
#define SSID7_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(6)
#define SSID7_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(6)
#define SSID7_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(6)
#define SSID7_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(6)
#define SSID7_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(6)
#define SSID7_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(6)
#define SSID7_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(6)

#define SSID8_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(7)
#define SSID8_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(7)
#define SSID8_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(7)
#define SSID8_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(7)
#define SSID8_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(7)
#define SSID8_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(7)
#define SSID8_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(7)
#define SSID8_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(7)

#define SSID9_RADIUS_ENABLED        OID_SSID_RAD_ENABLED(8)
#define SSID9_RADIUS_PRI_PORT       OID_SSID_RAD_PRI_PORT(8)
#define SSID9_RADIUS_PRI_SECRET     OID_SSID_RAD_PRI_SECRET(8)
#define SSID9_RADIUS_PRI_SERVER     OID_SSID_RAD_PRI_SERVER(8)
#define SSID9_RADIUS_SEC_PORT       OID_SSID_RAD_SEC_PORT(8)
#define SSID9_RADIUS_SEC_SECRET     OID_SSID_RAD_SEC_SECRET(8)
#define SSID9_RADIUS_SEC_SERVER     OID_SSID_RAD_SEC_SERVER(8)
#define SSID9_RADIUS_TIMEOUT        OID_SSID_RAD_TIMEOUT(8)

#define SSID10_RADIUS_ENABLED       OID_SSID_RAD_ENABLED(9)
#define SSID10_RADIUS_PRI_PORT      OID_SSID_RAD_PRI_PORT(9)
#define SSID10_RADIUS_PRI_SECRET    OID_SSID_RAD_PRI_SECRET(9)
#define SSID10_RADIUS_PRI_SERVER    OID_SSID_RAD_PRI_SERVER(9)
#define SSID10_RADIUS_SEC_PORT      OID_SSID_RAD_SEC_PORT(9)
#define SSID10_RADIUS_SEC_SECRET    OID_SSID_RAD_SEC_SECRET(9)
#define SSID10_RADIUS_SEC_SERVER    OID_SSID_RAD_SEC_SERVER(9)
#define SSID10_RADIUS_TIMEOUT       OID_SSID_RAD_TIMEOUT(9)

#define SSID11_RADIUS_ENABLED       OID_SSID_RAD_ENABLED(10)
#define SSID11_RADIUS_PRI_PORT      OID_SSID_RAD_PRI_PORT(10)
#define SSID11_RADIUS_PRI_SECRET    OID_SSID_RAD_PRI_SECRET(10)
#define SSID11_RADIUS_PRI_SERVER    OID_SSID_RAD_PRI_SERVER(10)
#define SSID11_RADIUS_SEC_PORT      OID_SSID_RAD_SEC_PORT(10)
#define SSID11_RADIUS_SEC_SECRET    OID_SSID_RAD_SEC_SECRET(10)
#define SSID11_RADIUS_SEC_SERVER    OID_SSID_RAD_SEC_SERVER(10)
#define SSID11_RADIUS_TIMEOUT       OID_SSID_RAD_TIMEOUT(10)

#define SSID12_RADIUS_ENABLED       OID_SSID_RAD_ENABLED(11)
#define SSID12_RADIUS_PRI_PORT      OID_SSID_RAD_PRI_PORT(11)
#define SSID12_RADIUS_PRI_SECRET    OID_SSID_RAD_PRI_SECRET(11)
#define SSID12_RADIUS_PRI_SERVER    OID_SSID_RAD_PRI_SERVER(11)
#define SSID12_RADIUS_SEC_PORT      OID_SSID_RAD_SEC_PORT(11)
#define SSID12_RADIUS_SEC_SECRET    OID_SSID_RAD_SEC_SECRET(11)
#define SSID12_RADIUS_SEC_SERVER    OID_SSID_RAD_SEC_SERVER(11)
#define SSID12_RADIUS_TIMEOUT       OID_SSID_RAD_TIMEOUT(11)

#define SSID13_RADIUS_ENABLED       OID_SSID_RAD_ENABLED(12)
#define SSID13_RADIUS_PRI_PORT      OID_SSID_RAD_PRI_PORT(12)
#define SSID13_RADIUS_PRI_SECRET    OID_SSID_RAD_PRI_SECRET(12)
#define SSID13_RADIUS_PRI_SERVER    OID_SSID_RAD_PRI_SERVER(12)
#define SSID13_RADIUS_SEC_PORT      OID_SSID_RAD_SEC_PORT(12)
#define SSID13_RADIUS_SEC_SECRET    OID_SSID_RAD_SEC_SECRET(12)
#define SSID13_RADIUS_SEC_SERVER    OID_SSID_RAD_SEC_SERVER(12)
#define SSID13_RADIUS_TIMEOUT       OID_SSID_RAD_TIMEOUT(12)

#define SSID14_RADIUS_ENABLED       OID_SSID_RAD_ENABLED(13)
#define SSID14_RADIUS_PRI_PORT      OID_SSID_RAD_PRI_PORT(13)
#define SSID14_RADIUS_PRI_SECRET    OID_SSID_RAD_PRI_SECRET(13)
#define SSID14_RADIUS_PRI_SERVER    OID_SSID_RAD_PRI_SERVER(13)
#define SSID14_RADIUS_SEC_PORT      OID_SSID_RAD_SEC_PORT(13)
#define SSID14_RADIUS_SEC_SECRET    OID_SSID_RAD_SEC_SECRET(13)
#define SSID14_RADIUS_SEC_SERVER    OID_SSID_RAD_SEC_SERVER(13)
#define SSID14_RADIUS_TIMEOUT       OID_SSID_RAD_TIMEOUT(13)

#define SSID15_RADIUS_ENABLED       OID_SSID_RAD_ENABLED(14)
#define SSID15_RADIUS_PRI_PORT      OID_SSID_RAD_PRI_PORT(14)
#define SSID15_RADIUS_PRI_SECRET    OID_SSID_RAD_PRI_SECRET(14)
#define SSID15_RADIUS_PRI_SERVER    OID_SSID_RAD_PRI_SERVER(14)
#define SSID15_RADIUS_SEC_PORT      OID_SSID_RAD_SEC_PORT(14)
#define SSID15_RADIUS_SEC_SECRET    OID_SSID_RAD_SEC_SECRET(14)
#define SSID15_RADIUS_SEC_SERVER    OID_SSID_RAD_SEC_SERVER(14)
#define SSID15_RADIUS_TIMEOUT       OID_SSID_RAD_TIMEOUT(14)

#define SSID16_RADIUS_ENABLED       OID_SSID_RAD_ENABLED(15)
#define SSID16_RADIUS_PRI_PORT      OID_SSID_RAD_PRI_PORT(15)
#define SSID16_RADIUS_PRI_SECRET    OID_SSID_RAD_PRI_SECRET(15)
#define SSID16_RADIUS_PRI_SERVER    OID_SSID_RAD_PRI_SERVER(15)
#define SSID16_RADIUS_SEC_PORT      OID_SSID_RAD_SEC_PORT(15)
#define SSID16_RADIUS_SEC_SECRET    OID_SSID_RAD_SEC_SECRET(15)
#define SSID16_RADIUS_SEC_SERVER    OID_SSID_RAD_SEC_SERVER(15)
#define SSID16_RADIUS_TIMEOUT       OID_SSID_RAD_TIMEOUT(15)

#define SSID1_DEFAULT_SECURITY      OID_SSID_SEC_DEF(0)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define SSID2_DEFAULT_SECURITY      OID_SSID_SEC_DEF(1)
#define SSID3_DEFAULT_SECURITY      OID_SSID_SEC_DEF(2)
#define SSID4_DEFAULT_SECURITY      OID_SSID_SEC_DEF(3)
#define SSID5_DEFAULT_SECURITY      OID_SSID_SEC_DEF(4)
#define SSID6_DEFAULT_SECURITY      OID_SSID_SEC_DEF(5)
#define SSID7_DEFAULT_SECURITY      OID_SSID_SEC_DEF(6)
#define SSID8_DEFAULT_SECURITY      OID_SSID_SEC_DEF(7)
#define SSID9_DEFAULT_SECURITY      OID_SSID_SEC_DEF(8)
#define SSID10_DEFAULT_SECURITY     OID_SSID_SEC_DEF(9)
#define SSID11_DEFAULT_SECURITY     OID_SSID_SEC_DEF(10)
#define SSID12_DEFAULT_SECURITY     OID_SSID_SEC_DEF(11)
#define SSID13_DEFAULT_SECURITY     OID_SSID_SEC_DEF(12)
#define SSID14_DEFAULT_SECURITY     OID_SSID_SEC_DEF(13)
#define SSID15_DEFAULT_SECURITY     OID_SSID_SEC_DEF(14)
#define SSID16_DEFAULT_SECURITY     OID_SSID_SEC_DEF(15)

#define SSID1_MAC_NUM               OID_SSID_MAC_NUM(0)     // used also to format 'exit' on output
#define SSID2_MAC_NUM               OID_SSID_MAC_NUM(1)
#define SSID3_MAC_NUM               OID_SSID_MAC_NUM(2)
#define SSID4_MAC_NUM               OID_SSID_MAC_NUM(3)
#define SSID5_MAC_NUM               OID_SSID_MAC_NUM(4)
#define SSID6_MAC_NUM               OID_SSID_MAC_NUM(5)
#define SSID7_MAC_NUM               OID_SSID_MAC_NUM(6)
#define SSID8_MAC_NUM               OID_SSID_MAC_NUM(7)
#define SSID9_MAC_NUM               OID_SSID_MAC_NUM(8)
#define SSID10_MAC_NUM              OID_SSID_MAC_NUM(9)
#define SSID11_MAC_NUM              OID_SSID_MAC_NUM(10)
#define SSID12_MAC_NUM              OID_SSID_MAC_NUM(11)
#define SSID13_MAC_NUM              OID_SSID_MAC_NUM(12)
#define SSID14_MAC_NUM              OID_SSID_MAC_NUM(13)
#define SSID15_MAC_NUM              OID_SSID_MAC_NUM(14)
#define SSID16_MAC_NUM              OID_SSID_MAC_NUM(15)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define DHCP_POOL1_ENABLED          OID_DHCP_POOL_ENABLED(0)
#define DHCP_POOL1_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(0)
#define DHCP_POOL1_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(0)
#define DHCP_POOL1_RANGE_END        OID_DHCP_POOL_RANGE_END(0)
#define DHCP_POOL1_RANGE_START      OID_DHCP_POOL_RANGE_START(0)
#define DHCP_POOL1_GATEWAY          OID_DHCP_POOL_GATEWAY(0)
#define DHCP_POOL1_MASK             OID_DHCP_POOL_MASK(0)
#define DHCP_POOL1_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(0)
#define DHCP_POOL1_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(0)
#define DHCP_POOL1_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(0)
#define DHCP_POOL1_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(0)

#define DHCP_POOL2_ENABLED          OID_DHCP_POOL_ENABLED(1)
#define DHCP_POOL2_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(1)
#define DHCP_POOL2_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(1)
#define DHCP_POOL2_RANGE_END        OID_DHCP_POOL_RANGE_END(1)
#define DHCP_POOL2_RANGE_START      OID_DHCP_POOL_RANGE_START(1)
#define DHCP_POOL2_GATEWAY          OID_DHCP_POOL_GATEWAY(1)
#define DHCP_POOL2_MASK             OID_DHCP_POOL_MASK(1)
#define DHCP_POOL2_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(1)
#define DHCP_POOL2_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(1)
#define DHCP_POOL2_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(1)
#define DHCP_POOL2_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(1)

#define DHCP_POOL3_ENABLED          OID_DHCP_POOL_ENABLED(4)
#define DHCP_POOL3_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(4)
#define DHCP_POOL3_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(4)
#define DHCP_POOL3_RANGE_END        OID_DHCP_POOL_RANGE_END(4)
#define DHCP_POOL3_RANGE_START      OID_DHCP_POOL_RANGE_START(4)
#define DHCP_POOL3_GATEWAY          OID_DHCP_POOL_GATEWAY(4)
#define DHCP_POOL3_MASK             OID_DHCP_POOL_MASK(4)
#define DHCP_POOL3_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(4)
#define DHCP_POOL3_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(4)
#define DHCP_POOL3_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(4)
#define DHCP_POOL3_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(4)

#define DHCP_POOL4_ENABLED          OID_DHCP_POOL_ENABLED(3)
#define DHCP_POOL4_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(3)
#define DHCP_POOL4_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(3)
#define DHCP_POOL4_RANGE_END        OID_DHCP_POOL_RANGE_END(3)
#define DHCP_POOL4_RANGE_START      OID_DHCP_POOL_RANGE_START(3)
#define DHCP_POOL4_GATEWAY          OID_DHCP_POOL_GATEWAY(3)
#define DHCP_POOL4_MASK             OID_DHCP_POOL_MASK(3)
#define DHCP_POOL4_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(3)
#define DHCP_POOL4_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(3)
#define DHCP_POOL4_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(3)
#define DHCP_POOL4_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(3)

#define DHCP_POOL5_ENABLED          OID_DHCP_POOL_ENABLED(4)
#define DHCP_POOL5_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(4)
#define DHCP_POOL5_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(4)
#define DHCP_POOL5_RANGE_END        OID_DHCP_POOL_RANGE_END(4)
#define DHCP_POOL5_RANGE_START      OID_DHCP_POOL_RANGE_START(4)
#define DHCP_POOL5_GATEWAY          OID_DHCP_POOL_GATEWAY(4)
#define DHCP_POOL5_MASK             OID_DHCP_POOL_MASK(4)
#define DHCP_POOL5_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(4)
#define DHCP_POOL5_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(4)
#define DHCP_POOL5_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(4)
#define DHCP_POOL5_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(4)

#define DHCP_POOL6_ENABLED          OID_DHCP_POOL_ENABLED(5)
#define DHCP_POOL6_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(5)
#define DHCP_POOL6_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(5)
#define DHCP_POOL6_RANGE_END        OID_DHCP_POOL_RANGE_END(5)
#define DHCP_POOL6_RANGE_START      OID_DHCP_POOL_RANGE_START(5)
#define DHCP_POOL6_GATEWAY          OID_DHCP_POOL_GATEWAY(5)
#define DHCP_POOL6_MASK             OID_DHCP_POOL_MASK(5)
#define DHCP_POOL6_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(5)
#define DHCP_POOL6_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(5)
#define DHCP_POOL6_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(5)
#define DHCP_POOL6_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(5)

#define DHCP_POOL7_ENABLED          OID_DHCP_POOL_ENABLED(6)
#define DHCP_POOL7_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(6)
#define DHCP_POOL7_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(6)
#define DHCP_POOL7_RANGE_END        OID_DHCP_POOL_RANGE_END(6)
#define DHCP_POOL7_RANGE_START      OID_DHCP_POOL_RANGE_START(6)
#define DHCP_POOL7_GATEWAY          OID_DHCP_POOL_GATEWAY(6)
#define DHCP_POOL7_MASK             OID_DHCP_POOL_MASK(6)
#define DHCP_POOL7_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(6)
#define DHCP_POOL7_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(6)
#define DHCP_POOL7_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(6)
#define DHCP_POOL7_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(6)

#define DHCP_POOL8_ENABLED          OID_DHCP_POOL_ENABLED(7)
#define DHCP_POOL8_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(7)
#define DHCP_POOL8_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(7)
#define DHCP_POOL8_RANGE_END        OID_DHCP_POOL_RANGE_END(7)
#define DHCP_POOL8_RANGE_START      OID_DHCP_POOL_RANGE_START(7)
#define DHCP_POOL8_GATEWAY          OID_DHCP_POOL_GATEWAY(7)
#define DHCP_POOL8_MASK             OID_DHCP_POOL_MASK(7)
#define DHCP_POOL8_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(7)
#define DHCP_POOL8_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(7)
#define DHCP_POOL8_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(7)
#define DHCP_POOL8_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(7)

#define DHCP_POOL9_ENABLED          OID_DHCP_POOL_ENABLED(8)
#define DHCP_POOL9_DEFAULT_LEASE    OID_DHCP_POOL_LEASE_DEFAULT(8)
#define DHCP_POOL9_MAX_LEASE        OID_DHCP_POOL_LEASE_MAX(8)
#define DHCP_POOL9_RANGE_END        OID_DHCP_POOL_RANGE_END(8)
#define DHCP_POOL9_RANGE_START      OID_DHCP_POOL_RANGE_START(8)
#define DHCP_POOL9_GATEWAY          OID_DHCP_POOL_GATEWAY(8)
#define DHCP_POOL9_MASK             OID_DHCP_POOL_MASK(8)
#define DHCP_POOL9_DNS_DOMAIN       OID_DHCP_POOL_DNS_DOMAIN(8)
#define DHCP_POOL9_DNS_SERVER1      OID_DHCP_POOL_DNS_SERVER1(8)
#define DHCP_POOL9_DNS_SERVER2      OID_DHCP_POOL_DNS_SERVER2(8)
#define DHCP_POOL9_DNS_SERVER3      OID_DHCP_POOL_DNS_SERVER3(8)

#define DHCP_POOL10_ENABLED         OID_DHCP_POOL_ENABLED(9)
#define DHCP_POOL10_DEFAULT_LEASE   OID_DHCP_POOL_LEASE_DEFAULT(9)
#define DHCP_POOL10_MAX_LEASE       OID_DHCP_POOL_LEASE_MAX(9)
#define DHCP_POOL10_RANGE_END       OID_DHCP_POOL_RANGE_END(9)
#define DHCP_POOL10_RANGE_START     OID_DHCP_POOL_RANGE_START(9)
#define DHCP_POOL10_GATEWAY         OID_DHCP_POOL_GATEWAY(9)
#define DHCP_POOL10_MASK            OID_DHCP_POOL_MASK(9)
#define DHCP_POOL10_DNS_DOMAIN      OID_DHCP_POOL_DNS_DOMAIN(9)
#define DHCP_POOL10_DNS_SERVER1     OID_DHCP_POOL_DNS_SERVER1(9)
#define DHCP_POOL10_DNS_SERVER2     OID_DHCP_POOL_DNS_SERVER2(9)
#define DHCP_POOL10_DNS_SERVER3     OID_DHCP_POOL_DNS_SERVER3(9)

#define DHCP_POOL11_ENABLED         OID_DHCP_POOL_ENABLED(10)
#define DHCP_POOL11_DEFAULT_LEASE   OID_DHCP_POOL_LEASE_DEFAULT(10)
#define DHCP_POOL11_MAX_LEASE       OID_DHCP_POOL_LEASE_MAX(10)
#define DHCP_POOL11_RANGE_END       OID_DHCP_POOL_RANGE_END(10)
#define DHCP_POOL11_RANGE_START     OID_DHCP_POOL_RANGE_START(10)
#define DHCP_POOL11_GATEWAY         OID_DHCP_POOL_GATEWAY(10)
#define DHCP_POOL11_MASK            OID_DHCP_POOL_MASK(10)
#define DHCP_POOL11_DNS_DOMAIN      OID_DHCP_POOL_DNS_DOMAIN(10)
#define DHCP_POOL11_DNS_SERVER1     OID_DHCP_POOL_DNS_SERVER1(10)
#define DHCP_POOL11_DNS_SERVER2     OID_DHCP_POOL_DNS_SERVER2(10)
#define DHCP_POOL11_DNS_SERVER3     OID_DHCP_POOL_DNS_SERVER3(10)

#define DHCP_POOL12_ENABLED         OID_DHCP_POOL_ENABLED(11)
#define DHCP_POOL12_DEFAULT_LEASE   OID_DHCP_POOL_LEASE_DEFAULT(11)
#define DHCP_POOL12_MAX_LEASE       OID_DHCP_POOL_LEASE_MAX(11)
#define DHCP_POOL12_RANGE_END       OID_DHCP_POOL_RANGE_END(11)
#define DHCP_POOL12_RANGE_START     OID_DHCP_POOL_RANGE_START(11)
#define DHCP_POOL12_GATEWAY         OID_DHCP_POOL_GATEWAY(11)
#define DHCP_POOL12_MASK            OID_DHCP_POOL_MASK(11)
#define DHCP_POOL12_DNS_DOMAIN      OID_DHCP_POOL_DNS_DOMAIN(11)
#define DHCP_POOL12_DNS_SERVER1     OID_DHCP_POOL_DNS_SERVER1(11)
#define DHCP_POOL12_DNS_SERVER2     OID_DHCP_POOL_DNS_SERVER2(11)
#define DHCP_POOL12_DNS_SERVER3     OID_DHCP_POOL_DNS_SERVER3(11)

#define DHCP_POOL13_ENABLED         OID_DHCP_POOL_ENABLED(12)
#define DHCP_POOL13_DEFAULT_LEASE   OID_DHCP_POOL_LEASE_DEFAULT(12)
#define DHCP_POOL13_MAX_LEASE       OID_DHCP_POOL_LEASE_MAX(12)
#define DHCP_POOL13_RANGE_END       OID_DHCP_POOL_RANGE_END(12)
#define DHCP_POOL13_RANGE_START     OID_DHCP_POOL_RANGE_START(12)
#define DHCP_POOL13_GATEWAY         OID_DHCP_POOL_GATEWAY(12)
#define DHCP_POOL13_MASK            OID_DHCP_POOL_MASK(12)
#define DHCP_POOL13_DNS_DOMAIN      OID_DHCP_POOL_DNS_DOMAIN(12)
#define DHCP_POOL13_DNS_SERVER1     OID_DHCP_POOL_DNS_SERVER1(12)
#define DHCP_POOL13_DNS_SERVER2     OID_DHCP_POOL_DNS_SERVER2(12)
#define DHCP_POOL13_DNS_SERVER3     OID_DHCP_POOL_DNS_SERVER3(12)

#define DHCP_POOL14_ENABLED         OID_DHCP_POOL_ENABLED(13)
#define DHCP_POOL14_DEFAULT_LEASE   OID_DHCP_POOL_LEASE_DEFAULT(13)
#define DHCP_POOL14_MAX_LEASE       OID_DHCP_POOL_LEASE_MAX(13)
#define DHCP_POOL14_RANGE_END       OID_DHCP_POOL_RANGE_END(13)
#define DHCP_POOL14_RANGE_START     OID_DHCP_POOL_RANGE_START(13)
#define DHCP_POOL14_GATEWAY         OID_DHCP_POOL_GATEWAY(13)
#define DHCP_POOL14_MASK            OID_DHCP_POOL_MASK(13)
#define DHCP_POOL14_DNS_DOMAIN      OID_DHCP_POOL_DNS_DOMAIN(13)
#define DHCP_POOL14_DNS_SERVER1     OID_DHCP_POOL_DNS_SERVER1(13)
#define DHCP_POOL14_DNS_SERVER2     OID_DHCP_POOL_DNS_SERVER2(13)
#define DHCP_POOL14_DNS_SERVER3     OID_DHCP_POOL_DNS_SERVER3(13)

#define DHCP_POOL15_ENABLED         OID_DHCP_POOL_ENABLED(14)
#define DHCP_POOL15_DEFAULT_LEASE   OID_DHCP_POOL_LEASE_DEFAULT(14)
#define DHCP_POOL15_MAX_LEASE       OID_DHCP_POOL_LEASE_MAX(14)
#define DHCP_POOL15_RANGE_END       OID_DHCP_POOL_RANGE_END(14)
#define DHCP_POOL15_RANGE_START     OID_DHCP_POOL_RANGE_START(14)
#define DHCP_POOL15_GATEWAY         OID_DHCP_POOL_GATEWAY(14)
#define DHCP_POOL15_MASK            OID_DHCP_POOL_MASK(14)
#define DHCP_POOL15_DNS_DOMAIN      OID_DHCP_POOL_DNS_DOMAIN(14)
#define DHCP_POOL15_DNS_SERVER1     OID_DHCP_POOL_DNS_SERVER1(14)
#define DHCP_POOL15_DNS_SERVER2     OID_DHCP_POOL_DNS_SERVER2(14)
#define DHCP_POOL15_DNS_SERVER3     OID_DHCP_POOL_DNS_SERVER3(14)

#define DHCP_POOL16_ENABLED         OID_DHCP_POOL_ENABLED(15)
#define DHCP_POOL16_DEFAULT_LEASE   OID_DHCP_POOL_LEASE_DEFAULT(15)
#define DHCP_POOL16_MAX_LEASE       OID_DHCP_POOL_LEASE_MAX(15)
#define DHCP_POOL16_RANGE_END       OID_DHCP_POOL_RANGE_END(15)
#define DHCP_POOL16_RANGE_START     OID_DHCP_POOL_RANGE_START(15)
#define DHCP_POOL16_GATEWAY         OID_DHCP_POOL_GATEWAY(15)
#define DHCP_POOL16_MASK            OID_DHCP_POOL_MASK(15)
#define DHCP_POOL16_DNS_DOMAIN      OID_DHCP_POOL_DNS_DOMAIN(15)
#define DHCP_POOL16_DNS_SERVER1     OID_DHCP_POOL_DNS_SERVER1(15)
#define DHCP_POOL16_DNS_SERVER2     OID_DHCP_POOL_DNS_SERVER2(15)
#define DHCP_POOL16_DNS_SERVER3     OID_DHCP_POOL_DNS_SERVER3(15)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define SSID1_DHCP_POOL             OID_SSID_DHCP_POOL(0)
#define SSID2_DHCP_POOL             OID_SSID_DHCP_POOL(1)
#define SSID3_DHCP_POOL             OID_SSID_DHCP_POOL(2)
#define SSID4_DHCP_POOL             OID_SSID_DHCP_POOL(3)
#define SSID5_DHCP_POOL             OID_SSID_DHCP_POOL(4)
#define SSID6_DHCP_POOL             OID_SSID_DHCP_POOL(5)
#define SSID7_DHCP_POOL             OID_SSID_DHCP_POOL(6)
#define SSID8_DHCP_POOL             OID_SSID_DHCP_POOL(7)
#define SSID9_DHCP_POOL             OID_SSID_DHCP_POOL(8)
#define SSID10_DHCP_POOL            OID_SSID_DHCP_POOL(9)
#define SSID11_DHCP_POOL            OID_SSID_DHCP_POOL(10)
#define SSID12_DHCP_POOL            OID_SSID_DHCP_POOL(11)
#define SSID13_DHCP_POOL            OID_SSID_DHCP_POOL(12)
#define SSID14_DHCP_POOL            OID_SSID_DHCP_POOL(13)
#define SSID15_DHCP_POOL            OID_SSID_DHCP_POOL(14)
#define SSID16_DHCP_POOL            OID_SSID_DHCP_POOL(15)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define DHCP_POOL1_NAME             OID_DHCP_POOL_NAME(0)
#define DHCP_POOL2_NAME             OID_DHCP_POOL_NAME(1)
#define DHCP_POOL3_NAME             OID_DHCP_POOL_NAME(2)
#define DHCP_POOL4_NAME             OID_DHCP_POOL_NAME(3)
#define DHCP_POOL5_NAME             OID_DHCP_POOL_NAME(4)
#define DHCP_POOL6_NAME             OID_DHCP_POOL_NAME(5)
#define DHCP_POOL7_NAME             OID_DHCP_POOL_NAME(6)
#define DHCP_POOL8_NAME             OID_DHCP_POOL_NAME(7)
#define DHCP_POOL9_NAME             OID_DHCP_POOL_NAME(8)
#define DHCP_POOL10_NAME            OID_DHCP_POOL_NAME(9)
#define DHCP_POOL11_NAME            OID_DHCP_POOL_NAME(10)
#define DHCP_POOL12_NAME            OID_DHCP_POOL_NAME(11)
#define DHCP_POOL13_NAME            OID_DHCP_POOL_NAME(12)
#define DHCP_POOL14_NAME            OID_DHCP_POOL_NAME(13)
#define DHCP_POOL15_NAME            OID_DHCP_POOL_NAME(14)
#define DHCP_POOL16_NAME            OID_DHCP_POOL_NAME(15)
//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define WDS_LINK1_ENABLE            OID_WDS_LINK_ENABLED(0)
#define WDS_LINK1_MAX_IAP           OID_WDS_LINK_MAX_IAP(0)
#define WDS_LINK1_TARGET            OID_WDS_LINK_TARGET(0)
#define WDS_LINK1_SSID              OID_WDS_LINK_SSID(0)
#define WDS_LINK1_USERNAME          OID_WDS_LINK_USERNAME(0)
#define WDS_LINK1_PASSWORD          OID_WDS_LINK_PASSWORD(0)
#define WDS_LINK1_SPARE1            OID_WDS_LINK_SPARE1(0)
#define WDS_LINK1_SPARE2            OID_WDS_LINK_SPARE2(0)

#define WDS_LINK2_ENABLE            OID_WDS_LINK_ENABLED(1)
#define WDS_LINK2_MAX_IAP           OID_WDS_LINK_MAX_IAP(1)
#define WDS_LINK2_TARGET            OID_WDS_LINK_TARGET(1)
#define WDS_LINK2_SSID              OID_WDS_LINK_SSID(1)
#define WDS_LINK2_USERNAME          OID_WDS_LINK_USERNAME(1)
#define WDS_LINK2_PASSWORD          OID_WDS_LINK_PASSWORD(1)
#define WDS_LINK2_SPARE1            OID_WDS_LINK_SPARE1(1)
#define WDS_LINK2_SPARE2            OID_WDS_LINK_SPARE2(1)

#define WDS_LINK3_ENABLE            OID_WDS_LINK_ENABLED(2)
#define WDS_LINK3_MAX_IAP           OID_WDS_LINK_MAX_IAP(2)
#define WDS_LINK3_TARGET            OID_WDS_LINK_TARGET(2)
#define WDS_LINK3_SSID              OID_WDS_LINK_SSID(2)
#define WDS_LINK3_USERNAME          OID_WDS_LINK_USERNAME(2)
#define WDS_LINK3_PASSWORD          OID_WDS_LINK_PASSWORD(2)
#define WDS_LINK3_SPARE1            OID_WDS_LINK_SPARE1(2)
#define WDS_LINK3_SPARE2            OID_WDS_LINK_SPARE2(2)

#define WDS_LINK4_ENABLE            OID_WDS_LINK_ENABLED(3)
#define WDS_LINK4_MAX_IAP           OID_WDS_LINK_MAX_IAP(3)
#define WDS_LINK4_TARGET            OID_WDS_LINK_TARGET(3)
#define WDS_LINK4_SSID              OID_WDS_LINK_SSID(3)
#define WDS_LINK4_USERNAME          OID_WDS_LINK_USERNAME(3)
#define WDS_LINK4_PASSWORD          OID_WDS_LINK_PASSWORD(3)
#define WDS_LINK4_SPARE1            OID_WDS_LINK_SPARE1(3)
#define WDS_LINK4_SPARE2            OID_WDS_LINK_SPARE2(3)

#define ACL_ENTRY                   OID_ACL_ENTRY
#define ACL_SIZE                    OID_ACL_SIZE              // cfg
#define RADIUS_USER                 OID_RADIUS_USER
#define RADIUS_USER_NUM             OID_RADIUS_USER_NUM       // cfg
#define SAVE_FLAG                   OID_SAVE_FLAG             // cfg
#define AUTO_CH_STATUS              OID_AUTO_CH_STATUS
#define NUM_LOGS                    OID_NUM_LOGS              // cfg
#define NUM_RADIOS                  OID_NUM_RADIOS
#define RESET_REASON                OID_RESET_REASON
#define RADAR_TEST_MODE             OID_RADAR_TEST_MODE
#define FILTER_ENTRY                OID_FILTER_ENTRY
#define RADIUS_USER_VAR_SIZE        OID_RADIUS_USER_VAR_SIZE  // cfg
#define RADIUS_USER_INDEX           OID_RADIUS_USER_INDEX     // cfg
#define ADMIN_ID_INDEX              OID_ADMIN_ENTRY_INDEX     // cfg

#define OLD_ACL_ENTRY               OID_OLD_ACL_ENTRY    // old obj id for acl_entry   (stored)
#define OLD_RADIUS_USER             OID_OLD_RADIUS_USER  // old obj id for radius_user (stored)

#define NUM_SECURITY_OBJECTS        NUM_SSID_SECURITY_OIDS
#define NUM_RADIUS_OBJECTS          NUM_SSID_RADIUS_OIDS
#undef  NUM_DHCP_POOL_OBJS          // Old file used OBJS in place of OIDS
#define NUM_DHCP_POOL_OBJS          NUM_DHCP_POOL_OIDS
#define NUM_ADMIN_OBJECTS           NUM_ADMIN_OIDS
#define NUM_SSID_OBJECTS            NUM_SSID_BASIC_OIDS
#define NUM_RADIO_OBJECTS           NUM_RADIO_OIDS

//---------------------------------------------------------
// cmd_read_struct definitions
//---------------------------------------------------------
#define ETH_STATS                   SID_ETH_STATS
#define RADIO_STATS                 SID_RADIO_STATS
#define SUMMARY                     SID_SUMMARY
#define STA_TABLE                   SID_STA_TABLE
#define NUM_STATIONS                SID_NUM_STATIONS
#define CHANNELS                    SID_CHANNELS
#define LOG                         SID_LOG
#define DRIVER_VERSION              SID_DRIVER_VERSION
#define STA_STATS                   SID_STA_STATS
#define GEOGRAPHY_INFO              SID_GEOGRAPHY_INFO

//---------------------------------------------------------
// cmd_execute definitions
//---------------------------------------------------------
#define RESET_ETH                   EID_RESET_ETH
#define CLEAR_ETH_STATS             EID_CLEAR_ETH_STATS
#define CLEAR_RADIO_STATS           EID_CLEAR_RADIO_STATS
#define CHECK_USER                  EID_CHECK_USER
#define SHOW_CUR_CFG                EID_SHOW_CUR_CFG
#define SAVE_CUR_CFG                EID_SAVE_CUR_CFG
#define REBOOT                      EID_REBOOT
#define RESET_CFG                   EID_RESET_CFG
#define ENABLE_ALL_RADIOS           EID_ENABLE_ALL_RADIOS
#define DISABLE_ALL_RADIOS          EID_DISABLE_ALL_RADIOS
#define ENABLE_AUTO_CH              EID_ENABLE_AUTO_CH
#define DEAUTH_STA                  EID_DEAUTH_STA
#define CLEAR_LOG                   EID_CLEAR_LOG
#define SAVE_IMAGE                  EID_SAVE_IMAGE
#define SHOW_CFG_DIFF               EID_SHOW_CFG_DIFF
#define CLEAR_STA_STATS             EID_CLEAR_STA_STATS
#define SET_DATE                    EID_SET_DATE
#define IMPORT_CFG                  EID_IMPORT_CFG
#define RESET_CONSOLE               EID_RESET_CONSOLE
#define DENY_STA                    EID_DENY_STA
#define LOGOUT_USER                 EID_LOGOUT_USER
#define MOVE_FILTER_UP              EID_MOVE_FILTER_UP
#define MOVE_FILTER_DOWN            EID_MOVE_FILTER_DOWN
#define RESET_WDS                   EID_RESET_WDS_CLIENTS

#define FILTER_NAME_LEN             REQ_LEN_FILTER_NAME
#define DHCP_POOL_NAME_LEN          REQ_LEN_DHCP_POOL_NAME
#define MAXSSIDLEN                  REQ_LEN_SSID
#define STRLEN                      REQ_LEN_STR
#define RAD_SECRET_LEN              REQ_LEN_RAD_SECRET
#define MAC_ADDR_STR_LEN            REQ_LEN_MAC_ADDR_STR
#define MAC_ADDR_SHORT_STR_LEN      REQ_LEN_MAC_ADDR_SHORT_STR
#define MAX_WEP_KEY_LEN             REQ_LEN_WEP_KEY
#define MIN_WPA_PASSPHRASE_LEN      REQ_LEN_MIN_WPA_PASSPHRASE
#define MAX_WPA_PASSPHRASE_LEN      REQ_LEN_WPA_PASSPHRASE
#define DRIVER_VERSION_LEN          REQ_LEN_DRIVER_VERSION
#define COUNTRY_CODE_LEN            REQ_LEN_COUNTRY_STR

//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define DHCP_GATEWAY                DHCP_POOL1_GATEWAY
#define DHCP_MASK                   DHCP_POOL1_MASK
#define DHCP_POOL_NUM_OBJECTS       NUM_DHCP_POOL_OBJS

#define NO_ENCRYPTION               ENC_NONE
#define WEP_ENCRYPTION              ENC_WEP
#define WPA_ENCRYPTION              ENC_WPA
#define WPA2_ENCRYPTION             ENC_WPA2
#define WPA_BOTH_ENCRYPTION         ENC_WPA_BOTH

#define NO_LIST                     ACL_LIST_NO
#define ALLOW_LIST                  ACL_LIST_ALLOW
#define DENY_LIST                   ACL_LIST_DENY

#define REMOTE_RAD_SERVER           RAD_SERVER_REMOTE
#define LOCAL_RAD_SERVER            RAD_SERVER_LOCAL

#endif // _XLATOID_H
