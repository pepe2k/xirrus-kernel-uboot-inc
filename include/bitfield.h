//******************************************************************************
// bitfield macros
//------------------------------------------------------------------------------
#ifndef _BITFIELD_OPS
#define _BITFIELD_OPS

// ---- 32 bit ops -------------------------------------------------------------
#define bit_const(b)                (1 << (b))

#define get_field(b1,b2,n)          (((n) >> (b2)) & ((1 << ((b1)-(b2)+1))-1))
#define set_field(b1,b2,n)          (((n) & ((1 << ((b1)-(b2)+1))-1)) << (b2))

#define field_mask(b1,b2)           (set_field(b1,b2,-1))
#define update_field(b1,b2,m,n)     (set_field(b1,b2, n) | (m & ~field_mask(b1,b2)))

#define get_nibble(w,n)             (((w) >> (n)*4) & 0xf)

// ---- 64 bit ops -------------------------------------------------------------
#define bit_const_64(b)             (1LL << (b))

#define get_field_64(b1,b2,n)       (((n) >> (b2)) & ((1LL << ((b1)-(b2)+1))-1))
#define set_field_64(b1,b2,n)       (((n) & ((1LL << ((b1)-(b2)+1))-1)) << (b2))

#define field_mask_64(b1,b2)        (set_field_64(b1,b2,-1LL))
#define update_field_64(b1,b2,m,n)  (set_field_64(b1,b2, n) | (m & ~field_mask_64(b1,b2)))

#define get_nibble_64(w,n)          (((w) >> (n)*4LL) & 0xfLL)

#endif

