//******************************************************************************
/** @file xirdhcp.h
 *
 * Structures and constants for the dhcp request interface
 *
 * <I>Copyright (C) 2006 Xirrus. All Rights Reserved</I>
 *
 * This file contains constants and structures that are used between the
 * configuration module and the dhcpd module
 *
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRDHCP_H
#define _XIRDHCP_H

#include "common.h"
#include "xircfg.h"
#include "cfgdefs.h"

#endif // _XIRDHCP_H

