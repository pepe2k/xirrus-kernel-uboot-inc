//* xirmac.h - device management functions for xirrus mac
/**
 * @file xirmac.h
 * @brief device management functions for mac hw shim
 * @remarks the mac hw shim presents a character-device-ish interface to the umac and lmac functions.  the
 * device driver manages 16 virtual radio devices, which are selected via the upper four bits of the device
 * minor number. each of these virtual radio devices further encompasses several functional sub-
 * devices. since there are several types of messages that are passed using file read and write functions, the
 * message types are differntiated via the functional sub-devices. the lower four bits of the device minor
 * number is used to identify the functional sub-device (message type). note that the lower four bits of the
 * minor device number are only used in conjuction with the file read and write functions, not ioctls.
 *
 * NOTE WELL: This file is now shared between many projects - including the driver and apps. As such,
 * be careful about what you add in here or some of them may no longer compile. In particular, try to
 * keep include files to a minimum. The only declarations here should be those that pertain to the
 * IOCTL interface between the driver and userland apps. (All the declarations should be here BTW
 * otherwise we end up duplicating declarations upstairs).
 */
#ifndef XIRMAC_H
#define XIRMAC_H

#ifndef _XIRLLDP_H
#ifdef __KERNEL__
#include <linux/netdevice.h>
#else
#include <net/ethernet.h>
#endif

#ifdef DARWIN
#include <arpa/inet.h>
#define __be32_to_cpu   ntohl
#define __le32_to_cpu   // don't use/don't care when compiling for os x
#define ETH_ALEN        6
#endif

#include "xirendian.h"
#endif /* not defined _XIRLLDP_H */

#include "xirver.h"
#include "xirstats.h"

//****************************************************************
// Some necessary motherhood
//----------------------------------------------------------------
#if defined(UNUSED)
  /* nothing */
#elif defined(__GNUC__)
#  define UNUSED(x) UNUSED_ ## x __attribute__((__unused__))
#  define UNUSED_FUNCTION(x) __attribute__((__unused__)) UNUSED_ ## x
#else
#  define UNUSED(x) UNUSED_ ## x
#  define UNUSED_FUNCTION(x) UNUSED_ ## x
#endif

//****************************************************************
// Global maximum definitions
//----------------------------------------------------------------
#define MAX_NUM_VLAN                   64
#define MAX_NUM_VLAN_POOL              16
#define MAX_NUM_SSID                   16
#define MAX_NUM_GROUP                  16
#define XIR_VLAN_TABLE_NUM             (MAX_NUM_VLAN + MAX_NUM_SSID + MAX_NUM_GROUP)

#define MAX_NUM_MULTICAST_EXCLUDE      16
#define MAX_NUM_MULTICAST_FORWARD      16
#define MAX_NUM_MULTICAST_FWD_VLAN     MAX_NUM_VLAN
#define MAX_NUM_MDNS_FILTER            16
#define REQ_LEN_MDNS_FILTER            24+1
#define MAX_NUM_DSCP_VALUES            64
#define DEFAULT_DSCP_MAP               { 0, 0, 0, 0, 0, 0, 0, 0, \
                                         1, 0, 0, 0, 0, 0, 0, 0, \
                                         0, 0, 0, 0, 0, 0, 0, 0, \
                                         2, 0, 2, 0, 0, 0, 0, 0, \
                                         0, 0, 0, 0, 0, 0, 0, 0, \
                                         2, 0, 0, 0, 0, 0, 3, 0, \
                                         3, 0, 0, 0, 0, 0, 0, 0, \
                                         0, 0, 0, 0, 0, 0, 0, 0 }

#define MAX_SIZE_HONEYPOT_BCAST        16           // SSID Honeypot broadcast list
#define MAX_SIZE_HONEYPOT_WLIST        150          // SSID Honeypot whitelist
#define MAX_SIZE_WLIST                 128          // SSID WPR whitelist
#define MAX_LEN_HOSTNAME_STR           (256+1)
#define MAX_LEN_REG_DUMP               60           // REGISTER_DUMP_LEN_MAX defined as 60 in madwifi-mips/ol_driver/perf_pwr_offload/drivers/host/wlan/lmac_offload_if/ol_if_ath.c

//****************************************************************
// Xirrus/Avaya OUI definitions
//----------------------------------------------------------------
#define  AVAYA_OUI_STR_1               "64:a7:dd"
#define  AVAYA_OUI_U32_1               0x64a7dd00

#define  CAMBIUM_OUI_STR_1             "00:04:56"
#define  CAMBIUM_OUI_STR_2             "58:C1:7A"
#define  CAMBIUM_OUI_STR_3             "BC:E6:7C"
#define  CAMBIUM_OUI_STR_4	       "BC:A9:93"
#define  CAMBIUM_OUI_U32_1             0x00045600
#define  CAMBIUM_OUI_U32_2             0x58C17A00
#define  CAMBIUM_OUI_U32_3             0xBCE67C00
#define  CAMBIUM_OUI_U32_4	       0xBCA99300

#define XIRRUS_OUI_STR_1               "00:0f:7d"
#define XIRRUS_OUI_STR_2               "50:60:28"
#define XIRRUS_OUI_STR_3               "48:c0:93"
#define XIRRUS_OUI_U32_1               0x000f7d00
#define XIRRUS_OUI_U32_2               0x50602800
#define XIRRUS_OUI_U32_3               0x48c09300

#define is_xirrus_macaddr(addr)        ({const u32 oui=__be32_to_cpu(((const u32 *)addr)[0]) & 0xffffff00; \
                                         (oui == AVAYA_OUI_U32_1 || oui == XIRRUS_OUI_U32_1  || oui == XIRRUS_OUI_U32_2  || oui == XIRRUS_OUI_U32_3  \
	                                                         || oui == CAMBIUM_OUI_U32_1 || oui == CAMBIUM_OUI_U32_2 || oui == CAMBIUM_OUI_U32_3 \
					  ) ? 1 : 0;})

#define is_xirrus_station(key)         ({const u32 oui=__le32_to_cpu(((const u32 *)&key)[1]) & 0xffffff00; \
                                         (oui == AVAYA_OUI_U32_1 || oui == XIRRUS_OUI_U32_1  || oui == XIRRUS_OUI_U32_2  || oui == XIRRUS_OUI_U32_3  \
					                         || oui == CAMBIUM_OUI_U32_1 || oui == CAMBIUM_OUI_U32_2 || oui == CAMBIUM_OUI_U32_3 \
					  ) ? 1 : 0;})

#define is_xirrus_oui_str(str)         (strncasecmp(str,  AVAYA_OUI_STR_1,  REQ_LEN_MAC_ADDR_STR/2) == 0 || \
                                        strncasecmp(str, XIRRUS_OUI_STR_1,  REQ_LEN_MAC_ADDR_STR/2) == 0 || \
                                        strncasecmp(str, XIRRUS_OUI_STR_2,  REQ_LEN_MAC_ADDR_STR/2) == 0 || \
                                        strncasecmp(str, XIRRUS_OUI_STR_3,  REQ_LEN_MAC_ADDR_STR/2) == 0 || \
                                        strncasecmp(str, CAMBIUM_OUI_STR_1, REQ_LEN_MAC_ADDR_STR/2) == 0 || \
                                        strncasecmp(str, CAMBIUM_OUI_STR_2, REQ_LEN_MAC_ADDR_STR/2) == 0 || \
                                        strncasecmp(str, CAMBIUM_OUI_STR_3, REQ_LEN_MAC_ADDR_STR/2) == 0 )

#define is_xirrus_only_macaddr(addr)   ({const u32 oui=__be32_to_cpu(((const u32 *)addr)[0]) & 0xffffff00; (oui == XIRRUS_OUI_U32_1 || oui == XIRRUS_OUI_U32_2 || oui == XIRRUS_OUI_U32_3) ? 1 : 0;})
#define is_xirrus_only_station(key)    ({const u32 oui=__le32_to_cpu(((const u32 *)&key)[1]) & 0xffffff00; (oui == XIRRUS_OUI_U32_1 || oui == XIRRUS_OUI_U32_2 || oui == XIRRUS_OUI_U32_3) ? 1 : 0;})
#define is_xirrus_only_oui_str(str)    (strncasecmp(str, XIRRUS_OUI_STR_1, REQ_LEN_MAC_ADDR_STR/2) == 0 || \
                                        strncasecmp(str, XIRRUS_OUI_STR_2, REQ_LEN_MAC_ADDR_STR/2) == 0 || \
                                        strncasecmp(str, XIRRUS_OUI_STR_3, REQ_LEN_MAC_ADDR_STR/2) == 0    )

#define is_avaya_macaddr(addr)         ({const u32 oui=__be32_to_cpu(((const u32 *)addr)[0]) & 0xffffff00; (oui == AVAYA_OUI_U32_1) ? 1 : 0;})
#define is_avaya_station(key)          ({const u32 oui=__le32_to_cpu(((const u32 *)&key)[1]) & 0xffffff00; (oui == AVAYA_OUI_U32_1) ? 1 : 0;})
#define is_avaya_oui_str(str)          (strncasecmp(str,  AVAYA_OUI_STR_1, REQ_LEN_MAC_ADDR_STR/2) == 0)

//****************************************************************
// Xirrus/Avaya IPv4 definitions
//----------------------------------------------------------------
#define XIRRUS_DEF_IPV4_STR             "10.0.2.1"
#define XIRRUS_DEF_IPV4_U32             0x0a000201
#define  AVAYA_DEF_IPV4_STR             "192.168.1.3"
#define  AVAYA_DEF_IPV4_U32             0xc0a80103

//****************************************************************
// Randomized MAC Address definitions
//----------------------------------------------------------------
#define is_random_macstr(m)             ({ char c = tolower((m)[1]); (c == '2' || c == '6' || c == 'a' || c == 'e'); })
#define is_random_macaddr(m)            ((m)[0] & 0x02)

//* tx frame functional sub-device
/**
 * @brief message type for transmitting frames
 * @remarks each message of this type is interrpreted as frame to be sent out on the wireless medium.
 * each transmitted frame should be prefixed with a tx_frame_info struct, which looks like this:
 * ---------------------------------------
 * | ssid   |  radio | rate    | reserved |
 * ---------------------------------------
 * | 1 byte | 1 byte |  1 byte | 1 byte   |
 * ----------------------------------------
 * each frame includes an 802.11 header.
 * @note reading from this fsd is not supported.
 */
#define TX_FSD    0x00;
#define AM_TX_FSD 0x04;

//* struct apme_tx_frame_info
/**
 * @brief extra info that must prefix each frame written via the MGMT_TX_FSD
 */
struct apme_tx_frame_info {
  s8 ssid;                                           //only applies to beacons
  s8 radio;
  u8 rate;
  u8 frame_id;                                       //if nz, then status will be returned in a struct apme_tx_status_info
} __attribute__ ((packed));                          //followed by frame data; max 1500 bytes


//* tx status functional sub-device
/**
 * @brief message type for getting transmit status
 * @remarks reading from this functional sub-device return a single tx_status struct, or nothing.
 * @note writing to this fsd is not suportted.
 */
#define TX_STATUS_FSD    0x01;
#define AM_TX_STATUS_FSD 0x06;


//* struct apme_tx_status_info
/**
 * @brief struct that can be read via the MGMT_TX_FSD
 */
struct apme_tx_status_info {
  u8 frame_id;                                       //frame id from apme_tx_frame_info
  s8 radio;
  u16 status;                                        //if (status & XMAC_TX_STATUS_FATAL_ERRORS) == 0, then tx was ok
#define XMAC_TX_STATUS_FATAL_ERRORS     0xff00       //convenience mask; fatal errors set bits in this mask
                                                     // (Note this must match FLOYD_TX_STATUS_FATAL_ERRORS in the driver)
} __attribute__ ((packed));


//* rx frames functional sub-device
/**
 * @brief message type for reading incoming frames
 * @remarks reading from this functional sub-device yeilds an incoming frame.
 * @note each 802.11 frame is prefixed with an rx_frame_info struct
 * @note it is possible to read less than an entire frame; for example it is possible to only read
 * the rx_frame_info struct. but on any one read it is not possible to read beyond the a frame boundary.
 * at any rate, it is not posible to read any data at all until an entire frame is available.
 * @note writing to this fsd is not suportted.
 */
#define RX_FSD    0x02;
#define AM_RX_FSD 0x05;


//* struc apme_rx_frame_info
/**
 * @brief extra info that prefixes each received frame read via the MGMT_RX_FCN device
 */
struct apme_rx_frame_info {
  u8 flags;
  u8 device;
  u8 channel;
  u8 rate;
  u16 length;
  s8 signal_strength;
  u8 signal_to_noise;
  u8 silence;
  u8 status;
  u16 message_id;                                                  //select particular mesage from union mac_to_xap_msg, basesd on this id
  u16 timestamp;
} __attribute__ ((packed));                                        //followed by frame data; max 1500 bytes
#define APME_RX_FLAGS_HAVE_RATE             0x01                   //rate is valid
#define APME_RX_FLAGS_HAVE_SIGNAL           0x02                   //signal level is valid
#define APME_RX_FLAGS_HAVE_SILENCE          0x04                   //silence level is valid
#define APME_RX_FLAGS_HAVE_SNR              0x08                   //signal to noise ratio is valid
#define APME_RX_FLAGS_HAVE_CHANNEL          0x10                   //channel is valid
#define APME_RX_FLAGS_XAPI_PKT              0x40                   //this is a xapi packet, read via the xapi packet queue
#define APME_RX_FLAGS_MAC_TO_XAPME_MSG      0x80                   //data is a union mac_to_xap_msg, check message_id
#define APME_RX_STATUS_OK                   0x00
#define APME_RX_STATUS_BAD_CRC              0x01

// The following defines for APME_RX_MSG_ID_ must match MSG_TYPE in drvMsg.h
#define APME_RX_MSG_ID_RX_PACKET            0x0000                 //data is a received packet, not a union mac_to_xap_msg
#define APME_RX_MSG_ID_UNASSOC_MSG          0x0001                 //mac_to_xap_msg is a struct rx_from_unassicated_sta_msg
#define APME_RX_MSG_ID_DECRYPT_MSG          0x0002                 //mac_to_xap_msg is a struct rx_decryption_err_msg
#define APME_RX_MSG_ID_PRISM_MODE           0x0003
#define APME_RX_MSG_ID_RADAR_DETECTED       0x0004                 //mac_to_xap_msg is a struct radar_detected_msg
#define APME_RX_MSG_ID_UNENCRYPTED          0x0005                 //mac_to_xap_msg is a struct rx_decryption_err_msg
#define APME_RX_MSG_ID_RATE_CHG_MSG         0x0006                 //mac_to_xap_msg is a struct rate_change_msg
#define APME_RX_MSG_ID_AS_TAG_MSG           0x0007                 //AeroScout Tag is forwarded up to xapme
#define APME_RX_MSG_ID_WDS_LINK_ACTIVE      0x0008                 //mac_to_xap_msg is a struct rx_from_wds_partner_msg
#define APME_RX_MSG_ID_CHANNEL_SWITCH       0x0009
#define APME_RX_MSG_ID_GOT_IPV4_ADDR        0x000a                 //mac_to_xap_msg is a struct got_ipv4_addr_msg
#define APME_RX_MSG_ID_GOT_IPV6_ADDR        0x000b                 //mac_to_xap_msg is a struct got_ipv6_addr_msg
#define APME_RX_MSG_ID_GOT_HOSTNAME         0x000c                 //mac_to_xap_msg is a struct got_hostname_msg
#define APME_RX_MSG_ID_GOT_NETBIOS_NAME     0x000d                 //mac_to_xap_msg is a struct got_netbios_msg
#define APME_RX_MSG_ID_GOT_USER_AGENT       0x000e                 //mac_to_xap_msg is a struct got_user_agent_msg
#define APME_RX_MSG_ID_IDS_EVENT            0x000f
#define APME_RX_MSG_ID_EKA_TAG_MSG          0x0010                 //Ekahau Tag is forwarded up to xapme
#define APME_RX_MSG_ID_GOT_URLS             0x0011                 //mac_to_xap_msg is a struct got_urls_msg
#define APME_RX_MSG_ID_STATUS               0x0012                 //mac_to_xap_msg is a struct drv_status_msg
#define APME_RX_MSG_ID_STA_DEAUTH           0x0013                 //mac_to_xap_msg is a struct sta_deauth_msg



//* message structure for messages passed to xap-me via the rx fsd
/*
 * @remarks this is the message structure passed in the skb, when the flags member of the apme_rx_frame_info indicates
 * a mac-to-xap message.
 */
union mac_to_xap_msg {
  struct rx_from_unassicated_sta_msg {                         //an rx packet was received from an unassociated sta
    u8 addr[6];                                                //mac addr of sta
    u8 ssid;                                                   //ssid on which this guy was trying to pass data
  } __attribute__ ((packed)) unassoc_msg;
  struct rx_decryption_err_msg {                               //an mic err from an associated sta
    u8 addr[6];                                                //mac addr of sta
    u16 aid;                                                   //aid of sta
    u16 err_type;                                              //mic err, icv err; see defines
  } __attribute__ ((packed)) decrypt_msg;
  struct get_into_prism_mode_msg {                             //force barker preamble mode on; also disable cck rate of 2 mbps
    u16 on_or_off;                                             //nz => prism mode on; z => prism mode off
  } __attribute__ ((packed)) prism_mode_msg;
  struct radar_detected_msg {                                  //radar has been detected
    u16 radar_id;                                              //0 = 330pps; 1 = 700pps; 2 = 1800pps; 3 = 714pps; 0xff = don't know
  } __attribute__ ((packed)) radar_msg;
  struct rate_change_msg {                                     //rate has changed
    u8 addr[6];                                                //mac addr of sta
    u16 rate;                                                  //new rate
  } __attribute__ ((packed)) rate_msg;
  struct rx_from_wds_partner_msg {                             //a null data packet was received from WDS partner
    u8 addr[6];                                                //mac addr of WDS partner
  } __attribute__ ((packed)) wds_msg;
  struct got_ipv4_addr_msg {                                   //we snooped this guys ipv4 addr
    u8 addr[6];                                                //mac addr of sta
    u32 ipv4_addr;                                             //ipv4 addr
  } __attribute__ ((packed)) ipv4_msg;
  struct got_ipv6_addr_msg {                                   //we snooped this guys ipv6 addr
    u8 addr[6];                                                //mac addr of sta
    u128 ipv6_addr;                                            //ipv6 addr
  } __attribute__ ((packed)) ipv6_msg;
  struct got_hostname_msg {                                    //we snooped this guys hostname
    u8 addr[6];                                                //mac addr of sta
    u8 hostname[256];                                          //hostname
  } __attribute__ ((packed)) hostname_msg;
  struct got_netbios_name_msg {                                //we snooped this guys netbios name
    u8 addr[6];                                                //mac addr of sta
    u8 netbios_name[16 + 1];                                   //netbios name
  } __attribute__ ((packed)) netbios_name_msg;
  struct got_user_agent_msg {                                  //we snooped this guys user-agent
    u8 addr[6];                                                //mac addr of sta
    u8 user_agent[256];                                        //user-agent string
  } __attribute__ ((packed)) user_agent_msg;
  struct ids_event_msg {                                       //ids event has been detected
    u16 event;
    u32 period;
    u32 cur_pkts;
    u32 avg_pkts;
    u32 max_pkts;
    u8 addr[6];
  } __attribute__ ((packed)) ids_msg;
  struct got_urls_msg {                                        //we snooped a bunch of urls
    u16 no_urls;                                               //how many urls are in this msg
    struct got_urls_msg_url_record {
      u8 mac_addr[6];                                          //mac addr of sta requesting urls
      u32 sip;                                                 //src port. be
      u32 dip;                                                 //dest ip. be
      u16 dport;                                               //dest port. be
      u8 len;                                                  //the length of this url
      char url[1];                                             //null terminated url. length is specified above
    } __attribute__ ((packed)) records[1];                     //there actually may be many of these...
  } __attribute__ ((packed)) urls_msg;
  struct drv_status_msg {                                      //driver status msg
    u16 status;
    u32 reg_dump_area;                                         //peregrine only target assert dump
    u32 reg_dump_values[MAX_LEN_REG_DUMP];                     //peregrine only target assert dump
  } __attribute__ ((packed)) status_msg;
  struct sta_deauth_msg {                                      //sta deauth requested
    u8 addr[6];                                                //mac addr of sta
    u8 ssid;                                                   //ssid on which this sta was associated
    u8 reason;                                                 //reason for deauth
  } __attribute__ ((packed)) deauth_msg;
};
#define DECRYPT_MSG_NO_ERR              0x0000                  //should never happen
#define DECRYPT_MSG_MIC_ERR             0x0001
#define DECRYPT_MSG_ICV_ERR             0x0002
#define DECRYPT_MSG_UNENCRYPTED         0x0003                  //received an unencrypted packet from an encrypted station

#define STATUS_MSG_INIT_DONE            0x0000                  //driver initialization complete (obsolete)
#define STATUS_MSG_RELOAD_REQ           0x0001                  //driver requests reload (obsolete)
#define STATUS_MSG_TARGET_ASSERT_REBOOT 0x0002                  //peregrine only target assert (followed by system reboot)
#define STATUS_MSG_TARGET_ASSERT_RESET  0x0003                  //peregrine only target assert (followed by target reset)
#define STATUS_MSG_TARGET_COOKIE_REBOOT 0x0004                  //peregrine only target out of cookie condition (followed by system reboot)
#define STATUS_MSG_TARGET_COOKIE_RESET  0x0005                  //peregrine only target out of cookie condition (followed by system reset)
#define STATUS_MSG_TARGET_RESET_SUCCESS 0x0006                  //peregrine only target reset successfull
#define STATUS_MSG_TARGET_RESET_FAIL    0x0007                  //peregrine only target reset failed (followed by system reboot)
#define STATUS_MSG_DEAD_RADIO_DETECTED  0x0008                  //peregrine only detected a dead radio (followed by radio down/up)
#define STATUS_MSG_TARGET_TXDESC_RESET  0x0009                  //peregrine only target out of tx descriptor condition (followed by system reset)
#define STATUS_MSG_TARGET_TXDESC_REBOOT 0x000a                  //peregrine only target out of tx descriptor condition (followed by system reboot)
#define STATUS_MSG_BAD_RADIO_DETECTED   0x000b                  //notification from driver on a potential bad radio

//* ioctl magic number and function assignmnets
/*
 * @brief
 * @remarks all commands use the ioctl_cmd structure; and when status is returned it is returned
 * within the data area of that structure.
 * @note all ioctl commands exchange data via a pointer, even when that data is just a single word.
 */
#define IOCTL_MAGIC                              'I'    //'I' is assigned to isdn devices, but that's close...
#define IOCTL_CMD_NULL                            0     //null cmd
#define IOCTL_CMD_GET_VERSION                     1     //return version info
#define IOCTL_CMD_GET_ROM_ADDR                    2     //return mac addr from rom
#define IOCTL_CMD_ENABLE_RADIO                    3     //flag the radio as enabled; still requires start cmd
#define IOCTL_CMD_DISABLE_RADIO                   4     //flag the radio as enabled; stop the radio now.
#define IOCTL_CMD_START                           5     //start all radios that are enabled
#define IOCTL_CMD_STOP                            6     //stop all radios
//#define IOCTL_CMD_BEACONSET                     7     //tbd.  - DEPRECATED
#define IOCTL_CMD_BEACON_ENABLE                   8     //enable/disable beacons for specified radio/ssids; word data is ssid mask
#define IOCTL_CMD_SET_SILENCE_MASK                9     //force silence; for 1 radio word data is flag; for all, word data is radio mask
#define IOCTL_CMD_SET_DTIM_PERIOD                10     //set period (in beacons) between ATIM's
#define IOCTL_CMD_SET_BSSID                      11     //set the mac address - DEPRECATED 12/08/05
#define IOCTL_CMD_GET_BSSID                      12     //get the current mac address - DEPRECATED 12/08/05
#define IOCTL_CMD_SET_CHANNEL                    13     //set the channel; channel is specified data word
#define IOCTL_CMD_GET_CHANNELS                   14     //get the current channels for all radios
#define IOCTL_CMD_GET_RATES                      15     //tbd
#define IOCTL_CMD_SET_TX_POWER                   16     //set transmit power
#define IOCTL_CMD_SET_PREAMBLE                   17     //set cck preamble 1 = short (and long), 0 = long only
#define IOCTL_CMD_SET_RTS_THRESHOLD              18     //set RTS threshold
#define IOCTL_CMD_SET_FRAG_THRESHOLD             19     //set fragmentation threshold
#define IOCTL_CMD_SET_SHORT_RETRY_LIMIT          20     //set short retry limit
#define IOCTL_CMD_SET_LONG_RETRY_LIMIT           21     //set long retry limit
//#define IOCTL_CMD_OLD_GET_STA_INFO             22     //get station info -- DEPRECATED. use IOCTL_CMD_GET_STA_INFO
#define IOCTL_CMD_SET_STA_INFO                   23     //set station info
#define IOCTL_CMD_GET_REBOOT_CAUSE               24     //word data is bitmask. see scd defines
#define IOCTL_CMD_GET_NO_DEVS                    25     //
#define IOCTL_CMD_SET_PIDDLE_DATA                26     //for use during debug / fcc testing
#define IOCTL_CMD_SET_RX_THRESHOLD               27     //set rx threshold
//#define IOCTL_CMD_OLD_GET_STATS                28     //get an all_stats struct for a device, since begin o' time - DEPRECATED
//#define IOCTL_CMD_OLD_DELTA_STATS              29     //get an all_stats struct for a device, since last delta stats - DEPRECATED
#define IOCTL_CMD_SET_WEP_KEYS                   30     //set static wep keys, wep must be enabled though to use static wep for broadcasts
#define IOCTL_CMD_SET_WPA_BROADCAST_KEYS         31     //set group wpa keys, does not imply that wpa broadcast is enabled
#define IOCTL_CMD_SET_ENCRYPTION_TYPES           32     //set encryption type for a particular radio/ssid; data is (u16) enc_type
#define IOCTL_CMD_SET_LED_BLINK_MASK             33     //determines when we blink the activity leds; see netdev.h for defines
#define IOCTL_CMD_SET_RX_MASK                    34     //use with caution: causes floyd to pass up different types of rx frames
#define IOCTL_CMD_SET_ANTENNA                    35     //0 = external, 1 = internal
#define IOCTL_CMD_GET_ANTENNA                    36     //0 = external, 1 = internal
#define IOCTL_CMD_AUTO_ASSIGN_CHANNELS           37     //signed word: -1 => all radios, -2 => all 5 ghz radios, -3 all 2 ghz radios
#define IOCTL_CMD_AUTO_CHANNELS_STATUS           38     //0 = done, x < 0 thinking, x > 0 scanning channel x
#define IOCTL_CMD_DEBUG_CTL                      39     //u16: Control the debugging behaviour of the driver
#define IOCTL_CMD_SET_G_MODE                     40     //word data is mode. see defines below for g-only/mixed-bg modes
#define IOCTL_CMD_SET_RATES                      41     //write data is struct ioctl_set_basic_rates_data
#define IOCTL_CMD_GET_STATION_STATS              42     //get stats for a single station, by mac address
#define IOCTL_CMD_SET_ETH_ALLOW_MGMT             43     //set the ethernet interfaces to allow management traffic
#define IOCTL_CMD_SET_ETH_MODE                   44     //set the mode for the ethernet interfaces
#define IOCTL_CMD_SET_GEOGRAPHY_INFO             45     //set the mode for radar detection, channels, etc
#define IOCTL_CMD_SET_STA_TO_STA_BLOCK           46     //set sta to sta blocking mode, word data: nz => block sta to sta comunications
#define IOCTL_CMD_SET_XAPI_MODE                  47     //word data: nz => rx packets from monitor radio go to air magnet rx device
#define IOCTL_CMD_SET_LOAD_BALANCING             48     //word data:set load balancing across iaps (0=off, 1=on, 2=aggressive)
#define IOCTL_CMD_SET_TX_COORDINATION            49     //word data:set tx coordination for adj channel iaps (0=off, 1=on, 2=cts)
#define IOCTL_CMD_SET_RX_COORDINATION            50     //word data:set rx coordination for adj channel iaps (0=off, 1=on)
#define IOCTL_CMD_SET_TXRX_BALANCING             51     //word data:set tx/rx balancing for adj channel iaps (0=off, 1=on)
#define IOCTL_CMD_SET_POWESAVE_DISABLED          52     //word data: 0 = normal, powersave is enabled, 1 = disable powersave
#define IOCTL_CMD_SET_PIDDLE_RATE                53     //word data is rate for all stations. (if CFG_PIDDLE_RATES is set)
#define IOCTL_CMD_ETH_MAC_CHECK                  54     //check if mac address received on ethernet - returns msw of jiffy age
#define IOCTL_CMD_RESET_PARTIAL_READ             55     //reset any partial read from our read devices so we sync to th the next packet
#define IOCTL_CMD_GET_STATS                      56     //new as of 10/20/2005) get_stats (superceeds old_get_stats)
//#define IOCTL_CMD_DELTA_STATS                  57     //get an all_stats struct for a device, since last delta stats - DEPRECATED
#define IOCTL_CMD_GET_STA_INFO                   58     //new (as of 10/18/2005) get_sta_info
#define IOCTL_CMD_WDS_BIND                       59
#define IOCTL_CMD_WDS_UNBIND                     60
#define IOCTL_CMD_WDS_GET_BIND_INFO              61
#define IOCTL_CMD_WDS_SET_LINK_INFO              62     //write wds_link_t array in autochannel.c to set up for auto channel assignment
#define IOCTL_CMD_WDS_GET_LINK_INFO              63     //read wds_link_t array in autochannel.c to get results of auto channel assignment
#define IOCTL_CMD_WDS_NAME_LINK                  64
#define IOCTL_CMD_SET_SSID_PPS_LIMITS            65     //set total traffic limit and per sta traffic limit for ssid in pps
#define IOCTL_CMD_SET_PAE_SWITCH                 66     //set 802.1X data path switch... for use by authenticator.
#define IOCTL_CMD_LED_CTRL                       67     //external control of the leds (e.g. progress indicators)
#define IOCTL_CMD_SET_COUNTRY_CODE               68     //set country code in atmel SCD
#define IOCTL_CMD_GET_COUNTRY_CODE               69     //get country code from atmel SCD
#define IOCTL_CMD_CLR_COUNTRY_CODE               70     //clear country code in atmel SCD (reset to 0xffff)
#define IOCTL_CMD_SET_SNEAKY_INTERVAL            71     //word data is interval in ms (max=32767). see defines below for special values.
#define IOCTL_CMD_SET_SHARP_CELL                 72     //word data:set sharp cell tx power control (0=off, 1=on)
#define IOCTL_CMD_SET_LATENCY_TEST               73     //set latency injector test value (in steps of 10 usec)
#define IOCTL_CMD_SET_PHONE_LIMIT                74     //set max phones per radio
#define IOCTL_CMD_SET_BROADCAST_OPTIMIZE         75     //set broadcast optimization (0=off, 1=on)
#define IOCTL_CMD_GET_FLOYD_REG                  76     //get floyd register
#define IOCTL_CMD_SET_FLOYD_REG                  77     //set floyd register
#define IOCTL_CMD_SET_OUR_IPS                    78     //set local (our) ip addresses
#define IOCTL_CMD_SET_NATIVE_VLAN                79     //set native vlan
#define IOCTL_CMD_SET_CPU_CLK_SPEED              80     //set cpu clock speed in atmel part
#define IOCTL_CMD_SET_STA_TIMEOUT                81     //inactivity time (in seconds) before clearing all staion info (inc stats)
#define IOCTL_CMD_GET_MAX_TX_POWER               82     //get max tx power used for sounding beacons
#define IOCTL_CMD_GET_FIPS_AES_RESULT            83     //get result of fips power-on aes test (see defines)
#define IOCTL_CMD_GET_VIRT_IFACE                 84     //create virtual iface; returns iface number as word data (ie. 5 => "viap5")
#define IOCTL_CMD_SET_WPR_REDIRECT_IP            85     //tell the driver the ip addr to which all wpr clients are directed. u32.
#define IOCTL_CMD_SET_WDS_LOOPBACK_MODE          86     //set or clear wds loopback mode
#define IOCTL_CMD_SET_WPR_PASSTHRU_IPS           87     //set pass-thru ips, per ssid, data is struct ioctl_set_wpr_passthru_ips
#define IOCTL_CMD_GET_UNASSOC_STA_INFO           88     //get unassociated station information
#define IOCTL_CMD_SET_DOT11N_PARMS               89     //set 802.11n parmeters. word data is per dot11n parmeters mode word defines
#define IOCTL_CMD_GET_DOT11N_CAPS                90     //get 802.11n capabilities. word data is per dot11n capabilities mode word defines
#define IOCTL_CMD_SET_VLAN_TABLE                 91     //set vlan table for gianfar driver nf-mark vlan idx to number lookup
#define IOCTL_CMD_SET_GROUP_PPS_LIMITS           92     //set total traffic limit and per sta traffic limit for group in pps
#define IOCTL_CMD_SET_CHANNEL_LOCK               93     //set autochannel channel locks (word data is radio mask)
#define IOCTL_CMD_RUN_TEST                       94     //word data => test spec
#define IOCTL_CMD_LED_REG_DIRECT                 95     //direct external control of the led register (must set led override first via ioctl 67)
#define IOCTL_CMD_GET_ADJUSTED_TX_POWER          96     //get backed off tx power (used for mfg test)
#define IOCTL_CMD_GET_MEASURED_TX_POWER          97     //get measured tx power (used for mfg test)
#define IOCTL_CMD_RUN_TX_PWR_CORRECTION          98     //run tx power correction (used for mfg test)
#define IOCTL_CMD_SET_ETH_LED_CTRL               99     //enable/disable gigabit ethernet leds
#define IOCTL_CMD_SET_ARP_FILTER                100     //enable/disable arp filter (0=off, 1=pass thru, 2=proxy)
#define IOCTL_CMD_SET_MCS_RATES_BASIC           101     //set 802.11n basic     rates. long long data is mcs mask (bit 0 = mcs0 ... bit 63 = mcs63)
#define IOCTL_CMD_SET_MCS_RATES_SUPP            102     //set 802.11n supported rates. long long data is mcs mask (bit 0 = mcs0 ... bit 63 = mcs63)
#define IOCTL_CMD_SET_RADAR_TEST_MODE           103     //enable/disable radar test mode, 11n driver needs to use this mode for extra noise filtering
#define IOCTL_CMD_SET_I2C_SPEED                 104     //set i2c bus speed (16 bit, KHz)
#define IOCTL_CMD_GET_TKIP_KEY_CACHE_SPACE      105     //get space left in tkip key cache by radio (for 11n)
#define IOCTL_CMD_GET_AES_KEY_CACHE_SPACE       106     //get space left in aes key cache by radio (for 11n)
#define IOCTL_CMD_SET_LICENSE_KEY               107     //write license key to atmel part
#define IOCTL_CMD_GET_LICENSE_KEY               108     //write license key to atmel part
#define IOCTL_CMD_SET_ASCOUT_MCAST              109     //aeroScount Tag's Multicast address
#define IOCTL_CMD_GET_ASCOUT_STATS              110     //get AeroScount Tag stats
#define IOCTL_CMD_GET_LOAD_BALANCE_STATE        111     //get load balance state for a station
#define IOCTL_CMD_PURGE_STA_TABLE               112     //purge station table
#define IOCTL_CMD_SET_ASCOUT_CHANNEL            113     //aeroscount tag's channel, for XN only
#define IOCTL_CMD_SET_WFA_MODE                  114     //wfa commpatibility mode
#define IOCTL_CMD_RLB_MODE                      115     //indicate if rlb is running
#define IOCTL_CMD_SET_XRP_IE                    116     //update probe response xrp ie
#define IOCTL_CMD_SET_WDS_LINK_DISTANCE         117     //set wds link distance
#define IOCTL_CMD_RLB_SET_CHAIN                 118     //rlb chain mask setting
#define IOCTL_CMD_SET_RDK_MODE                  119     //set rdk mode
#define IOCTL_CMD_GET_RDK_MODE                  120     //get rdk mode
#define IOCTL_CMD_GET_DFS_WAIT_TIME             121     //get dfs wait time (for any one or all interfaces)
#define IOCTL_CMD_SET_MULTICAST_MODE            122     //set multicast mode
#define IOCTL_CMD_GET_RADIOS_PRESENT_MASK       123     //get radios present mask
#define IOCTL_CMD_SET_IDS_CFG                   124     //set ids configuration parameters
#define IOCTL_CMD_GET_IDS_STATS                 125     //get ids stats
#define IOCTL_CMD_CLR_USER_AGENT_STR            126     //clear user agent string
#define IOCTL_CMD_SET_EXTRACT_FLAGS             127     //extract flags
#define IOCTL_CMD_SET_SSID_KBPS_LIMITS          128     //set total traffic limit and per sta traffic limit for ssid in Kbps
#define IOCTL_CMD_SET_GROUP_KBPS_LIMITS         129     //set total traffic limit and per sta traffic limit for group in Kbps
#define IOCTL_CMD_SET_DSCP_MODE                 130     //enable/disable DSCP to QoS mapping
#define IOCTL_CMD_SET_DSCP_MAP                  131     //set DSCP to QoS map
#define IOCTL_CMD_SET_MULTICAST_EXCLUDE         132     //set multicast exclude table
#define IOCTL_CMD_SET_MULTIPLE_CALL             133     //set multiple ioctls
#define IOCTL_CMD_GET_SYSTEM_STATUS             134     //get status: no. bad radios, memory errs, etc.
#define IOCTL_CMD_SET_MULTICAST_FORWARD         135     //set multicast forward table
#define IOCTL_CMD_SET_MCF_VLAN_TABLE            136     //set vlan table for multicast forwarding
#define IOCTL_CMD_SET_MDNS_FILTER_TABLE         137     //set mdns filter table
#define IOCTL_CMD_GET_ACTIVE_VLAN_TABLE         138     //get active vlan table
#define IOCTL_CMD_CLR_ACTIVE_VLAN_TABLE         139     //clear active vlan table
#define IOCTL_CMD_SET_HOTSPOT_CFG               140     //set hotspot configuration parameters
#define IOCTL_CMD_SAVE_CFG                      141     //indicate when current cfg is saved
#define IOCTL_CMD_SET_EKAHAU_MCAST              142     //ekahau tag's multicast address
#define IOCTL_CMD_CLR_ALL_DPI_STATS             143     //clear all dpi statistics
#define IOCTL_CMD_SET_DPI_APP_NAMES             144     //set dpi application names
#define IOCTL_CMD_GET_VLAN_APP_STATS            145     //get vlan dpi application statistics
#define IOCTL_CMD_GET_VLAN_CAT_STATS            146     //get vlan dpi category statistics
#define IOCTL_CMD_SET_WPR_WHITELIST             147     //action on wpr whitelist domain name
#define IOCTL_CMD_GET_DOT11AC_RADIOS_MASK       148     //get 802.11ac radios mask
#define IOCTL_CMD_SET_STA_URL_LOG               149     //enable/disable station URL logging
#define IOCTL_CMD_SET_HONEYPOT_WLIST            150     //set ssid honeypot whitelist
#define IOCTL_CMD_SET_HONEYPOT_BCAST            151     //set honeypot broadcast ssid list
#define IOCTL_CMD_SET_DOT11AC_PARMS             152     //set 802.11ac parmeters. word data is per dot11ac parmeters mode word defines
#define IOCTL_CMD_SET_DOT11W_MODE               153     //set 802.11w mode
#define IOCTL_CMD_SET_WMM_ACM                   154     //set wmm-acm mode
#define IOCTL_CMD_GET_WIFINO_CORE_TABLE         155     //get wifi_no & core_no table
#define IOCTL_CMD_REPLAY_CFG                    156     //replay radio cfg commands
#define IOCTL_CMD_SET_EXTRACT_IP_DHCP_PERIOD    157     //set IP address extraction via DHCP period
#define IOCTL_CMD_SET_STATUS_LED_CTRL           158     //status led control
#define IOCTL_CMD_WDS_MODE                      159     //set wds mode(host/client)
#define IOCTL_CMD_CAC_MAX_BW                    160     //max rf bandwidth(Call Admission Control)
#define IOCTL_CMD_GET_STA_RSSI_TABLE            161     //get all stations (assoc & unassoc) and rssi's
#define IOCTL_CMD_SET_TXBF_MODE                 162     //set Transmits Beam Forming Mode
#define IOCTL_CMD_SET_DOT11AC_RATES             163     //set 802.11ac rates. word data is mcs mask, 2 bits/nss. 1 => up to mcs7, 2 => up to mcs8, 3,0 => up to mcs9
#define IOCTL_CMD_SET_NEIGHBOR_TABLE            164     //set neighbor station info (inc. user group)
#define IOCTL_CMD_SET_FIXED_ADDR_TABLE          165     //set fixed (wired) station info (inc. user group)
#define IOCTL_CMD_SET_RETRIES_PER_RATE          166     //set retries per rate
#define IOCTL_CMD_SET_RETRY_RATE_DOWNS          167     //set retry rate downs
#define IOCTL_CMD_SET_PRB_RSP_MIN_RSSI          168     //set minimum rssi for probe response
#define IOCTL_CMD_SET_CCA_RX_THR_DELTA          169     //set delta between rx-threshold & cca
#define IOCTL_CMD_SET_AGGREGATE_RETRIES         170     //set block ack retries on failure
#define IOCTL_CMD_SET_BLOCK_NAK_RETRIES         171     //set block ack retries on success
#define IOCTL_CMD_RADIO_DOWN_UP                 172     //radio down up. note this is different from hard reset test
#define IOCTL_CMD_ETH_PHY_RESET                 173     //reset ethernet phy if it's an AR8033 phy
#define IOCTL_CMD_SET_WPR_HTTPS_PASSTHRU        174     //set https_passthru on an ssid
#define IOCTL_CMD_SET_BEACON_FT_PARAMS          175     //set ft params for a particular radio/ssid; write data is ioctl_set_beacon_ft_params
#define IOCTL_CMD_SET_FIPS_MODE                 176     //set the fips mode enabled flag on brcm device to enable software encryption
#define IOCTL_CMD_GET_SSID_APP_STATS            177     //get ssid dpi application statistics
#define IOCTL_CMD_GET_SSID_CAT_STATS            178     //get ssid dpi category statistics
#define IOCTL_CMD_XMT_EAPOL_PACKET              179     //Transmit EAPOL Packet
#define NO_STD_IOCTLS                           180

/* Hard Reset Test Ioctls - see xap_ioctl/xap_hrt.c and rincon/{chardev,hrt}.c  */
/* If you want to include HRESET testing in production code, enable:            */
/* Note, the ioctls will become no-ops if you de-select the above.              */
#define XIRRUS_ATHEROS_HRESET_TEST
#ifdef  XIRRUS_ATHEROS_HRESET_TEST
#define IOCTL_CMD_HRT_PROBE_CONFIG         (NO_STD_IOCTLS + 0)
#define IOCTL_CMD_HRT_TEST_TSF             (NO_STD_IOCTLS + 1)
#define IOCTL_CMD_HRT_RESET_RADIO          (NO_STD_IOCTLS + 2)
#define IOCTL_CMD_HRT_RESET_RADIO_ALL      (NO_STD_IOCTLS + 3)
#define IOCTL_CMD_HRT_RESET_PCI_LVL        (NO_STD_IOCTLS + 4)
#define IOCTL_CMD_HRT_READ_RINCON          (NO_STD_IOCTLS + 5)
#define IOCTL_CMD_HRT_READ_TOPANGA         (NO_STD_IOCTLS + 6)
#define NO_IOCTLS                          (NO_STD_IOCTLS + 7)
#else
#define NO_IOCTLS                           NO_STD_IOCTLS
#endif

//* IOCTL_CMD_SET_G_MODE mode word values
/*
 */
#define SET_G_MODE_CCK_ENB              1        //these are the gradations of mixed mode protection that can
#define SET_G_MODE_SHORT_SLOT_ENB       2        //be afforded to b clients in a mixed b/g network.
#define SET_G_MODE_CTS_PROTECT_ENB      4
#define SET_G_MODE_RTS_PROTECT_ENB      8
#define SET_G_MODE_G_ONLY           (!SET_G_MODE_CCK_ENB |  SET_G_MODE_SHORT_SLOT_ENB | !SET_G_MODE_CTS_PROTECT_ENB | !SET_G_MODE_RTS_PROTECT_ENB)
#define SET_G_MODE_NO_PROTECTION    ( SET_G_MODE_CCK_ENB |  SET_G_MODE_SHORT_SLOT_ENB | !SET_G_MODE_CTS_PROTECT_ENB | !SET_G_MODE_RTS_PROTECT_ENB)
#define SET_G_MODE_FULL_PROTECTION  ( SET_G_MODE_CCK_ENB | !SET_G_MODE_SHORT_SLOT_ENB | !SET_G_MODE_CTS_PROTECT_ENB |  SET_G_MODE_RTS_PROTECT_ENB)

//* IOCTL_CMD_GET_UNASSOC_STA_INFO flags
/*
 */
#define GET_UNASSOC_STA_EXC_XIRRUS      0x8000   // if bit is set in the aid, exclude xirrus mac addresses
#define GET_UNASSOC_STA_EXC_RANDOM      0x4000   // if bit is set in the aid, exclude random mac addresses
#define GET_UNASSOC_STA_EXC_MASK        0xc000

//* IOCTL_CMD_SET_SNEAKY_INTERVAL mode word values
/*
 */
#define SET_SNEAKY_DEFAULT              1        //sneaky irq disable feature is enabled; re-enable irqs at default interval
#define SET_SNEAKY_DISABLE              0        //sneaky irq disable feature is disabled; floyd irqs are always enabled
#define SET_SNEAKY_RESTORE              32768    //restore setting to what it was before the last call to this ioctl
#define SET_SNEAKY_MAX_INTERVAL         32767    //sneaky irq disable feature is enabled... but re-enable only every 33 seconds


//* struct ioctl_cmd_prop
/*
 * @remarks the ioctl_cmd_props table, which is a table of these structs, facilitates a table driven command parser.
 */
struct ioctl_cmd_prop {
  int write_size;                               //amount of data written from userspace-to-kernel
  int read_size;                                //amount of data returned from kernel-to-userspace
  int iface_flags;                              //defined below: does this cmd requires specific interface parameter
  int config_cnt;                               //non-zero to indicate radio cfg commands; count equals number of times ioctl is used
};
#define IOCTL_CMD_PROP_IFACE_IS_NA      0x0     //interface need not be specified
#define IOCTL_CMD_PROP_ANY_1_IFACE      0x1     //command applies to any one interface; must be specified
#define IOCTL_CMD_PROP_ALL_IFACE        0x2     //command applies to all interfaces; -1 must be specified
#define IOCTL_CMD_PROP_ANY_OR_ALL_IFACE 0x3     //command applies to any one, or all interfaces
#define IOCTL_CMD_PROP_IFACE_IS_PCILVL  0x4     //command applies to pci bus (incl all radios thereon)


//* ioctl_cmd struct
/**
 * @brief each command is prefixed with this header
 */
struct ioctl_cmd {
  u16 cmd;                                      //cmd code
  s8  iface;                                    //network interface 0-15, -1 for all
  s8  ssid;                                     //ssid 0-15, -1 for all
  u16 len;                                      //length of command data, or length of buffer for status
} __attribute__ ((packed));

struct cmd_buff
{
   struct ioctl_cmd hdr;
   char   data[];
}  __attribute__ ((packed));

struct ioctl_wds_bind_info {
  s32 iap_devno;
  u8  partner_bssid[8];
  s32 ssid;
  s32 vlan;
  s32 aid;
  s32 client;
  s32 pae;                                        //Read only
  s32 link_num;
  s32 connected;
  u8  base_mac_addr[8];
} __attribute__ ((packed));

#define SIZE_WDS_BIND_INFO      sizeof(struct ioctl_wds_bind_info)

struct xrp_ie_loudest {
  u8  mac[6];
  s16 rssi;
  s16 tx_power;
} __attribute__ ((packed));

#define NUM_LOUDEST             4                 //number of loudest arrays to track

//****************************************************************
// IDS definitions
//----------------------------------------------------------------
#define IDS_DOS_ATTACK_BEACON_FLOOD     0
#define IDS_DOS_ATTACK_PROBE_REQ_FLOOD  1
#define IDS_DOS_ATTACK_AUTH_FLOOD       2
#define IDS_DOS_ATTACK_ASSOC_FLOOD      3
#define IDS_DOS_ATTACK_DISASSOC_FLOOD   4
#define IDS_DOS_ATTACK_DEAUTH_FLOOD     5
#define IDS_DOS_ATTACK_EAP_FLOOD        6
#define IDS_DOS_ATTACK_NULL_PROBE_RESP  7
#define IDS_DOS_ATTACK_MIC_ERROR        8
#define IDS_DOS_ATTACK_SPOOFED_BEACON   9
#define IDS_DOS_ATTACK_SPOOFED_DISASSOC 10
#define IDS_DOS_ATTACK_SPOOFED_DEAUTH   11

#define NUM_IDS_DOS_ATTACKS             12
#define NUM_IDS_PKT_FLOODS              7

#define IDS_SEQ_NUM_ANOMALY             12
#define IDS_DURATION_ATTACK             13

#define IDS_DOS_ATTACK_DISABLED         0
#define IDS_DOS_ATTACK_MANUAL           1
#define IDS_DOS_ATTACK_AUTO             2

#define IDS_SEQ_NUM_ANOMALY_DISABLED    0
#define IDS_SEQ_NUM_ANOMALY_MGMT        1
#define IDS_SEQ_NUM_ANOMALY_DATA        2

#define IDS_EVENT_RESERVED              0
#define IDS_EVENT_BEACON_FLOOD          1
#define IDS_EVENT_PROBE_REQ_FLOOD       2
#define IDS_EVENT_AUTH_FLOOD            3
#define IDS_EVENT_ASSOC_FLOOD           4
#define IDS_EVENT_DISASSOC_FLOOD        5
#define IDS_EVENT_DEAUTH_FLOOD          6
#define IDS_EVENT_EAP_FLOOD             7
#define IDS_EVENT_SPOOFED_BEACON        8
#define IDS_EVENT_SPOOFED_DISASSOC      9
#define IDS_EVENT_SPOOFED_DEAUTH        10
#define IDS_EVENT_DURATION_ATTACK       11
#define IDS_EVENT_MIC_ERROR             12
#define IDS_EVENT_NULL_PROBE_RESP       13
#define IDS_EVENT_SEQ_NUM_ANOMALY       14
#define IDS_EVENT_STA_IMPERSONATION     15
#define IDS_EVENT_STA_BROADCAST         16
#define IDS_EVENT_EVIL_TWIN_ATTACK      17
#define IDS_EVENT_RF_JAMMING            18

struct dos_attack_cfg {
  u8  mode;
  u32 threshold;
  u32 period;
} __attribute__ ((packed));

#define SIZE_DOS_ATTACK_CFG sizeof(struct dos_attack_cfg)

struct dur_attack_cfg {
  u8  enabled;
  u32 threshold;
  u32 period;
  u16 nav;
} __attribute__ ((packed));

#define SIZE_DUR_ATTACK_CFG sizeof(struct dur_attack_cfg)

struct seq_num_anomaly_cfg {
  u8  mode;
  u16 gap;
  u16 gap_max;
} __attribute__ ((packed));

#define SIZE_SEQ_NUM_ANOMALY_CFG sizeof(struct seq_num_anomaly_cfg)

struct sta_imp_cfg {
  u8  enabled;
  u32 threshold;
  u32 period;
} __attribute__ ((packed));

#define SIZE_STA_IMP_CFG sizeof(struct sta_imp_cfg)

struct evil_twin_cfg {
  u8  enabled;
} __attribute__ ((packed));

#define SIZE_EVIL_TWIN_CFG sizeof(struct evil_twin_cfg)

struct ids_attack_cfg {
  struct dos_attack_cfg         dos_attacks[NUM_IDS_DOS_ATTACKS];
  struct dur_attack_cfg         dur_attack;
  struct seq_num_anomaly_cfg    seq_num_anomaly;
  struct sta_imp_cfg            sta_imp;
  struct evil_twin_cfg          evil_twin;
} __attribute__ ((packed));

#define SIZE_IDS_ATTACK_CFG sizeof(struct ids_attack_cfg)


#define MAX_RESET_REG_SUBTESTS    6

#define RESET_REG_RESULT_OK       0
#define RESET_REG_RESULT_UNSUPP   1
#define RESET_REG_RESULT_NO_SC    2
#define RESET_REG_RESULT_TOPFAIL  3
#define RESET_REG_RESULT_TOPRESET 4 /* see 8 msbs for regmask , next 8 for bus*/
#define RESET_REG_RESULT_NOPCI    5 /* radio's pci dev in linux not present */
#define RESET_REG_RESULT_BADSPLIT 6 /* Rincon's manufactured split completions
                                     * for us, see rest of subtest result for
                                     * other radios on the same bus */
#define RESET_REG_RESULT_VERIFY_FAIL 7
#define RESET_REG_RESULT_RRESET_FAIL 8

typedef struct HRT_ResetReg_status {

    int   repeat;
    u32   subTestResult[MAX_RESET_REG_SUBTESTS];

} __attribute__ ((packed)) HRT_ResetReg_status;


typedef struct HRT_ResetReg_ioctl {

    struct ioctl_cmd     cmd; /* must be first */
    HRT_ResetReg_status  status;

} HRT_ResetReg_ioctl;




/* IOCTL_CMD_HRT_READ_RINCON structs/defs     */

typedef struct HRT_Register_tuple {

    u32  reg, val;

} __attribute__ ((packed)) HRT_Register_tuple;

#define MAX_RINCON_REG 34

typedef struct HRT_Rincon_status {

    int                numRegisters;
    HRT_Register_tuple rin_registers[MAX_RINCON_REG];

} __attribute__ ((packed)) HRT_Rincon_status;

typedef struct HRT_Rincon_ioctl {

    struct ioctl_cmd   cmd; /* must be first */
    HRT_Rincon_status  status;

} HRT_Rincon_ioctl;

/* IOCTL_CMD_HRT_READ_TOPANGA structs/defs    */

#define MAX_TOPANGA_REG 2
#define MAX_TOPANGA_BUS 4

typedef struct HRT_Topanga_status {

    int                numBus;
    int                numRegisters;
    HRT_Register_tuple top_registers[MAX_TOPANGA_BUS][MAX_TOPANGA_REG];

} __attribute__ ((packed)) HRT_Topanga_status;

typedef struct HRT_Topanga_ioctl {

    struct ioctl_cmd   cmd; /* must be first */
    HRT_Topanga_status status;

} HRT_Topanga_ioctl;


#define HRT_MAX_PCI_CFG 16

#define HRT_PCI_CONFIG_OK        0
#define HRT_PCI_CONFIG_UNSUPP    1
#define HRT_PCI_CONFIG_EEXIST    2
typedef struct HRT_Atheros_PCICfg {

    u32                result; // one of HRT_PCI_CONFIG_
    int                numRegisters;
    HRT_Register_tuple athPciCfg_registers[HRT_MAX_PCI_CFG];

} __attribute__ ((packed)) HRT_Atheros_PCICfg;

typedef struct HRT_PciCfg_ioctl {

    struct ioctl_cmd   cmd; /* must be first */
    HRT_Atheros_PCICfg status;

} HRT_PciCfg_ioctl;

#define DISASSOC_FROM_APPLE_NOTEBOOK  0x1

//* ioctl_cmd write data
union ioctl_write_data {
  u16 word;                                       //for misc commands that write one word
  u32 long_word;                                  //for misc commands that write one long word
  u64 long_long;                                  //for misc commands that write one long long word
  u8 addr[6];                                     //for misc commands that write one address
  struct cmd_buff cmd;
  struct ioctl_get_sta_info_w {                   //for get_sta_info cmd. (must match mac_sta_info)
    u8 addr[6];                                   // mac addr sta to lookup
    u16 aid;                                      // for unassociated stations, the index to lookup
    u32 secs_since_activity;                      // not used
  } __attribute__ ((packed)) igsi;
  struct ioctl_set_sta_info {                     //for set_sta_info cmd.
    u8 addr[6];                                   // sta for whom to set info
    u16 aid;                                      // aid == 0 means dis-associate/remove sta from table
    u16 event;                                    // sta event that triggered IOCTL_CMD_SET_STA_INFO call
    u16 initial_rate;                             // initial data rate for transmits to this sta, -1 => don't touch
    u8 wpa_key[32];                               // encryption key (for aes or tkip only)
    u8 enc_type;                                  // defined below (ISSI_XXX)
    u8 key_id;                                    // 0-3 (for aes or tkip only)
    u16 virt_dev_no;                              // -1 => don't touch, -2 => set no virt dev
    u16 vlan;                                     // 0 = none, -1 => don't touch
    u16 qos_flags;                                // see sta_info.qos_flags defines, -1 => don't touch
    u64 supported_rates;                          // bit mask, bit 1 => 1 Mbps, bit 54 => 54 Mbps, -1 => don't touch
    u64 supported_mcs_rates;                      // bit mask, bit 0 => mcs0  , bit 31 => mcs31  , -1 => don't touch
    u8 is_sgi_40mhz;                              // supports short guard interval @ 40MHz
    u8 is_sgi_80mhz;                              // supports short guard interval @ 80MHz
    u8 is_sgi_160mhz;                             // supports short guard interval @ 160MHz
    u8 wpr_type;
    u8 is_wpr_authorized;
    u8 group;
    u8 dhcp_opt;
    u8 special_flags;
    u8 device;
    u8 ft_assoc;
  } __attribute__ ((packed)) issi;
  struct ioctl_set_wep_keys {                     //for set_wep_keys cmd.
    u8 lens[4];                                   // length of each key, 5 ir 13 bytes
    u8 keys[4][16];                               // keys, 5 or 13 bytes
    u16 tx_idx;                                   // *the* index of *the* key to use for all tx's from this ssid
  } __attribute__ ((packed)) iswk;
  struct ioctl_set_wpa_broadcast_key {            //for set_wpa_broadcast_key cmd.
    u8 key[32];                                   // encription key.
    u8 enc_type;                                  // type, defined below (ISSI_XXX) (not really per radio, type affects the whole box)
    u8 key_id;                                    // 0-3
  } __attribute__ ((packed)) iswbk;

  //* ioctl_cmd write data (continued)
  struct ioctl_set_piddle_data {
    u16 piddle0;
    u16 piddle1;
    u16 piddle2;
    u16 piddle3;
  } __attribute__ ((packed)) ispd;
  struct ioctl_set_rates_data {
    u64 basic_rates;                              //bit mask, bit 1 => 1 Mbps, bit 54 => 54 Mbps
    u64 supported_rates;                          //bit mask, bit 1 => 1 Mbps, bit 54 => 54 Mbps
  } __attribute__ ((packed)) isrd;
  struct ioctl_get_radio_stats_w {
    struct radio_stats *stats;
  } __attribute__ ((packed)) igs;
  struct ioctl_get_station_stats_w {
    u8 addr[6];                                   // sta for whom to get stats
    u16 aid;                                      //aid == 0 means we have no such sta in our table
    struct sta_stats *stats;
  } __attribute__ ((packed)) igss;
  struct ioctl_get_ids_stats_w {
    struct ids_stats *stats;
  } __attribute__ ((packed)) igis;

#define MAX_2GHZ_CHANNELS           15            //maximum number of 2 gHz channels, for all regulatory regions
#define MAX_5GHZ_CHANNELS           35            //maximum number of 2 gHz channels, for all regulatory regions
  struct ioctl_set_geography_info {
    u8 active_channels[MAX_2GHZ_CHANNELS + MAX_5GHZ_CHANNELS];
    u8 auto_2ghz_channels[MAX_2GHZ_CHANNELS];
    u8 auto_5ghz_channels[MAX_5GHZ_CHANNELS];
    u8 radar_channels[MAX_5GHZ_CHANNELS];
    u8 default_channels[MAX_IAPS];
    u8 power_limits[MAX_2GHZ_CHANNELS + MAX_5GHZ_CHANNELS];
  } __attribute__ ((packed)) isgi;
  struct ioctl_wds_mode {
    u8 mode;
  } __attribute__ ((packed)) iwdm;
  struct ioctl_wds_bind_info iwbi;
  struct ioctl_set_wds_info {
    u8 target_addr[6];
    u8 iface_cnt;
    s8 radios[3];
  } __attribute__ ((packed)) iswi[4];
  struct ioctl_set_ssid_traffic_limits {
    u32 limit_ssid;
    u32 limit_sta;
  } __attribute__ ((packed)) isstl;
  struct ioctl_set_group_traffic_limits {
    u32 limit_group;
    u32 limit_sta;
  } __attribute__ ((packed)) isgtl;
  struct ioctl_set_pae_switch {
        u8 addr[6];
        u16 state;
  } __attribute__ ((packed)) sps;
  struct ioctl_set_country_code {
    u8 country_code[2];
  }__attribute__ ((packed)) iscc;
  struct ioctl_set_local_ips {
    u32 ip0;
    u32 ip1;
    u32 ip2;
  }__attribute__ ((packed)) isli;
  struct ioctl_set_wpr_passthru_ips {
    u32 ips[8];
  }__attribute__ ((packed)) iswpi;
  struct ioctl_set_vlan_table {
    u16 vlan_table[XIR_VLAN_TABLE_NUM];
  }__attribute__ ((packed)) isvt;
  struct ioctl_set_mcf_vlan_table {
    u16 mcf_vlan_table[XIR_VLAN_TABLE_NUM];
  }__attribute__ ((packed)) ismv;
  struct ioctl_set_license_key {
    u8 license[32];
  }__attribute__ ((packed)) islk;
  struct ioctl_set_load_balance_state {
    u8 addr[6];
    u8 state;
  }__attribute__ ((packed)) iglb;
  struct ioctl_set_xrp_ie {
      u8  base_mac [6];
      u8  ipv4_addr[4];
      u16 socket;
      s16 tx_power;
      struct xrp_ie_loudest loudest_first;      // here for backward compatibility
      s16 max_tx_power;
      s16 auto_chan_neg_state;
      struct xrp_ie_loudest loudest_rest[NUM_LOUDEST-1];
      s16 stations;
      u16 crc;
  }__attribute__ ((packed)) isxi;
  struct ioctl_set_ids_cfg {
    u8 enabled;
    struct ids_attack_cfg attacks;
  }__attribute__ ((packed)) isic;
  struct ioctl_set_dscp_map {
    u8 map[MAX_NUM_DSCP_VALUES];
  }__attribute__ ((packed)) isdm;
  struct ioctl_set_mcx {
    u128 exclude[MAX_NUM_MULTICAST_EXCLUDE];
  }__attribute__ ((packed)) ismc;
  struct ioctl_set_mcf {
    u128 forward[MAX_NUM_MULTICAST_FORWARD];
  }__attribute__ ((packed)) ismf;
  struct ioctl_set_mdns_filter {
    char filter[MAX_NUM_MDNS_FILTER][REQ_LEN_MDNS_FILTER];
  }__attribute__ ((packed)) ismd;
  struct ioctl_set_hotspot_cfg {
    u8 interworking;
    u8 an_opts;
    u8 hessid[6];
  }__attribute__ ((packed)) ishs;
  HRT_ResetReg_status       hrt_resetreg_status;
  struct ioctl_set_app_names {
    char app_names[MAX_DPI_APP_INDEX][MAX_DPI_APP_NAME];
  }__attribute__ ((packed)) isan;
  struct ioctl_set_wpr_whitelist {
    u8   action;
    char domain_name[MAX_LEN_HOSTNAME_STR];
  }__attribute__ ((packed)) isww;
  struct ioctl_set_honeypot_wlist {
    u16 num_ssid;
    char ssids[MAX_SIZE_HONEYPOT_WLIST][33];
  }__attribute__ ((packed)) ishw;
  struct ioctl_set_honeypot_bcast {
    u16 num_ssid;
    char ssids[MAX_SIZE_HONEYPOT_BCAST][35];
  }__attribute__ ((packed)) ishb;
  struct ioctl_get_sta_rssi_table_w {
    u16 num_entries;
    struct mac_rssi_entry *table;
  }__attribute__ ((packed)) igsr;
  struct ioctl_set_neighbor_table_w {
    u32 num_entries;
    struct mac_rssi_entry *table;
  }__attribute__ ((packed)) isnt;
  struct ioctl_set_fixed_addr_table_w {
    u32 num_entries;
    struct mac_rssi_entry *table;
  }__attribute__ ((packed)) isft;
  struct ioctl_eth_phy_reset {
    char dev_name[8];
  }__attribute__ ((packed)) isdn;
  struct ioctl_set_https_passthru {
      u8 enable;
  }__attribute__ ((packed)) iswhp;
  struct ioctl_set_beacon_ft_params {
    u16 ftEnable;
    u16 ftMdid;
  } __attribute__ ((packed)) isbfp;
  struct ioctl_get_vlan_app_stats_w {
    struct dpi_stats_info *info;
  }__attribute__ ((packed)) igva;
  struct ioctl_get_ssid_app_stats_w {
    struct dpi_stats_info *info;
  }__attribute__ ((packed)) igsa;
  struct ioctl_xmt_eapol_pkt_w {
      u8    addr[6];
      u16   eapPacketLength;
      u8    eapPacketBuffer[1];
  }__attribute__ ((packed)) ixea;
}__attribute__ ((packed));


//* ioctl_cmd read data
union ioctl_read_data {
  u16 word;                                       //for misc commands that read one word
  u32 long_word;                                  //for misc commands that read one long word
  u64 long_long;                                  //for misc commands that write one long long word
  u8 addr[6];                                     //for misc commands that read one address
  u8 radios[16];                                  //for misc commands that read one byte per radio
//struct ioctl_old_get_sta_info_r {               //for old_get_sta_info cmd
//  u8 addr[6];                                   // mac addr of sta
//  u16 aid;                                      // aid == 0 means we have no such sta in our table
//  unsigned long secs_since_activity;            // seconds since last packet transmitted to, or received from this sta
//  u8 netbios_name[20];                          // 16 bytes, with a couple extra zeros
//  u32 ipv4_addr;                                //
//} __attribute__ ((packed)) iogsi;
  struct mac_sta_info igsi;
  struct ioctl_get_version {                      //for get_version cmd
    char driver_version_string[48];
    u16 floyd_version;
    u16 opie_version;
    u16 andy_version;
    u16 rincon_version;
    u16 mugu_version;
    u16 laguna_version;
    u16 topanga_version;
    u16 avalon_version;
    u8 scd_maj_rev;
    u8 scd_min_rev;
    u8 scd_month;
    u8 scd_year;
  } __attribute__ ((packed)) igv;
  struct ioctl_get_rom_addr {                     //for get_rom_addr cmd
    u8 addr[6];
  } __attribute__ ((packed)) igra;
//struct ioctl_old_get_stats {                    //old, deprecated get_stats. don't use this; usr the new get_state.
//  struct radio_stats stats;
//} __attribute__ ((packed)) iogs;
  struct ioctl_get_channel_assignments {
    u8 channels[16];
  } __attribute__ ((packed)) ica;
  struct ioctl_get_radio_stats_r {
    struct radio_stats *stats;
  } __attribute__ ((packed)) igs;
//# ifdef XMAC_DRIVER                             //make this file use-able by other programs
  struct ioctl_get_station_stats_r {
    u8 addr[6];                                   //sta for whom to set stats
    u16 aid;                                      //aid == -1 means we have no such sta in our table
    struct sta_stats *stats;
  } __attribute__ ((packed)) igss;
//# endif
  struct ioctl_get_ids_stats_r {
    struct ids_stats *stats;
  } __attribute__ ((packed)) igis;
  struct ioctl_wds_bind_info iwbi;
  struct ioctl_get_wds_info {
    u8 wds_link[16];
  } __attribute__ ((packed)) igwi;
  struct ioctl_get_country_code {
    u8 country_code[2];
  }__attribute__ ((packed)) igcc;
  struct ioctl_get_license_key {
    u8 license[32];
  }__attribute__ ((packed)) iglk;
  struct ioctl_get_load_balance_state {
    u8 addr[6];
    u8 state;
  }__attribute__ ((packed)) iglb;
  struct ioctl_get_system_status {
    u8 no_radios_configured;   //set from scd, or overridden
    u8 no_radios_found;        //radios found, eg. by pci probe
    u8 no_radios_failed;       //radios that failed in probe, or we determined are missing
    u8 err_mask;               //bit: 0 => cmb err
  }__attribute__ ((packed)) igsys;
  struct ioctl_get_active_vlan_table {
    u8 active_vlan[4096 / 8];
  }__attribute__ ((packed)) igav;
  struct ioctl_get_vlan_app_stats_r {
    struct dpi_stats_info *info;
  }__attribute__ ((packed)) igva;
  struct ioctl_get_vlan_cat_stats {
    struct dpi_stats_info info[MAX_DPI_CAT_INDEX];
  }__attribute__ ((packed)) igvc;
  struct ioctl_get_ssid_app_stats_r {
    struct dpi_stats_info *info;
  }__attribute__ ((packed)) igsa;
  struct ioctl_get_ssid_cat_stats {
    struct dpi_stats_info info[MAX_DPI_CAT_INDEX];
  }__attribute__ ((packed)) igsc;
  struct ioctl_get_wifino_core_table {
    s8 wifino[16];
    s8 core[16];
  }__attribute__ ((packed)) igwc;
  struct ioctl_get_sta_rssi_table_r {
    u16 num_entries;
    struct sta_rssi_entry *table;
  }__attribute__ ((packed)) igsr;
  HRT_ResetReg_status       hrt_resetreg_status;
  HRT_Rincon_status         hrt_rincon_status;
  HRT_Topanga_status        hrt_topanga_status;
  HRT_Atheros_PCICfg        hrt_atheros_status;
}__attribute__ ((packed));

//* encription type definitions for set station info, set wpa keys, and set encryption
/*
 * @note the encryption defines below must magically match those in the station table

 * @remarks the set wep keys ioctl just sets the four static wep keys, which are global for all ssid's and all
 * radios. it also sets the tx index which tells us which key to use for all transmits, including
 * broadcasts. note however, we do not send wep encrypted broadcasts unless the wep bit is set in the set
 * encryprtion type ioctl. the set wpa broadcast keys ioctl sets the keys that are used for encrypting
 * broadcasts. each radio gets its own key. when wpa broadcast keys are set for any radio, however, we can
 * infer the type of wpa encryption, tkip or aes. however wpa is not enabled until you call the set encryption
 * types ioctl. the set encryption types ioctl tells us what global encryption types are enabled across all
 * ssids. that is, if any ssid has open, or static wep, or wpa enabled, then the corresponding bit will be set
 * in the encryption type field in the set encryption types ioctl. we don't care for which ssid a certain type
 * of encryption is enabled - just that it is enabled for any ssid. note that we ignore the tkip and aes
 * fields, as those are inferred from the set wpa broadcast keys ioctl.
 */
#define ISSI_ENC_DIS_SSID                   0x0000                  //non-existant encryption that we don't perform for disabled ssids
#define ISSI_ENC_OPEN_EN                    0x0001                  //this is also default for set sta info, if no bits are set
#define ISSI_ENC_WEP_EN                     0x0002                  //key is in static keys
#define ISSI_ENC_WPA_EN                     0x0010                  //set for tkip & aes
#define ISSI_ENC_TKIP                       0x0004                  //temporal key is first 16, tx mic is next 8, rx mic is last 8
#define ISSI_ENC_AES                        0x0008                  //temporal key is first 16 bytes
#define ISSI_ENC_TKIP_EN        (ISSI_ENC_WPA_EN | ISSI_ENC_TKIP)   //temporal key is first 16, tx mic is next 8, rx mic is last 8
#define ISSI_ENC_AES_EN         (ISSI_ENC_WPA_EN | ISSI_ENC_AES)    //temporal key is first 16 bytes
#define ISSI_ENC_NO_CHG                     0x00ff                  //do not change enc type

//* WPR type definitions for set station info
#define ISSI_WPR_NONE                            0                  // no WPR
#define ISSI_WPR_STD                             1                  // standard WPR
#define ISSI_WPR_MDM                             2                  // WPR for MDM auth

//* qos flag definitions for set station info
/*
 * @note the qos flags defines below must magically match those in the station table
 */
#define ISSI_QOS_FLAGS_QUEUE_SHIFT          0x0000                  //
#define ISSI_QOS_FLAGS_QUEUE_MASK           0x0003                  //qos queue to use, if ISSI_QOS_FLAGS_QUEUE_PER_VLAN is not set
#define ISSI_QOS_FLAGS_USE_11E_FRAMES       0x4000                  //use 802.11e encapsulation for frames to this sta
#define ISSI_QOS_FLAGS_QUEUE_PER_VLAN       0x8000                  //use vlan priority for qos queue (if higher), if z always use sta setting

//* event definitions for set station info
#define ISSI_EVENT_UNKNOWN                       0
#define ISSI_EVENT_ASSOC_STA                     1
#define ISSI_EVENT_ASSOC_WDS_CLIENT              2
#define ISSI_EVENT_DISASSOC_RX                   3
#define ISSI_EVENT_DISASSOC_TX_CLASS3_ERR        4
#define ISSI_EVENT_DEAUTH_RX                     5
#define ISSI_EVENT_DEAUTH_TX_MULTI_STA           6
#define ISSI_EVENT_DEAUTH_TX_ADMIN               7
#define ISSI_EVENT_DEAUTH_TX_HOSTAPD             8
#define ISSI_EVENT_DEAUTH_TX_GROUP_NOT_ACTIVE    9
#define ISSI_EVENT_DEAUTH_TX_ROAM_ASSIST        10
#define ISSI_EVENT_DEAUTH_TX_XRP_ROAM_STA       11
#define ISSI_EVENT_DEAUTH_TX_ON_WDS_LINK        12
#define ISSI_EVENT_DEAUTH_TX_WPA_SUPPLICANT     13
#define ISSI_EVENT_DEAUTH_TX_WDS_LINK_TIMEOUT   14
#define ISSI_EVENT_DEAUTH_TX_WDS_CLIENT_ROAM    15
#define ISSI_EVENT_DEAUTH_ASSOC_TIMEOUT         16
#define ISSI_EVENT_DEAUTH_REASSOC               17
#define ISSI_EVENT_DEAUTH_ROAM                  18
#define ISSI_EVENT_SET_8021X_KEY_STA            19
#define ISSI_EVENT_SET_8021X_KEY_WDS_CLIENT     20
#define ISSI_EVENT_SET_8021X_VLAN               21
#define ISSI_EVENT_SET_GROUP_INFO               22
#define ISSI_EVENT_SET_WPR_AUTH_OOPME           23
#define ISSI_EVENT_SET_WPR_AUTH_WPRD            24
#define ISSI_EVENT_SET_L3ROAM_VIRT_IFACE        25
#define ISSI_EVENT_SET_TUN_VIRT_IFACE           26
#define ISSI_EVENT_CLEAR_STATS                  27
#define ISSI_EVENT_PURGE_STA_TABLE              28
#define ISSI_EVENT_DEAUTH_TX_DRIVER_REQUEST     29
#define ISSI_EVENT_DEAUTH_TX_UPSK_FAIL          30
#define ISSI_EVENT_SET_DEVICE                   31
#define ISSI_EVENT_DEAUTH_TX_GROUP_FULL         32

//* external led control
/*
 * @note Use this to temporarily wrest control of the leds from the driver. Bracket all calls with start/stop.
 *       "NEXT" will illuminate the next led in a clockwise pattern - useful for progress indicators
 */
#define ISSI_LED_CTRL_START                 0x0000                  //start external control of the leds
#define ISSI_LED_CTRL_NEXT                  0x0001                  //illuminate the next led
#define ISSI_LED_CTRL_STOP                  0x0002                  //return control to the driver
#define ISSI_LED_CTRL_ALL_ON                0x0003                  //turn all leds on
#define ISSI_LED_CTRL_DIRECT                0x0004                  //turn all leds on


//* fips result definitions for IOCTL_CMD_GET_FIPS_AES_RESULT
/*
 */
#define FIPS_TESTS_COUNT                         4
#define FIPS_TESTS_RUN_MASK             0x000000FF
#define FIPS_TESTS_PASSED_MASK          0x0000FF00

//* dot11n parms mode word
/*
 */
#define DOT11N_MODE_2040_MASK               0x0003
#define DOT11N_MODE_2040_NOP                0x0000
#define DOT11N_MODE_2040_STATIC20           0x0001
#define DOT11N_MODE_2040_STATIC40           0x0002
#define DOT11N_MODE_2040_DYNAMIC2040        0x0003
//
#define DOT11N_MODE_EXTOFFSET_MASK          0x000c
#define DOT11N_MODE_EXTOFFSET_NOP           0x0000
#define DOT11N_MODE_EXTOFFSET_NONE          0x0004
#define DOT11N_MODE_EXTOFFSET_MINUS1        0x0008
#define DOT11N_MODE_EXTOFFSET_PLUS1         0x000c
//
#define DOT11N_MODE_TXCHAINS_MASK           0x00f0
#define DOT11N_MODE_TXCHAINS_NOP            0x0000
#define DOT11N_MODE_TXCHAINS_SHIFT          0x0004                  //we cleverly set these values to the required
#define DOT11N_MODE_TXCHAINS_1              0x0010                  //masks to get the right number of chains...
#define DOT11N_MODE_TXCHAINS_2              0x0050
#define DOT11N_MODE_TXCHAINS_3              0x0070
#define DOT11N_MODE_TXCHAINS_4              0x00f0
#define DOT11N_MODE_TXCHAINS_BG             0x0030                  //for 2.4G channel only
#define DOT11N_MODE_TXCHAINS_EXT_XN4        0x0020                  //external antenna for XN4 only
#define DOT11N_MODE_TXCHAINS_EXT            0x0040                  //external antenna only
#define DOT11N_MODE_TXCHAINS_0_1            0x0030                  //setting for chain 0 and 1, used by 9280
#define DOT11N_MODE_TXCHAINS_0_2            0x0050
#define DOT11N_MODE_TXCHAINS_1_2            0x0060
//
#define DOT11N_MODE_RXCHAINS_MASK           0x0f00
#define DOT11N_MODE_RXCHAINS_NOP            0x0000
#define DOT11N_MODE_RXCHAINS_SHIFT          0x0008                  //we cleverly set these values to the required
#define DOT11N_MODE_RXCHAINS_1              0x0100                  //masks to get the right number of chains...
#define DOT11N_MODE_RXCHAINS_2              0x0500
#define DOT11N_MODE_RXCHAINS_3              0x0700
#define DOT11N_MODE_RXCHAINS_4              0x0f00
#define DOT11N_MODE_RXCHAINS_EXT_XN4        0x0200                  //external antenna for XN4 only
#define DOT11N_MODE_RXCHAINS_EXT            0x0400                  //external antenna only
#define DOT11N_MODE_RXCHAINS_0_1            0x0300                  //setting for chain 0 and 1, used by 9280
#define DOT11N_MODE_RXCHAINS_0_2            0x0500
#define DOT11N_MODE_RXCHAINS_1_2            0x0600
//
#define DOT11N_MODE_GUARD_MASK              0x3000
#define DOT11N_MODE_GUARD_NOP               0x0000
#define DOT11N_MODE_GUARD_LONG              0x1000
#define DOT11N_MODE_GUARD_SHORT             0x2000
//
#define DOT11N_MODE_ENABLE_MASK             0xc000
#define DOT11N_MODE_ENABLE_NOP              0x0000
#define DOT11N_MODE_ENABLE_OFF              0x4000
#define DOT11N_MODE_ENABLE_ON               0x8000
//
#define DOT11N_MODE_GI_20_MASK             0x30000                  //setting for .11ac capable radios -- needs plumbing
#define DOT11N_MODE_GI_20_NOP              0x00000
#define DOT11N_MODE_GI_20_LONG             0x10000
#define DOT11N_MODE_GI_20_SHORT            0x20000

//* dot11ac parms mode word
/*
 */
#define DOT11AC_MODE_EXTOFFSET_80_MASK      0x000c
#define DOT11AC_MODE_EXTOFFSET_80_NOP       0x0000
#define DOT11AC_MODE_EXTOFFSET_80_NONE      0x0004
#define DOT11AC_MODE_EXTOFFSET_80_MINUS1    0x0008
#define DOT11AC_MODE_EXTOFFSET_80_PLUS1     0x000c
//
#define DOT11AC_MODE_EXTOFFSET_160_MASK     0x00c0
#define DOT11AC_MODE_EXTOFFSET_160_NOP      0x0000
#define DOT11AC_MODE_EXTOFFSET_160_NONE     0x0040
#define DOT11AC_MODE_EXTOFFSET_160_MINUS1   0x0080
#define DOT11AC_MODE_EXTOFFSET_160_PLUS1    0x00c0
//
#define DOT11AC_MODE_GUARD_80_MASK          0x3000
#define DOT11AC_MODE_GUARD_80_NOP           0x0000
#define DOT11AC_MODE_GUARD_80_LONG          0x1000
#define DOT11AC_MODE_GUARD_80_SHORT         0x2000
//
#define DOT11AC_MODE_GUARD_160_MASK         0x0300
#define DOT11AC_MODE_GUARD_160_NOP          0x0000
#define DOT11AC_MODE_GUARD_160_LONG         0x0100
#define DOT11AC_MODE_GUARD_160_SHORT        0x0200
//
#define DOT11AC_MODE_ENABLE_MASK            0xc000
#define DOT11AC_MODE_ENABLE_NOP             0x0000
#define DOT11AC_MODE_ENABLE_OFF             0x4000
#define DOT11AC_MODE_ENABLE_ON              0x8000
//

//* old dot11ac parms mode word
//  (for backward compatibility)
/*
 */
#define DOT11AC_MODE_EXTOFFSET_MASK         DOT11AC_MODE_EXTOFFSET_80_MASK
#define DOT11AC_MODE_EXTOFFSET_NOP          DOT11AC_MODE_EXTOFFSET_80_NOP
#define DOT11AC_MODE_EXTOFFSET_NONE         DOT11AC_MODE_EXTOFFSET_80_NONE
#define DOT11AC_MODE_EXTOFFSET_MINUS1       DOT11AC_MODE_EXTOFFSET_80_MINUS1
#define DOT11AC_MODE_EXTOFFSET_PLUS1        DOT11AC_MODE_EXTOFFSET_80_PLUS1
//
#define DOT11AC_MODE_GUARD_MASK             DOT11AC_MODE_GUARD_80_MASK
#define DOT11AC_MODE_GUARD_NOP              DOT11AC_MODE_GUARD_80_NOP
#define DOT11AC_MODE_GUARD_LONG             DOT11AC_MODE_GUARD_80_LONG
#define DOT11AC_MODE_GUARD_SHORT            DOT11AC_MODE_GUARD_80_SHORT
//

//****************************************************************
// Radio load balance modes
//----------------------------------------------------------------
#define LOAD_BALANCE_OFF                    0
#define LOAD_BALANCE_ON                     1
#define LOAD_BALANCE_PASSIVE                1                       // load balance using selctive probe responses
#define LOAD_BALANCE_ACTIVE                 2                       // load balance using association response denials

//****************************************************************
// Multicast mode bits
//----------------------------------------------------------------
#define MULTICAST_STANDARD                  0                       // no multicast processing
#define MULTICAST_CONVERT                   1                       // convert multicast to unicast
#define MULTICAST_SNOOP                     2                       // convert multicast to unicast & snoop igmp multicast subscriptions
#define MULTICAST_PRUNE                     3                       // convert multicast to unicast & snoop igmp multicast subscriptions & prune multicasts that have no subscribers
#define MULTICAST_MASK                      0x0f                    // convert multicast to unicast & snoop igmp multicast subscriptions & prune multicasts that have no subscribers

//****************************************************************
// Multicast isolation mode bits
//----------------------------------------------------------------
#define ISOLATE_BY_NONE                     0                       // no multicast isolation
#define ISOLATE_BY_GROUP                    1                       // isolate multicasts by user-group
#define ISOLATE_BY_ARRAY                    2                       // isolate multicasts by associated stations
#define ISOLATE_BY_NEIGHBOR                 4                       // isolate multicasts by neighbor stations
#define ISOLATE_BY_FIXED_ADDR               8                       // isolate multicasts by fixed address list
#define ISOLATE_SHIFT                       4                       // isolate multicasts flags shift offset
#define ISOLATE_MASK                        0x0f                    // isolate multicasts flags mask

#define UNASSOCIATED                        -2                      // indicates aid is 0
#define ASSOC_NO_GROUP                      -1                      // indicates aid is non-zero, but no user group is assigned
#define ASSOC_NO_SSID                       -1                      // indicates aid is non-zero, but no ssid is assigned (for symmetry, shouldn't ever happen)

//****************************************************************
// Extract Flags
//----------------------------------------------------------------
#define EXTRACT_IPV4_ADDR                   0x01
#define EXTRACT_IPV6_ADDR                   0x02
#define EXTRACT_HOSTNAME                    0x04
#define EXTRACT_NETBIOS_NAME                0x08
#define EXTRACT_USER_AGENT_STR              0x10
#define EXTRACT_ALL                         0x1f
#define EXTRACT(type)                       (devs_info.extract_flags & EXTRACT_##type)

//****************************************************************
// Aggregate Types (passed in ssid)
//----------------------------------------------------------------
#define AGGREGATE_UNSPECIFIED               0
#define AGGREGATE_BEG_MONITOR_SCAN          1
#define AGGREGATE_END_MONITOR_SCAN          2
#define AGGREGATE_BEG_AUTOCHAN_SCAN         3
#define AGGREGATE_END_AUTOCHAN_SCAN         4
#define AGGREGATE_SET_AUTOCHAN_RESULT       5

//****************************************************************
// Radio Loopback test definitions
//----------------------------------------------------------------
#define RLB_TEST_DST                        { 0x11, 0x11, 0x11, 0x11, 0x11, 0x11 }
#define RLB_TEST_BSSID                      { 0x33, 0x33, 0x33, 0x33, 0x33, 0x33 }

//****************************************************************
// IAP device types
//----------------------------------------------------------------
#define IAP_DEV_REAL                        0
#define IAP_DEV_VIRT_L3ROAM                 1
#define IAP_DEV_VIRT_GRE                    2

//****************************************************************
// Aid definitions
//----------------------------------------------------------------
#define AID_MAX_COUNT                       2000                    // 2007 is absolute max (256 byte TIM IE less 5 bytes for TIM header = 251 bytes)
#define AID_BLOCK_SIZE                      16                      // aid's must be allocated in blocks of 16 (2 bytes) for correct tim bit control field operation
#define AID_BLOCKS(n)                       (n * AID_BLOCK_SIZE)    // helper to force allocation in blocks of 16 aids (2 bytes)
#define AID_BASE_OFFSET                     AID_BLOCKS(1)           // first aid block (16 aids) unused to avoid zero

#undef  CFG_VARIABLE_ASSOC_PER_IAP
#ifdef  CFG_VARIABLE_ASSOC_PER_IAP
#define AID_MAX_BLOCK_SIZE                  AID_BLOCKS(15)          // limit max number of stations to 240 in all cases
#define AID_MIN_BLOCK_SIZE                  AID_BLOCKS( 7)          // limit max number of stations to 112 for 16 or 12 radios
#define MAX_ASSOC_PER_IAP(_n)               (( (_n) <=  8) ? AID_MAX_BLOCK_SIZE : AID_MIN_BLOCK_SIZE) // variable
#define MAX_ASSOC_PER_ARRAY                 AID_MAX_COUNT                                             // use max aid count for cross platform compatibility
#else   // CFG_FIXED_ASSOC_PER_IAP
#define MAX_ASSOC_PER_IAP(_n)               AID_BLOCKS(15)                                            // fixed at 240
#define MAX_ASSOC_PER_ARRAY                 (MOM_IAPS * MAX_ASSOC_PER_IAP(0))                         // use maximum of maximums for cross platform compatibility
#endif  // CFG_ASSOC_PER_IAP

#define IAP_AID_OFFSET                      AID_BASE_OFFSET
#define INT_IAP_OFFSET                      0

#define MAX_ASSOC_PER_RADIO(_n)             MAX_ASSOC_PER_IAP(_n)   // max of iap and vap (iap should always be max)
#define MOM_ASSOC_PER_RADIO                 MAX_ASSOC_PER_IAP( 4)   // 4 -port has max association count when variable (same otherwise)
#define MIN_ASSOC_PER_RADIO                 MAX_ASSOC_PER_IAP(16)   // 16-port has min association count when variable (same otherwise)
#define OLD_ASSOC_PER_RADIO                 AID_BLOCKS(6)           // used to be fixed at 96
#define DFT_ASSOC_PER_RADIO                 AID_BLOCKS(4)           // matches max for xr500
#define MAX_ASSOC_PER_RADIO_11AC            195                     // hard limitation for atheros peregrine radio

#define CFG_PACKED_TIM_MAPPING
#ifdef  CFG_PACKED_TIM_MAPPING
#define SLOTS(_n)                           (((_n) == 12) ? 16 :  (_n))
#define OFFSET(_n)                          (((_n) ==  4) ?  2 : ((_n) == 2) ? 4 : 0)
#define RADIO_FROM_AID(_a, _n)              (((((((_a)                          - IAP_AID_OFFSET) / MAX_ASSOC_PER_IAP(_n)) + INT_IAP_OFFSET) * MOM_IAPS) / SLOTS(_n)) + OFFSET(_n))
#define AID_FROM_RADIO(_r, _n)              (((((( _r) * SLOTS(_n)) / MOM_IAPS) - INT_IAP_OFFSET) * MAX_ASSOC_PER_IAP(_n)) + IAP_AID_OFFSET)
#else   // CFG_FIXED_TIM_MAPPING
#define RADIO_FROM_AID(_a, _n)              ((((_a) - IAP_AID_OFFSET) / MAX_ASSOC_PER_IAP(_n)) + INT_IAP_OFFSET)
#define AID_FROM_RADIO(_r, _n)              ((((_r) - INT_IAP_OFFSET) * MAX_ASSOC_PER_IAP(_n)) + IAP_AID_OFFSET)
#endif  // CFG_TIM_MAPPING

#define MAX_TIM_BYTES                       ((MAX_ASSOC_PER_ARRAY + AID_BASE_OFFSET + 7) / 8)
#define AID_2ND_TIM_OFFSET                  (((AID_MAX_COUNT / MAX_ASSOC_PER_IAP(0)) * MAX_ASSOC_PER_IAP(0)) + AID_BASE_OFFSET)

#define   adjust_aid_to_air(_a, _p)         ({ int _aid=(_a); ((_p) && ((_a) > 0)) ? ((_aid - AID_BASE_OFFSET) % MAX_ASSOC_PER_IAP(0) + 1) : ((_aid > AID_2ND_TIM_OFFSET) ? (_aid - AID_2ND_TIM_OFFSET) : _aid); })
#define adjust_aid_from_air(_a, _r, _n, _p) ({ int _aid=(_a);  (_p)                ? (AID_FROM_RADIO(_r, _n) + _aid - 1)                   : ((AID_FROM_RADIO(_r, _n) > AID_2ND_TIM_OFFSET) ? (_aid + AID_2ND_TIM_OFFSET) : _aid); })

// skip this for now -- we are stretching our tim, but we never send the whole thing anyway ...
//#if    (MAX_ASSOC_PER_ARRAY > 2007)
//#error  MAX_ASSOC_PER_ARRAY cannot be greater than 2007
//#endif

//****************************************************************
// Distance / ack timeout calculation
//----------------------------------------------------------------
#define MILES_TO_USEC(miles)                ((((miles) * 54) + 5) / 10)     // 5.4 usec/mile

//****************************************************************
// XRP beacon definitions
//----------------------------------------------------------------
#define CFG_BEACON_XRP_OUI                  0x000f7d00              //beacon xrp ie oui
#define CFG_BEACON_XRP_IE                   249                     //beacon xrp ie number

//****************************************************************
// WDS MODE
//----------------------------------------------------------------
#define WDS_MODE_HOST                       0
#define WDS_MODE_CLIENT                     1
#define WDS_MODE_DISABLED                   2

#ifndef _XIRLLDP_H

//****************************************************************
// Radio to BSSID Mapping
//----------------------------------------------------------------
static inline int get_radio_from_mac(int num_slots, const u8 *mac, const u8 *UNUSED(base))
{
    switch (num_slots) {// internal addresses only
        case 16: return (((mac[5] >> 4) & 0xf)    );
        case 12: return (((mac[5] >> 4) & 0xf)    ); // same as 16
        case  8: return (((mac[5] >> 3) & 0xe)    );
        case  4: return (((mac[5] >> 2) & 0xc) + 2);
        case  2: return (((mac[5] >> 1) & 0x8) + 4);
        case  1: return (((mac[5] >> 0) & 0x0) + 4);
        default: return (0);
    }
}

static inline void set_mac_from_radio_ssid(int num_slots, u8 *mac, const u8 *base, int radio, int ssid)
{
    memcpy(mac, base, ETH_ALEN);

    if (!(radio < 0)) {
        switch (num_slots) {
            case 16:
            case 12: *(u32 *)&mac[2] += (u32)((radio < ((base[5] >> 4) & 0xf)) ? 0x100 : 0);  mac[5] = (u8)((mac[5] & ~0xf0) | ((radio & 0xf) << 4) | (ssid & 0xf)); break;
            case  8: *(u32 *)&mac[2] += (u32)((radio < ((base[5] >> 3) & 0xe)) ? 0x080 : 0);  mac[5] = (u8)((mac[5] & ~0x70) | ((radio & 0xe) << 3) | (ssid & 0xf)); break;
            case  4: *(u32 *)&mac[2] += (u32)((radio < ((base[5] >> 2) & 0xc)) ? 0x040 : 0);  mac[5] = (u8)((mac[5] & ~0x30) | ((radio & 0xc) << 2) | (ssid & 0xf)); break;
            case  2: *(u32 *)&mac[2] += (u32)((radio < ((base[5] >> 1) & 0x8)) ? 0x020 : 0);  mac[5] = (u8)((mac[5] & ~0x10) | ((radio & 0x8) << 1) | (ssid & 0xf)); break;
            case  1: *(u32 *)&mac[2] += (u32)((radio < ((base[5] >> 0) & 0x0)) ? 0x010 : 0);  mac[5] = (u8)((mac[5] & ~0x00) | ((radio & 0x0) << 0) | (ssid & 0xf)); break;
            default:  break;    // resolve warning for switch/no-default
        }
    }
}

#endif /* not defined _XIRLLDP_H */

//****************************************************************
// SSID DHCP Option 82
//----------------------------------------------------------------
#define SSID_DHCP_OPT_CKT_FMT_1             1  // Alcatel/Lucent format Circuit ID
#define SSID_DHCP_OPT_CKT_FMT_2             2  // Celcom format Circuit ID


#endif //XIRMAC_H
