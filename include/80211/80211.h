//* 80211.h
/**
 * @file 80211.h
 * @brief
 * @remarks
 */
#ifndef _80211_H
#define _80211_H

#ifdef XMAC_DRIVER
#include <linux/kernel.h>
#endif
#include <asm/byteorder.h>
#ifdef PROFILING
# include <asm-ppc/time.h>
# include <asm-ppc/timex.h>
#endif

//* logging & debugging defines & macros (aka pdebug & friends)
/**
 * @remarks the PDEBUGG macro facilitates quick disabling of individual debug messages, while still leaving
 * them in the code for future use.
 * @note to have debug messages displayed on the console you need to set the klogd console_loglevel to 8. if
 * klogd was started with the -c x option, where x is less than 8, then execute "echo 8 >
 * /proc/sys/kernel/printk" (if x was 8 then you're all set); or if you are not using klogd, or not using the
 * -c option, just execute "less /proc/kmsg" for the duration of your debugging session.  klogd seems to catch
 * more kernel messages when run wit the -s option, which causes it to capture messages via a system call,
 * rather than from proc/kmsg.
 * @note PDEBUGY is very slow, and can only be used *out* of interruot context
 */
#ifdef DEBUG_MSGS
# define debug_msgs_is_defined() 1
# ifdef __KERNEL__
#  define PDEBUG(fmt,args...)   printk(KERN_DEBUG "%s::%d: " fmt "\n", __FUNCTION__, __LINE__, ##args)
#  define PDEBUGY(fmt, args...) { yield(); printk(KERN_DEBUG "%s::%d: " fmt "\n", __FUNCTION__, __LINE__, ##args) ; }
# else
#  define PDEBUG(fmt, args...) printf("%s::%d: " fmt "\n", __FUNCTION__, __LINE__, ##args)
# endif
# define DEBUG_80211_dump(data) x80211_dump(data)
#else
# define debug_msgs_is_defined() 0
# define PDEBUG(fmt, args...)
# define PDEBUGY(fmt, args...)
# define DEBUG_80211_dump(data)
#endif
#define PDEBUGG(fmt, args...)


//* profiling defines & macros
/**
 * the profile macro is just like the pdebug macro, except it is defined for generating performance profiling data
 * get ptick counts at 33 MHz (@ 266 ddr, actually, ccb / 8, ie. 41 MHz for 333 ddr)
 */
#ifdef PROFILING
static inline long unsigned get_ptick(void) {
  return ((long unsigned)get_tbl());
}
# define PROFILE(fmt,args...)   printk(KERN_DEBUG "%s::%d: %08lu " fmt "\n", __FUNCTION__, __LINE__, get_ptick(), ##args)
#else
# define PROFILE(fmt,args...)
#endif


//* perror and pinfo
/**
 * @remarks messages that are "more than just informative" should be generated via PERROR - which is not only
 * for servere errors, but any exceptional condition about which we would want to be informed during
 * development; don't be shy about using perror - we'll probably get rid of all messages before the product
 * actually ships.
 * @note IMPORTANT: if a perror occurs during interrupt context, and DEBUG_ERRS_TO_CONSOLE is defined, you
 * WILL get a seg fault. for debugging, i guess that's ok - since you want to see if the condition occurred -
 * but otherwise set debugging off, or be prepared for some unpleasant crashes!
 */
#ifdef __KERNEL__
# ifdef DEBUG_ERRS_TO_CONSOLE
#  define PERROR(fmt, args...) printk(KERN_ERR "%s::%d DRIVER: " fmt "\n", __FUNCTION__, __LINE__, ##args)
# else
#  ifdef DEBUG_MSGS
#   define PERROR(fmt,args...)  printk(KERN_DEBUG "%s::%d ERROR: " fmt "\n", __FUNCTION__, __LINE__, ##args)
#  else
#   define PERROR(fmt,args...)  printk(KERN_DEBUG "[%010lu] %s::%d: " fmt "\n", jiffies, __FUNCTION__, __LINE__, ##args)
#  endif
# endif
#else
# define PERROR(fmt, args...) printf("Error: " fmt "\n", ##args)
#endif
#define PPERROR(fmt, args...)

#ifdef __KERNEL__
# define PINFO(fmt, args...) printk(KERN_INFO fmt "\n", ##args)
#else
# define PINFO(fmt, args...) printf("Info: " fmt "\n", ##args)
#endif


//* various helpful formats and strings
/**
 */
#define MODULE_NAME         "Xirrus"
#define IPSTRFMT            "%i.%i.%i.%i"
#define MACSTRFMT           "%02x:%02x:%02x:%02x:%02x:%02x"
#define WORDSFMT8           "%04x %04x %04x %04x %04x %04x %04x %04x"
#define BYTESFMT16          "%02x %02x %02x %02x %02x %02x %02x %02x - %02x %02x %02x %02x %02x %02x %02x %02x"
#ifndef BIT
#define BIT(x)              (1 << (x))
#endif
#define IPSTRBYTES(a)       (a)[0], (a)[1], (a)[2], (a)[3]
#define MACSTRBYTES(a)      (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#define MACSTRU64BYTES(a)   (a)[7], (a)[6], (a)[5], (a)[4], (a)[3], (a)[2]
#define WORDSDATA8(p)       (p)[0], (p)[1], (p)[2],  (p)[3],  (p)[4],  (p)[5],  (p)[6],  (p)[7]
#define BYTESDATA16(p)      (p)[0], (p)[1], (p)[2],  (p)[3],  (p)[4],  (p)[5],  (p)[6],  (p)[7], \
                            (p)[8], (p)[9], (p)[10], (p)[11], (p)[12], (p)[13], (p)[14], (p)[15]


//* mac_addr_to_u64
/**
 * @brief macro to convert a u8 mac_addr[6] to a u64 (upper 16 bits zeroed)
 * @note for this macro, the address in memory is considered as a little endian u64. thus the
 * first bit transmitted, bit 0 of the first byte, which is the multicast bit, is bit 0 of the
 * resultant u64.
 */
#define mac_addr_to_u64(a)           (__le64_to_cpu(*(u64 *)(a)) & 0xffffffffffffULL)
#define u64_to_mac_addr(a)           (__cpu_to_le64((a)) & 0xffffffffffff0000ULL)
#define mac_addr_u64_is_multicast(m) (m & 1)
#define broadcast_mac_addr_u64       0xffffffffffffULL

#define mac_addr_u64_is_broadcast(m) (((m) & broadcast_mac_addr_u64) == broadcast_mac_addr_u64)

//* miscellaneious 802.11 defines
/**
 */
#define MAX_CCK_RATE                 11
#define MAX_OFDM_RATE                54



//* 802.11 frame control defines
/**
 * @brief 802.11 frame control defines
 */
#define WLAN_FC_PVER               (BIT(1) | BIT(0))               //protocol version
#define WLAN_FC_TODS                BIT(8)
#define WLAN_FC_FROMDS              BIT(9)
#define WLAN_FC_MOREFRAG            BIT(10)
#define WLAN_FC_RETRY               BIT(11)
#define WLAN_FC_PWRMGT              BIT(12)
#define WLAN_FC_MOREDATA            BIT(13)
#define WLAN_FC_ISWEP               BIT(14)
#define WLAN_FC_ORDER               BIT(15)
#define WLAN_FC_GET_TYPE(fc)        (((fc)       &   (BIT(3) | BIT(2))) >> 2)
#define WLAN_FC_SET_TYPE(type)      ((type << 2) &   (BIT(3) | BIT(2)))
#define WLAN_FC_GET_STYPE(fc)       (((fc)       &   (BIT(7) | BIT(6) | BIT(5) | BIT(4))) >> 4)
#define WLAN_FC_SET_STYPE(type)     ((type << 4) &   (BIT(7) | BIT(6) | BIT(5) | BIT(4)))
#define WLAN_GET_SEQ_FRAG(seq)      ((seq)       &   (BIT(3) | BIT(2) | BIT(1) | BIT(0)))
#define WLAN_GET_SEQ_SEQ(seq)       (((seq)      & (~(BIT(3) | BIT(2) | BIT(1) | BIT(0)))) >> 4)
#define WLAN_FC_TYPE_MGMT 0
#define WLAN_FC_TYPE_CTRL 1
#define WLAN_FC_TYPE_DATA 2

#define packet_is_80211_management(fc)  (WLAN_FC_GET_TYPE(fc) == WLAN_FC_TYPE_MGMT)
#define packet_is_80211_beacon(fc)      (WLAN_FC_GET_TYPE(fc) == WLAN_FC_TYPE_MGMT && WLAN_FC_GET_STYPE(fc) == WLAN_FC_STYPE_BEACON)
#define packet_is_80211_control(fc)     (WLAN_FC_GET_TYPE(fc) == WLAN_FC_TYPE_CTRL)
#define packet_is_80211_data(fc)        (WLAN_FC_GET_TYPE(fc) == WLAN_FC_TYPE_DATA)

//* 802.11 frame control defines - management subtypes
#define WLAN_FC_STYPE_ASSOC_REQ      0
#define WLAN_FC_STYPE_ASSOC_RESP     1
#define WLAN_FC_STYPE_REASSOC_REQ    2
#define WLAN_FC_STYPE_REASSOC_RESP   3
#define WLAN_FC_STYPE_PROBE_REQ      4
#define WLAN_FC_STYPE_PROBE_RESP     5
#define WLAN_FC_STYPE_BEACON         8
#define WLAN_FC_STYPE_ATIM           9
#define WLAN_FC_STYPE_DISASSOC      10
#define WLAN_FC_STYPE_AUTH          11
#define WLAN_FC_STYPE_DEAUTH        12
// 802.11 frame control defines - control subtypes
#define WLAN_FC_STYPE_PSPOLL        10
#define WLAN_FC_STYPE_RTS           11
#define WLAN_FC_STYPE_CTS           12
#define WLAN_FC_STYPE_ACK           13
#define WLAN_FC_STYPE_CFEND         14
#define WLAN_FC_STYPE_CFENDACK      15
// 802.11 frame control defines - data subtypes
#define WLAN_FC_STYPE_DATA           0
#define WLAN_FC_STYPE_DATA_CFACK     1
#define WLAN_FC_STYPE_DATA_CFPOLL    2
#define WLAN_FC_STYPE_DATA_CFACKPOLL 3
#define WLAN_FC_STYPE_NULLFUNC       4
#define WLAN_FC_STYPE_CFACK          5
#define WLAN_FC_STYPE_CFPOLL         6
#define WLAN_FC_STYPE_CFACKPOLL      7
// 802.11 misc defines
#define MAX_SSID_LEN 32               //max length of ssid in ssid info elements
#define MAX_CHALL_LEN 253             //max length of challenge text in challenge text info elements
#define MAX_SUPP_RATES_LEN 8          //max number of rate-bytes in supportted rates info elements
#define BIG_WEP_KEY_LEN 13            //length of big wep keys
#define LITTLE_WEP_KEY_LEN 5          //length of small wep keys


//* struct to_ap_data_80211_hdr
/**
 * @brief 802.11 header for data frames to the access point
 */
struct to_ap_data_80211_hdr {
  u16 fc;                             //frame control
  u16 duration;                       //duration; for ps-poll frame this is assoc. id, bits 14,15 = 0.
  u8 ra[6];                           //receiver addr is ap mac addr = bssid
  u8 ta[6];                           //transmitter addr is sta mac addr
  u8 da[6];                           //destination addr is mac addr of destination
  u16 seq;
} __attribute__ ((packed));           //followed by frame data


//* struct from_ap_data_80211_hdr
/**
 * @brief 802.11 header for data frames from the access point
 */
struct from_ap_data_80211_hdr {
  u16 fc;                             //frame control
  u16 duration;                       //duration; for ps-poll frame this is assoc. id, bits 14,15 = 0.
  u8 ra[6];                           //receiver addr is sta mac addr
  u8 ta[6];                           //transmitter addr is ap mac addr = bssid
  u8 sa[6];                           //source addr is mac addr of source
  u16 seq;
} __attribute__ ((packed));           //followed by frame data


//* struct wds_data_80211_hdr
/**
 * @brief 802.11 header for wds (wireless bridge) data frames
 */
struct wds_data_80211_hdr {
  u16 fc;                             //frame control
  u16 duration;                       //duration; for ps-poll frame this is assoc. id, bits 14,15 = 0.
  u8 ra[6];                           //receiver addr is receiving ap mac addr/bssid
  u8 ta[6];                           //transmitter addr is transmitting ap mac addr/bssid
  u8 da[6];                           //destination addr is mac addr of wired destination
  u16 seq;
  u8 sa[6];                           //source addr is mac addr of wired source
} __attribute__ ((packed));           //followed by frame data


//* struct rts_frame
/**
 * @brief 802.11 header for management frames
 */
struct rts_frame {
  u16 fc;                             //frame control
  u16 duration;                       //duration of full frame exchange through 1st fragment
  u8 ra[6];                           //receiver address
  u8 ta[6];                           //transmitted address
} __attribute__ ((packed));


//* struct ack_frame
/**
 * @brief 802.11 header for management frames
 */
struct ack_frame {
  u16 fc;                             //frame control
  u16 duration;                       //duration
  u8 ra[6];                           //copied from tranmitted of the frame being acked
} __attribute__ ((packed));


//* struct wds_data_80211_hdr
/**
 * @brief 802.2 header for data frames (encapsulated in 802.11)
 */
struct data_8022_hdr {
  u8    dsap;                         //always 0xAA
  u8    ssap;                         //always 0xAA
  u8    ctrl;                         //always 0x03
  u8    oui[3];                       //organizational universal id: 00-00-00=1042, 00-00-f8=eth
  u16   type;                         //packet type 0800=ip, 0806=arp...
} __attribute__ ((packed));



//* struct ps_poll_80211_hdr
/**
 * @brief 802.11 header for power-save-poll control frame
 */
struct ps_poll_80211_hdr {
  u16 fc;                             //frame control
  u16 aid;                            //association id of sta, bits 14,15 = 1,1.
  u8 bssid[6];                        //receiver addr is ap mac addr = bssid
  u8 ta[6];                           //transmitter  addr is sta mac addr
} __attribute__ ((packed));           //followed by frame data


//* struct mgmt_80211_hdr
/**
 * @brief 802.11 header for management frames
 */
struct mgmt_80211_hdr {
  u16 fc;                             //frame control
  u16 duration;                       //duration; for ps-poll frame this is assoc. id, bits 14,15 = 0.
  u8 da[6];                           //destination addr is mac addr of destination (or multicast...)
  u8 sa[6];                           //source addr is mac addr of source
  u8 bssid[6];                        //bssid included in all mgmt frames (may duplicate da or sa)
  u16 seq;
} __attribute__ ((packed));           //followed by frame data


//* struct wep_iv_hdr
/**
 * @brief 802.11 wep initialization vector header
 */
struct wep_iv_hdr {
  u8 iv[3];                           //24 bit initialization vector, proper
  u8 key_id;                          //key id (bits 6, 7)
} __attribute__ ((packed));
#define WEP_IV_HDR_KEY_ID_MASK  0xc0
#define WEP_IV_HDR_KEY_ID_SHIFT 6


//* struct ssid_info_elem
/**
 * @brief ssid info element
 */
struct ssid_info_elem {
  u8 id;                              //zero byte prefixes the ssid
  u8 len;                             //length of ssid string
  u8 ssid[MAX_SSID_LEN];              //ascii string
} __attribute__ ((packed));           //


//* struct chall_info_elem
/**
 * @brief challenge info element
 */
struct chall_info_elem {
  u8 id;                              //challenge text info element prefixed with id=16
  u8 len;                             //length of challenge text
  u8 challenge[MAX_CHALL_LEN];        //variable length challenge text (up to 253 bytes)
} __attribute__ ((packed));           //


//* struct supp_rates_info_elem
/**
 * @brief supportted rates info element
 */
struct supp_rates_info_elem {
  u8 id;                              //supported rates info element prefixed with id=1
  u8 len;                             //number of indeividual rate bytes
  u8 rates[MAX_SUPP_RATES_LEN];       //1 byte for each rate: 2,4,11,22 = 1,2,5.5,11 Mbps
} __attribute__ ((packed));           //


//* struct tim_info_elem
/**
 * @brief tim element that is usually appended to each beacon
 */
struct tim_info_elem {
  u8 id;                              //tim info element prefixed with id=5
  u8 len;                             //length of element inc. partial virt. map
  u8 dtim_count;                      //how many beacons (inc this) b4 next dtim. 0->this is dtim
  u8 dtim_period;                     //how many tims between dtims >= 1
  u8 bitmap_ctl;                      //bit 0 -> aid 0 traffic, + 7 bits bitmap offset
  u8 bit_map[1];                      //1-251 bytes
} __attribute__ ((packed));           //followed by optional variable info elements



//* struct beacon_frame_body
/**
 * @brief frame body of a beacon (management frame)
 */
struct beacon_frame_body {
  u64 timestamp;                      //time since ap has been active in microseconds
  u16 beacon_int;                     //beacon interval in TU's (1024 uS)
  u16 capability;                     //capability info: ess/ibss, wep, hi-rate dsss-phy stuff, pcf etc
  struct ssid_info_elem ssid;
} __attribute__ ((packed));           //followed by optional variable info elements


//* struct probe_rsp_body
/**
 * @brief frame body of a probe response (management frame)
 */
struct probe_rsp_body {
  u64 timestamp;                      //time since ap has been active in microseconds
  u16 between_int;                    //beacon interval in TU's (1024 uS)
  u16 capability;                     //capability info: ess/ibss, wep, hi-rate dsss-phy stuff, pcf etc
  struct ssid_info_elem ssid;
} __attribute__ ((packed));           //followed by optional variable info elements
#define OPTIONAL_PROBE_RSP_BYTES 21   //optional probe resp data, 7 x fh parm, 2 x ds parm, 8 x cf parm, 4 x ibss parm

//* struct auth_frame_body
/**
 * @brief frame body of an authenticate (management frame)
 */
struct auth_frame_body {
  u16 algorithm;                      //0=open, 1=shared key wep, ...
  u16 seq;                            //authentication sequence number
  u16 status;                         //0=success...
  struct chall_info_elem challenge;
} __attribute__ ((packed));


//* struct assoc_req_frame_body
/**
 * @brief frame body of an authenticate (management frame)
 * @note in order to access the supp_rates fields you need to subtract (MAX_SSID_LEN - ssid.len).
 */
struct assoc_req_frame_body {
  u16 capability_info;                //ess/ibss, privacy, short preamble, pbcc, channel agility, cfp
  u16 listen_interval;                //how often sta will wake up, in beacon intervals
  struct ssid_info_elem ssid;
  struct supp_rates_info_elem supp_rates;
} __attribute__ ((packed));


//* struct assoc_rsp_frame_body
/**
 * @brief frame body of an association response (management frame)
 * @note in order to access the supp_rates fields you need to subtract (MAX_SSID_LEN - ssid.len).
 */
struct assoc_rsp_frame_body {
  u16 capability_info;                //ess/ibss, privacy, short preamble, pbcc, channel agility, cfp
  u16 status;                         //0=associated
  u16 aid;                            //association id 1-2007
  struct supp_rates_info_elem supp_rates;
} __attribute__ ((packed));


//* struct udp_hdr
/**
 * @brief udp header
 */
struct udp_hdr {
  u16 source;
  u16 dest;
  u16 len;
  u16 check;
};
#define IP_PROTOCOL_UDP       17      //ip header protocol value for udp


//* struct netbios_dgs
/**
 * @brief netbios datagram frame
 * @note NetBIOS names may contain characters which are not considered valid for use in DNS names, yet RFC 1001 and RFC 1002
 * attempted to map the NetBIOS name space into the DNS name space. To work around this conflict, NetBIOS names are encoded by
 * splitting each byte of the name into two nibbles and then adding the value of 'A' (0x41). Thus, the '&' character (0x26)
 * would be encoded as "CG". NetBIOS names are usually padded with spaces before being encoded.
 */
struct netbios_dgs {
  u8  packet_type;
  u8  flags;
  u16 datagram_id;
  u32 source_ip;
  u16 source_port;
  u16 datagram_length;
  u16 packet_offset;
  u8  source_length;                  //always 0x20, since name is space-padded
  u8  source_name[0x20];              //nibbles of each char split to 2 bytes, then 0x41 added to each byte
  u8  source_terminator;              //always 0
  u8  destination_length;             //
  u8  destination_name[0x20];         //
  u8  destination_terminator;         //
} __attribute__ ((packed));
#define NETBIOS_DATAGRAM_PORT 138     //udp port for netbios datagrms


//* x80211_dump
/**
 * @brief dump an 802.11 frame for debugging purposes
 * @param char *data - the frame data (including the 802.11 header)
 * @returns void
 */
extern void x80211_dump(char *data);
#endif //_80211_H
