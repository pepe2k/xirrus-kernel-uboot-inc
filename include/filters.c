//****************************************************************
// Filter display support routines
//----------------------------------------------------------------
#include <stdio.h>
#include "xircfg.h"
#include "filters.h"
#include "enumStr.h"

#ifdef XCFG_MODULE
extern struct vlan_info *vlan;
#else
extern struct vlan_cfg  *vlan;
#endif
extern int num_vlans;

struct filter_lookup {
    const char *name;
    int number;
};

struct filter_lookup protocol_table[] = {
    { "any",           0 },
    { "any-ip",      255 },
    { "icmp",          1 },
    { "igmp",          2 },
    { "tcp",           6 },
    { "udp",          17 },
    { "gre",          47 },
    { "esp",          50 },
    { "ah",           51 },
    { "icmpv6",       58 },
    { "l2tp",        115 },
    { "srp",         119 },
    { "sctp",        132 },
    { "arp",         254 },
    { "arp request", 253 },
    { "arp reply",   252 },
    { NULL,            0 }
};

struct filter_lookup port_table[] = {
    { "any",           0 },
    { "echo",          7 },
    { "discard",       9 },
    { "daytime",      13 },
    { "chargen",      19 },
    { "ftp-data",     20 },
    { "ftp",          21 },
    { "ssh",          22 },
    { "telnet",       23 },
    { "smtp",         25 },
    { "nameserver",   42 },
    { "whois",        43 },
    { "tacacs",       49 },
    { "dns",          53 },
    { "tacacs-ds",    65 },
    { "bootps",       67 },
    { "bootpc-dhcp",  68 },
    { "dhcp",         68 },
    { "tftp",         69 },
    { "gopher",       70 },
    { "finger",       79 },
    { "http",         80 },
    { "www",          80 },
    { "dnsix",        90 },
    { "hostname",    101 },
    { "pop2",        109 },
    { "pop3",        110 },
    { "sunrpc",      111 },
    { "uucp",        117 },
    { "nntp",        119 },
    { "ntp",         123 },
    { "netbios-ns",  137 },
    { "netbios-dgm", 138 },
    { "netbios-ssn", 139 },
    { "snmp",        161 },
    { "snmptrap",    162 },
    { "rsvd",        168 },
    { "xdmcp",       177 },
    { "bgp",         179 },
    { "irc",         194 },
    { "ipx",         213 },
    { "ldap",        389 },
    { "netware-ip",  396 },
    { "https",       443 },
    { "biff",        512 },
    { "who",         513 },
    { "syslog",      514 },
    { "lpd",         515 },
    { "talk",        517 },
    { "rip",         520 },
    { "klogin",      543 },
    { "kshell",      544 },
    { "xrp",       22610 },
    { NULL,            0 }
};

const char *filter_lookup(struct filter_lookup *table, int number)
{
    while (table->name && table->number != number)
        ++table;
    return (table->name);
}

const char *vlan_name(int vlan_num, const char *delim)
{
    static char buff[30];
    int i, len = 0;

    if (vlan_num == VLAN_NO_CHANGE) {
        len = sprintf(buff, "%s", "no-change");
    }
    else if (vlan_num == 0) {
        len = sprintf(buff, "%s", "none");
    }
    else {
        for (i = 0; i < num_vlans; ++i) {
            if (vlan_num == vlan[i].num) {
                len = sprintf(buff, "%s%s%s", delim, vlan[i].name, delim);
                break;
            }
        }
    }
    if (len == 0) {
        len = sprintf(buff, "%d", vlan_num);
    }
    return (buff);
}


//****************************************************************
// Display filter name & type
//----------------------------------------------------------------
int dsp_filter_name(char *buffer, struct filter_cfg *filter, const char *delim)
{
    return (sprintf(buffer, "%s%s%s", delim, filter->name, delim));
}

int dsp_filter_type(char *buffer, struct filter_cfg *filter, const char *delim)
{
    return (sprintf(buffer, "%s%s%s", (filter->type & FILTER_TYPE_ALLOW_OR_SET) ? "allow" : "deny",
                                      (filter->type & FILTER_TYPE_LOG         ) ?  delim  : ""    ,
                                      (filter->type & FILTER_TYPE_LOG         ) ? "log"   : ""    ));
}

int dsp_filter_allow(char *buffer, struct filter_cfg *filter)
{
    return (sprintf(buffer, "%s", (filter->type & FILTER_TYPE_ALLOW_OR_SET) ? "allow" : "deny"));
}

int dsp_filter_log(char *buffer, struct filter_cfg *filter, const char *on_str, const char *off_str)
{
    return sprintf(buffer, "%s", (filter->type & FILTER_TYPE_LOG) ? on_str : off_str);
}

int dsp_filter_layer(char *buffer, struct filter_cfg *filter)
{
    return (sprintf(buffer, "%d", is_layer2_filter(filter) ? 2 : 3));
}

//****************************************************************
// Display protocol or port name or number
//----------------------------------------------------------------
int dsp_filter_prot(char *buffer, struct filter_cfg *filter)
{
    const char *name = filter_lookup(protocol_table, filter->protocol);

    if (name) return (sprintf(buffer, "%s", name));
    else      return (sprintf(buffer, "%d", filter->protocol));
}

int dsp_filter_port(char *buffer, struct filter_cfg *filter)
{
    const char *name = filter_lookup(port_table, filter->port);

    if (filter->port_range) return (sprintf(buffer, "%d:%d", filter->port, filter->port_range));
    else if (name)          return (sprintf(buffer, "%s"   , name                            ));
    else                    return (sprintf(buffer, "%d"   , filter->port                    ));
}

//****************************************************************
// Display application or category name or number
//----------------------------------------------------------------
int dsp_filter_app(char *buffer, struct filter_cfg *filter, const char *prefix)
{
    return (sprintf(buffer, filter->app_index ? "%s%s" : "", prefix, filter->app_ident));
}

//****************************************************************
// Display source or destination address
//----------------------------------------------------------------
int dsp_filter_addr(char *buffer, struct filter_addr *filter, const char *allow_str, const char *deny_str, int width, const char *delim)
{
    const char *iface[] = {"iap", "wds client-link 1", "wds client-link 2", "wds client-link 3", "wds client-link 4", "wds all", "gig",
                                  "wds host-link ""1", "wds host-link ""2", "wds host-link ""3", "wds host-link ""4", "tunnel"         };
    int len = sprintf(buffer, "%s", (filter->type & FILTER_ADDR_NOT) ? deny_str : allow_str);

    switch (filter->type & ~FILTER_ADDR_NOT)
    {
        case FILTER_ADDR_ANY  :              len += sprintf(buffer + len, "any"                                                      ); break;
        case FILTER_ADDR_VLAN :              len += sprintf(buffer + len, "vlan %s"     ,   vlan_name(filter->addr.vlan,       delim)); break;
        case FILTER_ADDR_SSID :              len += sprintf(buffer + len, "ssid %s%s%s" ,      delim, filter->addr.ssid.name , delim ); break;
        case FILTER_ADDR_GROUP:              len += sprintf(buffer + len, "group %s%s%s",      delim, filter->addr.group.name, delim ); break;
        case FILTER_ADDR_IPV4 :              len += sprintf(buffer + len, "%-*s"        ,      width, filter->addr.ipv4.base         );
            if (filter->addr.ipv4.mask_bits) len += sprintf(buffer + len, "/%d"         ,             filter->addr.ipv4.mask_bits    ); break;
        case FILTER_ADDR_IPV6 :              len += sprintf(buffer + len, "%-*s"        ,      width, filter->addr.ipv6.base         );
            if (filter->addr.ipv6.mask_bits) len += sprintf(buffer + len, "/%d"         ,             filter->addr.ipv6.mask_bits    ); break;
        case FILTER_ADDR_MAC  :              len += sprintf(buffer + len, "%-*s"        ,      width, filter->addr.mac.base          );
            if (filter->addr.mac .mask_bits) len += sprintf(buffer + len, "/%d"         ,             filter->addr.mac.mask_bits     ); break;
        case FILTER_ADDR_IFACE:              len += sprintf(buffer + len, "iface %s"    ,       iface[filter->addr.iface ]           ); break;
        case FILTER_ADDR_DEVICE:             len += sprintf(buffer + len, "device %s"   , device_strs[filter->addr.device]           ); break;
    }
    return (len);
}

int dsp_filter_src(char *buffer, struct filter_cfg *filter, const char *allow_str, const char *deny_str, int width, const char *delim)
{
    return dsp_filter_addr(buffer, &filter->src, allow_str, deny_str, width, delim);
}

int dsp_filter_dst(char *buffer, struct filter_cfg *filter, const char *allow_str, const char *deny_str, int width, const char *delim)
{
    return dsp_filter_addr(buffer, &filter->dst, allow_str, deny_str, width, delim);
}


//****************************************************************
// Display additional filter action (set vlan or qos or dscp)
//----------------------------------------------------------------
int dsp_filter_set(char *buffer, struct filter_cfg *filter, const char * qos_str, const char *dscp_str, const char *vlan_str, const char *ip_str, const char *delim)
{
    return(is_set_qos_filter(filter) ? sprintf(buffer, "%s%d",  qos_str,             filter->type & FILTER_TYPE_SET_QOS_MASK) :
          is_set_dscp_filter(filter) ? sprintf(buffer, "%s%d", dscp_str,             filter->set_dscp                       ) :
          is_set_vlan_filter(filter) ? sprintf(buffer, "%s%s", vlan_str,   vlan_name(filter->vlan, delim)                   ) :
            is_set_ip_filter(filter) ? sprintf(buffer, "%s%s",   ip_str, set_ip_addr(filter)                                ) :
                                       sprintf(buffer, ""));
}

//****************************************************************
// Display filter traffic limits
//----------------------------------------------------------------
int dsp_fltr_lmt_cmd(char *buffer, struct filter_cfg *filter, const char *cmd_strs[], int cmd_width)
{
    const char *cmd_str = cmd_strs  ? cmd_strs[filter->limit_type & 0x7] : "";
    int value_width     = cmd_width ? cmd_width - strxlen(cmd_str) - 1   :  0;

    return (!(filter->limit_type ) ? sprintf(buffer, "%*s"  ,            cmd_width, ""                 ) :
            !(filter->limit_value) ? sprintf(buffer, "%s%*s", cmd_str, value_width, "unlimited"        ) :
                                     sprintf(buffer, "%s%*d", cmd_str, value_width, filter->limit_value));
}

int dsp_fltr_lmt_str(char *buffer, struct filter_cfg *filter, const char *units_strs[], int str_width)
{
    const char *unit_str = units_strs ? units_strs[filter->limit_type & 0x7] : "";
    int value_width      = str_width  ? str_width - strxlen(unit_str) - 1    :  0;

    return (!(filter->limit_type ) ? sprintf(buffer, "%*s",     str_width, ""                           ) :
            !(filter->limit_value) ? sprintf(buffer, "%*s%s", value_width, "unlimited"        , unit_str) :
                                     sprintf(buffer, "%*d%s", value_width, filter->limit_value, unit_str));
}

int dsp_fltr_lmt_unt(char *buffer, struct filter_cfg *filter, const char *units_strs[], int units_width)
{
    const char *units_str = units_strs ? units_strs[filter->limit_type & 0x7] : "";

    return (!(filter->limit_type) ? sprintf(buffer, "%-*s", units_width, ""       ) :
                                    sprintf(buffer, "%-*s", units_width, units_str));
}

int dsp_fltr_lmt_val(char *buffer, struct filter_cfg *filter, int value_width)
{
    return (!(filter->limit_type ) ? sprintf(buffer, "%*s", value_width, ""                 ) :
            !(filter->limit_value) ? sprintf(buffer, "%*s", value_width, "unlimited"        ) :
                                     sprintf(buffer, "%*d", value_width, filter->limit_value));
}

//****************************************************************
// Display filter time of day / day of week limits
//----------------------------------------------------------------
int dsp_filter_time(char *buffer, struct filter_cfg *filter, const char *sep_str, const char *cmd_str, int time_width)
{
    if (cmd_str && *cmd_str) {
        return ((!(filter->time_on < 0) && !(filter->time_off < 0)) ? sprintf(buffer, "%s %02d:%02d%s%02d:%02d%*s", cmd_str, filter->time_on/60, filter->time_on%60, sep_str, filter->time_off/60, filter->time_off%60, time_width ? time_width - (int)strxlen(cmd_str) - 12: 0, "") :
                (!(filter->time_on < 0)                           ) ? sprintf(buffer, "%s %02d:%02d"         "%*s", cmd_str, filter->time_on/60, filter->time_on%60,                                                    time_width ? time_width - (int)strxlen(cmd_str) -  6: 0, "") :
                                                                      sprintf(buffer,                        "%*s",                                                                                                                  time_width,                                 ""));
    }
    else {
        return ((!(filter->time_on < 0) && !(filter->time_off < 0)) ? sprintf(buffer,    "%02d:%02d%s%02d:%02d%*s",          filter->time_on/60, filter->time_on%60, sep_str, filter->time_off/60, filter->time_off%60, time_width ? time_width - 11: 0, "") :
                (!(filter->time_on < 0)                           ) ? sprintf(buffer,    "%02d:%02d"         "%*s",          filter->time_on/60, filter->time_on%60,                                                    time_width ? time_width -  5: 0, "") :
                                                                      sprintf(buffer,                        "%*s",                                                                                                                  time_width,         ""));
    }
}

int dsp_filter_days(char *buffer, struct filter_cfg *filter, const char *day_strs[], const char *cmd_str, int days_width)
{
    const char *day_str = day_strs[filter->days_on & 0x7f];

    if (cmd_str && *cmd_str) {
        return ((filter->days_on && filter->days_on != 0x7f) ? sprintf(buffer, "%s %s%*s", cmd_str, day_str, days_width ? days_width - (int)strxlen(cmd_str) - (int)strxlen(day_str) - 1 : 0, "") :
                                                               sprintf(buffer,      "%*s",                                days_width,                                                        ""));
    }
    else {
        return ((filter->days_on && filter->days_on != 0x7f) ? sprintf(buffer,    "%s%*s",          day_str, days_width ? days_width - (int)strxlen(day_str) : 0, "") :
                                                               sprintf(buffer,      "%*s",                                days_width,                             ""));
    }
}

//****************************************************************
// Display filter state (enable/disable or on/off)
//----------------------------------------------------------------
int dsp_filter_state(char *buffer, struct filter_cfg *filter, const char *on_str, const char *off_str)
{
    return (sprintf(buffer, "%s", (filter->type & FILTER_TYPE_DISABLE) ? off_str : on_str));
}

