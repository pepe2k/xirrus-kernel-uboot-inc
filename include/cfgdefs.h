//******************************************************************************
/** @file cfgdefs.h
 *
 * Deprecated cfg request defines
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 * @author  Eric K. Henderson
 * @date    11/7/2005
 *
 * This file contains a config module legacy constants not found in
 * xircfg.h, xiriap.h, and xiroid.h.
 *
 * PLEASE DO NOT ADD ANYTHING TO THIS FILE UNLESS IT IS NEEDED FOR LEGACY
 * SUPPORT.
 *
 * New OIDs should go in xiroid.h using the new naming conventions.
 * New WMI/CLI interface structures and defines belong in xircfg.h.
 * New iap/radio defines belong in xiriap.h
 **/
//------------------------------------------------------------------------------
#ifndef _CFGDEFS_H
#define _CFGDEFS_H
#include "xirver.h"
#include "xirmac.h"

#define HOMEDIR                     "/home/config"
#define DHCPD_SOCK_PATH             HOMEDIR"/dhcpd_sock"

#ifndef _XIRDHCP_H                  // above is all we want for dhcp

#include "xirsnmp.h"
#include <netinet/in.h>
#include <net/ethernet.h>

#define RAD_DIR_AD                  "radiusd_ad"                        // cfg
#define RAD_DIR_IN                  "radiusd_in"                        // cfg
#define RADIUSDIR_AD                "/home/" RAD_DIR_AD                 // cfg
#define RADIUSDIR_IN                "/home/" RAD_DIR_IN                 // cfg
#define DNS_CFG_FILE                "/etc/resolv.conf"                  // cfg
#define RADIUS_USERS_FILE           RADIUSDIR_IN "/users"               // cfg
#define PASSWORD_FILE               "/etc/passwd"                       // cfg
#define SHADOW_FILE                 "/etc/shadow"                       // cfg
#define HOSTNAME_CFG_FILE           "/etc/hostname"                     // cfg
#define INETD_CFG_FILE              "/etc/inetd.conf"                   // cfg
#define SERVICES_FILE               "/etc/services"                     // cfg
#define LOCAL_TIME_FILE             "/etc/localtime"                    // cfg
#define DHCP_SERVER_CFG_FILE        HOMEDIR"/dhcpd.conf"                // cfg
#define DHCP_SERVER_PID_FILE        "/var/run/dhcpd.pid"
#define NTP_CFG_FILE                HOMEDIR"/ntp.conf"                  // cfg
#define NTP_KEY_FILE                HOMEDIR"/ntp.key"                   // cfg
#define SYSLOG_CFG_FILE             HOMEDIR"/syslog.conf"               // cfg
#define SYSLOG_DUMMY_CFG_FILE       HOMEDIR"/syslog.dummy"              // cfg
#define HOSTAPD_SOCK1_PATH          HOMEDIR"/hostapd_sock1"
#define HOSTAPD_SOCK2_PATH          HOMEDIR"/hostapd_sock2"
#define HOSTAPD_SOCK3_PATH          HOMEDIR"/hostapd_sock3"
#define WPRD_SOCK_PATH              HOMEDIR"/wprd_sock"
#define WPRD_MDM_SOCK_PATH          HOMEDIR"/wprd_mdm_sock"
#define PROC_NET_DEV_PATH           "/proc/net/dev"                     // cfg
#define HOSTAPD_CFG_FILE            HOMEDIR"/hostapd.conf"              // cfg
#define HOSTAPD_TMP_FILE            HOMEDIR"/hostapd.temp"              // cfg
#define HOSTAPD_LOG_FILE            "/var/log/hostapd/hostapd.log"      // cfg
#define WPRD_CFG_FILE               HOMEDIR"/wprd.conf"                 // cfg
#define SNMP_ADDR_CACHE_FILE        "/var/log/snmpd/addrcache"
#define SNMP_PERSISTENT_CFG_FILE    "/var/net-snmp/snmpd.conf"          // cfg
#define SNMP_CFG_FILE               HOMEDIR"/snmpd.conf"                // cfg
#define SNMP_START_FILE             HOMEDIR"/snmpd.sh"                  // cfg
#define CFG_SNMPD_TRAP_SOCKET_PATH  HOMEDIR"/trap_sock"
#define CFG_LLDPD_SOCKET1_PATH      HOMEDIR"/lldpd_sock1"
#define CFG_LLDPD_SOCKET2_PATH      HOMEDIR"/lldpd_sock2"
#define CFG_LLDPD_SOCKET3_PATH      HOMEDIR"/lldpd_sock3"
#define OOPME_FIFO_READ             HOMEDIR"/oopme_fifo_read"
#define OOPME_FIFO_WRITE            HOMEDIR"/oopme_fifo_write"
#define CFG_SKIN_FILE               HOMEDIR"/cfg.skin"
#define SYSLOG_FILE_NAME            HOMEDIR"/log.txt"                   // cfg
#define SW_VERSION_FILE             HOMEDIR"/version.txt"               // cfg
#define SYSLOG_FILE_PIPE            HOMEDIR"/logfile"
#define SYSLOG_ARCHLOG_PIPE         HOMEDIR"/archlog.pipe"
#define SYSLOG_ARCHLOG_LEVEL        "debug"
#define SYSLOG_FILTER_PIPE          HOMEDIR"/filter.pipe"
#define SYSLOG_FILTER_LEVEL         "notice"
#define SYSLOG_STPMSG_PIPE          HOMEDIR"/stpmsg.pipe"
#define SYSLOG_STPMSG_ETH0_PIPE     HOMEDIR"/stpmsg-eth0.pipe"
#define SYSLOG_STPMSG_LEVEL         "notice"
#define SYSLOG_PERROR_PIPE          HOMEDIR"/perror.pipe"
#define SYSLOG_PERROR_LEVEL         "err"
#define UBOOT_ENV_READ              "/dev/xapenv"                       // cfg
#define UBOOT_ENV_WRITE             "/dev/xapenv"                       // cfg
#define TIME_ZONE_DIR               "/usr/share/zoneinfo"               // cfg
#define CONSOLE_PORT                "/dev/console"                      // cfg
#define BRCTL_LIST                  TMPDIR "/brctl.list"                // cfg
#define BRCTL_OUTPUT                TMPDIR "/brctl.out"                 // cfg
#define CFG_READY_FILE              HOMEDIR"/cfg_ready"                 // cfg
#define CFG_REQ_READY_FILE          HOMEDIR"/cfg_req_ready"             // cfg
#define SQUID_CFG_FILE              "/home/squid/squid.conf"            // cfg
#define SSHD_PATH                   "/usr/sbin/sshd"                    // cfg
#define SSHD_CFG_FILE               "/home/sshd/sshd_config"            // cfg
#define OOPME_LOCK_LOG              "/var/log/xapme-lock.log"

#define CLOUD_CFG_FILE_PATH        "/home/cloud/xms_c.conf"
#define XMS_E_CFG_FILE_PATH        "/home/cloud/xms_e.conf"
#define CLOUD_CLI_SOCK_PATH        "/home/cloud/xms_cli_sock"
#define CLOUD_TRAP_SOCK_PATH       HOMEDIR"/cloud_trap_sock"
#define CLOUD_START_SH             HOMEDIR"/start_cloud.sh"
#define XMS_CLIENT_LOG             "/var/log/xms.log"
#define ACTIVATION_LOG             "/var/log/activation.log"
#define XMS_E_CLI_SOCK_PATH        "/home/cloud/xms_cli_sock"
#define XMS_E_TRAP_SOCK_PATH       HOMEDIR"/xms_e_trap_sock"
#define WEB_PROXY_SOCK_PATH        HOMEDIR"/web_proxy_sock"
#define WEB_PROXY_CFG_SOCK         HOMEDIR"/web_proxy_cfg_sock"
#define WEB_PROXY_SH               HOMEDIR"/web-proxy.sh"
#define XMS_E_ACTIVE               "/home/cloud/xms_e.active"
#define XMS_C_ACTIVE               "/home/cloud/xms_c.active"
#define XMS_E_INACTIVE             "/home/cloud/xms_e.disabled"
#define XMS_C_INACTIVE             "/home/cloud/xms_c.disabled"
#define XMS_RESTART_FORCE          "/home/cloud/xms_restart"
#define AUTO_DISC_PATH             "/home/cloud/auto_discovery"


//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#ifndef put_console
#define put_console(fmt, args...)   ({char s[MAX_STR]; int l=snprintf(s, MAX_STR, "echo -n '" fmt "' >" CONSOLE_PORT, ##args); system(s); l++;})
#endif

//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define CFG_FILE_ID                 "Configuration"
#define CFG_FILE_ID_LEN             13
#define CFG_FILE_PREFIX_PRI_LEN     (CFG_FILE_ID_LEN + INT16_LEN + MD5_DIGEST_LENGTH)
#define CFG_FILE_PREFIX_SEC_LEN     (CFG_FILE_ID_LEN + INT32_LEN + MD5_DIGEST_LENGTH)

#define RC4_KEY_VALUE               "This is a default RC4 key"
#define RC4_KEY_LEN                 25

#define SHOW_NO_DEFAULTS            0       // cfg
#define SHOW_INC_DEFAULTS           1       // cfg

#define MAX_LEN_CFG_CMD             250     // cfg
#define MAX_LEN_FILTER_CMD          250     // cfg

#define INT8_LEN                    sizeof(int8)
#define INT16_LEN                   sizeof(int16)
#define INT32_LEN                   sizeof(int32)
#define INT64_LEN                   sizeof(int64)
#define FLOAT_LEN                   sizeof(float)
#define DOUBLE_LEN                  sizeof(double)

//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define SIOCETHTOOL                 0x8946

#define ETHTOOL_GSET                0x00000001
#define ETHTOOL_SSET                0x00000002
#define ETHTOOL_GLINK               0x0000000a

#define SUPPORTED_2500baseX_Full    (1 << 15)

#define MAX_NUM_ADMIN               32        // cfg
#define MAX_NUM_DHCP_POOL           16        // cfg
#define MAX_SIZE_DEF_PASSWD_FILE    1000      // cfg
#define MAX_SIZE_DEF_SHADOW_FILE    500       // cfg

#define TIMESTAMP_LEN               (15 + 1)
#define PRIORITY_BEG                (TIMESTAMP_LEN + 2)
#define PRIORITY_END                (PRIORITY_BEG  + 9)
#define MESSAGE_BEG                 (PRIORITY_END  + 2)
#define SYSLOG_MSG_LEN              (400 + 1)
#define SYSLOG_ENTRY_LEN            (TIMESTAMP_LEN + SYSLOG_MSG_LEN + 2 + 1)

#define NUM_RADIO_INTERFACES        20        // snmp
#define NUM_RADIO_DOT11A_INTERFACES 0         // snmp
#define NUM_RADIO_DOT11G_INTERFACES 20        // snmp

#define NUM_ETH_OBJECTS             10        // cfg

#define ADMIN_ID_OFFSET             0         // cfg
#define SSID_FLAGS_OFFSET           0         // cfg
#define SSID_NAME_OFFSET            2         // cfg

#define WDS_OFFSET_PASSWORD         5         // cfg
#define NUM_WDS_OBJECTS             8         // cfg

#define ALL_BOND                   -1         // cfg
#define BOND1                       1
#define BOND2                       2
#define BOND3                       3
#define BOND4                       4
#define BOND5                       5

#define ALL_ETH                    -1         // cfg
#define FAST_ETH                    0         //ETH_IF_FAST
#define GIG1_ETH                    1         //ETH_IF_GIG1
#define GIG2_ETH                    2         //ETH_IF_GIG2
#define GIG3_ETH                    3         //ETH_IF_GIG3
#define GIG4_ETH                    4         //ETH_IF_GIG4
#define GIG5_ETH                    5         //ETH_IF_GIG5

#define FAST_ETH_NAME               "eth0"    // cfg
#define GIG1_ETH_NAME               "br0"     // cfg

#define FAST_DEF_IPV4_ADDR          "10.0.1.1"      // cfg
#define GIG_DEF_IPV4_ADDR           "10.0.2.1"      // cfg
#define ETH_DEF_IPV4_MASK           "255.255.255.0" // cfg
#define AVAYA_DEF_IPV4_ADDR         "192.168.1.3"   // cfg

#define LOOPBACK_ADDR               "127.0.0.1"     // cfg

#define CFG_IPV4_DHCPCD_TIMEOUT     300  // cfg
#define CFG_IPV6_DHCPCD_TIMEOUT     300  // cfg
#define CFG_NTPDATE_TIMEOUT         300  // cfg

#define WDS_ALL                     -1               // cfg

#define LOCAL_RAD_SERVER_ADDR       LOOPBACK_ADDR    // cfg
#define LOCAL_RAD_SERVER_PORT       1812             // cfg
#define LOCAL_RAD_SERVER_SECRET     "xirrus"         // cfg

#define LOCAL_RAD_SERVER_ADDR_AD    LOOPBACK_ADDR    // cfg
#define LOCAL_RAD_SERVER_PORT_AD    1645             // cfg

#define SNMP_SYSTEM_OID_XS3500          "1.3.6.1.4.1.21013.1.100"   // cfg
#define SNMP_SYSTEM_OID_XS3700          "1.3.6.1.4.1.21013.1.101"   // cfg
#define SNMP_SYSTEM_OID_XS3900          "1.3.6.1.4.1.21013.1.102"   // cfg

#define SNMP_SYSTEM_OID_XS3500_512MB    "1.3.6.1.4.1.21013.1.103"   // cfg
#define SNMP_SYSTEM_OID_XS3700_1GB      "1.3.6.1.4.1.21013.1.104"   // cfg
#define SNMP_SYSTEM_OID_XS3900_1GB      "1.3.6.1.4.1.21013.1.105"   // cfg

#define SNMP_SYSTEM_OID_XS4             "1.3.6.1.4.1.21013.1.106"   // cfg
#define SNMP_SYSTEM_OID_XS8             "1.3.6.1.4.1.21013.1.107"   // cfg
#define SNMP_SYSTEM_OID_XS16            "1.3.6.1.4.1.21013.1.108"   // cfg

#define SNMP_SYSTEM_OID_XN4             "1.3.6.1.4.1.21013.1.109"   // cfg
#define SNMP_SYSTEM_OID_XN8             "1.3.6.1.4.1.21013.1.110"   // cfg
#define SNMP_SYSTEM_OID_XN16            "1.3.6.1.4.1.21013.1.111"   // cfg

#define SNMP_SYSTEM_OID_XS12            "1.3.6.1.4.1.21013.1.112"   // cfg
#define SNMP_SYSTEM_OID_XN12            "1.3.6.1.4.1.21013.1.113"   // cfg

#define SNMP_SYSTEM_OID_XR4420          "1.3.6.1.4.1.21013.1.114"   // cfg
#define SNMP_SYSTEM_OID_XR4430          "1.3.6.1.4.1.21013.1.115"   // cfg
#define SNMP_SYSTEM_OID_XR4820          "1.3.6.1.4.1.21013.1.116"   // cfg
#define SNMP_SYSTEM_OID_XR4830          "1.3.6.1.4.1.21013.1.117"   // cfg

#define SNMP_SYSTEM_OID_XR6820          "1.3.6.1.4.1.21013.1.118"   // cfg
#define SNMP_SYSTEM_OID_XR6830          "1.3.6.1.4.1.21013.1.119"   // cfg
#define SNMP_SYSTEM_OID_XR7220          "1.3.6.1.4.1.21013.1.120"   // cfg
#define SNMP_SYSTEM_OID_XR7230          "1.3.6.1.4.1.21013.1.121"   // cfg
#define SNMP_SYSTEM_OID_XR7620          "1.3.6.1.4.1.21013.1.122"   // cfg
#define SNMP_SYSTEM_OID_XR7630          "1.3.6.1.4.1.21013.1.123"   // cfg

#define SNMP_SYSTEM_OID_XR1220          "1.3.6.1.4.1.21013.1.124"   // cfg
#define SNMP_SYSTEM_OID_XR1230          "1.3.6.1.4.1.21013.1.125"   // cfg

#define SNMP_SYSTEM_OID_XR2420          "1.3.6.1.4.1.21013.1.126"   // cfg
#define SNMP_SYSTEM_OID_XR2430          "1.3.6.1.4.1.21013.1.127"   // cfg
#define SNMP_SYSTEM_OID_XR2220          "1.3.6.1.4.1.21013.1.128"   // cfg
#define SNMP_SYSTEM_OID_XR2230          "1.3.6.1.4.1.21013.1.129"   // cfg

#define SNMP_SYSTEM_OID_XR1120          "1.3.6.1.4.1.21013.1.130"   // cfg
#define SNMP_SYSTEM_OID_XR1130          "1.3.6.1.4.1.21013.1.131"   // cfg

#define SNMP_SYSTEM_OID_XR1120H         "1.3.6.1.4.1.21013.1.132"   // cfg
#define SNMP_SYSTEM_OID_XR1130H         "1.3.6.1.4.1.21013.1.133"   // cfg
#define SNMP_SYSTEM_OID_XR520H          "1.3.6.1.4.1.21013.1.134"   // cfg
#define SNMP_SYSTEM_OID_XR1230H         "1.3.6.1.4.1.21013.1.135"   // cfg

#define SNMP_SYSTEM_OID_XR2420H         "1.3.6.1.4.1.21013.1.136"   // cfg
#define SNMP_SYSTEM_OID_XR2430H         "1.3.6.1.4.1.21013.1.137"   // cfg
#define SNMP_SYSTEM_OID_XR2220H         "1.3.6.1.4.1.21013.1.138"   // cfg
#define SNMP_SYSTEM_OID_XR2230H         "1.3.6.1.4.1.21013.1.139"   // cfg

#define SNMP_SYSTEM_OID_XR420           "1.3.6.1.4.1.21013.1.140"   // cfg
#define SNMP_SYSTEM_OID_XR430           "1.3.6.1.4.1.21013.1.141"   // cfg
#define SNMP_SYSTEM_OID_XR520           "1.3.6.1.4.1.21013.1.142"   // cfg
#define SNMP_SYSTEM_OID_XR530           "1.3.6.1.4.1.21013.1.143"   // cfg
#define SNMP_SYSTEM_OID_XR1220H         "1.3.6.1.4.1.21013.1.144"   // cfg
#define SNMP_SYSTEM_OID_XR530H          "1.3.6.1.4.1.21013.1.145"   // cfg
#define SNMP_SYSTEM_OID_XR420H          "1.3.6.1.4.1.21013.1.146"   // cfg
#define SNMP_SYSTEM_OID_XR430H          "1.3.6.1.4.1.21013.1.147"   // cfg
#define SNMP_SYSTEM_OID_XR2425          "1.3.6.1.4.1.21013.1.148"   // cfg
#define SNMP_SYSTEM_OID_XR2435          "1.3.6.1.4.1.21013.1.149"   // cfg
#define SNMP_SYSTEM_OID_XR2425H         "1.3.6.1.4.1.21013.1.150"   // cfg
#define SNMP_SYSTEM_OID_XR2225          "1.3.6.1.4.1.21013.1.151"   // cfg
#define SNMP_SYSTEM_OID_XR2235          "1.3.6.1.4.1.21013.1.152"   // cfg

#define SNMP_SYSTEM_OID_XR620           "1.3.6.1.4.1.21013.1.153"   // cfg
#define SNMP_SYSTEM_OID_XR620H          "1.3.6.1.4.1.21013.1.154"   // cfg
#define SNMP_SYSTEM_OID_XR630           "1.3.6.1.4.1.21013.1.155"   // cfg
#define SNMP_SYSTEM_OID_XR2426          "1.3.6.1.4.1.21013.1.156"   // cfg
#define SNMP_SYSTEM_OID_XR2436          "1.3.6.1.4.1.21013.1.157"   // cfg
#define SNMP_SYSTEM_OID_XR2426H         "1.3.6.1.4.1.21013.1.158"   // cfg
#define SNMP_SYSTEM_OID_XR2226          "1.3.6.1.4.1.21013.1.159"   // cfg
#define SNMP_SYSTEM_OID_XR2226H         "1.3.6.1.4.1.21013.1.160"   // cfg
#define SNMP_SYSTEM_OID_XR2236          "1.3.6.1.4.1.21013.1.161"   // cfg

#define SNMP_SYSTEM_OID_XR630H          "1.3.6.1.4.1.21013.1.162"   // cfg

#define SNMP_SYSTEM_OID_XR2225H         "1.3.6.1.4.1.21013.1.163"   // cfg
#define SNMP_SYSTEM_OID_XR2235H         "1.3.6.1.4.1.21013.1.164"   // cfg
#define SNMP_SYSTEM_OID_XR2236H         "1.3.6.1.4.1.21013.1.165"   // cfg
#define SNMP_SYSTEM_OID_XR2435H         "1.3.6.1.4.1.21013.1.166"   // cfg
#define SNMP_SYSTEM_OID_XR2436H         "1.3.6.1.4.1.21013.1.167"   // cfg

#define SNMP_SYSTEM_OID_XR1126          "1.3.6.1.4.1.21013.1.168"   // cfg
#define SNMP_SYSTEM_OID_XR1136          "1.3.6.1.4.1.21013.1.169"   // cfg
#define SNMP_SYSTEM_OID_XR1226          "1.3.6.1.4.1.21013.1.170"   // cfg
#define SNMP_SYSTEM_OID_XR1236          "1.3.6.1.4.1.21013.1.171"   // cfg

#define SNMP_SYSTEM_OID_XR1126H         "1.3.6.1.4.1.21013.1.172"   // cfg
#define SNMP_SYSTEM_OID_XR1136H         "1.3.6.1.4.1.21013.1.173"   // cfg
#define SNMP_SYSTEM_OID_XR1226H         "1.3.6.1.4.1.21013.1.174"   // cfg
#define SNMP_SYSTEM_OID_XR1236H         "1.3.6.1.4.1.21013.1.175"   // cfg

#define SNMP_SYSTEM_OID_XR4426          "1.3.6.1.4.1.21013.1.176"   // cfg
#define SNMP_SYSTEM_OID_XR4436          "1.3.6.1.4.1.21013.1.177"   // cfg
#define SNMP_SYSTEM_OID_XR4826          "1.3.6.1.4.1.21013.1.178"   // cfg
#define SNMP_SYSTEM_OID_XR4836          "1.3.6.1.4.1.21013.1.179"   // cfg

#define SNMP_SYSTEM_OID_XR6826          "1.3.6.1.4.1.21013.1.180"   // cfg
#define SNMP_SYSTEM_OID_XR6836          "1.3.6.1.4.1.21013.1.181"   // cfg
#define SNMP_SYSTEM_OID_XR7226          "1.3.6.1.4.1.21013.1.182"   // cfg
#define SNMP_SYSTEM_OID_XR7236          "1.3.6.1.4.1.21013.1.183"   // cfg
#define SNMP_SYSTEM_OID_XR7626          "1.3.6.1.4.1.21013.1.184"   // cfg
#define SNMP_SYSTEM_OID_XR7636          "1.3.6.1.4.1.21013.1.185"   // cfg

#define SNMP_SYSTEM_OID_XD1_130         "1.3.6.1.4.1.21013.1.186"   // cfg
#define SNMP_SYSTEM_OID_XD2_130         "1.3.6.1.4.1.21013.1.187"   // cfg
#define SNMP_SYSTEM_OID_XD4_130         "1.3.6.1.4.1.21013.1.188"   // cfg
#define SNMP_SYSTEM_OID_XD8_130         "1.3.6.1.4.1.21013.1.189"   // cfg

#define SNMP_SYSTEM_OID_XH1_130         "1.3.6.1.4.1.21013.1.190"   // cfg
#define SNMP_SYSTEM_OID_XH2_130         "1.3.6.1.4.1.21013.1.191"   // cfg
#define SNMP_SYSTEM_OID_XH4_130         "1.3.6.1.4.1.21013.1.192"   // cfg
#define SNMP_SYSTEM_OID_XH8_130         "1.3.6.1.4.1.21013.1.193"   // cfg

#define SNMP_SYSTEM_OID_XD1_240         "1.3.6.1.4.1.21013.1.194"   // cfg
#define SNMP_SYSTEM_OID_XD2_240         "1.3.6.1.4.1.21013.1.195"   // cfg
#define SNMP_SYSTEM_OID_XD4_240         "1.3.6.1.4.1.21013.1.196"   // cfg
#define SNMP_SYSTEM_OID_XD8_240         "1.3.6.1.4.1.21013.1.197"   // cfg

#define SNMP_SYSTEM_OID_XH1_240         "1.3.6.1.4.1.21013.1.198"   // cfg
#define SNMP_SYSTEM_OID_XH2_240         "1.3.6.1.4.1.21013.1.199"   // cfg
#define SNMP_SYSTEM_OID_XH4_240         "1.3.6.1.4.1.21013.1.200"   // cfg
#define SNMP_SYSTEM_OID_XH8_240         "1.3.6.1.4.1.21013.1.201"   // cfg

#define SNMP_SYSTEM_OID_XD1_120         "1.3.6.1.4.1.21013.1.202"   // cfg
#define SNMP_SYSTEM_OID_XD2_120         "1.3.6.1.4.1.21013.1.203"   // cfg
#define SNMP_SYSTEM_OID_XD4_120         "1.3.6.1.4.1.21013.1.204"   // cfg
#define SNMP_SYSTEM_OID_XD8_120         "1.3.6.1.4.1.21013.1.205"   // cfg

#define SNMP_SYSTEM_OID_XH1_120         "1.3.6.1.4.1.21013.1.206"   // cfg
#define SNMP_SYSTEM_OID_XH2_120         "1.3.6.1.4.1.21013.1.207"   // cfg
#define SNMP_SYSTEM_OID_XH4_120         "1.3.6.1.4.1.21013.1.208"   // cfg
#define SNMP_SYSTEM_OID_XH8_120         "1.3.6.1.4.1.21013.1.209"   // cfg

#define SNMP_SYSTEM_OID_XR1147          "1.3.6.1.4.1.21013.1.210"   // cfg
#define SNMP_SYSTEM_OID_XR1247          "1.3.6.1.4.1.21013.1.211"   // cfg
#define SNMP_SYSTEM_OID_XR2247          "1.3.6.1.4.1.21013.1.212"   // cfg
#define SNMP_SYSTEM_OID_XR2447          "1.3.6.1.4.1.21013.1.213"   // cfg

#define SNMP_SYSTEM_OID_XR1147H         "1.3.6.1.4.1.21013.1.214"   // cfg
#define SNMP_SYSTEM_OID_XR1247H         "1.3.6.1.4.1.21013.1.215"   // cfg
#define SNMP_SYSTEM_OID_XR2247H         "1.3.6.1.4.1.21013.1.216"   // cfg
#define SNMP_SYSTEM_OID_XR2447H         "1.3.6.1.4.1.21013.1.217"   // cfg

#define SNMP_SYSTEM_OID_XR4447          "1.3.6.1.4.1.21013.1.218"   // cfg
#define SNMP_SYSTEM_OID_XR4847          "1.3.6.1.4.1.21013.1.219"   // cfg
#define SNMP_SYSTEM_OID_XR6847          "1.3.6.1.4.1.21013.1.220"   // cfg
#define SNMP_SYSTEM_OID_XR7247          "1.3.6.1.4.1.21013.1.221"   // cfg
#define SNMP_SYSTEM_OID_XR7647          "1.3.6.1.4.1.21013.1.222"   // cfg

#define SNMP_SYSTEM_OID_XA1_120         "1.3.6.1.4.1.21013.1.223"   // cfg
#define SNMP_SYSTEM_OID_XA2_120         "1.3.6.1.4.1.21013.1.224"   // cfg
#define SNMP_SYSTEM_OID_XA4_120         "1.3.6.1.4.1.21013.1.225"   // cfg
#define SNMP_SYSTEM_OID_XA8_120         "1.3.6.1.4.1.21013.1.226"   // cfg

#define SNMP_SYSTEM_OID_XA1_130         "1.3.6.1.4.1.21013.1.227"   // cfg
#define SNMP_SYSTEM_OID_XA2_130         "1.3.6.1.4.1.21013.1.228"   // cfg
#define SNMP_SYSTEM_OID_XA4_130         "1.3.6.1.4.1.21013.1.229"   // cfg
#define SNMP_SYSTEM_OID_XA8_130         "1.3.6.1.4.1.21013.1.230"   // cfg

#define SNMP_SYSTEM_OID_XA1_240         "1.3.6.1.4.1.21013.1.231"   // cfg
#define SNMP_SYSTEM_OID_XA2_240         "1.3.6.1.4.1.21013.1.232"   // cfg
#define SNMP_SYSTEM_OID_XA4_240         "1.3.6.1.4.1.21013.1.233"   // cfg
#define SNMP_SYSTEM_OID_XA8_240         "1.3.6.1.4.1.21013.1.234"   // cfg

#define SNMP_SYSTEM_OID_XD1_230         "1.3.6.1.4.1.21013.1.235"   // cfg
#define SNMP_SYSTEM_OID_XD2_230         "1.3.6.1.4.1.21013.1.236"   // cfg
#define SNMP_SYSTEM_OID_XD3_230         "1.3.6.1.4.1.21013.1.237"   // cfg
#define SNMP_SYSTEM_OID_XD4_230         "1.3.6.1.4.1.21013.1.238"   // cfg
#define SNMP_SYSTEM_OID_XD8_230         "1.3.6.1.4.1.21013.1.239"   // cfg

#define SNMP_SYSTEM_OID_XH1_230         "1.3.6.1.4.1.21013.1.240"   // cfg
#define SNMP_SYSTEM_OID_XH2_230         "1.3.6.1.4.1.21013.1.241"   // cfg
#define SNMP_SYSTEM_OID_XH3_230         "1.3.6.1.4.1.21013.1.242"   // cfg
#define SNMP_SYSTEM_OID_XH4_230         "1.3.6.1.4.1.21013.1.243"   // cfg
#define SNMP_SYSTEM_OID_XH8_230         "1.3.6.1.4.1.21013.1.244"   // cfg

#define SNMP_SYSTEM_OID_XA1_230         "1.3.6.1.4.1.21013.1.245"   // cfg
#define SNMP_SYSTEM_OID_XA2_230         "1.3.6.1.4.1.21013.1.246"   // cfg
#define SNMP_SYSTEM_OID_XA3_230         "1.3.6.1.4.1.21013.1.247"   // cfg
#define SNMP_SYSTEM_OID_XA4_230         "1.3.6.1.4.1.21013.1.248"   // cfg
#define SNMP_SYSTEM_OID_XA8_230         "1.3.6.1.4.1.21013.1.249"   // cfg

#define SNMP_SYSTEM_OID_XA3_240         "1.3.6.1.4.1.21013.1.250"   // cfg
#define SNMP_SYSTEM_OID_XH3_240         "1.3.6.1.4.1.21013.1.251"   // cfg
#define SNMP_SYSTEM_OID_XD3_240         "1.3.6.1.4.1.21013.1.252"   // cfg

// Avaya systems
#define SNMP_SYSTEM_OID_WAP9122         "1.3.6.1.4.1.45.8.1.1000"
#define SNMP_SYSTEM_OID_WAP9123         "1.3.6.1.4.1.45.8.1.1001"
#define SNMP_SYSTEM_OID_WAP9132         "1.3.6.1.4.1.45.8.1.1002"
#define SNMP_SYSTEM_OID_WAP9133         "1.3.6.1.4.1.45.8.1.1003"
#define SNMP_SYSTEM_OID_WAO9122         "1.3.6.1.4.1.45.8.1.1004"
#define SNMP_SYSTEM_OID_WAP9172         "1.3.6.1.4.1.45.8.1.1005"
#define SNMP_SYSTEM_OID_WAP9173         "1.3.6.1.4.1.45.8.1.1006"
#define SNMP_SYSTEM_OID_WAO9132         "1.3.6.1.4.1.45.8.1.1007"
#define SNMP_SYSTEM_OID_WAE9132         "1.3.6.1.4.1.45.8.1.1008"
#define SNMP_SYSTEM_OID_WAP9144         "1.3.6.1.4.1.45.8.1.1009"

#define BAUDRATES                   300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200
#define DEF_BAUDRATE                115200  // cfg

//---------------------------------------------------------
// cfg
//---------------------------------------------------------
#define INT8                        1
#define INT16                       2
#define INT32                       3
#define INT64                       4
#define UINT8                       5
#define UINT16                      6
#define UINT32                      7
#define UINT64                      8
#define STR                         9
#define LABEL_STR                   10
#define MAC                         11
#define RATES                       12
#define CHAN                        13
#define SSID                        14
#define DHCP_POOL                   15
#define WPA_REKEY                   16
#define VLAN                        17
#define TIME                        18
#define TRAFFIC                     19
#define TOUT8                       20
#define TOUT16                      21
#define TOUT32                      22
#define GROUP                       23
#define CHAN_SET                    24
#define RATES_11N                   25
#define RATES_11AC                  26
#define IAP_MASK                    27
#define MEM                         28
#define MAX_STA                     29
#define DATE                        30      // from personal-wifi
#define VLAN_LIST                   31      // from vlan-pooling
#define FLOAT                       32      // from position-info
#define DOUBLE                      33      // from position-info

#define hex_char_add(c, n)  (((c) <= '9') && ((c)+(n) > '9') ? ((c)+(n)-('9'+1)+'a') : ((c) + (n)))

//---------------------------------------------------------
// Cfg
//---------------------------------------------------------
#define IOCTL_CMD_GET_NO_DEVS       25      // used to get num_slots during init_cfg()
#define IOCTL_CMD_SET_COUNTRY_CODE  68      // set country code in atmel SCD
#define IOCTL_CMD_GET_COUNTRY_CODE  69      // get country code from atmel SCD

struct drv_cmd_header
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
} __attribute__ ((packed));

struct drv_gen_word16_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    uint16 value;
} __attribute__ ((packed));

struct drv_gen_word32_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    uint32 value;
} __attribute__ ((packed));

struct drv_get_version_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;                                          //for get_version cmd
    char   driver_version_string[48];
    uint16 floyd_version;
    uint16 opie_version;
    uint16 andy_version;
    uint16 rincon_version;
    uint16 mugu_version;
    uint16 laguna_version;
    uint16 topanga_version;
    uint16 avalon_version;
    uint8  scd_maj_rev;
    uint8  scd_min_rev;
    uint8  scd_month;
    uint8  scd_year;
} __attribute__ ((packed));

struct drv_get_system_status_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    uint8  no_radios_configured;
    uint8  no_radios_found;
    uint8  no_radios_failed;
    uint8  err_mask;
} __attribute__ ((packed));

struct drv_set_vlan_table_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    uint16 value[XIR_VLAN_TABLE_NUM];
} __attribute__ ((packed));

struct drv_set_mdns_filter_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    char   filter[MAX_NUM_MDNS_FILTER][REQ_LEN_MDNS_FILTER];
} __attribute__ ((packed));

struct drv_set_app_names_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    char   app_names[MAX_DPI_APP_INDEX][MAX_DPI_APP_NAME];
} __attribute__ ((packed));

struct drv_vlan_app_stats_cmd
{
    uint16 cmd;
    uint8  vlan;
    uint8  type;
    uint16 len;
    struct dpi_stats_info *info;
} __attribute__ ((packed));

struct drv_vlan_cat_stats_cmd
{
    uint16 cmd;
    uint8  vlan;
    uint8  type;
    uint16 len;
    struct dpi_stats_info cat_stats[MAX_DPI_CAT_INDEX];
} __attribute__ ((packed));

struct drv_ssid_app_stats_cmd
{
    uint16 cmd;
    uint8  type;
    uint8  ssid;
    uint16 len;
    struct dpi_stats_info *info;
} __attribute__ ((packed));

struct drv_ssid_cat_stats_cmd
{
    uint16 cmd;
    uint8  type;
    uint8  ssid;
    uint16 len;
    struct dpi_stats_info cat_stats[MAX_DPI_CAT_INDEX];
} __attribute__ ((packed));

struct drv_set_hotspot_cfg_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    uint8  interworking;
    uint8  an_opts;
    uint8  hessid[6];
} __attribute__ ((packed));

struct drv_set_ids_cfg_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    uint8  enabled;
    struct ids_attack_cfg attacks;
} __attribute__ ((packed));

struct drv_set_hp_bcast_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    uint16 num_ssid;
    char ssids[MAX_SIZE_HONEYPOT_BCAST][REQ_LEN_SSID + 2];
} __attribute__ ((packed));

struct drv_license_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
    uint8  value[32];
} __attribute__ ((packed));

struct drv_get_wifino_core_cmd
{
    uint16 cmd;
    uint8  iface;
    uint8  ssid;
    uint16 len;
     int8  wifino[16];
     int8  core[16];
} __attribute__ ((packed));

struct drv_set_fixed_addr_table_cmd
{
    uint16 cmd;
    uint8  vlan;
    uint8  type;
    uint16 len;
    uint32 num_entries;
    struct sta_rssi_entry *table;
} __attribute__ ((packed));

struct ethtool_cmd
{
    uint32 cmd;
    uint32 supported;
    uint32 advertising;
    uint16 speed;
    uint8 duplex;
    uint8 port;
    uint8 phy_address;
    uint8 transceiver;
    uint8 autoneg;
    uint32 maxtxpkt;
    uint32 maxrxpkt;
    uint32 reserved[4];
};

struct ethtool_value
{
   uint32 cmd;
   uint32 data;
};

#define MAX_OOPME_STRUCT_SIZE       (SIZE_STA_INFO * MAX_NUM_STATIONS)

struct oopme_req
{
    int type;
    int obj;
    int iface;
    int intvalue;
    uint64 int64value;
    char strvalue[MAX_STR];
} __attribute__ ((packed));

#define SIZE_OOPME_REQ sizeof(struct oopme_req)

struct oopme_struct_req
{
    int result;
    char strvalue[MAX_OOPME_STRUCT_SIZE];
} __attribute__ ((packed));

#define SIZE_OOPME_STRUCT_REQ sizeof(struct oopme_struct_req)

struct oopme_init_req
{
    int    type;
    int    intvalue;
    struct acl_cfg acl;
    struct load_balance_cfg   load_balance;
    struct radio_glb_cfg      radio_glb;
    struct radio_glb_11a_cfg  radio_glb_11a;
    struct radio_glb_11g_cfg  radio_glb_11g;
    struct radio_glb_11n_cfg  radio_glb_11n;
    struct radio_glb_11ac_cfg radio_glb_11ac;
    struct radio_cfg radio[MAX_NUM_RADIO_INTERFACES];
    struct security_cfg security;
    int    rad_acct_enabled;
    int    ssid_num;
    struct ssid_cfg ssid[MAX_NUM_SSID];
    struct ssid_glb_cfg ssid_glb;
    struct honeypot_wlist_cfg ssid_hp_bcast;
    struct honeypot_wlist_cfg ssid_hp_wlist;
    struct station_cfg station;
    char   ipv4_addr[REQ_LEN_IPV4_ADDRSTR];
    char   ipv4_mask[REQ_LEN_IPV4_ADDRSTR];
    char   mac_addr[REQ_LEN_VERSION];
    char   iface_name[REQ_LEN_IFACE_NAME];
    char   hostname[MAXHOSTNAMELEN];
    char   location[REQ_LEN_STR];
    int    eth_mode;
    struct geography_info geo_info;
    struct iap_counts_info iap_cnts;
    struct iap_decoder_ring iap_info[MAX_NUM_RADIO_INTERFACES];
    struct wds_client_cfg wds[MAX_NUM_WDS_CLIENT_LINKS];
    int    num_xrp_targets;
    struct xrp_target_cfg xrp_target[MAX_NUM_XRP_TARGETS];
    struct standby_cfg standby;
    struct ids_cfg ids;
    char   sw_version[REQ_LEN_SOFTWARE_VERSION];
    char   sw_name [REQ_LEN_VERSION];
    char   xbl_name[REQ_LEN_VERSION];
    char   scd_name[REQ_LEN_VERSION];
    char   flash   [REQ_LEN_VERSION];
    int    group_num;
    struct group_cfg group[MAX_NUM_GROUP];
    char   brand[REQ_LEN_VERSION];
    char   model[REQ_LEN_LONG_VERSION];
    char   serial[REQ_LEN_VERSION];
    char   license[REQ_LEN_LONG_VERSION];
    int    features;
    int    vlan_num;
    struct vlan_cfg vlan[MAX_NUM_VLAN];
    int    vlan_pool_num;
    struct vlan_pool_cfg vlan_pool[MAX_NUM_VLAN_POOL];
    struct wifi_tag_cfg wifi_tag;
    struct location_cfg location_info;
    int    fips_enabled;
    struct sta_assure_cfg sta_assure;
    struct roam_assist_cfg roam_assist;
    int    cluster_capable;
    int    cluster_running;
    int    syslog_sta_fmt;
    struct dot11u_cfg dot11u;
    struct hs20_cfg hs20;
    struct mdm_cfg mdm;
    uint8  proxy_mgmt_enabled;
    struct proxy_mgmt_entry proxy_mgmt_https;
    struct position_cfg position;
    struct device_id_mac_entry device_id[MAX_NUM_DEV_MAC_ENTRIES];
    int    num_device_id;
    struct mgmt_cfg mgmt;
    char   nas_id[REQ_LEN_HOSTNAME_STR];
    int32  memory;
    int32  speed;
    int32  cores;
    int32  tlbs;
} __attribute__ ((packed));

#define SIZE_OOPME_INIT_REQ sizeof(struct oopme_init_req)

struct session
{
    char user[REQ_LEN_USERNAME_STR];
    unsigned int id;
    unsigned int cli_pid;
    int socket;
    char socket_path[50];
    int priv;
    int org_priv;
};

#define SIZE_SESSION sizeof(struct session)


//****************************************************************
// Deprecated debug class translations
//----------------------------------------------------------------
#ifdef DBG_DEPRECATED

   #define dbgInfo(  _msg_ ) dbgPrint(CDbg::DBG_LEVEL_INFO , 0, __FILE__, __LINE__, __FUNCTION__, _msg_ )
   #define dbgError( _msg_ ) dbgPrint(CDbg::DBG_LEVEL_ERROR, 0, __FILE__, __LINE__, __FUNCTION__, _msg_ )
   #undef  DBG_INFO
   #define DBG_INFO  CDbg::DBG_LEVEL_INFO
   #undef  DBG_ERROR
   #define DBG_ERROR CDbg::DBG_LEVEL_ERROR
   #undef  DBG_WARN
   #define DBG_WARN  CDbg::DBG_LEVEL_WARN
   #undef  DBG_TRACE
   #define DBG_TRACE CDbg::DBG_LEVEL_TRACE

   #define DBG_TRACE( _msg_ ) dbgPrint(CDbg::DBG_LEVEL_TRACE, 0, __FILE__, __LINE__, __FUNCTION__, _msg_ )

   #define dbgTrace() dbgLevelIsTrace()

   #define DBG_BIT(x)                ( 1UL << (x))
   #define DBG_MASK(lo,hi)           ((1UL << (hi)) | \
                                     ((1UL << (hi)) - (1UL << (lo))))


   #define DBG_FLAG_RX            DBG_BIT_RX

   #define DBG_FLAG_TXSTATUS      DBG_BIT(28)

#endif // DBG_DEPRECATED
#endif /* not defined _XIRDHCP_H */
#endif // _CFGDEFS_H
