//******************************************************************************
/** @file xirver.h
 *
 * Constants for version specific compilation
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 * This file contains constants that define whether or not specific features
 * should be included or not
 *
 * Only version specific or related constants should be in here.
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRVER_H
#define _XIRVER_H

//------------------------------------------------------------------------------
// make sure anybody using this header file has defined xirrus_ver_major/minor
//------------------------------------------------------------------------------
#ifndef  XIRRUS_VER_MAJOR
#error   XIRRUS_VER_MAJOR undefined
#endif
#ifndef  XIRRUS_VER_MINOR
#error   XIRRUS_VER_MINOR undefined
#endif
#ifndef  XIRRUS_VER_PATCH
#define  XIRRUS_VER_PATCH 0     // FIX-ME: someday this will bite us ... mark my words
#endif

//------------------------------------------------------------------------------
// xirrus version handling macros & defines
//------------------------------------------------------------------------------
#define      XIRRUS_VER(major, minor)               ((major)*100 + (minor))
#define FULL_XIRRUS_VER(major, minor, patch)        (XIRRUS_VER(major, minor)*1000 + (patch))
#define      XIRRUS_VERSION                          XIRRUS_VER(XIRRUS_VER_MAJOR, XIRRUS_VER_MINOR)
#define      XIRRUS_VERSION_MAX                      XIRRUS_VER(9, 9)
#define FULL_XIRRUS_VERSION                     FULL_XIRRUS_VER(XIRRUS_VER_MAJOR, XIRRUS_VER_MINOR, XIRRUS_VER_PATCH)

#define      XIRRUS_MAJ(version)                     ( version         / 100)
#define      XIRRUS_MIN(version)                     ( version         % 100)
#define FULL_XIRRUS_MAJ(version)                     ((version / 1000) / 100)
#define FULL_XIRRUS_MIN(version)                     ((version / 1000) % 100)
#define FULL_XIRRUS_PAT(version)                     ( version % 1000)

//------------------------------------------------------------------------------
// Architecture definitions and dependent features
//------------------------------------------------------------------------------
#ifdef __mips__
#define XIRRUS_ARCH_MIPS
#endif

#ifdef __aarch64__
#define XIRRUS_ARCH_ARM
#endif

#ifdef __x86_64__
#define XIRRUS_ARCH_INTEL
#define CFG_VIRTUAL_AOS
#endif

#if (!defined(XIRRUS_ARCH_MIPS) && !defined(XIRRUS_ARCH_ARM) && !defined(XIRRUS_ARCH_INTEL))
#error XIRRUS_ARCH undefined
#endif

//------------------------------------------------------------------------------
// new / experimental features
//------------------------------------------------------------------------------
#define CFG_LOCATION_TWO_POINT_RANDOMIZATION    // randomize location with only 2 data points (aka "the Jack Hack")
#undef  CFG_CHK_LOCATION_BOUNDS                 // location boundary checking feature
// // JCODEUS #define CFG_USE_HOSTAPD_FIFOS                   // use fifos / named pipes to communicate to/from hostapd

//------------------------------------------------------------------------------
//  ethernet count definitions
//------------------------------------------------------------------------------
#define MAX_NUM_MIPS_ETH_INTERFACES     6
#define NUM_GIG_MIPS_ETH_INTERFACES     5

#define MAX_NUM_PPC_ETH_INTERFACES      3
#define NUM_GIG_PPC_ETH_INTERFACES      2

#define MAX_NUM_ETH_INTERFACES          MAX_NUM_MIPS_ETH_INTERFACES
#define NUM_GIG_ETH_INTERFACES          NUM_GIG_MIPS_ETH_INTERFACES

#define MOM_NUM_ETH_INTERFACES          MAX_NUM_MIPS_ETH_INTERFACES // maximum of maximums (mom)
#define MAX_GIG_ETH_INTERFACES          NUM_GIG_MIPS_ETH_INTERFACES

//------------------------------------------------------------------------------
// radio count definitions
//------------------------------------------------------------------------------
#define MAX_MIPS_IAPS                   16                  //max number of radios supported by hardware
#define MAX_MIPS_IAP_BOARDS             MAX_MIPS_IAPS       //max number of radio boards

#define MAX_PPC_IAPS                    16                  //max number of radios supported by hardware
#define MAX_PPC_IAP_BOARDS               4                  //max number of radio boards

#define MAX_IAPS                        MAX_MIPS_IAPS
#define MAX_IAP_BOARDS                  MAX_MIPS_IAP_BOARDS

#define MOM_IAPS                        MAX_MIPS_IAPS       // maximum of maximums (mom)
#define MOM_IAP_BOARDS                  MAX_MIPS_IAP_BOARDS // maximum of maximums (mom)

//------------------------------------------------------------------------------
// xr controller type defines
//------------------------------------------------------------------------------
#define XR_ALL                             0

#define XR500                            500
#define XR600                            600
#define XR1000                          1000
#define XR2000                          2000
#define XR4000                          4000
#define XR6000                          6000

#define XDBASE                         10000
#define XD1                   (XDBASE + 1000)
#define XD2                   (XDBASE + 2000)
#define XD4                   (XDBASE + 4000)
#define XD8                   (XDBASE + 8000)

#endif // _XIRVER_H

