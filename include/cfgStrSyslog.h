//******************************************************************************
/** @file cfgStrSyslog.h
 *
 * Format strings for syslog
 *
 * <I>Copyright (C) 2005 Xirrus. All Rights Reserved</I>
 *
 * @author  Eric K. Henderson
 * @date    11/6/2005
 *
 * These strings are used in the emit() methods of the configuration objects
 * to write the syslog file.
 **/
//------------------------------------------------------------------------------
#ifndef _CFGSTRSYSLOG_H
#define _CFGSTRSYSLOG_H

//********************************************************************************************************************************
//* Begin Splunkified (key/value pair) syslogs                                                                                   *
//********************************************************************************************************************************
//----------------------------------------------------------------
// Station events
//----------------------------------------------------------------
#define SYSLOG_OOPME_STA_ASSOC                      splunk() ? "station=%s iap=%s sta_event=\"associated\""                                                                   " ssid=\"%s\""                      : "Station %s, IAP %s: associated, SSID: %s"
#define SYSLOG_OOPME_STA_FT_ASSOC                   splunk() ? "station=%s iap=%s sta_event=\"FT associated\""                                                                " ssid=\"%s\""                      : "Station %s, IAP %s: FT associated, SSID: %s"
#define SYSLOG_OOPME_STA_REASSOC                    splunk() ? "station=%s iap=%s sta_event=\"reassociated\""                                                                 " ssid=\"%s\""                      : "Station %s, IAP %s: reassociated, SSID: %s"
#define SYSLOG_OOPME_STA_FT_REASSOC                 splunk() ? "station=%s iap=%s sta_event=\"FT reassociated\""                                                              " ssid=\"%s\""                      : "Station %s, IAP %s: FT reassociated, SSID: %s"
#define SYSLOG_OOPME_STA_AUTH_FAIL                  splunk() ? "station=%s iap=%s sta_event=\"authentication failure\" "                                                      " ssid=\"%s\""     " reason=\"%s\"" : "Station %s, IAP %s: authentication failure, SSID: %s, status: %s"
#define SYSLOG_OOPME_STA_ASSOC_FAIL                 splunk() ? "station=%s iap=%s sta_event=\"association failure\""                                                                             " reason=\"%s\"" : "Station %s, IAP %s: association failure, status: %s"
#define SYSLOG_OOPME_STA_DEAUTH                     splunk() ? "station=%s iap=%s sta_event=\"deauthenticated\""                                                                                 " reason=\"%s\"" : "Station %s, IAP %s: deauthenticated, reason: %s"
#define SYSLOG_OOPME_STA_DISASSOC                   splunk() ? "station=%s iap=%s sta_event=\"disassociated\""                                                                                   " reason=\"%s\"" : "Station %s, IAP %s: disassociated, reason: %s"
#define SYSLOG_OOPME_STA_ROAM                       splunk() ? "station=%s iap=%s sta_event=\"roamed to neighbor\""                                                           " neighbor=\"%s\""                  : "Station %s, IAP %s: roamed to neighbor %s"
#define SYSLOG_OOPME_STA_ROAM_NO_IAP                splunk() ? "station=%s "     "sta_event=\"roamed to neighbor\""                                                           " neighbor=\"%s\""                  : "Station %s"      ": roamed to neighbor %s"
#define SYSLOG_OOPME_STA_DATA_TUNNELLED             splunk() ? "station=%s iap=%s sta_event=\"data tunnelled via neighbor\""                                                  " neighbor=\"%s\""                  : "Station %s, IAP %s: data tunnelled via neighbor %s"
#define SYSLOG_OOPME_STA_RATE_CHANGE                splunk() ? "station=%s iap=%s sta_event=\"transmit rate changed from %d%sMbps to %d%sMbps\""                                                                  : "Station %s, IAP %s: transmit rate changed from %d%sMbps to %d%sMbps"
#define SYSLOG_OOPME_STA_RADIUS_SET_GROUP_FAIL      splunk() ? "station=%s iap=%s sta_event=\"failed to assign to any user group: no match for radius filter-id\""            " filter_id=\"%s\""                 : "Station %s, IAP %s: failed to assign to any user group, no match for RADIUS Filter-ID: %s"
#define SYSLOG_OOPME_STA_RADIUS_CHANGE_GROUP_FAIL   splunk() ? "station=%s iap=%s sta_event=\"failed to change "     "user group: no match for radius filter-id\""            " filter_id=\"%s\""                 : "Station %s, IAP %s: failed to change "     "user group, no match for RADIUS Filter-ID: %s"
#define SYSLOG_OOPME_STA_DEVICE_SET_GROUP_FAIL      splunk() ? "station=%s iap=%s sta_event=\"failed to assign to any user group: no match for group name\""                  " group=\"%s\""                     : "Station %s, IAP %s: failed to assign to any user group, no match for group name: %s"
#define SYSLOG_OOPME_STA_SET_GROUP                  splunk() ? "station=%s iap=%s sta_event=\"assigned to user group\""                                                       " group=\"%s\""                     : "Station %s, IAP %s: assigned to user group %s"
#define SYSLOG_OOPME_STA_GROUP_FULL                 splunk() ? "station=%s iap=%s sta_event=\"user group is full: "  "station deauthenticated\""                              " group=\"%s\""                     : "Station %s, IAP %s: user group %s is full, "  "station deauthenticated"
#define SYSLOG_OOPME_STA_GROUP_INACTIVE             splunk() ? "station=%s iap=%s sta_event=\"user group is inactive: station deauthenticated\""                              " group=\"%s\""                     : "Station %s, IAP %s: user group %s is inactive, station deauthenticated"
#define SYSLOG_OOPME_STA_GROUP_IGNORE_DHCP          splunk() ? "station=%s iap=%s sta_event=\"dhcp settings defined for user group ignored\""                                 " group=\"%s\""                     : "Station %s, IAP %s: DHCP settings defined for user group %s ignored"
#define SYSLOG_OOPME_STA_GROUP_IGNORE_WPR           splunk() ? "station=%s iap=%s sta_event=\"wpr settings defined for user group ignored\""                                  " group=\"%s\""                     : "Station %s, IAP %s: WPR settings defined for user group %s ignored"
#define SYSLOG_OOPME_STA_DRIVER_OLD                 splunk() ? "station=%s iap=%s sta_event=\"wifi device driver may be out of date%s: please check for an update\""                                              : "Station %s, IAP %s: WiFi device driver may be out of date%s. Please check for an update"
#define SYSLOG_OOPME_STA_IPV4_ADDR_WITH_USERNAME    splunk() ? "station=%s iap=%s sta_event=\"ipv4 address available\" ip=%s ssid=\"%s\" username=\"%s\""                                                         : "Station %s, IAP %s: IPv4 address available, IPv4: %s, SSID: %s, Username: %s"
#define SYSLOG_OOPME_STA_IPV4_ADDR_SANS_USERNAME    splunk() ? "station=%s iap=%s sta_event=\"ipv4 address available\" ip=%s ssid=\"%s\""                                                                         : "Station %s, IAP %s: IPv4 address available, IPv4: %s, SSID: %s"
#define SYSLOG_OOPME_STA_IPV4_ADDR_NON_ASSOCIATE    splunk() ? "station=%s iap=%s sta_event=\"ipv4 address available\" ip=%s"                                                                                     : "Station %s, IAP %s: IPv4 address available, IPv4: %s"
#define SYSLOG_OOPME_STA_IPV6_ADDR_WITH_USERNAME    splunk() ? "station=%s iap=%s sta_event=\"ipv6 address available\" ip=%s ssid=\"%s\" username=\"%s\""                                                         : "Station %s, IAP %s: IPv6 address available, IPv6: %s, SSID: %s, Username: %s"
#define SYSLOG_OOPME_STA_IPV6_ADDR_SANS_USERNAME    splunk() ? "station=%s iap=%s sta_event=\"ipv6 address available\" ip=%s ssid=\"%s\""                                                                         : "Station %s, IAP %s: IPv6 address available, IPv6: %s, SSID: %s"
#define SYSLOG_OOPME_STA_IPV6_ADDR_NON_ASSOCIATE    splunk() ? "station=%s iap=%s sta_event=\"ipv6 address available\" ip=%s"                                                                                     : "Station %s, IAP %s: IPv6 address available, IPv6: %s"

//----- OOPME  Debug Syslogs -----
#define SYSLOG_OOPME_STA_AUTH_REAUTH_ENFORCED       splunk() ? "station=%s "     "sta_event=\"reauthentication period of %d seconds will be enforced\""                                                           : "Station %s"      ": reauthentication period of %d seconds will be enforced"
#define SYSLOG_OOPME_STA_AUTH_RX_REAUTH_BLOCKED     splunk() ? "station=%s iap=%s sta_event=\"authentication "           "packet rejected, reauthentication period not expired\""                                 : "Station %s, IAP %s: authentication "           "packet rejected, station reauthentication period has not expired"
#define SYSLOG_OOPME_STA_AUTH_RX_IGNORE             splunk() ? "station=%s iap=%s sta_event=\"authentication "           "packet ignored, station is being deauthenticated\""                                     : "Station %s, IAP %s: authentication "           "packet ignored, station is being deauthenticated"
#define SYSLOG_OOPME_STA_AUTH_RX                    splunk() ? "station=%s iap=%s sta_event=\"authentication "           "packet received\""                                     " ssid=\"%s\" "                  : "Station %s, IAP %s: authentication "           "packet received, SSID: %s"
#define SYSLOG_OOPME_STA_AUTH_TX                    splunk() ? "station=%s iap=%s sta_event=\"authentication "           "packet sent\""                                                                          : "Station %s, IAP %s: authentication "           "packet sent"
#define SYSLOG_OOPME_STA_FT_AUTH_RX                 splunk() ? "station=%s iap=%s sta_event=\"FT authentication "        "packet received\""                                     " ssid=\"%s\" "                  : "Station %s, IAP %s: FT authentication "        "packet received, SSID: %s"
#define SYSLOG_OOPME_STA_FT_AUTH_TX                 splunk() ? "station=%s iap=%s sta_event=\"FT authentication "        "packet sent\""                                                                          : "Station %s, IAP %s: FT authentication "        "packet sent"
#define SYSLOG_OOPME_STA_ASSOC_REQ_BAD_AKMP         splunk() ? "station=%s iap=%s sta_event=\"association request "      "packet rejected, invalid-akmp=0x%08x\""                " ssid=\"%s\" "                  : "Station %s, IAP %s: association request "      "packet rejected, invalid AKMP (0x%08x), SSID: %s"
#define SYSLOG_OOPME_STA_ASSOC_REQ_RX_IGNORE        splunk() ? "station=%s iap=%s sta_event=\"association request "      "packet ignored, station is being deauthenticated\""                                     : "Station %s, IAP %s: association request "      "packet ignored, station is being deauthenticated"
#define SYSLOG_OOPME_STA_ASSOC_REQ_RX               splunk() ? "station=%s iap=%s sta_event=\"association request "      "packet received\""                                     " ssid=\"%s\" "                  : "Station %s, IAP %s: association request "      "packet received, SSID: %s"
#define SYSLOG_OOPME_STA_ASSOC_REQ_TX               splunk() ? "station=%s iap=%s sta_event=\"association request "      "packet sent\""                                                                          : "Station %s, IAP %s: association request "      "packet sent"
#define SYSLOG_OOPME_STA_FT_ASSOC_REQ_RX            splunk() ? "station=%s iap=%s sta_event=\"FT association request "   "packet received\""                                     " ssid=\"%s\" "                  : "Station %s, IAP %s: FT association request "   "packet received, SSID: %s"
#define SYSLOG_OOPME_STA_REASSOC_REQ_RX_IGNORE      splunk() ? "station=%s iap=%s sta_event=\"reassociation request "    "packet ignored, station is being deauthenticated\""                                     : "Station %s, IAP %s: reassociation request "    "packet ignored, station is being deauthenticated"
#define SYSLOG_OOPME_STA_REASSOC_REQ_RX             splunk() ? "station=%s iap=%s sta_event=\"reassociation request "    "packet received\""                                     " ssid=\"%s\" "                  : "Station %s, IAP %s: reassociation request "    "packet received, SSID: %s"
#define SYSLOG_OOPME_STA_REASSOC_REQ_TX             splunk() ? "station=%s iap=%s sta_event=\"reassociation request "    "packet sent\""                                                                          : "Station %s, IAP %s: reassociation request "    "packet sent"
#define SYSLOG_OOPME_STA_FT_REASSOC_REQ_RX          splunk() ? "station=%s iap=%s sta_event=\"FT reassociation request " "packet received\""                                     " ssid=\"%s\" "                  : "Station %s, IAP %s: FT reassociation request " "packet received, SSID: %s"
#define SYSLOG_OOPME_STA_ASSOC_RSP_RX               splunk() ? "station=%s iap=%s sta_event=\"association response "     "packet received\""                                                                      : "Station %s, IAP %s: association response "     "packet received"
#define SYSLOG_OOPME_STA_ASSOC_RSP_TX               splunk() ? "station=%s iap=%s sta_event=\"association response "     "packet sent\""                                                                          : "Station %s, IAP %s: association response "     "packet sent"
#define SYSLOG_OOPME_STA_FT_ASSOC_RSP_TX            splunk() ? "station=%s iap=%s sta_event=\"FT association response "  "packet sent\""                                                                          : "Station %s, IAP %s: FT association response "  "packet sent"
#define SYSLOG_OOPME_STA_REASSOC_RSP_RX             splunk() ? "station=%s iap=%s sta_event=\"reassociation response "   "packet received\""                                                                      : "Station %s, IAP %s: reassociation response "   "packet received"
#define SYSLOG_OOPME_STA_REASSOC_RSP_TX             splunk() ? "station=%s iap=%s sta_event=\"reassociation response "   "packet sent\""                                                                          : "Station %s, IAP %s: reassociation response "   "packet sent"
#define SYSLOG_OOPME_STA_FT_REASSOC_RSP_TX          splunk() ? "station=%s iap=%s sta_event=\"FT reassociation response ""packet sent\""                                                                          : "Station %s, IAP %s: FT reassociation response ""packet sent"
#define SYSLOG_OOPME_STA_DEAUTH_RX_ASSOC            splunk() ? "station=%s iap=%s sta_event=\"deauthentication "         "packet ignored, station associated on iap=%s\""                                         : "Station %s, IAP %s: deauthentication "         "packet ignored, station associated on IAP %s"
#define SYSLOG_OOPME_STA_DEAUTH_RX_IGNORE           splunk() ? "station=%s iap=%s sta_event=\"deauthentication "         "packet ignored, station is being deauthenticated\""                                     : "Station %s, IAP %s: deauthentication "         "packet ignored, station is being deauthenticated"
#define SYSLOG_OOPME_STA_DEAUTH_RX                  splunk() ? "station=%s iap=%s sta_event=\"deauthentication "         "packet received\""                                                                      : "Station %s, IAP %s: deauthentication "         "packet received"
#define SYSLOG_OOPME_STA_DEAUTH_TX                  splunk() ? "station=%s iap=%s sta_event=\"deauthentication "         "packet sent\""                                                                          : "Station %s, IAP %s: deauthentication "         "packet sent"
#define SYSLOG_OOPME_STA_DISASSOC_RX_ASSOC          splunk() ? "station=%s iap=%s sta_event=\"disassociation "           "packet ignored, station associated on iap=%s\""                                         : "Station %s, IAP %s: disassociation "           "packet ignored, station associated on IAP %s"
#define SYSLOG_OOPME_STA_DISASSOC_RX_IGNORE         splunk() ? "station=%s iap=%s sta_event=\"disassociation "           "packet ignored, station is being deauthenticated\""                                     : "Station %s, IAP %s: disassociation "           "packet ignored, station is being deauthenticated"
#define SYSLOG_OOPME_STA_DISASSOC_RX                splunk() ? "station=%s iap=%s sta_event=\"disassociation "           "packet received\""                                                                      : "Station %s, IAP %s: disassociation "           "packet received"
#define SYSLOG_OOPME_STA_DISASSOC_TX                splunk() ? "station=%s iap=%s sta_event=\"disassociation "           "packet sent\""                                                                          : "Station %s, IAP %s: disassociation "           "packet sent"
#define SYSLOG_OOPME_STA_ASSOC_TX_FAIL              splunk() ? "station=%s iap=%s sta_event=\"association response "     "packet not acknowledged\""                                                              : "Station %s, IAP %s: association response "     "packet not acknowledged"
#define SYSLOG_OOPME_STA_ASSOC_TIMEOUT              splunk() ? "station=%s iap=%s sta_event=\"association timed out\""                                                                                            : "Station %s, IAP %s: association timed out"
#define SYSLOG_OOPME_STA_WDS_LINK_TIMEOUT           splunk() ? "station=%s iap=%s sta_event=\"wds link timed out\""                                                                                               : "Station %s, IAP %s: WDS link timed out"
#define SYSLOG_OOPME_STA_FINGERPRINT_INFO           splunk() ? "station=%s "     "sta_event=\"fingerprint info: %s\""                                                                                             : "Station %s"      ": Fingerprint Info %s"
#define SYSLOG_OOPME_STA_MDM_AUTH_STATUS            splunk() ? "station=%s "     "sta_event=\"mdm authentication status: %s\""                                                                                    : "Station %s"      ": MDM authentication status: %s"
#define SYSLOG_OOPME_STA_CLOUD_AUTH_STATUS          splunk() ? "station=%s "     "sta_event=\"cloud authentication status: %s\""                                                                                  : "Station %s"      ": Cloud authentication status: %s"
#define SYSLOG_OOPME_STA_XMS_E_AUTH_STATUS          splunk() ? "station=%s "     "sta_event=\"xms-e authentication status: %s\""                                                                                  : "Station %s"      ": XMS-E authentication status: %s"
#define SYSLOG_OOPME_STA_UPSK_STATUS                splunk() ? "station=%s "     "sta_event=\"u-psk lookup status: %s\""                                                                                          : "Station %s"      ": U-PSK lookup status: %s"
#define SYSLOG_OOPME_STA_UPSK_WPR_STATUS            splunk() ? "station=%s "     "sta_event=\"u-psk lookup status: %s, wpr status: %s\""                                                                          : "Station %s"      ": U-PSK lookup status: %s, WPR status: %s"
#define SYSLOG_OOPME_STA_HTTP_GET_REQ_RX            splunk() ? "station=%s "     "sta_event=\"http get request received\" gig1_mac=%s src_ip=%s dst_ip=%s dst_port=%d url=%s"                                     : "Station %s"      ": HTTP GET request received, gig1 MAC: %s, Src IP: %s, Dst IP: %s, Dst Port: %d, URL: %s"

//----------------------------------------------------------------
// Station Assurance
//----------------------------------------------------------------
#define SYSLOG_STA_ASSURE_AUTH_FAILURES             splunk() ? "station=%s " "connect_alarm=\"authentication failures"              ", %d"     " is greater than threshold of %d"     "\""                        : "Station %s, connectivity alarm: authentication failures"           ", %d"     " is greater than threshold of %d"
#define SYSLOG_STA_ASSURE_ASSOC_TIME                splunk() ? "station=%s " "connect_alarm=\"associated time"                      ", %d sec" " is less" " than threshold of %d sec" "\""                        : "Station %s, connectivity alarm: associated time"                   ", %d sec" " is less" " than threshold of %d sec"
#define SYSLOG_STA_ASSURE_ERROR_RATE                splunk() ? "station=%s " "connect_alarm=\"error "   "rate"                      ", %d%%"   " is greater than threshold of %d%%"   "\""                        : "Station %s, connectivity alarm: error "   "rate"                   ", %d%%"   " is greater than threshold of %d%%"
#define SYSLOG_STA_ASSURE_RETRY_RATE                splunk() ? "station=%s " "connect_alarm=\"retry "   "rate"                      ", %d%%"   " is greater than threshold of %d%%"   "\""                        : "Station %s, connectivity alarm: retry "   "rate"                   ", %d%%"   " is greater than threshold of %d%%"
#define SYSLOG_STA_ASSURE_DATA_RATE                 splunk() ? "station=%s " "connect_alarm=\"data "    "rate"                      ", %dMbps" " is less" " than threshold of %dMbps" "\""                        : "Station %s, connectivity alarm: data "    "rate"                   ", %dMbps" " is less" " than threshold of %dMbps"
#define SYSLOG_STA_ASSURE_RSSI                      splunk() ? "station=%s " "connect_alarm=\"rssi"                                 ", %ddB"   " is less" " than threshold of %ddB"   "\""                        : "Station %s, connectivity alarm: rssi"                              ", %ddB"   " is less" " than threshold of %ddB"
#define SYSLOG_STA_ASSURE_SNR                       splunk() ? "station=%s " "connect_alarm=\"snr"                                  ", %ddB"   " is less" " than threshold of %ddB"   "\""                        : "Station %s, connectivity alarm: snr"                               ", %ddB"   " is less" " than threshold of %ddB"
#define SYSLOG_STA_ASSURE_DISTANCE                  splunk() ? "station=%s " "connect_alarm=\"distance"                             ", %d feet"" is greater than threshold of %d feet""\""                        : "Station %s, connectivity alarm: distance"                          ", %d feet"" is greater than threshold of %d feet"
#define SYSLOG_STA_ASSURE_DRIVER_FAILURES           splunk() ? "station=%s " "connect_alarm=\"driver revision failures (rev. < %s)" ", %d"     " is greater than threshold of %d"     "\""                        : "Station %s, connectivity alarm: driver revision failures (rev. < %s), %d"     " is greater than threshold of %d"

//----------------------------------------------------------------
// Hostapd
//----------------------------------------------------------------
#define SYSLOG_HOSTAPD_RADIUS_AUTH_SERVER_FAIL      splunk() ? "radius_server=\"%s\" radius_type=authentication radius_event=\"server not responding to request for station\" station=%s"                         : "RADIUS authentication server %s not responding to request for station %s"
#define SYSLOG_HOSTAPD_RADIUS_AUTH_SERVER_FAILOVER  splunk() ? "radius_server=\"%s\" radius_type=authentication radius_event=\"server not responding to request for station\" station=%s fail_over=\"%s\""        : "RADIUS authentication server %s not responding to request for station %s, fail-over to %s"
#define SYSLOG_HOSTAPD_RADIUS_AUTH_SERVER_RETRY     splunk() ? "radius_server=\"%s\" radius_type=authentication radius_event=\"server changed back to primary\""                                                  : "RADIUS authentication server changed back to primary server %s"
#define SYSLOG_HOSTAPD_RADIUS_ACCT_SERVER_FAIL      splunk() ? "radius_server=\"%s\" radius_type=accounting"  " radius_event=\"server not responding to request for station\" station=%s"                         : "RADIUS accounting server "  "%s not responding to request for station %s"
#define SYSLOG_HOSTAPD_RADIUS_ACCT_SERVER_FAILOVER  splunk() ? "radius_server=\"%s\" radius_type=accounting"  " radius_event=\"server not responding to request for station\" station=%s fail_over=\"%s\""        : "RADIUS accounting server "  "%s not responding to request for station %s, fail-over to %s"
#define SYSLOG_HOSTAPD_RADIUS_ACCT_SERVER_RETRY     splunk() ? "radius_server=\"%s\" radius_type=accounting"  " radius_event=\"server changed back to primary\""                                                  : "RADIUS accounting server changed back to primary server %s"

#define SYSLOG_HOSTAPD_RADIUS_RX_UNKNOWN_SERVER     splunk() ? "radius_event=\"discarding radius %s packet received from unknown server\" radius_server=\"%s\""                                                   : "Discarding RADIUS %s packet received from unknown server %s"

#define SYSLOG_HOSTAPD_STA_DOT1X_AUTH_FAIL          splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"802.1x authentication failure\""                                    " username=\"%s\""          : "Station %02x:%02x:%02x:%02x:%02x:%02x, 802.1x authentication failure, Username: %s"
#define SYSLOG_HOSTAPD_STA_DOT1X_AUTH_SUCCESS       splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"802.1x authentication success\""                                    " username=\"%s\""          : "Station %02x:%02x:%02x:%02x:%02x:%02x, 802.1x authentication success, Username: %s"
#define SYSLOG_HOSTAPD_STA_WPR_AUTH_FAIL            splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"wpr authentication failure\""                                       " username=\"%s\""          : "Station %02x:%02x:%02x:%02x:%02x:%02x, WPR authentication failure, " "Username: %s"
#define SYSLOG_HOSTAPD_STA_WPR_AUTH_SUCCESS         splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"wpr authentication success\""                                       " username=\"%s\""          : "Station %02x:%02x:%02x:%02x:%02x:%02x, WPR authentication success, " "Username: %s"
#define SYSLOG_HOSTAPD_STA_RADIUS_VLAN_SET          splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"assigned vlan id\""                                                 " vlan=%d"                  : "Station %02x:%02x:%02x:%02x:%02x:%02x, assigned VLAN ID %d"
#define SYSLOG_HOSTAPD_STA_RADIUS_VLAN_FAIL         splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"failed to assign vlan id, no such vlan defined\""                   " vlan=%d"                  : "Station %02x:%02x:%02x:%02x:%02x:%02x, failed to assign VLAN ID %d, no such VLAN defined"
#define SYSLOG_HOSTAPD_STA_RADIUS_VLAN_IGNORE       splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"assignment to vlan id ignored, already assigned to user group\""    " vlan=%d filter_id=\"%s\"" : "Station %02x:%02x:%02x:%02x:%02x:%02x, assignment to VLAN ID %d ignored, already assigned to User Group (RADIUS Filter-ID: %s)"
#define SYSLOG_HOSTAPD_STA_RADIUS_FILTER_ID_SET     splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"assigned radius filter-id\""                                                " filter_id=\"%s\"" : "Station %02x:%02x:%02x:%02x:%02x:%02x, assigned RADIUS Filter-ID: %s"
#define SYSLOG_HOSTAPD_STA_RADIUS_MULTI_SESS_ID_SET splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"assigned radius acct-multi-session-id\""                        " acct-multi-session-id=\"%s\"" : "Station %02x:%02x:%02x:%02x:%02x:%02x, assigned RADIUS Acct-Multi-Session-Id : %s"

//----- Hostapd/WPA_Supplicant Debug Syslogs -----
#define SYSLOG_HOSTAPD_STA_DOT1X_EAP_TX             splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eap %s "      "packet (type %s) sent\""                                                         : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAP %s "      "packet (type %s) sent"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAP_RX             splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eap %s "      "packet (type %s) received\""                                                     : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAP %s "      "packet (type %s) received"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAP_TX_NO_TYPE     splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eap %s "      "packet sent\""                                                                   : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAP %s "      "packet sent"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAP_RX_NO_TYPE     splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eap %s "      "packet received\""                                                               : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAP %s "      "packet received"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_KEY_TX       splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-key "   "packet sent\""                                                                   : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-key "   "packet sent"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_KEY_TX_MSG   splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-key "   "packet sent     (%s)\""                                                          : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-key "   "packet sent     (%s)"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_KEY_RX       splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-key "   "packet received\""                                                               : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-key "   "packet received"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_KEY_RX_MSG   splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-key "   "packet received (%s)\""                                                          : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-key "   "packet received (%s)"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_KEY_RX_DROP  splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-key "   "packet dropped  (%s%s)\""                                                        : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-key "   "packet dropped  (%s%s)"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_START_TX     splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-start " "packet sent\""                                                                   : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-start " "packet sent"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_START_RX     splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-start " "packet received\""                                                               : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-start " "packet received"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_LOGOFF_TX    splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-logoff ""packet sent\""                                                                   : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-logoff ""packet sent"
#define SYSLOG_HOSTAPD_STA_DOT1X_EAPOL_LOGOFF_RX    splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"eapol-logoff ""packet received\""                                                               : "Station %02x:%02x:%02x:%02x:%02x:%02x, EAPOL-logoff ""packet received"
#define SYSLOG_HOSTAPD_STA_RADIUS_TX                splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"radius %s "   "packet sent\""                                       " radius_server=\"%s\""     : "Station %02x:%02x:%02x:%02x:%02x:%02x, RADIUS %s "   "packet sent to server %s"
#define SYSLOG_HOSTAPD_STA_RADIUS_RX                splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"radius %s "   "packet received\""                                   " radius_server=\"%s\""     : "Station %02x:%02x:%02x:%02x:%02x:%02x, RADIUS %s "   "packet received from server %s"
#define SYSLOG_HOSTAPD_STA_RADIUS_RX_INVALID_AUTH   splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"radius %s "   "packet with invalid message-authenticator received\""" radius_server=\"%s\""     : "Station %02x:%02x:%02x:%02x:%02x:%02x, RADIUS %s "   "packet with invalid Message-Authenticator received from server %s"
#define SYSLOG_HOSTAPD_STA_ERROR                    splunk() ? "station=%02x:%02x:%02x:%02x:%02x:%02x sta_event=\"%s\""                                                                                           : "Station %02x:%02x:%02x:%02x:%02x:%02x, %s"

//----------------------------------------------------------------
// Monitor / IDS
//----------------------------------------------------------------
#define SYSLOG_MONITOR_APPROVED_DETECT              splunk() ? "ids_event=\"approved ""ap detected\""" ssid=\"%s\" bssid=%02x:%02x:%02x:%02x:%02x:%02x manufacturer=\"%s\" channel=%d rssi=%d security=\"%s\""    : "Approved ""AP detected."" SSID: %s, BSSID: %02x:%02x:%02x:%02x:%02x:%02x, Manufacturer: %s, Channel: %d, RSSI: %d, Security: %s"
#define SYSLOG_MONITOR_KNOWN_DETECT                 splunk() ? "ids_event=\"known "   "ap detected\""" ssid=\"%s\" bssid=%02x:%02x:%02x:%02x:%02x:%02x manufacturer=\"%s\" channel=%d rssi=%d security=\"%s\""    : "Known "   "AP detected."" SSID: %s, BSSID: %02x:%02x:%02x:%02x:%02x:%02x, Manufacturer: %s, Channel: %d, RSSI: %d, Security: %s"
#define SYSLOG_MONITOR_ROGUE_DETECT                 splunk() ? "ids_event=\"rogue "   "ap detected\""" ssid=\"%s\" bssid=%02x:%02x:%02x:%02x:%02x:%02x manufacturer=\"%s\" channel=%d rssi=%d security=\"%s\""    : "Rogue "   "AP detected."" SSID: %s, BSSID: %02x:%02x:%02x:%02x:%02x:%02x, Manufacturer: %s, Channel: %d, RSSI: %d, Security: %s"
#define SYSLOG_MONITOR_ROGUE_BLOCKED                splunk() ? "ids_event=\"rogue "   "ap blocked\"" " ssid=\"%s\" bssid=%02x:%02x:%02x:%02x:%02x:%02x manufacturer=\"%s\" channel=%d rssi=%d security=\"%s\""    : "Rogue "   "AP blocked." " SSID: %s, BSSID: %02x:%02x:%02x:%02x:%02x:%02x, Manufacturer: %s, Channel: %d, RSSI: %d, Security: %s"

#define SYSLOG_OOPME_IDS_EVENT                      splunk() ? "ids_event=\"%s detected\" iap=%s channel=%d"                                                                                                      : "IDS event: %s detected, IAP %s, channel %d"
#define SYSLOG_OOPME_IDS_EVENT_PKT_FLOOD            splunk() ? "ids_event=\"%s detected\" iap=%s channel=%d"                        " ids_status=\"%d packets in last %d seconds\""                               : "IDS event: %s detected, IAP %s, channel %d, %d packets in last %d seconds"
#define SYSLOG_OOPME_IDS_EVENT_MIC_ERROR            splunk() ? "ids_event=\"%s detected\" iap=%s channel=%d"                        " ids_status=\"%d MIC errors in last %d seconds\""                            : "IDS event: %s detected, IAP %s, channel %d, %d MIC errors in last %d seconds"
#define SYSLOG_OOPME_IDS_EVENT_NULL_PROBE_RESP      splunk() ? "ids_event=\"%s detected\" iap=%s channel=%d"                        " ids_status=\"%d null probe responses in last %d seconds\""                  : "IDS event: %s detected, IAP %s, channel %d, %d null probe responses in last %d seconds"
#define SYSLOG_OOPME_IDS_EVENT_SPOOFED_PKT          splunk() ? "ids_event=\"%s detected\" iap=%s channel=%d"                        " ids_status=\"%d spoofed packets in last %d seconds\""                       : "IDS event: %s detected, IAP %s, channel %d, %d spoofed packets in last %d seconds"
#define SYSLOG_OOPME_IDS_EVENT_SEQ_NUM_ANOMALY      splunk() ? "ids_event=\"%s detected\" iap=%s channel=%d station=\"%s\""                                                                                       : "IDS event: %s detected, IAP %s, channel %d, station %s"
#define SYSLOG_OOPME_IDS_EVENT_STA_IMPERSONATION    splunk() ? "ids_event=\"%s detected\" iap=%s channel=%d station=\"%s\""         " ids_status=\"%d events in last %d seconds\""                                : "IDS event: %s detected, IAP %s, channel %d, station %s, %d events in last %d seconds"
#define SYSLOG_OOPME_IDS_EVENT_EVIL_TWIN_ATTACK     splunk() ? "ids_event=\"%s detected\" "     "channel=%d" " ssid=\"%s\" bssid=%s"                                                                              : "IDS event: %s detected, channel %d, SSID: %s, BSSID: %s"

//********************************************************************************************************************************
//* End of Splunkified (key/value pair) syslogs                                                                                  *
//********************************************************************************************************************************

//****************************************************************
// OOPME (& Mace)
//----------------------------------------------------------------
#define SYSLOG_OOPME_WPA_TKIP_CM                    "WPA TKIP Countermeasures are on"
#define SYSLOG_OOPME_RADAR_DETECTED                 "Radar detected on IAP %s, changing channel from %d to %d"
#define SYSLOG_OOPME_RADAR_DETECTED_TEST            "Radar detected on IAP %s, channel %d, counter %d (Test mode)"
#define SYSLOG_OOPME_B_ONLY_PROTECT_ON              "802.11b-only traffic detected on IAP %s, protection mode enabled"
#define SYSLOG_OOPME_B_ONLY_PROTECT_OFF             "802.11b-only traffic no longer detected on IAP %s, protection mode disabled"
#define SYSLOG_OOPME_WDS_ASSOC                      "WDS client link %d, IAP %s: associated, SSID: %s"
#define SYSLOG_OOPME_WDS_AUTH_FAIL                  "WDS client link %d, IAP %s: authentication failure, status: %s"
#define SYSLOG_OOPME_WDS_ASSOC_FAIL                 "WDS client link %d, IAP %s: association failure, status: %s"
#define SYSLOG_OOPME_WDS_DEAUTH                     "WDS client link %d, IAP %s: deauthenticated, reason: %s"
#define SYSLOG_OOPME_WDS_DISASSOC                   "WDS client link %d, IAP %s: disassociated, reason: %s"
#define SYSLOG_OOPME_STANDBY_ENABLED                "Standby mode enabled, disabling all IAPs"
#define SYSLOG_OOPME_STANDBY_ALL_RADIOS_ON          "Standby target down, enabling all IAPs"
#define SYSLOG_OOPME_STANDBY_ALL_RADIOS_OFF         "Standby target up, disabling all IAPs"
#define SYSLOG_OOPME_STANDBY_ONE_RADIO_ON           "Standby target IAP failed, enabling IAP %s on channel %d"
#define SYSLOG_OOPME_STANDBY_ONE_RADIO_OFF          "Standby target IAP restored, disabling IAP %s on channel %d"

#define SYSLOG_OOPME_MISSED_BEACONS_BCN_RST         "No received beacons from IAP %s for %d seconds with %dpps traffic, resetting interface beacons."
#define SYSLOG_OOPME_MISSED_BEACONS_PHY_RST         "No received beacons from IAP %s for %d seconds with %dpps traffic, resetting interface PHY."
#define SYSLOG_OOPME_MISSED_BEACONS_MAC_RST         "No received beacons from IAP %s for %d seconds with %dpps traffic, resetting interface MAC."
#define SYSLOG_OOPME_MISSED_BEACONS_SYS_RST         "No received beacons from IAP %s for %d seconds with %dpps traffic, scheduling system reboot."
#define SYSLOG_OOPME_MISSED_BEACONS_BCN_MSG         "No received beacons from IAP %s for %d seconds with %dpps traffic, interface reset recommended."
#define SYSLOG_OOPME_MISSED_BEACONS_PHY_MSG         "No received beacons from IAP %s for %d seconds with %dpps traffic, interface reset recommended."
#define SYSLOG_OOPME_MISSED_BEACONS_MAC_MSG         "No received beacons from IAP %s for %d seconds with %dpps traffic, interface reset recommended."
#define SYSLOG_OOPME_MISSED_BEACONS_SYS_MSG         "No received beacons from IAP %s for %d seconds with %dpps traffic, system reboot recommended."

#define SYSLOG_OOPME_MISSED_PRBRSPS_BCN_RST         "No received probe responses from IAP %s for %d seconds with %dpps traffic, resetting interface beacons."
#define SYSLOG_OOPME_MISSED_PRBRSPS_PHY_RST         "No received probe responses from IAP %s for %d seconds with %dpps traffic, resetting interface PHY."
#define SYSLOG_OOPME_MISSED_PRBRSPS_MAC_RST         "No received probe responses from IAP %s for %d seconds with %dpps traffic, resetting interface MAC."
#define SYSLOG_OOPME_MISSED_PRBRSPS_SYS_RST         "No received probe responses from IAP %s for %d seconds with %dpps traffic, scheduling system reboot."
#define SYSLOG_OOPME_MISSED_PRBRSPS_BCN_MSG         "No received probe responses from IAP %s for %d seconds with %dpps traffic, interface reset recommended."
#define SYSLOG_OOPME_MISSED_PRBRSPS_PHY_MSG         "No received probe responses from IAP %s for %d seconds with %dpps traffic, interface reset recommended."
#define SYSLOG_OOPME_MISSED_PRBRSPS_MAC_MSG         "No received probe responses from IAP %s for %d seconds with %dpps traffic, interface reset recommended."
#define SYSLOG_OOPME_MISSED_PRBRSPS_SYS_MSG         "No received probe responses from IAP %s for %d seconds with %dpps traffic, system reboot recommended."

#define SYSLOG_OOPME_SCHED_REBOOT                   "Performing scheduled system reboot."
#define SYSLOG_OOPME_SCHED_REBOOT_NO_STAS           "No stations associated, performing scheduled system reboot now."
#define SYSLOG_OOPME_SCHED_REBOOT_IAP_LMT           "%d IAPs have scheduled an system reboot, performing scheduled system reboot now."

#define SYSLOG_OOPME_MAC_STOPPED                    "Interface IAP Multi-Channel MAC controller stopped."
#define SYSLOG_OOPME_MAC_STARTED                    "Interface IAP Multi-Channel MAC controller started."
#define SYSLOG_OOPME_MAC_FIPS_AES_FAILED            "FIPS AES encryption test failed. Rebooting system."
#define SYSLOG_OOPME_MAC_AES_LOOPBACK_FAILED        "AES encryption loopback test failed."

#define SYSLOG_OOPME_TARGET_ASSERT_REBOOT           "Target assert detected on IAP %s. Rebooting array."
#define SYSLOG_OOPME_TARGET_ASSERT_RESET            "Target assert detected on IAP %s. Resetting IAP."
#define SYSLOG_OOPME_TARGET_COOKIE_REBOOT           "Target low resource condition detected on IAP %s. Rebooting array."
#define SYSLOG_OOPME_TARGET_COOKIE_RESET            "Target low resource condition detected on IAP %s. Resetting IAP."
#define SYSLOG_OOPME_TARGET_RESET_FAIL              "Unable to reset IAP %s. Rebooting array."
#define SYSLOG_OOPME_DEAD_RADIO_RESET               "Low activity condition detected on IAP %s. Resetting IAP."
#define SYSLOG_OOPME_BAD_RADIO_DETECTED             "Potential bad IAP %s detected."
#define SYSLOG_OOPME_TARGET_TXDESC_RESET            "Target out of tx descriptor detected on IAP %s. Resetting IAP."
#define SYSLOG_OOPME_TARGET_TXDESC_REBOOT           "Target out of tx descriptor detected on IAP %s. Rebooting array."

#define SYSLOG_OOPME_WDS_STOPPING                   "IAP %s disconnected from WDS client link"
#define SYSLOG_OOPME_WDS_CHANNEL_SWITCH             "Switching channel to %d for WDS client link on IAP %s following channel switch announcement by WDS host"
#define SYSLOG_OOPME_AID_MAP_SYNC                   "Synchronized AID table to Station table"
#define SYSLOG_OOPME_XRP_SYS_RESOURCES_ERR          "Unable to establish tunnel with neighbor %s due to low system resources"

#define SYSLOG_OOPME_MDM_AW_API_QUERY_ERROR         "Unable to query AirWatch API, HTTP status code: %d"
#define SYSLOG_OOPME_MDM_AW_API_CONN_ERROR          "Unable to connect to AirWatch API"
#define SYSLOG_OOPME_GUEST_ACCESS_CONN_ERROR        "Unable to connect to Guest Access Server"
#define SYSLOG_OOPME_UPSK_SERVER_CONN_ERROR         "Unable to connect to U-PSK server"

//****************************************************************
// Config (& Mace)
//----------------------------------------------------------------
#define SYSLOG_CFG_INIT_GOOD_FLASH                  "System flash is enabled."
#define SYSLOG_CFG_INIT_BAD_FLASH                   "System flash is disabled. System is currently running without flash."
#define SYSLOG_CFG_INIT_BAD_HW_KEY                  "System initialization failed. Invalid hardware security key."
#define SYSLOG_CFG_INIT_BAD_HARDWARE                "System initialization failed. Software version incompatible with this hardware."
#define SYSLOG_CFG_INIT_BAD_DRIVER                  "System initialization failed. IAP driver failed to load."
#define SYSLOG_CFG_INIT_BAD_DRIVER_NO_RADIOS        "System initialization failed. IAP driver failed to find any radios."
#define SYSLOG_CFG_INIT_BAD_DRIVER_MISSING_RADIOS   "System initialization failed. IAP driver failed to find one or more radios."
#define SYSLOG_CFG_INIT_UNSUPPORTED_RADIO           "System does not support 802.11ac Wave 2 radios.  802.11ac Wave 2 radios disabled."
#define SYSLOG_CFG_INIT_INVALID_RADIO_MIX           "System does not support a mix of 802.11ac Wave 2 radios and non-Wave 2 radios. Non-Wave 2 radios disabled."
#define SYSLOG_CFG_INIT_SW_UPGRADE_GOOD             "System software upgrade to image %s succeeded. ""System is running new software version."
#define SYSLOG_CFG_INIT_SW_UPGRADE_FAIL             "System software upgrade to image %s failed. "   "System is running backup software version %s"
#define SYSLOG_CFG_INIT_SW_LOAD_FAIL                "System software image %s failed to load. "      "System is running backup software version %s"
#define SYSLOG_CFG_INIT_SW_LICENSE_FAIL_DOWNGRADE   "Downgrading to backup software version %s.  Please update the license key before upgrading again."
#define SYSLOG_CFG_INIT_SW_LICENSE_FAIL_MESSAGE     "System is not licensed to run software image %s."
#define SYSLOG_CFG_INIT_SW_LICENSE_FAIL             "System is not licensed to run current software image.  Please update the license key or downgrade to previous software image."
#define SYSLOG_CFG_INIT_SW_IMAGE_REMOVE             "Removing invalid system software image %s"
#define SYSLOG_CFG_INIT_SW_IMAGE_PRI                "Active system software image set to %s"
#define SYSLOG_CFG_INIT_SW_IMAGE_BAK                "Backup system software image set to %s"
#define SYSLOG_CFG_INIT_SUCCESS                     "System initialized successfully"
#define SYSLOG_CFG_INIT_REASON                      "System reset,   reason : %s"
#define SYSLOG_CFG_INIT_SW_IMAGE                    "System software image  : %s"
#define SYSLOG_CFG_INIT_HOSTNAME                    "System DNS hostname    : %s"
#define SYSLOG_CFG_INIT_GIG1_IP                     "System gig1 IP  address: %s"
#define SYSLOG_CFG_INIT_GIG1_MAC                    "System gig1 MAC address: %s"
#define SYSLOG_CFG_INIT_IAP_MAC                     "System IAP  MAC range  : %s"
#define SYSLOG_CFG_INIT_LICENSE_REPAIRED            "System license key     : missing or invalid. Key repaired."
#define SYSLOG_CFG_INIT_LICENSE_INVALID             "System license key     : missing or invalid."
#define SYSLOG_CFG_INIT_LICENSE_KEY                 "System license key     : %s"
#define SYSLOG_CFG_INIT_LICENSE_FEATURES            "System license features: "
#define SYSLOG_CFG_INIT_PARTITION_CORRUPT           "Correctable Media Error: Please use the format command to resolve this problem, or contact Xirrus Field Service for assistance."

#define SYSLOG_CFG_INIT_REBOOT_REQUESTED            "Reboot requested"
#define SYSLOG_CFG_INIT_REBOOT_POWER_ON             "Power on"
#define SYSLOG_CFG_INIT_REBOOT_BROWN_OUT            "Power brown out"
#define SYSLOG_CFG_INIT_REBOOT_SYS_WATCHDOG         "System control watchdog"
#define SYSLOG_CFG_INIT_REBOOT_PRO_WATCHDOG         "Processor watchdog"
#define SYSLOG_CFG_INIT_REBOOT_UNKNOWN              "Unavailable"

#define SYSLOG_CFG_LICENSE_CHANGED                  "System license key changed to %s"
#define SYSLOG_CFG_LICENSE_CLEARED                  "System license key cleared"
#define SYSLOG_CFG_LICENSE_CHANGED_BAD              "New system license key invalid (%s). Ignored."
#define SYSLOG_CFG_LICENSE_TEMPORARY                "System license changed to temporary key %s"
#define SYSLOG_CFG_LICENSE_TEMPORARY_BAD            "New system license temporary key invalid (%s). Ignored."
#define SYSLOG_CFG_LICENSE_RESTORED                 "System license key restored to %s"
#define SYSLOG_CFG_LICENSE_NO_RxM_RFM_OFF           "No RF Manager       " "(RxM) feature sets " "enabled, RF monitor disabled."
#define SYSLOG_CFG_LICENSE_NO_RxM_IDS_OFF           "RF Security Manager " "(RSM) feature set not enabled, intrusion detection disabled."
#define SYSLOG_CFG_LICENSE_NO_RxM_AUTOCELL_OFF      "RF Performance Manager (RPM) feature set not enabled, auto cell disabled."
#define SYSLOG_CFG_LICENSE_NO_RxM_ASSURANCE_OFF     "RF Analysis Manager " "(RAM) feature set not enabled, radio assurance disabled."
#define SYSLOG_CFG_LICENSE_NO_RxM_STANDBY_OFF       "RF Analysis Manager " "(RAM) feature set not enabled, standby mode disabled."
#define SYSLOG_CFG_LICENSE_NO_DOT11N_OFF            "802.11n feature set not enabled, 802.11n disabled."
#define SYSLOG_CFG_LICENSE_NO_DOT11N_40_BOND_OFF    "802.11n feature set not enabled, 40MHz channel bonding disabled."
#define SYSLOG_CFG_LICENSE_NO_DOT11AC_OFF           "802.11ac feature set not enabled, 802.11ac disabled."
#define SYSLOG_CFG_LICENSE_NO_DOT11AC_80_BOND_OFF   "802.11ac feature set not enabled, 80MHz channel bonding disabled."
#define SYSLOG_CFG_LICENSE_NO_DOT11AC_160_BOND_OFF  "802.11ac feature set not enabled, 160MHz channel bonding disabled."
#define SYSLOG_CFG_LICENSE_TEMP_NTP_ON              "Temporary license key installed.  NTP forced on."
#define SYSLOG_CFG_LICENSE_TEMP_REMAINING           "%d days left on temporary license key."
#define SYSLOG_CFG_LICENSE_TEMP_EXPIRED             "Temporary license key expired. "

#define SYSLOG_CFG_LOGIN_SUCCESS                    "Admin user %s logged into %s interface%s%s"
#define SYSLOG_CFG_LOGIN_FAIL                       "Admin user %s login to %s interface failed%s%s"
#define SYSLOG_CFG_LOGOUT                           "Admin user %s logged out of %s interface"
#define SYSLOG_CFG_TIMEOUT                          "Admin user %s was logged out of %s interface due to timeout"
#define SYSLOG_CFG_LOGIN_DUPLICATE                  "Admin user %s with session id %u is already logged in and will not be added to admin history"
#define SYSLOG_CFG_LOGOUT_DUPLICATE                 "Admin user %s was logged out of %d sessions with session id %u"

#define SYSLOG_CFG_LOGIN_CLI_SUCCESS                "Admin user %s logged into command line interface from serial console with privilege level %s%s"
#define SYSLOG_CFG_LOGIN_WMI_SUCCESS                "Admin user %s logged into web management interface with privilege level %s%s"
#define SYSLOG_CFG_LOGIN_CLI_SUCCESS_IP             "Admin user %s logged into command line interface from %s with privilege level %s"
#define SYSLOG_CFG_LOGIN_WMI_SUCCESS_IP             "Admin user %s logged into web management interface from %s with privilege level %s"
#define SYSLOG_CFG_LOGIN_CLI_FAIL                   "Admin user %s login to command line interface failed from serial console"
#define SYSLOG_CFG_LOGIN_WMI_FAIL                   "Admin user %s login to web management interface failed"
#define SYSLOG_CFG_LOGIN_CLI_FAIL_IP                "Admin user %s login to command line interface failed from %s"
#define SYSLOG_CFG_LOGIN_WMI_FAIL_IP                "Admin user %s login to web management interface failed from %s"
#define SYSLOG_CFG_LOGOUT_CLI                       "Admin user %s logged out of command line interface"
#define SYSLOG_CFG_LOGOUT_WMI                       "Admin user %s logged out of web management interface"
#define SYSLOG_CFG_TIMEOUT_CLI                      "Admin user %s was logged out of command line interface due to timeout"
#define SYSLOG_CFG_TIMEOUT_WMI                      "Admin user %s was logged out of web management interface due to timeout"

#define SYSLOG_CFG_ADMIN_ADD                        "Admin user %s added"
#define SYSLOG_CFG_ADMIN_DEL                        "Admin user %s deleted"

#define SYSLOG_CFG_OAUTH_ADD                        "OAUTH %s added"
#define SYSLOG_CFG_OAUTH_DEL                        "OAUTH %s deleted"

#define SYSLOG_CFG_SSID_ADD                         "SSID %s added"
#define SYSLOG_CFG_SSID_DEL                         "SSID %s deleted"
#define SYSLOG_CFG_SSID_BAND                        "SSID %s band changed to %s"
#define SYSLOG_CFG_SSID_BROADCAST                   "Broadcast SSID(s) changed, 802.11a: %s, 802.11b/g: %s"
#define SYSLOG_CFG_SSID_POOL                        "SSID %s DHCP pool changed to %s"
#define SYSLOG_CFG_SSID_WPR                         "SSID %s Web Page Redirect changed to %s"
#define SYSLOG_CFG_SSID_FILTER_LIST                 "SSID %s Filter List changed to %s"
#define SYSLOG_CFG_SSID_WPR_WLIST_RESET             "SSID %s Web Page Redirect whitelist cleared"
#define SYSLOG_CFG_SSID_WPR_WLIST_ADD               "SSID %s Web Page Redirect whitelist entry %s added"
#define SYSLOG_CFG_SSID_WPR_WLIST_DEL               "SSID %s Web Page Redirect whitelist entry %s deleted"
#define SYSLOG_CFG_SSID_HONEYPOT_BCAST_RESET        "SSID %s broadcast list cleared"
#define SYSLOG_CFG_SSID_HONEYPOT_BCAST_ADD          "SSID %s broadcast list SSID %s added"
#define SYSLOG_CFG_SSID_HONEYPOT_BCAST_DEL          "SSID %s broadcast list SSID %s deleted"
#define SYSLOG_CFG_SSID_HONEYPOT_WLIST_RESET        "SSID %s whitelist cleared"
#define SYSLOG_CFG_SSID_HONEYPOT_WLIST_ADD          "SSID %s whitelist SSID %s added"
#define SYSLOG_CFG_SSID_HONEYPOT_WLIST_DEL          "SSID %s whitelist SSID %s deleted"

#define SYSLOG_CFG_SSID_STATION_LIMIT               "All SSID station "      "limits changed to %d"
#define SYSLOG_CFG_SSID_PPS_LIMIT                   "All SSID "      "traffic limits changed to %s pps"
#define SYSLOG_CFG_SSID_PPS_PER_STA                 "All SSID station traffic limits changed to %s pps"
#define SYSLOG_CFG_SSID_KBPS_LIMIT                  "All SSID "      "traffic limits changed to %s Kbps"
#define SYSLOG_CFG_SSID_KBPS_PER_STA                "All SSID station traffic limits changed to %s Kbps"

#define SYSLOG_CFG_WPR_FILE_NOT_FOUND               "Webpage redirect file for %s %s not found, using default"

#define SYSLOG_CFG_GROUP_ADD                        "User Group %s added"
#define SYSLOG_CFG_GROUP_DEL                        "User Group %s deleted"
#define SYSLOG_CFG_GROUP_POOL                       "User Group %s DHCP pool changed to %s"
#define SYSLOG_CFG_GROUP_WPR                        "User Group %s Web Page Redirect changed to %s"
#define SYSLOG_CFG_GROUP_FILTER_LIST                "User Group %s Filter List changed to %s"
#define SYSLOG_CFG_GROUP_VLAN                       "User Group %s VLAN changed to %s"
#define SYSLOG_CFG_GROUP_WPR_WLIST_RESET            "Group %s Web Page Redirect whitelist cleared"
#define SYSLOG_CFG_GROUP_WPR_WLIST_ADD              "Group %s Web Page Redirect whitelist entry %s added"
#define SYSLOG_CFG_GROUP_WPR_WLIST_DEL              "Group %s Web Page Redirect whitelist entry %s deleted"

#define SYSLOG_CFG_DEVICE_ID_ADD                    "Device ID for MAC substring %s added, type: %s, class: %s"
#define SYSLOG_CFG_DEVICE_ID_EDIT                   "Device ID for MAC substring %s edited, type: %s, class: %s"
#define SYSLOG_CFG_DEVICE_ID_DEL                    "Device ID for MAC substring %s deleted"
#define SYSLOG_CFG_DEVICE_ID_CLEAR                  "Device ID MAC table cleared"

#define SYSLOG_CFG_ACL_ADD                          "ACL MAC address %02x:%02x:%02x:%02x:%02x:%02x added"
#define SYSLOG_CFG_ACL_DEL                          "ACL MAC address %02x:%02x:%02x:%02x:%02x:%02x deleted"

#define SYSLOG_CFG_ACL_STR_ADD                      "ACL MAC address %s added"
#define SYSLOG_CFG_ACL_STR_DEL                      "ACL MAC address %s deleted"

#define SYSLOG_CFG_SSID_ACL_STR_ADD                 "SSID %s ACL MAC address %s added"
#define SYSLOG_CFG_SSID_ACL_STR_DEL                 "SSID %s ACL MAC address %s deleted"

#define SYSLOG_CFG_RADIUS_ADD                       "RADIUS user %s added"
#define SYSLOG_CFG_RADIUS_DEL                       "RADIUS user %s deleted"
#define SYSLOG_CFG_RADIUS_PASSWORD                  "RADIUS user %s password changed"
#define SYSLOG_CFG_RADIUS_SSID                      "RADIUS user %s SSID set to %s"
#define SYSLOG_CFG_RADIUS_GROUP                     "RADIUS user %s User Group set %s"

#define SYSLOG_CFG_HOSTAPD_ACTION                   "WPA Authentication Engine %s (configuration file: " HOSTAPD_CFG_FILE ".%d)"
#define SYSLOG_CFG_HOSTAPD_STATUS                   "WPA Authentication Engine %s (%sstarted, %srunning,%s iteration %d)", "Status"

#define SYSLOG_CFG_DHCP_POOL_ADD                    "DHCP pool %s added"
#define SYSLOG_CFG_DHCP_POOL_DEL                    "DHCP pool %s deleted"

#define SYSLOG_CFG_TIME_SET                         "Time set to %s"

#define SYSLOG_CFG_IDS_DATABASE_CLR                 "Rogue AP SSID list cleared"
#define SYSLOG_CFG_IDS_DATABASE_ADD                 "Rogue AP SSID %s added as %s"
#define SYSLOG_CFG_IDS_DATABASE_CHG                 "Rogue AP SSID %s changed to %s"
#define SYSLOG_CFG_IDS_DATABASE_DEL                 "Rogue AP SSID %s deleted"

#define SYSLOG_CFG_MULTICAST_LIST_CLR               "Multicast address %s list cleared"
#define SYSLOG_CFG_MULTICAST_LIST_SET               "Multicast address %s list set to %s"

#define SYSLOG_CFG_MDNS_FILTER_CLR                  "mDNS filter list cleared"
#define SYSLOG_CFG_MDNS_FILTER_SET                  "mDNS filter list set to %s"

#define SYSLOG_CFG_FILTER_ADD                       "Global protocol filter %s added"
#define SYSLOG_CFG_FILTER_EDIT                      "Global protocol filter %s changed"
#define SYSLOG_CFG_FILTER_DEL                       "Global protocol filter %s deleted"
#define SYSLOG_CFG_FILTER_MOVE                      "Global protocol filter %s order moved %s"

#define SYSLOG_CFG_FILTER_LIST_ADD                  "Filter List %s added"
#define SYSLOG_CFG_FILTER_LIST_DEL                  "Filter List %s deleted"
#define SYSLOG_CFG_FILTER_LIST_CLR                  "Filter List %s cleared"

#define SYSLOG_CFG_FILTER_LIST_ENTRY_ADD            "Protocol filter %s added to Filter List %s"
#define SYSLOG_CFG_FILTER_LIST_ENTRY_EDIT           "Protocol filter %s changed in Filter List %s"
#define SYSLOG_CFG_FILTER_LIST_ENTRY_DEL            "Protocol filter %s deleted from Filter List %s"
#define SYSLOG_CFG_FILTER_LIST_ENTRY_MOVE           "Protocol filter %s order moved %s in Filter List %s"

#define SYSLOG_CFG_APP_LIST_ADD                     "Application List %s added"
#define SYSLOG_CFG_APP_LIST_DEL                     "Application List %s deleted"
#define SYSLOG_CFG_APP_LIST_CLR                     "Application List %s cleared"

#define SYSLOG_CFG_APP_LIST_ENTRY_ADD               "Application %s added to Application List %s"
#define SYSLOG_CFG_APP_LIST_ENTRY_DEL               "Application %s deleted from Application List %s"

#define SYSLOG_CFG_TUNNEL_ADD                       "Tunnel %s added"
#define SYSLOG_CFG_TUNNEL_DEL                       "Tunnel %s deleted"
#define SYSLOG_CFG_TUNNEL_EDIT_ENABLED              "Tunnel %s state changed to %s"
#define SYSLOG_CFG_TUNNEL_EDIT_TYPE                 "Tunnel %s type changed to %s"
#define SYSLOG_CFG_TUNNEL_EDIT_LOCAL                "Tunnel %s local endpoint IP address changed to %s"
#define SYSLOG_CFG_TUNNEL_EDIT_PRI_REMOTE           "Tunnel %s primary remote endpoint IP address changed to %s"
#define SYSLOG_CFG_TUNNEL_EDIT_SEC_REMOTE           "Tunnel %s secondary remote endpoint IP address changed to %s"
#define SYSLOG_CFG_TUNNEL_EDIT_MTU                  "Tunnel %s MTU changed to %d"
#define SYSLOG_CFG_TUNNEL_EDIT_SSID_MASK            "Tunnel %s SSID list changed to %s"
#define SYSLOG_CFG_TUNNEL_EDIT_VLAN_LIST            "Tunnel %s VLAN list changed to %s"
#define SYSLOG_CFG_TUNNEL_EDIT_DHCP_OPT             "Tunnel %s DHCP relay agent information option changed to %s"
#define SYSLOG_CFG_TUNNEL_EDIT_INTERVAL             "Tunnel %s remote endpoint failover ping interval changed to %d seconds"
#define SYSLOG_CFG_TUNNEL_EDIT_FAILURES             "Tunnel %s remote endpoint failover number of ping failures changed to %d"

#define SYSLOG_CFG_EDIT_IPV6_ADDR                   "IPv6 DHCP address changed to %s/%d"

#define SYSLOG_CFG_VLAN_ADD                         "VLAN %s (number %d) added"
#define SYSLOG_CFG_VLAN_DEL                         "VLAN %s deleted"
#define SYSLOG_CFG_VLAN_EDIT_NUM                    "VLAN %s number changed to %d"
#define SYSLOG_CFG_VLAN_EDIT_MGMT                   "VLAN %s management %s"
#define SYSLOG_CFG_VLAN_EDIT_DHCP                   "VLAN %s DHCP client %s"
#define SYSLOG_CFG_VLAN_EDIT_IPV4_ADDR              "VLAN %s IPv4 address changed to %s"
#define SYSLOG_CFG_VLAN_EDIT_IPV4_MASK              "VLAN %s IPv4 mask changed to %s"
#define SYSLOG_CFG_VLAN_EDIT_IPV4_GATEWAY           "VLAN %s IPv4 gateway changed to %s"
#define SYSLOG_CFG_VLAN_EDIT_IPV6_ADDR              "VLAN %s IPv6 DHCP address changed to %s/%d"
#define SYSLOG_CFG_VLAN_EDIT_TUNNEL_SERVER          "VLAN %s tunnel server changed to %s"
#define SYSLOG_CFG_VLAN_EDIT_TUNNEL_SECRET          "VLAN %s tunnel secret changed"
#define SYSLOG_CFG_VLAN_EDIT_TUNNEL_PORT            "VLAN %s tunnel port changed to %d"
#define SYSLOG_CFG_VLAN_EDIT_XRP                    "VLAN %s layer 3 fast roaming changed to %s"
#define SYSLOG_CFG_VLAN_EDIT_FA                     "VLAN %s fabric attach %s"
#define SYSLOG_CFG_VLAN_DEL_DEFAULT                 "Default VLAN %s (number %d) deleted. Resetting default gateway to gig port"
#define SYSLOG_CFG_VLAN_DEL_NATIVE                  "Native VLAN %s (number %d) deleted. Resetting native VLAN to none"

#define SYSLOG_CFG_VLAN_POOL_ADD                    "VLAN Pool %s added"
#define SYSLOG_CFG_VLAN_POOL_DEL                    "VLAN Pool %s deleted"
#define SYSLOG_CFG_VLAN_POOL_EDIT_LIST              "VLAN Pool %s list changed to %s"
#define SYSLOG_CFG_VLAN_POOL_DEL_LIST               "VLAN Pool %s list deleted"

#define SYSLOG_CFG_NATIVE_VLAN_EDIT_IPV4_DHCP       "Native VLAN %s IPv4 DHCP client %s"
#define SYSLOG_CFG_NATIVE_VLAN_EDIT_IPV4_ADDR       "Native VLAN %s IPv4 address changed to %s"
#define SYSLOG_CFG_NATIVE_VLAN_EDIT_IPV4_MASK       "Native VLAN %s IPv4 mask changed to %s"
#define SYSLOG_CFG_NATIVE_VLAN_EDIT_IPV4_GATEWAY    "Native VLAN %s IPv4 gateway changed to %s"

#define SYSLOG_CFG_NATIVE_VLAN_EDIT_IPV6_DHCP       "Native VLAN %s IPv6 DHCP client %s"
#define SYSLOG_CFG_NATIVE_VLAN_EDIT_IPV6_ADDR       "Native VLAN %s IPv6 address changed to %s/%d"

#define SYSLOG_CFG_RADAR_CHANNEL_LOCK_DOWN          "Radar detected on IAP %s, channel %d.  Channel locked, IAP disabled."
#define SYSLOG_CFG_RADAR_NO_CHANNEL_DOWN            "Radar detected on IAP %s, channel %d.  No available alternate channel, IAP disabled."
#define SYSLOG_CFG_RADAR_CHANNEL_CHG                "Radar detected on IAP %s, changing IAP channel from %d to %d"
#define SYSLOG_CFG_RADAR_WDS_CHANNEL_DOWN           "Disabling IAP %s to preserve WDS link on IAP %s."

#define SYSLOG_CFG_NO_RADAR_CHAN_CHG                "Radar no longer detected, changing IAP %s back to channel %d from %d"
#define SYSLOG_CFG_NO_RADAR_IAP_UP                  "Radar no longer detected, enabling IAP %s"

#define SYSLOG_CFG_SSID_CONNECTIVITY_CHG            "SSID %s %s"
#define SYSLOG_CFG_SSID_ACTIVE_CHG                  "SSID %s state changed to %sactive"
#define SYSLOG_CFG_GROUP_CONNECTIVITY_CHG           "User Group %s %s"
#define SYSLOG_CFG_GROUP_ACTIVE_CHG                 "User Group %s state changed to %sactive"

#define SYSLOG_CFG_XRP_TARGET_CLR                   "Fast roaming target list cleared"
#define SYSLOG_CFG_XRP_TARGET_ADD                   "Fast roaming target %s added"
#define SYSLOG_CFG_XRP_TARGET_DEL                   "Fast roaming target %s deleted"

#define SYSLOG_CFG_FIXED_ADDR_ADD                   "Multicast fixed address %s added"
#define SYSLOG_CFG_FIXED_ADDR_DEL                   "Multicast fixed address %s deleted"

#define SYSLOG_CFG_11U_VENUE_TYPE                   "802.11u venue type changed to %s"

#define SYSLOG_CFG_11U_ROAM_CONSORTIUM_LIST_RESET   "802.11u roaming consortium list cleared"
#define SYSLOG_CFG_11U_ROAM_CONSORTIUM_OI_ADD       "802.11u roaming consortium OI %s added"
#define SYSLOG_CFG_11U_ROAM_CONSORTIUM_OI_DEL       "802.11u roaming consortium OI %s deleted"

#define SYSLOG_CFG_11U_DOMAIN_LIST_RESET            "802.11u domain name list cleared"
#define SYSLOG_CFG_11U_DOMAIN_ADD                   "802.11u domain name %s added"
#define SYSLOG_CFG_11U_DOMAIN_DEL                   "802.11u domain name %s deleted"

#define SYSLOG_CFG_11U_CELL_NETWORK_LIST_RESET      "802.11u cellular network information list cleared"
#define SYSLOG_CFG_11U_CELL_NETWORK_ADD             "802.11u cellular network (MCC=%s, MNC=%s) added"
#define SYSLOG_CFG_11U_CELL_NETWORK_DEL             "802.11u cellular network (MCC=%s, MNC=%s) deleted"

#define SYSLOG_CFG_11U_NAI_REALM_LIST_RESET         "802.11u NAI realm list cleared"
#define SYSLOG_CFG_11U_NAI_REALM_ADD                "802.11u NAI realm %s added"
#define SYSLOG_CFG_11U_NAI_REALM_EDIT               "802.11u NAI realm %s changed"
#define SYSLOG_CFG_11U_NAI_REALM_DEL                "802.11u NAI realm %s deleted"

#define SYSLOG_CFG_11U_NETWORK_AUTH_TYPE_LIST_RESET "802.11u network authentication type list cleared"
#define SYSLOG_CFG_11U_NETWORK_AUTH_TYPE_ADD        "802.11u network authentication type %s added"
#define SYSLOG_CFG_11U_NETWORK_AUTH_TYPE_DEL        "802.11u network authentication type %s deleted"

#define SYSLOG_CFG_11U_VENUE_NAME_LIST_RESET        "802.11u venue name list cleared"
#define SYSLOG_CFG_11U_VENUE_NAME_ADD               "802.11u venue name \"%s\" (language: %s) added"
#define SYSLOG_CFG_11U_VENUE_NAME_DEL               "802.11u venue name \"%s\" (language: %s) deleted"

#define SYSLOG_CFG_HS20_CONN_CAP_LIST_RESET         "Hotspot 2.0 connection capability list reset to defaults"
#define SYSLOG_CFG_HS20_CONN_CAP_ADD                "Hotspot 2.0 connection capability %s added"
#define SYSLOG_CFG_HS20_CONN_CAP_EDIT               "Hotspot 2.0 connection capability %s changed"
#define SYSLOG_CFG_HS20_CONN_CAP_DEL                "Hotspot 2.0 connection capability %s deleted"

#define SYSLOG_CFG_HS20_OPER_NAME_LIST_RESET        "Hotspot 2.0 operator friendly name list cleared"
#define SYSLOG_CFG_HS20_OPER_NAME_LIST_ADD          "Hotspot 2.0 operator friendly name \"%s\" (language: %s) added"
#define SYSLOG_CFG_HS20_OPER_NAME_LIST_DEL          "Hotspot 2.0 operator friendly name \"%s\" (language: %s) deleted"

#define SYSLOG_CFG_WDS_CLIENT_SSID_CLR              "WDS client link %d SSID cleared"

#define SYSLOG_CFG_ENV_CTRL_OVER_TEMP               "Outdoor enclosure temperature too high"
#define SYSLOG_CFG_ENV_CTRL_UNDER_TEMP              "Outdoor enclosure temperature too low"
#define SYSLOG_CFG_ENV_CTRL_OVER_HUMID              "Outdoor enclosure humidity too high"
#define SYSLOG_CFG_ENV_CTRL_FAN_FAIL                "Outdoor enclosure fan failure"

#define SYSLOG_CFG_SAVE_IMAGE_START                 "Saving new system software image to flash file system"
#define SYSLOG_CFG_SAVE_IMAGE_ABORT                 "System software image save to flash file system aborted"
#define SYSLOG_CFG_SAVE_IMAGE_FAILED                "System software image save to flash file system failed"
#define SYSLOG_CFG_SAVE_IMAGE_SUCCESS               "System software image save to flash file system completed"
#define SYSLOG_CFG_SAVE_IMAGE_UPDATED               "System software image update successful"
#define SYSLOG_CFG_SAVE_IMAGE_DOWNGRADE             "System software image downgrade in process"
#define SYSLOG_CFG_SAVE_INTEGRITY_FAILED            "System software image failed integrity test"
#define SYSLOG_CFG_SAVE_LICENSE_FAILED              "System software image update failed due to insufficient license level"
#define SYSLOG_CFG_SAVE_HW_COMPATIBILITY_FAILED     "System software image update failed due to hardware incompatibility"

#define SYSLOG_CFG_SAVE_CONFIG                      "Configuration saved"
#define SYSLOG_CFG_SAVE_CONFIG_NAMED_LOCAL          "Local configuration file %s saved"
#define SYSLOG_CFG_SAVE_CONFIG_NAMED_REMOTE         "Remote configuration file %s saved"
#define SYSLOG_CFG_LOAD_CONFIG_NAMED_LOCAL          "Local configuration file %s loaded"
#define SYSLOG_CFG_LOAD_CONFIG_NAMED_REMOTE         "Remote configuration file %s loaded"

#define SYSLOG_CFG_CREATE_RESTORE_POINT             "Restore point created"
#define SYSLOG_CFG_RESTORE_CONFIG_START             "Restoring configuration file %s"
#define SYSLOG_CFG_RESTORE_CONFIG_FINISH            "Configuration file %s restored"

#define SYSLOG_CFG_QUICK_CONFIG_START               "Applying quick configuration template %s"
#define SYSLOG_CFG_QUICK_CONFIG_FINISH              "Quick configuration template %s applied"

#define SYSLOG_CFG_SAVE_DIAG                        "Diagnostic file saved"
#define SYSLOG_CFG_SAVE_DIAG_NAMED_LOCAL            "Local diagnostic file %s saved"
#define SYSLOG_CFG_SAVE_DIAG_NAMED_REMOTE           "Remote diagnostic file %s saved"

#define SYSLOG_CFG_EID_FILE_ERASE                   "File %s erased from flash file system"

#define SYSLOG_CFG_EXEC_SELF_TEST                   "Self test started"
#define SYSLOG_CFG_SAVE_SELF_TEST                   "Self test completed. Results file saved"
#define SYSLOG_CFG_SAVE_SELF_TEST_NAMED_LOCAL       "Self test completed. Local results file %s saved"
#define SYSLOG_CFG_SAVE_SELF_TEST_NAMED_REMOTE      "Self test completed. Remote results file %s saved"

#define SYSLOG_EXE_IAP_AUTO_CELL                    "Interface IAP %s automatic cell size assignment executed"
#define SYSLOG_EXE_IAP_AUTO_CELL_ADJ                "Interface IAP %s automatic cell size adjusted"

#define SYSLOG_CFG_IAP_AUTO_CHAN_BEG                "Interface IAP %sautomatic channel assignment started"
#define SYSLOG_CFG_IAP_AUTO_CHAN_NEG                "Interface IAP %sautomatic channel assignment negotiation %s"
#define SYSLOG_CFG_IAP_AUTO_CHAN_DONE               "Interface IAP %sautomatic channel assignment completed"
#define SYSLOG_CFG_IAP_DFLT_CHAN_DONE               "Interface IAP country code default channel assignment completed"

#define SYSLOG_CFG_IAP_RF_MONITOR                   "RF Monitor changed to IAP %s"

#define SYSLOG_CFG_IAP_DSCP_MAP                     "Interface IAP global DSCP to QoS mapping table changed"

#define SYSLOG_CFG_UBOOT_ENV_VAR_SET                "Boot loader environment variable %s set to %s"
#define SYSLOG_CFG_UBOOT_ENV_VAR_CLR                "Boot loader environment variable %s cleared"

#define SYSLOG_CFG_WRITE_SECURITY_IP_ERR            "External RADIUS configuration error. Invalid %s server IP address (%s)"
#define SYSLOG_CFG_WRITE_SECURITY_PSK_EAP_BOTH_ON   "Cannot enable both EAP and PSK"
#define SYSLOG_CFG_WRITE_SECURITY_PSK_EAP_BOTH_OFF  "Cannot disable both EAP and PSK"

#define SYSLOG_CFG_EID_RESET_ETH                    "Interface %s reset"
#define SYSLOG_CFG_EID_CLEAR_ETH_STATS              "Interface %s statistics cleared"
#define SYSLOG_CFG_EID_CLEAR_RADIO_STATS            "Interface IAP %s statistics cleared"
#define SYSLOG_CFG_EID_CHECK_USER                   ""
#define SYSLOG_CFG_EID_SHOW_CUR_CFG                 ""
#define SYSLOG_CFG_EID_SAVE_CUR_CFG                 "Configuration saved"
#define SYSLOG_CFG_EID_REBOOT                       "Reboot requested"
#define SYSLOG_CFG_EID_RESET_CFG                    "Configuration reset to factory default"
#define SYSLOG_CFG_EID_ENABLE_ALL_RADIOS            "Interface IAP %s state changed to up"
#define SYSLOG_CFG_EID_DISABLE_ALL_RADIOS           "Interface IAP %s state changed to down"
#define SYSLOG_CFG_EID_ENABLE_AUTO_CH               "Interface IAP %s automatic channel assignment executed"
#define SYSLOG_CFG_EID_DEAUTH_STA                   "Station %s deauthenticated"
#define SYSLOG_CFG_EID_CLEAR_LOG                    "Syslog cleared"
#define SYSLOG_CFG_EID_SAVE_IMAGE                   ""
#define SYSLOG_CFG_EID_SHOW_CFG_DIFF                ""
#define SYSLOG_CFG_EID_CLEAR_STA_STATS              "Station %s statistics cleared"
#define SYSLOG_CFG_EID_SET_DATE                     ""
#define SYSLOG_CFG_EID_IMPORT_CFG                   "Configuration uploaded"
#define SYSLOG_CFG_EID_RESET_CONSOLE                ""
#define SYSLOG_CFG_EID_DENY_STA                     "Station %s denied access"
#define SYSLOG_CFG_EID_LOGOUT_USER                  ""
#define SYSLOG_CFG_EID_MOVE_FILTER_UP               ""
#define SYSLOG_CFG_EID_MOVE_FILTER_DOWN             ""
#define SYSLOG_CFG_EID_RESET_WDS_CLIENTS            "All WDS links set to off"
#define SYSLOG_CFG_EID_CLEAR_VLAN_STATS             ""
#define SYSLOG_CFG_EID_AUTO_CELL                    "Interface IAP %s automatic cell size assignment executed"
#define SYSLOG_CFG_EID_ENV_CTRL_SET_STATUS          ""
#define SYSLOG_CFG_EID_ENV_CTRL_SET_ALARM           ""
#define SYSLOG_CFG_EID_ENV_CTRL_SET_VERSION         ""
#define SYSLOG_CFG_EID_CLEAR_WDS_STATS              "Statistics cleared for %s"
#define SYSLOG_CFG_EID_SITE_SURVEY                  "Site survey mode changed to %s"
#define SYSLOG_CFG_EID_TXPOWER_ALL_RADIOS           "Interface IAP %s TX power changed to %d"
#define SYSLOG_CFG_EID_RXTHRESHOLD_ALL_RADIOS       "Interface IAP %s RX threshold changed to %d"
#define SYSLOG_CFG_EID_RESET_ALL_CHANNELS           "Interface IAP channel assignments reset to defaults"
#define SYSLOG_CFG_EID_SET_UBOOT_ENV_VAR            ""
#define SYSLOG_CFG_EID_SAVE_UBOOT_ENV               "Boot loader environment variables saved"
#define SYSLOG_CFG_EID_DIAGNOSTIC_LOG               ""
#define SYSLOG_CFG_EID_RESET_CFG_PRESERVE_ETH       "Configuration reset to factory default preserving ethernet settings"
#define SYSLOG_CFG_EID_RESET_SNMPD_LINK             ""
#define SYSLOG_CFG_EID_WDS_LOOPBACK_LINK_INIT       ""
#define SYSLOG_CFG_EID_WDS_LOOPBACK_LINK_TEST       ""
#define SYSLOG_CFG_EID_WDS_LOOPBACK_LINK_DONE       ""
#define SYSLOG_CFG_EID_SELF_TEST                    "System self test executed"
#define SYSLOG_CFG_EID_CELLSIZE_ALL_RADIOS          "Interface IAP %s cell size changed to %s"
#define SYSLOG_CFG_EID_SYSLOG_MAIL_TEST             "Syslog test email sent"
#define SYSLOG_CFG_EID_BOND_ALL_RADIOS              "Interface IAP %s channel bond assigned"
#define SYSLOG_CFG_EID_MOVE_FILTER_LIST_UP          ""
#define SYSLOG_CFG_EID_MOVE_FILTER_LIST_DOWN        ""
#define SYSLOG_CFG_EID_LOCK_ALL_RADIOS              "Interface IAP %s channel locked"
#define SYSLOG_CFG_EID_WMI_RESTART                  "Web management interface restarted"
#define SYSLOG_CFG_EID_WMI_CLEAR_CERT               "SSL certificate deleted"
#define SYSLOG_CFG_EID_WMI_USE_SS_CERT              "Using self-signed SSL certificate"

#define SYSLOG_CFG_ARCHLOG_START                    "Archlog started"
#define SYSLOG_CFG_ARCHLOG_STOP                     "Archlog stopped"
#define SYSLOG_CFG_ARCHLOG_BUILD                    "Archlog compiled"
#define SYSLOG_CFG_ARCHLOG_CLEAN                    "Archlog files cleared"

#define SYSLOG_EXE_RESET                            "Interface %s reset"
#define SYSLOG_EXE_ETH_STATS_CLEARED                "Interface %s statistics cleared"
#define SYSLOG_EXE_IAP_STATS_CLEARED                "Interface IAP %s statistics cleared"
#define SYSLOG_EXE_CONFIG_SAVED                     "Configuration saved"
#define SYSLOG_EXE_REBOOT                           "Reboot requested"
#define SYSLOG_EXE_CONFIG_RESET                     "Configuration reset to factory default"
#define SYSLOG_EXE_IAP_STATE_UP                     "Interface IAP %s state changed to up"
#define SYSLOG_EXE_IAP_STATE_DOWN                   "Interface IAP %s state changed to down"
#define SYSLOG_EXE_IAP_AUTO_CHANNEL                 "Interface IAP %s automatic channel assignment executed"
#define SYSLOG_EXE_IAP_AUTO_CELL                    "Interface IAP %s automatic cell size assignment executed"
#define SYSLOG_EXE_IAP_AUTO_CELL_ADJ                "Interface IAP %s automatic cell size adjusted"
#define SYSLOG_EXE_STA_DEAUTH                       "Station %s deauthenticated"
#define SYSLOG_EXE_SYSLOG_CLEARED                   "Syslog cleared"
#define SYSLOG_EXE_SOFTWARE_UPDATED                 "New system software image saved"
#define SYSLOG_EXE_STA_STATS_CLEARED                "Station %s statistics cleared"
#define SYSLOG_EXE_DATE_SET                         "Date and time set to %s"
#define SYSLOG_EXE_CONFIG_UPLOADED                  "Configuration uploaded"
#define SYSLOG_EXE_WDS_STATS_CLEARED                "Statistics cleared for %s"
#define SYSLOG_EXE_SITE_SURVEY                      "Site survey mode changed to %s"
#define SYSLOG_EXE_RESET_CHANNELS                   "Interface IAP channel assignments reset to defaults"

#define SYSLOG_CFG_MGMT_REAUTH_ENFORCED             "Management reauthentication period of %d seconds will be enforced on %s %s"
#define SYSLOG_CFG_ERR_ACT_STATE_FILE_OPEN          "Error opening " ACTIVATION_STATE_FILE
#define SYSLOG_CFG_ERR_ACT_STATE_FILE_LOCK          "Error locking " ACTIVATION_STATE_FILE
#define SYSLOG_CFG_ERR_ACT_STATE_FILE_REMOVE        "Error removing " ACTIVATION_STATE_FILE
#define SYSLOG_CFG_ERR_ACT_STATE_FILE_BAD           "Error in contents of " ACTIVATION_STATE_FILE
#define SYSLOG_CFG_ERR_ACT_BAD_VER_NUM              "Error retrieving version & build numbers for activation daemon"
//****************************************************************
// Health Monitor
//----------------------------------------------------------------
#define SYSLOG_HEALTH_CONFIG_FAILURE                "Configuration process communication restart required"
#define SYSLOG_HEALTH_XCONFIG_FAILURE               "Cluster Configuration process communication restart required"
#define SYSLOG_HEALTH_OOPME_FAILURE                 "Management engine communication restart required"
#define SYSLOG_HEALTH_WMI_FAILURE                   "Web Server communication restart required"
#define SYSLOG_HEALTH_CLOUD_FAILURE                 "Cloud communication restart required"
#define SYSLOG_HEALTH_XMS_E_FAILURE                 "XMS-E communication restart required"
#define SYSLOG_HEALTH_DRIVER_PATH_FAILURE           "IAP driver communication path restart required"
#define SYSLOG_HEALTH_DRIVER_FAILURE                "IAP driver communication restart required"
#define SYSLOG_HEALTH_COMM_FAILURE                  "Interprocess communication restart required"
#define SYSLOG_HEALTH_MEMORY_FAILURE                "Memory restart required"
#define SYSLOG_HEALTH_FILE_FAILURE                  "File system restart required: %s"
#define SYSLOG_HEALTH_PROCESS_FAILURE               "Process restart required: %s"
#define SYSLOG_HEALTH_ETH_LINK_FAILURE              "Interface %s link down.  Resetting interface"
#define SYSLOG_HEALTH_ETH_LINK_SPEED_CHANGE         "Interface %s speed changed to %d Mbps"
#define SYSLOG_HEALTH_CPU_OVERBUSY                  "CPU was %d%% utilized over the last %d seconds"
#define SYSLOG_HEALTH_MEM_OVERFULL                  "Only %d KiB of memory free"
#define SYSLOG_HEALTH_RESOURCES_CHECK_ERROR         "Error while checking cpu and memory utilization"

#define SYSLOG_HEALTH_FLASH_STORAGE_NOT_PRESENT     "Flash storage device not present at %7.7sa"
#define SYSLOG_HEALTH_FLASH_STORAGE_WRONG_MOUNT     "Flash storage device found at %7.7s%c"
#define SYSLOG_HEALTH_FLASH_STORAGE_RESTORE         "Flash storage device restoration being attempted"
#define SYSLOG_HEALTH_FLASH_STORAGE_NOT_CHECKED     "Flash storage device busy, not checked"
#define SYSLOG_HEALTH_FLASH_STORAGE_ERRORS          "Flash storage device errors detected"
#define SYSLOG_HEALTH_FLASH_STORAGE_LOCK_FAILURE    "Flash storage device lock unobtainable after %d tries."
#define SYSLOG_HEALTH_FLASH_STORAGE_LOCK_RELEASE    "Flash storage device lock cannot be released."
#define SYSLOG_HEALTH_FLASH_STORAGE_RESTORE_FAIL    "Flash storage device restoration failed (status: %d)."

//****************************************************************
// FIPS-required test failures
//----------------------------------------------------------------
#define SYSLOG_FIPS_FIRMWARE_INTEGRITY_FAILURE      "Firmware integrity test failed.  ""System has entered an error state and will be rebooted."
#define SYSLOG_FIPS_CONT_RNG_TEST_FAILURE           "Continuous RNG test failed.  "    "System has entered an error state and will be rebooted."

//****************************************************************
// PCI Audit failures
//----------------------------------------------------------------
#define SYSLOG_PCI_TELNET_ON                        "PCI audit failure: telnet enabled"
#define SYSLOG_PCI_ADMIN_RADIUS_OFF                 "PCI audit failure: admin RADIUS authentication disabled"
#define SYSLOG_PCI_SYSLOG_OFF                       "PCI audit failure: syslog disabled"
#define SYSLOG_PCI_SYSLOG_SERVER_UNDEF              "PCI audit failure: external syslog server undefined"
#define SYSLOG_PCI_SSID_ENC_TOO_WEAK                "PCI audit failure: SSID %s encryption too weak"
#define SYSLOG_PCI_AUDIT_PASSED                     "PCI audit passed : System properly configured for PCI compliance"

//****************************************************************
// LLDP
//----------------------------------------------------------------
#define SYSLOG_LLDPD_FA_ELEMENT_DISCOVERED                      "Fabric Attach element (Type: %s, MAC: %s, IP: %s) was discovered on interface %s"
#define SYSLOG_LLDPD_FA_ELEMENT_AGED_OUT                        "Fabric Attach element (Type: %s, MAC: %s, IP: %s) aged out on interface %s"
#define SYSLOG_LLDPD_FA_MGMT_VLAN_MISMATCH                      "Fabric Attach management VLAN mismatch detected: default route VLAN congifured is %d, while management VLAN specified on the network is %d"
#define SYSLOG_LLDPD_POWER_ALLOCATED                            "Requested PoE power allocated"
#define SYSLOG_LLDPD_POWER_ALLOCATED_ENABLE_IAPS                "Requested PoE power allocated. Enabling IAPs that were disabled on boot."
#define SYSLOG_LLDPD_INSUFFICIENT_POWER_ALLOCATED               "Insufficient PoE power allocated"
#define SYSLOG_LLDPD_INSUFFICIENT_POWER_ALLOCATED_ENABLE_IAPS   "Insufficient PoE power allocated. Enabling IAPs that were disabled on boot in 10 minutes."
#define SYSLOG_LLDPD_INSUFFICIENT_POWER_ALLOCATED_ENABLE_NOW    "Insufficient PoE power allocated. Enabling IAPs that were disabled on boot."
#define SYSLOG_LLDPD_NO_POWER_ALLOCATED                         "No PoE power allocated"
#define SYSLOG_LLDPD_NO_POWER_ALLOCATED_ENABLE_IAPS             "No PoE power allocated. Enabling IAPs that were disabled on boot in 10 minutes."
#define SYSLOG_LLDPD_NO_POWER_ALLOCATED_NO_LLDP_PKT             "No PoE power allocated due to no LLDP packets received"
#define SYSLOG_LLDPD_NO_POWER_ALLOCATED_NO_LLDP_PKT_ENABLE_IAPS "No PoE power allocated due to no LLDP packets received. Enabling IAPs that were disabled on boot in %d minutes."

//****************************************************************
// Mace (??)
//----------------------------------------------------------------
#define STR_SYSLOG_NONE                             NULL

// access control list info
#define STR_SYSLOG_ACL                              "ACL state changed to %s"

// administrator info
#define STR_SYSLOG_ADMIN_PASSWORD                   "Admin user %s password changed"
#define STR_SYSLOG_ADMIN_PRIVILEGE                  "Admin user %s privilege changed to %s"

// location & description info
#define STR_SYSLOG_ARRAY_LOCATION                   "System location changed to %s"
#define STR_SYSLOG_ARRAY_NAME                       "System name changed to %s"

// contect info
#define STR_SYSLOG_CONTACT_EMAIL                    "Contact email changed to %s"
#define STR_SYSLOG_CONTACT_NAME                     "Contact name  changed to %s"
#define STR_SYSLOG_CONTACT_PHONE                    "Contact phone changed to %s"

// dhcp-server info

// dns info
#define STR_SYSLOG_DNS_DOMAIN                       "DNS domain changed to %s"
#define STR_SYSLOG_DNS_HOSTNAME                     "DNS hostname changed to %s"
#define STR_SYSLOG_DNS_SERVER1                      "DNS server1 changed to %s"
#define STR_SYSLOG_DNS_SERVER2                      "DNS server2 changed to %s"
#define STR_SYSLOG_DNS_SERVER3                      "DNS server3 changed to %s"
#define STR_SYSLOG_DNS_IGNORE_DOMAIN                "DNS domain assignment to %s ignored"
#define STR_SYSLOG_DNS_IGNORE_HOSTNAME              "DNS hostname assignment to %s ignored"
#define STR_SYSLOG_DNS_IGNORE_SERVER                "DNS server%d assignment to %s ignored"

// eth info
#define STR_SYSLOG_ETH_MANAGEMENT                   "Interface %s management changed to %s"
#define STR_SYSLOG_ETH_AUTONEG                      "Interface %s autonegotiation changed to %s"
#define STR_SYSLOG_ETH_DHCP_BIND                    "Interface %s DHCP client changed to %s"
#define STR_SYSLOG_ETH_DUPLEX                       "Interface %s duplex changed to %s"
#define STR_SYSLOG_ETH_ENABLED                      "Interface %s state changed to %s"
#define STR_SYSLOG_ETH_IPV4_GATEWAY                 "Interface %s IPv4 gateway changed to %s"
#define STR_SYSLOG_ETH_IPV4_ADDRESS                 "Interface %s IPv4 address changed to %s"
#define STR_SYSLOG_ETH_IPV4_MASK                    "Interface %s IPv4 mask changed to %s"
#define STR_SYSLOG_ETH_MTU                          "Interface %s MTU changed to %d"
#define STR_SYSLOG_ETH_SPEED                        "Interface %s speed changed to %d"
#define STR_SYSLOG_ETH_LINK                         "Interface %s link is %s"

// console info
#define STR_SYSLOG_CONSOLE_BAUD                     "Console baud rate changed to %d"
#define STR_SYSLOG_CONSOLE_PARITY                   "Console parity changed to %s"
#define STR_SYSLOG_CONSOLE_STOPBITS                 "Console stopbits changed to %d"
#define STR_SYSLOG_CONSOLE_TIMEOUT                  "Console idle timeout changed to %d"
#define STR_SYSLOG_CONSOLE_BYTESIZE                 "Console byte size changed to %d"

// ntp server info
#define STR_SYSLOG_NTP_ENABLED                      "NTP state changed to %s\n"
#define STR_SYSLOG_NTP_PRIMARY                      "NTP primary server changed to %s\n"
#define STR_SYSLOG_NTP_SECONDARY                    "NTP secondary server changed to %s\n"

// a radios info
#define STR_SYSLOG_RADIO_CELLSIZE                   "Interface IAP %s cell size changed to %s"
#define STR_SYSLOG_RADIO_CHANNEL                    "Interface IAP %s channel changed to %d"
#define STR_SYSLOG_RADIO_DESCRIPTION                "Interface IAP %s description changed to %s"
#define STR_SYSLOG_RADIO_ENABLED                    "Interface IAP %s state changed to %s"
#define STR_SYSLOG_RADIO_TX_POWER_MAX               "Interface IAP %s client TX power changed to %d"
#define STR_SYSLOG_RADIO_RX_THRESHOLD               "Interface IAP %s RX threshold changed to %d"
#define STR_SYSLOG_RADIO_TX_POWER                   "Interface IAP %s TX power changed to %d"
// abg radios info
#define STR_SYSLOG_RADIO_ANTENNA                    "Interface IAP %s antenna changed to %s"
#define STR_SYSLOG_RADIO_BAND                       "Interface IAP %s band changed to %s"

// radius info
#define STR_SYSLOG_RADIUS_ENABLED                   "Default RADIUS server selection changed to %s"
#define STR_SYSLOG_RADIUS_PRIMARY_PORT              "Default External RADIUS server primary port changed to %d"
#define STR_SYSLOG_RADIUS_PRIMARY_SECRET            "Default External RADIUS server primary secret changed"
#define STR_SYSLOG_RADIUS_PRIMARY_IP                "Default External RADIUS server primary IP changed to %s"
#define STR_SYSLOG_RADIUS_SECOND_PORT               "Default External RADIUS server secondary port changed to %d"
#define STR_SYSLOG_RADIUS_SECOND_SECRET             "Default External RADIUS server secondary secret changed"
#define STR_SYSLOG_RADIUS_SECOND_IP                 "Default External RADIUS server secondary IP changed to %s"
#define STR_SYSLOG_RADIUS_TIMEOUT                   "Default External RADIUS server timeout changed to %d"

#define STR_SYSLOG_RADIUS_ADD                       "RADIUS user %s added for SSID %s"
#define STR_SYSLOG_RADIUS_DEL                       "RADIUS user %s deleted"
#define STR_SYSLOG_RADIUS_PASSWORD                  "RADIUS user %s password changed"
#define STR_SYSLOG_RADIUS_SSID                      "RADIUS user %s changed to SSID %s"

// snmp info
#define STR_SYSLOG_SNMP_COMMUNITY                   "SNMP community changed to %s"
#define STR_SYSLOG_SNMP_ENABLED                     "SNMP state changed to %s"
#define STR_SYSLOG_SNMP_AUTH                        "SNMP trap-auth changed to %s"
#define STR_SYSLOG_SNMP_HOST                        "SNMP trap-host changed to %s"
#define STR_SYSLOG_SNMP_PORT                        "SNMP trap-port changed to %d"

// ssid info
#define STR_SYSLOG_SSID_BAND                        "SSID %s band changed to %s"
#define STR_SYSLOG_SSID_ENC                         "SSID %s encryption changed to %s"
#define STR_SYSLOG_SSID_NAME                        "SSID %s added"
#define STR_SYSLOG_SSID_QOS                         "SSID %s QoS level changed to %d"
#define STR_SYSLOG_SSID_VLAN                        "SSID %s VLAN changed to %s"

#define STR_SYSLOG_SSID_ADD                         "SSID %s added"
#define STR_SYSLOG_SSID_DEL                         "SSID %s deleted"
#define STR_SYSLOG_SSID_BROADCAST                   "Broadcast SSID(s) changed, 802.11a: %s, 802.11b/g: %s"
#define STR_SYSLOG_SSID_POOL                        "SSID %s DHCP pool changed to %s"
#define STR_SYSLOG_SSID_ACTIVE_CHG                  "SSID %s state changed to %sactive"

// station activity info
#define STR_SYSLOG_ACTIV_TIMEOUT                    "Station inactivity timeout changed to %d"
#define STR_SYSLOG_ACTIV_REAUTH                     "Station reauthorization period changed to %d"

// syslog info
#define STR_SYSLOG_SYSLOG_CONSOLE                   "Syslog console changed to %s"
#define STR_SYSLOG_SYSLOG_ENABLED                   "Syslog state changed to %s"
#define STR_SYSLOG_SYSLOG_BUFFERED                  "Syslog buffer changed to %d"
#define STR_SYSLOG_SYSLOG_LEVEL                     "Syslog level changed to %d"
#define STR_SYSLOG_SYSLOG_SERVER                    "Syslog server changed to %s"

// security info
#define STR_SYSLOG_SEC_WEP_ENABLED                  "Default WEP support changed to %s"
#define STR_SYSLOG_SEC_WEP_KEY                      "Default WEP key %d changed"
#define STR_SYSLOG_SEC_WEP_KEY_SIZE                 "Default WEP key %d size changed to %s"
#define STR_SYSLOG_SEC_WEP_KEY_DEFAULT              "Default WEP default key changed to %d"
#define STR_SYSLOG_SEC_WPA_ENABLED                  "Default WPA support changed to %s"
#define STR_SYSLOG_SEC_WPA_TKIP_ENABLED             "Default WPA TKIP support changed to %s"
#define STR_SYSLOG_SEC_WPA_AES_ENABLED              "Default WPA AES  support changed to %s"
#define STR_SYSLOG_SEC_WPA_EAP_ENABLED              "Default WPA EAP  support changed to %s"
#define STR_SYSLOG_SEC_WPA_PSK_ENABLED              "Default WPA PSK  support changed to %s"
#define STR_SYSLOG_SEC_WPA_PASSPHRASE               "Default WPA passphrase changed"
#define STR_SYSLOG_SEC_WPA_REKEY                    "Default WPA rekey period changed to %d"

// rogue detect info
#define STR_SYSLOG_IDS_ENABLED                      "Rogue detect state changed to %s"
#define STR_SYSLOG_IDS_DATABASE_CLR                 "Rogue AP SSID list cleared"
#define STR_SYSLOG_IDS_DATABASE_DEL                 "Rogue AP SSID %s deleted"
#define STR_SYSLOG_IDS_DATABASE_ADD                 "Rogue AP SSID %s added as %s"
#define STR_SYSLOG_IDS_DATABASE_CHG                 "Rogue AP SSID %s changed to %s"
#define STR_SYSLOG_IDS_AUTOBLOCK_WLIST_SET          "Rogue AP AutoBlock Channel Whitelist set to %s"
#define STR_SYSLOG_IDS_AUTOBLOCK_WLIST_CLR          "Rogue AP AutoBlock Channel Whitelist cleared"

// global iap info
#define STR_SYSLOG_IAP_BEACON_RATE                  "Interface IAP global beacon rate changed to %d"
#define STR_SYSLOG_IAP_BEACON_DTIM                  "Interface IAP global beacon DTIM changed to %d"
#define STR_SYSLOG_IAP_LONG_RETRIES                 "Interface IAP global long retries changed to %d"
#define STR_SYSLOG_IAP_SHORT_RETRIES                "Interface IAP global short retries changed to %d"
#define STR_SYSLOG_IAP_A_RATES_BASIC                "Interface IAP global 802.11a basic rates changed to %s"
#define STR_SYSLOG_IAP_A_RATES_SUPPORTED            "Interface IAP global 802.11a supported rates changed to %s"
#define STR_SYSLOG_IAP_A_FRAG_THRESHOLD             "Interface IAP global 802.11a fragment threshold changed to %d"
#define STR_SYSLOG_IAP_A_RTS_THRESHOLD              "Interface IAP global 802.11a RTS threshold changed to %d"
#define STR_SYSLOG_IAP_G_RATES_BASIC                "Interface IAP global 802.11bg basic rates changed to %s"
#define STR_SYSLOG_IAP_G_RATES_SUPPORTED            "Interface IAP global 802.11bg supported rates changed to %s"
#define STR_SYSLOG_IAP_G_FRAG_THRESHOLD             "Interface IAP global 802.11bg fragment threshold changed to %d"
#define STR_SYSLOG_IAP_G_RTS_THRESHOLD              "Interface IAP global 802.11bg RTS threshold changed to %d"
#define STR_SYSLOG_IAP_G_SLOT_TIME                  "Interface IAP global 802.11bg slot time changed to %s"
#define STR_SYSLOG_IAP_G_PREAMBLE                   "Interface IAP global 802.11bg preamble changed to %s"
#define STR_SYSLOG_IAP_GLOBAL_LED                   "Interface IAP global LED settings changed"

// misc info
#define STR_SYSLOG_TELNET_ENABLED                   "Telnet enable changed to %s"
#define STR_SYSLOG_SSH_ENABLED                      "SSH enable changed to %s"
#define STR_SYSLOG_TIMEZONE_HOURS                   "Timezone hour offset changed to %d"
#define STR_SYSLOG_TIMEZONE_MINS                    "Timezone minute offset changed to %d"
#define STR_SYSLOG_DOT11G_PROTECT                   "Interface IAP global bg dot11g protect changed to %s"
#define STR_SYSLOG_DOT11G_ONLY                      "Interface IAP global bg dot11g only changed to %s"
#define STR_SYSLOG_AUTOCH_POWERUP                   "Interface IAP global auto channel power_up changed to %s"
#define STR_SYSLOG_AUTOCH_SCHEDULE                  "Interface IAP global auto channel schedule changed to %s"
#define STR_SYSLOG_IAP_STATIONS                     "Interface IAP global maximum IAP stations changed to %d"
#define STR_SYSLOG_IAP_MANAGEMENT                   "Interface IAP global management changed to %s"
#define STR_SYSLOG_TIME_DST                         "Daylight savings adjust state changed to %s\n"
#define STR_SYSLOG_IAP_STA2STA                      "Interface IAP global station to station traffic set to %s"
#define STR_SYSLOG_SEC_WPA2_ENABLED                 "WPA2 support changed to %s"
#define STR_SYSLOG_ETH_PORT_MODE                    "Gigabit ethernet mode changed to %s"
#define STR_SYSLOG_IAP_LOAD_BALANCE                 "Interface IAP global load balancing changed to %s"
#define STR_SYSLOG_IAP_TX_COORD                     "Interface IAP global TX coordination changed to %s"
#define STR_SYSLOG_IAP_RX_COORD                     "Interface IAP global RX coordination changed to %s"
#define STR_SYSLOG_IAP_TXRX_BALANCE                 "Interface IAP global TXRX balancing changed to %s"
#define STR_SYSLOG_MGMT_HTTPS                       "HTTPs enable changed to %s"
#define STR_SYSLOG_LINE_MANAGEMENT                  "Console management changed to %s"
#define STR_SYSLOG_MGMT_TELNET_TIMEOUT              "Telnet idle timeout changed to %d"
#define STR_SYSLOG_MGMT_SSH_TIMEOUT                 "SSH idle timeout changed to %d"
#define STR_SYSLOG_MGMT_HTTPS_TIMEOUT               "HTTPs idle timeout changed to %d"

// security info by ssid
#define STR_SYSLOG_SSID_SEC_WEP_ENABLED             "SSID %s WEP support changed to %s"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_1               "SSID %s WEP key 1 changed"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_2               "SSID %s WEP key 2 changed"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_3               "SSID %s WEP key 3 changed"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_4               "SSID %s WEP key 4 changed"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_SIZE_1          "SSID %s WEP key 1 size changed to %s"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_SIZE_2          "SSID %s WEP key 2 size changed to %s"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_SIZE_3          "SSID %s WEP key 3 size changed to %s"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_SIZE_4          "SSID %s WEP key 4 size changed to %s"
#define STR_SYSLOG_SSID_SEC_WEP_KEY_DEFAULT         "SSID %s WEP default key changed to %s"
#define STR_SYSLOG_SSID_SEC_WPA_ENABLED             "SSID %s WPA support changed to %s"
#define STR_SYSLOG_SSID_SEC_WPA_TKIP_ENABLED        "SSID %s WPA TKIP support changed to %s"
#define STR_SYSLOG_SSID_SEC_WPA_AES_ENABLED         "SSID %s WPA AES  support changed to %s"
#define STR_SYSLOG_SSID_SEC_WPA_EAP_ENABLED         "SSID %s WPA EAP  support changed to %s"
#define STR_SYSLOG_SSID_SEC_WPA_PSK_ENABLED         "SSID %s WPA PSK  support changed to %s"
#define STR_SYSLOG_SSID_SEC_WPA_PASSPHRASE          "SSID %s WPA passphrase changed"
#define STR_SYSLOG_SSID_SEC_WPA_REKEY               "SSID %s WPA rekey period changed to %d"

// radius info by ssid
#define STR_SYSLOG_SSID_RADIUS_ENABLED              "SSID %s RADIUS server selection changed to %s"
#define STR_SYSLOG_SSID_RADIUS_PRIMARY_PORT         "SSID %s external RADIUS server primary port changed to %d"
#define STR_SYSLOG_SSID_RADIUS_PRIMARY_SECRET       "SSID %s external RADIUS server primary secret changed"
#define STR_SYSLOG_SSID_RADIUS_PRIMARY_IP           "SSID %s external RADIUS server primary IP changed to %s"
#define STR_SYSLOG_SSID_RADIUS_SECOND_PORT          "SSID %s external RADIUS server secondary port changed to %d"
#define STR_SYSLOG_SSID_RADIUS_SECOND_SECRET        "SSID %s external RADIUS server secondary secret changed"
#define STR_SYSLOG_SSID_RADIUS_SECOND_IP            "SSID %s external RADIUS server secondary IP changed to %s"
#define STR_SYSLOG_SSID_RADIUS_TIMEOUT              "SSID %s external RADIUS server timeout changed to %d"

// default security info by ssid
#define STR_SYSLOG_SSID_SEC_DEF                     "SSID %s security changed to %s"

// terminator info by ssid

// dhcp-pool info
#define STR_SYSLOG_DHCP_POOL_ENABLED                "DHCP Pool %s state changed to %s"
#define STR_SYSLOG_DHCP_POOL_DEFAULT_LEASE          "DHCP Pool %s default lease time changed to %d"
#define STR_SYSLOG_DHCP_POOL_MAX_LEASE              "DHCP Pool %s max lease time changed to %d"
#define STR_SYSLOG_DHCP_POOL_IP_END                 "DHCP Pool %s IP range end changed to %s"
#define STR_SYSLOG_DHCP_POOL_IP_START               "DHCP Pool %s IP range start changed to %s"
#define STR_SYSLOG_DHCP_POOL_IP_GATEWAY             "DHCP Pool %s IP gateway changed to %s"
#define STR_SYSLOG_DHCP_POOL_IP_MASK                "DHCP Pool %s IP mask changed to %s"
#define STR_SYSLOG_DHCP_POOL_DNS_DOMAIN             "DHCP Pool %s DNS domain changed to %s"
#define STR_SYSLOG_DHCP_POOL_DNS_SERVER1            "DHCP Pool %s DNS server1 changed to %s"
#define STR_SYSLOG_DHCP_POOL_DNS_SERVER2            "DHCP Pool %s DNS server2 changed to %s"
#define STR_SYSLOG_DHCP_POOL_DNS_SERVER3            "DHCP Pool %s DNS server3 changed to %s"

// dhcp pool info by ssid
#define STR_SYSLOG_SSID_DHCP                        "SSID %s DHCP pool changed to %s"

// dhcp pool info by ssid
#define STR_SYSLOG_DHCP_POOL_NAME                   "DHCP pool %s added"
#define STR_SYSLOG_DHCP_POOL_DEL                    "DHCP pool %s deleted"

// dhcp pool number
#define STR_SYSLOG_DHCP_POOL_NUMBER                 "Number of DHCP pools changed to %d"

// wifi tag
#define STR_SYSLOG_WIFI_TAG_START                   "wifi-tag mode: start"
#define STR_SYSLOG_WIFI_TAG_STOP                    "wifi-tag mode: stop"
#define STR_SYSLOG_WIFI_TAG_REPORT_SENT             "Sent wifi-tag report to tag server %s from %s with channel %d RSSI %d"
#define STR_SYSLOG_EKA_WIFI_TAG_REPORT_SENT         "Sent Ekahau wifi-tag report to tag server %s from %s with channel %d RSSI %d"

#define STR_SYSLOG_PROXY_MGMT_HTTP_ENABLED          "HTTP Proxy enabled for management traffic"
#define STR_SYSLOG_PROXY_MGMT_HTTP_DISABLED         "HTTP Proxy disabled for management traffic"
#define STR_SYSLOG_PROXY_MGMT_HTTPS_ENABLED         "HTTPS Proxy enabled for management traffic"
#define STR_SYSLOG_PROXY_MGMT_HTTPS_DISABLED        "HTTPS Proxy disabled for management traffic"
#define STR_SYSLOG_PROXY_MGMT_SOCKS_ENABLED         "SOCKS Proxy enabled for management traffic"
#define STR_SYSLOG_PROXY_MGMT_SOCKS_DISABLED        "SOCKS Proxy disabled for management traffic"
#define STR_SYSLOG_PROXY_MGMT_NET_ADDED             "SOCKS Proxy local network added."
#define STR_SYSLOG_PROXY_MGMT_NET_REMOVED           "SOCKS Proxy local network removed."

//WPA2 security change
#define STR_SYSLOG_WPA2_AES_ON                      "WPA2 encryption requires AES, enabling AES on SSID %s"

#endif //_CFGSTRSYSLOG_H
