//******************************************************************************
/** @file xircfg_iface.h
 *
 * Structures and constants for interface plumbing
 *
 * <I>Copyright (C) 2012 Xirrus. All Rights Reserved</I>
 *
 * This file contains constants and structures that are used between the
 * configuration module and client applications such as WMI, CLI, SNMPD etc.
 *
 * Only constants and structures defined for the cfg request interface should
 * be in here.
 *
 **/
//------------------------------------------------------------------------------
#ifndef _XIRCFG_IFACE_H
#define _XIRCFG_IFACE_H
#include "xircfg.h"

#define MAX_NUM_INTERFACES             100
#define MAX_NUM_IPV4_ROUTES            20
#define MAX_NUM_BONDS                  4
#define MAX_NUM_BRIDGES                100
#define MAX_NUM_TAGGED                 100
#define MAX_NUM_PORTS                  100
#define MAX_NUM_PORTALS                 16

#define REQ_LEN_IFACE_BOND_SLAVES_STR      (MAX_NUM_ETH_INTERFACES * MAX_INTERFACE_BASENAME + 1)
#define REQ_LEN_IFACE_BRIDGE_IFACES_STR    (MAX_NUM_PORTS * MAX_INTERFACE_BASENAME + 1)

enum interface_ip_usage {notused,usedhcp,usestatic,setbydhcp};
struct interface_ip_addr { char str[REQ_LEN_IPV4_ADDRSTR+1]; enum interface_ip_usage use;};
struct interface_hostdom_name { char str[MAXHOSTNAMELEN+1]; enum interface_ip_usage use;};

struct interface_ip_cfg
{
	struct interface_ip_addr addr;
	struct interface_ip_addr mask;
	struct interface_ip_addr gateway;
	struct interface_ip_addr dns1;
	struct interface_ip_addr dns2;
	struct interface_ip_addr dns3;
	struct interface_hostdom_name hostname;
	struct interface_hostdom_name domainname;
	int mtu;
	int nozeroconf;
	char name[MAX_INTERFACE_BASENAME+1];				/* For use in cfg requests and summary for the interface name */
} __attribute__((packed));

#define SIZE_INTERFACE_IP_CFG sizeof(struct interface_ip_cfg)
#define MAX_NUM_IP_LISTS 100

struct ipv4_route_cfg
{
        char name     [REQ_LEN_IFACE_NAME    +1];
	char addr     [REQ_LEN_IPV4_ADDRSTR    +1];
	char mask     [REQ_LEN_IPV4_ADDRSTR    +1];
	char gateway  [REQ_LEN_IPV4_ADDRSTR    +1];
	char iface    [MAX_INTERFACE_BASENAME+1];
	int  status;
} __attribute__((packed));

#define SIZE_INTERFACE_IP_ROUTE sizeof(struct ipv4_route_cfg)

enum interface_type {anyinterface, ethernet, wireless, tunnel, bond, tagged, essid, lbridge};

struct interface_plumbing_cfg
{
	struct interface_plumbing_cfg *parent;
	struct interface_plumbing_cfg *next;
	struct interface_ip_cfg *ip;

	struct {
		int priority;
		int path_cost;
	} brif;

	/* For all intf ptype > mtype */
	enum interface_type mtype;
	enum interface_type ptype;

	int native;
	int vid;
	int admin;

	char parent_name[MAX_INTERFACE_BASENAME+1];

	char my_name[MAX_INTERFACE_BASENAME+1];
} __attribute__((packed));

#define SIZE_INTERFACE_PLUMBING_CFG sizeof(struct interface_plumbing_cfg)

#define plumbing_container(inst, thestruct, themember) \
	(inst ? ((struct thestruct *)((void *)inst - (void *)&(((struct thestruct *)0)->themember))) : NULL)

struct interface_cfg
{
    char   name[MAX_INTERFACE_BASENAME+1];
    uint8  exists;
    uint8  enabled;
    uint16 mtu;
    uint8  mgmt;

} __attribute__((packed));

struct interface_eth_cfg
{
    struct interface_cfg iface;

    uint8  autoneg;
    uint8  duplex;
    uint16 speed;
    uint8  led;
    struct {
        uint8  link;
        uint16 speed;
        uint8  duplex;
    } status;
} __attribute__((packed));

#define SIZE_INTERFACE_ETH_CFG sizeof(struct interface_eth_cfg)

struct interface_bond_cfg
{
    enum BOND_MODE { BALANCE_RR = 0, ACTIVE_BACKUP, BALANCE_XOR, BROADCAST, EIGHT02DOT3_AD, BALANCE_TLB, BALANCE_ALB };
    enum BOND_XMIT_HASH_POLICY { LAYER2, LAYER3PLUS4, LAYER2PLUS3};

    struct interface_cfg iface;

    uint8  mode;
    uint16 mii_mon;
    uint8  xmit_hash_policy;

    char   slaves      [REQ_LEN_IFACE_BOND_SLAVES_STR];
    char   active_vlans[REQ_LEN_ACTIVE_VLANS_STR];
} __attribute__((packed));

#define SIZE_INTERFACE_BOND_CFG  sizeof(struct interface_bond_cfg)

struct interface_bridge_cfg
{
   struct interface_cfg iface;

    uint8  stp;
    uint16 priority;
    uint16 fwd_delay;
    uint16 max_age;
    uint16 helo_time;
    uint16 aging_time;

    char   ifaces[REQ_LEN_IFACE_BRIDGE_IFACES_STR];

} __attribute__((packed));
#define SIZE_INTERFACE_BRIDGE_CFG sizeof(struct interface_bridge_cfg)

struct interface_tagged_cfg
{
   struct interface_cfg iface;

    uint16 vtag;
    char physdev[MAX_INTERFACE_BASENAME+1];

} __attribute__((packed));

#define SIZE_INTERFACE_TAGGED_CFG sizeof(struct interface_tagged_cfg)

#define REQ_LEN_IFACE_TUNNEL_MODE_STR (32)

struct interface_tunnel_cfg
{
    struct interface_cfg iface;

    char  mode[REQ_LEN_IFACE_TUNNEL_MODE_STR];
             #define IFACE_TUNNEL_MODE_STR_GRE         "gre"
             #define IFACE_TUNNEL_MODE_STR_IPIP        "ipip"
             #define IFACE_TUNNEL_MODE_STR_VTUN_SRVR   "vtun-server"
             #define IFACE_TUNNEL_MODE_STR_VTUN_CLIENT "vtun-client"
             #define IFACE_TUNNEL_MODE_STR_VPN_SRVR    "vpn-server"
             #define IFACE_TUNNEL_MODE_STR_VPN_P2P     "vpn-p2p"
             #define IFACE_TUNNEL_MODE_STR_VPN_CLIENT  "vpn-client"

    uint8  security;
             #define TUNNEL_SEC_NONE                   0
             #define TUNNEL_SEC_VPN_STATIC_KEY         1
             #define TUNNEL_SEC_VPN_TLS_SERVER         2
             #define TUNNEL_SEC_VPN_TLS_CLIENT         3
             #define TUNNEL_SEC_VTUN_DEFAULT           4
             #define TUNNEL_SEC_VTUN_BLOWFISH_128_ECB  5
             #define TUNNEL_SEC_VTUN_BLOWFISH_128_CBC  6
             #define TUNNEL_SEC_VTUN_BLOWFISH_128_CFB  7
             #define TUNNEL_SEC_VTUN_BLOWFISH_128_OFB  8
             #define TUNNEL_SEC_VTUN_BLOWFISH_256_ECB  9
             #define TUNNEL_SEC_VTUN_BLOWFISH_256_CBC 10
             #define TUNNEL_SEC_VTUN_BLOWFISH_256_CFB 11
             #define TUNNEL_SEC_VTUN_BLOWFISH_256_OFB 12
             #define TUNNEL_SEC_VTUN_AES_128_ECB      13
             #define TUNNEL_SEC_VTUN_AES_128_CBC      14
             #define TUNNEL_SEC_VTUN_AES_128_CFB      15
             #define TUNNEL_SEC_VTUN_AES_128_OFB      16
             #define TUNNEL_SEC_VTUN_AES_256_ECB      17
             #define TUNNEL_SEC_VTUN_AES_256_CBC      18
             #define TUNNEL_SEC_VTUN_AES_256_CFB      19
             #define TUNNEL_SEC_VTUN_AES_256_OFB      20

    uint8  layer;

    char   local    [REQ_LEN_IPV4_ADDRSTR];
    char   remote[2][MAX_INTERFACE_BASENAME];

    char   dst    [REQ_LEN_IPV4_ADDRSTR];
    uint16 port;
    uint8  proto;
    char   subnet [REQ_LEN_IPV4_ADDRSTR];
    char   submask[REQ_LEN_IPV4_ADDRSTR];
    char   secret [REQ_LEN_RAD_SECRET];
    char   static_key[REQ_LEN_STR];
    char   tls_ca  [REQ_LEN_STR];
    char   tls_cert[REQ_LEN_STR];
    char   tls_key [REQ_LEN_STR];

    uint16 interval;
    uint16 failures;
} __attribute__ ((packed));

#define SIZE_TUNNEL_CFG sizeof(struct tunnel_cfg)

struct interface_port_cfg
{
   struct interface_cfg iface;

    uint8  dhcp_bind;
    char   ip_addr[REQ_LEN_IPV4_ADDRSTR];
    char   mask   [REQ_LEN_IPV4_ADDRSTR];
    char   gateway[REQ_LEN_IPV4_ADDRSTR];

    char   dhcp_pool[REQ_LEN_DHCP_POOL_NAME];
} __attribute__((packed));

#define SIZE_INTERFACE_PORT_CFG sizeof(struct interface_port_cfg)

struct interface_wdsl_cfg
{
	struct interface_plumbing_cfg plumbing;
} __attribute__((packed));

#define SIZE_INTERFACE_WDSL_CFG sizeof(struct interface_wdsl_cfg)
#define PLUMBING_TO_WDSL(inst) plumbing_container(inst, interface_wdsl_cfg, plumbing)

struct interface_iap_cfg
{
	struct radio_stats stats;
	u32 time_cleared;
    char radio_name[MAX_INTERFACE_BASENAME+1];
    int bssid;
	struct interface_plumbing_cfg plumbing;
} __attribute__((packed));

#define SIZE_INTERFACE_IAP_CFG  sizeof(struct interface_iap_cfg)
#define PLUMBING_TO_IAP(inst) plumbing_container(inst, interface_iap_cfg, plumbing)

#define SIZE_INTERFACE_BOND_CFG  sizeof(struct interface_bond_cfg)

struct interface_ssid_cfg
{
    struct ssid_cfg ssid;

	struct interface_plumbing_cfg plumbing;
} __attribute__((packed));

#define SIZE_INTERFACE_SSID_CFG sizeof(struct interface_ssid_cfg)
#define PLUMBING_TO_SSID(inst) plumbing_container(inst, interface_ssid_cfg, plumbing)
#define SIZE_INTERFACE_ESSID sizeof(struct ssid_cfg)

#endif // _XIRCFG_IFACE_H

