#include "xirver.h"
#include "xircfg.h"

/************************************************************************************************************************
 * Object specific string tables (number of strings per table must be a power of 2)                                     *
 ************************************************************************************************************************/
#define su      "sun "
#define m       "mon "
#define tu      "tue "
#define w       "wed "
#define th      "thu "
#define f       "fri "
#define sa      "sat "

#define SU      "Sun "
#define M       "Mon "
#define TU      "Tue "
#define W       "Wed "
#define TH      "Thu "
#define F       "Fri "
#define SA      "Sat "

#define SU_     "Sun,"
#define M_      "Mon,"
#define TU_     "Tue,"
#define W_      "Wed,"
#define TH_     "Thu,"
#define F_      "Fri,"
#define SA_     "Sat,"

#define rpm     "RPM"
#define rpm_    "RPM"      "+"
#define ram     "RAM"
#define ram_    "RAM"      "+"
#define rsm     "RSM"
#define rsm_    "RSM"      "+"
#define app     "APP"
#define app_    "APP"      "+"
#define psb     "PSB"
#define psb_    "PSB"      "+"
#define end     "END"
#define end_    "END"      "+"
#define dn      "802.11n"
#define dn_     "802.11n"  "+"
#define ac      "802.11ac"
#define ac_     "802.11ac" "+"

#define RPM     "RF Performance Manager"
#define RPM_    "RF Performance Manager" " + "
#define RAM     "RF Analysis" " Manager"
#define RAM_    "RF Analysis" " Manager" " + "
#define RSM     "RF Security" " Manager"
#define RSM_    "RF Security" " Manager" " + "
#define APP     "Application Control"
#define APP_    "Application Control"    " + "
#define PSB     "Public Safety Band"
#define PSB_    "Public Safety Band"     " + "
#define END     "Enhanced Debug"
#define END_    "Enhanced Debug"         " + "
#define DN      "802.11n"
#define DN_     "802.11n"                " + "
#define AC      "802.11ac"
#define AC_     "802.11ac"               " + "

#define XF_IP4  "ipv4-address"   " "
#define XF_IP6  "ipv6-address"   " "
#define XF_HST  "hostname"       " "
#define XF_NBN  "netbios-name"   " "
#define XF_UAS  "user-agent-str" " "
#define XF_IPA   XF_IP4 XF_IP6

#define XC_IP4  "IPv4-Address"
#define XC_IP4_ "IPv4-Address"   ", "
#define XC_IP6  "IPv6-Address"
#define XC_IP6_ "IPv6-Address"   ", "
#define XC_HST  "Hostname"
#define XC_HST_ "Hostname"       ", "
#define XC_NBN  "Netbios-Name"
#define XC_NBN_ "Netbios-Name"   ", "
#define XC_UAS  "User-Agent-Str"
#define XC_UAS_ "User-Agent-Str" ", "
#define XC_IPA   XC_IP4_ XC_IP6
#define XC_IPA_  XC_IP4_ XC_IP6_

#define XS_IP4  "IPv4"
#define XS_IP4_ "IPv4"       ", "
#define XS_IP6  "IPv6"
#define XS_IP6_ "IPv6"       ", "
#define XS_HST  "Host"
#define XS_HST_ "Host"       ", "
#define XS_NBN  "Netbios"
#define XS_NBN_ "Netbios"    ", "
#define XS_UAS  "User-Agent"
#define XS_UAS_ "User-Agent" ", "
#define XS_IPA   XS_IP4_ XS_IP6
#define XS_IPA_  XS_IP4_ XS_IP6_

#define AC_BE   "best-effort"
#define AC_BK   "background"
#define AC_VI   "video"
#define AC_VO   "voice"

#define iso_no  "none"
#define iso_no_ "none"         " "
#define iso_ug  "user-group"
#define iso_ug_ "user-group"   " "
#define iso_ap  "access-point"
#define iso_ap_ "access-point" " "
#define iso_ne  "ap-neighbors"
#define iso_ne_ "ap-neighbors" " "
#define iso_fl  "fixed-list"
#define iso_fl_ "fixed-list"   " "

#define ISO_NO  "None"
#define ISO_NO_ "None"         " & "
#define ISO_UG  "User-Group"
#define ISO_UG_ "User-Group"   " & "
#define ISO_AP  "Access-Point"
#define ISO_AP_ "Access-Point" " & "
#define ISO_NE  "AP-Neighbors"
#define ISO_NE_ "AP-Neighbors" " & "
#define ISO_FL  "Fixed-List"
#define ISO_FL_ "Fixed-List"   " & "

const char *       acl_strs[] = { "off"                            , "on allow-list"                  , "on deny-list"                   , "on deny-list inc-blocked-rogues", };
const char *       Acl_Strs[] = { "Disabled"                       , "Allow list"                     , "Deny list"                      , "Deny List, inc. blocked rogues" , };
const char *  acl_abbr_strs[] = { "off"                            , "allow"                          , "deny"                           , ""                               , };
const char *  Acl_Abbr_Strs[] = { "Off"                            , "Allow"                          , "Deny"                           , ""                               , };
const char *    ac_neg_strs[] = { "negotiating"                    , "waiting for dfs"                , "completed"                      , "timed out"                      ,
                                  "failed"                         , ""                               , ""                               , ""                               , };
const char *   ac_abbr_strs[] = { ""                               , "wait dfs"                       , "done"                           , "timed out"                      ,
                                  "failed"                         , ""                               , ""                               , ""                               , };
const char *  ac_state_strs[] = { "inactive"                       , "request"                        , "acquired"                       , "active"                         ,
                                  "negotiated"                     , "scanning"                       , "searching"                      , "complete"                       , };
const char *    active_strs[] = { "inactive"                       , "active"                         , ""                               , ""                               , };
const char *    admin_iface[] = { "CLI via console"                , "CLI via telnet"                 , "CLI via ssh"                    , "WMI via https"                  , };
const char *       ant_strs[] = { "external"                       , "internal directional"           , ""                               , ""                               , };
const char *   ant_ext_strs[] = { ""                               , "external"                       , ""                               , ""                               , };
const char *  ant_type_strs[] = { "external"                       , "internal directional"           , "internal omni"                  , ""                               , };
const char *  ant_abbr_strs[] = { "external"                       , "int-dir "                       , "int-omni"                       , ""                               , };
const char *      auth_strs[] = { "open"                           , "radius-mac"                     , "802-1x"                         , ""                               , };
const char *      Auth_Strs[] = { "Open"                           , "Radius-MAC"                     , "802.1x"                         , ""                               , };
const char *       arp_strs[] = { "off"                            , "pass-thru"                      , "proxy"                          , ""                               , };
const char *       Arp_Strs[] = { "Off"                            , "Pass-thru"                      , "Proxy"                          , ""                               , };
const char * assurance_strs[] = { "off"                            , "alert-only"                     , "repair-without-reboot"          , "reboot-allowed"                 , };
const char * Assurance_Strs[] = { "Disabled"                       , "Alert only"                     , "Repair w/o reboot"              , "Repair, reboot allowed"         , };
const char *    aw_err_strs[] = { "block"                          , "allow"                          , ""                               , ""                               , };
const char *    Aw_Err_Strs[] = { "Block"                          , "Allow"                          , ""                               , ""                               , };
const char *      band_strs[] = { "5GHz"                           , "2.4GHz"                         , ""                               , ""                               , };
const char *  band_sup_strs[] = { ""                               , ""                               , "2.4GHz"                         , ""                               ,
                                  ""                               , "5GHz"                           , ""                               , "both"                           , };
const char *      bool_strs[] = { "false"                          , "true"                           , ""                               , "N/A"                            , };
const char *      bond_strs[] = { "off"                            , "+1"                             , ""                               , "-1"                             , };
const char * bond_mode_strs[] = { "link-backup"                    , "link-backup"                    , "link-backup"                    , "load-balance"                   ,
                                  "bridge"                         , "802.3ad"                        , "broadcast"                      , "mirror"                         ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , "N/A"                            , };
const char * Bond_Mode_Strs[] = { "Link backup"                    , "Link backup"                    , "Link backup"                    , "Load balance"                   ,
                                  "Bridge"                         , "802.3ad"                        , "Broadcast"                      , "Mirror"                         ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , "N/A"                            , };
const char * bond_name_strs[] = { ""                               , "bond1"                          , "bond2"                          , "bond3"                          ,
                                  "bond4"                          , "bond5"                          , ""                               , "N/A"                            , };
const char *   bond_op_strs[] = { "active-backup"                  , "active-backup"                  , "active-backup"                  , "balance-xor"                    ,
                                  "active-backup"                  , "802.3ad"                        , "broadcast"                      , "active-backup"                  ,
                                  "active-backup"                  , "active-backup"                  , "active-backup"                  , "active-backup"                  ,
                                  "active-backup"                  , "active-backup"                  , "active-backup"                  , "active-backup"                  , };
const char *  bond_prt_strs[] = { ""                               , "eth0"                           , "gig1"                           , "eth0 gig1"                      ,
                                  "gig2"                           , "eth0 gig2"                      , "gig1 gig2"                      , "eth0 gig1 gig2"                 ,
                                       "gig3"                      , "eth0 "   "gig3"                 , "gig1 "   "gig3"                 , "eth0 gig1 "   "gig3"            ,
                                  "gig2 gig3"                      , "eth0 gig2 gig3"                 , "gig1 gig2 gig3"                 , "eth0 gig1 gig2 gig3"            ,
                                            "gig4"                 , "eth0 "        "gig4"            , "gig1 "        "gig4"            , "eth0 gig1 "        "gig4"       ,
                                  "gig2 "   "gig4"                 , "eth0 gig2 "   "gig4"            , "gig1 gig2 "   "gig4"            , "eth0 gig1 gig2 "  " gig4"       ,
                                       "gig3 gig4"                 , "eth0 "   "gig3 gig4"            , "gig1 "   "gig3 gig4"            , "eth0 gig1 "   "gig3 gig4"       ,
                                  "gig2 gig3 gig4"                 , "eth0 gig2 gig3 gig4"            , "gig1 gig2 gig3 gig4"            , "eth0 gig1 gig2 gig3 gig4"       ,
                                  ""             "gig5"            , "eth0 "             "gig5"       , "gig1 "             "gig5"       , "eth0 gig1 "             "gig5"  ,
                                  "gig2 "        "gig5"            , "eth0 gig2 "        "gig5"       , "gig1 gig2 "        "gig5"       , "eth0 gig1 gig2 "        "gig5"  ,
                                       "gig3 "   "gig5"            , "eth0 "   "gig3 "   "gig5"       , "gig1 "   "gig3 "   "gig5"       , "eth0 gig1 "   "gig3 "   "gig5"  ,
                                  "gig2 gig3 "   "gig5"            , "eth0 gig2 gig3 "   "gig5"       , "gig1 gig2 gig3 "   "gig5"       , "eth0 gig1 gig2 gig3 "   "gig5"  ,
                                            "gig4 gig5"            , "eth0 "        "gig4 gig5"       , "gig1 "        "gig4 gig5"       , "eth0 gig1 "        "gig4 gig5"  ,
                                  "gig2 "   "gig4 gig5"            , "eth0 gig2 "   "gig4 gig5"       , "gig1 gig2 "   "gig4 gig5"       , "eth0 gig1 gig2 "  " gig4 gig5"  ,
                                       "gig3 gig4 gig5"            , "eth0 "   "gig3 gig4 gig5"       , "gig1 "   "gig3 gig4 gig5"       , "eth0 gig1 "   "gig3 gig4 gig5"  ,
                                  "gig2 gig3 gig4 gig5"            , "eth0 gig2 gig3 gig4 gig5"       , "gig1 gig2 gig3 gig4 gig5"       , "eth0 gig1 gig2 gig3 gig4 gig5"  , };
const char * chan_mode_strs[] = { "default"                        , "manual"                         , "automatic"                      , "radar"                          ,
                                  "locked"                         , "dedicated monitor"              , "timeshare monitor"              , ""                               , };
const char * chan_abbr_strs[] = { ""                               , "manual"                         , "auto"                           , "radar"                          ,
                                  "locked"                         , "monitor"                        , "monitor"                        , ""                               , };
const char *      chan_strs[] = { "unlocked"                       , "unlocked"                       , "unlocked"                       , "unlocked"                       ,
                                  "locked"                         , "monitor dedicated"              , "monitor timeshare"              , ""                               , };
const char *    cipher_strs[] = { "none"                           , "RC4"                            , "TKIP"                           , "AES"                            , };
const char *   cb_mode_strs[] = { "dynamic"                        , "static"                         , ""                               , ""                               , };
const char *    cb_pri_strs[] = { "lower"                          , "upper"                          , ""                               , ""                               , };
const char *       cck_strs[] = { "1"                              , "2"                              , "5.5"                            , "11"                             , };
const char *      cell_strs[] = { "manual"                         , "small"                          , "medium"                         , "large"                          ,
                                  "max"                            , "auto"                           , "monitor"                        , ""                               , };
const char * cell_long_strs[] = { "manual"                         , "small"                          , "medium"                         , "large"                          ,
                                  "maximum"                        , "automatic"                      , "monitor"                        , ""                               , };
const char *   chp_pap_strs[] = { "chap"                           , "pap"                            , "ms-chap"                        , ""                               , };
const char *   Chp_Pap_Strs[] = { "CHAP"                           , "PAP"                            , "MS-CHAP"                        , ""                               , };
const char *   connect_strs[] = { "not-connected"                  , "connected"                      , ""                               , ""                               , };
const char *   Connect_Strs[] = { "Not connected"                  , "Connected"                      , ""                               , ""                               , };
const char * das_event_strs[] = { "optional"                       , "required"                       , ""                               , ""                               , };
const char *   default_strs[] = { "unique-settings"                , "global-settings"                , ""                               , ""                               , };
const char *   Default_Strs[] = { "Unique"                         , "Global"                         , ""                               , ""                               , };
const char *    defkey_strs[] = { "1"                              , "2"                              , "3"                              , "4"                              , };
const char *    device_strs[] = { ""                               , "ap"                             , "appliance"                      , "car"                            ,
                                  "desktop"                        , "game"                           , "notebook"                       , "phone"                          ,
                                  "player"                         , "tablet"                         , "watch"                          , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               , };
const char *    Device_Strs[] = { ""                               , "AP"                             , "Appliance"                      , "Car"                            ,
                                  "Desktop"                        , "Game"                           , "Notebook"                       , "Phone"                          ,
                                  "Player"                         , "Tablet"                         , "Watch"                          , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               , };
const char *device_src_strs[] = { ""                               , "manufacturer"                   , "netbios name"                   , "dhcp hostname"                  ,
                                  "user agent string"              , "internal rules"                 , "external rules"                 , "cache"                          ,
                                  "user_mac_table"                 , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               , };
const char *      dhcp_strs[] = { ""                               , "  ipv4 dhcp\n"                  , ""                               , ""                               , };
const char *       dir_strs[] = { "down"                           , "up"                             , ""                               , "N/A"                            , };
const char *      dist_strs[] = { ""                               , " 1 mile"                        , " 2 miles"                       , " 3 miles"                       ,
                                  " 4 miles"                       , " 5 miles"                       , " 6 miles"                       , " 7 miles"                       ,
                                  " 8 miles"                       , " 9 miles"                       , "10 miles"                       , "11 miles"                       ,
                                  "12 miles"                       , "13 miles"                       , "14 miles"                       , "15 miles"                       ,
                                  "16 miles"                       , "17 miles"                       , "18 miles"                       , "19 miles"                       ,
                                  "20 miles"                       , "21 miles"                       , "22 miles"                       , "23 miles"                       ,
                                  "24 miles"                       , "25 miles"                       , "26 miles"                       , "27 miles"                       ,
                                  "28 miles"                       , "29 miles"                       , "30 miles"                       , "31 miles"                       , };
const char * dist_abbr_strs[] = { ""             , " 1mi"          , " 2mi"         , " 3mi"          , " 4mi"         , " 5mi"          , " 6mi"         , " 7mi"          ,
                                  " 8mi"         , " 9mi"          , "10mi"         , "11mi"          , "12mi"         , "13mi"          , "14mi"         , "15mi"          ,
                                  "16mi"         , "17mi"          , "18mi"         , "19mi"          , "20mi"         , "21mi"          , "22mi"         , "23mi"          ,
                                  "24mi"         , "25mi"          , "26mi"         , "27mi"          , "28mi"         , "29mi"          , "30mi"         , "31mi"          , };
const char *      door_strs[] = { "closed"                         , "opened"                         , ""                               , ""                               , };
const char *   dot11ac_strs[] = { "no"                             , "wave-1"                         , "wave-2"                         , ""                               , };
const char *    dot11w_strs[] = { "disable"                        , "capable"                        , "required"                       , ""                               , };
const char *   dot11wd_strs[] = { "disabled"                       , "capable"                        , "required"                       , ""                               , };
const char *    Dot11w_Strs[] = { "Disabled"                       , "Capable"                        , "Required"                       , ""                               , };
const char *    duplex_strs[] = { "half"                           , "full"                           , ""                               , ""                               , };
const char *   enb_dis_strs[] = { "disable"                        , "enable"                         , ""                               , "N/A"                            , };
const char * enbd_disd_strs[] = { "disabled"                       , "enabled"                        , ""                               , "N/A"                            , };
const char * Enbd_Disd_Strs[] = { "Disabled"                       , "Enabled "                       , ""                               , "N/A"                            , };
const char *       enc_strs[] = { "none"                           , "wep"                            , "wpa"                            , "wpa2"                           ,
                                  "wpa-both"                       , ""                               , ""                               , ""                               , };
const char *       Enc_Strs[] = { "None"                           , "WEP"                            , "WPA"                            , "WPA2"                           ,
                                  "WPA Both"                       , ""                               , ""                               , ""                               , };
const char *  eth_bond_strs[] = { "N/A"                            , "1"                              , "2"                              , "3"                              ,
                                  "4"                              , ""                               , ""                               , "N/A"                            , };
const char *       eth_strs[] = { "eth0"                           , "gig1"                           , "gig2"                           , "gig3"                           ,
                                  "gig4"                           , "gig5"                           , "gig6"                           , ""                               , };
const char *   extract_strs[] = { ""                               , XF_IP4                           , XF_IP6                           , XF_IPA                           ,
                                  XF_HST                           , XF_IP4  XF_HST                   , XF_IP6  XF_HST                   , XF_IPA  XF_HST                   ,
                                          XF_NBN                   , XF_IP4          XF_NBN           , XF_IP6          XF_NBN           , XF_IPA          XF_NBN           ,
                                  XF_HST  XF_NBN                   , XF_IP4  XF_HST  XF_NBN           , XF_IP6  XF_HST  XF_NBN           , XF_IPA  XF_HST  XF_NBN           ,
                                                  XF_UAS           , XF_IP4                  XF_UAS   , XF_IP6                  XF_UAS   , XF_IPA                  XF_UAS   ,
                                  XF_HST          XF_UAS           , XF_IP4  XF_HST          XF_UAS   , XF_IP6  XF_HST          XF_UAS   , XF_IPA  XF_HST          XF_UAS   ,
                                          XF_NBN  XF_UAS           , XF_IP4          XF_NBN  XF_UAS   , XF_IP6          XF_NBN  XF_UAS   , XF_IPA          XF_NBN  XF_UAS   ,
                                  XF_HST  XF_NBN  XF_UAS           , XF_IP4  XF_HST  XF_NBN  XF_UAS   , XF_IP6  XF_HST  XF_NBN  XF_UAS   , XF_IPA  XF_HST  XF_NBN  XF_UAS   , };
const char *   Extract_Strs[] = { ""                               , XC_IP4                           , XC_IP6                           , XC_IPA                           ,
                                  XC_HST                           , XC_IP4_ XC_HST                   , XC_IP6_ XC_HST                   , XC_IPA_ XC_HST                   ,
                                          XC_NBN                   , XC_IP4_         XC_NBN           , XC_IP6_         XC_NBN           , XC_IPA_         XC_NBN           ,
                                  XC_HST_ XC_NBN                   , XC_IP4_ XC_HST_ XC_NBN           , XC_IP6_ XC_HST_ XC_NBN           , XC_IPA_ XC_HST_ XC_NBN           ,
                                                  XC_UAS           , XC_IP4_                 XC_UAS   , XC_IP6_                 XC_UAS   , XC_IPA_                 XC_UAS   ,
                                  XC_HST_         XC_UAS           , XC_IP4_ XC_HST_         XC_UAS   , XC_IP6_ XC_HST_         XC_UAS   , XC_IPA_ XC_HST_         XC_UAS   ,
                                          XC_NBN_ XC_UAS           , XC_IP4_         XC_NBN_ XC_UAS   , XC_IP6_         XC_NBN_ XC_UAS   , XC_IPA_         XC_NBN_ XC_UAS   ,
                                  XC_HST_ XC_NBN_ XC_UAS           , XC_IP4_ XC_HST_ XC_NBN_ XC_UAS   , XC_IP6_ XC_HST_ XC_NBN_ XC_UAS   , XC_IPA_ XC_HST_ XC_NBN_ XC_UAS   , };
const char *  Ext_Abbr_Strs[] = { ""                               , XS_IP4                           , XS_IP6                           , XS_IPA                           ,
                                  XS_HST                           , XS_IP4_ XS_HST                   , XS_IP6_ XS_HST                   , XS_IPA_ XS_HST                   ,
                                          XS_NBN                   , XS_IP4_         XS_NBN           , XS_IP6_         XS_NBN           , XS_IPA_         XS_NBN           ,
                                  XS_HST_ XS_NBN                   , XS_IP4_ XS_HST_ XS_NBN           , XS_IP6_ XS_HST_ XS_NBN           , XS_IPA_ XS_HST_ XS_NBN           ,
                                                  XS_UAS           , XS_IP4_                 XS_UAS   , XS_IP6_                 XS_UAS   , XS_IPA_                 XS_UAS   ,
                                  XS_HST_         XS_UAS           , XS_IP4_ XS_HST_         XS_UAS   , XS_IP6_ XS_HST_         XS_UAS   , XS_IPA_ XS_HST_         XS_UAS   ,
                                          XS_NBN_ XS_UAS           , XS_IP4_         XS_NBN_ XS_UAS   , XS_IP6_         XS_NBN_ XS_UAS   , XS_IPA_         XS_NBN_ XS_UAS   ,
                                  XS_HST_ XS_NBN_ XS_UAS           , XS_IP4_ XS_HST_ XS_NBN_ XS_UAS   , XS_IP6_ XS_HST_ XS_NBN_ XS_UAS   , XS_IPA_ XS_HST_ XS_NBN_ XS_UAS   , };
const char *   Fa_type_strs[] = LLDP_FA_TYPE_STRS;
const char * flbk_ssid_strs[] = { "none"                           , "disable-ssid"                   , ""                               , ""                               , };
const char * Flbk_Ssid_Strs[] = { "None"                           , "Disable SSID"                   , ""                               , ""                               , };
const char * flbk_grp_strs [] = { "none"                           , "disable-group"                  , ""                               , ""                               , };
const char * Flbk_Grp_Strs [] = { "None"                           , "Disable Group"                  , ""                               , ""                               , };
const char * Flbk_Abbr_Strs[] = { "None"                           , "Disable"                        , ""                               , ""                               , };
const char * feat_abbr_strs[] = { ""                               ,           rsm                    ,                               dn ,           rsm_                dn ,
                                  rpm                              , rpm_      rsm                    , rpm_                          dn , rpm_      rsm_                dn ,
                                       ram                         ,      ram_ rsm                    ,      ram_                     dn ,      ram_ rsm_                dn ,
                                  rpm_ ram                         , rpm_ ram_ rsm                    , rpm_ ram_                     dn , rpm_ ram_ rsm_                dn ,
                                  ""                  psb          ,           rsm_      psb          ,                     psb_      dn ,           rsm_      psb_      dn ,
                                  rpm_                psb          , rpm_      rsm_      psb          , rpm_                psb_      dn , rpm_      rsm_      psb_      dn ,
                                       ram_           psb          ,      ram_ rsm_      psb          ,      ram_           psb_      dn ,      ram_ rsm_      psb_      dn ,
                                  rpm_ ram_           psb          , rpm_ ram_ rsm_      psb          , rpm_ ram_           psb_      dn , rpm_ ram_ rsm_      psb_      dn ,
                                  ""                       end     ,           rsm_           end     ,                          end_ dn ,           rsm_           end_ dn ,
                                  rpm_                     end     , rpm_      rsm_           end     , rpm_                     end_ dn , rpm_      rsm_           end_ dn ,
                                       ram_                end     ,      ram_ rsm_           end     ,      ram_                end_ dn ,      ram_ rsm_           end_ dn ,
                                  rpm_ ram_                end     , rpm_ ram_ rsm_           end     , rpm_ ram_                end_ dn , rpm_ ram_ rsm_           end_ dn ,
                                  ""                  psb_ end     ,           rsm_      psb_ end     ,                     psb_ end_ dn ,           rsm_      psb_ end_ dn ,
                                  rpm_                psb_ end     , rpm_      rsm_      psb_ end     , rpm_                psb_ end_ dn , rpm_      rsm_      psb_ end_ dn ,
                                       ram_           psb_ end     ,      ram_ rsm_      psb_ end     ,      ram_           psb_ end_ dn ,      ram_ rsm_      psb_ end_ dn ,
                                  rpm_ ram_           psb_ end     , rpm_ ram_ rsm_      psb_ end     , rpm_ ram_           psb_ end_ dn , rpm_ ram_ rsm_      psb_ end_ dn ,
                                  ""             app               ,           rsm  app               ,                app_           dn ,           rsm_ app_           dn ,
                                  rpm            app               , rpm_      rsm  app               , rpm_           app_           dn , rpm_      rsm_ app_           dn ,
                                       ram       app               ,      ram_ rsm  app               ,      ram_      app_           dn ,      ram_ rsm_ app_           dn ,
                                  rpm_ ram       app               , rpm_ ram_ rsm  app               , rpm_ ram_      app_           dn , rpm_ ram_ rsm_ app_           dn ,
                                  ""             app_ psb          ,           rsm_ app_ psb          ,                app_ psb_      dn ,           rsm_ app_ psb_      dn ,
                                  rpm_           app_ psb          , rpm_      rsm_ app_ psb          , rpm_           app_ psb_      dn , rpm_      rsm_ app_ psb_      dn ,
                                       ram_      app_ psb          ,      ram_ rsm_ app_ psb          ,      ram_      app_ psb_      dn ,      ram_ rsm_ app_ psb_      dn ,
                                  rpm_ ram_      app_ psb          , rpm_ ram_ rsm_ app_ psb          , rpm_ ram_      app_ psb_      dn , rpm_ ram_ rsm_ app_ psb_      dn ,
                                  ""             app_      end     ,           rsm_ app_      end     ,                app_      end_ dn ,           rsm_ app_      end_ dn ,
                                  rpm_           app_      end     , rpm_      rsm_ app_      end     , rpm_           app_      end_ dn , rpm_      rsm_ app_      end_ dn ,
                                       ram_      app_      end     ,      ram_ rsm_ app_      end     ,      ram_      app_      end_ dn ,      ram_ rsm_ app_      end_ dn ,
                                  rpm_ ram_      app_      end     , rpm_ ram_ rsm_ app_      end     , rpm_ ram_      app_      end_ dn , rpm_ ram_ rsm_ app_      end_ dn ,
                                  ""             app_ psb_ end     ,           rsm_ app_ psb_ end     ,                app_ psb_ end_ dn ,           rsm_ app_ psb_ end_ dn ,
                                  rpm_           app_ psb_ end     , rpm_      rsm_ app_ psb_ end     , rpm_           app_ psb_ end_ dn , rpm_      rsm_ app_ psb_ end_ dn ,
                                       ram_      app_ psb_ end     ,      ram_ rsm_ app_ psb_ end     ,      ram_      app_ psb_ end_ dn ,      ram_ rsm_ app_ psb_ end_ dn ,
                                  rpm_ ram_      app_ psb_ end     , rpm_ ram_ rsm_ app_ psb_ end     , rpm_ ram_      app_ psb_ end_ dn , rpm_ ram_ rsm_ app_ psb_ end_ dn ,
                                  ""                            ac ,           rsm_                ac ,                               ac ,           rsm_                ac ,
                                  rpm_                          ac , rpm_      rsm_                ac , rpm_                          ac , rpm_      rsm_                ac ,
                                       ram_                     ac ,      ram_ rsm_                ac ,      ram_                     ac ,      ram_ rsm_                ac ,
                                  rpm_ ram_                     ac , rpm_ ram_ rsm_                ac , rpm_ ram_                     ac , rpm_ ram_ rsm_                ac ,
                                  ""                  psb_      ac ,           rsm_      psb_      ac ,                     psb_      ac ,           rsm_      psb_      ac ,
                                  rpm_                psb_      ac , rpm_      rsm_      psb_      ac , rpm_                psb_      ac , rpm_      rsm_      psb_      ac ,
                                       ram_           psb_      ac ,      ram_ rsm_      psb_      ac ,      ram_           psb_      ac ,      ram_ rsm_      psb_      ac ,
                                  rpm_ ram_           psb_      ac , rpm_ ram_ rsm_      psb_      ac , rpm_ ram_           psb_      ac , rpm_ ram_ rsm_      psb_      ac ,
                                  ""                       end_ ac ,           rsm_           end_ ac ,                          end_ ac ,           rsm_           end_ ac ,
                                  rpm_                     end_ ac , rpm_      rsm_           end_ ac , rpm_                     end_ ac , rpm_      rsm_           end_ ac ,
                                       ram_                end_ ac ,      ram_ rsm_           end_ ac ,      ram_                end_ ac ,      ram_ rsm_           end_ ac ,
                                  rpm_ ram_                end_ ac , rpm_ ram_ rsm_           end_ ac , rpm_ ram_                end_ ac , rpm_ ram_ rsm_           end_ ac ,
                                  ""                  psb_ end_ ac ,           rsm_      psb_ end_ ac ,                     psb_ end_ ac ,           rsm_      psb_ end_ ac ,
                                  rpm_                psb_ end_ ac , rpm_      rsm_      psb_ end_ ac , rpm_                psb_ end_ ac , rpm_      rsm_      psb_ end_ ac ,
                                       ram_           psb_ end_ ac ,      ram_ rsm_      psb_ end_ ac ,      ram_           psb_ end_ ac ,      ram_ rsm_      psb_ end_ ac ,
                                  rpm_ ram_           psb_ end_ ac , rpm_ ram_ rsm_      psb_ end_ ac , rpm_ ram_           psb_ end_ ac , rpm_ ram_ rsm_      psb_ end_ ac ,
                                  ""             app_           ac ,           rsm  app_           ac ,                app_           ac ,           rsm_ app_           ac ,
                                  rpm            app_           ac , rpm_      rsm  app_           ac , rpm_           app_           ac , rpm_      rsm_ app_           ac ,
                                       ram       app_           ac ,      ram_ rsm  app_           ac ,      ram_      app_           ac ,      ram_ rsm_ app_           ac ,
                                  rpm_ ram       app_           ac , rpm_ ram_ rsm  app_           ac , rpm_ ram_      app_           ac , rpm_ ram_ rsm_ app_           ac ,
                                  ""             app_ psb_      ac ,           rsm_ app_ psb_      ac ,                app_ psb_      ac ,           rsm_ app_ psb_      ac ,
                                  rpm_           app_ psb_      ac , rpm_      rsm_ app_ psb_      ac , rpm_           app_ psb_      ac , rpm_      rsm_ app_ psb_      ac ,
                                       ram_      app_ psb_      ac ,      ram_ rsm_ app_ psb_      ac ,      ram_      app_ psb_      ac ,      ram_ rsm_ app_ psb_      ac ,
                                  rpm_ ram_      app_ psb_      ac , rpm_ ram_ rsm_ app_ psb_      ac , rpm_ ram_      app_ psb_      ac , rpm_ ram_ rsm_ app_ psb_      ac ,
                                  ""             app_      end_ ac ,           rsm_ app_      end_ ac ,                app_      end_ ac ,           rsm_ app_      end_ ac ,
                                  rpm_           app_      end_ ac , rpm_      rsm_ app_      end_ ac , rpm_           app_      end_ ac , rpm_      rsm_ app_      end_ ac ,
                                       ram_      app_      end_ ac ,      ram_ rsm_ app_      end_ ac ,      ram_      app_      end_ ac ,      ram_ rsm_ app_      end_ ac ,
                                  rpm_ ram_      app_      end_ ac , rpm_ ram_ rsm_ app_      end_ ac , rpm_ ram_      app_      end_ ac , rpm_ ram_ rsm_ app_      end_ ac ,
                                  ""             app_ psb_ end_ ac ,           rsm_ app_ psb_ end_ ac ,                app_ psb_ end_ ac ,           rsm_ app_ psb_ end_ ac ,
                                  rpm_           app_ psb_ end_ ac , rpm_      rsm_ app_ psb_ end_ ac , rpm_           app_ psb_ end_ ac , rpm_      rsm_ app_ psb_ end_ ac ,
                                       ram_      app_ psb_ end_ ac ,      ram_ rsm_ app_ psb_ end_ ac ,      ram_      app_ psb_ end_ ac ,      ram_ rsm_ app_ psb_ end_ ac ,
                                  rpm_ ram_      app_ psb_ end_ ac , rpm_ ram_ rsm_ app_ psb_ end_ ac , rpm_ ram_      app_ psb_ end_ ac , rpm_ ram_ rsm_ app_ psb_ end_ ac , };
const char *   feature_strs[] = { ""                               ,           RSM                    ,                               DN ,           RSM_                DN ,
                                  RPM                              , RPM_      RSM                    , RPM_                          DN , RPM_      RSM_                DN ,
                                       RAM                         ,      RAM_ RSM                    ,      RAM_                     DN ,      RAM_ RSM_                DN ,
                                  RPM_ RAM                         , RPM_ RAM_ RSM                    , RPM_ RAM_                     DN , RPM_ RAM_ RSM_                DN ,
                                  ""                  PSB          ,           RSM_      PSB          ,                     PSB_      DN ,           RSM_      PSB_      DN ,
                                  RPM_                PSB          , RPM_      RSM_      PSB          , RPM_                PSB_      DN , RPM_      RSM_      PSB_      DN ,
                                       RAM_           PSB          ,      RAM_ RSM_      PSB          ,      RAM_           PSB_      DN ,      RAM_ RSM_      PSB_      DN ,
                                  RPM_ RAM_           PSB          , RPM_ RAM_ RSM_      PSB          , RPM_ RAM_           PSB_      DN , RPM_ RAM_ RSM_      PSB_      DN ,
                                  ""                       END     ,           RSM_           END     ,                          END_ DN ,           RSM_           END_ DN ,
                                  RPM_                     END     , RPM_      RSM_           END     , RPM_                     END_ DN , RPM_      RSM_           END_ DN ,
                                       RAM_                END     ,      RAM_ RSM_           END     ,      RAM_                END_ DN ,      RAM_ RSM_           END_ DN ,
                                  RPM_ RAM_                END     , RPM_ RAM_ RSM_           END     , RPM_ RAM_                END_ DN , RPM_ RAM_ RSM_           END_ DN ,
                                  ""                  PSB_ END     ,           RSM_      PSB_ END     ,                     PSB_ END_ DN ,           RSM_      PSB_ END_ DN ,
                                  RPM_                PSB_ END     , RPM_      RSM_      PSB_ END     , RPM_                PSB_ END_ DN , RPM_      RSM_      PSB_ END_ DN ,
                                       RAM_           PSB_ END     ,      RAM_ RSM_      PSB_ END     ,      RAM_           PSB_ END_ DN ,      RAM_ RSM_      PSB_ END_ DN ,
                                  RPM_ RAM_           PSB_ END     , RPM_ RAM_ RSM_      PSB_ END     , RPM_ RAM_           PSB_ END_ DN , RPM_ RAM_ RSM_      PSB_ END_ DN ,
                                  ""             APP               ,           RSM  APP               ,                APP_           DN ,           RSM_ APP_           DN ,
                                  RPM            APP               , RPM_      RSM  APP               , RPM_           APP_           DN , RPM_      RSM_ APP_           DN ,
                                       RAM       APP               ,      RAM_ RSM  APP               ,      RAM_      APP_           DN ,      RAM_ RSM_ APP_           DN ,
                                  RPM_ RAM       APP               , RPM_ RAM_ RSM  APP               , RPM_ RAM_      APP_           DN , RPM_ RAM_ RSM_ APP_           DN ,
                                  ""             APP_ PSB          ,           RSM_ APP_ PSB          ,                APP_ PSB_      DN ,           RSM_ APP_ PSB_      DN ,
                                  RPM_           APP_ PSB          , RPM_      RSM_ APP_ PSB          , RPM_           APP_ PSB_      DN , RPM_      RSM_ APP_ PSB_      DN ,
                                       RAM_      APP_ PSB          ,      RAM_ RSM_ APP_ PSB          ,      RAM_      APP_ PSB_      DN ,      RAM_ RSM_ APP_ PSB_      DN ,
                                  RPM_ RAM_      APP_ PSB          , RPM_ RAM_ RSM_ APP_ PSB          , RPM_ RAM_      APP_ PSB_      DN , RPM_ RAM_ RSM_ APP_ PSB_      DN ,
                                  ""             APP_      END     ,           RSM_ APP_      END     ,                APP_      END_ DN ,           RSM_ APP_      END_ DN ,
                                  RPM_           APP_      END     , RPM_      RSM_ APP_      END     , RPM_           APP_      END_ DN , RPM_      RSM_ APP_      END_ DN ,
                                       RAM_      APP_      END     ,      RAM_ RSM_ APP_      END     ,      RAM_      APP_      END_ DN ,      RAM_ RSM_ APP_      END_ DN ,
                                  RPM_ RAM_      APP_      END     , RPM_ RAM_ RSM_ APP_      END     , RPM_ RAM_      APP_      END_ DN , RPM_ RAM_ RSM_ APP_      END_ DN ,
                                  ""             APP_ PSB_ END     ,           RSM_ APP_ PSB_ END     ,                APP_ PSB_ END_ DN ,           RSM_ APP_ PSB_ END_ DN ,
                                  RPM_           APP_ PSB_ END     , RPM_      RSM_ APP_ PSB_ END     , RPM_           APP_ PSB_ END_ DN , RPM_      RSM_ APP_ PSB_ END_ DN ,
                                       RAM_      APP_ PSB_ END     ,      RAM_ RSM_ APP_ PSB_ END     ,      RAM_      APP_ PSB_ END_ DN ,      RAM_ RSM_ APP_ PSB_ END_ DN ,
                                  RPM_ RAM_      APP_ PSB_ END     , RPM_ RAM_ RSM_ APP_ PSB_ END     , RPM_ RAM_      APP_ PSB_ END_ DN , RPM_ RAM_ RSM_ APP_ PSB_ END_ DN ,
                                  ""                            AC ,           RSM_                AC ,                               AC ,           RSM_                AC ,
                                  RPM_                          AC , RPM_      RSM_                AC , RPM_                          AC , RPM_      RSM_                AC ,
                                       RAM_                     AC ,      RAM_ RSM_                AC ,      RAM_                     AC ,      RAM_ RSM_                AC ,
                                  RPM_ RAM_                     AC , RPM_ RAM_ RSM_                AC , RPM_ RAM_                     AC , RPM_ RAM_ RSM_                AC ,
                                  ""                  PSB_      AC ,           RSM_      PSB_      AC ,                     PSB_      AC ,           RSM_      PSB_      AC ,
                                  RPM_                PSB_      AC , RPM_      RSM_      PSB_      AC , RPM_                PSB_      AC , RPM_      RSM_      PSB_      AC ,
                                       RAM_           PSB_      AC ,      RAM_ RSM_      PSB_      AC ,      RAM_           PSB_      AC ,      RAM_ RSM_      PSB_      AC ,
                                  RPM_ RAM_           PSB_      AC , RPM_ RAM_ RSM_      PSB_      AC , RPM_ RAM_           PSB_      AC , RPM_ RAM_ RSM_      PSB_      AC ,
                                  ""                       END_ AC ,           RSM_           END_ AC ,                          END_ AC ,           RSM_           END_ AC ,
                                  RPM_                     END_ AC , RPM_      RSM_           END_ AC , RPM_                     END_ AC , RPM_      RSM_           END_ AC ,
                                       RAM_                END_ AC ,      RAM_ RSM_           END_ AC ,      RAM_                END_ AC ,      RAM_ RSM_           END_ AC ,
                                  RPM_ RAM_                END_ AC , RPM_ RAM_ RSM_           END_ AC , RPM_ RAM_                END_ AC , RPM_ RAM_ RSM_           END_ AC ,
                                  ""                  PSB_ END_ AC ,           RSM_      PSB_ END_ AC ,                     PSB_ END_ AC ,           RSM_      PSB_ END_ AC ,
                                  RPM_                PSB_ END_ AC , RPM_      RSM_      PSB_ END_ AC , RPM_                PSB_ END_ AC , RPM_      RSM_      PSB_ END_ AC ,
                                       RAM_           PSB_ END_ AC ,      RAM_ RSM_      PSB_ END_ AC ,      RAM_           PSB_ END_ AC ,      RAM_ RSM_      PSB_ END_ AC ,
                                  RPM_ RAM_           PSB_ END_ AC , RPM_ RAM_ RSM_      PSB_ END_ AC , RPM_ RAM_           PSB_ END_ AC , RPM_ RAM_ RSM_      PSB_ END_ AC ,
                                  ""             APP_           AC ,           RSM  APP_           AC ,                APP_           AC ,           RSM_ APP_           AC ,
                                  RPM            APP_           AC , RPM_      RSM  APP_           AC , RPM_           APP_           AC , RPM_      RSM_ APP_           AC ,
                                       RAM       APP_           AC ,      RAM_ RSM  APP_           AC ,      RAM_      APP_           AC ,      RAM_ RSM_ APP_           AC ,
                                  RPM_ RAM       APP_           AC , RPM_ RAM_ RSM  APP_           AC , RPM_ RAM_      APP_           AC , RPM_ RAM_ RSM_ APP_           AC ,
                                  ""             APP_ PSB_      AC ,           RSM_ APP_ PSB_      AC ,                APP_ PSB_      AC ,           RSM_ APP_ PSB_      AC ,
                                  RPM_           APP_ PSB_      AC , RPM_      RSM_ APP_ PSB_      AC , RPM_           APP_ PSB_      AC , RPM_      RSM_ APP_ PSB_      AC ,
                                       RAM_      APP_ PSB_      AC ,      RAM_ RSM_ APP_ PSB_      AC ,      RAM_      APP_ PSB_      AC ,      RAM_ RSM_ APP_ PSB_      AC ,
                                  RPM_ RAM_      APP_ PSB_      AC , RPM_ RAM_ RSM_ APP_ PSB_      AC , RPM_ RAM_      APP_ PSB_      AC , RPM_ RAM_ RSM_ APP_ PSB_      AC ,
                                  ""             APP_      END_ AC ,           RSM_ APP_      END_ AC ,                APP_      END_ AC ,           RSM_ APP_      END_ AC ,
                                  RPM_           APP_      END_ AC , RPM_      RSM_ APP_      END_ AC , RPM_           APP_      END_ AC , RPM_      RSM_ APP_      END_ AC ,
                                       RAM_      APP_      END_ AC ,      RAM_ RSM_ APP_      END_ AC ,      RAM_      APP_      END_ AC ,      RAM_ RSM_ APP_      END_ AC ,
                                  RPM_ RAM_      APP_      END_ AC , RPM_ RAM_ RSM_ APP_      END_ AC , RPM_ RAM_      APP_      END_ AC , RPM_ RAM_ RSM_ APP_      END_ AC ,
                                  ""             APP_ PSB_ END_ AC ,           RSM_ APP_ PSB_ END_ AC ,                APP_ PSB_ END_ AC ,           RSM_ APP_ PSB_ END_ AC ,
                                  RPM_           APP_ PSB_ END_ AC , RPM_      RSM_ APP_ PSB_ END_ AC , RPM_           APP_ PSB_ END_ AC , RPM_      RSM_ APP_ PSB_ END_ AC ,
                                       RAM_      APP_ PSB_ END_ AC ,      RAM_ RSM_ APP_ PSB_ END_ AC ,      RAM_      APP_ PSB_ END_ AC ,      RAM_ RSM_ APP_ PSB_ END_ AC ,
                                  RPM_ RAM_      APP_ PSB_ END_ AC , RPM_ RAM_ RSM_ APP_ PSB_ END_ AC , RPM_ RAM_      APP_ PSB_ END_ AC , RPM_ RAM_ RSM_ APP_ PSB_ END_ AC , };
const char *   gps_ref_strs[] = { "map"                            , "access-point"                   , ""                               , ""                               , };
const char * grp_layer_strs[] = { "layer       2-only"             , "layer       2-and-3"            , "off"                            , ""                               , };
const char *     guard_strs[] = { "long"                           , "short"                          , ""                               , ""                               , };
const char * hs20_link_strs[] = { ""                               , "up"                             , "down"                           , "test"                           , };
const char * Hs20_Link_Strs[] = { ""                               , "Up"                             , "Down"                           , "Test"                           , };
const char * hs20_conn_strs[] = { "closed"                         , "open"                           , "unknown"                        , ""                               , };
const char *       iap_strs[] = { "iap1"                           , "iap2"                           , "iap3"                           , "iap4"                           ,
                                  "iap5"                           , "iap6"                           , "iap7"                           , "iap8"                           ,
                                  "iap9"                           , "iap10"                          , "iap11"                          , "iap12"                          ,
                                  "iap13"                          , "iap14"                          , "iap15"                          , "iap16"                          ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , "global 802.11bg"                , "global 802.11a"                 , "global"                         , };
const char *     iap_n_strs[] = { "iap1"                           , "iap2"                           , "iap3"                           , "iap4"                           ,
                                  "iap5"                           , "iap6"                           , "iap7"                           , "iap8"                           ,
                                  "iap9"                           , "iap10"                          , "iap11"                          , "iap12"                          ,
                                  "iap13"                          , "iap14"                          , "iap15"                          , "iap16"                          ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               ,
                                  "global 802.11n"                 , "global 802.11bg"                , "global 802.11a"                 , "global"                         , };
const char *  iap_type_strs[] = { ""                               , "global"                         , "global 802.11a"                 , "global 802.11bg"                , };
const char *  ids_stat_strs[] = { "unknown"                        , "known"                          , "approved"                       , "blocked"                        ,
                                  "all"                            , ""                               , ""                               , ""                               ,
                                  "active unknown"                 , "active known"                   , "active approved"                , "active blocked"                 ,
                                  "all active"                     , ""                               , ""                               , ""                               , };
const char *  Ids_Stat_Strs[] = { "Unknown"                        , "Known"                          , "Approved"                       , "Blocked"                        ,
                                  "All"                            , "Active"                         , ""                               , ""                               ,
                                  "Active Unknown"                 , "Active Known"                   , "Active Approved"                , "Active Blocked"                 ,
                                  "All Active"                     , ""                               , ""                               , ""                               , };
const char *   Ids_Blk_Strs[] = { "Block unencrypted"              , "Block WEP & unencrypted"        , "Block regardless of encryption" , ""                               , };
const char *   ids_enc_strs[] = { "none"                           , "wep-and-none"                   , "all"                            , ""                               , };
const char *   Ids_Enc_Strs[] = { "None"                           , "WEP & none"                     , "All"                            , ""                               , };
const char *  ids_type_strs[] = { "off"                            , "standard"                       , "advanced"                       , "standard auto-block"            , };
const char *  Ids_Type_Strs[] = { "Off"                            , "Standard"                       , "Advanced"                       , "Standard with auto-blocking"    , };
const char *  ids_mode_strs[] = { "off"                            , "manual"                         , "auto"                           , ""                               , };
const char *   ids_seq_strs[] = { "off"                            , "mgmt"                           , "data"                           , ""                               , };
const char * ids_match_strs[] = { ""                               , "bssid"                          , "ssid"                           , "manufacturer"                   , };
const char * Ids_Match_Strs[] = { ""                               , "BSSID"                          , "SSID"                           , "Manufacturer"                   , };
const char * ids_ntype_strs[] = { "all"                            , "ibss"                           , "ess"                            , ""                               , };
const char * Ids_Ntype_Strs[] = { "All"                            , "IBSS/ad-hoc only"               , "ESS/infrastructure only"        , ""                               , };
const char * Ids_Nabbr_Strs[] = { "All"                            , "IBSS only"                      , "ESS only"                       , ""                               , };
const char *   int_ext_strs[] = { "disabled"                       , "external-radius"                , "internal-radius"                , "active-directory"               , };
const char *   Int_Ext_Strs[] = { "Disabled"                       , "External"                       , "Internal"                       , "Active Directory"               , };
const char * isolation_strs[] = { iso_no                           , iso_ug                           , iso_ap                           , iso_ug_ iso_ap                   ,
                                  iso_ne                           , iso_ug_ iso_ne                   , iso_ap_ iso_ne                   , iso_ug_ iso_ap_ iso_ne           ,
                                          iso_fl                   , iso_ug_         iso_fl           , iso_ap_         iso_fl           , iso_ug_ iso_ap_         iso_fl   ,
                                  iso_ne_ iso_fl                   , iso_ug_ iso_ne_ iso_fl           , iso_ap_ iso_ne_ iso_fl           , iso_ug_ iso_ap_ iso_ne_ iso_fl   , };
const char * Isolation_Strs[] = { ISO_NO                           , ISO_UG                           , ISO_AP                           , ISO_UG_ ISO_AP                   ,
                                  ISO_NE                           , ISO_UG_ ISO_NE                   , ISO_AP_ ISO_NE                   , ISO_UG_ ISO_AP_ ISO_NE           ,
                                          ISO_FL                   , ISO_UG_         ISO_FL           , ISO_AP_         ISO_FL           , ISO_UG_ ISO_AP_         ISO_FL   ,
                                  ISO_NE_ ISO_FL                   , ISO_UG_ ISO_NE_ ISO_FL           , ISO_AP_ ISO_NE_ ISO_FL           , ISO_UG_ ISO_AP_ ISO_NE_ ISO_FL   , };
const char *       key_strs[] = { "none"                           , "EAP"                            , "PSK"                            , ""                               , };
const char *    keylen_strs[] = { "not-set"                        , "", "", "", ""                   ,
                                  " 40"                            , "", "", "", ""                   ,                                    /*  5= 40 BIT ASCII */
                                  " 40"                            , "", ""                           ,                                    /* 10= 40 BIT HEX   */
                                  "104"                            , "", "", "", "", "", ""           , "", "", "", "", "", "",            /* 13=104 BIT ASCII */
                                  "104"                            , "", "", "", "", ""               ,                                    /* 26=104 BIT HEX   */             };
const char *    Keylen_Strs[] = { "Not set"                        , "", "", "", ""                   ,
                                  " 40 bits / WEP-64 "             , "", "", "", ""                   ,                                    /*  5= 40 BIT ASCII */
                                  " 40 bits / WEP-64 "             , "", ""                           ,                                    /* 10= 40 BIT HEX   */
                                  "104 bits / WEP-128"             , "", "", "", "", "", ""           , "", "", "", "", "", "",            /* 13=104 BIT ASCII */
                                  "104 bits / WEP-128"             , "", "", "", "", ""               ,                                    /* 26=104 BIT HEX   */             };
const char *       led_strs[] = { ""                               , "beacon"                         , ""                               , "tx-data"                        ,
                                  ""                               , "tx-mgmt"                        , ""                               , "rx-data"                        ,
                                  "rx-mgmt"                        , "broadcast"                      , "probe-req"                      , "assoc"                          ,
                                  ""                               , ""                               , ""                               , ""                               , };
const char *     level_strs[] = { "OFF"                            , "ERROR"                          , "WARN"                           , "INFO"                           ,
                                  "TRACE"                          , ""                               , ""                               , ""                               , };
const char *     limit_strs[] = { "", "limit all pps "             , "limit all kbps "                , "limit sta pps "                 , "limit sta kbps ",     "", "", "", };
const char * limit_cmd_strs[] = { "", "limit-all-pps  "            , "limit-all-kbps "                , "limit-conn-pps "                , "limit-conn-kbps",     "", "", "", };
const char *limit_post_strs[] = { "", " pps"                       , " Kbps"                          , " pps/sta"                       , " Kbps/sta"      ,     "", "", "", };
const char *      link_strs[] = { "1"                              , "2"                              , "3"                              , "4"                              , };
const char *   max_mcs_strs[] = { "mcs7"                           , "mcs8"                           , "mcs9"                           , ""                               , };
const char *   Max_MCS_Strs[] = { "MCS7"                           , "MCS8"                           , "MCS9"                           , ""                               , };
const char *  mdm_auth_strs[] = { "none"                           , "airwatch"                       , ""                               , ""                               , };
const char *  Mdm_Auth_Strs[] = { "None"                           , "AirWatch"                       , ""                               , ""                               , };
const char *     media_strs[] = { "  -"                            , ".11b"                           , ".11g"                           , ".11bg"                          ,
                                  ".11a"                           , ".11ab"                          , ".11ag"                          , ".11abg"                         ,
                                  ".11n"                           , ".11bn"                          , ".11gn"                          , ".11bgn"                         ,
                                  ".11an"                          , ".11abn"                         , ".11agn"                         , ".11abgn"                        ,
                                  ".11ac"                          , ".11bac"                         , ".11gac"                         , ".11bgac"                        ,
                                  ".11aac"                         , ".11abac"                        , ".11agac"                        , ".11abgac"                       ,
                                  ".11nac"                         , ".11bnac"                        , ".11gnac"                        , ".11bgnac"                       ,
                                  ".11anac"                        , ".11abnac"                       , ".11agnac"                       , ".11abgnac"                      , };
const char *      mgmt_strs[] = { "disallowed"                     , "allowed"                        , ""                               , ""                               , };
const char *      Mgmt_Strs[] = { "Disallowed"                     , "Allowed"                        , ""                               , ""                               , };
const char *      mode_strs[] = { "5GHz   "                        , "2.4GHz "                        , "monitor"                        , ""                               , };
const char *   mode_ns_strs[] = { "5GHz"                           , "2.4GHz"                         , "monitor"                        , ""                               , };
const char *    module_strs[] = { "NONE"                           , "WMI"                            , "CLI"                            , "DHCPCD"                         ,
                                  "INTERNAL"                       , "BOOT"                           , "SNMPD"                          , "HEALTH_MON"                     ,
                                  "OOPME"                          , "HOSTAPD"                        , "MONITOR"                        , "CLUSTER"                        ,
                                  "CONFIG"                         , "RCFGD"                          , "WPRD"                           , "DOOR_MON"                       ,
                                  "INT_THREAD"                     , "WEB_PROXY"                      , "CLOUD"                          , "LLDPD"                          ,
                                  "PROXY_MGMT"                     , "21"                             , "22"                             , "23"                             ,
                                  "24"                             , "25"                             , "26"                             , "27"                             ,
                                  "28"                             , "29"                             , "30"                             , "31"                             , };
const char *       mon_strs[] = { "Jan"                            , "Feb"                            , "Mar"                            , "Apr"                            ,
                                  "May"                            , "Jun"                            , "Jul"                            , "Aug"                            ,
                                  "Sep"                            , "Oct"                            , "Nov"                            , "Dec"                            ,
                                  "12?"                            , "13?"                            , "14?"                            , "15?"                            , };
const char *     mount_strs[] = { "face-down"                      , "face-up"                        , ""                               , "N/A"                            , };
const char * multicast_strs[] = { "standard"                       , "convert"                        , "convert+snoop"                  , "convert+snoop+prune"            , };
const char * Multicast_Strs[] = { "Standard"                       , "Convert"                        , "Convert+Snoop"                  , "Convert+Snoop+Prune"            , };
const char * mcst_abbr_strs[] = { "standard"                       , "convert"                        ,         "snoop"                  ,               "prune"            , };
const char * Mcst_Abbr_Strs[] = { "Standard"                       , "Convert"                        ,         "Snoop"                  ,               "Prune"            , };
const char *  mir_bond_strs[] = { "off"                            , "1"                              , "2"                              , "3"                              ,
                                  "4"                              , ""                               , ""                               , "N/A"                            , };
const char *        na_strs[] = { "Hostname unresolved"            , "Connectivity OK"                , "No connectivity"                , "No connectivity"                , };
const char *   netflow_strs[] = { "off"                            , "v5"                             , "v9"                             , "ipfix"                          , };
const char *   NetFlow_strs[] = { "Off"                            , "v5"                             , "v9"                             , "IPFIX"                          , };
const char *  ntp_auth_strs[] = { "none"                           , "md5"                            , "sha1"                           , ""                               , };
const char *  Ntp_Auth_strs[] = { "none"                           , "MD5"                            , "SHA1"                           , ""                               , };
const char *      ofdm_strs[] = { "6"                              , "9"                              , "12"                             , "18"                             ,
                                  "24"                             , "36"                             , "48"                             , "54"                             , };
const char *      omni_strs[] = { "internal omni"                  , "internal directional"           , ""                               , ""                               , };
const char * omni_only_strs[] = { "internal omni"                  , "internal omni"                  , ""                               , ""                               , };
const char *    on_off_strs[] = { "off"                            , "on"                             , ""                               , "N/A"                            , };
const char *    On_Off_Strs[] = { "Off"                            , "On"                             , ""                               , ""                               , };
const char *     proxy_strs[] = { "HTTP"                           , "HTTPS"                          , "SOCKS4"                         , "SOCKS5"                         , };
const char *      http_strs[] = { "http"                           , "https"                          , ""                               , ""                               , };
const char *  opt_bcst_strs[] = { "standard"                       , "optimized"                      , ""                               , ""                               , };
const char *  Opt_Bcst_Strs[] = { "Standard"                       , "Optimized"                      , ""                               , ""                               , };
const char *    parity_strs[] = { "none"                           , "odd"                            , "even"                           , ""                               , };
const char *   pending_strs[] = { ""                               , "<pwr>"                          , "<cfg>"                          , "<geo>"                          ,
                                  "<dfs>"                          , "<acn>"                          , "<rst>"                          , ""                               , };
const char *   Pending_Strs[] = { ""                               , "LLDP Power"                     , "Configuration Change"           , "Geography Change"               ,
                                 "DFS Recovery"                    , "Auto Channel"                   , "Channel Reset"                  , ""                               , };
const char *       pre_strs[] = { "long-only"                      , "auto-length"                    , ""                               , ""                               , };
const char *       pri_strs[] = { "emergency"                      , "alert"                          , "critical"                       , "error"                          ,
                                  "warning"                        , "notification"                   , "information"                    , "debugging"                      , };
const char *   protect_strs[] = { "off"                            , "auto-cts"                       , "auto-rts"                       , ""                               , };
const char * proxy_fwd_strs[] = { "off"                            , "bluecoat"                       , "netboxblue"                     , ""                               , };
const char * pwifi_exp_strs[] = { "disable"                        , "enable"                         , "timed"                          , "N/A"                            , };
const char * Pwifi_Exp_Strs[] = { "Disabled"                       , "Enabled"                        , "Timed"                          , "N/A"                            , };
const char *       psk_strs[] = { "off"                            , "on"                             , "u-psk"                          , ""                               , };
const char *       Psk_Strs[] = { "Off"                            , "On"                             , "U-PSK"                          , ""                               , };
const char *       qos_strs[] = { "best effort"                    , "background"                     , "video"                          , "voice"                          , };
const char * rad_staid_strs[] = { "bssid-ssid"                     , "bssid"                          , "ethernet-mac"                   , ""                               , };
const char * Rad_Staid_Strs[] = { "BSSID:SSID"                     , "BSSID"                          , "Ethernet-MAC"                   , ""                               , };
const char *   rad_mac_strs[] = { "lower"                          , "upper"                          , "lower-hyphen"                   , "upper-hyphen"                   , };
const char *    rf_mon_strs[] = { "off"                            , "dedicated"                      , "timeshare"                      , ""                               , };
const char *    RF_Mon_Strs[] = { "Disabled"                       , "Dedicated"                      , "Timeshare"                      , ""                               , };
const char *   RFM_Enb_Strs[] = { "Disabled"                       , "Enabled, dedicated"             , "Enabled, timeshare"             , ""                               , };
const char *    scheme_strs[] = { "wss"                            , "ws"                             , "wsss"                           , ""                               , };
const char *     scope_strs[] = { "map"                            , "global"                         , ""                               , ""                               , };
const char *   Set_Not_Strs[] = { "Not set"                        , "Set"                            , ""                               , ""                               , };
const char *      slot_strs[] = { "auto-time"                      , "short-only"                     , ""                               , ""                               , };
const char * Ssid_Act1_Strs[] = { "Active Bands"                   , "Active Band"                    , "Active Band"                    , "Active Bands"                   , };
const char * Ssid_Act2_Strs[] = { "2.4GHz & 5GHz"                  , "5GHz only"                      , "2.4GHz only"                    , "2.4GHz & 5GHz"                  , };
const char * ssid_band_strs[] = { "both"                           , "5GHz"                           , "2.4GHz"                         , "both"                           , };
const char * Ssid_Band_Strs[] = { " Both "                         , " 5GHz "                         , "2.4GHz"                         , " Both "                         , };
const char * Ssid_BFlg_Strs[] = { "Both"                           , "Both"                           , "5GHz"                           , "5GHz"                           ,
                                  "2.4GHz"                         , "2.4GHz"                         , "Both"                           , "Both"                           , };
const char * Ssid_Flag_Strs[] = { "Both bands, not broadcast"      , "Both bands, broadcast"          , "5GHz band, not broadcast"       , "5GHz band, broadcast"           ,
                                  "2.4GHz band, not broadcast"     , "2.4GHz band, broadcast"         , "Both bands, not broadcast"      , "Both bands, broadcast"          , };
const char * snmp_auth_strs[] = { "md5"                            , "sha"                            , ""                               , ""                               , };
const char * snmp_priv_strs[] = { "des"                            , "aes"                            , ""                               , ""                               , };
const char *   sta_fmt_strs[] = { "standard"                       , "key-value"                      , ""                               , ""                               , };
const char *   sta2sta_strs[] = { "forward"                        , "block"                          , ""                               , ""                               , };
const char *   Sta2Sta_Strs[] = { "Forward"                        , "Block"                          , ""                               , ""                               , };
const char *     state_strs[] = { "down"                           , "up"                             , ""                               , ""                               , };
const char *     State_Strs[] = { "down"                           , "up"                             , ""                               , ""                               , };
const char *       stp_strs[] = { "gig1"                           , "gig2"                           , "gig3"                           , "gig4"                           ,
                                  "gig5"                           , "iap"                            ,
                                  "wds_client_1"                   , "wds_client_2"                   , "wds_client_3"                   , "wds_client_4"                   ,
                                  "wds_host_1"                     , "wds_host_2"                     , "wds_host_3"                     , "wds_host_4"                     ,
                                  ""                               , ""                               ,                                                                       };
const char *    stp_op_strs[] = { "stp-off"                        , "stp-on"                         , ""                               , ""                               , };
const char *    stream_strs[] = { "1x1"                            , "2x2"                            , "3x3"                            , "4x4"                            , };
const char * slog_time_strs[] = { "rfc3164"                        , "rfc3339"                        , ""                               , ""                               , };
const char *  tun_type_strs[] = { "none"                           , "gre"                            , ""                               , ""                                 };
const char *  tx_coord_strs[] = { "off"                            , "on"                             , "cts"                            , ""                               , };
const char *  upsk_err_strs[] = { "block"                          , "allow"                          , ""                               , ""                               , };
const char *  Upsk_Err_Strs[] = { "Block"                          , "Allow"                          , ""                               , ""                               , };
const char *       wds_strs[] = { "none"                           , "C-1"                            , "C-2"                            , "C-3"                            ,
                                  "C-4"                            , "H-1"                            , "H-2"                            , "H-3"                            ,
                                  "H-4"                            , ""                               , ""                               , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               , };
const char *  wds_host_strs[] = { "none"                           , "H-1"                            , "H-2"                            , "H-3"                            ,
                                  "H-4"                            , ""                               , ""                               , ""                               , };
const char *  wds_link_strs[] = { "all WDS links"                  , "all WDS client links"           , "all WDS host links"             , "WDS client link 1"              ,
                                  "WDS client link 2"              , "WDS client link 3"              , "WDS client link 4"              , "WDS host link 1"                ,
                                  "WDS host link 2"                , "WDS host link 3"                , "WDS host link 4"                , ""                               ,
                                  ""                               , ""                               , ""                               , ""                               , };
const char *      wifi_strs[] = { "none"                           , "a-only"                         , "b-only"                         , "ab"                             ,
                                  "g-only"                         , "ag"                             , "bg"                             , "abg"                            ,
                                  "n-only"                         , "an"                             , "bn"                             , "abn"                            ,
                                  "gn"                             , "agn"                            , "bgn"                            , "abgn"                           ,
                                  "ac-only"                        , "aac"                            , "bac"                            , "abac"                           ,
                                  "gac"                            , "agac"                           , "bgac"                           , "abgac"                          ,
                                  "nac"                            , "anac"                           , "bnac"                           , "abnac"                          ,
                                  "gnac"                           , "agnac"                          , "bgnac"                          , "abgnac"                         , };
const char * wifi_cntr_strs[] = { "       "                        , " a-only"                        , " b-only"                        , "  ab   "                        ,
                                  " g-only"                        , "  ag   "                        , "  bg   "                        , " abg   "                        ,
                                  " n-only"                        , "  an   "                        , "  bn   "                        , " abn   "                        ,
                                  "  gn   "                        , " agn   "                        , " bgn   "                        , "abgn   "                        ,
                                  "ac-only"                        , "   aac "                        , "   bac "                        , "  abac "                        ,
                                  "   gac "                        , "  agac "                        , "  bgac "                        , " abgac "                        ,
                                  "   nac "                        , "  anac "                        , "  bnac "                        , " abnac "                        ,
                                  "  gnac "                        , " agnac "                        , " bgnac "                        , "abgnac "                        ,};
const char * wifi_shrt_strs[] = { "none"                           , "a"                              , "b"                              , "ab"                             ,
                                  "g"                              , "ag"                             , "bg"                             , "abg"                            ,
                                  "n"                              , "an"                             , "bn"                             , "abn"                            ,
                                  "gn"                             , "agn"                            , "bgn"                            , "abgn"                           ,
                                  "ac"                             , "aac"                            , "bac"                            , "abac"                           ,
                                  "gac"                            , "agac"                           , "bgac"                           , "abgac"                          ,
                                  "nac"                            , "anac"                           , "bnac"                           , "abnac"                          ,
                                  "gnac"                           , "agnac"                          , "bgnac"                          , "abgnac"                         ,};
const char *   wmm_acm_strs[] = { ""                               , AC_BE                            , AC_BK                            , AC_BE"+"AC_BK                    ,
                                  AC_VI                            , AC_BE"+"AC_VI                    , AC_BK"+"AC_VI                    , AC_BE"+"AC_BK"+"AC_VI            ,
                                          AC_VO                    , AC_BE"+"        AC_VO            , AC_BK"+"        AC_VO            , AC_BE"+"AC_BK"+"        AC_VO    ,
                                  AC_VI"+"AC_VO                    , AC_BE"+"AC_VI"+"AC_VO            , AC_BK"+"AC_VI"+"AC_VO            , AC_BE"+"AC_BK"+"AC_VI"+"AC_VO    , };
const char *       Wpa_Strs[] = {         ""     ,          "EAP"  ,          "PSK",         "PSK+EAP",         ""     ,          "EAP"  ,          "PSK",         "PSK+EAP",
                                      "TKIP"     ,     "TKIP+EAP"  ,     "TKIP+PSK",    "TKIP+PSK+EAP",     "TKIP"     ,     "TKIP+EAP"  ,     "TKIP+PSK",    "TKIP+PSK+EAP",
                                       "AES"     ,      "AES+EAP"  ,      "AES+PSK",     "AES+PSK+EAP",      "AES"     ,      "AES+EAP"  ,      "AES+PSK",     "AES+PSK+EAP",
                                  "AES+TKIP"     , "AES+TKIP+EAP"  , "AES+TKIP+PSK","AES+TKIP+PSK+EAP", "AES+TKIP"     , "AES+TKIP+EAP"  , "AES+TKIP+PSK","AES+TKIP+PSK+EAP", };
const char *      wpa2_strs[] = { "off "                           , "on  "                           , "wpa2"                           , ""                               , };
const char *   wpr_svr_strs[] = { "external"                       , "internal"                       , "cloud"                          , ""                               , };
const char *   Wpr_Svr_Strs[] = { "External"                       , "Internal"                       , "Cloud"                          , ""                               , };
const char *   wpr_lgn_strs[] = { "splash"                         , "login"                          , "none"                           , "hotel"                          ,
                                  ""                               , ""                               , ""                               , "landing-only"                   , };
const char *   Wpr_Lgn_Strs[] = { "Splash"                         , "Login"                          , "None"                           , "Hotel"                          ,
                                  ""                               , ""                               , ""                               , "Landing Only"                   , };
const char *   wpr_hpt_strs[] = { "off"                            , "on"                             , "block"                          , ""                               , };
const char *   Wpr_Hpt_Strs[] = { "Off"                            , "On"                             , "Block"                          , ""                               , };
const char *   xc_abbr_strs[] = { "off"                            , "aos"                            , "boot"                           , "on "                            , };
const char *    xircon_strs[] = { "off"                            , "aos-only"                       , "boot-only"                      , "on"                             , };
const char *    Xircon_Strs[] = { "disabled"                       , "AOS only"                       , "boot loader only"               , "enabled"                        , };
const char *  xrp_mode_strs[] = { "off"                            , "broadcast"                      , "tunnel"                         , ""                               , };
const char *  Xrp_Mode_Strs[] = { "Off"                            , "Broadcast"                      , "Tunneled"                       , ""                               , };
const char *  xrp_peer_strs[] = { "target-only"                    , "in-range"                       , "all"                            , ""                               , };
const char *  Xrp_Peer_Strs[] = { "Target neighbors only"          , "Neighbors in-range or targeted" , "All discovered neighbors"       , ""                               , };
const char *  xrp_type_strs[] = { "mac"                            , "ip"                             , "host"                           , ""                               , };
const char *  Xrp_Abbr_Strs[] = { "2-only"                         , "2-and-3"                        , "None"                           , ""                               , };
const char * xrp_layer_strs[] = { "layer 2-only"                   , "layer 2-and-3"                  , "off"                            , ""                               , };
const char * Xrp_Layer_Strs[] = { "Layer 2 only"                   , "Layers 2 and 3"                 , "Disabled"                       , ""                               , };
const char *xrp_tunnel_strs[] = { "control"                        , "data from"                      , "data to"                        , ""                               , };
const char *    yes_no_strs[] = { "no"                             , "yes"                            , ""                               , ""                               , };
const char *    Yes_No_Strs[] = { "No "                            , "Yes"                            , ""                               , ""                               , };
const char *start_stop_strs[] = { "start"                          , "stop"                           , ""                               , ""                               , };
const char *Start_Stop_Strs[] = { "Start"                          , "Stop"                           , ""                               , ""                               , };

const char * time_zone_strs[] = { "Eniwetok Kwajalein"                                                , ""                                                                  ,
                                  "Midway Is., Samoa"                                                 , ""                                                                  ,
                                  "Hawaii"                                                            , ""                                                                  ,
                                  "Alaska"                                                            , ""                                                                  ,
                                  "Pacific  Time (US & Canada), Tijuana"                              , ""                                                                  ,
                                  "Mountain Time (US & Canada)"                                       , ""                                                                  ,
                                  "Central  Time (US & Canada)"                                       , ""                                                                  ,
                                  "Eastern  Time (US & Canada)"                                       , ""                                                                  ,
                                  "Atlantic Time (Canada)"                                            , ""                                                                  ,
                                  "Buenos Aires, Georgetown"                                          , "Newfoundland"                                                      ,
                                  "Mid-Atlantic"                                                      , ""                                                                  ,
                                  "Azores, Cape Verde Is."                                            , ""                                                                  ,
                                  "Greenwich Mean Time: Dublin, Lisbon, London"                       , ""                                                                  ,
                                  "Amsterdam, Copenhagen, Madrid, Paris"                              , ""                                                                  ,
                                  "Israel"                                                            , ""                                                                  ,
                                  "Moscow, St. Petersburg, Volgograd"                                 , "Tehran"                                                            ,
                                  "Abu Dhabi, Muscat"                                                 , "Kabul"                                                             ,
                                  "Islamabad, Karachi, Tashkent"                                      , "Bombay, Calcutta, Madras, New Delhi"                               ,
                                  "Almaty, Dhaka"                                                     , ""                                                                  ,
                                  "Bangkok, Hanoi, Jakarta"                                           , ""                                                                  ,
                                  "Beijing, Chongqing, Hong Kong"                                     , ""                                                                  ,
                                  "Osaka, Sapporo, Tokyo"                                             , "Adelaide"                                                          ,
                                  "New South Wales"                                                   , ""                                                                  ,
                                  "Magadan, Solomon Is., New Caledonia"                               , ""                                                                  ,
                                  "Auckland, Wellington"                                              , ""                                                                  , };

const char *ids_attack_strs[] = { "Beacon flood",
                                  "Probe request flood",
                                  "Authentication flood",
                                  "Association flood",
                                  "Disassociation flood",
                                  "Deauthentication flood",
                                  "EAP handshake flood",
                                  "Null probe response",
                                  "MIC error attack",
                                  "AP impersonation",
                                  "Disassociation attack",
                                  "Deauthentication attack" };

const char *  week_day_strs[] = { "none"
//
//  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA
//  -----------------  -----------------  -----------------  -----------------  -----------------  -----------------  -----------------  -----------------
                     ,                sa,              f   ,              f sa,           th     ,           th   sa,           th f   ,           th f sa,
            w        ,         w      sa,         w    f   ,         w    f sa,         w th     ,         w th   sa,         w th f   ,         w th f sa,
         tu          ,      tu        sa,      tu      f   ,      tu      f sa,      tu   th     ,      tu   th   sa,      tu   th f   ,      tu   th f sa,
         tu w        ,      tu w      sa,      tu w    f   ,      tu w    f sa,      tu w th     ,      tu w th   sa,      tu w th f   ,      tu w th f sa,
       m             ,    m           sa,    m         f   ,    m         f sa,    m      th     ,    m      th   sa,    m      th f   ,    m      th f sa,
       m    w        ,    m    w      sa,    m    w    f   ,    m    w    f sa,    m    w th     ,    m    w th   sa,    m    w th f   ,    m    w th f sa,
       m tu          ,    m tu        sa,    m tu      f   ,    m tu      f sa,    m tu   th     ,    m tu   th   sa,    m tu   th f   ,    m tu   th f sa,
       m tu w        ,    m tu w      sa,    m tu w    f   ,    m tu w    f sa,    m tu w th     ,    m tu w th   sa,    m tu w th f   ,    m tu w th f sa,
    su               , su             sa, su           f   , su           f sa, su        th     , su        th   sa, su        th f   , su        th f sa,
    su      w        , su      w      sa, su      w    f   , su      w    f sa, su      w th     , su      w th   sa, su      w th f   , su      w th f sa,
    su   tu          , su   tu        sa, su   tu      f   , su   tu      f sa, su   tu   th     , su   tu   th   sa, su   tu   th f   , su   tu   th f sa,
    su   tu w        , su   tu w      sa, su   tu w    f   , su   tu w    f sa, su   tu w th     , su   tu w th   sa, su   tu w th f   , su   tu w th f sa,
    su m             , su m           sa, su m         f   , su m         f sa, su m      th     , su m      th   sa, su m      th f   , su m      th f sa,
    su m    w        , su m    w      sa, su m    w    f   , su m    w    f sa, su m    w th     , su m    w th   sa, su m    w th f   , su m    w th f sa,
    su m tu          , su m tu        sa, su m tu      f   , su m tu      f sa, su m tu   th     , su m tu   th   sa, su m tu   th f   , su m tu   th f sa,
    su m tu w        , su m tu w      sa, su m tu w    f   , su m tu w    f sa, su m tu w th     , su m tu w th   sa, su m tu w th f   ,
//
    "all"
};
const char *  Week_Day_Strs[] = { "None"
//
//  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA  SU-M-TU-W-TH-F-SA
//  -----------------  -----------------  -----------------  -----------------  -----------------  -----------------  -----------------  -----------------
                     ,                SA,              F   ,              F SA,           TH     ,           TH   SA,           TH F   ,           TH F SA,
            W        ,         W      SA,         W    F   ,         W    F SA,         W TH     ,         W TH   SA,         W TH F   ,         W TH F SA,
         TU          ,      TU        SA,      TU      F   ,      TU      F SA,      TU   TH     ,      TU   TH   SA,      TU   TH F   ,      TU   TH F SA,
         TU W        ,      TU W      SA,      TU W    F   ,      TU W    F SA,      TU W TH     ,      TU W TH   SA,      TU W TH F   ,      TU W TH F SA,
       M             ,    M           SA,    M         F   ,    M         F SA,    M      TH     ,    M      TH   SA,    M      TH F   ,    M      TH F SA,
       M    W        ,    M    W      SA,    M    W    F   ,    M    W    F SA,    M    W TH     ,    M    W TH   SA,    M    W TH F   ,    M    W TH F SA,
       M TU          ,    M TU        SA,    M TU      F   ,    M TU      F SA,    M TU   TH     ,    M TU   TH   SA,    M TU   TH F   ,    M TU   TH F SA,
       M TU W        ,    M TU W      SA,    M TU W    F   ,    M TU W    F SA,    M TU W TH     ,    M TU W TH   SA,    M TU W TH F   ,    M TU W TH F SA,
    SU               , SU             SA, SU           F   , SU           F SA, SU        TH     , SU        TH   SA, SU        TH F   , SU        TH F SA,
    SU      W        , SU      W      SA, SU      W    F   , SU      W    F SA, SU      W TH     , SU      W TH   SA, SU      W TH F   , SU      W TH F SA,
    SU   TU          , SU   TU        SA, SU   TU      F   , SU   TU      F SA, SU   TU   TH     , SU   TU   TH   SA, SU   TU   TH F   , SU   TU   TH F SA,
    SU   TU W        , SU   TU W      SA, SU   TU W    F   , SU   TU W    F SA, SU   TU W TH     , SU   TU W TH   SA, SU   TU W TH F   , SU   TU W TH F SA,
    SU M             , SU M           SA, SU M         F   , SU M         F SA, SU M      TH     , SU M      TH   SA, SU M      TH F   , SU M      TH F SA,
    SU M    W        , SU M    W      SA, SU M    W    F   , SU M    W    F SA, SU M    W TH     , SU M    W TH   SA, SU M    W TH F   , SU M    W TH F SA,
    SU M TU          , SU M TU        SA, SU M TU      F   , SU M TU      F SA, SU M TU   TH     , SU M TU   TH   SA, SU M TU   TH F   , SU M TU   TH F SA,
    SU M TU W        , SU M TU W      SA, SU M TU W    F   , SU M TU W    F SA, SU M TU W TH     , SU M TU W TH   SA, SU M TU W TH F   ,
//
    "All"
};
const char *week_comma_strs[] = { "none"
//
//  SU, M, TU, W, TH, F, SA  SU, M, TU, W, TH, F, SA  SU, M, TU, W, TH, F, SA  SU, M, TU, W, TH, F, SA  SU, M, TU, W, TH, F, SA  SU, M, TU, W, TH, F, SA  SU, M, TU, W, TH, F, SA  SU, M, TU, W, TH, F, SA
//  -----------------------  -----------------------  -----------------------  -----------------------  -----------------------  -----------------------  -----------------------  -----------------------
                           ,                      SA,                   F    ,                   F_ SA,               TH       ,               TH_    SA,               TH_ F    ,               TH_ F_ SA,
               W           ,            W_        SA,            W_     F    ,            W_     F_ SA,            W_ TH       ,            W_ TH_    SA,            W_ TH_ F    ,            W_ TH_ F_ SA,
           TU              ,        TU_           SA,        TU_        F    ,        TU_        F_ SA,        TU_    TH       ,        TU_    TH_    SA,        TU_    TH_ F    ,        TU_    TH_ F_ SA,
           TU_ W           ,        TU_ W_        SA,        TU_ W_     F    ,        TU_ W_     F_ SA,        TU_ W_ TH       ,        TU_ W_ TH_    SA,        TU_ W_ TH_ F    ,        TU_ W_ TH_ F_ SA,
        M                  ,     M_               SA,     M_            F    ,     M_            F_ SA,     M_        TH       ,     M_        TH_    SA,     M_        TH_ F    ,     M_        TH_ F_ SA,
        M_     W           ,     M_     W_        SA,     M_     W_     F    ,     M_     W_     F_ SA,     M_     W_ TH       ,     M_     W_ TH_    SA,     M_     W_ TH_ F    ,     M_     W_ TH_ F_ SA,
        M_ TU              ,     M_ TU_           SA,     M_ TU_        F    ,     M_ TU_        F_ SA,     M_ TU_    TH       ,     M_ TU_    TH_    SA,     M_ TU_    TH_ F    ,     M_ TU_    TH_ F_ SA,
        M_ TU_ W           ,     M_ TU_ W_        SA,     M_ TU_ W_     F    ,     M_ TU_ W_     F_ SA,     M_ TU_ W_ TH       ,     M_ TU_ W_ TH_    SA,     M_ TU_ W_ TH_ F    ,     M_ TU_ W_ TH_ F_ SA,
    SU                     , SU_                  SA, SU_               F    , SU_               F_ SA, SU_           TH       , SU_           TH_    SA, SU_           TH_ F    , SU_           TH_ F_ SA,
    SU_        W           , SU_        W_        SA, SU_        W_     F    , SU_        W_     F_ SA, SU_        W_ TH       , SU_        W_ TH_    SA, SU_        W_ TH_ F    , SU_        W_ TH_ F_ SA,
    SU_    TU              , SU_    TU_           SA, SU_    TU_        F    , SU_    TU_        F_ SA, SU_    TU_    TH       , SU_    TU_    TH_    SA, SU_    TU_    TH_ F    , SU_    TU_    TH_ F_ SA,
    SU_    TU_ W           , SU_    TU_ W_        SA, SU_    TU_ W_     F    , SU_    TU_ W_     F_ SA, SU_    TU_ W_ TH       , SU_    TU_ W_ TH_    SA, SU_    TU_ W_ TH_ F    , SU_    TU_ W_ TH_ F_ SA,
    SU_ M                  , SU_ M_               SA, SU_ M_            F    , SU_ M_            F_ SA, SU_ M_        TH       , SU_ M_        TH_    SA, SU_ M_        TH_ F    , SU_ M_        TH_ F_ SA,
    SU_ M_     W           , SU_ M_     W_        SA, SU_ M_     W_     F    , SU_ M_     W_     F_ SA, SU_ M_     W_ TH       , SU_ M_     W_ TH_    SA, SU_ M_     W_ TH_ F    , SU_ M_     W_ TH_ F_ SA,
    SU_ M_ TU              , SU_ M_ TU_           SA, SU_ M_ TU_        F    , SU_ M_ TU_        F_ SA, SU_ M_ TU_    TH       , SU_ M_ TU_    TH_    SA, SU_ M_ TU_    TH_ F    , SU_ M_ TU_    TH_ F_ SA,
    SU_ M_ TU_ W           , SU_ M_ TU_ W_        SA, SU_ M_ TU_ W_     F    , SU_ M_ TU_ W_     F_ SA, SU_ M_ TU_ W_ TH       , SU_ M_ TU_ W_ TH_    SA, SU_ M_ TU_ W_ TH_ F    ,
//
    "all"
};

const char * dot11u_na_strs[] = { "accept-terms"                      , "online-enroll"               , "http-redirect"         , "dns-redirect"    };
const char * Dot11u_Na_Strs[] = { "Acceptance of terms and conditions", "On-line enrollment supported", "HTTP/HTTPS redirection", "DNS redirection" };

const char * dot11u_i4_strs[] = { "not-available"                                             , "public-addr"                                               ,
                                  "port-restricted-addr"                                      , "single-nat-priv-addr"                                      ,
                                  "double-nat-priv-addr"                                      , "port-restricted-single-nat-addr"                           ,
                                  "port-restricted-double-nat-addr"                           , "not-known"                                                  };
const char * Dot11u_I4_Strs[] = { "Not available"                                             , "Public address available"                                  ,
                                  "Port-restricted address available"                         , "Single NATed private address available"                    ,
                                  "Double NATed private address available"                    , "Port-restricted address and single NATed address available",
                                  "Port-restricted address and double NATed address available", "Not known"                                                  };

const char * dot11u_i6_strs[] = { "not-available"          , "available"                        , "not-known"                   , ""                    };
const char * Dot11u_I6_Strs[] = { "Not available"          , "Available"                        , "Not known"                   , ""                    };

const char * dot11u_in_strs[] = { "unspecified"            , "provided"                         , ""                            , ""                    };
const char * Dot11u_In_Strs[] = { "Unspecified"            , "Provided"                         , ""                            , ""                    };

const char * dot11u_an_strs[] = { "private-network"        , "private-network-guest-access"     , "chargeable-public-network"   , "free-public-network",
                                  "personal-device-network", "emergency-services-network"       , ""                            , ""                   ,
                                  ""                       , ""                                 , ""                            , ""                   ,
                                  ""                       , ""                                 , "test-network"                , "wildcard"            };
const char * Dot11u_An_Strs[] = { "Private network"        , "Private network with guest access", "Chargeable public network"   , "Free public network",
                                  "Personal device network", "Emergency services only network"  , ""                            , ""                   ,
                                  ""                       , ""                                 , ""                            , ""                   ,
                                  ""                       , ""                                 , "Test or experimental network", "Wildcard"            };

const char * dot11u_vg_strs[] = { "unspecified"            , "assembly"                         , "business"                    , "educational"        ,
                                  "factory"                , "institutional"                    , "mercantile"                  , "residential"        ,
                                  "storage"                , "utility"                          , "vehicular"                   , "outdoor"            ,
                                  ""                       , ""                                 , ""                            ,  ""                   };
const char * Dot11u_Vg_Strs[] = { "Unspecified"            , "Assembly"                         , "Business"                    , "Educational"        ,
                                  "Factory and Industrial" , "Institutional"                    , "Mercantile"                  , "Residential"        ,
                                  "Storage"                , "Utility and Miscellaneous"        , "Vehicular"                   , "Outdoor"            ,
                                  ""                       , ""                                 , ""                            ,  ""                   };

struct venue_type_strs dot11u_vt_strs[] =  { { { "unspecified"                                                                                                                        },
                                               { "Unspecified"                                                                                                                        } },
                                             { { "unspecified"                      , "arena"                   , "stadium"                , "terminal"                              ,
                                                 "amphitheater"                     , "amusement-park"          , "worship"                , "convention"                            ,
                                                 "library"                          , "museum"                  , "restaurant"             , "theater"                               ,
                                                 "bar"                              , "coffee-shop"             , "zoo"                    , "emergency"                              },
                                               { "Unspecified"                      , "Arena"                   , "Stadium"                , "Passenger Terminal"                    ,
                                                 "Amphitheater"                     , "Amusement Park"          , "Place of Worship"       , "Convention Center"                     ,
                                                 "Library"                          , "Museum"                  , "Restaurant"             , "Theater"                               ,
                                                 "Bar"                              , "Coffee Shop"             , "Zoo or Aquarium"        , "Emergency Coordination Center"          } },
                                             { { "unspecified"                      , "doctor"                  , "bank"                   , "fire-station"                          ,
                                                 "police-station"                   ,  ""                       , "post-office"            , "prof-office"                   ,
                                                 "r-and-d-facility"                 , "attorney-office"                                                                                },
                                               { "Unspecified"                      , "Doctor or Dentist office", "Bank"                   , "Fire Station"                          ,
                                                 "Police Station"                   ,  ""                       , "Post Office"            , "Professional Office"                           ,
                                                 "Research and Development Facility", "Attorney Office"                                                                                } },
                                             { { "unspecified"                      , "school-pri"              , "school-sec"             , "college"                                 },
                                               { "Unspecified"                      , "School, Primary"         , "School, Secondary"      , "University or College"                   } },
                                             { { "unspecified"                      , "factory"                                                                                        },
                                               { "Unspecified"                      , "Factory"                                                                                        } },
                                             { { "unspecified"                      , "hospital"                , "long-term-care"         , "rehab"                                 ,
                                                 "group-home"                       , "jail"                                                                                           },
                                               { "Unspecified"                      , "Hospital"                , "Long-Term Care Facility", "Alcohol and Drug Rehabilitation Center",
                                                 "Group Home"                       , "Prison or Jail"                                                                                 } },
                                             { { "unspecified"                      , "retail"                  , "grocery"                , "auto-station"                          ,
                                                 "mall"                             , "gas-station"                                                                                    },
                                               { "Unspecified"                      , "Retail Store"            , "Grocery Market"         , "Automotive Service Station"            ,
                                                 "Shopping Mall"                    , "Gas Station"                                                                                    } },
                                             { { "unspecified"                      , "private-residence"       , "hotel"                  , "dorm"                                  ,
                                                 "boarding-house"                                                                                                                      },
                                               { "Unspecified"                      , "Private Residence"       , "Hotel or Motel"         , "Dormitory"                             ,
                                                 "Boarding House"                                                                                                                      } },
                                             { { "unspecified"                                                                                                                         },
                                               { "Unspecified"                                                                                                                         } },
                                             { { "unspecified"                                                                                                                         },
                                               { "Unspecified"                                                                                                                         } },
                                             { { "unspecified"                      , "automobile"              , "plane"                  , "bus"                                   ,
                                                 "ferry"                            , "ship"                    , "train"                  , "bike"                                    },
                                               { "Unspecified"                      , "Automobile or Truck"     , "Airplane"               , "Bus"                                   ,
                                                 "Ferry"                            , "Ship or Boat"            , "Train"                  , "Motor Bike"                              } },
                                             { { "unspecified"                      , "muni-mesh"               , "city-park"              , "rest-area"                             ,
                                                 "traffic-control"                  , "bus-stop"                , "kiosk"                                                              },
                                               { "Unspecified"                      , "Muni-mesh Network"       , "City Park"              , "Rest Area"                             ,
                                                 "Traffic Control"                  , "Bus Stop"                , "Kiosk"                                                              } } };

struct eap_method_str dot11u_em_strs[] = { {   0, "none"                      , "None"          },
                                           {   4, "md5-challenge"             , "MD5-Challenge" },
                                           {   6, "gtc"                       , "GTC"           },
                                           {  13, "eap-tls"                   , "EAP-TLS"       },
                                           {  18, "eap-sim"                   , "EAP-SIM"       },
                                           {  21, "eap-ttls"                  , "EAP-TTLS"      },
                                           {  23, "eap-aka"                   , "EAP-AKA"       },
                                           {  25, "peap"                      , "PEAP"          },
                                           {  29, "eap-mschap-v2"             , "EAP-MSCHAP-V2" },
                                           {  43, "eap-fast"                  , "EAP-FAST"      },
                                           {  50, "eap-aka-prime"             , "EAP-AKA'"      },
                                           { 254, "reserved-for-expanded-type", "Reserved"      } };

int dot11u_em_strs_num = countof(dot11u_em_strs);

const char * dot11u_ap_strs[] = { "none"                     , "exp-eap-method"     , "non-eap-inner-auth-type"            , "inner-auth-eap-method-type"          ,
                                  "exp-inner-eap-method"     , "credential-type"    , "tun-eap-method-credential-type"     , ""                                     };
const char * Dot11u_Ap_Strs[] = { "None"                     , "Expanded EAP Method", "Non-EAP Inner Authentication Type"  , "Inner Authentication EAP Method Type",
                                  "Expanded Inner EAP Method", "Credential Type"    , "Tunneled EAP Method Credential Type", ""                                     };

const char * dot11u_tc_strs[] = { ""                         , "sim"                , "usim"                               , "nfc-secure-elem"                     ,
                                  "hw-token"                 , "softoken"           , "certificate"                        , "user-pass"                           ,
                                  ""                         , "anonymous"          , ""                                   , ""                                    ,
                                  ""                         , ""                   , ""                                   , ""                                     };
const char * Dot11u_Tc_Strs[] = { ""                         , "SIM"                , "USIM"                               , "NFC Secure Element"                  ,
                                  "Hardware Token"           , "Softoken"           , "Certificate"                        , "Username/Password"                   ,
                                  ""                         , "Anonymous"          , ""                                   , ""                                    ,
                                  ""                         , ""                   , ""                                   , ""                                     };

const char * dot11u_ct_strs[] = { ""                         , "sim"                , "usim"                               , "nfc-secure-elem"                     ,
                                  "hw-token"                 , "softoken"           , "certificate"                        , "user-pass"                           ,
                                  "none"                     , ""                   , ""                                   , ""                                    ,
                                  ""                         , ""                   , ""                                   , ""                                     };
const char * Dot11u_Ct_Strs[] = { ""                         , "SIM"                , "USIM"                               , "NFC Secure Element"                  ,
                                  "Hardware Token"           , "Softoken"           , "Certificate"                        , "Username/Password"                   ,
                                  "None"                     , ""                   , ""                                   , ""                                    ,
                                  ""                         , ""                   , ""                                   , ""                                     };

const char * dot11u_ne_strs[] = { ""                         , "pap"                , "chap"                               , "mschap"                              ,
                                  "mschapv2"                 , ""                   , ""                                   , ""                                     };
const char * Dot11u_Ne_Strs[] = { ""                         , "PAP"                , "CHAP"                               , "MSCHAP"                              ,
                                  "MSCHAPV2"                 , ""                   , ""                                   , ""                                     };

const char *dot11n_mcs_name_strs[] = {
    "  MCS0", "  MCS1", "  MCS2", "  MCS3", "  MCS4", "  MCS5", "  MCS6", "  MCS7", "MCS8.1", "MCS9.1",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    "  MCS8", "  MCS9", " MCS10", " MCS11", " MCS12", " MCS13", " MCS14", " MCS15", "MCS8.2", "MCS9.2",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    " MCS16", " MCS17", " MCS18", " MCS19", " MCS20", " MCS21", " MCS22", " MCS23", "MCS8.3", "MCS9.3",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    " MCS24", " MCS25", " MCS26", " MCS27", " MCS28", " MCS29", " MCS30", " MCS31"," MCS8.4", "MCS9.4",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
};
const char *dot11ac_mcs_name_strs[] = {
    "MCS0.1", "MCS1.1", "MCS2.1", "MCS3.1", "MCS4.1", "MCS5.1", "MCS6.1", "MCS7.1", "MCS8.1", "MCS9.1",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    "MCS0.2", "MCS1.2", "MCS2.2", "MCS3.2", "MCS4.2", "MCS5.2", "MCS6.2", "MCS7.2", "MCS8.2", "MCS9.2",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    "MCS0.3", "MCS1.3", "MCS2.3", "MCS3.3", "MCS4.3", "MCS5.3", "MCS6.3", "MCS7.3", "MCS8.3", "MCS9.3",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    "MCS0.4", "MCS1.4", "MCS2.4", "MCS3.4", "MCS4.4", "MCS5.4", "MCS6.4", "MCS7.4", "MCS8.4", "MCS9.4",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
};

const char *mcs_rate_strs[] = {
    // 20MHz, normal guard interval
    "   6.5", "  13.0", "  19.5", "  26.0", "  39.0", "  52.0", "  58.5", "  65.0", "  78.0", "  86.7",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    "  13.0", "  26.0", "  39.0", "  52.0", "  78.0", " 104.0", " 117.0", " 130.0", " 156.0", " 173.3",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    "  19.5", "  39.0", "  58.5", "  78.0", " 117.0", " 156.0", " 175.5", " 195.0", " 234.0", " 260.0",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    "  26.0", "  52.0", "  78.0", " 104.0", " 156.0", " 208.0", " 234.0", " 260.0", " 312.0", " 346.7",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 20MHz,  short guard interval
    "   7.2", "  14.4", "  21.7", "  28.9", "  43.3", "  57.8", "  65.0", "  72.2", "  86.7", "  96.3",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    "  14.4", "  28.9", "  43.3", "  57.8", "  86.7", " 115.6", " 130.0", " 144.4", " 173.3", " 192.6",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    "  21.7", "  43.3", "  65.0", "  86.7", " 130.0", " 173.3", " 195.0", " 216.7", " 260.0", " 288.9",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    "  28.9", "  57.8", "  86.7", " 115.6", " 173.3", " 231.1", " 260.0", " 288.9", " 346.7", " 385.2",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 40MHz, normal guard interval
    "  13.5", "  27.0", "  40.5", "  54.0", "  81.0", " 108.0", " 121.5", " 135.0", " 162.0", " 180.0",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    "  27.0", "  54.0", "  81.0", " 108.0", " 162.0", " 216.0", " 243.0", " 270.0", " 324.0", " 360.0",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    "  40.5", "  81.0", " 121.5", " 162.0", " 243.0", " 324.0", " 364.5", " 405.0", " 486.0", " 540.0",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    "  54.0", " 108.0", " 162.0", " 216.0", " 324.0", " 432.0", " 486.0", " 540.0", " 648.0", " 720.0",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 40MHz,  short guard interval
    "  15.0", "  30.0", "  45.0", "  60.0", "  90.0", " 120.0", " 135.0", " 150.0", " 180.0", " 200.0",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    "  30.0", "  60.0", "  90.0", " 120.0", " 180.0", " 240.0", " 270.0", " 300.0", " 360.0", " 400.0",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    "  45.0", "  90.0", " 135.0", " 180.0", " 270.0", " 360.0", " 405.0", " 450.0", " 540.0", " 600.0",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    "  60.0", " 120.0", " 180.0", " 240.0", " 360.0", " 480.0", " 540.0", " 600.0", " 720.0", " 800.0",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 80MHz, normal guard interval
    "  29.3", "  58.5", "  87.8", " 117.0", " 175.5", " 234.0", " 263.3", " 292.5", " 351.0", " 390.0",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    "  58.5", " 117.0", " 175.5", " 234.0", " 351.0", " 468.0", " 526.5", " 585.0", " 702.0", " 780.0",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    "  87.8", " 175.5", " 263.3", " 351.0", " 526.5", " 702.0", " 789.9", " 877.5", "1053.0", "1170.0",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    " 117.0", " 234.0", " 351.0", " 468.0", " 702.0", " 936.0", "1053.0", "1170.0", "1404.0", "1560.0",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 80MHz,  short guard interval
    "  32.5", "  65.0", "  97.5", " 130.0", " 195.0", " 260.0", " 292.5", " 325.0", " 390.0", " 433.3",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    "  65.0", " 130.0", " 195.0", " 260.0", " 390.0", " 520.0", " 585.0", " 650.0", " 780.0", " 866.7",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    "  97.5", " 195.0", " 292.5", " 390.0", " 585.0", " 780.0", " 877.5", " 975.0", "1070.0", "1300.0",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    " 130.0", " 260.0", " 390.0", " 520.0", " 780.0", "1040.0", "1170.0", "1300.0", "1560.0", "1733.3",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 160MHz, normal guard interval
    "  58.5", " 117.0", " 175.5", " 234.0", " 351.0", " 468.0", " 526.5", " 585.0", " 702.0", " 780.0",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    " 117.0", " 234.0", " 351.0", " 468.0", " 702.0", " 936.0", "1053.0", "1170.0", "1404.0", "1560.0",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    " 175.5", " 351.0", " 526.5", " 702.0", "1053.0", "1404.0", "1579.5", "1755.0", "2106.0", "2340.0 ",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    " 234.0", " 468.0", " 702.0", " 936.0", "1404.0", "1872.0", "2106.0", "2340.0", "2808.0", "3120.0",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 160MHz,  short guard interval
    "  65.0", " 130.0", " 195.0", " 260.0", " 390.0", " 520.0", " 585.0", " 650.0", " 780.0", " 866.7",    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
    " 130.0", " 260.0", " 390.0", " 520.0", " 780.0", "1040.0", "1170.0", "1300.0", "1560.0", "1733.3",    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
    " 195.0", " 390.0", " 585.0", " 780.0", "1170.0", "1560.0", "1755.0", "1950.0", "2340.0", "2600.1",    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
    " 260.0", " 520.0", " 780.0", "1040.0", "1560.0", "2080.0", "2340.0", "2600.0", "3120.0", "3466.7",    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
};

const int mcs_rate_kbps[] = {
    // 20MHz, normal guard interval
       6500,   13000,   19500,   26000,   39000,   52000,   58500,   65000,   78000,   86666,    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
      13000,   26000,   39000,   52000,   78000,  104000,  117000,  130000,  156000,  173333,    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
      19500,   39000,   58500,   78000,  117000,  156000,  175500,  195000,  234000,  260000,    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
      26000,   52000,   78000,  104000,  156000,  208000,  234000,  260000,  312000,  346666,    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 20MHz, short guard interval
       7200,   14400,   21700,   28900,   43300,   57800,   65000,   72200,   86700,   96300,    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
      14400,   28900,   43300,   57800,   86700,  115600,  130000,  144400,  173300,  192600,    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
      21700,   43300,   65000,   86700,  130000,  173300,  195000,  216700,  260000,  288900,    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
      28900,   57800,   86700,  115600,  173300,  231100,  260000,  288900,  346700,  385200,    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 40MHz, normal guard interval
      13500,   27000,   40500,   54000,   81000,  108000,  121500,  135000,  162000,  180000,    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
      27000,   54000,   81000,  108000,  162000,  216000,  243000,  270000,  324000,  360000,    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
      40500,   81000,  121500,  162000,  243000,  324000,  364500,  405000,  486000,  540000,    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
      54000,  108000,  162000,  216000,  324000,  432000,  486000,  540000,  648000,  720000,    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 40MHz, short guard interval
      15000,   30000,   45000,   60000,   90000,  120000,  135000,  150000,  180000,  200000,    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
      30000,   60000,   90000,  120000,  180000,  240000,  270000,  300000,  360000,  400000,    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
      45000,   90000,  135000,  180000,  270000,  360000,  405000,  450000,  540000,  600000,    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
      60000,  120000,  180000,  240000,  360000,  480000,  540000,  600000,  720000,  800000,    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 80MHz, normal guard interval
      29300,   58500,   87800,  117000,  175500,  234000,  263300,  292500,  351000,  390000,    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
      58500,  117000,  175500,  234000,  351000,  468000,  526500,  585000,  702000,  780000,    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
      87800,  175500,  263300,  351000,  526500,  702000,  789900,  877500, 1053000, 1170000,    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
     117000,  234000,  351000,  468000,  702000,  936000, 1053000, 1170000, 1404000, 1560000,    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 80MHz, short guard interval
      32500,   65000,   97500,  130000,  195000,  260000,  292500,  325000,  390000,  433300,    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
      65000,  130000,  195000,  260000,  390000,  520000,  585000,  650000,  780000,  866700,    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
      97500,  195000,  292500,  390000,  585000,  780000,  877500,  975000, 1070000, 1300000,    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
     130000,  260000,  390000,  520000,  780000, 1040000, 1170000, 1300000, 1560000, 1733300,    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 160MHz, normal guard interval
      58500,  117000,  175500,  234000,  351000,  468000,  526500,  585000,  702000,  780000,    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
     117000,  234000,  351000,  468000,  702000,  936000, 1053000, 1170000, 1404000, 1560000,    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
     175500,  351000,  526500,  702000, 1053000, 1404000, 1579500, 1755000, 2106000, 2340000,    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
     234000,  468000,  702000,  936000, 1404000, 1872000, 2106000, 2340000, 2808000, 3120000,    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // 160MHz, short guard interval
      65000,  130000,  195000,  260000,  390000,  520000,  585000,  650000,  780000,  866700,    // one   spatial stream  (11n mcs0 - 7) + (11ac mcs8-9, 1SS)
     130000,  260000,  390000,  520000,  780000, 1040000, 1170000, 1300000, 1560000, 1733300,    // two   spatial streams (11n mcs8 -15) + (11ac mcs8-9, 2SS)
     195000,  390000,  585000,  780000, 1170000, 1560000, 1755000, 1950000, 2340000, 2600100,    // three spatial streams (11n mcs16-23) + (11ac mcs8-9, 3SS)
     260000,  520000,  780000, 1040000, 1560000, 2080000, 2340000, 2600000, 3120000, 3466700,    // four  spatial streams (11n mcs23-31) + (11ac mcs8-9, 4SS)
    // fill out table to 512
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
};

const char   **ht20_ngi_strs = &mcs_rate_strs[  0];     //  20MHz, normal guard interval
const char   **ht20_sgi_strs = &mcs_rate_strs[ 40];     //  20MHz,  short guard interval
const char   **ht40_ngi_strs = &mcs_rate_strs[ 80];     //  40MHz, normal guard interval
const char   **ht40_sgi_strs = &mcs_rate_strs[120];     //  40MHz,  short guard interval
const char  **vht80_ngi_strs = &mcs_rate_strs[160];     //  80MHz, normal guard interval
const char  **vht80_sgi_strs = &mcs_rate_strs[200];     //  80MHz,  short guard interval
const char **vht160_ngi_strs = &mcs_rate_strs[240];     // 160MHz, normal guard interval
const char **vht160_sgi_strs = &mcs_rate_strs[280];     // 160MHz,  short guard interval

      char   eth_names[][8] = {"eth0" , "gig1" , "gig2" , ""     , ""     };
      char eth_1_names[][8] = {"gig1" , ""     , ""     , ""     , ""     };
      char eth_3_names[][8] = {"eth0" , "gig1" , "gig2" , ""     , ""     };
      char eth_2_names[][8] = {"gig1" , "gig2" , ""     , ""     , ""     };
      char eth_4_names[][8] = {"gig1" , "gig2" , "gig3" , "gig4" , ""     };
      char eth_5_names[][8] = {"gig1" , "gig2" , "gig3" , "gig4" , "gig5" };

const char *bond_dev_strs[] = {"bond1", "bond2", "bond3", "bond4", "bond5"};
const char * eth_dev_strs[] = {eth_names[0], eth_names[1], eth_names[2], eth_names[3], eth_names[4]};

char     iap_names[][8] = {"iap1"  , "iap2"  , "iap3"  , "iap4"  ,
                           "iap5"  , "iap6"  , "iap7"  , "iap8"  ,
                           "iap9"  , "iap10" , "iap11" , "iap12" ,
                           "iap13" , "iap14" , "iap15" , "iap16" };
char   iap_n_names[][8] = {"iap1"  , "iap2"  , "iap3"  , "iap4"  ,
                           "iap5"  , "iap6"  , "iap7"  , "iap8"  ,
                           "iap9"  , "iap10" , "iap11" , "iap12" ,
                           "iap13" , "iap14" , "iap15" , "iap16" };
char *  iap_cntr_strs[] = {"iap1 " , "iap2 " , "iap3 " , "iap4 " ,
                           "iap5 " , "iap6 " , "iap7 " , "iap8 " ,
                           "iap9 " , "iap10" , "iap11" , "iap12" ,
                           "iap13" , "iap14" , "iap15" , "iap16" };
char *iap_n_cntr_strs[] = {"iap1 " , "iap2 " , "iap3 " , "iap4 " ,
                           "iap5 " , "iap6 " , "iap7 " , "iap8 " ,
                           "iap9 " , "iap10" , "iap11" , "iap12" ,
                           "iap13" , "iap14" , "iap15" , "iap16" };

