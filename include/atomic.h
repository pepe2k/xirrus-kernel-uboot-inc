/************************************************************************************************************************
 * Atomic operators                                                                                                     *
 ************************************************************************************************************************/
#ifndef _ATOMIC_OPS
#define _ATOMIC_OPS

#define atomic32_set(v, i)          (uint32_t)__sync_val_compare_and_swap((uint32_t *)&(v), (uint32_t)(v), (uint32_t)(i))
#define atomic32_add(v, i)          (uint32_t)__sync_add_and_fetch((uint32_t *)&(v), (uint32_t)(i))
#define atomic32_sub(v, i)          (uint32_t)__sync_sub_and_fetch((uint32_t *)&(v), (uint32_t)(i))
#define atomic32_inc(v)             (uint32_t)__sync_add_and_fetch((uint32_t *)&(v), (uint32_t)(1))
#define atomic32_dec(v)             (uint32_t)__sync_sub_and_fetch((uint32_t *)&(v), (uint32_t)(1))
#define atomic32_clr(v)             (uint32_t)__sync_and_and_fetch((uint32_t *)&(v), (uint32_t)(0))
#define atomic32_get(v)             (uint32_t)__sync_add_and_fetch((uint32_t *)&(v), (uint32_t)(0))

#define atomic64_set(v, i)          (uint64_t)__sync_val_compare_and_swap((uint64_t *)&(v), (uint64_t)(v), (uint64_t)(i))
#define atomic64_add(v, i)          (uint64_t)__sync_add_and_fetch((uint64_t *)&(v), (uint64_t)(i))
#define atomic64_sub(v, i)          (uint64_t)__sync_sub_and_fetch((uint64_t *)&(v), (uint64_t)(i))
#define atomic64_inc(v)             (uint64_t)__sync_add_and_fetch((uint64_t *)&(v), (uint64_t)(1))
#define atomic64_dec(v)             (uint64_t)__sync_sub_and_fetch((uint64_t *)&(v), (uint64_t)(1))
#define atomic64_clr(v)             (uint64_t)__sync_and_and_fetch((uint64_t *)&(v), (uint64_t)(0))
#define atomic64_get(v)             (uint64_t)__sync_add_and_fetch((uint64_t *)&(v), (uint64_t)(0))

#endif
