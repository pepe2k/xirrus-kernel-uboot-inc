//******************************************************************************
/** @file ratemask.h
 *
 * Irate mask constants
 *
 * <I>Copyright (C) 2007 Xirrus. All Rights Reserved</I>
 *
 **/
//------------------------------------------------------------------------------
#ifndef _RATEMASK_H
#define _RATEMASK_H
#include "bitfield.h"

//****************************************************************
// Rates mask
//----------------------------------------------------------------
#define RATE_BIT(x)                            (1ULL << (x))

#define MANDATORY_OFDM_RATES_MASK              (RATE_BIT(6) | RATE_BIT(12) | RATE_BIT(24))
#define VALID_OFDM_RATES_MASK                  (RATE_BIT(6) | RATE_BIT(9)  | RATE_BIT(12) | RATE_BIT(18) | RATE_BIT(24) | RATE_BIT(36) | RATE_BIT(48) | RATE_BIT(54))
#define VALID_CCK_RATES_MASK                   (RATE_BIT(1) | RATE_BIT(2)  | RATE_BIT( 5) | RATE_BIT(11))

#define VALID_A_RATES_MASK                     (VALID_OFDM_RATES_MASK)
#define VALID_G_RATES_MASK                     (VALID_OFDM_RATES_MASK | VALID_CCK_RATES_MASK)

#define DOT11A_DEFAULT_BASIC_RATES              MANDATORY_OFDM_RATES_MASK
#define DOT11A_DEFAULT_SUPP_RATES               VALID_OFDM_RATES_MASK

#define DOT11A_BEST_RANGE_BASIC_RATES           RATE_BIT(6)
#define DOT11A_BEST_RANGE_SUPP_RATES            VALID_OFDM_RATES_MASK

#define DOT11A_BEST_THROUGHPUT_BASIC_RATES      VALID_OFDM_RATES_MASK
#define DOT11A_BEST_THROUGHPUT_SUPP_RATES       VALID_OFDM_RATES_MASK

#define DOT11G_DEFAULT_BASIC_RATES              VALID_CCK_RATES_MASK
#define DOT11G_DEFAULT_SUPP_RATES              (VALID_CCK_RATES_MASK | VALID_OFDM_RATES_MASK)

#define DOT11G_BEST_RANGE_BASIC_RATES           RATE_BIT(1)
#define DOT11G_BEST_RANGE_SUPP_RATES           (VALID_CCK_RATES_MASK | VALID_OFDM_RATES_MASK)

#define DOT11G_BEST_THROUGHPUT_BASIC_RATES     (VALID_CCK_RATES_MASK | VALID_OFDM_RATES_MASK)
#define DOT11G_BEST_THROUGHPUT_SUPP_RATES      (VALID_CCK_RATES_MASK | VALID_OFDM_RATES_MASK)

#define DOT11G_ONLY_DEFAULT_BASIC_RATES         MANDATORY_OFDM_RATES_MASK
#define DOT11G_ONLY_DEFAULT_SUPP_RATES          VALID_OFDM_RATES_MASK

#define DOT11G_ONLY_BEST_RANGE_BASIC_RATES      RATE_BIT(6)
#define DOT11G_ONLY_BEST_RANGE_SUPP_RATES       VALID_OFDM_RATES_MASK

#define DOT11G_ONLY_BEST_THROUGHPUT_BASIC_RATES VALID_OFDM_RATES_MASK
#define DOT11G_ONLY_BEST_THROUGHPUT_SUPP_RATES  VALID_OFDM_RATES_MASK

#define is_valid_ofdm_rate(x)                   (RATE_BIT(x) & VALID_OFDM_RATES_MASK)
#define is_valid_cck_rate(x)                    (RATE_BIT(x) & VALID_CCK_RATES_MASK )

#define is_valid_abg_rate(x)                    (RATE_BIT(x) & (VALID_OFDM_RATES_MASK | VALID_CCK_RATES_MASK))
#define is_valid_ag_rate(x)                     (RATE_BIT(x) &  VALID_OFDM_RATES_MASK)
#define is_valid_b_rate(x)                      (RATE_BIT(x) &  VALID_CCK_RATES_MASK )
#define is_valid_n_rate(x)                      (!is_valid_abg_rate(x))

#define MCS_NO_RATES                            0
#define MCS_0_15_RATES                          0xffff
#define MCS_0_31_RATES                          0xffffffff
#define MCS_MAX_NUM_RATES                       32
#define MCS_DEFAULT_BASIC_RATES                 MCS_NO_RATES
#define MCS_DEFAULT_SUPP_RATES                  MCS_0_31_RATES

/********************************************************************************
 * Rate Definitions                                                             *
 ********************************************************************************/
#define INDEX_1MBPS             0               /* CCK Modulation,   1 Mbps     */
#define INDEX_2MBPS             1               /* CCK Modulation,   2 Mbps     */
#define INDEX_5_5MBPS           2               /* CCK Modulation, 5.5 Mbps     */
#define INDEX_11MBPS            3               /* CCK Modulation,  11 Mbps     */
#define NO_CCK_RATE_INDICIES    4

#define INDEX_6MBPS             0               /* OFDM Modulation,  6 Mbps     */
#define INDEX_9MBPS             1               /* OFDM Modulation,  9 Mbps     */
#define INDEX_12MBPS            2               /* OFDM Modulation, 12 Mbps     */
#define INDEX_18MBPS            3               /* OFDM Modulation, 18 Mbps     */
#define INDEX_24MBPS            4               /* OFDM Modulation, 24 Mbps     */
#define INDEX_36MBPS            5               /* OFDM Modulation, 36 Mbps     */
#define INDEX_48MBPS            6               /* OFDM Modulation, 48 Mbps     */
#define INDEX_54MBPS            7               /* OFDM Modulation, 54 Mbps     */
#define NO_OFDM_RATE_INDICIES   8


#define MASK_1MBPS              bit_const(0)    /* CCK Modulation,   1 Mbps     */
#define MASK_2MBPS              bit_const(1)    /* CCK Modulation,   2 Mbps     */
#define MASK_5_5MBPS            bit_const(2)    /* CCK Modulation, 5.5 Mbps     */
#define MASK_11MBPS             bit_const(3)    /* CCK Modulation,  11 Mbps     */

#define MASK_6MBPS              bit_const(0)    /* OFDM Modulation,  6 Mbps     */
#define MASK_9MBPS              bit_const(1)    /* OFDM Modulation,  9 Mbps     */
#define MASK_12MBPS             bit_const(2)    /* OFDM Modulation, 12 Mbps     */
#define MASK_18MBPS             bit_const(3)    /* OFDM Modulation, 18 Mbps     */
#define MASK_24MBPS             bit_const(4)    /* OFDM Modulation, 24 Mbps     */
#define MASK_36MBPS             bit_const(5)    /* OFDM Modulation, 36 Mbps     */
#define MASK_48MBPS             bit_const(6)    /* OFDM Modulation, 48 Mbps     */
#define MASK_54MBPS             bit_const(7)    /* OFDM Modulation, 54 Mbps     */

#define BPSK                    3               /* BPSK Modulation type         */
#define QPSK                    2               /* QPSK Modulation type         */
#define QAM16                   1               /* 16 QAM Modulation type       */
#define QAM64                   0               /* 64 QAM Modulation type       */

#define LO_RATE                 (2<<2)          /* Coding rate = 1/2 (or 2/3)   */
#define HI_RATE                 (3<<2)          /* Coding rate = 3/4            */

#define CODE_6MBPS              (BPSK  | LO_RATE) /* OFDM Modulation,  6 Mbps   */
#define CODE_9MBPS              (BPSK  | HI_RATE) /* OFDM Modulation,  9 Mbps   */
#define CODE_12MBPS             (QPSK  | LO_RATE) /* OFDM Modulation, 12 Mbps   */
#define CODE_18MBPS             (QPSK  | HI_RATE) /* OFDM Modulation, 18 Mbps   */
#define CODE_24MBPS             (QAM16 | LO_RATE) /* OFDM Modulation, 24 Mbps   */
#define CODE_36MBPS             (QAM16 | HI_RATE) /* OFDM Modulation, 36 Mbps   */
#define CODE_48MBPS             (QAM64 | LO_RATE) /* OFDM Modulation, 48 Mbps   */
#define CODE_54MBPS             (QAM64 | HI_RATE) /* OFDM Modulation, 54 Mbps   */

#define CODE_1MBPS               10               /* CCK Modulation,   1 Mbps   */
#define CODE_2MBPS               20               /* CCK Modulation,   2 Mbps   */
#define CODE_5_5MBPS             55               /* CCK Modulation, 5.5 Mbps   */
#define CODE_11MBPS             110               /* CCK Modulation,  11 Mbps   */

#define  CCK_MANDATORY_RATES    (MASK_1MBPS |  MASK_2MBPS | MASK_5_5MBPS | MASK_11MBPS)
#define OFDM_MANDATORY_RATES    (MASK_6MBPS | MASK_12MBPS | MASK_24MBPS)

#define  DFLT_CCK_BASIC_RATES   CCK_MANDATORY_RATES
#define DFLT_OFDM_BASIC_RATES   OFDM_MANDATORY_RATES


#endif //_RATEMASK_H

