//****************************************************************
// Filter display support routines
//----------------------------------------------------------------
#ifndef FILTERS_H
#define FILTERS_H

#define is_ebt_filter(f)        (((f)->type     &  FILTER_TYPE_LAYER_2)                || \
                                 ((f)->src.type & ~FILTER_ADDR_NOT) == FILTER_ADDR_MAC || \
                                 ((f)->dst.type & ~FILTER_ADDR_NOT) == FILTER_ADDR_MAC || \
                                  (f)->protocol == FILTER_PROT_ARP                     || \
                                  (f)->protocol == FILTER_PROT_ARP_REQUEST             || \
                                  (f)->protocol == FILTER_PROT_ARP_REPLY                 )

#define is_device_filter(f)     (((f)->src.type & ~FILTER_ADDR_NOT) == FILTER_ADDR_DEVICE || \
                                 ((f)->dst.type & ~FILTER_ADDR_NOT) == FILTER_ADDR_DEVICE   )

#define is_tunnel_filter(f)    ((((f)->src.type & ~FILTER_ADDR_NOT) == FILTER_ADDR_IFACE && (f)->src.addr.iface == FILTER_IFACE_TUNNEL) || \
                                (((f)->dst.type & ~FILTER_ADDR_NOT) == FILTER_ADDR_IFACE && (f)->dst.addr.iface == FILTER_IFACE_TUNNEL)   )

#define is_ipv4_addr(a)         (((a).type & ~FILTER_ADDR_NOT) == FILTER_ADDR_IPV4)
#define is_ipv6_addr(a)         (((a).type & ~FILTER_ADDR_NOT) == FILTER_ADDR_IPV6)

#define is_set_ipv4_filter(f)    ((f)->set_ipv4[0])
#define is_set_ipv6_filter(f)    ((f)->set_ipv6[0])
#define is_set_ip_filter(f)      (is_set_ipv4_filter(f)  || is_set_ipv6_filter(f))
#define is_ipv4_filter(f)        (is_ipv4_addr((f)->src) || is_ipv4_addr((f)->dst) || is_set_ipv4_filter(f))
#define is_ipv6_filter(f)        (is_ipv6_addr((f)->src) || is_ipv6_addr((f)->dst) || is_set_ipv6_filter(f) || (f)->protocol == FILTER_PROT_ICMPV6)

#define is_dpi_filter(f)         ((f)->app_ident[0])
#define is_limit_filter(f)       ((f)->limit_value)
#define is_mangle_allow(f)       ((f)->limit_type == FILTER_TYPE_ALLOW)
#define is_time_filter(f)       (((f)->days_on && (f)->days_on != 0x7f) || (f)->time_on >= 0 || (f)->time_off >= 0)
#define is_disabled_filter(f)    ((f)->type & FILTER_TYPE_DISABLE     )
#define is_log_filter(f)         ((f)->type & FILTER_TYPE_LOG         )
#define is_set_filter(f)         ((f)->type & FILTER_TYPE_SET         )
#define is_set_qos_filter(f)     ((f)->type & FILTER_TYPE_SET_QOS     )
#define is_set_vlan_filter(f)    ((f)->type & FILTER_TYPE_SET_VLAN    )
#define is_set_dscp_filter(f)   (((f)->type & FILTER_TYPE_SET_DSCP    ) && !((f)->type & FILTER_TYPE_SET_QOS))
#define is_spec_l2_filter(f)     ((f)->type & FILTER_TYPE_LAYER_2     )
#define is_allow_filter(f)       ((f)->type & FILTER_TYPE_ALLOW       )
#define is_set_or_allow(f)       ((f)->type & FILTER_TYPE_ALLOW_OR_SET)
#define is_ctmark_filter(f)      ((f)->type & FILTER_TYPE_CTMARK_SET  )
#define is_inverted_address(a)   ((a)->type & FILTER_ADDR_NOT         )

#define is_setting_filter(f)    (is_set_filter(f)  || is_set_dscp_filter(f) || is_set_ip_filter(f) || is_limit_filter(f))
#define is_allowed_filter(f)    (is_set_filter(f)  || is_set_dscp_filter(f) || is_set_ip_filter(f) || is_limit_filter(f) || is_allow_filter(f))
#define is_ipt_filter(f)        (is_dpi_filter(f)  || is_set_dscp_filter(f) || is_set_ip_filter(f) || is_limit_filter(f) ||  is_time_filter(f) || is_tunnel_filter(f) || !is_ebt_filter(f))
#define is_bad_layer_filter(f)  (is_ebt_filter(f)  && is_ipt_filter(f))
#define is_bad_proto_filter(f)  (is_ipv4_filter(f) && is_ipv6_filter(f))

#define is_layer2_filter(f)     (is_ebt_filter(f))
#define is_layer3_filter(f)     (is_ipt_filter(f))

#define set_ip_addr(f)          (is_set_ipv6_filter(f) ? (f)->set_ipv6 : (f)->set_ipv4)

int dsp_filter_name (char *buffer, struct filter_cfg *filter, const char *delim);
int dsp_filter_type (char *buffer, struct filter_cfg *filter, const char *delim);
int dsp_filter_allow(char *buffer, struct filter_cfg *filter);
int dsp_filter_log  (char *buffer, struct filter_cfg *filter, const char *on_str, const char *off_str);
int dsp_filter_layer(char *buffer, struct filter_cfg *filter);
int dsp_filter_prot (char *buffer, struct filter_cfg *filter);
int dsp_filter_port (char *buffer, struct filter_cfg *filter);
int dsp_filter_app  (char *buffer, struct filter_cfg *filter, const char *prefix);
int dsp_filter_src  (char *buffer, struct filter_cfg *filter, const char *allow_str   , const char *deny_str, int   src_width, const char *delim);
int dsp_filter_dst  (char *buffer, struct filter_cfg *filter, const char *allow_str   , const char *deny_str, int   dst_width, const char *delim);
int dsp_filter_set  (char *buffer, struct filter_cfg *filter, const char * qos_str    , const char *dscp_str,
                                                              const char *vlan_str    , const char *  ip_str,                  const char *delim);
int dsp_fltr_lmt_cmd(char *buffer, struct filter_cfg *filter, const char *  cmd_strs[],                       int   cmd_width);
int dsp_fltr_lmt_str(char *buffer, struct filter_cfg *filter, const char *units_strs[],                       int   str_width);
int dsp_fltr_lmt_unt(char *buffer, struct filter_cfg *filter, const char *units_strs[],                       int units_width);
int dsp_fltr_lmt_val(char *buffer, struct filter_cfg *filter,                                                 int value_width);
int dsp_filter_time (char *buffer, struct filter_cfg *filter, const char *  sep_str   , const char *cmd_str,  int  time_width);
int dsp_filter_days (char *buffer, struct filter_cfg *filter, const char *  day_strs[], const char *cmd_str,  int  days_width);
int dsp_filter_state(char *buffer, struct filter_cfg *filter, const char *   on_str   , const char *off_str);

const char *vlan_name(int vlan_num, const char *delim);

#endif

