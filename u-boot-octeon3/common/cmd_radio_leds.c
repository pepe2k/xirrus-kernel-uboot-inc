/*
 * Radio LED Functions
 *
 */

#include <common.h>
#include <command.h>
#include <radio_info.h>
#include <otis.h>
#include <watchdog.h>
#include <asm/gpio.h>
#include <asm/arch/cvmx-pcie.h>
#include <asm/arch/lib_octeon.h>
#include <linux/ctype.h>

int radio_get_leds(void);
void radio_set_leds(int mask);
void radio_spin_leds(int spin_state);

extern uchar scd_reg_read (unsigned long reg);
extern void scd_reg_write (unsigned long reg, uchar val);
extern int avr_present (void);

/************************************************************************
 *  LED board specific routines                                         *
 ************************************************************************/
#define LED_START_DELAY                 (CONFIG_SYS_HZ/2)
#define LEDS_ON                         0xffff
#define LEDS_OFF                        0
#define LED_OFF_TIMEOUT                 5

/* ----- LED sweep definitions for 16 port ---------------------------------------- */
#define XS16_SWEEP_CYCLE                24
#define XS16_SWEEP_RATE                 (CONFIG_SYS_HZ/24)
#define XS16_SWEEP_ON_STATE             0
#define XS16_SWEEP_OFF_STATE            16
#define XS16_SWEEP_NUM_STATES           (sizeof(xs16_sweep_led_states) / sizeof(u32))

static u32 xs16_sweep_led_states[] = {  // orange & green concurrently
  0x00000000,
  0x00010001,
  0x00030003,
  0x00070007,
  0x000f000f,
  0x001f001f,
  0x003f003f,
  0x007f007f,
  0x00ff00ff,
  0x01ff01ff,
  0x03ff03ff,
  0x07ff07ff,
  0x0fff0fff,
  0x1fff1fff,
  0x3fff3fff,
  0x7fff7fff,
  0xffffffff,
  0xffffffff,
  0xfffefffe,
  0xfffcfffc,
  0xfff8fff8,
  0xfff0fff0,
  0xffe0ffe0,
  0xffc0ffc0,
  0xff80ff80,
  0xff00ff00,
  0xfe00fe00,
  0xfc00fc00,
  0xf800f800,
  0xf000f000,
  0xe000e000,
  0xc000c000,
  0x80008000
};

/* ----- LED sweep definitions for 8 port ---------------------------------------- */
#define XS8_SWEEP_CYCLE                 16
#define XS8_SWEEP_RATE                  (CONFIG_SYS_HZ/8)
#define XS8_SWEEP_ON_STATE              0
#define XS8_SWEEP_OFF_STATE             8
#define XS8_SWEEP_NUM_STATES            (sizeof(xs8_sweep_led_states) / sizeof(u32))

static u32 xs8_sweep_led_states[] = {  // orange & green concurrently
  0x00000000,
  0x00010001,
  0x00050005,
  0x00150015,
  0x00550055,
  0x01550155,
  0x05550555,
  0x15551555,
  0x55555555,
  0x55555555,
  0x55545554,
  0x55505550,
  0x55405540,
  0x55005500,
  0x54005400,
  0x50005000,
  0x40004000
};

/* ----- LED sweep definitions for 4 port ---------------------------------------- */
#define XS4_SWEEP_CYCLE                 8
#define XS4_SWEEP_RATE                  (CONFIG_SYS_HZ/5)
#define XS4_SWEEP_ON_STATE              0
#define XS4_SWEEP_OFF_STATE             4
#define XS4_SWEEP_NUM_STATES            (sizeof(xs4_sweep_led_states) / sizeof(u32))

static u32 xs4_sweep_led_states[] = {  // orange & green concurrently
  0x00000000,
  0x00010001,
  0x00050005,
  0x00150015,
  0x00550055,
  0x00550055,
  0x00540054,
  0x00500050,
  0x00400040
};

static u32 xd4_240_sweep_led_states[] = {  // orange & green concurrently
  0x00000000,
  0x00010001,
  0x00030003,
  0x00070007,
  0x000f000f,
  0x000f000f,
  0x000e000e,
  0x000c000c,
  0x00080008
};

static u32 xa4_240_sweep_led_states[] = {  // orange only
  0x00000000,
  0x00000001,
  0x00000003,
  0x00000007,
  0x0000000f,
  0x0000000f,
  0x0000000e,
  0x0000000c,
  0x00000008
};

/* ----- LED sweep definitions for 3 port ---------------------------------------- */
#define XS3_SWEEP_CYCLE                 6
#define XS3_SWEEP_RATE                  (CONFIG_SYS_HZ/4)
#define XS3_SWEEP_ON_STATE              0
#define XS3_SWEEP_OFF_STATE             3
#define XS3_SWEEP_NUM_STATES            (sizeof(xs3_sweep_led_states) / sizeof(u32))

static u32 xs3_sweep_led_states[] = {  // orange only
  0x00000000,
  0x00000001,
  0x00000003,
  0x00000007,
  0x00000007,
  0x00000006,
  0x00000004
};

/* ----- LED sweep definitions for 2 port ---------------------------------------- */
#define XS2_SWEEP_CYCLE                 4
#define XS2_SWEEP_RATE                  (CONFIG_SYS_HZ/3)
#define XS2_SWEEP_ON_STATE              0
#define XS2_SWEEP_OFF_STATE             2
#define XS2_SWEEP_NUM_STATES            (sizeof(xs2_sweep_led_states) / sizeof(u32))

static u32 xs2_sweep_led_states[] = {  // orange & green concurrently
  0x00000000,
  0x00010001,
  0x00030003,
  0x00030003,
  0x00020002
};

static u32 xs2_xr500_sweep_led_states[] = {  // orange only
  0x00000000,
  0x00000001,
  0x00000003,
  0x00000003,
  0x00000002
};

/* ----- LED global definitions --------------------------------------- */
static int  led_all_on      = 0;
static int  led_enable      = 1;
static int  led_spin_delay  = 0;
static int  led_spin_cntr   = 0;
static int  led_spin_rate   = XS16_SWEEP_RATE;
static int  led_cycle       = XS16_SWEEP_CYCLE;
static int  led_on_state    = XS16_SWEEP_ON_STATE;
static int  led_off_state   = XS16_SWEEP_OFF_STATE;
static int  led_num_states  = XS16_SWEEP_NUM_STATES;
static u32 *led_states      = xs16_sweep_led_states;

int  led_spin_state  = LEDS_NOT_SPINNING;

void radio_init_leds(void)
{
    char *s;

    led_enable     = 1;
    led_spin_state = LEDS_NOT_SPINNING;
    led_spin_delay = LED_START_DELAY;

    if ((s = getenv("ledshow")) != NULL) {
        led_enable = (strcmp(s, "none"  ) != 0);
        led_all_on = (strcmp(s, "all_on") == 0);
    }
    if ((s = getenv("ledrate")) != NULL) {
        led_spin_rate = simple_strtoul(s, NULL, 10);
    }
    if (led_all_on) {
        radio_set_leds(LEDS_ON);
    }
}

void radio_set_leds(int mask)
{
    DECLARE_GLOBAL_DATA_PTR;

    if (led_all_on) {
        mask = LEDS_ON;
    }

    if (led_enable) {
        if (gd->arch.board_desc.rev_major == BD_XR500) {
            mask = ~mask;  // invert for xr500
            if (mask>>16 & 0x1)  //rf0 green led
                gpio_set_value(6, 1);
            else
                gpio_set_value(6, 0);
            if (mask & 0x1)      //rf0 orange led
                gpio_set_value(8, 1);
            else
                gpio_set_value(8, 0);
            if (mask & 0x2)      //rf1 orange led
                gpio_set_value(7, 1);
            else
                gpio_set_value(7, 0);
        }
        else if ((gd->arch.board_desc.rev_major == BD_XR600  ) ||
                 (gd->arch.board_desc.rev_major == BD_XR700  ) ||
                 (gd->arch.board_desc.rev_major == BD_XD2_230)) {
            if (mask>>16 & 0x1)  //rf0 green led
                gpio_set_value(6, 1);
            else
                gpio_set_value(6, 0);
            if (mask>>16 & 0x2)  //rf1 green led
                gpio_set_value(7, 1);
            else
                gpio_set_value(7, 0);
            if (mask & 0x1)      //rf0 orange led
                gpio_set_value(9, 1);
            else
                gpio_set_value(9, 0);
            if (mask & 0x2)      //rf1 orange led
                gpio_set_value(10, 1);
            else
                gpio_set_value(10, 0);
        }
        else if (gd->arch.board_desc.rev_major == BD_XR1000) {
            if (mask>>16 & 0x1)  //rf0 green led
                gpio_set_value(6, 1);
            else
                gpio_set_value(6, 0);
            if (mask>>16 & 0x2)  //rf1 green led
                gpio_set_value(7, 1);
            else
                gpio_set_value(7, 0);
            if (mask & 0x1)      //rf0 orange led
                gpio_set_value(8, 1);
            else
                gpio_set_value(8, 0);
            if (mask & 0x2)      //rf1 orange led
                gpio_set_value(9, 1);
            else
                gpio_set_value(9, 0);
        }
        else if (gd->arch.board_desc.rev_major == BD_XD2_240) {
            if (mask>>16 & 0x1)  //rf0 green led
                gpio_set_value(12, 1);
            else
                gpio_set_value(12, 0);
            if (mask>>16 & 0x2)  //rf1 green led
                gpio_set_value(13, 1);
            else
                gpio_set_value(13, 0);
            if (mask & 0x1)      //rf0 orange led
                gpio_set_value(16, 1);
            else
                gpio_set_value(16, 0);
            if (mask & 0x2)      //rf1 orange led
                gpio_set_value(17, 1);
            else
                gpio_set_value(17, 0);
        }
        else if (gd->arch.board_desc.rev_major == BD_XD3_230) {
            if (mask>>16 & 0x1)  //rf0 green led
                gpio_set_value(12, 1);
            else
                gpio_set_value(12, 0);
            if (mask & 0x1)      //rf0 orange led
                gpio_set_value(16, 1);
            else
                gpio_set_value(16, 0);
            if (mask & 0x2)      //rf1 orange led
                gpio_set_value(13, 1);
            else
                gpio_set_value(13, 0);
            if (mask & 0x2)      //rf2 orange led
                gpio_set_value(17, 1);
            else
                gpio_set_value(17, 0);
        }
        else if (gd->arch.board_desc.rev_major == BD_XD4_130) {
            if (mask>>16 & 0x01)  //rf0 green led
                gpio_set_value(12, 1);
            else
                gpio_set_value(12, 0);
            if (mask>>16 & 0x04)  //rf1 green led
                gpio_set_value(13, 1);
            else
                gpio_set_value(13, 0);
            if (mask>>16 & 0x10)  //rf2 green led
                gpio_set_value(14, 1);
            else
                gpio_set_value(14, 0);
            if (mask>>16 & 0x40)  //rf3 green led
                gpio_set_value(15, 1);
            else
                gpio_set_value(15, 0);
            if (mask & 0x01)      //rf0 orange led
                gpio_set_value(16, 1);
            else
                gpio_set_value(16, 0);
            if (mask & 0x04)      //rf1 orange led
                gpio_set_value(17, 1);
            else
                gpio_set_value(17, 0);
            if (mask & 0x10)      //rf2 orange led
                gpio_set_value(18, 1);
            else
                gpio_set_value(18, 0);
            if (mask & 0x40)      //rf3 orange led
                gpio_set_value(19, 1);
            else
                gpio_set_value(19, 0);
        }
        else {
            *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_GRN_LED_OFFSET)) = mask>>16;
            *((u16 *) (CONFIG_XIRRUS_CONTROL_BASE + CONFIG_XIRRUS_CONTROL_ORG_LED_OFFSET)) = mask;
        }
    }
}

int radio_get_leds(void)
{
    return 0;
}

void radio_spin_leds(int spin_state)
{
    if (spin_state == LEDS_START_SPINNING && led_spin_state == LEDS_NOT_SPINNING)
        led_spin_cntr = (radio_get_leds() == LEDS_OFF) ? led_off_state : led_on_state;
    led_spin_state = spin_state;
}

void radio_wait_spin(void)  // this won't work unless LEDs are updated via interrupt or timer_hook is called by WATCHDOG_RESET()
{
    unsigned long long timeout = set_timeout(LED_OFF_TIMEOUT*1000000);

    while (   led_enable
           && led_spin_state != LEDS_NOT_SPINNING
           && !has_expired(timeout)) {
        WATCHDOG_RESET();
    }
}

void radio_leds_on(void)
{
    if (led_spin_state != LEDS_NOT_SPINNING)
        led_spin_state = LEDS_STOP_SPINNING_ON;
    else
        radio_set_leds(LEDS_ON);        /* turn on all radio leds */
}

void radio_leds_off(void)
{
    if (led_spin_state != LEDS_NOT_SPINNING)
        led_spin_state = LEDS_STOP_SPINNING_OFF;
    else
        radio_set_leds(LEDS_OFF);       /* turn off all radio leds */
}

void radio_leds_start(int num_radios)
{
    DECLARE_GLOBAL_DATA_PTR;

    if (led_enable) {
        if (num_radios == 2) {
            led_spin_rate  = XS2_SWEEP_RATE;
            led_cycle      = XS2_SWEEP_CYCLE;
            led_on_state   = XS2_SWEEP_ON_STATE;
            led_off_state  = XS2_SWEEP_OFF_STATE;
            led_num_states = XS2_SWEEP_NUM_STATES;
            led_states     = (gd->arch.board_desc.rev_major == BD_XR500) ? xs2_xr500_sweep_led_states : xs2_sweep_led_states;
        }
        else if (num_radios == 3) {
            led_spin_rate  = XS3_SWEEP_RATE;
            led_cycle      = XS3_SWEEP_CYCLE;
            led_on_state   = XS3_SWEEP_ON_STATE;
            led_off_state  = XS3_SWEEP_OFF_STATE;
            led_num_states = XS3_SWEEP_NUM_STATES;
            led_states     = xs3_sweep_led_states;
        }
        else if (num_radios == 4) {
            led_spin_rate  = XS4_SWEEP_RATE;
            led_cycle      = XS4_SWEEP_CYCLE;
            led_on_state   = XS4_SWEEP_ON_STATE;
            led_off_state  = XS4_SWEEP_OFF_STATE;
            led_num_states = XS4_SWEEP_NUM_STATES;
            led_states     = (gd->arch.board_desc.rev_major == BD_XD4_240) ? xd4_240_sweep_led_states :
                                                                             ((gd->arch.board_desc.rev_major == BD_XA4_240) ? xa4_240_sweep_led_states : xs4_sweep_led_states);
        }
        else if (num_radios == 8) {
            led_spin_rate  = XS8_SWEEP_RATE;
            led_cycle      = XS8_SWEEP_CYCLE;
            led_on_state   = XS8_SWEEP_ON_STATE;
            led_off_state  = XS8_SWEEP_OFF_STATE;
            led_num_states = XS8_SWEEP_NUM_STATES;
            led_states     = xs8_sweep_led_states;
        }
        else {
            led_spin_rate  = XS16_SWEEP_RATE;
            led_cycle      = XS16_SWEEP_CYCLE;
            led_on_state   = XS16_SWEEP_ON_STATE;
            led_off_state  = XS16_SWEEP_OFF_STATE;
            led_num_states = XS16_SWEEP_NUM_STATES;
            led_states     = xs16_sweep_led_states;
        }
        led_spin_cntr  = led_on_state;
        led_spin_state = LEDS_START_SPINNING;
    }

    cvmx_write_csr(CVMX_CIU_TIMX(0), (unsigned long long)led_spin_rate * ((unsigned long long)octeon_get_ioclk_hz() / 1000));
    //printf("CIU_TIM0 contents %x %x %x\n",cvmx_read_csr(CVMX_CIU_TIMX(0)),led_spin_rate,(unsigned long long)octeon_get_ioclk_hz());
}

#define CIU_INT0_SUM0_TIM0_BIT 52 /* CN52XX spec */

void timer_hook(int timestamp)
{
        //DECLARE_GLOBAL_DATA_PTR;

        if ((cvmx_read_csr(CVMX_CIU_INTX_SUM0(0)) >> CIU_INT0_SUM0_TIM0_BIT) & 0x1) {
                cvmx_write_csr(CVMX_CIU_INTX_SUM0(0),(1ull<<CIU_INT0_SUM0_TIM0_BIT));
                if (led_spin_delay)
                        --led_spin_delay;
                //else if (led_enable && led_spin_state != LEDS_NOT_SPINNING && (timestamp % led_spin_rate) == 0) {
                else if (led_enable && led_spin_state != LEDS_NOT_SPINNING) {
                        if (led_spin_state == LEDS_STOP_SPINNING_OFF && (led_spin_cntr % led_cycle) == led_off_state) {
                                led_spin_state = LEDS_NOT_SPINNING;
                                radio_set_leds(LEDS_OFF);               /* turn off all radio leds */
                                //printf("radio_set_leds(LEDS_OFF)\n");
                        }
                        else if (led_spin_state == LEDS_STOP_SPINNING_ON  && (led_spin_cntr % led_cycle) == led_on_state ) {
                                led_spin_state = LEDS_NOT_SPINNING;
                                radio_set_leds(LEDS_ON);                /* turn on all radio leds */
                                //printf("radio_set_leds(LEDS_ON)\n");
                        }
                        else {
                                radio_set_leds(led_states[led_spin_cntr % led_num_states]);
                                //if (gd->flags & GD_FLG_MAIN)
                                //    printf("radio_set_leds(led_states[led_spin_cntr \% led_num_states]) %08x\n",led_states[led_spin_cntr % led_num_states]);
                        }
                        led_spin_cntr++;
                }
        }
}

void show_boot_progress (int status)
{
        if (status < 0) {       /* --- error condition: blink status on red led --------------- */
                scd_reg_write(SCD_LED1_OFF, 0xff);                      /* turn green led off   */
                scd_reg_write(SCD_LED1_ON,  0x00);

                if (status != -1)
                        scd_reg_write(SCD_LED2_MSG, (((-status)/10) << 4) + ((-status)%10));

                scd_reg_write(SCD_LED2_ON,  CONFIG_FAST_LED_TIME/10);      /* blink red led fast   */
                scd_reg_write(SCD_LED2_OFF, CONFIG_FAST_LED_TIME/10);
                radio_leds_off();                                       /* radio leds off       */
        }
        else if (status == 0) { /* --- load boot file: start slow blink on green led ---------- */
                scd_reg_write(SCD_LED2_ON,  0);                         /* turn red led off     */
                scd_reg_write(SCD_LED2_OFF, 0xff);
                scd_reg_write(SCD_LED1_ON,  CONFIG_SLOW_LED_TIME/10);      /* blink green led fast */
                scd_reg_write(SCD_LED1_OFF, CONFIG_SLOW_LED_TIME/10);
                if (led_spin_state == LEDS_NOT_SPINNING) {
                        radio_init_leds();
                        radio_leds_on();
                        radio_spin_leds(LEDS_START_SPINNING);           /* spin radio leds      */
                }
        }
        else if (status == 2) { /* --- start handoff to linux: start fast blink on green led -- */
                scd_reg_write(SCD_LED2_ON,  0);                         /* turn red led off     */
                scd_reg_write(SCD_LED2_OFF, 0xff);
                scd_reg_write(SCD_LED1_ON,  CONFIG_FAST_LED_TIME/10);      /* blink green led fast */
                scd_reg_write(SCD_LED1_OFF, CONFIG_FAST_LED_TIME/10);
                radio_leds_on();                                        /* radio leds on        */
                radio_wait_spin();
        }
        else if (status == 15) {/* --- handoff to linux complete: turn green led on solid ----- */
                scd_reg_write(SCD_LED2_ON,  0);                         /* turn red led off     */
                scd_reg_write(SCD_LED2_OFF, 0xff);
                scd_reg_write(SCD_LED1_ON,  0xff);                      /* turn green led on    */
                scd_reg_write(SCD_LED1_OFF, 0x00);
                radio_set_leds(LEDS_OFF);                               /* radio leds off       */
        }
}

/************************************************************************************************************************
 * Radio LED Control Routines                                                                                           *
 ************************************************************************************************************************/

#define STATUS_LED      0x10000         /* green status/power led       */
#define FAULT_LED       0x20000         /* red fault/error              */

/* Radio LED
 *
 * Syntax:
 *      led [!]radio [!][radio] ....
 */

int do_radio_led ( cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        int not_flag;
        int type_flag;
        int led_mask;
        int scd_led_mask = 0;
        int value;
        char *s;
        DECLARE_GLOBAL_DATA_PTR;

        if (argc < 2) {
                printf ("Usage:\n%s\n", cmdtp->usage);
                return 1;
        }

        led_mask = radio_get_leds();
        if (scd_reg_read(SCD_LED1_ON)) scd_led_mask |= STATUS_LED;
        if (scd_reg_read(SCD_LED2_ON)) scd_led_mask |= FAULT_LED;

        while (--argc) {
                s = *++argv;
                type_flag = 'd';

                if ((not_flag = (*s == '!')) != 0)
                        ++s;

                if (*s == '0')
                        ++s;

                while (isalpha(*s))
                        if ((type_flag = *s++) == 'x')
                                break;

                value = simple_strtoul(s, NULL, type_flag == 'x' ? 16 : 10);

                switch (type_flag) {
                        case 'f': led_mask = 0;
                                  scd_led_mask = 0;
                                  break;
                        case 'n': led_mask = 0xffffffff;
                                  scd_led_mask = 0xffffffff;
                                  break;
                        case 'x': led_mask = value;
                                  break;
                        case 's': value = STATUS_LED;
                                  if (avr_present()) {
                                          not_flag ? (scd_led_mask &= ~value) : (scd_led_mask |= value);
                                  }
                                  else {
                                          if ((gd->arch.board_desc.rev_major == BD_XD2_240) ||
                                              (gd->arch.board_desc.rev_major == BD_XD3_230) ||
                                              (gd->arch.board_desc.rev_major == BD_XD4_130) ||
                                              (gd->arch.board_desc.rev_major == BD_XD4_240) ||
                                              (gd->arch.board_desc.rev_major == BD_XA4_240))
                                                  gpio_set_value(10, ~not_flag & 0x1);
                                          else
                                                  gpio_set_value(15, ~not_flag & 0x1);
                                  }
                                  break;
                        case 't': value = FAULT_LED;
                                  if (avr_present())
                                          not_flag ? (scd_led_mask &= ~value) : (scd_led_mask |= value);
                                  else
                                          if ((gd->arch.board_desc.rev_major == BD_XD2_240) ||
                                              (gd->arch.board_desc.rev_major == BD_XD3_230) ||
                                              (gd->arch.board_desc.rev_major == BD_XD4_130) ||
                                              (gd->arch.board_desc.rev_major == BD_XD4_240) ||
                                              (gd->arch.board_desc.rev_major == BD_XA4_240))
                                                  gpio_set_value(9, ~not_flag & 0x1);
                                          else
                                                  gpio_set_value(14, ~not_flag & 0x1);
                                  break;
                        case 'e': radio_spin_leds(1);
                                  return(1);
                        default : printf("Unrecognized radio number type (%c)\n", type_flag);
                                  return(1);
                }
        }
        radio_spin_leds(0);
        radio_set_leds(led_mask);
        scd_reg_write(SCD_LED1_OFF, (scd_led_mask & STATUS_LED) ? 0x00 : 0xff);  /* turn green led on or off */
        scd_reg_write(SCD_LED1_ON,  (scd_led_mask & STATUS_LED) ? 0xff : 0x00);
        scd_reg_write(SCD_LED2_OFF, (scd_led_mask & FAULT_LED)  ? 0x00 : 0xff);  /* turn red led on or off   */
        scd_reg_write(SCD_LED2_ON,  (scd_led_mask & FAULT_LED)  ? 0xff : 0x00);
        return(1);
}

U_BOOT_CMD(
        led,    16,     0,      do_radio_led,
        "radio led",
        "[on | off] [!][status] [!][fault] [rotate] [0x mask]\n"
        "    - turn on or off leds"
);
