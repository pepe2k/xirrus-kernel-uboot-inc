/*
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <environment.h>

#if defined(CONFIG_ENV_MULTI) && defined(CONFIG_ENV_IS_IN_FLASH) && defined(CONFIG_ENV_IS_IN_EEPROM)

env_t *env_ptr = (env_t *)CONFIG_ENV_FLASH_ADDR;
//char  env_name_spec[12];
char  *env_name_spec;

extern int flash_env_init (void);
extern int flash_saveenv (void);
extern uchar flash_env_get_char_spec (int index);
extern void flash_env_relocate_spec (void);

extern int eeprom_env_init (void);
extern int eeprom_saveenv (void);
extern uchar eeprom_env_get_char_spec (int index);
extern void eeprom_env_relocate_spec (void);

extern int eeprom_present (void);

int env_init (void)
{
        if (eeprom_present())
                return eeprom_env_init();
        else
                return flash_env_init();
}

int saveenv (void)
{
        if (eeprom_present())
                return eeprom_saveenv();
        else
                return flash_saveenv();
}

uchar env_get_char_spec (int index)
{
        if (eeprom_present())
                return eeprom_env_get_char_spec(index);
        else
                return flash_env_get_char_spec(index);
}

void env_relocate_spec (void)
{
        if (eeprom_present())
                eeprom_env_relocate_spec();
        else
                flash_env_relocate_spec();
}

#endif  // defined(CONFIG_ENV_MULTI) && defined(CONFIG_ENV_IS_IN_FLASH) && defined(CONFIG_ENV_IS_IN_EEPROM)
