/*
 * Copyright 2000-2009
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <version.h>
#include <linux/compiler.h>
#ifdef CONFIG_SYS_COREBOOT
#include <asm/arch/sysinfo.h>
#endif

const char __weak version_string[] = U_BOOT_VERSION_STRING;

static int do_version(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
#ifdef CONFIG_OCTEON_XARRAY
        DECLARE_GLOBAL_DATA_PTR;
	if (gd->avaya)
                printf("\n%s\n", alt_ver_string);
        else
                printf("\n%s\n", version_string);
#else
	printf("\n%s\n", version_string);
#endif //CONFIG_OCTEON_XARRAY
#ifndef CONFIG_OCTEON_XARRAY
#ifdef CC_VERSION_STRING
	puts(CC_VERSION_STRING "\n");
#endif
#ifdef LD_VERSION_STRING
	puts(LD_VERSION_STRING "\n");
#endif
#ifdef CONFIG_SYS_COREBOOT
	printf("coreboot-%s (%s)\n", lib_sysinfo.version, lib_sysinfo.build);
#endif
#endif //CONFIG_OCTEON_XARRAY
	return 0;
}

U_BOOT_CMD(
	version,	1,		1,	do_version,
#ifdef CONFIG_OCTEON_XARRAY
	"print monitor version",
#else
	"print monitor, compiler and linker version",
#endif
	""
);
