/************************************************************************
 *  System Configuration Device Management Routines                     *
 ************************************************************************/

#include <common.h>
#include <asm/gpio.h>
#include <asm/arch/cvmx-pcie.h>
#include <i2c.h>
#include <rtc.h>
#include <otis.h>
#include <radio_info.h>
#include <linux/ctype.h>
#include <version.h>
#include "../board/octeon/common/phy/aquantia_regs.h"
#include <asm/arch/cvmx-mdio.h>

int scd_loaded = 0;
int suppress_flash_messages = 0;

extern uint pcie_port_with_avalon;
extern uint radio_pcie_bar0_array[20];
extern uint avalon_pcie_bar0;

extern int flash_copy(ulong addr, ulong dest, int size);
extern int flash_sect_erase (ulong addr_first, ulong addr_last);
extern int flash_sect_protect (int p, ulong addr_first, ulong addr_last);
extern int radio_devid_check (int, int);
extern int radio_eeprom_read (int, int, char *, int);
extern int qca_data_process(u8 *qca_rd_ptr);
extern int get_reset_status (uchar *stat);
extern int avr_present (void);
extern int compass_present (void);

#define XMK_STR(x)              #x
#define MK_STR(x)               XMK_STR(x)

#define get_country_code(buff)  ({buff[0] = scd_reg_read(SCD_CNTRY_0); \
                                  buff[1] = scd_reg_read(SCD_CNTRY_1); })

#define set_country_code(buff)  ({scd_reg_write(SCD_CNTRY_0, buff[0]); \
                                  scd_reg_write(SCD_CNTRY_1, buff[1]); })

#define valid_mac_addr(e)       ((e[0]==0x00 && e[1]==0x0f && e[2]==0x7d &&   \
                                 (e[3]!=0xff || e[4]!=0xff || e[5]!=0xff)) || \
                                 (e[0]==0x50 && e[1]==0x60 && e[2]==0x28 &&   \
                                 (e[3]!=0xff || e[4]!=0xff || e[5]!=0xff)) || \
                                 (e[0]==0x48 && e[1]==0xc0 && e[2]==0x93 &&   \
                                 (e[3]!=0xff || e[4]!=0xff || e[5]!=0xff)) || \
                                 (e[0]==0x64 && e[1]==0xa7 && e[2]==0xdd &&   \
                                 (e[3]!=0xff || e[4]!=0xff || e[5]!=0xff)))

char *mon_strs[] = {
        " 0?", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct", "Nov", "Dec", "13?", "14?", "15?"
};

uint16_t byte_swap16(uint16_t data)
{
    uint16_t tmp, tmp1;

    tmp1 = (data >> 0) & 0xff;
    tmp = tmp1<<8;

    tmp1 = (data >> 8) & 0xff;
    tmp = tmp | (tmp1<<0);

    return(tmp);
}

uint32_t byte_swap32(uint32_t data)
{
    uint32_t tmp, tmp1;

    tmp1 = (data >> 0) & 0xff;
    tmp = tmp1<<24;

    tmp1 = (data >> 8) & 0xff;
    tmp = tmp | (tmp1<<16);

    tmp1 = (data >> 16) & 0xff;
    tmp = tmp | (tmp1<<8);

    tmp1 = (data >> 24) & 0xff;
    tmp = tmp | (tmp1<<0);

    return(tmp);
}

int check_sum(uchar *buff, int size)
{
        uchar sum = 0;

        while (size--)
                sum += *buff++;
        return sum;
}

int get_avalon (uint reg)
{
        uint64_t pcie_addr_base;
        int data;

        pcie_addr_base = (cvmx_pcie_get_mem_base_address(pcie_port_with_avalon) | (1ull << 63)) + avalon_pcie_bar0;
        data = cvmx_read64_uint32(pcie_addr_base + reg);
        return (data);
}

static sys_cfg_dev_t default_scd = {
        0x00,   0x00,           /* status register                                      */
        0x00,   0x80,           /* control register                                     */
        0x4b,   0x28,           /* system io speed & fan speed setting                  */
        0x04,   0x02,           /* system & core pll ratio, fallback (266MHz, 800MHz)   */
        0xf401,                 /* watchdog value, current (5 sec)                      */
        0xf401,                 /* watchdog value, reset   (5 sec)                      */
        0x46,   0x32,           /* processor speed (700Mhz), reset time (500 ms)        */
     {{ 0x00,   0x00,           /* LED1 count & message number                          */
        0x32,   0x32,           /* LED1 on & off time, current (500ms)                  */
        0x32,   0x32 },         /* LED1 on & off time, reset   (500ms)                  */
      { 0x00,   0x00,           /* LED2 count & message number                          */
        0x00,   0xff,           /* LED2 on & off time, current (off)                    */
        0x00,   0xff }},        /* LED2 on & off time, reset   (off)                    */
        0xffff,                 /* fan speed (rpm)                                      */
        0x05,   0x00,           /* scd major & minor revision level (5.0 for s/w scd)   */
        0xac,   0xff,           /* scd date code (10/12 for s/w scd) & std reg checksum */
        0xffffffff,             /* controller board serial number                       */
      { 0xffff,                 /* controller board type number                         */
        0xff,   0xff },         /* controller board version & revision number           */
     {{ 0xff,   0xff,           /* controller board mfg date, century & year            */
        0xff,   0xff },         /* controller board mfg date, month   & day             */
      { 0xff,   0xff,           /* controller board mfg date, hours   & minutes         */
        0xff }, 0xff },         /* controller board mfg date, seconds & location        */
        0xff, { 0xff,           /* ethernet mac address, count   & base[0]              */
        0xff,   0xff,           /* ethernet mac address, base[1] & base[2]              */
        0xff,   0xff,           /* ethernet mac address, base[3] & base[4]              */
        0xff }, 0xff,           /* radio    mac address, base[5] & country code[0]      */
        0xff, { 0xff,           /* radio    mac address, count   & base[0]              */
        0xff,   0xff,           /* radio    mac address, base[1] & base[2]              */
        0xff,   0xff,           /* radio    mac address, base[3] & base[4]              */
        0xff }, 0xff,           /* radio    mac address, base[5] & country code[1]      */
        /************************* extended register file *******************************/
      { .ext_chksum = 0xff },   /* extended register file checksum                      */
        0xff,                   /* extended register file revision                      */
        0xff,   0x00,           /* oscillator calibration byte     & mpc no start count */
      { 0xffff,                 /* array type number                                    */
        0xff,   0xff },         /* array version & revision number                      */
     {{ 0xff,   0xff,           /* array assembly date, century & year                  */
        0xff,   0xff },         /* array assembly date, month   & day                   */
      { 0xff,   0xff,           /* array assembly date, hours   & minutes               */
        0xff }, 0xff },         /* array assembly date, seconds & location              */
      { 0xff,   0xff,           /* system serial number string[ 0] & string[ 1]         */
        0xff,   0xff,           /* system serial number string[ 2] & string[ 3]         */
        0xff,   0xff,           /* system serial number string[ 4] & string[ 5]         */
        0xff,   0xff,           /* system serial number string[ 6] & string[ 7]         */
        0xff,   0xff,           /* system serial number string[ 8] & string[ 9]         */
        0xff,   0xff,           /* system serial number string[10] & string[11]         */
        0xff,   0xff,           /* system serial number string[12] & string[13]         */
        0xff,   0xff },         /* system serial number string[14] & string[15]         */
      { 0xff,   0xff,           /* license key string [ 0] & [ 1]                       */
        0xff,   0xff,           /* license key string [ 2] & [ 3]                       */
        0xff,   0xff,           /* license key string [ 4] & [ 5]                       */
        0xff,   0xff,           /* license key string [ 6] & [ 7]                       */
        0xff,   0xff,           /* license key string [ 8] & [ 9]                       */
        0xff,   0xff,           /* license key string [10] & [11]                       */
        0xff,   0xff,           /* license key string [12] & [13]                       */
        0xff,   0xff,           /* license key string [14] & [15]                       */
        0xff,   0xff,           /* license key string [16] & [17]                       */
        0xff,   0xff,           /* license key string [18] & [19]                       */
        0xff,   0xff,           /* license key string [20] & [21]                       */
        0xff,   0xff },         /* license key string [22] & [23]                       */
      { 0xffff,                 /* x_data_min_cal                                       */
        0xffff,                 /* x_data_max_cal                                       */
        0xffff,                 /* y_data_min_cal                                       */
        0xffff }                /* y_data_max_cal                                       */
};

static sys_cfg_dev_t scd_buffer;

void scd_load (void)
{
        if (!avr_present()) {
             /* load scd from flash and put it in DDR */
                memcpy(&scd_buffer, (void *)CONFIG_SCD_ADDR, sizeof(scd_buffer));
                scd_loaded = 1;
        }
}

int scd_read (unsigned long offset, uchar *buf, unsigned long len)
{
        int retval;

        if (!avr_present()) {
                if (scd_loaded) {  /* don't allow access until scd is loaded from flash */
                        memcpy(buf, (uchar *)(&scd_buffer) + offset, len);
                        retval = 0;
                }
                else {  /* read directly from flash - may not be running in DDR yet */
                        memcpy(buf, (void *)CONFIG_SCD_ADDR + offset, len);
                        retval = 0;
                }
        }
        else {
                retval = i2c_read(SYS_CONFIG_I2C_ADDR, offset, 1, buf, len);
        }

        return (retval);
}

int scd_write (unsigned long offset, uchar *buf, unsigned long len)
{
        int i;
        int retval;
        uchar tmp[sizeof(sys_cfg_dev_t)];

        if (!avr_present()) {
             /* Emulate the write protection mechanism in the AVR code where you need to write 0xff to most byte
                locations before a new value can be written.  If writing to a protected area, the write will only
                occur if >=1 of these conditions is true:
                  (1) writing 0xff (to unprotect)
                  (2) writing non-0xff value to a byte that is currently 0x00
                  (3) writing non-0xff value to a byte that is currently 0xff
                  (4) writing to SCD_BOARD_REV
             */
                if (scd_loaded) {  /* don't allow access until scd is loaded from flash */
                        memcpy(&tmp, buf, len);
                        for (i=0; i<len; i++) {
                                if ((offset <= SCD_NON_WP_CNT) ||
                                    (tmp[i] == 0xff) ||
                                    ((*((uchar *)(&scd_buffer) + offset + i) == 0x00) && (offset > SCD_NON_WP_CNT)) ||
                                    ((*((uchar *)(&scd_buffer) + offset + i) == 0xff) && (offset > SCD_NON_WP_CNT)) ||
                                    ((offset + i) == SCD_BOARD_REV))
                                        *((uchar *)(&scd_buffer) + offset + i) = tmp[i];
                        }
                        retval = 0;
                }
                else {
                        retval = 1;
                }
        }
        else {
                retval = i2c_write(SYS_CONFIG_I2C_ADDR, offset, 1, buf, len);
        }

        return (retval);
}

uchar scd_reg_read (unsigned long reg)
{
        uchar buf[1];

        return ((scd_read (reg, buf, 1) == 0) ? buf[0] : 0);
}

void scd_reg_write (unsigned long reg, uchar val)
{
        scd_write (reg, &val, 1);
}

void scd_print_bits(int reg, char *reg_strs[])
{
        int i;

        for (i = 0; i < 8; i++, reg <<= 1) {
                putc((*reg_strs[i] != ' ' && !(reg & 0x80)) ? '!' : ' ');
                puts ( reg_strs[i] );
        }
        putc('\n');
}

int do_scd_print (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        sys_cfg_dev_t scd;
        static char *stat_lo_strs[]  = {"irqrq ", " --   ", "eerst ", "mpcwd ", "scdwd ", "brown ", "exrst ", "pwron "};
        static char *stat_hi_strs[]  = {" --   ", " --   ", " --   ", " --   ", " --   ", " --   ", " --   ", " --   "};
        static char *ctrl_lo_strs[]  = {"irqen ", " --   ", "epmwr ", "ocnwd ", "scnwd ", "ld2on ", "ld1on ", "swrst "};
        static char *ctrl_hi_strs[]  = {"dpath ", " --   ", " --   ", " --   ", "fnsp1 ", "fnsp0 ", " --   ", " --   "};
        int gpio_bit, prev_gpio_bit;
        int fan_count, fan_speed, loop_count;
        unsigned long start_time, total_time;
        int tach;
        u8 lsb, msb;
        DECLARE_GLOBAL_DATA_PTR;

        memset(&scd, 0xff, sizeof(scd));

        if (scd_read(0,               (unsigned char *)&scd,               SCD_STD_REG_CNT) == 0 &&
            scd_read(SCD_EXT_REG_SET, (unsigned char *)&scd.ext_reg_start, SCD_EXT_REG_CNT) == 0)
        {
                printf("\n");

                if (scd.maj_rev        != 0xff &&
                    scd.min_rev        != 0xff &&
                    scd.sw_date        != 0xff)
                                                      printf("%-21.21s: %d.%02d  %d/%04d\n", "SCD Software Version", scd.maj_rev, scd.min_rev,
                                                                                                                    (scd.sw_date >> 4),
                                                                                                                    (scd.sw_date & 0xf)+2000);                  else printf("%-21.21s: -.--  --/----\n",           "SCD Software Version");
                                                      printf("\n");
                if (scd.system.version != 0xff &&
                    scd.system.type    != 0xffff) {   printf("%-21.21s: 180-%04u-%03u", "System Part   Number", byte_swap16(scd.system.type), scd.system.version);
                                                      if (SCD_HAS_MAJ_REV(scd.system.revision)) printf(".%c" , SCD_BOARD_MAJ_REV(scd.system.revision));
                                                      if (SCD_HAS_MIN_REV(scd.system.revision)) printf("%-2d", SCD_BOARD_MIN_REV(scd.system.revision));
                                                      printf("\n");                                                                                         }   else printf("%-21.21s: ---.----.---\n",            "System Part   Number");

                if (scd.sys_serial[0]  != 0xff)       printf("%-21.21s: %-.15s\n", "System Serial Number", scd.sys_serial);                                      else printf("%-21.21s: ---------------\n",         "System Serial Number");

                if (scd.sys_license[0] != 0xff)       printf("%-21.21s: %-.31s\n", "System License  Key",  scd.sys_license);                                     else printf("%-21.21s: -----.-----.-----.-----\n", "System License  Key");

                if (scd.assy.date.cent != 0xff &&
                    scd.assy.date.year != 0xff &&
                    scd.assy.date.mon  != 0xff &&
                    scd.assy.date.mday != 0xff)       printf("%-21.21s: %2d%02d-%s-%02d", "System Assembly Date", scd.assy.date.cent,       scd.assy.date.year,
                                                                                                        mon_strs[scd.assy.date.mon & 0xf], scd.assy.date.mday); else printf("%-21.21s: ----/---/--",               "System Assembly Date");
                if (scd.assy.time.hour != 0xff &&
                    scd.assy.time.min  != 0xff &&
                    scd.assy.time.sec  != 0xff)       printf(" %2d:%02d:%02d\n", scd.assy.time.hour, scd.assy.time.min, scd.assy.time.sec);                     else printf(" --:--:--\n");

                if (scd.assy.location  != 0xff)       printf("%-21.21s: %d\n", "System Assembly Site", scd.assy.location);                                       else printf("%-21.21s: ---\n",                     "System Assembly Site");

                if (scd.country_code_0 != 0xff &&
                    scd.country_code_1 != 0xff)       printf("%-21.21s: %c%c\n", "System Country  Code", scd.country_code_0, scd.country_code_1);                else printf("%-21.21s: --\n",                      "System Country  Code");
                                                      printf("\n");
                if (scd.board.version  != 0xff &&
                    scd.board.type     != 0xffff) {   printf("%-21.21s: 100-%04u-%03u", "Controller Part   #", byte_swap16(scd.board.type), scd.board.version);
                                                      if (SCD_HAS_MAJ_REV(scd.board.revision)) printf(".%c" , SCD_BOARD_MAJ_REV(scd.board.revision));
                                                      if (SCD_HAS_MIN_REV(scd.board.revision)) printf("%-2d", SCD_BOARD_MIN_REV(scd.board.revision));
                                                      printf("\n");                                                                                       }     else printf("%-21.21s: ---.----.---.---\n",        "Controller Part   #");

                if (scd.ser_num        != 0xffffffff) printf("%-21.21s: %010u\n", "Controller Serial #", byte_swap32(scd.ser_num));                             else printf("%-21.21s: ----------\n",              "Controller Serial #");

                if (scd.mfg.date.cent  != 0xff &&
                    scd.mfg.date.year  != 0xff &&
                    scd.mfg.date.mon   != 0xff &&
                    scd.mfg.date.mday  != 0xff)       printf("%-21.21s: %2d%02d-%s-%02d", "Controller Mfg Date", scd.mfg.date.cent,       scd.mfg.date.year,
                                                                                                        mon_strs[scd.mfg.date.mon & 0xf], scd.mfg.date.mday);   else printf("%-21.21s: ----/---/--",               "Controller Mfg Date");
                if (scd.mfg.time.hour  != 0xff &&
                    scd.mfg.time.min   != 0xff &&
                    scd.mfg.time.sec   != 0xff)       printf(" %2d:%02d:%02d\n", scd.mfg.time.hour, scd.mfg.time.min, scd.mfg.time.sec);                        else printf(" --:--:--\n");

                if (scd.mfg.location   != 0xff)       printf("%-21.21s: %d\n", "Controller Mfg Site", scd.mfg.location);                                        else printf("%-21.21s: ---\n",                     "Controller Mfg Site");

                                                      printf("\n");
                if (scd.eth_mac_count  != 0xff)       printf("%-21.21s: %d\n",                            "Eth MAC Addr Count", scd.eth_mac_count);             else printf("%-21.21s: --\n",                      "Eth MAC Addr Count");
                if (valid_mac_addr(scd.eth_mac_base)) printf("%-21.21s: %02x:%02x:%02x:%02x:%02x:%02x\n", "Eth MAC Addr Base",
                                                             scd.eth_mac_base[0], scd.eth_mac_base[1], scd.eth_mac_base[2],
                                                             scd.eth_mac_base[3], scd.eth_mac_base[4], scd.eth_mac_base[5]);                                    else printf("%-21.21s: --:--:--:--:--:--\n",       "Eth MAC Addr Base");

                if (scd.rad_mac_count  != 0xff)       printf("%-21.21s: %d\n",                            "IAP MAC Addr Count", scd.rad_mac_count);             else printf("%-21.21s: --\n",                      "IAP MAC Addr Count");
                if (valid_mac_addr(scd.rad_mac_base)) printf("%-21.21s: %02x:%02x:%02x:%02x:%02x:%02x\n", "IAP MAC Addr Base",
                                                             scd.rad_mac_base[0],scd.rad_mac_base[1],scd.rad_mac_base[2],
                                                             scd.rad_mac_base[3],scd.rad_mac_base[4],scd.rad_mac_base[5]);                                      else printf("%-21.21s: --:--:--:--:--:--\n",       "IAP MAC Addr Base");
		if (avr_present()) {
                        printf("\n");
                        if (OCTEON_IS_MODEL(OCTEON_CN6XXX))
                                printf("%-21.21s: CPU Speed = %4d MHz  IO Speed = %4d MHz  Reset   = %4d ms\n", "Processor Info", scd.proc_speed*10, scd.proc_io_speed*10, scd.reset_time*10);
                        else    printf("%-21.21s: CPU Speed = %4d MHz "                 " Reset    = %4d ms\n", "Processor Info", scd.proc_speed*10,                       scd.reset_time*10);

                        printf("%-21.21s: Current   = %2d.%1d sec  Initial  = %2d.%1d sec\n",           "Watchdog  Timer", byte_swap16(scd.wd_timer_cnt)/100, (byte_swap16(scd.wd_timer_cnt)%100)/10,
                                                                                                                           byte_swap16(scd.wd_reset_cnt)/100, (byte_swap16(scd.wd_reset_cnt)%100)/10);

                        printf("%-21.21s: On time   = %4d ms   Off time = %4d ms\n",                  "Green LED Initial", scd.led[SCD_GRN].on_rst*10, scd.led[SCD_GRN].off_rst*10);
                        printf("%-21.21s: On time   = %4d ms   Off time = %4d ms   Message = %d%d\n", "Green LED Current", scd.led[SCD_GRN].on    *10, scd.led[SCD_GRN].off    *10,
                                                                                                                  (scd.led[SCD_GRN].msg >> 4), scd.led[SCD_GRN].msg & 0x0f);
                        printf("%-21.21s: On time   = %4d ms   Off time = %4d ms\n",                  "Red   LED Initial", scd.led[SCD_RED].on_rst*10, scd.led[SCD_RED].off_rst*10);
                        printf("%-21.21s: On time   = %4d ms   Off time = %4d ms   Message = %d%d\n", "Red   LED Current", scd.led[SCD_RED].on    *10, scd.led[SCD_RED].off    *10,
                                                                                                                  (scd.led[SCD_RED].msg >> 4), scd.led[SCD_RED].msg & 0x0f);
                        printf("\n");
                        printf("%-21.21s: %4d rpm\n", "Current Fan Speed"   , byte_swap16(scd.fan_speed ));
                        printf("%-21.21s: %4d %%\n" , "Current Fan Setting" ,             scd.fan_setting);
		}
                else if (gd->arch.board_desc.rev_major == BD_XR2100) {
                        fan_count = 0;
                        loop_count = 100000;
                        prev_gpio_bit = gpio_bit = 1;
                        while (!((prev_gpio_bit == 0) && (gpio_bit == 1)) && --loop_count) {  // find rising edge of gpio 13
                              prev_gpio_bit = gpio_bit;
                              gpio_bit = gpio_get_value(13);
                        }
                        start_time = get_timer(0);  // get start time
                        while ((fan_count < 8) && loop_count) {
                              loop_count = 100000;
                              prev_gpio_bit = gpio_bit = 1;
                              while (!((prev_gpio_bit == 0) && (gpio_bit == 1)) && --loop_count) {  // find rising edge of gpio 13
                                    prev_gpio_bit = gpio_bit;
                                    gpio_bit = gpio_get_value(13);
                              }
                              fan_count++;
                        }
                        total_time = get_timer(0) - start_time;  // find time to see 8 pulses (msec)
                        if (loop_count && total_time > 0)
                              fan_speed = (int)(240000/total_time);  // 240000 ==> (4 revolutions/pulse) * (60 seconds/minute) * (1000 milli-seconds/second)
                        else
                              fan_speed = 0;
                        printf("\n");
                        printf("%-21.21s: %4d rpm\n", "Current Fan Speed", fan_speed);
                }
                else if ((gd->arch.board_desc.rev_major == BD_XD4_130) ||
                         (gd->arch.board_desc.rev_major == BD_XD4_240) ||
                         (gd->arch.board_desc.rev_major == BD_XA4_240)) {
                        if (i2c_read(CONFIG_SYS_DTT_LM63_ADDR, 0x46, 1, &lsb, 1) == 0 && i2c_read(CONFIG_SYS_DTT_LM63_ADDR, 0x47, 1, &msb, 1) == 0) {
                                tach = (msb << 8) | lsb;
                                fan_speed = tach ? 5400000/tach : 0;
                                printf("\n");
                                printf("%-21.21s: %4d rpm\n", "Current Fan Speed", fan_speed);
                        }
                }

		if (compass_present()) {
                        /* code prior to this version will have zeros before scd is cleared; the zero check will avoid zeros getting programmed into environment variables */
                        if (((scd.cmps.x_data_min == 0xffff) && (scd.cmps.x_data_max == 0xffff) && (scd.cmps.y_data_min == 0xffff) && (scd.cmps.y_data_max == 0xffff)) ||
                            ((scd.cmps.x_data_min == 0x0000) && (scd.cmps.x_data_max == 0x0000) && (scd.cmps.y_data_min == 0x0000) && (scd.cmps.y_data_max == 0x0000))) {
                                printf("%-21.21s: Xmin = ----   Xmax = ----   Ymin = ----   Ymax = ----\n", "Compass Cal Data");
                        }
                        else {
                                printf("%-21.21s: Xmin = %4d   Xmax = %5d   Ymin = %4d   Ymax = %5d\n", "Compass Cal Data",
                                       (short)byte_swap16(scd.cmps.x_data_min), (short)byte_swap16(scd.cmps.x_data_max),
                                       (short)byte_swap16(scd.cmps.y_data_min), (short)byte_swap16(scd.cmps.y_data_max));
                        }
		}

                printf("\n");
                printf("%-21.21s: %02x = ", "Stat Reg High Byte", scd.stat_hi);  scd_print_bits(scd.stat_hi, stat_hi_strs);
                printf("%-21.21s: %02x = ", "Stat Reg Low  Byte", scd.stat_lo);  scd_print_bits(scd.stat_lo, stat_lo_strs);
                printf("%-21.21s: %02x = ", "Ctrl Reg High Byte", scd.ctrl_hi);  scd_print_bits(scd.ctrl_hi, ctrl_hi_strs);
                printf("%-21.21s: %02x = ", "Ctrl Reg Low  Byte", scd.ctrl_lo);  scd_print_bits(scd.ctrl_lo, ctrl_lo_strs);
                printf("\n");
                return 0;
        }
        else
                return 1;
}

int do_scd_reset (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        sys_cfg_dev_t scd;
        puts("Resetting non-write protected SCD information to defaults ... ");
        scd_read(SCD_PROC_SPEED, (unsigned char *)&scd.proc_speed, sizeof(scd.proc_speed));
        if (scd_write(0, (unsigned char *)(&default_scd), SCD_NON_WP_CNT) != 0) {
                puts("## Failed ##\n");
                return 1;
        }
        scd_write(SCD_PROC_SPEED, (unsigned char *)&scd.proc_speed, sizeof(scd.proc_speed));
        puts("done\n");
        return 0;
}

int do_scd_clear (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        sys_cfg_dev_t scd;
        puts("Clearing all SCD information ... ");
        scd_read(SCD_PROC_SPEED, (unsigned char *)&scd.proc_speed, sizeof(scd.proc_speed));
        if (scd_write(0, (unsigned char *)(&default_scd), sizeof(default_scd)) != 0) {
                puts("## Failed ##\n");
                return 1;
        }
        scd_write(SCD_PROC_SPEED, (unsigned char *)&scd.proc_speed, sizeof(scd.proc_speed));
        puts("done\n");
        return 0;
}

int do_scd_save (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        sys_cfg_dev_t tmp;
        uchar ctrl;
        ulong start_time;
        int retval;

        if (!avr_present()) {
                if (!flag) {
                        puts("Saving SCD settings to SCD Flash ... ");
                }
                /* copy scd from DDR to flash - scd must be copied to not alter scd structure in DDR */
	        memcpy(&tmp, &scd_buffer, sizeof(scd_buffer));

                /* update SCD checksums */
                tmp.chksum -= check_sum((uchar *)&tmp.stat_lo, SCD_STD_REG_CNT);
                tmp.ext_reg_start -= check_sum((uchar *)&tmp.ext_reg_start, SCD_EXT_REG_CNT);

                suppress_flash_messages = 1;
                retval =                  flash_sect_protect(0,           CONFIG_SCD_ADDR, CONFIG_SCD_ADDR+CONFIG_SCD_SECT_SIZE-1);
                if (retval == 0) retval = flash_sect_erase  (             CONFIG_SCD_ADDR, CONFIG_SCD_ADDR+CONFIG_SCD_SECT_SIZE-1);
                if (retval == 0) retval = flash_copy((unsigned long)&tmp, CONFIG_SCD_ADDR, sizeof(scd_buffer));
                if (retval == 0) retval = flash_sect_protect(1,           CONFIG_SCD_ADDR, CONFIG_SCD_ADDR+CONFIG_SCD_SECT_SIZE-1);
                suppress_flash_messages = 0;

                if (retval == 0) {
                        if (!flag) {
                                puts("done");
                                if (argc) puts("\n");
                        }
                        return 0;
                }
                else {
                        if (!flag) {
                                puts("fail");
                                if (argc) puts("\n");
                        }
                        return 1;
                }
        }
        else {
                puts("Saving SCD settings to SCD EEPROM ... ");
                if (scd_read(SCD_CTRL_LO, &ctrl, 1) != 0) {
                        puts("## Failed ##\n");
                        return 1;
                }
                ctrl |= CRL_EEP_WR;
                scd_write(SCD_CTRL_LO, &ctrl, 1);
                start_time = get_timer(0);
                while (ctrl & CRL_EEP_WR) {
                        if (get_timer(start_time) > SCD_WR_TIMEOUT) {
                                puts("## Timeout ##\n");
                                return 1;
                        }
                        if (scd_read(SCD_CTRL_LO, &ctrl, 1) != 0) {
                                puts("## Failed ##\n");
                                return 1;
                        }
                }
                puts("done");
                if (argc) puts("\n");
                return 0;
        }
}

/* ----- system configuration device set routine lookup tables -------- */
struct base_def_t {
        char *name;
        int   addr;
};
static struct base_def_t *base_ptr;
static struct base_def_t  base_defs[] = {
        {"array",       68}, {"aryid", 68}, {"ary",  68},
        {"board",       36}, {"bdid",  36}, {"bd",   36},
        {"control",      2}, {"ctrl",   2},
        {"controller",  36}, {"ctrlr", 36}, {"ctl",  36},
        {"country",     55},
        {"cpu",         36},
        {"ethernet",    48}, {"eth",   48},
        {"fan",          4},
        {"green",       14}, {"grn",   14},
        {"hardware",    36}, {"hwid",  36}, {"hw",   36},
        {"led1",        14}, {"led2",  20},
        {"mpc",         67},
        {"oscillator",  66}, {"osccal",66}, {"osc",  66},
        {"pll",          4},
        {"processor",   11}, {"proc",  11},
        {"radio",       56}, {"rad",   56}, {"iap",  56},
        {"red",         20},
        {"status",       0}, {"stat",   0},
        {"system",      68}, {"sysid", 68}, {"sys",  68},
        {"watchdog",     8}, {"wdt",    8},
        {NULL,           0}
};

struct offset_def_t {
        char *name;
        int   offset;
};
static struct offset_def_t *offset_ptr;
static struct offset_def_t  offset_defs[] = {
        {"address",     0}, {"addr",  0},
        {"base",        1},
        {"board",       0}, {"bd",    0},
        {"card",        0},
        {"ccb",         0},
        {"code",        0},
        {"core",        1},
        {"count",       0}, {"cnt",   0},
        {"current",     0}, {"cur",   0},
        {"date",        0},
        {"fallback",    2}, {"fbk",   2},
        {"id",          0},
        {"io",         -8},
        {"initial",     2}, {"init",  2},
        {"led",         0},
        {"location",    7}, {"loc",   7},
        {"mac",         0},
        {"message",     1}, {"msg",   1},
        {"now",        -1},
        {"number",      0}, {"num",   0},
        {"off",         3}, {"on",    2},
        {"part",        0}, {"prt#",  0},
        {"register",    0}, {"reg",   0},
        {"reset",       2}, {"rst",   2},
        {"serial",     -4}, {"ser#", -4},
        {"setting",     1}, {"set",   1},
        {"site",        7},
        {"speed",       1}, {"spd",   1},
        {"time",        4},
        {"timer",       0}, {"tmr",   0},
        {"manufacture", 4}, {"manuf", 4}, {"mfg",  4},
        {NULL,          0}
};
static struct offset_def_t ext_offset_defs[] = {
        {"assembly",    4}, {"assy",  4},
        {"calibration", 0}, {"cal",   0},
        {"date",        0},
        {"id",          0},
        {"license",    28},
        {"location",    7}, {"loc",   7},
        {"no",          0},
        {"nostart",     0},
        {"now",        -1},
        {"number",      0}, {"num",   0},
        {"part",        0}, {"prt#",  0},
        {"serial",     12}, {"ser#", 12},
        {"site",        7},
        {"start",       0},
        {"time",        4},
        {"xmin",       52},
        {"xmax",       54},
        {"ymin",       56},
        {"ymax",       58},
        {NULL,          0}
};

struct ctrl_bit_def_t {
        char *name;
        char *def;
        int   mask;
};
static struct ctrl_bit_def_t *ctrl_bit_ptr;
static struct ctrl_bit_def_t  ctrl_bit_defs[] = {
        {"dpath",       "pcie data path select",        0x0080},
        {"fnsp1",       "fan speed control - bit 1",    0x0008},
        {"fnsp0",       "fan speed control - bit 0",    0x0004},
        {"irqen",       "interrupt enable",             0x8000},
        {"epmwr",       "scd eeprom write",             0x2000},
        {"ocnwd",       "oct watchdog disable",         0x1000},
        {"scnwd",       "scd watchdog disable",         0x0800},
        {"ld2on",       "led2 force on",                0x0400},
        {"ld1on",       "led1 force on",                0x0200},
        {"swrst",       "software reset",               0x0100},
        {NULL,          NULL,                           0}
};

static struct reg_def_t {
        char *reg_name;
        int   divisor;
        int   base;
        int   size;
} reg_defs[] = {
        {"status  register",      1, 16,  2},    {"status  register",      1, 16,  0},
        {"control register",      1, 16,  2},    {"control register",      1, 16,  0},
        {"processor io speed",   10, 10,  1},    {"fan speed setting",     1, 10,  1},
        {"spare 0",               1, 10,  1},    {"spare 1",               1, 10,  1},
        {"watchdog timer",       10, 10,  2},    {"watchdog timer",       10, 10,  0},
        {"watchdog reset time",  10, 10,  2},    {"watchdog reset time",  10, 10,  0},
        {"processor speed",      10, 10,  1},    {"processor reset time", 10, 10,  1},
        {"grn led cur count",    10, 10,  1},    {"grn led message",       1, 16,  1},
        {"grn led on  time",     10, 10,  1},    {"grn led off time",     10, 10,  1},
        {"grn led rst on  time", 10, 10,  1},    {"grn led rst off time", 10, 10,  1},
        {"red led cur count",    10, 10,  1},    {"red led message",       1, 16,  1},
        {"red led on time",      10, 10,  1},    {"red led off time",     10, 10,  1},
        {"red led rst on  time", 10, 10,  1},    {"red led rst off time", 10, 10,  1},
        {"fan speed",             1, 10,  2},    {"fan speed",             1, 10,  0},
        {"software major rev",    1, 10,  1},    {"software minor rev",    1, 10,  1},
        {"software date code",    1, 16,  1},    {"std reg checksum",      1, 16,  1},
        {"controller serial #",   1, 10,  4},    {"controller serial #",   1, 10,  0},
        {"controller serial #",   1, 10,  0},    {"controller serial #",   1, 10,  0},
        {"controller part num",   1, 10,  1},    {"controller type",       1, 10,  1},
        {"controller version",    1, 10,  1},    {"controller revision",   1, 10,  1},
        {"controller mfg date",   1, 10,  1},    {"controller mfg year",   1, 10,  1},
        {"controller mfg month",  1, 10,  1},    {"controller mfg day",    1, 10,  1},
        {"controller mfg time",   1, 10,  1},    {"controller mfg mins",   1, 10,  1},
        {"controller mfg secs",   1, 10,  1},    {"controller mfg loc",    1, 10,  1},
        {"ethernet mac count",    1, 10,  1},    {"ethernet mac base",     1, 16,  6},
        {"ethernet mac base",     1, 16,  0},    {"ethernet mac base",     1, 16,  0},
        {"ethernet mac base",     1, 16,  0},    {"ethernet mac base",     1, 16,  0},
        {"ethernet mac base",     1, 16,  0},    {"country code byte 0",   1, 16,  1},
        {"radio mac count",       1, 10,  1},    {"radio mac base",        1, 16,  6},
        {"radio mac base",        1, 16,  0},    {"radio mac base",        1, 16,  0},
        {"radio mac base",        1, 16,  0},    {"radio mac base",        1, 16,  0},
        {"radio mac base",        1, 16,  0},    {"country code byte 1",   1, 16,  1},
        {"ext reg checksum",      1, 16,  1},    {"ext reg revision",      1, 16,  1},
        {"osc calibration byte",  1, 16,  1},    {"mpc no start count",    1, 16,  1},
        {"array part number",     1, 10,  1},    {"array type",            1, 10,  1},
        {"array version",         1, 10,  1},    {"array revision",        1, 10,  1},
        {"array assy date",       1, 10,  1},    {"array assy year",       1, 10,  1},
        {"array assy month",      1, 10,  1},    {"array assy day",        1, 10,  1},
        {"array assy time",       1, 10,  1},    {"array assy minutes",    1, 10,  1},
        {"array assy seconds",    1, 10,  1},    {"array assy location",   1, 10,  1},
        {"array serial string",   1, 16, 16},    {"array serial string",   1, 16,  0},
        {"array serial string",   1, 16,  0},    {"array serial string",   1, 16,  0},
        {"array serial string",   1, 16,  0},    {"array serial string",   1, 16,  0},
        {"array serial string",   1, 16,  0},    {"array serial string",   1, 16,  0},
        {"array serial string",   1, 16,  0},    {"array serial string",   1, 16,  0},
        {"array serial string",   1, 16,  0},    {"array serial string",   1, 16,  0},
        {"array serial string",   1, 16,  0},    {"array serial string",   1, 16,  0},
        {"array serial string",   1, 16,  0},    {"array serial string",   1, 16,  0},
        {"array license key",     1, 16, 24},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"array license key",     1, 16,  0},    {"array license key",     1, 16,  0},
        {"x-axis minimum",        1, 10,  2},    {"x-axis minimum",        1, 10,  0},
        {"x-axis maximum",        1, 10,  2},    {"x-axis maximum",        1, 10,  0},
        {"y-axis minimum",        1, 10,  2},    {"y-axis minimum",        1, 10,  0},
        {"y-axis maximum",        1, 10,  2},    {"y-axis maximum",        1, 10,  0},
        {NULL,                    0,  0,  0}
};

int do_scd_set (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        int base_addr  = -1;
        int reg_size   =  0;
        int offset     =  0;
        int reg_num    =  0;
        int not_flag   =  0;
        ushort ctrl    =  0;
        unsigned value =  0;
        char *value_ptr;
        char *cur_argv;
        uchar enetaddr[6];
        char verify[8];
        struct rtc_time rtc;
        sys_cfg_dev_t   scd;
        scd_mfg_info_t  mfg;
        scd_board_id_t  board;
        int board_level;
        char *s;
        int i, year;

        if (scd_read(0, (unsigned char *)&scd, SCD_STD_REG_CNT) != 0) {
            puts(" ## Unable to read SCD contents ##\n");
            return 1;
        }

        int num_regs = SCD_STD_REG_CNT + SCD_EXT_REG_CNT;

        while (--argc) {
                cur_argv = *++argv;
                if (*cur_argv == '?') {
                        printf("Usage: scd set [[!]ctrlbit | [basename offset [ ... offset] value] ...");

                        printf("\n\n   - ctrlbit : ");
                        ctrl_bit_ptr = ctrl_bit_defs;
                        while (ctrl_bit_ptr->name) {
                                printf("%-8s ", ctrl_bit_ptr->name);
                                if ((++ctrl_bit_ptr - ctrl_bit_defs) % 7 == 0 && ctrl_bit_ptr->name)
                                        printf("\n               ");
                        }

                        printf("\n\n   - basename: ");
                        base_ptr = base_defs;
                        while (base_ptr->name) {
                                printf("%-12s ", base_ptr->name);
                                if ((++base_ptr - base_defs) % 5 == 0 && base_ptr->name)
                                        printf("\n               ");
                        }

                        printf("\n\n   - offset  : ");
                        offset_ptr = offset_defs;
                        while (offset_ptr->name) {
                                printf("%-12s ", offset_ptr->name);
                                if ((++offset_ptr - offset_defs) % 5 == 0 && offset_ptr->name)
                                        printf("\n               ");
                        }

                        printf("\n\n   - ext off : ");
                        offset_ptr = ext_offset_defs;
                        while (offset_ptr->name) {
                                printf("%-12s ", offset_ptr->name);
                                if ((++offset_ptr - ext_offset_defs) % 5 == 0 && offset_ptr->name)
                                        printf("\n               ");
                        }

                        printf("\n");
                        return 0;
                }
                if (*cur_argv == '!') {
                        not_flag = 1;
                        if (!*++cur_argv)
                                goto next_arg;
                }
                for (base_ptr = base_defs; base_ptr->name; base_ptr++) {
                        if (strcmp(cur_argv, base_ptr->name) == 0) {
                                base_addr  = base_ptr->addr;
                                goto next_arg;
                        }
                }
                for (offset_ptr = (base_addr < SCD_STD_REG_CNT) ? offset_defs : ext_offset_defs; offset_ptr->name; offset_ptr++) {
                        if (strcmp(cur_argv, offset_ptr->name) == 0 && offset_ptr->offset != -1) {
                                offset += offset_ptr->offset;
                                goto next_arg;
                        }
                }
                for (ctrl_bit_ptr = ctrl_bit_defs; ctrl_bit_ptr->name; ctrl_bit_ptr++) {
                        if (strcmp(cur_argv, ctrl_bit_ptr->name) == 0) {
                                printf("%s control register %s bit ", not_flag ? "Clearing" : "Setting ", ctrl_bit_ptr->def);
                                if (scd_read(SCD_CTRL_LO, (uchar *)&ctrl, 2) != 0) {
                                        puts(" ## Read failed ##\n");
                                        goto next_reg;
                                }
                                not_flag ? (ctrl &= ~ctrl_bit_ptr->mask) : (ctrl |= ctrl_bit_ptr->mask);
                                if (scd_write(SCD_CTRL_LO, (uchar *)&ctrl, 2) != 0) {
                                        puts(" ## Write failed ##\n");
                                        goto next_reg;
                                }
                                puts("\n");
                                goto next_reg;
                        }
                }
                if (base_addr < 0) {
                        puts("Syntax error: register base not specified\n");
                        goto next_reg;
                }
                reg_num = base_addr + offset;
                if (reg_num >= num_regs) {
                        printf("Syntax error: register number (%d) out of range\n", reg_num);
                        goto next_reg;
                }
                reg_size = reg_defs[reg_num].size;
                if (!reg_size) {
                        puts("Syntax error: invalid register specified\n");
                        goto next_reg;
                }
                switch (reg_num) {
                        case SCD_BOARD_ID:
                        case SCD_SYSTEM_ID:
                                if ((s = strchr(cur_argv, '-')) != NULL && strchr(++s, '-') != NULL) {
                                        board_level = simple_strtoul(cur_argv, &s, 10);
                                        if (*s) s++;
                                }
                                else {
                                        board_level = 0;
                                        s = cur_argv;
                                }
                                board.type     = simple_strtoul(s, &s, 10); if (*s) s++;
                                board.version  = simple_strtoul(s, &s, 10); if (*s) s++;
                                board.revision = (*s) ? SCD_BOARD_SET_REV(s) : 0xff;
                                if (board_level) printf("Setting  %-20s to %03u-%04u-%03u", reg_defs[reg_num].reg_name, board_level, board.type, board.version);
                                else             printf("Setting  %-20s to %04u-%03u",      reg_defs[reg_num].reg_name,              board.type, board.version);
                                printf(SCD_HAS_MAJ_REV(board.revision) ? ".%c"  : "  ", SCD_BOARD_MAJ_REV(board.revision));
                                printf(SCD_HAS_MIN_REV(board.revision) ? "%-2d" : "  ", SCD_BOARD_MIN_REV(board.revision));
                                board.type = byte_swap16(board.type);
                                value_ptr  = (char *)&board;
                                reg_size   = sizeof(board);
                                break;

                        case SCD_MAN_TIME:
                        case SCD_MAN_DATE:
                        case SCD_SYS_TIME:
                        case SCD_SYS_DATE:

                                if (strcmp(cur_argv, "now") == 0) {
                                        reg_num = (reg_num == SCD_MAN_TIME || reg_num == SCD_MAN_DATE) ? SCD_MAN_DATE : SCD_SYS_DATE;
                                        rtc_get(&rtc);
                                        mfg.time.hour = rtc.tm_hour;
                                        mfg.time.min  = rtc.tm_min;
                                        mfg.time.sec  = rtc.tm_sec;
                                        mfg.date.cent = rtc.tm_year/100;
                                        mfg.date.year = rtc.tm_year%100;
                                        mfg.date.mon  = rtc.tm_mon;
                                        mfg.date.mday = rtc.tm_mday;
                                        printf("Setting  %-20s to %d:%02d:%02d and  %-20s to %d%02d-%s-%02d ",
                                               reg_defs[reg_num + 4].reg_name, mfg.time.hour, mfg.time.min,  mfg.time.sec,
                                               reg_defs[reg_num    ].reg_name, mfg.date.cent, mfg.date.year, mon_strs[mfg.date.mon & 0xf], mfg.date.mday);
                                        value_ptr = (char *)&mfg;
                                        reg_size  = sizeof(mfg.date) + sizeof(mfg.time);
                                }
                                else if (reg_num == SCD_MAN_TIME || reg_num == SCD_SYS_TIME) {
                                        mfg.time.hour = simple_strtoul(cur_argv, &s, 10);
                                        if (*s) s++;
                                        mfg.time.min  = simple_strtoul(s, &s, 10);
                                        if (*s) s++;
                                        mfg.time.sec  = simple_strtoul(s, &s, 10);
                                        printf("Setting  %-20s to %d:%02d:%02d ", reg_defs[reg_num].reg_name,
                                               mfg.time.hour, mfg.time.min, mfg.time.sec);
                                        value_ptr = (char *)&mfg.time;
                                        reg_size  = sizeof(mfg.time);
                                }
                                else {
                                        year = simple_strtoul(cur_argv, &s, 10);
                                        if (year == 0xffff) {
                                                mfg.date.cent = 0xff;
                                                mfg.date.year = 0xff;
                                        }
                                        else {
                                                mfg.date.cent = year/100;
                                                mfg.date.year = year%100;
                                        }
                                        if (*s) s++;
                                        mfg.date.mon  = simple_strtoul(s, &s, 10);
                                        if (*s) s++;
                                        mfg.date.mday = simple_strtoul(s, NULL, 10);
                                        printf("Setting  %-20s to %d%02d-%s-%02d ", reg_defs[reg_num].reg_name,
                                               mfg.date.cent, mfg.date.year, mon_strs[mfg.date.mon & 0xf], mfg.date.mday);
                                        value_ptr = (char *)&mfg.date;
                                        reg_size  = sizeof(mfg.date);
                                }
                                break;

                        case SCD_ETH_MAC:
                        case SCD_RAD_MAC:
                                s = cur_argv;
                                for (i = 0; i < 6; ++i) {
                                        enetaddr[i] = simple_strtoul(s, &s, 16);
                                        if (*s) s++;
                                }
                                printf("Setting  %-20s to %02x:%02x:%02x:%02x:%02x:%02x ", reg_defs[reg_num].reg_name,
                                       enetaddr[0], enetaddr[1], enetaddr[2], enetaddr[3], enetaddr[4], enetaddr[5]);
                                value_ptr = (char *)enetaddr;
                                reg_size = sizeof(enetaddr);
                                break;

                        case SCD_CNTRY_0:
                                if (!isalpha(cur_argv[0]) || !isalpha(cur_argv[1])) {
                                    cur_argv[0] = 0xff;
                                    cur_argv[1] = 0xff;
                                    printf("Setting  %-20s to 0xffff ", "country code");
                                }
                                else {
                                    cur_argv[0] = toupper(cur_argv[0]);
                                    cur_argv[1] = toupper(cur_argv[1]);
                                    printf("Setting  %-20s to %c%c ", "country code", cur_argv[0], cur_argv[1]);
                                }
                                set_country_code(cur_argv);
                                get_country_code(verify);
                                puts(memcmp(cur_argv, verify, 2) ? " ## Verify failed ##\n" : "\n");
                                goto next_reg;

                        case SCD_SYS_SERIAL:
                                if (!isalpha(cur_argv[0])) {
                                    printf("Setting  %-20s to all 0xff ", reg_defs[reg_num].reg_name);
                                    memset( scd.sys_serial, 0xff    , sizeof(scd.sys_serial)  );
                                }
                                else {
                                    printf("Setting  %-20s to %s ", reg_defs[reg_num].reg_name, cur_argv);
                                    memset( scd.sys_serial, 0       , sizeof(scd.sys_serial)  );
                                    strncpy((char *)scd.sys_serial, cur_argv, sizeof(scd.sys_serial)-1);
                                }
                                value_ptr = (char *)scd.sys_serial;
                                reg_size  = sizeof(scd.sys_serial);
                                break;

                        case SCD_SYS_LICENSE:
                                if (!isalnum(cur_argv[0])) {
                                    printf("Setting  %-20s to all 0xff ", reg_defs[reg_num].reg_name);
                                    memset( scd.sys_license, 0xff    , sizeof(scd.sys_license)  );
                                }
                                else {
                                    printf("Setting  %-20s to %s ", reg_defs[reg_num].reg_name, cur_argv);
                                    memset( scd.sys_license, 0       , sizeof(scd.sys_license)  );
                                    strncpy((char *)scd.sys_license, cur_argv, sizeof(scd.sys_license)-1);
                                }
                                value_ptr = (char *)scd.sys_license;
                                reg_size  = sizeof(scd.sys_license);
                                break;

                        case SCD_XMIN:
                        case SCD_XMAX:
                        case SCD_YMIN:
                        case SCD_YMAX:
                                value = simple_strtol(cur_argv, NULL, reg_defs[reg_num].base);
                                printf("Setting  %-20s to %d ", reg_defs[reg_num].reg_name, value);
                                value = byte_swap32(value);
                                value_ptr = (char *)&value;
                                break;

                        default:
                                value = simple_strtoul(cur_argv, NULL, reg_defs[reg_num].base);
                                printf(reg_defs[reg_num].base == 16 ? "Setting  %-20s to %x " : "Setting  %-20s to %u ", reg_defs[reg_num].reg_name, value);
                                value /= reg_defs[reg_num].divisor;
                                value = byte_swap32(value);
                                value_ptr = (char *)&value;
                }
                //printf("\nscd set - reg_num %d value %016llx reg_size %d\n", reg_num, *(u64 *)value_ptr, reg_size);
                if (scd_write(reg_num, (uchar *)value_ptr, reg_size) != 0)
                        puts(" ## Write failed ##\n");
                else if (scd_read(reg_num, (uchar *)verify, reg_size) != 0 || memcmp(value_ptr, verify, reg_size) != 0) {
                        //printf("scd verify - value_ptr %016llx verify %016llx\n", *(u64 *)value_ptr, *(u64 *)verify);
                        puts(" ## Verify failed ##\n");
                }
                else
                        puts("\n");
next_reg:
                offset = not_flag = 0;
next_arg:
                value = 0;
        }
        return 0;
}

int find_our_scd_image(uchar **addr, int *cnt)
{
    DECLARE_GLOBAL_DATA_PTR;
    char *ident_str = (gd->arch.board_desc.rev_major == BD_XR1000) ? SCD_IDENT_XR1K_STR :
                      (gd->arch.board_desc.rev_major == BD_XR6000) ? SCD_IDENT_63XX_STR :
                      (gd->arch.board_desc.rev_major == BD_XSP   ) ? SCD_IDENT_68XX_STR :
                                                                     SCD_IDENT_52XX_STR ;
    return find_scd_image(ident_str, addr, cnt);
}

int do_scd_update (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        uchar *addr = (uchar *)simple_strtoul (getenv("loadaddr"), NULL, 16);
        int    cnt  =          simple_strtoul (getenv("filesize"), NULL, 16);
        uchar  zero = 0;

        if (!avr_present()) {
                printf ("SCD update not supported on this device\n");
                return 0;
        }

        if (argc > 4) {
                return CMD_RET_USAGE;
        }
        if (argc > 3) cnt  =          simple_strtoul(argv[3], NULL, 16);
        if (argc > 2) addr = (uchar *)simple_strtoul(argv[2], NULL, 16);

        put_cmd_label("SCD", "Program");

        if (cnt < SCD_GEN3_MIN_CODE_SIZE || cnt > 2*SCD_GEN3_MAX_CODE_SIZE) {
                printf("### ERROR: Invalid SCD image size ###\n");
                return 1;
        }
        if (!find_our_scd_image(&addr, &cnt)) {
                printf("### ERROR: Invalid SCD image ###\n");
                return 1;
        }
#ifndef CONFIG_DEBUG_SCD_FIND_IMAGE
        printf("Copying %08x to SCD flash... ", (unsigned int)addr);
        if (scd_write(SCD_PRGM_FLASH, addr, cnt) != 0) {
                puts("## Failed ##\n");
                return 1;
        }
        scd_write(SCD_PRGM_FLASH_DONE, &zero, 1);  // data sent is a don't care
#endif
        puts("done\n");
        return 0;
}

int do_scd_upload (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        ulong addr = simple_strtoul (getenv("loadaddr"), NULL, 16);

        if (!avr_present()) {
                printf ("SCD upload not supported on this device\n");
                return 0;
        }

        if (argc > 3) {
                return CMD_RET_USAGE;
        }
        if (argc > 2) addr = simple_strtoul(argv[2], NULL, 16);

        printf("Copying SCD flash to %08x ... ", (unsigned int)addr);
        if (scd_read(SCD_PRGM_FLASH, (uchar *)addr, SCD_MAX_CODE_SIZE) != 0) {
                puts("## Failed ##\n");
                return 1;
        }
        puts("done\n");
        return 0;
}

/*
 * New command line interface: "scd" command with subcommands
 */
static cmd_tbl_t cmd_scd_sub[] = {
	U_BOOT_CMD_MKENT(print, 1, 0, do_scd_print, "", ""),
	U_BOOT_CMD_MKENT(reset, 1, 0, do_scd_reset, "", ""),
	U_BOOT_CMD_MKENT(clear, 1, 0, do_scd_clear, "", ""),
	U_BOOT_CMD_MKENT(save,  1, 0, do_scd_save, "", ""),
	U_BOOT_CMD_MKENT(set, CONFIG_SYS_MAXARGS, 0, do_scd_set, "", ""),
	U_BOOT_CMD_MKENT(update, 3, 0, do_scd_update, "", ""),
	U_BOOT_CMD_MKENT(upload, 2, 0, do_scd_upload, "", ""),
};

static int do_scd(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	cmd_tbl_t *cp;

	if (argc < 2)
		return CMD_RET_USAGE;

	/* drop initial "scd" arg */
	argc--;
	argv++;

	cp = find_cmd_tbl(argv[0], cmd_scd_sub, ARRAY_SIZE(cmd_scd_sub));

	if (cp)
		return cp->cmd(cmdtp, flag, argc, argv);

	return CMD_RET_USAGE;
}

#ifdef CONFIG_SYS_LONGHELP
static char scd_help_text[] =
        "clear - clear all system configuration device registers to defaults\n"
        "scd print - display the contents of the system configuration device\n"
        "scd reset - reset unprotected system configuration device registers to defaults\n"
        "scd save  - save system configuration device settings to persistent storage\n"
        "scd set register value ... - set system configuration 'register' to 'value ...'\n"
        "scd update address count - copy memory at 'address', length 'count' to scd flash\n"
        "scd upload address - copy scd flash to memory at 'address'\n";
#endif

U_BOOT_CMD(
	scd, CONFIG_SYS_MAXARGS, 0, do_scd,
	"scd handling commands", scd_help_text
);

/************************************************************************
 * Initialize the u-boot environment based on system configuration data *
 ************************************************************************/

#if defined(CONFIG_ENV_HW_INITIALIZE)

int month_to_index(char *name)
{
    int i;

    for (i = 1; i <= 12; ++i) {
            if (strncmp(name, mon_strs[i], 3) == 0)
                    return(i);
    }
    return(0);
}

int update_env(char *var, const char *val)
{
        char *s = getenv(var);

        if ((!s && !*val) || (s && strcmp(s, val) == 0)) {
                return(0);
        }
        setenv_force(var, val);
        return(1);
}

void initialize_environment(void)
{
        DECLARE_GLOBAL_DATA_PTR;
        sys_cfg_dev_t scd;
        radio_mfg_data_t radio_bd;
        ext_radio_mfg_data_t ext_radio_bd;
        char var[64], val[64], *s;
        unsigned long long eth_mac_value = 0;
        uchar *eth_mac_bytes = (uchar *)&eth_mac_value;
        int env_changed = 0;
        int scd_changed = 0;
	int ext_radio_bd_valid;
        int i;
        int reg_data;
        int vendor;
        uint64_t pcie_addr_base;
        qca_radio_data_t qca_rd;

        scd_load();

        memset(&scd, 0xff, sizeof(scd));

        if (!avr_present()) {
                scd_read(0, (unsigned char *)&scd, sizeof(scd));
                if ((check_sum((uchar *)&scd.stat_lo, SCD_STD_REG_CNT) != 0) || (check_sum((uchar *)&scd.ext_reg_start, SCD_EXT_REG_CNT) != 0)) {
                        puts("Invalid SCD checksum - using default SCD ... ");
                        scd_write(0, (unsigned char *)(&default_scd), sizeof(default_scd));
                        do_scd_save(NULL, 1, 0, NULL);
                }
        }

        if (scd_read(0, (unsigned char *)&scd, SCD_STD_REG_CNT) == 0)
        {
                if (scd_read(SCD_EXT_REG_SET, (unsigned char *)&scd.ext_reg_start, SCD_EXT_REG_CNT) == 0)
                {
                        if (scd.sys_serial[0] != 0xff && scd.sys_serial[0] != 0x00) {
                                sprintf(val, "%-.15s", scd.sys_serial);
                                env_changed += update_env("system_ser#", val);
                        }
                        else if ((s = getenv("system_ser#")) != NULL) {
                                memset( scd.sys_serial, 0, sizeof(scd.sys_serial)  );
                                strncpy((char *)scd.sys_serial, s, sizeof(scd.sys_serial)-1);
                                scd_changed++;
                        }

                        if (scd.sys_license[0] != 0xff && scd.sys_license[0] != 0x00) {
                                sprintf(val, "%-.31s", scd.sys_license);
                                env_changed += update_env("license_key", val);
                        }
                        else if ((s = getenv("license_key")) != NULL) {
                                memset( scd.sys_license, 0, sizeof(scd.sys_license)  );
                                strncpy((char *)scd.sys_license, s, sizeof(scd.sys_license)-1);
                                scd_changed++;
                        }

                     /* code prior to this version will have zeros before scd is cleared; the zero check will avoid zeros getting programmed into environment variables */
                        if (!((scd.cmps.x_data_min == 0xffff) && (scd.cmps.x_data_max == 0xffff) && (scd.cmps.y_data_min == 0xffff) && (scd.cmps.y_data_max == 0xffff)) &&
                            !((scd.cmps.x_data_min == 0x0000) && (scd.cmps.x_data_max == 0x0000) && (scd.cmps.y_data_min == 0x0000) && (scd.cmps.y_data_max == 0x0000))) {
                                sprintf(val, "%d", (short)byte_swap16(scd.cmps.x_data_min));
                                env_changed += update_env("compass_xmin", val);
                                sprintf(val, "%d", (short)byte_swap16(scd.cmps.x_data_max));
                                env_changed += update_env("compass_xmax", val);
                                sprintf(val, "%d", (short)byte_swap16(scd.cmps.y_data_min));
                                env_changed += update_env("compass_ymin", val);
                                sprintf(val, "%d", (short)byte_swap16(scd.cmps.y_data_max));
                                env_changed += update_env("compass_ymax", val);
                        }
                        else if ((scd.cmps.x_data_min == 0xffff) && (scd.cmps.x_data_max == 0xffff) && (scd.cmps.y_data_min == 0xffff) && (scd.cmps.y_data_max == 0xffff)) {
                                if ((s = getenv("compass_xmin")) != NULL) {
                                        scd.cmps.x_data_min = byte_swap16((short)simple_strtol(s, &s, 10));
                                        scd_changed++;
                                }
                                if ((s = getenv("compass_xmax")) != NULL) {
                                        scd.cmps.x_data_max = byte_swap16((short)simple_strtol(s, &s, 10));
                                        scd_changed++;
                                }
                                if ((s = getenv("compass_ymin")) != NULL) {
                                        scd.cmps.y_data_min = byte_swap16((short)simple_strtol(s, &s, 10));
                                        scd_changed++;
                                }
                                if ((s = getenv("compass_ymax")) != NULL) {
                                        scd.cmps.y_data_max = byte_swap16((short)simple_strtol(s, &s, 10));
                                        scd_changed++;
                                }
                        }

                        if (scd.system.type != 0xffff && scd.system.type != 0x0000 && scd.system.version != 0xff && scd.system.version != 0x00) {
                                sprintf(val, "180-%04u-%03u", byte_swap16(scd.system.type), scd.system.version);
                                if (scd.system.revision != 0xff && scd.system.revision != 0x00) {
                                        sprintf(val + strlen(val), ".%c", SCD_BOARD_MAJ_REV(scd.system.revision));
                                        if (SCD_BOARD_MIN_REV(scd.system.revision))
                                                sprintf(val + strlen(val), "%d", SCD_BOARD_MIN_REV(scd.system.revision));
                                }
                                env_changed += update_env("system_prt#", val);
                        }
                        else if ((s = getenv("system_prt#")) != NULL) {
                                scd.system.type     = simple_strtoul(s+4, &s, 10);
                                scd.system.version  = simple_strtoul(s+1, &s, 10);
                                scd.system.revision = *s ? SCD_BOARD_SET_REV(s+1) : 0xff;
                                scd.system.type     = byte_swap16(scd.system.type);
                                scd_changed++;
                        }

                        if (scd.assy.location != 0xff && scd.assy.location != 0x00) {
                                sprintf(val, "%d", scd.assy.location);
                                env_changed += update_env("system_mfg#", val);
                        }
                        else if ((s = getenv("system_mfg#")) != NULL) {
                                scd.assy.location = simple_strtoul(s, &s, 10);
                                scd_changed++;
                        }

                        if (scd.assy.date.year != 0xff && scd.assy.date.year != 0x00) {
                                sprintf(val, "%2d%02d-%s-%02d %2d:%02d",
                                        scd.assy.date.cent, scd.assy.date.year, mon_strs[scd.assy.date.mon & 0xf], scd.assy.date.mday,
                                        scd.assy.time.hour, scd.assy.time.min);
                                env_changed += update_env("system_date", val);
                        }
                        else if ((s = getenv("system_date")) != NULL) {
                                int year           = simple_strtoul(s, &s, 10);
                                scd.assy.date.cent = year / 100;
                                scd.assy.date.year = year % 100;
                                scd.assy.date.mon  = month_to_index(s+1);
                                scd.assy.date.mday = simple_strtoul(s+5, &s, 10); while (*s && *s == ' ') ++s;
                                scd.assy.time.hour = simple_strtoul(s  , &s, 10);
                                scd.assy.time.min  = simple_strtoul(s+1, &s, 10);
                                scd.assy.time.sec  = 0;
                                scd_changed++;
                        }

                        if (scd.mpc_nostart != 0) {
                                sprintf(val, "%d", scd.mpc_nostart);
                                env_changed += update_env("mpc_nostart", val);
                        }
                        else if ((s = getenv("mpc_nostart")) != NULL) {
                                env_changed += update_env("mpc_nostart", "");
                        }

                        if (scd_changed)
                                scd_write(SCD_EXT_REG_SET, (unsigned char *)&scd.ext_reg_start, SCD_EXT_REG_CNT);
                }

                if (!avr_present()) {
                        if (scd.maj_rev != default_scd.maj_rev) {
                                scd.maj_rev = default_scd.maj_rev;
                                scd_changed++;
                        }
                        if (scd.min_rev != default_scd.min_rev) {
                                scd.min_rev = default_scd.min_rev;
                                scd_changed++;
                        }
                        if (scd.sw_date != default_scd.sw_date) {
                                scd.sw_date = default_scd.sw_date;
                                scd_changed++;
                        }
                }

                if (scd.ser_num != 0xffffffff && scd.ser_num != 0x00000000) {
                        sprintf(val, "%010u", byte_swap32(scd.ser_num));
                        env_changed += update_env("cpucard_ser#", val);
                }
                else if ((s = getenv("cpucard_ser#")) != NULL) {
                        scd.ser_num = simple_strtoul(s, &s, 10);
                        scd.ser_num = byte_swap32(scd.ser_num);
                        scd_changed++;
                }

                if (scd.board.type != 0xffff && scd.board.type != 0x0000 && scd.board.version != 0xff && scd.board.version != 0x00) {
                        sprintf(val, "100-%04u-%03u", byte_swap16(scd.board.type), scd.board.version);
                        if (scd.board.revision != 0xff && scd.board.revision != 0x00) {
                                sprintf(val + strlen(val), ".%c", SCD_BOARD_MAJ_REV(scd.board.revision));
                                if (SCD_BOARD_MIN_REV(scd.board.revision))
                                        sprintf(val + strlen(val), "%d", SCD_BOARD_MIN_REV(scd.board.revision));
                        }
                        env_changed += update_env("cpucard_prt#", val);
                }
                else if ((s = getenv("cpucard_prt#")) != NULL) {
                        scd.board.type     = simple_strtoul(s+4, &s, 10);
                        scd.board.version  = simple_strtoul(s+1, &s, 10);
                        scd.board.revision = *s ? SCD_BOARD_SET_REV(s+1) : 0xff;
                        scd.board.type     = byte_swap16(scd.board.type);
                        scd_changed++;
                }

                if (scd.mfg.location != 0xff && scd.mfg.location != 0x00) {
                        sprintf(val, "%d", scd.mfg.location);
                        env_changed += update_env("cpucard_mfg#", val);
                }
                else if ((s = getenv("cpucard_mfg#")) != NULL) {
                        scd.mfg.location = simple_strtoul(s, &s, 10);
                        scd_changed++;
                }

                if (scd.mfg.date.year != 0xff && scd.mfg.date.year != 0x00) {
                        sprintf(val, "%2d%02d-%s-%02d %2d:%02d",
                                scd.mfg.date.cent, scd.mfg.date.year, mon_strs[scd.mfg.date.mon & 0xf], scd.mfg.date.mday,
                                scd.mfg.time.hour, scd.mfg.time.min);
                        env_changed += update_env("cpucard_date", val);
                }
                else if ((s = getenv("cpucard_date")) != NULL) {
                        int year          = simple_strtoul(s, &s, 10);
                        scd.mfg.date.cent = year / 100;
                        scd.mfg.date.year = year % 100;
                        scd.mfg.date.mon  = month_to_index(s+1);
                        scd.mfg.date.mday = simple_strtoul(s+5, &s, 10); while (*s && *s == ' ') ++s;
                        scd.mfg.time.hour = simple_strtoul(s  , &s, 10);
                        scd.mfg.time.min  = simple_strtoul(s+1, &s, 10);
                        scd.mfg.time.sec  = 0;
                        scd_changed++;
                }

                if (scd.eth_mac_count != 0xff && scd.eth_mac_count != 0x00 && valid_mac_addr(scd.eth_mac_base)) {
                        memcpy(eth_mac_bytes + 2, scd.eth_mac_base, 6);
                     // Set gd->arch.mac_desc.mac_addr_base and count here for Octeon
                        gd->arch.mac_desc.count = scd.eth_mac_count;
                        for (i = 0; i < 6; i++)
                                gd->arch.mac_desc.mac_addr_base[i] = eth_mac_bytes[i+2];
                        for (i = 0; i < scd.eth_mac_count; i++) {
                                sprintf(var, "eth%daddr", i);
                                sprintf(val, "%02x:%02x:%02x:%02x:%02x:%02x",
                                        eth_mac_bytes[2], eth_mac_bytes[3], eth_mac_bytes[4],
                                        eth_mac_bytes[5], eth_mac_bytes[6], eth_mac_bytes[7]);
                                env_changed += update_env(var, val);
                                ++eth_mac_value;
                        }
                        for ( ; i < CONFIG_TSEC_COUNT; i++) {
                                sprintf(var, "eth%daddr", i);
                                env_changed += update_env(var, "");
                        }
                }
                else if ((s = getenv("eth0addr")) != NULL && strcmp(s, MK_STR(CONFIG_ETH0ADDR)) != 0) {
                        for (i = 0; i < 6; i++, s++)
                                scd.eth_mac_base[i] = simple_strtoul(s, &s, 16);

                        scd.eth_mac_count = 1 + (getenv("eth1addr") ? 1 : 0)
                                              + (getenv("eth2addr") ? 1 : 0)
                                              + (getenv("eth3addr") ? 1 : 0)
                                              + (getenv("eth4addr") ? 1 : 0)
                                              + (getenv("eth5addr") ? 1 : 0);

                        memcpy(eth_mac_bytes + 2, scd.eth_mac_base, 6);
                     // Set gd->arch.mac_desc.mac_addr_base and count here for Octeon
                        gd->arch.mac_desc.count = scd.eth_mac_count;
                        for (i = 0; i < 6; i++)
                                gd->arch.mac_desc.mac_addr_base[i] = eth_mac_bytes[i+2];

                        scd_changed++;
                }

                if (scd.rad_mac_count != 0xff && scd.rad_mac_count != 0x00 && valid_mac_addr(scd.rad_mac_base)) {
                        sprintf(val, "%d", scd.rad_mac_count);
                        env_changed += update_env("rfaddrcount", val);
                        sprintf(val, "%02x:%02x:%02x:%02x:%02x:%02x",
                                scd.rad_mac_base[0],scd.rad_mac_base[1],scd.rad_mac_base[2],
                                scd.rad_mac_base[3],scd.rad_mac_base[4],scd.rad_mac_base[5]);
                        env_changed += update_env("rfaddrbase", val);
                }
                else if ((s = getenv("rfaddrbase")) != NULL) {
                        for (i = 0; i < 6; i++, s++)                scd.rad_mac_base[i] = simple_strtoul(s, &s, 16);
                        if ((s = getenv("rfaddrcount")) != NULL)    scd.rad_mac_count   = simple_strtoul(s, &s, 10);
                        scd_changed++;
                }

                if (scd.country_code_0 != 0xff && scd.country_code_0 != 0x00 && scd.country_code_1 != 0xff && scd.country_code_1 != 0x00) {
                        sprintf(val, "%c%c", scd.country_code_0, scd.country_code_1);
                        env_changed += update_env("country_code", val);
                }
                else if ((s = getenv("country_code")) != NULL) {
                        scd.country_code_0 = *s++;
                        scd.country_code_1 = *s++;
                        scd_changed++;
                }
        }

        for (i = 0; i < 16; i++) {  /* radios with BAR entry of zero will be skipped - i.e., radio was not enumerated */
                if (radio_pcie_bar0_array[i] && (vendor = radio_devid_check(i,0)) &&
                    (radio_eeprom_read (i, (vendor == QCA) ? AR_EEPROM_MFG_BYTE_OFFSET : BCM_EEPROM_MFG_BYTE_OFFSET, (char *)&radio_bd, sizeof(radio_bd)) == 0) &&
                     check_sum((uchar *)&radio_bd, sizeof(radio_bd)) == 0) {
                        memset((void *)&ext_radio_bd, -1, sizeof(ext_radio_bd));
                        if ((radio_eeprom_read (i, (vendor == QCA) ? AR_EEPROM_EXT_MFG_BYTE_OFFSET : BCM_EEPROM_EXT_MFG_BYTE_OFFSET, (char *)&ext_radio_bd, sizeof(ext_radio_bd)) == 0) &&
                            (check_sum((uchar *)&ext_radio_bd, sizeof(ext_radio_bd)) == 0))
                                ext_radio_bd_valid = 1;
                        else
                                ext_radio_bd_valid = 0;
                        if (radio_bd.ser_num != 0xffffffff) {
                                sprintf(var, "rfcard%02d_ser#", i);
                                sprintf(val, "%010u", byte_swap32(radio_bd.ser_num));
                                env_changed += update_env(var, val);
                        }
                        if (radio_bd.hw.type != 0xffff && radio_bd.hw.version != 0xff && radio_bd.hw.revision != 0xff) {
                                sprintf(var, "rfcard%02d_prt#", i);
				if (ext_radio_bd_valid && ext_radio_bd.design != 0xffff)
                                        sprintf(val, "%03u-%04u-%03u.%c", byte_swap16(ext_radio_bd.design), byte_swap16(radio_bd.hw.type),
                                                radio_bd.hw.version, RAD_BOARD_MAJ_REV(radio_bd.hw.revision));
				else
                                        sprintf(val, "100-%04u-%03u.%c", byte_swap16(radio_bd.hw.type),
                                                radio_bd.hw.version, RAD_BOARD_MAJ_REV(radio_bd.hw.revision));
                                if (RAD_BOARD_MIN_REV(radio_bd.hw.revision))
                                        sprintf(val + strlen(val), "%d", RAD_BOARD_MIN_REV(radio_bd.hw.revision));
                                env_changed += update_env(var, val);
                        }
                        if (radio_bd.mfg.location != 0xff) {
                                sprintf(var, "rfcard%02d_mfg#", i);
                                sprintf(val, "%d", radio_bd.mfg.location);
                                env_changed += update_env(var, val);
                        }
                        if (radio_bd.mfg.date.year != 0xff) {
                                sprintf(var, "rfcard%02d_date", i);
                                sprintf(val, "%2d%02d-%s-%02d %2d:%02d",
                                        radio_bd.mfg.date.cent, radio_bd.mfg.date.year, mon_strs[radio_bd.mfg.date.mon & 0xf], radio_bd.mfg.date.mday,
                                        radio_bd.mfg.time.hour, radio_bd.mfg.time.min);
                                env_changed += update_env(var, val);
                        }
                }
                else if (radio_pcie_bar0_array[i] && (radio_devid_check(i,0) == 1) &&
                         ((gd->arch.board_desc.rev_major == BD_XD4_130) ||
                          (gd->arch.board_desc.rev_major == BD_XR600  ) || (gd->arch.board_desc.rev_major == BD_XR700  ))) {
                        if ((radio_eeprom_read (i, QCA_RADIO_DATA_BYTE_OFFSET, (char *)&qca_rd, sizeof(qca_rd)) == 0) && (qca_data_process((u8 *)&qca_rd) == 0)) {
                                sprintf(var, "rfcard%02d_prt#", i);
                                if (qca_rd.rev_letter == 0)
                                        sprintf(val, "100-01%u%u-%03u.%u",
                                                qca_rd.part_num[0], qca_rd.part_num[1], qca_rd.version_num, qca_rd.rev_num);
                                else
                                        sprintf(val, "100-01%u%u-%03u.%X%u",
                                                qca_rd.part_num[0], qca_rd.part_num[1], qca_rd.version_num, qca_rd.rev_letter, qca_rd.rev_num);
                                env_changed += update_env(var, val);
                                sprintf(var, "rfcard%02d_ser#", i);
                                sprintf(val, "000%u%u%u%u%u%u%u",
                                        qca_rd.ser_num[0], qca_rd.ser_num[1], qca_rd.ser_num[2], qca_rd.ser_num[3], qca_rd.ser_num[4], qca_rd.ser_num[5], qca_rd.ser_num[6]);
                                env_changed += update_env(var, val);
                                sprintf(var, "rfcard%02d_mfg#", i);
                                env_changed += update_env(var, "");
                                sprintf(var, "rfcard%02d_date", i);
                                sprintf(val, "20%u%u-%s-%u%u %u%u:00",
                                        qca_rd.year[0], qca_rd.year[1], mon_strs[qca_rd.month[1] + (qca_rd.month[0] ? 10 : 0)], qca_rd.day[0], qca_rd.day[1], qca_rd.hour[0], qca_rd.hour[1]);
                                env_changed += update_env(var, val);
                        }
                        else {  //OTP programmed instead of EEPROM or QCA customer data in EEPROM is invalid
                                sprintf(var, "rfcard%02d_ser#", i);
                                env_changed += update_env(var, "");
                                sprintf(var, "rfcard%02d_prt#", i);
                                pcie_addr_base = (cvmx_pcie_get_mem_base_address(i) | (1ull << 63)) + radio_pcie_bar0_array[i];
                                reg_data = cvmx_read64_uint32(pcie_addr_base + AR9890_NUM_CHAINS_DETECT);
                                if (byte_swap32(reg_data) & 1<<13) //bit 13 ==> 1="2 chains", 0="3 chains"
                                        sprintf(val, "100-0166-002.2");  //XR620
                                else
                                        sprintf(val, "100-0166-001.2");  //XR630
                                env_changed += update_env(var, val);
                                sprintf(var, "rfcard%02d_mfg#", i);
                                env_changed += update_env(var, "");
                                sprintf(var, "rfcard%02d_date", i);
                                env_changed += update_env(var, "");
                        }
                }
                else {
                        sprintf(var, "rfcard%02d_ser#", i);
                        env_changed += update_env(var, "");
                        sprintf(var, "rfcard%02d_prt#", i);
                        env_changed += update_env(var, "");
                        sprintf(var, "rfcard%02d_mfg#", i);
                        env_changed += update_env(var, "");
                        sprintf(var, "rfcard%02d_date", i);
                        env_changed += update_env(var, "");
                }
        }

        if (avalon_pcie_bar0) {
                sprintf(val, "3000-%02d.%03d", (get_avalon(AVALON_REV_ADDR) >> 8) & 0x0f, get_avalon(AVALON_REV_ADDR) & 0xff);
                env_changed += update_env("bootrev_avalon", val);
        }
        else {
                env_changed += update_env("bootrev_avalon", "");
        }

        env_changed += update_env("subindent"   , "8");

        if (gd->avaya) {
                env_changed += update_env("version", env_alt_ver_string);
        }
        else {
                env_changed += update_env("version", env_version_string);
        }

        if ((gd->arch.board_desc.rev_major == BD_XA4_240) ||
            (gd->arch.board_desc.rev_major == BD_XD4_240)) {
                int devid_1, devid_2, devid;

                devid_1 = cvmx_mdio_45_read(0, 0, 0x1d, AQ_REG_GBE_STANDARD_DEVICE_ID_1);
                devid_2 = cvmx_mdio_45_read(0, 0, 0x1d, AQ_REG_GBE_STANDARD_DEVICE_ID_2);
                devid = devid_1<<16 | (devid_2 & 0xffff);

                if (devid == PHY_ID_AQR105) {
                        sprintf(val, "%s (0x%08x)", PHY_ID_AQR105_NAME, devid);
                }
                if (devid == PHY_ID_AQR109revAe ||
                    devid == PHY_ID_AQR109revA ) {
                        sprintf(val, "%s (0x%08x)", PHY_ID_AQR109revA_NAME, devid);
                }
                if (devid == PHY_ID_AQR109revB) {
                        sprintf(val, "%s (0x%08x)", PHY_ID_AQR109revB_NAME, devid);
                }

                env_changed += update_env("aquantia_rev", val);
        }

        if (env_changed) {
                printf("Saving updates");
                saveenv();
                puts(", ");
        }

        if (scd_changed) {
                scd_write(0             , (unsigned char *)&default_scd         , SCD_NON_WP_CNT                );
                scd_write(SCD_PROC_SPEED, (unsigned char *)&scd.proc_speed      , sizeof(scd.proc_speed)        );
                scd_write(SCD_NON_WP_CNT, (unsigned char *)&scd + SCD_NON_WP_CNT, SCD_STD_REG_CNT-SCD_NON_WP_CNT);
                do_scd_save(NULL, 0, 0, NULL);
                puts(", ");
        }
}
#endif // CONFIG_ENV_HW_INITIALIZE
