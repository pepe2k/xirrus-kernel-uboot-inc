/*
 * Copyright (c) 2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * Inspired by cmd_ext_common.c, cmd_fat.c.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <common.h>
#include <command.h>
#include <fs.h>

#ifdef CONFIG_OCTEON_XARRAY

static char dev_string[] = "0";

static char *mem_type (void) {
        DECLARE_GLOBAL_DATA_PTR;

	if ((gd->arch.board_desc.rev_major == BD_XD2_230) ||
	    (gd->arch.board_desc.rev_major == BD_XD2_240) ||
	    (gd->arch.board_desc.rev_major == BD_XD3_230) ||
	    (gd->arch.board_desc.rev_major == BD_XD4_130) ||
	    (gd->arch.board_desc.rev_major == BD_XD4_240) ||
	    (gd->arch.board_desc.rev_major == BD_XA4_240) ||
	    (gd->arch.board_desc.rev_major == BD_XR600  ) ||
	    (gd->arch.board_desc.rev_major == BD_XR700  ))
	        return ("mmc");
        else
	        return ("usb");
}

int do_load_wrapper(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        char *new_argv[5];
        int new_argc = 5;

        new_argv[0] = argv[0];
        new_argv[1] = mem_type();
        new_argv[2] = dev_string;
        new_argv[3] = ((argv[1] != NULL) && ishexnum(argv[1])) ? argv[1] : "0";  // 0 forces loadaddr to be used
        new_argv[4] = ((argv[1] != NULL) && ishexnum(argv[1])) ? argv[2] : argv[1];

	debug("%s %s %s %s %s\n", new_argv[0], new_argv[1], new_argv[2], new_argv[3], new_argv[4]);

	return do_load(cmdtp, flag, new_argc, new_argv, FS_TYPE_ANY, 16);
}

U_BOOT_CMD(
	load,	3,	0,	do_load_wrapper,
	"load binary file from a filesystem",
	"[<addr> [<filename>]]\n"
	"    - Load binary file 'filename' to address 'addr' in memory.\n"
	"      The address must be hexadecimal."
);

int do_ls_wrapper(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
        char *new_argv[4];

        new_argv[0] = argv[0];
        new_argv[1] = mem_type();
        new_argv[2] = dev_string;
        new_argv[3] = argv[1];

	debug("%s %s %s %s\n", new_argv[0], new_argv[1], new_argv[2], new_argv[3]);

	return do_ls(cmdtp, flag, argc+2, new_argv, FS_TYPE_ANY);
}

U_BOOT_CMD(
	ls,	2,	0,	do_ls_wrapper,
	"list files in a directory (default /)",
	"[directory]\n"
	"    - List files in directory 'directory'."
);

#else //!CONFIG_OCTEON_XARRAY

int do_load_wrapper(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	return do_load(cmdtp, flag, argc, argv, FS_TYPE_ANY, 0);
}

U_BOOT_CMD(
	load,	7,	0,	do_load_wrapper,
	"load binary file from a filesystem",
	"<interface> [<dev[:part]> [<addr> [<filename> [bytes [pos]]]]]\n"
	"    - Load binary file 'filename' from partition 'part' on device\n"
	"       type 'interface' instance 'dev' to address 'addr' in memory.\n"
	"      'bytes' gives the size to load in bytes.\n"
	"      If 'bytes' is 0 or omitted, the file is read until the end.\n"
	"      'pos' gives the file byte position to start reading from.\n"
	"      If 'pos' is 0 or omitted, the file is read from the start.\n"
	"      All numeric parameters are assumed to be decimal,\n"
	"      unless specified otherwise using a leading \"0x\"."
);

int do_ls_wrapper(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	return do_ls(cmdtp, flag, argc, argv, FS_TYPE_ANY);
}

U_BOOT_CMD(
	ls,	4,	1,	do_ls_wrapper,
	"list files in a directory (default /)",
	"<interface> [<dev[:part]> [directory]]\n"
	"    - List files in directory 'directory' of partition 'part' on\n"
	"      device type 'interface' instance 'dev'."
);

#endif //CONFIG_OCTEON_XARRAY
