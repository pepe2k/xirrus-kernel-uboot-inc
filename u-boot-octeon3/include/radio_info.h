/********************************************************************************
 * General / Global Radio Definitions                                           *
 ********************************************************************************/
#ifndef RADIO_INFO_H
#define RADIO_INFO_H

#include <bitfield.h>

#undef USE_BCM_EEPROM_STRUCT

#define MAX_NUM_RADIOS                      16 // max number of radios in any array

#define radio_mask(n)           (1 << (n))
#define get_radio_mac_cnt()     scd_reg_read(SCD_RAD_NUM)

#define QCA  1
#define BCM  2

#define AR_VENDOR_ID                    0x168c
#define BCM_VENDOR_ID                   0x14e4

#define AR_EEPROM_BASE                  0x2000
#define AR_EEPROM_MAX_BYTE_OFFSET       0x1ffc
#define AR_EEPROM_MFG_BYTE_OFFSET       0x0ff0
#define AR_EEPROM_EXT_MFG_BYTE_OFFSET   0x0fea

#define BCM_EEPROM_MAX_BYTE_OFFSET      0x1ffc
#ifdef USE_BCM_EEPROM_STRUCT
#define BCM_EEPROM_MFG_BYTE_OFFSET      0x0040
#define BCM_EEPROM_EXT_MFG_BYTE_OFFSET  0x003a
#else
#define BCM_EEPROM_MFG_BYTE_OFFSET      0x0ff0
#define BCM_EEPROM_EXT_MFG_BYTE_OFFSET  0x0fea
#endif

#define AR9220_DEVICE_ID_DEFAULT        0xff1d
#define AR9220_DEVICE_ID                0x0029
#define AR9280_DEVICE_ID_DEFAULT        0xff1c
#define AR9280_DEVICE_ID                0x002a

#define AR92XX_GPIO_IN_OUT              0x4048
#define AR92XX_GPIO_OE_OUT              0x404c
#define AR92XX_GPIO_INPUT_EN_VAL        0x4054
#define AR92XX_GPIO_OUTPUT_MUX1         0x4060
#define AR92XX_GPIO_OUTPUT_MUX2         0x4064
#define AR92XX_EEPROM_STATUS_DATA       0x407c
#define AR92XX_EEPROM_STATUS_DATA_BUSY  0x00010000

#define AR9390_DEVICE_ID_DEFAULT        0xabcd
#define AR9390_DEVICE_ID                0x0030
#define AR9390_GPIO_OUTPUT              0x4048
#define AR9390_GPIO_OE_OUT              0x4050
#define AR9390_GPIO_OUTPUT_MUX2         0x406c
#define AR9390_EEPROM_STATUS_DATA       0x4084
#define AR9390_EEPROM_STATUS_DATA_BUSY  0x00010000

#define AR9890_DEVICE_ID_DEFAULT        0x003c
#define AR9890_SOC_LF_TIMER_CONTROL0    0x04050
#define AR9890_NUM_CHAINS_DETECT        0x040ec //bit 13 ==> 1="2 chains", 0="3 chains"
#define AR9890_SI_CONFIG                0x10000
#define AR9890_SI_CS                    0x10004
#define AR9890_TX_DATA0                 0x10008
#define AR9890_RX_DATA0                 0x10010
#define AR9890_WLAN_GPIO_OUT_W1TS       0x14004
#define AR9890_WLAN_GPIO_OUT_W1TC       0x14008
#define AR9890_WLAN_GPIO_ENABLE         0x1400c
#define AR9890_WLAN_GPIO_PIN2           0x14030
#define AR9890_WLAN_GPIO_PIN4           0x14038
#define AR9890_SI_CS_DONE_INT           0x00020000
#define AR9890_SI_CS_DONE_ERR           0x00040000

#define BCM43525_DEVICE_ID_DEFAULT      0x4365
#define BCM43460_DEVICE_ID_DEFAULT      0x4360
#define BCM43525_DEVICE_ID              0x43ca //xd3-230
#define BCM43460_DEVICE_ID              0x43a0 //xd3-230
#define BCM43465sb_DEVICE_ID            0x43c5 //xa4-240
#define BCM43465db_DEVICE_ID            0x43c3 //xd4-240, xa4-240

#define BCM43465_EEPROM_CTRL            0x3190
#define BCM43465_EEPROM_ADDR            0x3194
#define BCM43465_EEPROM_DATA            0x3198
#define BCM43465_EEPROM_START           0x80000000
#define BCM43465_EEPROM_BUSY            0x80000000
#define BCM43465_EEPROM_READ            0x00000000
#define BCM43465_EEPROM_WRITE           0x20000000
#define BCM43465_EEPROM_WREN            0x60000000
#define BCM43465_EEPROM_WRDIS           0x40000000

#define ascii_to_hex(c)     (((c) >= 'a') ? (c) - 'a' + 10 :    \
                             ((c) >= 'A') ? (c) - 'A' + 10 :    \
                                            (c) - '0'      )

#define LEDS_NOT_SPINNING       0
#define LEDS_START_SPINNING     1
#define LEDS_STOP_SPINNING_OFF  2
#define LEDS_STOP_SPINNING_ON   3

/********************************************************************************
 * Radio Manufacturing EEPROM structure                                         *
 ********************************************************************************/
typedef struct {
        u8 cent;                /* manufacturing date, century                  */
        u8 year;                /* manufacturing date, year                     */
        u8 mon;                 /* manufacturing date, month                    */
        u8 mday;                /* manufacturing date, month day                */
} mfg_date_t;

typedef struct {
        u8 hour;                /* manufacturing date, hours                    */
        u8 min;                 /* manufacturing date, minutes                  */
} mfg_time_t;

typedef struct {
        mfg_date_t date;        /* manufacturing date                           */
        mfg_time_t time;        /* manufacturing time                           */
        u8 location;            /* manufacturing location                       */
} mfg_info_t;

typedef struct {
        u16 type;               /* board type number                            */
        u8 version;             /* board version number                         */
        u8 revision;            /* board revision number                        */
} mfg_board_id_t;

typedef union
{
    struct
    {
        u8 rsvd        : 6;     /* reserved                                     */
        u8 test_result : 1;     /* radio has passed validation tests            */
        u8 calibrated  : 1;     /* radio has been calibrated                    */
    } s;
} mfg_flags_t;

typedef struct {
        u16 design;             /* prefix for board id indicating design house  */
        u8 rack;                /* rack number on which board was calibrated    */
        mfg_flags_t flags;      /* flags set after mfg cal/test                 */
        u8 version;             /* manufacturing data structure version         */
        u8 chksum;              /* checksum (8 bit, two's complement)           */
} ext_radio_mfg_data_t;

typedef struct {
        u32 ser_num;            /* serial number                                */
        mfg_board_id_t hw;      /* hardware information                         */
        mfg_info_t mfg;         /* manufacturing time, date & location          */
        u8 chksum;              /* checksum (8 bit, two's complement)           */
} radio_mfg_data_t;

typedef struct {
        radio_mfg_data_t mfg;   /* radio board manufacturing data               */
        u8 reserved[0x3e0];     /* reserved                                     */
        u8 pda[4][0x300];       /* radio production data areas                  */
} radio_eeprom_t;


/********************************************************************************
 * Radio Manufacturing EEPROM data offsets                                      *
 ********************************************************************************/
#define RAD_MFG_DATA      0     /* start of manufacturing data                  */
#define RAD_SER_NUM       0     /* serial number                                */

#define RAD_BOARD_ID      4     /* hardware revision                            */
#define RAD_BOARD_TYPE    4     /* hardware revision                            */
#define RAD_BOARD_VER     6     /* hardware revision                            */
#define RAD_BOARD_REV     7     /* hardware revision                            */

#define RAD_MAN_DATE      8     /* manufacturing date                           */
#define RAD_MAN_CENT      8     /* manufacturing date, century                  */
#define RAD_MAN_YEAR      9     /* manufacturing date, year                     */
#define RAD_MAN_MON      10     /* manufacturing date, month                    */
#define RAD_MAN_MDAY     11     /* manufacturing date, month day                */

#define RAD_MAN_TIME     12     /* manufcaturing time                           */
#define RAD_MAN_HOUR     12     /* manufacturing time, hours                    */
#define RAD_MAN_MIN      13     /* manufacturing time, minutes                  */
#define RAD_MAN_SEC      14     /* manufacturing time, seconds                  */

#define RAD_CHKSUM       15     /* register file checksum                       */

#define RAD_REG_CNT      16     /* end of register file                         */

/* ---------------- Pseudo Register Definitions ------------------------------- */
#define RAD_RADIO_SEL    16     /* radio select register number                 */
#define RAD_SAVE_CMD     17     /* save command register number                 */
#define RAD_CLR_CMD      18     /* clear command register number                */
#define RAD_CMD_CNT      21     /* end of register/commands                     */

#define RAD_EXT_VER_NUM   0     /* extended register file version number        */
#define RAD_EXT_CHKSUM    4     /* extended register file checksum              */
#define RAD_EXT_SAVE_CMD 19     /* extended save command register number        */
#define RAD_EXT_CLR_CMD  20     /* extended clear command register number       */

#ifdef  CFG_NON_MIXED_REV_FIELD

#define RAD_BOARD_MAJ_REV(n)    ((((n) & 0xf0) >> 4) + 'A')
#define RAD_BOARD_MIN_REV(n)      ((n) & 0x0f)
#define RAD_BOARD_SET_REV(s)    ((((((s)[0] | 0x20) - 'a') & 0xf) << 4) | \
                                (simple_strtoul((s)+1, NULL, 10) & 0xf))
#else

#define RAD_BOARD_MAJ_REV(n)    ((((n) & 0x70) >> 4) + (((n) & 0x80) ? 'A' : '0'))
#define RAD_BOARD_MIN_REV(n)    (  (n) & 0x0f)

#define RAD_BOARD_SET_REV(s)    ((((((s)[0] > '9') ? ((((s)[0] | 0x20) - 'a') | 0x8) : \
                                    ((s)[0] - '0')) & 0xf) << 4) |                     \
                                    (simple_strtoul((s)+1, NULL, 10) & 0xf))
#endif

/*******************************
 * QCA Customer Data Structure *
 *******************************/

typedef struct {
  u8 year[2];
  u8 month[2];
  u8 day[2];
  u8 hour[2];
  u8 part_num[2];
  u8 version_num;
  u8 rev_letter;
  u8 rev_num;
  u8 ser_num[7];
} qca_radio_data_t;

#define QCA_RADIO_DATA_BYTE_OFFSET 45

#endif //RADIO_INFO_H
