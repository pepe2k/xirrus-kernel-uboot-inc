/*
 * (C) Copyright 2003
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * Copyright (C) 2004 - 2013 Cavium, Inc. <support@cavium.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * This file contains the configuration parameters for
 * Octeon XARRAY board.
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

#define CONFIG_EEPROM_HW_WR_PROTECT

#define CONFIG_FPGA_FLASH_LOAD

#define CONFIG_ENV_HW_INITIALIZE

#define CONFIG_CMD_MEMTEST
#define CONFIG_SYS_ALT_MEMTEST

#define CONFIG_PLX0_I2C_ADDR        0x38
#define CONFIG_PLX1_I2C_ADDR        0x39

#define CONFIG_WATCHDOG_WORKAROUND
#define CONFIG_WATCHDOG_BOOTTIME    40
#define CONFIG_WATCHDOG_SHOW
#define CONFIG_WATCHDOG_PERIOD      5
#define CONFIG_BOOTMAPSZ          (8 << 20)          /* Initial Memory map for Linux */
#define CONFIG_BOOT_PASS_IP                          /* Pass IP address in boot args */
#define CONFIG_BARGSIZE           CONFIG_SYS_CBSIZE
#define CONFIG_BDINFO_TIME
//#define CONFIG_MULTI_HANDLERS                        /* handle multi-image boot file */
#if 0
//#define CONFIG_NO_SHOW_INITRD_HDR
#define CONFIG_NO_SHOW_IMAGE_HDRS
#define CFG_IMAGE_LABELS        {"CODE: Linux Kernel     ",             \
                                 "CODE: Initial Ram Disk ",             \
                                 "FPGA: Queue Control/FTE",             \
                                 "FPGA: Multi-Channel MAC",             \
                                 "FPGA: Encryption Engine"              }
#endif

#define AVALON_REV_ADDR         0

#define CONFIG_ETH0ADDR         00:0f:7d:00:00:00
#define CONFIG_TSEC_COUNT       6

#define SYS_CONFIG_I2C_ADDR              0x30  /* Atmel system conf. device     */
#define SYS_CONFIG_I2C_ADDR_LEN             1  /* Atmel SCD address len=1 byte  */

#define CONFIG_COMPASS
#define CONFIG_SYS_COMPASS_I2C_ADDR      0x1e

#define CONFIG_CMD_DTT
#define CONFIG_MULTI_DTT
#define CONFIG_DTT_LM63
#define CONFIG_DTT_LM75
#define CONFIG_DTT_TMP42X
#define CONFIG_DTT_SENSORS      {0,1}   /* up to 2 sensors on cpu brd   */
#define CONFIG_DTT_SENSOR_NAMES {"CPU"}
#define CONFIG_SYS_DTT_LM63_ADDR         0x4c    /* LM63 I2C address             */
#define CONFIG_SYS_DTT_LM75_ADDR         0x48    /* LM75 I2C address             */
#define CONFIG_SYS_DTT_TMP42X_ADDR       0x4c    /* TMP42X I2C address           */
#define CONFIG_SYS_DTT_TMP42X   { 0, 1, 0, 0, 1, 1, 2, 0, 0, 0};

#define CONFIG_HW_RESET                 /* external reset capable       */

#define CONFIG_SYS_I2C_SPEED		380000

#define CONFIG_FLASH_PROTECT_LEN        (24*64*1024)  /* 24 sectors in 2MB flash for now */
#define CONFIG_FLASH_PROTECT_OLD        0x100000      /* protect 1MB */
#define CONFIG_MONITOR_OLD_OFFSET       (CONFIG_FLASH_PROTECT_OLD-8)

#define SPARTAN6_SIGNATURE         0xaa995566
#define SPARTAN6_SIGNATURE_OFFSET  16

#define CONFIG_AVALON_BASE            0x1d040000                   /* start of Avalon on boot bus     */
#define CONFIG_AVALON_CHIP_SEL        6
#define CONFIG_AVALON_WINDOW_SIZE     0x260000     /* 2.375MB section to store Avalon */
#define CONFIG_AVALON_IMAGE           (CONFIG_SYS_FLASH_BASE + 0x200000)  /* image in boot rom - 2MB offset  */
#define CONFIG_AVALON_IMAGE_END       (CONFIG_SYS_FLASH_BASE + 0x200000) + (CONFIG_AVALON_WINDOW_SIZE - 1)
#define CONFIG_AVALON_I2C_ADDR        0x40  /* 2-byte address + 4 bytes of data are required on each access */

#define CONFIG_CMD_BOOTD
#define CONFIG_DISK_CMDS

#define CONFIG_NEW_CMD_EDITOR
#define CONFIG_CONS_MORE
#define CONFIG_CONS_LINES         24
#define CONFIG_CONS_WIDTH         80
#define CONFIG_SWAP_HELP_SYNTAX

#define CONFIG_REDUCED_CMD_LIST

#define USE_OCTEON_INTERNAL_ARBITER  /* PCI arbiter for CN5020 */

#define CONFIG_TEMP_CORRECTION          /* enable temp correction       */
#define CONFIG_FAN_CONTROL              /* enable hw fan control        */

#define CONFIG_NET_RETRY_COUNT	  14

#define RESET_CODE_PON     0x0
#define RESET_CODE_MWD     0x1
#define RESET_CODE_UNUSED  0x2
#define RESET_CODE_EXT     0x3
#define RESET_CODE_MASK    0x3
#define RESET_CODE_SHIFT    16

#define CONFIG_PHY_AQUANTIA

/************************************************************************
 * Radio LED & Reset Configuration                                      *
 ************************************************************************/

#define CONFIG_XIRRUS_CONTROL_BASE              0x1d020000
#define CONFIG_XIRRUS_CONTROL_CHIP_SEL          7
#define CONFIG_XIRRUS_CONTROL_GRN_LED_OFFSET    0
#define CONFIG_XIRRUS_CONTROL_ORG_LED_OFFSET    2
#define CONFIG_XIRRUS_CONTROL_INT_RESET_OFFSET  4
#define CONFIG_XIRRUS_CONTROL_10G_RESET_OFFSET  6
#define CONFIG_XIRRUS_CONTROL_5V_SUPPLY_OFFSET  6
#define CONFIG_FAST_LED_TIME                    250     /* on/off time, ms, fast blink  */
#define CONFIG_SLOW_LED_TIME                    500     /* on/off time, ms, slow blink  */

/************************************************************************
 * Boot Configuration                                                   *
 ************************************************************************/
#define CONFIG_BOOTARGS         "console=ttyS0,115200n8 "               \
                                "root=/dev/ram rw "                     \
                                "quiet "

#define CONFIG_BOOTCOMMAND      "bootsetup; "                           \
                                "load $loadaddr $bootfile && "          \
                                "boot $loadaddr;"                       \
                                "reset"

#define CONFIG_BOOTFILE_ACTIVE      "X*.bin"
#define CONFIG_BOOTFILE_BACKUP      "X*.bin"

#define CONFIG_USERNAME         "admin"
#define CONFIG_PASSWORD         "enc 862d8d4937"

#define CONFIG_BOOTDELAY        3       /* enable autoboot after 3 sec  */
#define CONFIG_MENUPROMPT       "\nPress space bar to exit to bootloader: %2d "
#define CONFIG_SPECIFIC_MENUKEY ' '

#define CONFIG_STDIN            "ser_xc"
#define CONFIG_STDOUT           "ser_xc"
#define CONFIG_STDERR           "ser_xc"

#define CFG_XC_TIMEOUT       1    /* time to wait for char in ms  */

/************************************************************************
 * HUSH Parser Configuration                                            *
 ************************************************************************/
#define CONFIG_SYS_HUSH_PARSER
#undef CONFIG_FEATURE_COMMAND_EDITING

/************************************************************************
 * Branding Options & Rev definitions                                   *
 ************************************************************************/
#define CONFIG_BRAND_NAME "Xirrus"
#define CONFIG_BRAND_PREFIX "XS"
#define CONFIG_BRAND_PREFIX_ALT "XR"
#define CONFIG_BRAND_PROMPT "XBL"
#define CONFIG_BRAND_ALT "Avaya"
#define CONFIG_SYS_PROMPT CONFIG_BRAND_PROMPT">"

#define BD_XD2_230   0x82
#define BD_XD2_240   0x02
#define BD_XD3_230   0x03
#define BD_XD4_130   0x04
#define BD_XD4_240   0x84
#define BD_XA4_240   0x94
#define BD_XD8       0x08

#define BD_XR500        5
#define BD_XR600B       6  // used in beta
#define BD_XR700        7
#define BD_XR1000      10
#define BD_XR600       11  // hardcoded as 11 in toolchain due to old XR1100, so leave at 11 instead of changing to 6
#define BD_XR2000      20
#define BD_XR2100      21
#define BD_XR4000      40
#define BD_XR6000      60
#define BD_XSP         80

/************************************************************************
 * Message Options                                                      *
 ************************************************************************/
#define CONFIG_IDENT_MAIN       CONFIG_BRAND_NAME" Boot Loader"
#define CONFIG_IDENT_REV        "7.1.0"
#define CONFIG_IDENT_STRING     CONFIG_IDENT_MAIN " " CONFIG_IDENT_REV

#define CONFIG_ALTID_MAIN       CONFIG_BRAND_ALT" Boot Loader"
#define CONFIG_ALTID_REV        "7.1.0"
#define CONFIG_ALTID_STRING     CONFIG_ALTID_MAIN " " CONFIG_ALTID_REV

/*
 * Define CONFIG_OCTEON_PCI_HOST = 1 to map the pci devices on the
 * bus.  Define CONFIG_OCTEON_PCI_HOST = 0 for target mode when the
 * host system performs the pci bus mapping instead.  Note that pci
 * commands are enabled to allow access to configuration space for
 * both modes.
 */
#ifndef CONFIG_OCTEON_PCI_HOST
# define CONFIG_OCTEON_PCI_HOST		1
#endif

#define CONFIG_USB_OCTEON	     /** Enable USB support on OCTEON I */
#define CONFIG_OCTEON_USB_OCTEON2    /** Enable USB support on OCTEON II */

#include "octeon_common.h"

/* CONFIG_OCTEON_XARRAY set by Makefile in include/config.h */

/* Default DDR clock if tuple doesn't exist in EEPROM */
#define XARRAY_CN50XX_DEF_DRAM_FREQ     333
#define XARRAY_CN52XX_DEF_DRAM_FREQ     400
#define XARRAY_CN60XX_DEF_DRAM_FREQ     400
#define XARRAY_CN63XX_DEF_DRAM_FREQ     666
#define XARRAY_CN70XX_DEF_DRAM_FREQ     666

#define CONFIG_LBA48			/* 48-bit mode */
#define CONFIG_SYS_64BIT_LBA		/* 64-bit LBA support */
#define CONFIG_SYS_ATA_BASE_ADDR	0 /* Make compile happy */
/* Base address of Common memory for Compact flash */

/* eMMC support */
#define CONFIG_OCTEON_MMC		/* Enable MMC support */
#define CONFIG_MMC_MBLOCK		/* Multi-block support */
#define CONFIG_CMD_MMC			/* Enable mmc command */
#define CONFIG_SYS_MMC_SET_DEV		/* Enable multiple MMC devices */
#define CONFIG_MMC
#define CONFIG_OCTEON_MIN_BUS_SPEED_HZ	100000

/* Address of board EEPROM on TWSI bus */
#define CONFIG_SYS_I2C_EEPROM_ADDR       0x50  /* EEPROM address    */
#define CONFIG_SYS_I2C_EEPROM_ADDR_LEN   2     /* 16 bit addressing */
/* Default EEPROM address */
#define CONFIG_SYS_DEF_EEPROM_ADDR	 CONFIG_SYS_I2C_EEPROM_ADDR
/* These speed up writes to the serial EEPROM by enabling page writes.
 * Please see the datasheet for details.
 */
//#define CONFIG_SYS_EEPROM_PAGE_WRITE_BITS	7	/* 128 bytes */
#define CONFIG_SYS_EEPROM_PAGE_WRITE_DELAY_MS	5
//#define CFG_EEPROM_HW_WR_PROTECT                /* enable hw wr protect */

/** Enable octbootbus command */
#define CONFIG_CMD_OCTEON_BOOTBUS

/* The 'mtdids' environment variable is used in conjunction with the 'mtdparts'
 * variable to define the MTD partitions for u-boot.
 */
#define MTDPARTS_DEFAULT				\
	"octeon_nor0:2560k(bootloader)ro,"			\
	"2m(kernel),"					\
	"3576k(cramfs),"				\
	"8k(environment)ro\0"

#define MTDIDS_DEFAULT	"nor0=octeon_nor0\0"

/* Define this to enable built-in octeon ethernet support */
#define CONFIG_OCTEON_SGMII_ENET
#undef CONFIG_OCTEON_MGMT_ENET

/* Enable Octeon built-in networking if RGMII support is enabled */
#if defined(CONFIG_OCTEON_RGMII_ENET) || defined(CONFIG_OCTEON_SGMII_ENET) || \
	defined(OCTEON_XAUI_ENET)
# define CONFIG_OCTEON_INTERNAL_ENET
#endif

/* PCI console is supported since oct-remote-boot is supported.
 * Enable MUX and oct-remote-bootcmd support as well
 */
#undef CONFIG_SYS_PCI_CONSOLE
//#define CONFIG_CONSOLE_MUX
#define CONFIG_OCTEON_BOOTCMD

#include "octeon_cmd_conf.h"

#ifdef  CONFIG_CMD_NET
/**
 * Define available PHYs
 */
# define CONFIG_PHY_GIGE
# undef CONFIG_PHY_MARVELL
# undef CONFIG_PHY_VITESSE
# undef CONFIG_PHY_ATHEROS
# include "octeon_network.h"
#endif
#define CONFIG_BOOTP_SEND_HOSTNAME
#define CONFIG_BOOTP_SEND_MAC_CLIENTID

/* "tlv_eeprom" command */
#define CONFIG_CMD_OCTEON_TLVEEPROM
/* "qlm" command */
#define CONFIG_CMD_QLM

/* Enable MTD partitioning support */
#define CONFIG_CMD_MTDPARTS

/* Enable multiple file system support */
#define CONFIG_CMD_FS_GENERIC

/* Enable "date" command to set and get date from RTC */
#define CONFIG_CMD_DATE
#define CONFIG_CMD_FLASH		/* flinfo, erase, protect	*/
//#define CONFIG_CMD_EXT2			/* EXT2/3 filesystem support	*/
#define CONFIG_CMD_EXT4			/* EXT4 filesystem support	*/
//#define CONFIG_CMD_EXT4_WRITE
#define CONFIG_CMD_FAT			/* FAT support			*/
//#define CONFIG_FAT_WRITE		/* FAT write support		*/

/* SPI NOR flash support */
#define CONFIG_SF_DEFAULT_BUS		0
#define CONFIG_SF_DEFAULT_CS		0
#define CONFIG_SF_DEFAULT_SPEED		16000000

#undef CONFIG_OCTEON_SPI		/* Enable OCTEON SPI driver	*/
#undef CONFIG_SPI_FLASH		/* Enable SPI flash driver	*/
#undef CONFIG_SPI_FLASH_STMICRO	/* Enable ST Micro SPI flash	*/
#undef CONFIG_CMD_SPI			/* Enable SPI command		*/
#undef CONFIG_CMD_SF			/* Enable SPI flash command	*/

/*
 * Miscellaneous configurable options
 */
/* Environment variables that will be set by default */
#define	CONFIG_EXTRA_ENV_SETTINGS       "env-reset=1\0"

/*-----------------------------------------------------------------------
 * FLASH and environment organization
 */
#define CONFIG_SYS_FLASH_SIZE	        (8*1024*1024)	/* Flash size (bytes) 8MB */
#define CONFIG_SYS_REDUCED_FLASH_SIZE   (2*1024*1024)   /*                    2MB */
#define CONFIG_SYS_MAX_FLASH_BANKS	1	/* max number of memory banks */
#define CONFIG_SYS_MAX_FLASH_SECT	(256)	/* max number of sectors on one chip */

/* Width of CFI bus to start scanning */
#define CONFIG_SYS_FLASH_CFI_WIDTH	FLASH_CFI_8BIT
/* Enable extra elements in CFI driver for storing flash geometry */
#define CONFIG_SYS_FLASH_CFI  		1
/* Enable CFI flash driver */
#define CONFIG_FLASH_CFI_DRIVER		1
#define CONFIG_SYS_FLASH_USE_BUFFER_WRITE

/* We're not RAM_RESIDENT so CONFIG_ENV_IS_IN_FLASH will be set. */
#if CONFIG_RAM_RESIDENT
# define	CONFIG_ENV_IS_NOWHERE	1
#else
# define	CONFIG_ENV_IS_IN_FLASH	1
#endif

/* Address and size of Primary Environment Sector	*/
#define CONFIG_ENV_SIZE		        (4*1024)

#define CONFIG_ENV_MULTI
#define CONFIG_ENV_IS_IN_FLASH          1
#define CONFIG_ENV_IS_IN_EEPROM         1

#define CONFIG_SCD_ADDR                 (CONFIG_SYS_FLASH_BASE + CONFIG_SYS_REDUCED_FLASH_SIZE - 24*1024)
#define CONFIG_SCD_SECT_SIZE            (8*1024)

#define CONFIG_ENV_SECT_SIZE            (8*1024)
#define CONFIG_ENV_FLASH_ADDR           (CONFIG_SYS_FLASH_BASE + CONFIG_SYS_REDUCED_FLASH_SIZE - 32*1024)

#define CONFIG_AOS_CRASH_ADDR           (CONFIG_SYS_FLASH_BASE + CONFIG_SYS_REDUCED_FLASH_SIZE - 64*1024)
#define CONFIG_AOS_CRASH_SECT_SIZE      (32*1024)

#define CONFIG_AOS_CONFIG_ADDR          (CONFIG_SYS_FLASH_BASE + CONFIG_SYS_REDUCED_FLASH_SIZE - 128*1024)
#define CONFIG_AOS_CONFIG_SECT_SIZE     (64*1024)

#define CONFIG_EEPROM_SIZE              (64*1024)  /* EEPROM total size */
#define CONFIG_ENV_OFFSET               (CONFIG_EEPROM_SIZE-CONFIG_ENV_SIZE)
#define CONFIG_ENV_ADDR		        CONFIG_ENV_OFFSET
#define CONFIG_ENV_BLK_SIZE             0x400   /* block size when writing to eeprom */

#define CONFIG_CERT_FLASH_ADDR          (CONFIG_SYS_FLASH_BASE + 0x1C0000)
#define CONFIG_CERT_FLASH_SIZE          (128*1024)

//#define CONFIG_CRC_SIZE                 4
#define CONFIG_ENV_CLR_EEPROM_ON_RST       /* clear entire eeprom on reset */
//#define CONFIG_ENV_HW_INITIALIZE           /* initial environment from hw  */
//#define CONFIG_ENV_INIT_ON_BOOT            /* init env every boot          */
//#define CONFIG_ENV_RESIZE_LESS_CRC         /* smooth move to new env_size  */

#define CONFIG_ENV_PERSIST_VARS                                                             \
        {"system_ser#",      "system_prt#",      "system_date",      "system_mfg#",         \
         "cpucard_ser#",     "cpucard_prt#",     "cpucard_date",     "cpucard_mfg#",        \
         "rfcard0_ser#",     "rfcard0_prt#",     "rfcard0_date",     "rfcard0_mfg#",        \
         "rfcard1_ser#",     "rfcard1_prt#",     "rfcard1_date",     "rfcard1_mfg#",        \
         "rfcard2_ser#",     "rfcard2_prt#",     "rfcard2_date",     "rfcard2_mfg#",        \
         "rfcard3_ser#",     "rfcard3_prt#",     "rfcard3_date",     "rfcard3_mfg#",        \
         "eth0addr",         "eth1addr",         "eth2addr",         "eth3addr",            \
         "rfaddrbase",       "rfaddrcount",      "country_code",     "scd_ocscal",          \
         "license_key",                                                                     }

#define CONFIG_OCTEON_FLASH_USES_ALE

/*-----------------------------------------------------------------------
 * Cache Configuration
 */
#define CONFIG_SYS_DCACHE_SIZE		(32 * 1024)
#define CONFIG_SYS_ICACHE_SIZE		(37 * 1024)

#if CONFIG_OCTEON_PCI_HOST
/* Right now only the Intel E1000 driver has been ported */
//# define CONFIG_E1000
# define CONFIG_LIBATA
#if 0
# define CONFIG_CMD_SATA			/* Enable the sata command */
# define CONFIG_SYS_SATA_MAX_DEVICE	8	/* Support up to 2 devices */
# define CONFIG_SATA_AHCI
# define CONFIG_SATA_PMP
# define CONFIG_AHCI_SETFEATURES_XFER
# define CONFIG_SYS_SATA_MAX_LUN	16
#endif
#endif

/* The CN70XX has two TWSI buses and the XARRAY board uses both
 * of them
 */
//#define CONFIG_SYS_MAX_I2C_BUS	 0

#define CONFIG_RTC
#define CONFIG_MULTI_RTC                     /* support multiple RTCs        */
#define CONFIG_RTC_PCF8563                   /* real time clock (XR500)      */
#define CONFIG_RTC_DS1338                    /* real time clock (else)       */
#define CONFIG_SYS_RTC_PCF8563_ADDR    0x51  /* PCF8563 I2C addres           */
#define CONFIG_SYS_RTC_DS1338_ADDR     0x68  /* DS1338 I2C address           */
#define CONFIG_SYS_RTC_BUS_NUM	          0
#define CONFIG_SYS_TIMEZONE_STR        "GMT"

/* Enable watchdog support */
#define CONFIG_HW_WATCHDOG

/* Configure QLM */
#define CONFIG_OCTEON_QLM

/* Include shared board configuration, consisting mainly of DRAM details. */
#include "octeon_xarray_shared.h"

#endif	/* __CONFIG_H__ */
