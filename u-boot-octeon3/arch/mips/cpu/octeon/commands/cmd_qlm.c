/***********************license start************************************
 * Copyright (c) 2003-2011 Cavium Inc. (support@cavium.com). All rights
 * reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of Cavium Inc. nor the names of
 *       its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written
 *       permission.
 *
 * TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS"
 * AND WITH ALL FAULTS AND CAVIUM INC. MAKES NO PROMISES, REPRESENTATIONS
 * OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, WITH
 * RESPECT TO THE SOFTWARE, INCLUDING ITS CONDITION, ITS CONFORMITY TO ANY
 * REPRESENTATION OR DESCRIPTION, OR THE EXISTENCE OF ANY LATENT OR PATENT
 * DEFECTS, AND CAVIUM SPECIFICALLY DISCLAIMS ALL IMPLIED (IF ANY) WARRANTIES
 * OF TITLE, MERCHANTABILITY, NONINFRINGEMENT, FITNESS FOR A PARTICULAR
 * PURPOSE, LACK OF VIRUSES, ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET
 * POSSESSION OR CORRESPONDENCE TO DESCRIPTION.  THE ENTIRE RISK ARISING OUT
 * OF USE OR PERFORMANCE OF THE SOFTWARE LIES WITH YOU.
 *
 *
 * For any questions regarding licensing please contact
 * marketing@cavium.com
 *
 ***********************license end**************************************/

/**
 * @file
 *
 * $Id: cmd_qlm.c 78131 2012-11-08 00:05:12Z cchavva $
 *
 */

#include <common.h>
#include <command.h>
#include <exports.h>
#include <linux/ctype.h>
#include <net.h>
#include <elf.h>
#include <asm/mipsregs.h>
#include <asm/processor.h>
#include <asm/arch/cvmx-qlm.h>
#include <asm/arch/cvmx-helper-jtag.h>
#include <asm/arch/octeon_boot.h>

#ifndef CONFIG_OCTEON_XARRAY
static void usage(char * const argv[])
{
	printf("\n"
	       "Usage:\n"
	       "  %s <qlm>\n"
	       "    Read and display all QLM jtag settings.\n"
	       "\n"
	       "  %s <qlm> <lane> <name> <value> ... <lane x> <name x> <value x>\n"
	       "    Write a QLM \"lane\" jtag setting of \"name\" as \"value\".\n"
	       "\n", argv[0], argv[0]);
	printf("    qlm     Which QLM to access.\n"
	       "    lane    Which lane number to write a setting for or \"all\" for all lanes.\n"
	       "    name    Name of qlm setting to write.\n"
	       "    value   The value can be in decimal of hex (0x...).\n"
	       "\n"
	       "Incorrect settings can damage chips, so be careful!\n" "\n");
}
#endif

static uint64_t inline convert_number(const char *str)
{
	unsigned long long result;
	result = simple_strtoul(str, NULL, 10);
	return result;
}

#ifdef CONFIG_OCTEON_XARRAY
/**
 * Display the state of all register for all lanes
 * on a QLM.
 */
static void display_registers(int qlm)
{
	int num_lanes = cvmx_qlm_get_lanes(qlm);
        int lane;
	const __cvmx_qlm_jtag_field_t *ptr = cvmx_qlm_jtag_get_field();

	printf("%29s", "Field[<stop bit>:<start bit>]");
	for (lane = 0; lane < num_lanes; lane++)
		printf("\t      Lane %d", lane);
	printf("\n");

	while (ptr != NULL && ptr->name) {
		printf("%20s[%3d:%3d]", ptr->name, ptr->stop_bit, ptr->start_bit);
		for (lane = 0; lane < num_lanes; lane++) {
			uint64_t val = cvmx_qlm_jtag_get(qlm, lane, ptr->name);
			printf("\t%4llu (0x%04llx)", val, val);
		}
		printf("\n");
		ptr++;
	}
}
#endif

int do_qlm(cmd_tbl_t * cmdtp, int flag, int argc, char * const argv[])
{

	int num_qlm;
	int qlm;
	int c_arg;

	/* Make sure we got the correct number of arguments */
#ifdef CONFIG_OCTEON_XARRAY
	if (argc != 2) {
		printf("Invalid number of arguments %d\n\n", argc);
		return -1;
	}
#else
	if (((argc - 2) % 3) != 0) {
		printf("Invalid number of arguments %d\n", argc);
		usage(argv);
		return -1;
	}
#endif
	
	num_qlm = cvmx_qlm_get_num();

	qlm = simple_strtoul(argv[1], NULL, 0);

#ifdef CONFIG_OCTEON_XARRAY
        if (num_qlm == 0) {
		printf("No QLMs are available on this chip.\n");
		return 0; 
        } else if ((qlm < 0) || (qlm >= num_qlm)) {
                printf("Invalid qlm number - must be 0 to %d\n", num_qlm-1);
		return 0;
	}
#else
	if ((qlm < 0) || (qlm >= num_qlm)) {
		printf("Invalid qlm number\n");
		return -1;
	}
#endif

	if (argc >= 5) {
		int lane;
		uint64_t value;
		int field_count;
		int num_fields = (argc - 2) / 3;
		c_arg = 2;
		for (field_count = 0; field_count < num_fields; field_count++) {
			char name[30];
			if (!strcmp(argv[c_arg], "all"))
				lane = -1;
			else {
				lane = (int)simple_strtol(argv[c_arg], NULL, 0);
				if (lane >= cvmx_qlm_get_lanes(qlm)) {
					printf("Invalid lane passed\n");
					return -1;
				}
			}
			strcpy(name, argv[c_arg + 1]);
			value = convert_number(argv[c_arg + 2]);
			cvmx_qlm_jtag_set(qlm, lane, name, value);

			/* Assert serdes_tx_byp to force the new settings to
  			   override the QLM default. */
			if (strncmp(name, "biasdrv_", 8) == 0 ||
				strncmp(name, "tcoeff_", 7) == 0)
				cvmx_qlm_jtag_set(qlm, lane, "serdes_tx_byp", 1);
			c_arg += 3;
			if (lane == -1)
				break;
		}
	} else
#ifdef CONFIG_OCTEON_XARRAY
		display_registers(qlm);
#else
		cvmx_qlm_display_registers(qlm);
#endif
	return 0;
}

U_BOOT_CMD(qlm, (CONFIG_SYS_MAXARGS - 1), 0, do_qlm,
#ifdef CONFIG_OCTEON_XARRAY
	   "Octeon QLM debug function",
#else
	   "Octeon QLM debug function (dangerous - remove from final product)",
#endif
	   "Usage:\n"
	   "  qlm <qlm>\n"
	   "    Read and display all QLM jtag settings.\n"
#ifdef CONFIG_OCTEON_XARRAY
	   "\n");
#else
	   "\n"
	   "  qlm <qlm> <lane> <name> <value> ... <lane x> <name x> <value x>\n"
	   "    Write one or more QLM \"lane\" jtag setting of \"name\" as \"value\".\n"
	   "\n"
	   "    qlm     Which QLM to access.\n"
	   "    lane    Which lane number to write a setting for or \"all\" for all lanes.\n"
	   "    name    Name of qlm setting to write.\n"
	   "    value   The value can be in decimal or hex (0x...).\n"
	   " Note: multiple lane, name, value triplets may be specified on the same line.\n"
	   "\n"
	   "WARNING: Incorrect settings can damage chips, so be careful!\n\n");
#endif
