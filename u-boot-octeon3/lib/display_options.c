/*
 * (C) Copyright 2000-2002
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <config.h>
#include <common.h>
#include <version.h>
#include <linux/ctype.h>
#include <asm/io.h>

int display_options (void)
{
#if defined(BUILD_TAG)
	printf ("\n\n%s, Build: %s\n\n", version_string, BUILD_TAG);
#else
	printf ("\n\n%s\n\n", version_string);
#endif
	return 0;
}

#ifdef CONFIG_OCTEON_XARRAY
void PrintSize (uint64_t size, int ld, int td, const char *s)
{
	uint64_t m, n;
	uint64_t d = 1;
	uint64_t p;
	char *c = " KMG";

	while (size / d >= 1000) {
		c++;
		d <<= 10;
	}

	p = (td == 0)             ? 1   : 
	    (td == 1 || td == -1) ? 10  : 
	    (td == 2)             ? 100 : 
		                    1000;

	n = size / d;

	m = (p * (size - n*d) + d/2) / d;

	if (m >= p) {
		m -= p;
		n += 1;
	}

	printf ("%*d", ld, (int)n);
	if (td > 0 || (td == -1 && m != 0)) {
          printf (".%0*d", td < 0 ? 1 : td, (int)m);
	}
	printf (" %cB%s", *c, s);
}
#endif

/*
 * print sizes as "xxx KiB", "xxx.y KiB", "xxx MiB", "xxx.y MiB",
 * xxx GiB, xxx.y GiB, etc as needed; allow for optional trailing string
 * (like "\n")
 */
void print_size(unsigned long long size, const char *s)
{
#ifdef CONFIG_OCTEON_XARRAY
	PrintSize(size, 3, -1, s);
#else
	unsigned long m = 0, n;
	unsigned long long f;
	static const char names[] = {'E', 'P', 'T', 'G', 'M', 'K'};
	unsigned long d = 10 * ARRAY_SIZE(names);
	char c = 0;
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(names); i++, d -= 10) {
		if (size >> d) {
			c = names[i];
			break;
		}
	}

	if (!c) {
		printf("%llu Bytes%s", size, s);
		return;
	}

	n = size >> d;
	f = size & ((1ULL << d) - 1);

	/* If there's a remainder, deal with it */
	if (f) {
		m = (10ULL * f + (1ULL << (d - 1))) >> d;

		if (m >= 10) {
			m -= 10;
			n += 1;
		}
	}

	printf ("%lu", n);
	if (m) {
		printf (".%ld", m);
	}
	printf (" %ciB%s", c, s);
#endif
}

/*
 * Print data buffer in hex and ascii form to the terminal.
 *
 * data reads are buffered so that each memory address is only read once.
 * Useful when displaying the contents of volatile registers.
 *
 * parameters:
 *    addr: Starting address to display at start of line
 *    data: pointer to data buffer
 *    width: data value width.  May be 1, 2, or 4.
 *    count: number of values to display
 *    linelen: Number of values to print per line; specify 0 for default length
 */
#define MAX_LINE_LENGTH_BYTES (64)
#define DEFAULT_LINE_LENGTH_BYTES (16)
int print_buffer(ulong addr, const void *data, uint width, uint count,
		 uint linelen)
{
	/* linebuf as a union causes proper alignment */
	union linebuf {
		uint32_t ui[MAX_LINE_LENGTH_BYTES/sizeof(uint32_t) + 1];
		uint16_t us[MAX_LINE_LENGTH_BYTES/sizeof(uint16_t) + 1];
		uint8_t  uc[MAX_LINE_LENGTH_BYTES/sizeof(uint8_t) + 1];
	} lb;
	int i;

	if (linelen*width > MAX_LINE_LENGTH_BYTES)
		linelen = MAX_LINE_LENGTH_BYTES / width;
	if (linelen < 1)
		linelen = DEFAULT_LINE_LENGTH_BYTES / width;

	while (count) {
		uint thislinelen = linelen;
		printf("%08lx:", addr);

		/* check for overflow condition */
		if (count < thislinelen)
			thislinelen = count;

		/* Copy from memory into linebuf and print hex values */
		for (i = 0; i < thislinelen; i++) {
			uint32_t x;
			if (width == 4)
				x = lb.ui[i] = *(volatile uint32_t *)data;
			else if (width == 2)
				x = lb.us[i] = *(volatile uint16_t *)data;
			else
				x = lb.uc[i] = *(volatile uint8_t *)data;
			printf(" %0*x", width * 2, x);
			data += width;
		}

		while (thislinelen < linelen) {
			/* fill line with whitespace for nice ASCII print */
			for (i=0; i<width*2+1; i++)
				puts(" ");
			linelen--;
		}

		/* Print data in ASCII characters */
		for (i = 0; i < thislinelen * width; i++) {
			if (!isprint(lb.uc[i]) || lb.uc[i] >= 0x80)
				lb.uc[i] = '.';
		}
		lb.uc[i] = '\0';
		printf("    %s\n", lb.uc);

		/* update references */
		addr += thislinelen * width;
		count -= thislinelen;

		if (ctrlc())
			return -1;
	}

	return 0;
}

#ifdef CONFIG_OCTEON_XARRAY
/*
 * print times as "xxx ms " or "xxx.x sec" as needed;
 * allow for optional trailing string (like "\n")
 */
void PrintTime (ulong t, int ld, int td, const char *str)
{
	ulong s = t/1000;

	if (s) {
		ulong f;
		ulong p;

		p = (td == 0)             ? 1   : 
		    (td == 1 || td == -1) ? 10  : 
		    (td == 2)             ? 100 : 
					    1000;

		f = (p * (t - s*1000) + 500) / 1000;

		if (f >= p) {
			f -= p;
			s += 1;
		}

		printf ("%*ld", ld, s);
		if (td > 0 || (td == -1 && f != 0)) {
			printf (".%0*ld", td < 0 ? 1 : td, f);
		}
		puts(" sec");
	}
	else {
		printf ("%*ld ms", ld, t);
	}
	puts(str);
}

/*
 * Display progress line with size included
 */

#ifndef CONFIG_CONS_WIDTH
#define CONFIG_CONS_WIDTH		80	/* console width in chars 	*/
#endif

#define HASH_INDENT		30	/* indent for "loading" hashes  */
#define HASH_SPEED		60	/* default hash speed (ms/hash) */

static int   HashCount;
static int   HashesPerLine;
static ulong HashStartTime;
static ulong HashLastTime;
static ulong HashIntervalTime;

static char command_str[CFG_CMD_MSG_WIDTH+1];
static char sub_cmd_str[CFG_CMD_SUB_WIDTH+1];

void StartHash (char *cmdstr, char *substr) 
{
	char *s;

	strncpy(command_str, cmdstr, CFG_CMD_MSG_WIDTH);
	strncpy(sub_cmd_str, substr, CFG_CMD_SUB_WIDTH);

	HashesPerLine    = (((s = getenv ("consolewidth")) != NULL) ? simple_strtoul (s, NULL, 0) : CONFIG_CONS_WIDTH) - HASH_INDENT;
	HashIntervalTime = (((s = getenv ("hashspeed"))    != NULL) ? simple_strtoul (s, NULL, 0) : HASH_SPEED);

	HashCount = 0;
	HashLastTime = HashStartTime = get_timer(0);

#ifdef CONFIG_CONS_MORE
	serial_rstmore(-1);
#endif
	put_cmd_label(command_str, sub_cmd_str);
	puts ("*\b");
}

int GetHashLen (void) 
{
	return(HashesPerLine-1);
}

void PutHash (char c, ulong bytes)
{
	if (get_timer(HashLastTime) > HashIntervalTime) {
		putc (c);
		if (bytes) {
			putc (' ');
			PrintSize(bytes, 3, 1, "\b\b\b\b\b\b\b\b\b");
		}
		if ((++HashCount % HashesPerLine) == 0) {
			puts("         \n");
			put_cmd_label(command_str, sub_cmd_str);
		}
		HashLastTime = get_timer(0);
	}
}

void EndHash (ulong bytes) 
{
	ulong HashEndTime = get_timer(HashStartTime);
	ulong speed;

	puts ("\b done     \n");
	put_cmd_label(command_str, "Complete");
	PrintTime(HashEndTime, 1, 1, ", ");

        if (HashEndTime != 0) {
	        if      (bytes > 400000000) speed = (bytes*1)   /(HashEndTime/1000);
	        else if (bytes >  40000000) speed = (bytes*10)  /(HashEndTime/100);
	        else if (bytes >   4000000) speed = (bytes*100) /(HashEndTime/10);
	        else                        speed = (bytes*1000)/(HashEndTime/1);
        }
        else
                speed = 0;

	PrintSize(speed, 1, 1, "/sec\n");

#ifdef CONFIG_CONS_MORE
	serial_rstmore(0);
#endif
}

#endif //CONFIG_OCTEON_XARRAY
