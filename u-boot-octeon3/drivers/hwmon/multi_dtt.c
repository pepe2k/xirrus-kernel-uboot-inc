/*
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * Multi DTT support.
 *
 * Allows code to be compiled with multiple DTT drivers to support
 * multiple versions of hardware.  The code uses the first DTT found.
 * To add support for other DTT's, add the dtt_call macro to the original
 * driver file and then add the appropriate sections here.
 */

#include <common.h>
#include <command.h>
#include <dtt.h>
#include <i2c.h>

#if defined(CONFIG_MULTI_DTT) && defined(CONFIG_CMD_DTT)

/*---------------------------------------------------------------------*/
#undef DEBUG_DTT

#ifdef DEBUG_DTT
#define DEBUGD(fmt,args...) printf(fmt ,##args)
#else
#define DEBUGD(fmt,args...)
#endif
/*---------------------------------------------------------------------*/
static uchar test_data;

#define dtt_test(addr, alen, id)  (i2c_read (addr, 0xff, alen, &test_data, 1) == 0) && ((test_data == id) || (id == -1))


#ifdef CONFIG_DTT_LM63
    int LM63_init_one(int sensor);
    int LM63_get_temp(int sensor);
#define LM63_test() dtt_test(CONFIG_SYS_DTT_LM63_ADDR, 1, 0x49)
#endif

#ifdef CONFIG_DTT_LM75
    int LM75_init_one(int sensor);
    int LM75_get_temp(int sensor);
#define LM75_test() dtt_test(CONFIG_SYS_DTT_LM75_ADDR, 1, -1)
#endif

#ifdef CONFIG_DTT_TMP42X
    int TMP42X_init_one(int sensor);
    int TMP42X_get_temp(int sensor);
#define TMP42X_test() dtt_test(CONFIG_SYS_DTT_TMP42X_ADDR, 1, 0x21)
#endif

/*
 * initial the temp sensor
 */
int dtt_init_one (int sensor)
{
#ifdef CONFIG_TEMP_CORRECTION
        extern void lookup_temp_correction(void);
        lookup_temp_correction();
#endif
#ifdef CONFIG_DTT_LM63
	if (LM63_test())   return LM63_init_one(sensor);
#endif
#ifdef CONFIG_DTT_LM75
	if (LM75_test())   return LM75_init_one(sensor);
#endif
#ifdef CONFIG_DTT_TMP42X
	if (TMP42X_test()) return TMP42X_init_one(sensor);
#endif
        return -1;
}

/*
 * get the current temp(s)
 */
int dtt_get_temp (int sensor)
{
#ifdef CONFIG_TEMP_CORRECTION
        DECLARE_GLOBAL_DATA_PTR;
        int temp_offset = (sensor == 0) ? gd->temp_correction : 0;
#else
        int temp_offset = 0;
#endif
#ifdef CONFIG_DTT_LM63
	if (LM63_test())   {int temp = LM63_get_temp(sensor);   return temp ? temp + temp_offset : 0; }
#endif
#ifdef CONFIG_DTT_LM75
	if (LM75_test())   {int temp = LM75_get_temp(sensor);   return temp ? temp + temp_offset : 0; }
#endif
#ifdef CONFIG_DTT_TMP42X
	if (TMP42X_test()) {int temp = TMP42X_get_temp(sensor); return temp ? temp + temp_offset : 0; }
#endif
        return -1;
}

#endif /* CONFIG_MULTI_DTT && CONFIG_CMD_DTT */
