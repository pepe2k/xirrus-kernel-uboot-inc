/*
 * (C) Copyright 2004
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <command.h>
#include <stdio_dev.h>
#include <net.h>
#include <rc4.h>
#include <asm/arch/lib_octeon.h>

/* This code was pulled from Xirrus u-boot based on Cavium Octeon SDK-2.3 and then modified from there. */

#ifdef CONFIG_NETCONSOLE

#undef  DEBUG_NC
#ifdef  DEBUG_NC
#define debug_nc(fmt,args...) ({char buffer[100]; sprintf(buffer, fmt ,##args); serial_puts(buffer);})
#else
#define debug_nc(fmt,args...)
#endif

#define ELAPSED_TIME(then)    ({uint64_t now = get_timer(0), elapsed; elapsed = (now >= then) ? now - then : now - (int64_t)then;})

#define CFG_XIRCON_1ST_BEACON "\000!XBL"
#define CFG_XIRCON_XBL_BEACON "\000XBL"
#define nc_send_1st_beacon()  nc_put_raw(CFG_XIRCON_1ST_BEACON, sizeof(CFG_XIRCON_1ST_BEACON))
#define nc_send_beacon()      nc_put_raw(CFG_XIRCON_XBL_BEACON, sizeof(CFG_XIRCON_XBL_BEACON))

#define CFG_ENCRYPT_NETCONSOLE_TX
#define CFG_ENCRYPT_NETCONSOLE_RX
#undef  CFG_XC_KEEPALIVE

#define CFG_XIRCON_MULTICAST
#ifdef  CFG_XIRCON_MULTICAST
IPaddr_t dflt_remote_ip       =  0xe0000078; // 224.0.0.120
uchar    dflt_remote_ether[6] = {0x01, 0x00, 0x5e, 0x00, 0x00, 0x78};
#else
IPaddr_t dflt_remote_ip       =  0xffffffff; // 255.255.255.255
uchar    dflt_remote_ether[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
#endif

DECLARE_GLOBAL_DATA_PTR;

static char input_buffer[512];
static int  input_size       = 0;	/* char count in input buffer */
static int  input_offset     = 0;	/* offset to valid chars in input buffer */
static int  input_recursion  = 0;
static int  output_recursion = 0;
static uchar nc_ether[6];		/* server enet address */
static IPaddr_t nc_ip;			/* server ip */
static short nc_port;			/* source/target port */
static const char *output_packet;	/* used by first send udp */
static int output_packet_len     = 0;
static uint64_t last_packet_time = 0;

/*
 * Start with a default last protocol.
 * We are only interested in NETCONS or not.
 */
enum proto_t net_loop_last_protocol = BOOTP;

static void nc_wait_arp_handler(uchar *pkt, IPaddr_t sip, unsigned dest, unsigned src, unsigned len)
{
        net_set_state(NETLOOP_SUCCESS);	/* got arp reply - quit net loop */
}

static void nc_handler(uchar *pkt, IPaddr_t sip, unsigned dest, unsigned src, unsigned len)
{
	if (input_size)
                net_set_state(NETLOOP_SUCCESS);	/* got input - quit net loop */
}

static void nc_timeout(void)
{
        net_set_state(NETLOOP_SUCCESS);
}

void NcStart(void)
{
	if (!output_packet_len || memcmp (nc_ether, NetEtherNullAddr, 6) != 0) {
		/* going to check for input packet */
		net_set_udp_handler(nc_handler);
		NetSetTimeout(CFG_XC_TIMEOUT, nc_timeout);
	} else {
		/* send arp request */
		char *pkt;
		net_set_udp_handler(nc_wait_arp_handler);
                NetSetTimeout(0, (thand_f *)0);
		pkt = (char *)NetTxPacket + NetEthHdrSize () + IP_UDP_HDR_SIZE;
#ifdef  CFG_ENCRYPT_NETCONSOLE_TX
                rc4_process_str_idx(output_packet, pkt + 2, output_packet_len, NetIPID);
                *(uint16_t *)pkt = in_cksum(output_packet, output_packet_len);
                output_packet_len += 2;
#else
                memcpy(pkt, output_packet, output_packet_len);
#endif
		NetSendUDPPacket(nc_ether, nc_ip, nc_port, nc_port, output_packet_len);
	}
}

static void nc_send_packet(const char *buf, int len)
{
	struct eth_device *eth;
	int inited = 0;
	char *pkt;

	if (memcmp(nc_ether, NetEtherNullAddr, 6) == 0) {
                if ((eth = eth_get_cur_dev()) && eth->state == ETH_STATE_ACTIVE)
                        return;             /* inside net loop */

		output_packet = buf;
		output_packet_len = len;
		NetLoop(NETCONS);          /* wait for arp reply and send packet */
		output_packet_len = 0;
		return;
	}
	if ((eth = eth_get_dev()) && eth->state != ETH_STATE_ACTIVE) {
		if (!eth_init(gd->bd))
                        return;
		inited = 1;
	}
	pkt = (char *)NetTxPacket + NetEthHdrSize () + IP_UDP_HDR_SIZE;

#ifdef  CFG_ENCRYPT_NETCONSOLE_TX
        rc4_process_str_idx(buf, pkt + 2, len, NetIPID);
        *(uint16_t *)pkt = in_cksum(buf, len);
        len += 2;
#else
        memcpy(pkt, buf, len);
#endif
	NetSendUDPPacket(nc_ether, nc_ip, nc_port, nc_port, len);

	if (inited) {
		eth_halt();
                if (NetProtocol == NET_IDLE && ELAPSED_TIME(last_packet_time) > 100) {
                        NetLoop(NETCONS);       /* kind of poll */
                }
        }
        last_packet_time = get_timer(0);
}

static void nc_put_raw(const char *s, int len)
{
        if (output_recursion)
                return;

        gd->flags |= GD_FLG_NETCONSOLE;
        output_recursion = 1;

        nc_send_packet(s, len);

        output_recursion = 0;
        gd->flags &= ~GD_FLG_NETCONSOLE;
}

static void nc_put_buff(const char *s, int len)
{
        if (!(gd->flags & GD_FLG_XIRCON_NOT_STARTED)) {
                int max_pkt = PKTSIZE - NetEthHdrSize() - IP_UDP_HDR_SIZE;

                if (len > max_pkt)
                    len = max_pkt;

                nc_put_raw(s, len);
        }
}

int nc_input_packet(struct ethernet_hdr *et, struct ip_udp_hdr *ip, char *data, unsigned dest, unsigned src, unsigned len)
{
	int end, chunk;

        debug_nc("nc data rcv, port=%d\n\r", dest);

	if (dest != nc_port || !len)
		return 0;		/* not for us */

#ifdef  CFG_ENCRYPT_NETCONSOLE_RX
        if (len <= 2)
                return 0;
        len -= 2;
        char buff[len];
        rc4_process_str_idx(data + 2, buff, len, ntohs(ip->ip_id));
        if (*(uint16_t *)data != in_cksum(buff, len))
                return 0;
        data = buff;
#endif
	// check if beacon
        if (len == 2 && *(uint16_t *)data == 0)
	{
                gd->flags &= ~GD_FLG_XIRCON_NOT_STARTED;

		if (output_recursion)
			return 0;

		if (!(gd->flags & GD_FLG_NETCONSOLE)) {
			gd->flags |= GD_FLG_NETCONSOLE;
			output_recursion = 1;
		}
		nc_send_beacon();

		if (output_recursion) {
			output_recursion = 0;
			gd->flags &= ~GD_FLG_NETCONSOLE;
		}
		return 1;
	}

	// only accept input from directed packets
	if (memcmp(et->et_dest, NetOurEther, 6) == 0)
	{
		last_packet_time = get_timer(0);

		if (input_size == sizeof input_buffer)
			return 1;		/* no space */
		if (len > sizeof input_buffer - input_size)
			len = sizeof input_buffer - input_size;

		end = input_offset + input_size;
		if (end > sizeof input_buffer)
			end -= sizeof input_buffer;

		chunk = len;
		if (end + len > sizeof input_buffer) {
			chunk = sizeof input_buffer - end;
			memcpy(input_buffer, data + chunk, len - chunk);
		}
		memcpy (input_buffer + end, data, chunk);

		input_size += len;
                gd->flags  &= ~GD_FLG_XIRCON_NOT_STARTED;
		return 1;
	}
	return 0;
}

static void nc_putc(char c)
{
	nc_put_buff(&c, 1);
}

static void nc_puts(const char *s)
{
	nc_put_buff(s, strlen(s));
}

static int nc_tstc(void)
{
	struct eth_device *eth;

	if (input_recursion) return 0;
	if (input_size)      return 1;

        if ((eth = eth_get_cur_dev()) && eth->state == ETH_STATE_ACTIVE)
		return 0;	            /* inside net loop */

        gd->flags |= GD_FLG_NETCONSOLE;
	input_recursion = 1;

	if (NetLoop(NETCONS) < 0)           /* kind of poll */
            udelay((CFG_XC_TIMEOUT * 5000 * 750)/(octeon_get_ioclk_hz()/1000000));  /* kill some time if netloop failed, needed to make bootloader exit timing work */
                                                                                    /* 750 Mhz is XR6000 IO clock speed */
#ifdef  CFG_XC_KEEPALIVE
        {
            char c = 0;

            if (ELAPSED_TIME(last_packet_time) > CFG_XC_KEEPALIVE)
                    nc_send_packet(&c, 1);      /* send keep alive */
        }
#endif
	input_recursion = 0;
        gd->flags &= ~GD_FLG_NETCONSOLE;

	return input_size != 0;
}

static int nc_getc(void)
{
        uchar c;

        while (!nc_tstc())
                ;

	c = input_buffer[input_offset++];

	if (input_offset >= sizeof input_buffer)
		input_offset -= sizeof input_buffer;
	input_size--;

	return c;
}

static int nc_start(void)
{
        // only ever do this once on first time called
        // use global data console initial msg buffer as our flag
        if (!gd->con_init_buff[0])
                return 0;

        int netmask, our_ip;
        int wait_count = 100;
        char *p;

        /* initialize global variables */
        input_size        = 0;
        input_offset      = 0;
        input_recursion   = 0;
        output_recursion  = 0;
        output_packet     = 0;
        output_packet_len = 0;
        nc_port           = 22612;                                          /* default port */
        memset(input_buffer, 0, sizeof(input_buffer));

        gd->flags |= GD_FLG_XIRCON_NOT_STARTED;

        if ((p = getenv ("xcport")) != NULL) {
                nc_port = simple_strtoul (p , NULL, 10);
        }
        if (getenv ("xcip")) {
                nc_ip = getenv_IPaddr("xcip");
                if (!nc_ip)
                        return -1;                                          /* xcip is 0.0.0.0 */
                if ((p = strchr (getenv("xcip"), ':')) != NULL)
                        nc_port = simple_strtoul (p + 1, NULL, 10);
        } else
                nc_ip = dflt_remote_ip;                                     /* xcip is not set */

        if ((our_ip = getenv_IPaddr("ipaddr")) == 0)
             our_ip = getenv_IPaddr("ip0addr");
        netmask     = getenv_IPaddr("netmask");

        if (nc_ip == dflt_remote_ip ||                                      /* 255.255.255.255 */
           ((netmask & our_ip) == (netmask & nc_ip) &&                      /* on the same net */
            (netmask |  nc_ip) ==  dflt_remote_ip))                         /* broadcast to our net */
                memcpy (nc_ether,  dflt_remote_ether, sizeof nc_ether);
        else
                memcpy (nc_ether,  NetEtherNullAddr , sizeof nc_ether);     /* force arp request */

        /*
         * Initialize the static IP settings and buffer pointers
         * incase we call NetSendUDPPacket before NetLoop
         */
        net_init();

        nc_send_1st_beacon();                                               /* send a beacon to get things started */
        while ((gd->flags & GD_FLG_XIRCON_NOT_STARTED) && wait_count-- > 0)
                nc_tstc();                                                  /* wait to see if anyone is listening */

        nc_puts((const char *)gd->con_init_buff);
        gd->con_init_buff[0] = 0;

        eth_set_nc_dev();                                                   /* record eth_current as our nc device */
        return 0;
}

int drv_nc_init(void)
{
	struct stdio_dev dev;
	int rc;

	memset(&dev, 0, sizeof (dev));

	strcpy(dev.name, "xc");
	dev.flags = DEV_FLAGS_OUTPUT | DEV_FLAGS_INPUT | DEV_FLAGS_SYSTEM;
	dev.start = nc_start;
	dev.putc  = nc_putc;
	dev.puts  = nc_puts;
	dev.getc  = nc_getc;
	dev.tstc  = nc_tstc;

	rc = stdio_register(&dev);

	return (rc == 0) ? 1 : rc;
}

/************************************************************************
 * Combo serial/netconsole device                                       *
 ************************************************************************/
static void ser_nc_putc(char c)
{
            nc_putc(c);
        serial_putc(c);
}

static void ser_nc_puts(const char *s)
{
            nc_puts(s);
        serial_puts(s);
}

static int ser_nc_getc(void)
{
        for (;;) {
            if (serial_tstc()) return serial_getc();
            if (    nc_tstc()) return     nc_getc();
        }
        return 0;
}

static int ser_nc_tstc(void)
{
        return (serial_tstc()
                ||  nc_tstc());
}

int drv_ser_nc_init(void)
{
	struct stdio_dev dev;
	int rc;

	memset(&dev, 0, sizeof (dev));

	strcpy(dev.name, "ser_xc");
	dev.flags = DEV_FLAGS_OUTPUT | DEV_FLAGS_INPUT | DEV_FLAGS_SYSTEM;
	dev.start =     nc_start;
	dev.putc  = ser_nc_putc;
	dev.puts  = ser_nc_puts;
	dev.getc  = ser_nc_getc;
	dev.tstc  = ser_nc_tstc;

	rc = stdio_register (&dev);

	return (rc == 0) ? 1 : rc;
}
#endif // CONFIG_NETCONSOLE
