/*
 * XRP support driver
 *
 * Xirrus 2016
 *
 */

#include <common.h>
#include <command.h>
#include <net.h>
#include <rtc.h>
#include "xrp.h"

#undef  XRP_DEBUG
#ifdef  XRP_DEBUG
#define xrp_debug(fmt, args...)     printf(fmt, ##args)
#define xrp_decl(args...)           args
#else
#define xrp_debug(fmt, args...)
#define xrp_decl(args...)
#endif

#define XRP_TIMEOUT             100000UL
#define NZ128(p)                (((u32 *)(p))[0] || ((u32 *)(p))[1] || ((u32 *)(p))[2] || ((u32 *)(p))[3])
#define NZ48(p)                 (((u16 *)(p))[0] || ((u16 *)(p))[1] || ((u16 *)(p))[2])

static void demangle(u8 *xrp_packet, int length)
{
    u8 *salt = ((struct xrp_header *)xrp_packet)->salt;
    int i;

    xrp_packet += sizeof(struct xrp_header);
    length     -= sizeof(struct xrp_header);

    for (i = 0; i < length; i++) xrp_packet[i] ^= salt[i % XRP_MANGLE_SALT_LEN];
}

static struct xrp_info_element *ie_next(u8 *xrp_packet, int length, struct xrp_info_element *ie_last)
{
    u8 *end = xrp_packet + length - sizeof(struct xrp_info_element);
    struct xrp_info_element *ie_next;

    if (length < sizeof(struct xrp_beacon_pkt) + sizeof(struct xrp_info_element)) return NULL;
    if ((u8 *) ie_last == NULL) return ((struct xrp_beacon_pkt *)xrp_packet)->beacon.ies;
    if ((u8 *) ie_last  > end ) return NULL;
    if ((u8 *)(ie_next = (struct xrp_info_element *)((u8 *)ie_last + ie_last->len + 2)) > end ||
               ie_next->len == 0 || ie_next->data + ie_next->len > end) return NULL;

    return ie_next;
}

static void dumpXrpBeacon(u8 *xrp_packet, int length)
{
    struct xrp_beacon_pkt *xrp_beacon = (struct xrp_beacon_pkt *)xrp_packet;
    struct xrp_info_element *ie = NULL;
    int i;

    if (length < sizeof(struct xrp_beacon_pkt) + sizeof(struct xrp_info_element))
        return;

    demangle(xrp_packet, length);

    printf(         "----------------------------------------------------------------------\n");
    printf("[XRP   ] LENGTH  : %d bytes""\n", length);
    printf("[XRP   ] COMMAND : %d "     "\n", xrp_beacon->header.cmd );
    printf("[XRP   ] TUNNEL  : %d "     "\n", xrp_beacon->header.type);
    printf("[XRP   ] MAC ADDR: %pM "    "\n", xrp_beacon->beacon.mac );
    printf("[XRP   ] IP ADDR : %pI4 "   "\n", xrp_beacon->beacon.ip  );

    while ((ie = ie_next(xrp_packet, length, ie)) != NULL) {
        printf("[XRP   ] IE %2d   : ", ie->id);

        switch (ie->id) {
            // handle string IEs
            case IE_HOSTNAME : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[HOSTNAME ] %s", str); break; }
            case IE_LOCATION : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[LOCATION ] %s", str); break; }
            case IE_SOFTWARE : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[SOFTWARE ] %s", str); break; }
            case IE_XBL_NAME : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[XBL_NAME ] %s", str); break; }
            case IE_SCD_NAME : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[SCD_NAME ] %s", str); break; }
            case IE_MODEL    : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[MODEL    ] %s", str); break; }
            case IE_LICENSE  : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[LICENSE  ] %s", str); break; }
            case IE_SERIAL   : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[SERIAL   ] %s", str); break; }
            case IE_RECOVERY : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[RECOVERY ] %s", str); break; }
            case IE_FLASH    : { char str[ie->len+1]; strxcpy(str, ie->data); printf("[FLASH    ] %s", str); break; }

            // handle single numerical IEs
            case IE_XRP_MODE : printf("[XRP_MODE ] "); {              u8  *p =              (u8  *)ie->data; printf("%d "    ,       *p) ; break; }
            case IE_FEATURES : printf("[FEATURES ] "); {              u16 *p =              (u16 *)ie->data; printf("0x%04x ", ntohs(*p)); break; }

            // handle list IEs
            case IE_MAX_STAS : printf("[MAX_STAS ] "); {              u16 *p =              (u16 *)ie->data; for (; (u8 *)p - ie->data < ie->len            ; ++p) printf("%d "          ,   ntohs(*p)                                                          ); break; }
            case IE_STA_TYPES: printf("[STA_TYPES] "); {              u16 *p =              (u16 *)ie->data; for (; (u8 *)p - ie->data < ie->len            ; ++p) printf("%d "          ,   ntohs(*p)                                                          ); break; }
            case IE_CHAN_LIST: printf("[CHAN_LIST] "); {              u8  *p =              (u8  *)ie->data; for (; (u8 *)p - ie->data < ie->len && *p != 0 ; ++p) printf("%d "          ,         *p                                                           ); break; }
            case IE_GROUPS   : printf("[GROUPS   ] "); { struct group_mac *p = (struct group_mac *)ie->data; for (; (u8 *)p - ie->data < ie->len && *p->name; ++p) printf("%s=%d "       , p->name, p->mac_num                                                  ); break; }
            case IE_IPV6_ADDR: printf("[IPV6_ADDR] "); { struct ipv6_addr *p = (struct ipv6_addr *)ie->data; for (; (u8 *)p - ie->data < ie->len && NZ128(p); ++p) printf("%pI6 "        , p                                                                    ); break; }
            case IE_LOC_CHK  : printf("[LOC_CHK  ] "); { struct location  *p = (struct location  *)ie->data; for (i = 0; i < MAX_LOC_APS && NZ48(&p->aps[i]); ++i) printf("%pM %d %d %d ", p->aps[i].addr, p->aps[i].rssi, p->aps[i].bias, ntohl(p->aps[i].time)); break; }

            // handle structure IEs
            case IE_COUNTS   : printf("[COUNTS   ] "); { struct array_cnt *p = (struct array_cnt *)ie->data; printf("%d %d %d %d %d %d", p->iaps , p->iaps_up, p->ssids, p->ssids_up, ntohl(p->time_up), ntohs(p->stations)); break; }
            case IE_POSITION : printf("[POSITION ] "); { struct position  *p = (struct position  *)ie->data; printf("%d %d %s"         , p->mount, p->scope  , p->map                                                      ); break; }
            case IE_BOUNDARY : printf("[BOUNDARY ] "); { break; }

            // unknown IEs
            default          : printf("[UNKNOWN  ] "); { break; }
        }
        printf("\n");
    }
    printf("[XRP   ] ----------------------------------------------------------------------\n");
}

static void
XrpTimeout(void)
{
    puts("Timeout : ");
    net_set_state(NETLOOP_FAIL);
    return;
}

xrp_decl(int packet_count);

static void
XrpHandler(uchar *xrp_packet, unsigned dest, IPaddr_t sip, unsigned src, unsigned length)
{
    struct xrp_beacon_pkt *xrp_beacon = (struct xrp_beacon_pkt *)xrp_packet;
    struct xrp_info_element *ie = NULL;

    debug("%s\n", __func__);

    if (dest == XRP_UDP_SOCKET_DEFAULT && length >= sizeof(struct xrp_beacon_pkt) + sizeof(struct xrp_info_element) && xrp_beacon->header.cmd == XC_IPV4_BEACON)
    {
        if (XrpSetTFTP) {
            char *wildcard;
            char *image_name_tmp  = "XS-X.Y.Z-NNNN";
            int   image_name_len  = strlen(image_name_tmp);

            if  (!XrpSetTFTPImage[0]) {
                printf("Fail : image not set\n");
                net_set_state(NETLOOP_FAIL);
            }
            image_name_len = min(image_name_len, strlen(XrpSetTFTPImage));
            if ((wildcard = strchr(XrpSetTFTPImage, '*')) != NULL) {
                image_name_len = wildcard - XrpSetTFTPImage;
            }
            demangle(xrp_packet, length);

            while ((ie = ie_next(xrp_packet, length, ie)) != NULL) {
#ifdef XRP_DEBUG
                if (ie->id == IE_HOSTNAME) {
                    char    hostname[ ie->len + 1 ];
                    strxcpy(hostname, ie->data);
                    printf("%shostname: %s", packet_count++ ? "[XRP   ] " : "", hostname);
                }
#endif
                if (ie->id == IE_RECOVERY) {
                    char    bootfile_remote[ ie->len + 1 ];
                    strxcpy(bootfile_remote, ie->data);

                    if (strncasecmp(XrpSetTFTPImage, bootfile_remote, min(ie->len, image_name_len)) == 0) {
                        char    serverip[ REQ_LEN_IPV4_ADDRSTR + 1 ];
                        sprintf(serverip, "%pI4", xrp_beacon->beacon.ip);

                        // set serverip & bootfile_remote from xrp info (env will be saved in cmd_bootm)
                        setenv("serverip", serverip);
                        setenv("bootfile_remote", bootfile_remote);

                        // flag success first so "more" functions correctly while printing
                        net_set_state(NETLOOP_SUCCESS);
                        xrp_debug(", ");
                        printf("serverip: %s, bootfile_remote: %s\n", serverip, bootfile_remote);
                        return;
                    }
                }
            }
            xrp_debug("\n");
        }
        else {
            // flag success first so "more" functions correctly while printing
            net_set_state(NETLOOP_SUCCESS);
            dumpXrpBeacon(xrp_packet, length);
        }
    }
}

void
XrpStart(void)
{
    debug("%s\n", __func__);

    xrp_decl(packet_count = 0);
    NetSetTimeout(XrpSetTFTP ? XRP_TIMEOUT : XRP_TIMEOUT/10, XrpTimeout);
    net_set_udp_handler(XrpHandler);
}
