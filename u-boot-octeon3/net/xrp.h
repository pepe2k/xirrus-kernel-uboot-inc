/*
 * (C) Xirrus 2016
 *
 */

#ifndef __XRP_H__
#define __XRP_H__

//****************************************************************
// Constants (borrowed from OOPME/src/xrp.h & include/xircfg.h)
//----------------------------------------------------------------
#define XRP_MANGLE_SALT_LEN         22
#define XRP_UDP_SOCKET_DEFAULT      22610   //'XR'

// XRP packet types
#define XT_OPEN                     0
#define XT_TUNNELED                 1

// XRP command types (the only one we care about)
#define XC_IPV4_BEACON              6

// XRP beacon information elements
#define IE_HOSTNAME                 1
#define IE_LOCATION                 2
#define IE_COUNTS                   3
#define IE_XRP_MODE                 4
#define IE_SOFTWARE                 5
#define IE_MODEL                    6
#define IE_SERIAL                   7
#define IE_MAX_STAS                 8
#define IE_XBL_NAME                 9
#define IE_SCD_NAME                 10
#define IE_LICENSE                  11
#define IE_FEATURES                 12
#define IE_STA_TYPES                13
#define IE_CHAN_LIST                14
#define IE_POSITION                 15
#define IE_LOC_CHK                  16
#define IE_GROUPS                   17
#define IE_IPV6_ADDR                18
#define IE_80211R                   19
#define IE_RECOVERY                 20
#define IE_FLASH                    21
#define IE_BOUNDARY                 22

// Misc. string/array lengths
#define REQ_LEN_GROUP               (32 + 1)
#define REQ_LEN_MAP_STR             (40 + 1)
#define REQ_LEN_IPV4_ADDRSTR        (15 + 1)
#define REQ_LEN_SOFTWARE_VERSION    (60 + 1)
#define IPV6_ADDR_LEN               16
#define MAX_LOC_APS                 5

//****************************************************************
// strxncpy -- safe strncpy -- force null termination (borrowed from include/common.h)
//----------------------------------------------------------------
#define strxncpy(d, s, l)       ({ char *_d=(char *)(d); const char *_s=(const char *)(s); int _l=l; if (_d && _s) strncpy(_d,_s,_l); if (_d) _d[_l ? _l-1 : 0]=0; _d; })
#define strxcpy( d, s)          strxncpy(d, s, sizeof(d))

//****************************************************************
// XRP packet definitions (borrowed from OOPME/src/xrp.h)
//----------------------------------------------------------------
struct xrp_header {
    u8  cmd;
    u8  type;
    u8  salt[XRP_MANGLE_SALT_LEN];
} __attribute__ ((packed));

struct xrp_info_element {
    u8  id;
    u8  len;
    u8  data[1];
} __attribute__ ((packed)) ;

struct xrp_ipv4_beacon {
    u8  mac[6];
    u8  ip [4];
    struct xrp_info_element ies[0];
} __attribute__ ((packed));

struct xrp_beacon_pkt {
    struct xrp_header      header;
    struct xrp_ipv4_beacon beacon;
} __attribute__ ((packed));

//****************************************************************
// XRP information element definitions (borrowed from include/xircfg.h)
//----------------------------------------------------------------
struct array_cnt {              // IE_COUNTS
    u8  iaps;
    u8  iaps_up;
    u8  ssids;
    u8  ssids_up;
    u32 time_up;
    u16 stations;
} __attribute__ ((packed));

struct position {               // IE_POSITION
    float x;
    float y;
    float z;
    float scale;
    float angle;
    u8    mount;
    u8    scope;
    char  map[REQ_LEN_MAP_STR];
    struct {
        float angle    ;
        float latitude ;
        float longitude;
        float elevation;
        u8    reference;
    } gps;
} __attribute__ ((packed));

struct location {               // IE_LOC_CHK
    float x;
    float y;
    float z;
    struct {
        u8  addr[6];
        s8  rssi;
        s8  bias;
        u32 time;
    } aps[MAX_LOC_APS];
    float location_error;
    float angular_stddev;
} __attribute__ ((packed));

struct group_mac {              // IE_GROUPS
    char   name[REQ_LEN_GROUP];
    u8     mac_num;
} __attribute__ ((packed));

struct ipv6_addr {              // IE_IPV6_ADDR
    u8 addr[IPV6_ADDR_LEN];
} __attribute__ ((packed));

//****************************************************************
// XRP prototypes
//----------------------------------------------------------------
extern void XrpStart(void);	/* Begin XRP */

#endif /* __XRP_H__ */
